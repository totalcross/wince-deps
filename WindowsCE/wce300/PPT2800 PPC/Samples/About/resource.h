//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDC_APPICON                     101
#define IDS_ABOUT1                      102
#define IDS_ABOUT2                      103
#define IDC_ABOUT1                      104
#define IDC_ABOUT2                      105
#define IDD_ABOUT_PPC_STD               107
#define IDD_ABOUT_7500_STD              109
#define IDD_ABOUT_PPC_SIP               111
#define IDD_ABOUT_7500_TSK              112
#define IDD_ABOUT_7200_TSK              114
#define IDD_ABOUT_7200_STD              115
#define IDD_ABOUT_PC_STD                116
#define IDD_ABOUT_HPC_STD               117

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        120
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
