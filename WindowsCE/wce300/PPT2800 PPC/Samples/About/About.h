
//--------------------------------------------------------------------
// FILENAME:			About.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for About.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef ABOUT_H_

#define ABOUT_H_

#ifdef __cplusplus
extern "C"
{
#endif

//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------

#include "resource.h"

#include "..\common\StdGlobs.h"

#include "..\common\StdAbout.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("AboutTest")
#define APP_TITLE TEXT("About Box Program")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDABOUT_H_	*/

