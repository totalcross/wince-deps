// ReadSmsDlg.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"

#include "ReadSmsDlg.h"
#include "Gsm.h"


/////////////////////////////////////////////////////////////////////////////
// External variables
/////////////////////////////////////////////////////////////////////////////
extern	CGsm gsmDevice;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReadSmsDlg dialog


CReadSmsDlg::CReadSmsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReadSmsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReadSmsDlg)
	//}}AFX_DATA_INIT
}


void CReadSmsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReadSmsDlg)
	DDX_Control(pDX, IDC_EDT_DATE, m_edtDate);
	DDX_Control(pDX, IDC_TXT_MESSAGE, m_txtMessage);
	DDX_Control(pDX, IDC_EDT_STATUS, m_edtStatus);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReadSmsDlg, CDialog)
	//{{AFX_MSG_MAP(CReadSmsDlg)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_CLEAR, OnBtnClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReadSmsDlg message handlers

BOOL CReadSmsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Launch timer which handles the message checking in the background
	m_nTimer = SetTimer(2, 200, 0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReadSmsDlg::OnTimer(UINT nIDEvent) 
{
	static BOOL fInit = TRUE;
	int nError = GSM_NO_ERROR;


	if (fInit)
	{
		m_edtStatus.SetWindowText( TEXT("Deleting all messages...") );	

		gsmDevice.m_bCommActive = TRUE;	
		HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));

		// Delete all messages
		nError = gsmDevice.DeleteAllTextSms();

		gsmDevice.m_bCommActive = FALSE;
		SetCursor(hOldCursor);

		if (nError != GSM_NO_ERROR)
			m_edtStatus.SetWindowText( TEXT("Failed to delete messages !") );

		KillTimer(m_nTimer);
		fInit = FALSE;
		m_nTimer = SetTimer(2, 6000, 0);
		m_edtStatus.SetWindowText( TEXT("Waiting for a message...") );	
	}
	else
	{
		gsmDevice.m_bCommActive = TRUE;	

		m_edtStatus.SetWindowText( TEXT("Reading message list...") );

		// Check for received message
		if ( gsmDevice.ReadTextSms(1) == GSM_NO_ERROR )
		{
			m_txtMessage.SetWindowText(gsmDevice.m_smsMsg.strMsg);

			CString strTemp;
			strTemp.Format( TEXT("%s, %s"), gsmDevice.m_smsMsg.strDate, gsmDevice.m_smsMsg.strTime );
			m_edtDate.SetWindowText(strTemp);

			m_edtStatus.SetWindowText( TEXT("Deleting a message...") );	
			gsmDevice.DeleteTextSms(1);
		}

		m_edtStatus.SetWindowText( TEXT("Waiting for a message...") );	

		gsmDevice.m_bCommActive = FALSE;
	}

	
	CDialog::OnTimer(nIDEvent);
}

void CReadSmsDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Destroy the timer if we started it
	if (gsmDevice.m_bRadioPresent)
		KillTimer(m_nTimer);
}

void CReadSmsDlg::OnBtnClear() 
{
	// Clear the message
	m_txtMessage.SetWindowText( TEXT("") );
	m_edtDate.SetWindowText( TEXT("") );
}
