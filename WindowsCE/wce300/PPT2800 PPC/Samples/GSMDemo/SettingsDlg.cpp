// SettingsDlg.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"
#include "SettingsDlg.h"


#include "Gsm.h"
#include "UserMsgs.h"


/////////////////////////////////////////////////////////////////////////////
// External variables
/////////////////////////////////////////////////////////////////////////////
extern	CGsm gsmDevice;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog


CSettingsDlg::CSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSettingsDlg)
	//}}AFX_DATA_INIT
}


void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsDlg)
	DDX_Control(pDX, IDC_EDT_SCA, m_edtSca);
	DDX_Control(pDX, IDC_EDT_PINSTS, m_edtPinSts);
	DDX_Control(pDX, IDC_EDT_NEWSCA, m_edtNewSca);
	DDX_Control(pDX, IDC_EDT_NEWPIN, m_edtNewPin);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingsDlg)
	ON_BN_CLICKED(IDC_BTN_UPDATE, OnBtnUpdate)
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBtnCancel)
	//}}AFX_MSG_MAP

	// User defined messages
	ON_MESSAGE(UM_INIT_DLG, OnUserInitDlg)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg message handlers

BOOL CSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Clear fields of new data entry
	m_edtNewPin.SetWindowText( TEXT("") );
	m_edtNewSca.SetWindowText( TEXT("") );


	// Post a message to initialize dialog
	PostMessage(UM_INIT_DLG, 0, 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingsDlg::OnUserInitDlg(WPARAM wParam, LPARAM lParam)
{
	CString strSca;
	DWORD dwSimSts;

	gsmDevice.m_bCommActive = TRUE;	
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));


	// Get the SCA, and SIM card status
	strSca = gsmDevice.GetSca();
	dwSimSts = gsmDevice.GetSimSts();

	gsmDevice.m_bCommActive = FALSE;
	SetCursor(hOldCursor);


	if ( strSca.IsEmpty() )
		m_edtSca.SetWindowText( TEXT("Error reading SCA") );
	else
		m_edtSca.SetWindowText(strSca);


	if (dwSimSts == SIMSTAT_NOCARD)
	{
		m_edtPinSts.SetWindowText( TEXT("SIM Not Present") );
		m_edtNewPin.EnableWindow(FALSE);	// disable pin entry
	}
	else if (dwSimSts == SIMSTAT_READY)
	{
		m_edtPinSts.SetWindowText( TEXT("SIM Ready") );
		m_edtNewPin.EnableWindow(FALSE);	// disable pin entry
	}
	else
	{
		m_edtPinSts.SetWindowText( TEXT("PIN Required") );
	}
}

void CSettingsDlg::OnBtnUpdate() 
{
	CString strSca, strPin;
	BOOL fScaDone = FALSE;
	BOOL fPinDone = FALSE;

	m_edtNewSca.GetWindowText(strSca);
	m_edtNewPin.GetWindowText(strPin);


	if ( strSca.IsEmpty() && strPin.IsEmpty() )
	{
		MessageBox( TEXT("No changes were made, aboring update !") );
		return;
	}


	gsmDevice.m_bCommActive = TRUE;	
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));

	// Update SCA
	if ( !strSca.IsEmpty() )
	{
		if ( ! (fScaDone = gsmDevice.SetSca(strSca)) )
			MessageBox( TEXT("Error setting SCA !") );
	}


	// Update PIN #
	if ( !strPin.IsEmpty() )
	{
		if ( ! (fPinDone = gsmDevice.SetPin(strPin)) )
			MessageBox( TEXT("Error setting PIN # !") );
	}

	gsmDevice.m_bCommActive = FALSE;
	SetCursor(hOldCursor);

	if (fScaDone || fPinDone)
		MessageBox( TEXT("Update done !") );
}

void CSettingsDlg::OnBtnCancel() 
{
	PostMessage(WM_CLOSE, 0, 0);
}
