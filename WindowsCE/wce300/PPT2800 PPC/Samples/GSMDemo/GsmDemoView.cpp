// GsmDemoView.cpp : implementation of the CGsmDemoView class
//

#include "stdafx.h"
#include "GsmDemo.h"

#include "GsmDemoDoc.h"
#include "GsmDemoView.h"

#include "UserMsgs.h"
#include "Gsm.h"
#include "SendSmsDlg.h"
#include "ReadSmsDlg.h"
#include "Dial.h"


/////////////////////////////////////////////////////////////////////////////
// Global variables
/////////////////////////////////////////////////////////////////////////////
CGsm gsmDevice;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoView

IMPLEMENT_DYNCREATE(CGsmDemoView, CFormView)

BEGIN_MESSAGE_MAP(CGsmDemoView, CFormView)
	//{{AFX_MSG_MAP(CGsmDemoView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_REG, OnBtnReg)
	ON_BN_CLICKED(IDC_BTN_DEREG, OnBtnDereg)
	ON_BN_CLICKED(IDC_BTN_SENDSMS, OnBtnSendSms)
	ON_BN_CLICKED(IDC_BTN_READSMS, OnBtnReadSms)
	ON_BN_CLICKED(IDC_BTN_DIAL, OnBtnDial)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP

	// User defined messages
	ON_MESSAGE(UM_INIT_APP, OnUserInitApp)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoView construction/destruction

CGsmDemoView::CGsmDemoView()
	: CFormView(CGsmDemoView::IDD)
{
	//{{AFX_DATA_INIT(CGsmDemoView)
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CGsmDemoView::~CGsmDemoView()
{
}

void CGsmDemoView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGsmDemoView)
	DDX_Control(pDX, IDC_EDT_STATUS, m_edtRegStatus);
	DDX_Control(pDX, IDC_PGS_SIGNAL, m_pgsSignal);
	//}}AFX_DATA_MAP
}

BOOL CGsmDemoView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoView diagnostics

#ifdef _DEBUG
void CGsmDemoView::AssertValid() const
{
	CFormView::AssertValid();
}

void CGsmDemoView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmDemoDoc* CGsmDemoView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmDemoDoc)));
	return (CGsmDemoDoc*)m_pDocument;
}
#endif //_DEBUG


void CGsmDemoView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// RSSI Progress bar range = RSSI valid dBm range.
	// Set range and reset position to 0
	m_pgsSignal.SetRange32(PGS_RSSI_MIN, PGS_RSSI_MAX);
	m_pgsSignal.SetPos(PGS_RSSI_MIN);

	// Post a message to initialize application
	PostMessage(UM_INIT_APP, 0, 0);
}

void CGsmDemoView::OnUserInitApp(WPARAM wParam, LPARAM lParam)
{
	int nResult = GSM_NO_ERROR;

	m_edtRegStatus.SetWindowText( TEXT("Initializing...") );
	
	// Initialize modem and, retrieve general information
	nResult = gsmDevice.InitModem();

	if(GSM_NO_ERROR == nResult)
	{
		// Flag presence of radio
		gsmDevice.m_bRadioPresent = TRUE;
		gsmDevice.m_bRadioStatus = TRUE;
		
		gsmDevice.m_nTimer = SetTimer(1, 50, 0);	// Launch timer...
	}
	else
	{
		// Flag absence of radio
		gsmDevice.m_bRadioPresent = FALSE;		
		MessageBox(_T("GSM radio not present !"));

		// Set status to indicate no radio installed
		m_edtRegStatus.SetWindowText( TEXT("NO RADIO !") );
	}
}

void CGsmDemoView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// Close the comm port
	if(INVALID_HANDLE_VALUE != gsmDevice.m_hComm)
		CloseHandle(gsmDevice.m_hComm);

	// Destroy the timer if we started it
	if (gsmDevice.m_bRadioPresent)
		KillTimer(gsmDevice.m_nTimer);
}

void CGsmDemoView::OnTimer(UINT nIDEvent) 
{
	static BOOL fRequestRssi = FALSE;	// request reg status when true, RSSI when false

	if( (gsmDevice.m_bRadioPresent == TRUE) && (gsmDevice.m_bRadioStatus == TRUE) )
	{
		// Set status to indicate we are not yet registered
		m_edtRegStatus.SetWindowText( TEXT("Not registered") );
		KillTimer(gsmDevice.m_nTimer);
		gsmDevice.m_nTimer = SetTimer(1, 6000, 0);
		gsmDevice.m_bRadioStatus = FALSE;
	}

	// Check for communications in progress
	if(FALSE == gsmDevice.m_bCommActive)
	{
		if (fRequestRssi)
		{
			RequestRssi();
			fRequestRssi = FALSE;
		}
		else
		{
			RequestRegSts();
			fRequestRssi = TRUE;
		}
	} // if com port is available

	
	CFormView::OnTimer(nIDEvent);
}

void CGsmDemoView::RequestRegSts()
{
	int nRegStatus = REGSTAT_UNKNOWN;

	if (gsmDevice.GetRegStatus(&nRegStatus) == GSM_NO_ERROR)
	{
		if ( (nRegStatus == REGSTAT_HOME) || (nRegStatus == REGSTAT_ROAM) )
		{
			// REGISTERED
			gsmDevice.m_bRegistered = TRUE;

			if (nRegStatus == REGSTAT_HOME)
				m_edtRegStatus.SetWindowText( TEXT("Registered HOME") );
			else
				m_edtRegStatus.SetWindowText( TEXT("Registered ROAM") );
		}
		else
		{
			// NOT REGISTERED or SEARCHING
			gsmDevice.m_bRegistered = FALSE;

			if (nRegStatus == REGSTAT_SEARCHING)
				m_edtRegStatus.SetWindowText( TEXT("Searching...") );
			else
				m_edtRegStatus.SetWindowText( TEXT("Not registered") );
		}
	}
}

void CGsmDemoView::RequestRssi()
{
	int nRssi;
	int nPos = PGS_RSSI_MIN;

	if (gsmDevice.GetRssi(&nRssi) == GSM_NO_ERROR)
	{
		// If rssi is detectable, set progress bar
		// (bar range = rssi dbm range)
		if (nRssi != 99)
			nPos = nRssi;
	}

	// Cap new position to the progress bar range and set it
	nPos = max(min(nPos, PGS_RSSI_MAX), PGS_RSSI_MIN);
	m_pgsSignal.SetPos(nPos);
}

// Register to network
void CGsmDemoView::OnBtnReg() 
{
	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		MessageBox( TEXT("GSM radio not present !") );
		return;
	}

	// If already registered, abort
	if(gsmDevice.m_bRegistered)
	{
		MessageBox( TEXT("Already registered with network") );
		return;
	}

	gsmDevice.m_bCommActive = TRUE;

	// Set status to indicate we are registering...
	m_edtRegStatus.SetWindowText( TEXT("Registering...") );
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));

	// Register to network
	gsmDevice.m_bRegistered = gsmDevice.Register();
	if (gsmDevice.m_bRegistered)
	{
		// If registered, check for home/roam network
		if (gsmDevice.m_nRegStatus == REGSTAT_HOME)
			m_edtRegStatus.SetWindowText( TEXT("Registered HOME") );
		else
			m_edtRegStatus.SetWindowText( TEXT("Registered ROAM") );
	}
	else
		m_edtRegStatus.SetWindowText( TEXT("Not registered") );

	SetCursor(hOldCursor);
	gsmDevice.m_bCommActive = FALSE;
	
	
	// Display message box with the result
	if (gsmDevice.m_bRegistered)
		MessageBox( TEXT("Registered successfully") );
	else
		MessageBox( TEXT("Registration failed !") );
}

void CGsmDemoView::OnBtnDereg() 
{
	int nResult = GSM_NO_ERROR;

	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		MessageBox( TEXT("GSM radio not present !") );
		return;
	}

	// If not registered, abort
	if(!gsmDevice.m_bRegistered)
	{
		MessageBox( TEXT("Not registered") );
		return;
	}

	gsmDevice.m_bCommActive = TRUE;


	// Set status to indicate we are registering...
	m_edtRegStatus.SetWindowText( TEXT("De-registering...") );
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));

	gsmDevice.m_bRegistered = !gsmDevice.Deregister();

	if (!gsmDevice.m_bRegistered)
		m_edtRegStatus.SetWindowText( TEXT("Not registered") );
	else
	{
		// If registered, check for home/roam network
		if (gsmDevice.m_nRegStatus == REGSTAT_HOME)
			m_edtRegStatus.SetWindowText( TEXT("Registered HOME") );
		else
			m_edtRegStatus.SetWindowText( TEXT("Registered ROAM") );
	}

	SetCursor(hOldCursor);
	gsmDevice.m_bCommActive = FALSE;


	// Display message box with the result
	if (!gsmDevice.m_bRegistered)
		MessageBox( TEXT("De-registered successfully") );
	else
		MessageBox( TEXT("De-registration failed !") );
}


void CGsmDemoView::OnBtnSendSms() 
{
	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		MessageBox( TEXT("GSM radio not present !") );
		return;
	}

	// If not registered, abort
	if(!gsmDevice.m_bRegistered)
	{
		MessageBox( TEXT("Not registered with network !") );
		return;
	}	
		
	CSendSmsDlg dlgSendSms;
	dlgSendSms.DoModal();
}

void CGsmDemoView::OnBtnReadSms() 
{
	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		MessageBox( TEXT("GSM radio not present !") );
		return;
	}

	// If not registered, abort
	if(!gsmDevice.m_bRegistered)
	{
		MessageBox( TEXT("Not registered with network !") );
		return;
	}	

	CReadSmsDlg dlgReadSms;
	dlgReadSms.DoModal();
}

void CGsmDemoView::OnBtnDial() 
{
	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		MessageBox( TEXT("GSM radio not present !") );
		return;
	}


	CDial dlgDial;
	dlgDial.DoModal();
}
