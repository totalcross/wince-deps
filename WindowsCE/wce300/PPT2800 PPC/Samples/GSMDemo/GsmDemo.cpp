// GsmDemo.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GsmDemo.h"

#include "MainFrm.h"

#include "GsmDemoDoc.h"
#include "GsmDemoView.h"

#include "Gsm.h"
#include "SettingsDlg.h"


/////////////////////////////////////////////////////////////////////////////
// External variables
/////////////////////////////////////////////////////////////////////////////
extern	CGsm gsmDevice;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoApp

BEGIN_MESSAGE_MAP(CGsmDemoApp, CWinApp)
	//{{AFX_MSG_MAP(CGsmDemoApp)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_COMMAND(ID_OPTIONS_EXIT, OnOptionsExit)
	ON_COMMAND(ID_OPTIONS_SETTINGS, OnOptionsSettings)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoApp construction

CGsmDemoApp::CGsmDemoApp()
	: CWinApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGsmDemoApp object

CGsmDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoApp initialization

BOOL CGsmDemoApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// Change the registry key under which our settings are stored.
	// You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));


	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CGsmDemoDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CGsmDemoView));
	AddDocTemplate(pDocTemplate);
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();


	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CEdit	m_edtFwDate;
	CEdit	m_edtVendor;
	CEdit	m_edtSerialNum;
	CEdit	m_edtModel;
	CEdit	m_edtFwVer;
	CEdit	m_edtFwRev;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();		// Added for WCE apps
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_EDT_FWDATE, m_edtFwDate);
	DDX_Control(pDX, IDC_EDT_VENDOR, m_edtVendor);
	DDX_Control(pDX, IDC_EDT_SERIALNUM, m_edtSerialNum);
	DDX_Control(pDX, IDC_EDT_MODEL, m_edtModel);
	DDX_Control(pDX, IDC_EDT_FWVER, m_edtFwVer);
	DDX_Control(pDX, IDC_EDT_FWREV, m_edtFwRev);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGsmDemoApp commands
// Added for WCE apps

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Update GSM modem information
	m_edtVendor.SetWindowText(gsmDevice.m_strVendor);
	m_edtModel.SetWindowText(gsmDevice.m_strModel);
	m_edtFwVer.SetWindowText(gsmDevice.m_strFwVer);
	m_edtFwRev.SetWindowText(gsmDevice.m_strFwRev);
	m_edtFwDate.SetWindowText(gsmDevice.m_strFwDate);
	m_edtSerialNum.SetWindowText(gsmDevice.m_strSerialNum);


	CenterWindow();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CGsmDemoApp::OnHelpAbout() 
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CGsmDemoApp::OnOptionsExit() 
{
	m_pMainWnd->PostMessage(WM_CLOSE, 0, 0);
}

void CGsmDemoApp::OnOptionsSettings() 
{
	// If no radio, abort
	if(!gsmDevice.m_bRadioPresent)
	{
		AfxMessageBox( TEXT("GSM radio not present !") );
		return;
	}


	CSettingsDlg settingsDlg;;
	settingsDlg.DoModal();
}
