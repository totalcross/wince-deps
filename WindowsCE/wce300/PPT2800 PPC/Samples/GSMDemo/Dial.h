#if !defined(AFX_DIAL_H__031B421D_6CA4_44A4_BA46_3A56BAE6FBB2__INCLUDED_)
#define AFX_DIAL_H__031B421D_6CA4_44A4_BA46_3A56BAE6FBB2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Dial.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDial dialog

class CDial : public CDialog
{
// Construction
public:
	CDial(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDial)
	enum { IDD = IDD_DIAL };
	CSliderCtrl	m_sldVolume;
	CButton	m_btnMute;
	CEdit	m_edtStatus;
	CEdit	m_edtNumber;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDial)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

protected:

	// Generated message map functions
	//{{AFX_MSG(CDial)
	afx_msg void OnBtnDial();
	afx_msg void OnBtnHangup();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBtnMute();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIAL_H__031B421D_6CA4_44A4_BA46_3A56BAE6FBB2__INCLUDED_)
