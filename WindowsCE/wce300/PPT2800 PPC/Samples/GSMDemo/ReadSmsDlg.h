#if !defined(AFX_READSMSDLG_H__50FE0C54_B3DF_47C4_93CF_AEE9F64150CF__INCLUDED_)
#define AFX_READSMSDLG_H__50FE0C54_B3DF_47C4_93CF_AEE9F64150CF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ReadSmsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CReadSmsDlg dialog

class CReadSmsDlg : public CDialog
{
// Construction
public:
	CReadSmsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CReadSmsDlg)
	enum { IDD = IDD_READSMS };
	CEdit	m_edtDate;
	CStatic	m_txtMessage;
	CEdit	m_edtStatus;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReadSmsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	UINT m_nTimer;


	// Generated message map functions
	//{{AFX_MSG(CReadSmsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBtnClear();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_READSMSDLG_H__50FE0C54_B3DF_47C4_93CF_AEE9F64150CF__INCLUDED_)
