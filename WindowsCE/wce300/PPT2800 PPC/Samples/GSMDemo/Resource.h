//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GsmDemo.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_GSMDEMO_FORM                101
#define IDS_EDIT                        102
#define IDS_TOOL                        103
#define IDM_MENU                        104
#define IDR_MAIN_EDIT                   105
#define IDR_MAIN_TOOL                   106
#define IDR_MAINFRAME                   128
#define IDD_SENDSMS                     130
#define IDD_DIAL                        132
#define IDD_READSMS                     133
#define IDD_SETTINGS                    135
#define IDC_EDT_STATUS                  1001
#define IDC_PGS_SIGNAL                  1003
#define IDC_BTN_REG                     1004
#define IDC_BTN_DEREG                   1005
#define IDC_BTN_SENDSMS                 1006
#define IDC_BTN_READSMS                 1007
#define IDC_BTN_DIAL                    1008
#define IDC_EDT_GSMVERSION              1009
#define IDC_EDT_VENDOR                  1009
#define IDC_EDT_NUMBER                  1010
#define IDC_EDT_MESSAGE                 1011
#define IDC_BTN_SEND                    1012
#define IDC_EDT_MODEL                   1015
#define IDC_EDT_FWVER                   1016
#define IDC_EDT_FWREV                   1017
#define IDC_EDT_SERIALNUM               1018
#define IDC_EDT_FWDATE                  1019
#define IDC_BTN_HANGUP                  1023
#define IDC_BTN_CLEAR                   1025
#define IDC_EDT_DATA                    1026
#define IDC_BTN_VIEW                    1032
#define IDC_BTN_DELETE                  1033
#define IDC_LIST1                       1034
#define IDC_LST_MESSAGES                1034
#define IDC_LST_MESSAGE                 1034
#define IDC_EDT_NEWSCA                  1036
#define IDC_EDT_PIN                     1037
#define IDC_EDT_PINSTS                  1037
#define IDC_BTN_UPDATE                  1038
#define IDC_EDT_NEWPIN                  1040
#define IDC_BTN_CANCEL                  1041
#define IDC_EDT_SCA                     1043
#define IDC_TXT_MESSAGE                 1047
#define IDC_EDT_DATE                    1049
#define IDC_BTN_MUTE                    1054
#define IDC_SLIDER_VOLUME               1055
#define IDC_BTN_SET_VOL                 1056
#define IDC_SLIDER1                     1058
#define IDS_CAP_HELP                    32773
#define ID_HELP_ABOUT                   32774
#define ID_OPTIONS                      32775
#define IDS_CAP_OPTIONS                 32777
#define ID_OPTIONS_EXIT                 32778
#define ID_OPTIONS_SETTINGS             32779
#define ID_HELP_MODEMINFO               32781
#define IDS_NEW                         65000
#define IDS_FILE                        65001
#define IDS_MHELP                       65002
#define IDS_SAVE                        65003
#define IDS_CUT                         65004
#define IDS_COPY                        65005
#define IDS_PASTE                       65006
#define IDS_ABOUT                       65007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1059
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
