//--------------------------------------------------------------------
// FILENAME:            GSM.cpp
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// PLATFORMS:       Windows CE, Pocket PC, Strong ARM, PPT 2800
//
// NOTES:           Public
//
// %IF Symbol_Internal
// AUTHOR(s):       Eyal Peretz
//
// CREATION DATE:       
//
// DERIVED FROM:    
//
// EDIT HISTORY:
//
// %End
//--------------------------------------------------------------------

#include "stdafx.h"
#include "Gsm.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGsm::CGsm()
{
	m_szCommPort = "COM6:";
	m_dwBaud = CBR_9600;
	m_strPin = TEXT("0000");

	m_bRadioPresent = FALSE;
	m_bRegistered = FALSE;
	m_bCommActive = FALSE;
	m_bRadioStatus = FALSE;
	m_bConnected = FALSE;

	m_strVendor.Empty();
	m_strModel.Empty();
	m_strFwVer.Empty();
	m_strFwRev.Empty();
	m_strFwDate.Empty();
	m_strSerialNum.Empty();
}

/////////////////////////////////////////////////////////////////////////////

CGsm::~CGsm()
{
	if(INVALID_HANDLE_VALUE != m_hComm)
	{
		EscapeCommFunction(m_hComm, CLRDTR);
		EscapeCommFunction(m_hComm, CLRRTS);
		CloseHandle(m_hComm);
	}
}

/////////////////////////////////////////////////////////////////////////////

int CGsm::Open(LPCTSTR szCommPort, DWORD dwBaud)
{
	DCB dcb;
	COMMTIMEOUTS cto;

	m_hComm = CreateFile(szCommPort,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);
	if(INVALID_HANDLE_VALUE==m_hComm) 
		return COM_PORT_NOT_OPENED;

	m_dwBaud = dwBaud;

	GetCommState(m_hComm, &dcb);
	dcb.BaudRate = m_dwBaud;
	dcb.fParity = FALSE;
	dcb.fNull = FALSE;
	dcb.StopBits = ONESTOPBIT;
	dcb.Parity = NOPARITY;
	dcb.ByteSize = 8;
	dcb.fRtsControl = RTS_CONTROL_ENABLE;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	SetCommState(m_hComm,&dcb);

	cto.ReadIntervalTimeout = 0;
	cto.ReadTotalTimeoutConstant = 500;
	cto.ReadTotalTimeoutMultiplier = 10;

	cto.WriteTotalTimeoutConstant = 0;
	cto.WriteTotalTimeoutMultiplier =0;

	SetCommTimeouts(m_hComm, &cto);

	PurgeComm(m_hComm, PURGE_TXCLEAR | PURGE_RXCLEAR);

	EscapeCommFunction(m_hComm, SETRTS);
	EscapeCommFunction(m_hComm, SETDTR);

	return(GSM_NO_ERROR);
}

/////////////////////////////////////////////////////////////////////////////

int CGsm::ConfigTextSms()
{
	static BOOL fConfigured = FALSE;

	// Configure only once
	if (fConfigured)
		return(GSM_NO_ERROR);

	// Select the TEXT mode for SMS messages
	Send(_T("AT+CMGF=1\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_SELECT_TEXT_ERROR);

 	// Select message service
	Send(_T("AT+CSMS=0\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_SMS_NOT_SUPPORTED);

	// Select Preferred Message Storage
	Send(_T("AT+CPMS=\"SM\"\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_STORAGE_ERROR);

	// Indictae configured
	fConfigured = TRUE;

	return(GSM_NO_ERROR);
}

int CGsm::SendTextSms(CString strNumber, CString strMsg)
{
	CString strCmd;
	int nError = GSM_NO_ERROR;

	// Configure SMS mode to TEXT
	if ( (nError = ConfigTextSms()) != GSM_NO_ERROR)
		return(nError);

	// Format and send message
	strCmd.Format( TEXT("AT+CMGS=\"%s\"\r"), strNumber);
	Send(strCmd);
	if (!WaitForReply( TEXT(">"), MODEM_COMMAND_TIMEOUT))
		return(GSM_SENDMSG_ERROR);


	// Append CTRL+Z to end message and send it
	// We don't want to purge buffers since the message may still be on its way out
	strMsg += CTRL_Z;
	Send(strMsg, FALSE);
	if (!WaitForReply( TEXT("OK"), NETWORK_SENDMSG_TIMEOUT))
		return(GSM_SENDMSG_ERROR);


	return(GSM_NO_ERROR);
}

int CGsm::DeleteAllTextSms(void)
{
	int nError = GSM_NO_ERROR;

	// Configure SMS mode to TEXT
	if ( (nError = ConfigTextSms()) != GSM_NO_ERROR)
		return(nError);

	Send( TEXT("AT+CMGD=1,4\r") );
	if (!WaitForReply( TEXT("OK"), NETWORK_DELETEMSG_TIMEOUT))
		return(GSM_DELETEMSG_ERROR);

	return(nError);
}

int CGsm::DeleteTextSms(int nId)
{
	TCHAR szCmd[50];
	int nError = GSM_NO_ERROR;
	CString strTemp;

	// Configure SMS mode to TEXT
	if ( (nError = ConfigTextSms()) != GSM_NO_ERROR)
		return(nError);

	// Format command to read the specified message ID
	wsprintf(szCmd, TEXT("AT+CMGD=%d\r"), nId);
	Send(szCmd);
	if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT))
		return(GSM_DELETEMSG_ERROR);

	return(nError);
}

int CGsm::ReadTextSms(int nId)
{
	TCHAR szCmd[50];
	int nError = GSM_NO_ERROR;
	CString strTemp;

	// Configure SMS mode to TEXT
	if ( (nError = ConfigTextSms()) != GSM_NO_ERROR)
		return(nError);

	// Format command to read the specified message ID
	wsprintf(szCmd, TEXT("AT+CMGR=%d\r"), nId);
	Send(szCmd);
	if (!WaitForReply( TEXT("OK"), NETWORK_READMSG_TIMEOUT))
		return(GSM_READMSG_ERROR);


	// Response:
	// +CMGR: "<status>","<sender address>",,"<time stamp>" ... <CRLF> <msg>
	TrimReply();
	ExtractField( TEXT("\"") );
	strTemp = ExtractField( TEXT("\"") );	// get status
	m_smsMsg.fUnRead = strTemp.Find( TEXT("REC READ") ) == -1;


	ExtractField( TEXT("\"") );
	m_smsMsg.strFrom = ExtractField( TEXT("\"") );	// get sender address


	ExtractField( TEXT(",\"") );
	m_smsMsg.strDate = ExtractField( TEXT(",") );	// get date
	m_smsMsg.strTime = ExtractField( TEXT("\"") );	// get time (+ time zone)

	ExtractField( TEXT("\r\n") );
	m_smsMsg.strMsg = ExtractField(NULL);


	return(nError);
}

//////////////////////////////////////////////////////////////////////////

int CGsm::InitModem()
{
	int nResult = GSM_NO_ERROR;


	// Open COM port
	nResult = Open(m_szCommPort, m_dwBaud);
	if(nResult == COM_PORT_NOT_OPENED) 
		return(nResult);

	// Send AT command to make sure the modem is there
	Send(_T("AT\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_AT_NOT_WORKING);

	// Enable full phone functionality
	// A complete software reset is done and defaults are restored !
	Send( _T("AT+CFUN=1\r") );
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_MODEM_INIT_ERROR);

	// Reset modem, disable echo and, enable verbose response
	Send(_T("ATE0V1\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_TIMEOUT);

	// Retrieve modem information
	nResult = GetModemInfo();


	return(nResult);
}

//////////////////////////////////////////////////////////////////////////

int CGsm::GetModemInfo()
{
	CString strFwDate, strFwTime;

	// Vendor
	Send(_T("AT+CGMI\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_TIMEOUT);
	m_strVendor = TrimReply();


	// Model
	Send(_T("AT+CGMM\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_TIMEOUT);
	m_strModel = TrimReply();


	// Serial number
	Send(_T("AT+CGSN\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_TIMEOUT);
	m_strSerialNum = TrimReply();


	// Firmware version (including revision)
	Send(_T("AT+CGMR\r"));
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(GSM_TIMEOUT);

	// Get only the reply, and extract all fields
	TrimReply();
	m_strFwVer = ExtractField( TEXT(".") );
	m_strFwRev = ExtractField( TEXT(" ") );
	ExtractField( TEXT(" ") );				// get rid of unwanted field
	strFwDate = ExtractField( TEXT(" ") );
	strFwTime = ExtractField(NULL);			// get whatever left in buffer

	// Format firmware date and time stamp
	m_strFwDate.Format( TEXT("%s-%s-%s, %s"), strFwDate.Left(2), strFwDate.Mid(2,2), 
	 									      strFwDate.Right(2), strFwTime );

	return(GSM_NO_ERROR);
}

// Trim the \r\n in the beginning and the \r\n\r\nOK at the end from the reply and,
// return the resulting string as well
CString CGsm::TrimReply()
{
	CString strData = m_strRcvData;
	CString strField = m_strRcvData;
	TCHAR szFrom[] = TEXT("\r\n");
	TCHAR szTo[] = TEXT("\r\n\r\nOK");
	int nStart, nEnd;

	if ( (nStart = strData.Find(szFrom)) != -1)
	{
		// skip the starting delimiter string
		nStart += wcslen(szFrom);

		// If start point is specified the character at that pointis already excluded
		if (nStart > 0)
			nStart--;
		if ((nEnd = strData.Find(szTo, nStart)) != -1)
			strField = strData.Mid( nStart+1, (nEnd -1 - nStart) ); // exclude end delimiter
	}

	m_strRcvData = strField;
	return(strField);
}

// Extract a field from the reply ending with the specified character string
// Return the field if exists, without the delimiter
CString CGsm::ExtractField(TCHAR *pszDelim)
{
	int nPos;
	CString strField = TEXT("");

	// If delimiter is NULL, extract whatever left in buffer
	if (pszDelim == NULL)
	{
		strField = m_strRcvData;
		m_strRcvData.Empty();
	}
	else if ( (nPos = m_strRcvData.Find(pszDelim)) != -1)
	{
		// Otherwise, find the delimiter and, extract the field up to that  point

		// Get the field.
		strField = m_strRcvData.Left(nPos);

		// Update reply string to exclude the field and the delimiter strings
		nPos += wcslen(pszDelim);
		m_strRcvData = m_strRcvData.Mid(nPos);
	}

	return(strField);
}

BOOL CGsm::Close()
{
	EscapeCommFunction(m_hComm,CLRRTS);
	EscapeCommFunction(m_hComm,CLRDTR);

	CloseHandle(m_hComm);
    return 0;	
}

//
// Wait to receive reply with a timeout
//
// INPUT:   pReply - ptr to reply terminated by zero
//          DWTimeOut - time out in mili-seconds
//
// RETURN:  TRUE for success, FALSE for failure
BOOL CGsm::WaitForReply(LPCTSTR pszReply, DWORD dwTimeOut)
{
	static char cRxBuffer[256];
	BOOL fReplyOk = FALSE;
    DWORD dwBytesToRead=0, dwBytesRead=0, dwErrors=0, dwRxPos=0;
	COMSTAT Stat;			// comm status


	// Reset our receive buffers
	m_strRcvData.Empty();

	DWORD dwStartTime = GetTickCount();	// start timer

    // Wait for reply with a timeout
    // Fill in buffer with whatever received from modem,
    // If received data macthes reply, we are done.
	while ( (GetTickCount() - dwStartTime) < dwTimeOut)
    {
        // Receive whatever is in the queue
		BOOL fResult = ClearCommError(m_hComm, &dwErrors, &Stat); // get number of bytes in queue
		if (fResult == 0)
			return(FALSE);

		// Ignore any errors, let protocol take care of them
//		if (dwErrors)
//		{
//		}


		// Cache # of bytes received
		dwBytesToRead = Stat.cbInQue;

		// If received anything, read it into buffer
		if (dwBytesToRead > 0)
		{
			// Make sure we have room in buffer
			if ( dwBytesToRead >= sizeof(cRxBuffer) )
				return(FALSE);

			fResult = ReadFile(m_hComm, cRxBuffer, dwBytesToRead, &dwBytesRead, NULL);
			if (fResult == 0)
				return(FALSE);

            // Append reply to our receive buffer and,
			// compare received buffer with reply pattern,
            // If it matches, we are done.
			m_strRcvData += cRxBuffer;
			if (m_strRcvData.Find(pszReply) != -1)
                return(TRUE);

			// If modem returned error, there is no point to wait for the correct reply,
			// abort operation
			if (m_strRcvData.Find( TEXT("ERROR") ) != -1)
                return(FALSE);

		} // if received anything

	} // while not timed out
	

	// If we are here, we timed out
    return(FALSE);
}

void CGsm::ComFlush()
{
	COMSTAT ComStat;
	DWORD dwBytes = 0;
	DWORD dwErrors = 0;

    do
    {
        // Since data is ariving continuously,
        // wait 5ms which is more then enough for one byte to arrive
        Sleep(5);

        // Get # of bytes in rx-queue, if error, bail out flushing
		dwBytes = 0;
		if (ClearCommError(m_hComm, &dwErrors, &ComStat))
			dwBytes = ComStat.cbInQue;

		// Purge both queues
		PurgeComm(m_hComm, PURGE_TXCLEAR | PURGE_RXCLEAR);
    }
    while(dwBytes > 0);

	// Make sure our receive buffer string is empty
	m_strRcvData.Empty();
}

///////////////////////////////////////////////////////////////////////
BOOL CGsm::Send(LPCTSTR lpszData)
{
	return(Send(lpszData, TRUE) );
}

BOOL CGsm::Send(LPCTSTR lpszData, BOOL bPurge)
{	
	DWORD dwBytesSent,dwNumberOfBytesToSend;
	char szData[255];
	long lStrLen = wcslen(lpszData);
	BOOL bResult = TRUE;


	memset(szData,0,sizeof(szData));

	if(lStrLen==0)
	{
		return(FALSE);
	}

	dwNumberOfBytesToSend = wcstombs(szData,lpszData,sizeof(szData));
	if(0 >= dwNumberOfBytesToSend)
	{ 
		return(FALSE);
	}

	// If requested, purge tx and rx buffers
	if (bPurge)
		ComFlush();

	bResult = WriteFile(m_hComm,szData,dwNumberOfBytesToSend,&dwBytesSent,NULL);

	return(bResult);
}

/////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Wait for modem to register to network (either home or roam)
// PARAMETERS:		dwWaitTime - timeout in milliseconds
//								 default to 40 seconds if specified as 0
// RETURN:		TRUE for success FALSE for error
/////////////////////////////////////////////////////////////////////////////
BOOL CGsm::WaitForRegister(DWORD dwWaitTime)
{
	int nRegStatus = REGSTAT_UNKNOWN;
	BOOL fRegistered = FALSE;


	if(INVALID_HANDLE_VALUE == m_hComm) 
		return (FALSE);

	
	// Use default timeout if called with zero...
	if(dwWaitTime == 0)
		dwWaitTime = 40000;

	DWORD dwStartTime = GetTickCount();
	do
	{
		if (GetRegStatus(&m_nRegStatus) != GSM_NO_ERROR)
			break;
			
		// Check if we are registered
		if ( (m_nRegStatus == REGSTAT_HOME) || (m_nRegStatus == REGSTAT_ROAM) )
		{
			fRegistered = TRUE;
			break;
		}

	} while( (GetTickCount() - dwStartTime) <= (dwWaitTime) );


	return(fRegistered);
}

/////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Get registration status
// PARAMETERS:	pnStatus - ptr to registration status
/////////////////////////////////////////////////////////////////////////////
int CGsm::GetRegStatus(int *pnStatus)
{
	int nResult = GSM_NO_ERROR;
	CString strStatus;

	
	if(INVALID_HANDLE_VALUE == m_hComm)
		return (COM_PORT_NOT_OPENED);


	// Get registration status (500ms timeout)
	Send(_T("AT+CREG?\r"));
    if (!WaitForReply( TEXT("OK"), 500) )
		return(GSM_TIMEOUT);


	// Parse the status from response
	// CREG: <mode> "," <status> <CRLF>
	TrimReply();
	ExtractField( TEXT(",") );		// get rid of unwanted data
	strStatus = ExtractField(NULL);	// get whatever left in buffer
	*pnStatus = _wtoi(strStatus);

	return(nResult);
}

// Return RSSI in dBm or, 99 for unknown or undetectable
int CGsm::GetRssi(int *pnRssi)
{
	int nRssi = 99;	// default to unknown

	// Get RSSI dBm (500ms timeout)
	Send( TEXT("AT+CSQ\r") );
	if (!WaitForReply( TEXT("OK"), 500) )
		return(GSM_TIMEOUT);

	TrimReply();
	ExtractField( TEXT(":") );
	nRssi = _wtoi(ExtractField( TEXT(",") ) );
    if (nRssi == 99)
		*pnRssi = 99;
	else
        *pnRssi = -113 + nRssi * 2;

	return(GSM_NO_ERROR);
}

CString CGsm::GetSca()
{
	CString strSca;

	strSca.Empty();

	Send( TEXT("AT+CSCA?\r"), TRUE);
	if (WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
	{
		// Extract the SCA
		TrimReply();
		ExtractField( TEXT("\"") );
		strSca = ExtractField( TEXT("\"") );
	}
	

	return(strSca);
}

BOOL CGsm::SetSca(CString strSca)
{
	CString strCmd;

	strCmd.Format( TEXT("AT+CSCA=\"%s\"\r"), strSca );
	Send(strCmd, TRUE);

	if ( !WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(FALSE);

	// Update system SCA
	m_strSca = strSca;

	return(TRUE);
}

DWORD CGsm::GetSimSts()
{
	Send( TEXT("AT+CPIN?\r"), TRUE);
	if (!WaitForReply( TEXT("CPIN:"), MODEM_COMMAND_TIMEOUT) )
		return(SIMSTAT_NOCARD);

	if ( m_strRcvData.Find( TEXT("READY") ) )
		return(SIMSTAT_READY);
	else if ( m_strRcvData.Find( TEXT("SIM PIN") ) )
		return(SIMSTAT_ENTERPIN);
	
	return(SIMSTAT_NOCARD);
}

BOOL CGsm::SetPin(CString strPin)
{
	CString strCmd;

	strCmd.Format( TEXT("AT+CPIN=%s"), strPin);

	Send(strCmd);
	if ( !WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(FALSE);

	// Update system pin
	m_strPin = strPin;

	return(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// Dial number to place a voice call
/////////////////////////////////////////////////////////////////////////////
int CGsm::DialNumber(CString strNumber)
{
	TCHAR szNumber[50];
	DWORD dwTimeout = MODEM_COMMAND_TIMEOUT;

	wsprintf(szNumber, TEXT("ATD%s;\r"), strNumber);	// ";" for voice call

	// Issue dial command
	Send(szNumber);

	// If we dial an emergency call, and we are not registered, this may take a while
	if (!m_bRegistered)
		dwTimeout = NETWORK_REG_TIMEOUT;

    if (!WaitForReply( TEXT("OK"), dwTimeout) )
		return(GSM_TIMEOUT);

	m_bConnected = TRUE;

	return (GSM_NO_ERROR);
}

// Hangup active call
int CGsm::Hangup()
{
	// First try to disconnect using AT command
	Send( _T("ATH\r") );
	if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
	{
		// If failed, pulse DTR for 500ms
		EscapeCommFunction(m_hComm, CLRDTR);
		Sleep(1000);
		EscapeCommFunction(m_hComm, SETDTR);
	}

	// Indicate disconnected
	m_bConnected = FALSE;

	return (GSM_NO_ERROR);
}

// Register from network
// RETURN:		TRUE for success FALSE for error
BOOL CGsm::Register()
{
	// Register to network
	Send( _T("AT+COPS=0\r") );
    if (!WaitForReply( TEXT("OK"), NETWORK_REG_TIMEOUT) )
		return(FALSE);

	// Wait for modem to finish registration
	return (WaitForRegister(MODEM_COMMAND_TIMEOUT) );
}

// Deregister from network
// RETURN:		TRUE for success FALSE for error
BOOL CGsm::Deregister()
{
	// Deregister from network
	Send( _T("AT+COPS=2\r") );
    if (!WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT) )
		return(FALSE);

	return (TRUE);
}

BOOL CGsm::MuteMic(BOOL fMute)
{
	BOOL fResult = FALSE;

	// Mute microphone
	if (fMute)
		Send( _T("AT+CMUT=1\r") );	// Mute ON
	else
		Send( _T("AT+CMUT=0\r") );	// Mute OFF

    fResult = WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT);

	return (fResult);
}


BOOL CGsm::SetVolume(int nVolume)
{
	TCHAR szCmd[100];

	nVolume = max(min(nVolume, 255), 0);
	wsprintf(szCmd, _T("AT+VGR=%d\r"), nVolume);

	Send(szCmd);

	return (WaitForReply( TEXT("OK"), MODEM_COMMAND_TIMEOUT));
}
