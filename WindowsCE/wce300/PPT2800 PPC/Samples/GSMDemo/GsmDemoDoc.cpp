// GsmDemoDoc.cpp : implementation of the CGsmDemoDoc class
//

#include "stdafx.h"
#include "GsmDemo.h"

#include "GsmDemoDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoDoc

IMPLEMENT_DYNCREATE(CGsmDemoDoc, CDocument)

BEGIN_MESSAGE_MAP(CGsmDemoDoc, CDocument)
	//{{AFX_MSG_MAP(CGsmDemoDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoDoc construction/destruction

CGsmDemoDoc::CGsmDemoDoc()
{
	// TODO: add one-time construction code here

}

CGsmDemoDoc::~CGsmDemoDoc()
{
}

BOOL CGsmDemoDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGsmDemoDoc serialization

void CGsmDemoDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoDoc diagnostics

#ifdef _DEBUG
void CGsmDemoDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGsmDemoDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoDoc commands
