#if !defined(AFX_SENDSMSDLG_H__D8CB8F1C_A04D_4B49_B3DA_B932B96B33EC__INCLUDED_)
#define AFX_SENDSMSDLG_H__D8CB8F1C_A04D_4B49_B3DA_B932B96B33EC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SendSmsDlg.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// CSendSmsDlg dialog

class CSendSmsDlg : public CDialog
{
// Construction
public:
	CSendSmsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendSmsDlg)
	enum { IDD = IDD_SENDSMS };
	CEdit	m_edtNumber;
	CEdit	m_edtMsg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendSmsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendSmsDlg)
	afx_msg void OnBtnSend();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDSMSDLG_H__D8CB8F1C_A04D_4B49_B3DA_B932B96B33EC__INCLUDED_)
