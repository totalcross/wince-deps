//--------------------------------------------------------------------
// FILENAME:            Gsm.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// %IF Symbol_Internal
// AUTHOR:          Eyal Peretz

// CREATION DATE:       
//
// DERIVED FROM:    
//
// EDIT HISTORY:
//
// %End
//--------------------------------------------------------------------
#if !defined(AFX_GSM_H__1126B30E_BAF8_4BAC_9295_072BAE21FD29__INCLUDED_)
#define AFX_GSM_H__1126B30E_BAF8_4BAC_9295_072BAE21FD29__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// Return error codes
#define GSM_NO_ERROR				 0
#define GSM_TIMEOUT					 1
#define COM_PORT_NOT_OPENED			 2
#define GSM_SELECT_TEXT_ERROR		 3
#define GSM_SMS_NOT_SUPPORTED		 4
#define GSM_STORAGE_ERROR			 5
#define GSM_SENDMSG_ERROR			 6
#define GSM_READMSG_ERROR			 7
#define GSM_LISTMSG_ERROR			 8
#define GSM_AT_NOT_WORKING           9
#define GSM_MODEM_INIT_ERROR		10
#define GSM_DELETEMSG_ERROR			11


// Timeouts
#define NETWORK_SENDMSG_TIMEOUT		10000
#define NETWORK_READMSG_TIMEOUT		 6000
#define NETWORK_DELETEMSG_TIMEOUT	10000
#define NETWORK_REG_TIMEOUT			40000
#define MODEM_COMMAND_TIMEOUT		 6000


// Registration status (from +CREG)
#define REGSTAT_NOT_REG		0
#define REGSTAT_HOME		1
#define REGSTAT_SEARCHING	2
#define REGSTAT_DENIED		3
#define REGSTAT_UNKNOWN		4
#define REGSTAT_ROAM		5

// SIM status
#define SIMSTAT_NOCARD		0
#define SIMSTAT_READY		1
#define SIMSTAT_ENTERPIN	2


#define CTRL_Z	TEXT("\x1A")

// Defaults
#define DEFAULT_DIAL_NUMBER		TEXT("121212")
#define DEFAULT_SMS_NUMBER		TEXT("123456")
#define DEFAULT_SMS_MESSAGE		TEXT("Hello from Symbol PPT2800")
#define EMERGENCY_NUMBER		TEXT("112")


// Short Message structure
typedef struct tagSMSMSG
{
	BOOL	fUnRead;			// status unread
	CString strFrom;			// originator address
	CString strDate;			// service center date,
	CString strTime;			// and time
	CString strMsg;				// the message itself
} SMSMSG;


class CGsm  
{

public:
	
	HANDLE m_hComm;
	BOOL m_bRadioPresent;
	BOOL m_bRegistered;
	int	 m_nRegStatus;
	BOOL m_bCommActive;
	BOOL m_bRadioStatus;
	BOOL m_bConnected;
	UINT m_nTimer;


	CGsm();
	virtual ~CGsm();

	int  Open(LPCTSTR szCommPort, DWORD dwBaud);
	BOOL Close();

	BOOL Send(LPCTSTR lpszData);
	BOOL Send(LPCTSTR lpszData, BOOL bPurge);
	BOOL WaitForReply(LPCTSTR pszReply, DWORD dwTimeOut);

	int  InitModem();
	int  GetModemInfo();

	BOOL Register();
	BOOL Deregister();
	BOOL WaitForRegister(DWORD dwWaitTime);

	int  GetRegStatus(int *pnStatus);
	int  GetRssi(int *pnRssi);

	int  ConfigTextSms();
	void ComFlush();
	int  SendTextSms(CString strNumber, CString strMsg);
	int  ReadTextSms(int nId);
	int  DeleteTextSms(int nId);
	int  DeleteAllTextSms(void);
	int  DialNumber(CString strNumber);
	int	 Hangup();
	BOOL MuteMic(BOOL fMute);
	BOOL SetVolume(int nVolume);

	CString GetSca();
	BOOL    SetSca(CString strSca);
	DWORD   GetSimSts();
	BOOL	SetPin(CString strPin);

	
	CString m_szCommPort;
	DWORD	m_dwBaud;
	CString m_strRcvData;
	CString m_strSca;
	CString m_strPin;

	// Modem info
	CString m_strVendor;
	CString m_strModel;
	CString m_strFwVer;
	CString m_strFwRev;
	CString m_strSerialNum;
	CString m_strFwDate;

	SMSMSG  m_smsMsg;
	int		m_nMuteBtnSts;

private:

	CString ExtractField(TCHAR *pszDelim);
	CString TrimReply(void);
};

#endif // !defined(AFX_GSM_H__1126B30E_BAF8_4BAC_9295_072BAE21FD29__INCLUDED_)
