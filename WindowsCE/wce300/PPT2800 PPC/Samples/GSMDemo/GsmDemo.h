// GsmDemo.h : main header file for the GSMDEMO application
//

#if !defined(AFX_GSMDEMO_H__DB25AA77_2955_4F8B_84D6_38AB164CA652__INCLUDED_)
#define AFX_GSMDEMO_H__DB25AA77_2955_4F8B_84D6_38AB164CA652__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGsmDemoApp:
// See GsmDemo.cpp for the implementation of this class
//

class CGsmDemoApp : public CWinApp
{
public:
	CGsmDemoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmDemoApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGsmDemoApp)
	afx_msg void OnHelpAbout();
	afx_msg void OnOptionsExit();
	afx_msg void OnOptionsSettings();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSMDEMO_H__DB25AA77_2955_4F8B_84D6_38AB164CA652__INCLUDED_)
