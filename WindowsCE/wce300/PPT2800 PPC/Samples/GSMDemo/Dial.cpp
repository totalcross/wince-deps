// Dial.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"

#include "Dial.h"
#include "Gsm.h"


/////////////////////////////////////////////////////////////////////////////
// External variables
/////////////////////////////////////////////////////////////////////////////
extern	CGsm gsmDevice;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDial dialog


CDial::CDial(CWnd* pParent /*=NULL*/)
	: CDialog(CDial::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDial)
	//}}AFX_DATA_INIT
}


void CDial::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDial)
	DDX_Control(pDX, IDC_SLIDER_VOLUME, m_sldVolume);
	DDX_Control(pDX, IDC_BTN_MUTE, m_btnMute);
	DDX_Control(pDX, IDC_EDT_STATUS, m_edtStatus);
	DDX_Control(pDX, IDC_EDT_NUMBER, m_edtNumber);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDial, CDialog)
	//{{AFX_MSG_MAP(CDial)
	ON_BN_CLICKED(IDC_BTN_DIAL, OnBtnDial)
	ON_BN_CLICKED(IDC_BTN_HANGUP, OnBtnHangup)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_MUTE, OnBtnMute)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP

	//ON_NOTIFY(HDN_ENDDRAG, IDC_SLIDER_VOLUME, OnAdjustVolume)
	ON_WM_HSCROLL()


END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDial message handlers
BOOL CDial::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_edtStatus.SetWindowText( TEXT("Disconnected") );

	if (!gsmDevice.m_bRegistered)
	{
		m_edtNumber.SetWindowText(EMERGENCY_NUMBER);
		m_edtNumber.EnableWindow(FALSE);
	}
	else
	{
		m_edtNumber.SetWindowText(DEFAULT_DIAL_NUMBER);
		m_edtNumber.EnableWindow(TRUE);
	}

	// Get initial mute button state
	gsmDevice.m_nMuteBtnSts = m_btnMute.GetCheck();	// get initial mute state

	// Set Volume slider
	//	# ticks line and page size to 1, range to 0..8
	m_sldVolume.SetPageSize(1);
	m_sldVolume.SetLineSize(1);
	m_sldVolume.SetPos(5);		// default is 64. (-2db)
	m_sldVolume.SetRange(0, 8, TRUE);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDial::OnBtnDial() 
{
	CString strNumber;
	int nError = GSM_NO_ERROR;
	BOOL bEmergency = FALSE;

	if (gsmDevice.m_bConnected)
	{
		MessageBox( TEXT("Already connected !") );
		return;
	}

	m_edtStatus.SetWindowText( TEXT("Disconnected") );

	// Make sure a phone number was specified
	m_edtNumber.GetWindowText( strNumber );
	if ( strNumber.IsEmpty() )
	{
		MessageBox( TEXT("Phone number must be specified !") );
		return;
	}

	// If emergency number, allow call
	if ( strNumber.Compare(EMERGENCY_NUMBER) == 0)
		bEmergency = TRUE;


	// Make sure we are registered
	if( (!gsmDevice.m_bRegistered) && (!bEmergency) )
	{
		MessageBox( TEXT("Not registered with network !") );
		return;
	}


	gsmDevice.m_bCommActive = TRUE;	
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));

	if (bEmergency)
		m_edtStatus.SetWindowText( TEXT("Dialing Emergency Call...") );
	else
		m_edtStatus.SetWindowText( TEXT("Dialing...") );
	nError = gsmDevice.DialNumber(strNumber);

	gsmDevice.m_bCommActive = FALSE;
	SetCursor(hOldCursor);

	if (nError == GSM_NO_ERROR)
	{
		if (bEmergency)
			m_edtStatus.SetWindowText( TEXT("Connected Emergency Call") );
		else
			m_edtStatus.SetWindowText( TEXT("Connected") );
	}
	else
		m_edtStatus.SetWindowText( TEXT("Disconnected") );
}

void CDial::OnBtnHangup() 
{
	if (!gsmDevice.m_bConnected)
	{
		MessageBox( TEXT("You are not currently connected !") );
		return;
	}

	// Disconnect phone call
	m_edtStatus.SetWindowText( TEXT("Hanging up...") );
	gsmDevice.m_bCommActive = TRUE;	
	gsmDevice.Hangup();
	gsmDevice.m_bCommActive = FALSE;
	m_edtStatus.SetWindowText( TEXT("Disconnected") );


	// Release mute since call is not active any more.
	gsmDevice.m_nMuteBtnSts = BST_UNCHECKED;
	m_btnMute.SetCheck(gsmDevice.m_nMuteBtnSts);
}

void CDial::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// If we are connected, make sure we hang up before exiting.
	if (gsmDevice.m_bConnected)
	{
		gsmDevice.m_bCommActive = TRUE;	
		gsmDevice.Hangup();
		gsmDevice.m_bCommActive = FALSE;	
	}
}

void CDial::OnBtnMute() 
{
	int nNewMuteSts = m_btnMute.GetCheck();	// save button state
	BOOL fMuteOn = (nNewMuteSts == BST_CHECKED ? TRUE : FALSE);

	// Use mute only when a call is active
	if (!gsmDevice.m_bConnected)
	{
		MessageBox( TEXT("You are not currently connected !") );
		m_btnMute.SetCheck(gsmDevice.m_nMuteBtnSts);	// restore mute button status
		return;
	}

	gsmDevice.m_bCommActive = TRUE;	
	if (!gsmDevice.MuteMic(fMuteOn) )
		m_btnMute.SetCheck(gsmDevice.m_nMuteBtnSts);	// restore mute button status
	else
		gsmDevice.m_nMuteBtnSts = nNewMuteSts;		// update current mute status
	gsmDevice.m_bCommActive = FALSE;
}


void CDial::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// Get volume slider position
	int nCurpos = m_sldVolume.GetPos();


	// 16 levels of 16 steps each, one slider step = 2 levels
	int nVol = 255 - (15 + nCurpos * 15 * 2);

	TCHAR szTemp[200];


	// Command modem to adjust volume
	CString strStatus;
	m_edtStatus.GetWindowText(strStatus);	// save status momentary
//	wsprintf(szTemp, _T("Adjust vol. to %d..."), nVol);
	wsprintf( szTemp, _T("Adjusting volume...") );
	m_edtStatus.SetWindowText(szTemp);
		
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));
	gsmDevice.m_bCommActive = TRUE;	

	gsmDevice.SetVolume(nVol);

	gsmDevice.m_bCommActive = FALSE;
	SetCursor(hOldCursor);
	m_edtStatus.SetWindowText(strStatus);	// restore status


	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}
