//--------------------------------------------------------------------
// FILENAME:            GSMDEFS.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifdef _WIN32
	#include <windows.h>
	#define far
#else
	typedef unsigned char BYTE;
#endif

#define TRUE 1
#define FALSE 0

// **** SMS MESSAGE TYPES AND INFO ****

// Commands/ Responses issued by TE
#define LIST_REQUEST				0x00
#define GET_MESSAGE					0x01
#define GET_FIRST_MESSAGE			0x02
#define GET_NEXT_MESSAGE			0x03
#define TRANSFER_INC_SMS			0x04
#define INDICATE_INC_SMS			0x05
#define TRANSFER_INC_CBS			0x06
#define INSERT_SMS					0x07
#define DELETE_MESSAGE				0x08
#define TE_UNABLE_TO_PROCESS		0x09
#define TE_END_SMS_MODE				0x1E
#define ACKNOWLEDGE_MESSAGE			0x1F

// Responses/Indications issued by MT
#define MESSAGE_LIST				0x20
#define MESSAGE						0x21
#define GET_MESSAGE_FAILURE			0x22
#define INC_MESSAGE					0x23
#define MESSAGE_ARRIVED				0x24
#define INSERT_SMS_COMPLETE			0x25
#define INSERT_SMS_FAILURE			0x26
#define DELETE_MESSAGE_COMPLETE		0x27
#define DELETE_MESSAGE_FAILURE		0x28
#define MT_UNABLE_TO_PROCESS		0x29
#define REQUEST_CONFIRMED			0x2A
#define MT_END_SMS_MODE				0x3F

// Short message status (GSM 07.05.2.5.2.5)
#define SM_STATUS_NOT_READ_SENT		0x00
#define SM_STATUS_READ_SENT			0x01

// Insert type (GSM 07.05 2.5.2.4)
#define IT_STORE					0x01
#define IT_SEND						0x02
#define IT_STORE_AND_SEND			0x03

#define TON_UNKNOWN					0x00
#define TON_INTERNATIONAL			0x01
#define TON_NATIONAL				0x02
#define TON_NETWORK					0x03
#define TON_SUBSCRIBER				0x04
#define TON_ALNUM					0x05
#define TON_ABREVIATED				0x06
#define TON_RESERVED_FOR_EXT		0x07
#define TON     					0x90

#define NPI_UNKNOWN					0x00
#define NPI_ISDN_OR_PHONE			0x08
#define NPI_DATA					0x18
#define NPI_TELEX					0x20
#define NPI_NATIONAL				0x40
#define NPI_PRIVATE					0x48
#define NPI_ERMES					0x50
#define NPI_RESERVED_FOR_EXT		0x78
