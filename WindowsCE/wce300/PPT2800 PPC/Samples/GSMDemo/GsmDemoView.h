// GsmDemoView.h : interface of the CGsmDemoView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GSMDEMOVIEW_H__0249D028_975D_4D4F_92B5_6D35C4822B8F__INCLUDED_)
#define AFX_GSMDEMOVIEW_H__0249D028_975D_4D4F_92B5_6D35C4822B8F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


// RSSI Signal Progress Bar range (in dBm)
#define PGS_RSSI_MIN -113
#define PGS_RSSI_MAX  -51



class CGsmDemoView : public CFormView
{
protected: // create from serialization only
	CGsmDemoView();
	DECLARE_DYNCREATE(CGsmDemoView)

public:
	//{{AFX_DATA(CGsmDemoView)
	enum { IDD = IDD_GSMDEMO_FORM };
	CEdit	m_edtRegStatus;
	CProgressCtrl	m_pgsSignal;
	//}}AFX_DATA

// Attributes
public:
	CGsmDemoDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmDemoView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	// User defined message handlers...
	afx_msg void OnUserInitApp(WPARAM wParam, LPARAM lParam);


// Implementation
public:
	virtual ~CGsmDemoView();


	void RequestRegSts();
	void RequestRssi();


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGsmDemoView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnReg();
	afx_msg void OnBtnDereg();
	afx_msg void OnBtnSendSms();
	afx_msg void OnBtnReadSms();
	afx_msg void OnBtnDial();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GsmDemoView.cpp
inline CGsmDemoDoc* CGsmDemoView::GetDocument()
   { return (CGsmDemoDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSMDEMOVIEW_H__0249D028_975D_4D4F_92B5_6D35C4822B8F__INCLUDED_)
