// GsmDemoDoc.h : interface of the CGsmDemoDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GSMDEMODOC_H__515BB30D_E5CB_4061_A344_822D7FE6ED7D__INCLUDED_)
#define AFX_GSMDEMODOC_H__515BB30D_E5CB_4061_A344_822D7FE6ED7D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CGsmDemoDoc : public CDocument
{
protected: // create from serialization only
	CGsmDemoDoc();
	DECLARE_DYNCREATE(CGsmDemoDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmDemoDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGsmDemoDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGsmDemoDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSMDEMODOC_H__515BB30D_E5CB_4061_A344_822D7FE6ED7D__INCLUDED_)
