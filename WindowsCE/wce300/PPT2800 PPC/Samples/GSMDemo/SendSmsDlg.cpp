// SendSmsDlg.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"
#include "SendSmsDlg.h"

#include "Gsm.h"

/////////////////////////////////////////////////////////////////////////////
// External variables
/////////////////////////////////////////////////////////////////////////////
extern	CGsm gsmDevice;




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendSmsDlg dialog


CSendSmsDlg::CSendSmsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSendSmsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendSmsDlg)
	//}}AFX_DATA_INIT
}


void CSendSmsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendSmsDlg)
	DDX_Control(pDX, IDC_EDT_NUMBER, m_edtNumber);
	DDX_Control(pDX, IDC_EDT_MESSAGE, m_edtMsg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendSmsDlg, CDialog)
	//{{AFX_MSG_MAP(CSendSmsDlg)
	ON_BN_CLICKED(IDC_BTN_SEND, OnBtnSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendSmsDlg message handlers
BOOL CSendSmsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_edtNumber.SetWindowText(DEFAULT_SMS_NUMBER);
	m_edtMsg.SetWindowText(DEFAULT_SMS_MESSAGE);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSendSmsDlg::OnBtnSend() 
{
	CString strNumber;
	CString strMsg;
	int nError = GSM_NO_ERROR;

	// Make sure we are registered
	if(!gsmDevice.m_bRegistered)
	{
		MessageBox( TEXT("Not registered with network !") );
		return;
	}

	// Make sure a phone number was specified
	m_edtNumber.GetWindowText( strNumber );
	if ( strNumber.IsEmpty() )
	{
		MessageBox( TEXT("Phone number must be specified !") );
		return;
	}

	gsmDevice.m_bCommActive = TRUE;	
	HCURSOR hOldCursor = SetCursor(AfxGetApp()->LoadCursor(IDC_WAIT));
	m_edtMsg.GetWindowText( strMsg );

	nError = gsmDevice.SendTextSms(strNumber, strMsg);

	gsmDevice.m_bCommActive = FALSE;
	SetCursor(hOldCursor);

	if (nError == GSM_NO_ERROR)
		MessageBox( TEXT("Transmission successfull") );
	else
		MessageBox( TEXT("Transmission failed !") );
}

