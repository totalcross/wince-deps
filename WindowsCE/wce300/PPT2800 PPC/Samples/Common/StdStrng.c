
//--------------------------------------------------------------------
// FILENAME:			StdStrng.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains standard string handling functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>

#include "..\common\StdStrng.h"


//----------------------------------------------------------------------------
// Replace
//----------------------------------------------------------------------------

void Replace(LPTSTR lpszBase,
			 LPTSTR lpszPat,
			 LPTSTR lpszRep)
{
	TCHAR szHold[MAX_PATH];

	// Find the first occurence of pattern in base string
	LPTSTR lpszFnd = TEXTSTR(lpszBase,lpszPat);

	// If pattern was found
	if ( lpszFnd != NULL )
	{
		// Copy string following pattern into holding area
		TEXTCPY(szHold,lpszFnd+TEXTLEN(lpszPat));

		// Copy replacement string over pattern string
		TEXTCPY(lpszFnd,lpszRep);

		// Append rest of string from hold area
		TEXTCAT(lpszBase,szHold);
	}
}


#ifndef _WIN32_WCE

//----------------------------------------------------------------------------
// ReplaceEnv
//----------------------------------------------------------------------------

void ReplaceEnv(LPTSTR lpszStr)
{
	LPTSTR lpszStart;
	DWORD dwSize;

	// While there is a % in the string
	while ( ( lpszStart = TEXTCHR(lpszStr,TEXT('%')) ) != NULL )
	{
		LPTSTR lpszEnd;
		TCHAR szVar[MAX_PATH];
		LPTSTR lpszEnv;
		
		// If there is no closing %
		if ( ( lpszEnd = TEXTCHR(lpszStart+1,TEXT('%')) ) == NULL )
		{
			// Then we are done
			return;
		}

		// Compute size of environment reference (what's between the two % markers)
		dwSize = lpszEnd-lpszStart+2;

		// If its bigger than our buffer
		if ( dwSize > TEXTSIZEOF(szVar) )
		{
			// Then we are done
			return;
		}

		// Copy the part between the % markers
		TEXTNCPY(szVar,lpszStart+1,dwSize-2);
		szVar[dwSize-3] = TEXT('\0');

#ifdef UNICODE

		{
			LPSTR lpszStr = (LPSTR)szVar;
			
			// Convert environment variable name to ASCII
			ConvertUnicodeToAscii(lpszStr,szVar);
		
			// Look it up in the environment
			lpszStr = getenv(lpszStr);

			// If it was found in the environment
			if ( lpszStr != NULL )
			{
				// Convert value from environment into UNICODE
				ConvertAsciiToUnicode(szVar,lpszStr);

				// Replace original value pointer with UNICODE copy
				lpszEnv = szVar;
			}
		}

#else

		// Look it up in the environment
		lpszEnv = getenv(szVar);

#endif

		// If it was found in the environment
		if ( lpszEnv != NULL )
		{
			// Copy the part with the % markers
			TEXTNCPY(szVar,lpszStart,dwSize);
			szVar[dwSize-1] = 0;

			// Replace it with the environment value
			Replace(lpszStr,szVar,lpszEnv);
		}
		else
		{
			// Otherwise skip past closing % marker to avoid infinite loop
			lpszStr = lpszEnd + 1;
		}
	}
}

#endif


//----------------------------------------------------------------------------
// GetExtension
//----------------------------------------------------------------------------

BOOL GetExtension(LPTSTR lpszExtension,
				  int nSize,
				  LPTSTR lpszFileSpec)
{
	// Get length of file path spec
	DWORD dwLen = TEXTLEN(lpszFileSpec);

	// Initialize extension to empty
	lpszExtension[0] = TEXT('\0');

	// Start at end of file path spec, while more data
	while ( dwLen )
	{
		// If we found the start of the extension
		if ( lpszFileSpec[--dwLen] == TEXT('.') )
		{
			// Copy extension into target buffer
			TEXTCPY(lpszExtension,lpszFileSpec+dwLen);

			// Stop looping
			break;
		}
	}

	// Return result based on presence of data in target buffer
	return((lpszExtension[0] != TEXT('\0')));
}


//----------------------------------------------------------------------------
// RemoveLeadingSpaces
//----------------------------------------------------------------------------

LPTSTR RemoveLeadingSpaces(LPTSTR lpszStr)
{
	// If the source string is null
	if ( lpszStr == NULL )
	{
		// Return failure
		return(NULL);
	}
	
	// While the source string starts with a space
	while ( *lpszStr == TEXT(' ') )
	{
		// Advance past the space
		lpszStr++;
	}

	// Return the pointer to the first non-space character
	return(lpszStr);
}



//----------------------------------------------------------------------------
// GetFileName
//----------------------------------------------------------------------------

BOOL GetFileName(LPTSTR lpszPath,
				 LPTSTR lpszName)
{
	LPTSTR lpszChr;
	LPTSTR lpszNamePtr = lpszName;
	
	// If supplied path string is null
	if ( lpszPath == NULL )
	{
		// Return no file name found 
		return(FALSE);
	}
	
	// Remove any leading blanks from path
	lpszPath = RemoveLeadingSpaces(lpszPath);

	// Loop
	do
	{
		// Find location of first backslash in path
		lpszChr = TEXTCHR(lpszPath,TEXT('\\'));

		// If there was one
		if ( lpszChr != NULL )
		{
			// Move to first char past it
			lpszPath = lpszChr+1;
		}
	  // Continue looping until we can no longer find a backslash in path
	} while ( lpszChr != NULL );

	// If the path is now empty
	if ( *lpszPath == TEXT('\0') )
	{
		// Return no file name found 
		return(FALSE);
	}

	// Loop through file name part of file path until extension is found
	while ( *lpszPath &&
			( ( *lpszPath != TEXT('.') ) ||
			  ( TEXTCHR(lpszPath+1,TEXT('.')) != NULL ) ) )
	{
		// Copy chars of file name
		*lpszNamePtr++ = *lpszPath++;
	}

	// Null terminate the file name
	*lpszNamePtr = TEXT('\0');

	// Return result based on presence of data in target buffer
	return((lpszName[0] != TEXT('\0')));
}



//----------------------------------------------------------------------------
// ProcessSlashes
//----------------------------------------------------------------------------

void ProcessSlashes(LPTSTR lpszStr)
{
	// While there are double backslash pairs in the string
	while ( TEXTSTR(lpszStr,TEXT("\\\\")) != NULL )
	{
		// Replace the pair with a single backslash
		Replace(lpszStr,TEXT("\\\\"),TEXT("\\"));
	}

	// While then are escaped quotes \" in the string
	while ( TEXTSTR(lpszStr,TEXT("\\\"")) != NULL )
	{
		// Remove backslash from in front of the quote
		Replace(lpszStr,TEXT("\\\""),TEXT("\""));
	}
}


//----------------------------------------------------------------------------
// ConvertAsciiToUnicode
//----------------------------------------------------------------------------

void ConvertAsciiToUnicode(LPTSTR lpszDest,LPSTR lpszSrc)
{
	int i;
	DWORD dwLen = strlen(lpszSrc);

	// Loop through string backwards
	for(i=dwLen;i>=0;i--)
	{
		// Copy and convert char
		lpszDest[i] = (TCHAR)lpszSrc[i];
	}
}



//----------------------------------------------------------------------------
// ConvertUnicodeToAscii
//----------------------------------------------------------------------------

void ConvertUnicodeToAscii(LPSTR lpszDest,LPTSTR lpszSrc)
{
	DWORD i;
	DWORD dwLen = TEXTLEN(lpszSrc);

	// Loop through string forwards
	for(i=0;i<=dwLen;i++)
	{
		// Copy and convert char
		lpszDest[i] = (CHAR)lpszSrc[i];
	}
}



//----------------------------------------------------------------------------
// GetPathName
//----------------------------------------------------------------------------

BOOL GetPathName(LPTSTR lpszPathAndFile,
				 LPTSTR lpszDest)
{
	LPTSTR lpszChr;	
	LPTSTR lpszPath;
	
	// If supplied path string is null
	if ( lpszPathAndFile == NULL )
	{
		// Return no file name found 
		return(FALSE);
	}
	
	// Remove any leading blanks from path
	lpszPathAndFile = RemoveLeadingSpaces(lpszPathAndFile);
	lpszPath = lpszPathAndFile;

	// Loop
	do
	{
		// Find location of first backslash in path
		lpszChr = TEXTCHR(lpszPath,TEXT('\\'));

		// If there was one
		if ( lpszChr != NULL )
		{
			// Move to first char past it
			lpszPath = lpszChr+1;
		}
	  // Continue looping until we can no longer find a backslash in path
	} while ( lpszChr != NULL );

	// If there is no path
	if ( lpszPath == lpszPathAndFile )
	{
		*lpszDest = TEXT('\0');
		
		// Return no path name found
		return(FALSE);
	}

	// Copy path to destination
	TEXTCPY(lpszDest,lpszPathAndFile);

	// Null terminate the path
	lpszDest[lpszPath-lpszPathAndFile] = TEXT('\0');

	// Return path found
	return(TRUE);
}


