
//--------------------------------------------------------------------
// FILENAME:			StdAbout.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Functions for standard About box handling
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"

#include "..\common\StdAbout.h"



DIALOG_CHOICE AboutDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_ABOUT_PC_STD,	NULL },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_ABOUT_HPC_STD,	NULL },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_ABOUT_PPC_STD,	NULL },
	{ ST_PPC_SIP,	IDD_ABOUT_PPC_SIP,	NULL },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_ABOUT_7200_STD,	NULL },
	{ ST_7200_TSK,	IDD_ABOUT_7200_TSK,	NULL },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_ABOUT_7500_STD,	NULL },
	{ ST_7500_TSK,	IDD_ABOUT_7500_TSK,	NULL },
#endif
	{ ST_UNKNOWN,	IDD_ABOUT_7500_TSK,	NULL }
};


int nAboutCtl1Id = IDC_ABOUT1;
int nAboutCtl2Id = IDC_ABOUT2;
int nAboutButtonId = IDC_APPICON;


int nAboutStr2Id;
int nAboutStr1Id;
int nAboutIconId;


//----------------------------------------------------------------------------
// AboutDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK AboutDlgProc(HWND hwnd,
							  UINT uMsg,
							  WPARAM wParam,
							  LPARAM lParam)
{
	TCHAR szText[MAX_PATH];
	HICON hIcon;
    HDC hdc;
	LPDRAWITEMSTRUCT lpdis;
	DWORD dwUIStyle = GetUIStyle();

	switch(uMsg)
	{
		case UM_RESIZE:

			if ( dwUIStyle != UI_STYLE_PC )
			{
				ResizeDialog(NULL);
			}

			break;

		case WM_INITDIALOG:

			if ( dwUIStyle != UI_STYLE_PC )
			{
				MaxDialog(hwnd);
			}

			if ( nAboutStr1Id < 0xffff )
			{
				LoadString(g_hInstance,
						   nAboutStr1Id,
						   szText,
						   TEXTSIZEOF(szText));
			}
			else
			{
				TEXTCPY(szText,(LPTSTR)nAboutStr1Id);
			}
			Edit_SetText(GetDlgItem(hwnd,nAboutCtl1Id),szText);
			
			if ( nAboutStr2Id < 0xffff )
			{
				LoadString(g_hInstance,
						   nAboutStr2Id,
						   szText,
						   TEXTSIZEOF(szText));
			}
			else
			{
				TEXTCPY(szText,(LPTSTR)nAboutStr2Id);
			}
			Edit_SetText(GetDlgItem(hwnd,nAboutCtl2Id),szText);

			SetFocus(GetDlgItem(hwnd,nAboutCtl1Id));
            
			// Return FALSE since focus was set manually
			return(FALSE);

        case WM_DRAWITEM:
			
			if ( nAboutIconId )
			{
				RECT rect;

				lpdis = (LPDRAWITEMSTRUCT) lParam; 
				hdc = lpdis->hDC;
				rect = lpdis->rcItem;
				
				hIcon = LoadImage(g_hInstance,
								  MAKEINTRESOURCE(nAboutIconId),
								  IMAGE_ICON,
								  32,
								  32,
								  0);
				
				DrawIconEx(hdc,0,0,hIcon,0,0,0,NULL,DI_NORMAL);
			}

			return(TRUE);

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDOK:
				case IDCANCEL:

					// Exit the dialog
					ExitDialog();
                    
					break;
			}
			return(TRUE);
	}

	return(FALSE);
}


//----------------------------------------------------------------------------
// About
//----------------------------------------------------------------------------

void About(HWND hwnd,
		   int p_nAboutStr2Id,
		   int p_nAboutStr1Id,
		   int p_nAboutIconId)
{
	if ( g_bInAbout )
	{
		return;
	}
	
	g_bInAbout = TRUE;
	
	nAboutStr2Id = p_nAboutStr2Id;
	nAboutStr1Id = p_nAboutStr1Id;
	nAboutIconId = p_nAboutIconId;
	
	g_hdlg = CreateChosenDialog(g_hInstance,
								AboutDialogChoices,
								g_hwnd,
								AboutDlgProc,
								0);
		
	WaitUntilDialogDoneSkip(TRUE);

	SetFocus(g_hwnd);

	g_bInAbout = FALSE;
}


