//--------------------------------------------------------------------
// FILENAME:			ScanSel.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for ScanSel.C
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANSEL_H_

#define SCANSEL_H_

#ifdef __cplusplus
extern "C"
{
#endif


BOOL ScanSel(void);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANSEL_H_   */

