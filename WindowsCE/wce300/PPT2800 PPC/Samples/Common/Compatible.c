
//--------------------------------------------------------------------
// FILENAME:            Compatible.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------


#define SI_GETLEN(ptr) ((ptr->dwUsed <= sizeof(STRUCT_INFO)) \
						? 0 \
						: (ptr->dwUsed - sizeof(STRUCT_INFO)))


BOOL LengthsCompatible(DWORD dwMin1,
					   DWORD dwMax1,
					   DWORD dwMin2,
					   DWORD dwMax2)
{
	// If read 1 has a single length
	if ( dwMin1 == dwMax1 )
	{
		// If read 2 has a single length
		if ( dwMin2 == dwMax2 )
		{
			// If the lengths are different
			if ( dwMin1 != dwMin2 )
			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
		// If read 2 has two discete lengths
		else if ( dwMin2 > dwMax2 )
		{
			// For now, assume they are incompatible
			//   Later we can see if the three lengths
			//   make a valid range
			return(FALSE);
		}
		// If read 2 has a range of lengths
		else
		{
			// If the single length is adjacent to the range
			if ( ( dwMin1 == ( dwMin2 - 1 ) ) ||
				 ( dwMin1 == ( dwMax2 + 1 ) ) )

			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
	}
	// If read 1 has two discete lengths
	else if ( dwMin1 > dwMax1 )
	{
		// If read 2 has a single length
		if ( dwMin2 == dwMax2 )
		{
			// For now, assume they are incompatible
			//   Later we can see if the three lengths
			//   make a valid range
			return(FALSE);
		}
		// If read 2 has two discete lengths
		else if ( dwMin2 > dwMax2 )
		{
			// For now, assume they are incompatible
			//   Later we can see if the four lengths
			//   make a valid range
			return(FALSE);
		}
		// If read 2 has a range of lengths
		else
		{
			// If discrete lengths are adjacent to the range
			//	Could be one on each side of the range or
			//	Both consecutive above or below the range
			if ( ( ( dwMax1 == ( dwMin2 - 1 ) ) &&
				   ( dwMin1 == ( dwMax2 + 1 ) ) ) ||
				 ( ( dwMax1 == ( dwMin1 - 1 ) ) &&
				   ( ( dwMax1 == ( dwMin2 - 1 ) ) ||
				     ( dwMin1 == ( dwMax2 + 1 ) ) ) )
			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
	}
	// If read 1 has a range of lengths
	else
	{
		// If read 2 has a single length
		if ( dwMin2 == dwMax2 )
		{
			// If the single length is adjacent to the range
			if ( ( dwMin2 == ( dwMin1 - 1 ) ) ||
				 ( dwMin2 == ( dwMax1 + 1 ) ) )

			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
		// If read 2 has two discete lengths
		else if ( dwMin2 > dwMax2 )
		{
			// If both discrete lengths are adjacent to the range
			if ( ( ( dwMin2 == ( dwMin1 - 1 ) ) ||
				   ( dwMin2 == ( dwMax1 + 1 ) ) &&
				 ( ( dwMax2 == ( dwMin1 - 1 ) ) ||
				   ( dwMax2 == ( dwMax1 + 1 ) ) ) )

			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
		// If read 2 has a range of lengths
		else
		{
			// If the ranges are adjacent
			if ( ( dwMax1 = ( dwMin2 - 1 ) ) ||
				 ( dwMin1 = ( dwMax2 + 1 ) ) )
			{
				// Then the lengths are compatible
				return(TRUE);
			}
			else
			{
				// Else the lengths are incompatible
				return(FALSE);
			}
		}
	}
}



int WhereIn(LPDECODER_LIST lpDecoders,
			BYTE byDecoder)
{
	int i;

	// Loop through all decoders in list
	for(i=0;i<SI_GETLEN(lpDecoders);i++)
	{
		// If it matches
		if ( lpDecoders[i] == byDecoder )
		{
			// Return where found
			return(i);
		}
	}

	// Indicate not found
	return(-1);
}


BOOL AnyUPCorEAN(LPDECODER_LIST lpDecoders)
{
	if ( WhereIn(lpDecoders,TYPE_UPCE0) >= 0 )
	{
		return(TRUE);
	}
	if ( WhereIn(lpDecoders,TYPE_UPCE1) >= 0 )
	{
		return(TRUE);
	}
	if ( WhereIn(lpDecoders,TYPE_UPCA) >= 0 )
	{
		return(TRUE);
	}
	if ( WhereIn(lpDecoders,TYPE_EAN8) >= 0 )
	{
		return(TRUE);
	}
	if ( WhereIn(lpDecoders,TYPE_EAN13) >= 0 )
	{
		return(TRUE);
	}
	return(FALSE);
}


BOOL Equal(LPVOID lpData1,
		   LPVOID lpData2,
		   DWORD dwSize)
{
	if ( memcmp(lpData1,lpData2,dwSize) == 0 )
	{
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}


BOOL CompatibleReads(LPREAD lpRead1,
					 LPREAD lpRead2)
{
	int d1;

	// Loop through all decoders in read 1 list
	for(d1=0;d1<lpRead1->SI_GETLEN(decoder_list);d1++)
	{
		int d2;
		
		// Get position of the decoder in read 2 list
		d2 = WhereIn(lpRead2->decoder_list,
					 lpRead2->decoder_list[i]);

		// If the decoder is in both reads
		if ( d2 >= 0 )
		{
			// If the decoder-specific params are the same
			//   (note: does not include min and max lengths)
			if ( Equal(&lpRead1->decoder_params[d1],
					   sizeof(lpRead1->decoder_params[d1]),
					   &lpRead2->decoder_params[d2]) )
			{
				// If the lengths are not compatible
				if ( !CompatibleLengths(lpRead1,d1,lpRead2,d2) )
				{
				// Then the reads are not compatible
					return(FALSE);
				}
			}
			else
			{
				// Then the reads are not compatible
				return(FALSE);
			}
		}
	}

	// If both reads include any UPC or EAN
	if ( AnyUPCorEAN(lpRead1->decoder_list) &&
		 AnyUPCorEAN(lpRead1->decoder_list) )
	{
		// If the UPCEAN general params do not match exactly
		if ( !Equal(&lpRead1->upcean_general,
					sizeof(lpRead1->upcean_general),
					&lpRead2->upcean_general) )
		{
			// Then the reads are not compatible
			return(FALSE);
		}
	}

	// Passed all tests, the reads are compatible
	return(TRUE);
}


