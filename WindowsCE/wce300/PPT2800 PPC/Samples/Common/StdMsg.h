
//--------------------------------------------------------------------
// FILENAME:			StdMsg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for StdMsg.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDMSG_H_

#define STDMSG_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------

#include "..\common\StdFuncs.h"


//----------------------------------------------------------------------------
// Externals
//----------------------------------------------------------------------------

extern int nMsgStr1Id;
extern int nMsgStr2Id;
extern int nMsgDialogId;
extern int nYesNoDialogId;
extern DIALOG_CHOICE MsgDialogChoices[];
extern DIALOG_CHOICE YesNoDialogChoices[];


//----------------------------------------------------------------------------
// Imported Libraries
//----------------------------------------------------------------------------

#ifdef _WIN32_WCE
	#pragma comment(lib,"winsock.lib")
#else
	#pragma comment(lib,"ws2_32.lib")
#endif


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

void MyError(LPTSTR pszMsg1,LPTSTR pszMsg2);

void LastError(LPTSTR pszFunction);

void ReportError(LPTSTR pszFunction,
				 DWORD dwError);


void MyMessage(LPTSTR pszMsg1,LPTSTR pszMsg2);

void MyMessageThenPost(LPTSTR pszMsg1,
					   LPTSTR pszMsg2,
					   HWND hwnd,
					   UINT umsg,
					   WPARAM wparam,
					   LPARAM lparam);

BOOL MyYesNo(LPTSTR pszMsg1,
			 LPTSTR pszMsg2);

void WSAError(LPTSTR pszFunction);


#define ErrorHandler() ErrorHandlerEx((WORD)(__LINE__), TEXT(__FILE__))

void ErrorHandlerEx(WORD, LPTSTR);


void SetRealMessageBox(BOOL bFlag);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDMSG_H_	*/

