//--------------------------------------------------------------------
// FILENAME:			StdDlg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for StdDlg.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef STDDLG_H_

#define STDDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Exported Functions
//--------------------------------------------------------------------


DWORD InvokeDialog(LPTSTR lpszDialogTitle,
				   LPTSTR lpszFileAndPath,
				   LPTSTR lpszReturnValue,
				   DWORD dwReturnValueSize);




#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDDLG_H_	*/

