
//--------------------------------------------------------------------
// FILENAME:			StdInit.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains exported functions and variables from
//						StdInit.c.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef STDINIT_H_

#define STDINIT_H_

#ifdef __cplusplus
extern "C"
{
#endif



//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------


BOOL InitApplication(HINSTANCE hInstance,
					 LPTSTR szAppClass,
					 HBRUSH hBackgroundBrush,
					 LPTSTR szMainMenu,
					 LPTSTR szAppIcon,
					 BOOL bMultipleInstances);


BOOL InitInstance(HINSTANCE hInstance,
				  int nCmdShow,
				  DWORD dwStyle,
				  LPTSTR szAppClass,
				  LPTSTR szAppTitle);


int StdDoMain(int nAccel);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef STDINIT_H_   */

