
//--------------------------------------------------------------------
// FILENAME:			StdAbout.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for StdAbout.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDABOUT_H_

#define STDABOUT_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Nested Includes
//----------------------------------------------------------------------------

#include "..\common\StdFuncs.h"

#include "..\about\resource.h"


//----------------------------------------------------------------------------
// Externals
//----------------------------------------------------------------------------

extern DIALOG_CHOICE AboutDialogChoices[];

extern int nAboutDialogId;
extern int nAboutCtl1Id;
extern int nAboutStr1Id;
extern int nAboutCtl2Id;
extern int nAboutStr2Id;
extern int nAboutButtonId;
extern int nAboutIconId;


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

void About(HWND hwnd,
		   int p_nAboutStr2Id,
		   int p_nAboutStr1Id,
		   int p_nAboutIconId);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDABOUT_H_	*/

