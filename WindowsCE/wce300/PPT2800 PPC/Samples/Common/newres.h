//--------------------------------------------------------------------
// FILENAME:			NEWRES.H
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef __NEWRES_H__
#define __NEWRES_H__

#if !defined(UNDER_CE)
#define UNDER_CE _WIN32_WCE
#endif

#if defined(_WIN32_WCE)
	#if !defined(WCEOLE_ENABLE_DIALOGEX)
		#define DIALOGEX DIALOG DISCARDABLE
	#endif
	#include <commctrl.h>
	#define  SHMENUBAR RCDATA
	#ifdef WIN32_PLATFORM_PDT8100
		#define WIN32_PLATFORM_PSPC
	#endif
	#ifdef WIN32_PLATFORM_PPT2800
		#define WIN32_PLATFORM_PSPC
	#endif
	#ifdef WIN32_PLATFORM_SPS3000
		#define WIN32_PLATFORM_PSPC
	#endif
	#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)
		#include <aygshell.h>
		#define AFXCE_IDR_SCRATCH_SHMENU  28700
	#else
		#define I_IMAGENONE		(-2)
		#define NOMENU			0xFFFF
		#define IDS_SHNEW		1

		#define IDM_SHAREDNEW        10
		#define IDM_SHAREDNEWDEFAULT 11
	#endif // _WIN32_WCE_PSPC
	#define AFXCE_IDD_SAVEMODIFIEDDLG 28701
#else
	#include <commctrl.h>
	#define I_IMAGENONE		(-2)
	#define LANG_ENGLISH                     0x09
	#define SUBLANG_ENGLISH_US               0x01    /* English (USA) */
#endif // _WIN32_WCE

#ifdef RC_INVOKED
#ifndef _INC_WINDOWS
#define _INC_WINDOWS
	#include "winuser.h"           // extract from windows header
#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)
	#include "winver.h"
#endif
#endif
#endif

#ifdef IDC_STATIC
#undef IDC_STATIC
#endif
#define IDC_STATIC      (-1)

#endif //__NEWRES_H__
