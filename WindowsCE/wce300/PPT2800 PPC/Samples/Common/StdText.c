
//--------------------------------------------------------------------
// FILENAME:			StdText.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Standard text handling functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <malloc.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdMsg.h"
#include "..\common\StdScrns.h"
#include "..\common\StdFuncs.h"

#include "..\common\StdText.h"


typedef struct tagTEXTLINE
{
	BOOL bDirty;
	LPTSTR lpszData;
} TEXTLINE;
typedef TEXTLINE *LPTEXTLINE;

static LPTEXTLINE lpLines = NULL;
static int nMaxLines = 0;
static int nCurLine = 0;
static RECT workarea;
static int nFontHeight;

static HFONT l_hFont;

static HANDLE hHeap = NULL;


//----------------------------------------------------------------------------
// TextFreeLine
//----------------------------------------------------------------------------

DWORD TextFreeLine(int i)
{
	LPTSTR lpszData;

	// If no lines are allocated yet, then we are done
	if ( lpLines == NULL )
	{
		return(ERROR_SUCCESS);
	}
	
	// Get the pointer to the line
	lpszData = lpLines[i].lpszData;

	// If its allocated
	if ( lpszData != NULL )
	{
		// Release the memory
		HeapFree(hHeap,
				 0,
				 (HLOCAL)lpszData);
	}

	// Mark the line unallocated and dirty
	lpLines[i].lpszData = NULL;
	lpLines[i].bDirty = TRUE;

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextAllocLine
//----------------------------------------------------------------------------

DWORD TextAllocLine(int i,
				   DWORD dwSize)
{
	DWORD dwByteSize;
	
	// Compute the size in bytes needed for the line 
	dwByteSize = (dwSize+1)*sizeof(TCHAR);

	// If the line is allocated and
	//   its too small for the new data
	if ( ( lpLines[i].lpszData != NULL ) &&
		 ( HeapSize(hHeap,
					0,
					lpLines[i].lpszData) < dwByteSize ) )
	{
		// Deallocate the line
		TextFreeLine(i);
	}

	// If the line is unallocated
	if ( lpLines[i].lpszData == NULL )
	{
		LPTSTR lpszData;

		// If the heap is invalid
		if ( !HeapValidate(hHeap,
						   0,
						   NULL) )
		{
			// Report an error
			LastError(TEXT("Heap Invalid"));

			// Return an error
			return(ERROR_NOT_ENOUGH_MEMORY);
		}
		else
		{
			// Allocate the line
			lpszData = (LPTSTR)HeapAlloc(hHeap,
										 HEAP_ZERO_MEMORY,
										 dwByteSize);

			// Mark the line allocated
			lpLines[i].lpszData = lpszData;
		}
	}

	// If the the is unallocated
	if ( lpLines[i].lpszData == NULL )
	{
		// Report an error
		LastError(TEXT("TextAllocLine"));

		// Return an error
		return(ERROR_NOT_ENOUGH_MEMORY);
	}

	// Mark the line dirty
	lpLines[i].bDirty = TRUE;

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextScrollUp
//----------------------------------------------------------------------------

DWORD TextScrollUp(void)
{
	// If the current line is not at the top of the screen
	if ( nCurLine > 0 )
	{
		int i;
		
		// Deallocate the first line
		TextFreeLine(0);

		// Loop through all but last line
		for(i=0;i<nMaxLines-1;i++)
		{
			// Move line up one
			lpLines[i].lpszData = lpLines[i+1].lpszData;

			// Mark the line dirty
			lpLines[i].bDirty = TRUE;
		}
		
		// Mark last line non-existant and dirty
		lpLines[nMaxLines-1].lpszData = NULL;
		lpLines[nMaxLines-1].bDirty = TRUE;
		
		// Back current line up one
		nCurLine--;

		// Invalidate the area held by the line
		InvalidateRect(g_hwnd,NULL,TRUE);
	}

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextInit
//----------------------------------------------------------------------------

DWORD TextInit(void)
{
	HDC hdc;
	TEXTMETRIC tm;
	int nNewLines;
	int i;
	DWORD dwByteSize;

	if ( g_hwnd == NULL )
	{
		return(ERROR_SUCCESS);
	}
	
	// If the heap has not been allocated
	if ( hHeap == NULL )
	{
		// Allocate the heap
		hHeap = HeapCreate(0,0,0);
	}

	// If the heap is not allocated
	if ( hHeap == NULL )
	{
		// Report an error
		LastError(TEXT("HeapCreate"));

		// Return an error
		return(ERROR_NOT_ENOUGH_MEMORY);
	}
	
	l_hFont = SetNewWindowFont(g_hwnd,0);

	// Get the font height
	hdc = GetDC(g_hwnd);

	SelectObject(hdc,l_hFont);

	GetTextMetrics(hdc,&tm);
	nFontHeight = tm.tmHeight + tm.tmExternalLeading;
	ReleaseDC(g_hwnd,hdc);

	// Get the child area
	GetChildArea(&workarea);
	
	// Compute number of lines in child area
	nNewLines = ( workarea.bottom - workarea.top - g_nMenuHeight ) / nFontHeight;
	
	// Compute size of line array needed for the number of lines
	dwByteSize = nNewLines*sizeof(TEXTLINE);

	// If the line array is allocated
	if ( lpLines != NULL )
	{
		// If the number of lines has increased
		if ( nNewLines > nMaxLines )
		{
			// Reallocate line array larger
			lpLines = (LPTEXTLINE)HeapReAlloc(hHeap,
											HEAP_ZERO_MEMORY,
											  (HLOCAL)lpLines,
											  dwByteSize);
	
			if ( lpLines != NULL )
			{
				for(i=nMaxLines;i<nNewLines;i++)
				{
					lpLines[i].lpszData = NULL;
				}
			}

		}
		else
		{
			// If the number of lines has decreased
			if ( nNewLines < nMaxLines )
			{
				// While the current line is off the screen
				while ( nCurLine >= nNewLines )
				{
					// Scroll the screen up one line
					TextScrollUp();
				}

				// Deallocate lines that are now off screen
				for(i=nNewLines;i<nMaxLines;i++)
				{
					TextFreeLine(i);
				}
			}
		}
	}

	// If the line array is not allocated
	if ( lpLines == NULL )
	{
		// Allocate the line array
		lpLines = (LPTEXTLINE)HeapAlloc(hHeap,
										HEAP_ZERO_MEMORY,
										dwByteSize);

		for(i=0;i<nNewLines;i++)
		{
			lpLines[i].lpszData = NULL;
		}
	}

	// If the line array is not allocated
	if ( lpLines == NULL )
	{
		// Report an error
		LastError(TEXT("TextInit"));

		// Return an error
		return(ERROR_NOT_ENOUGH_MEMORY);
	}

	// Change number of lines in array
	nMaxLines = nNewLines;

	// Invalidate the entire screen
	InvalidateRect(g_hwnd,NULL,TRUE);

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextPaint
//----------------------------------------------------------------------------

DWORD TextPaint(HWND hwnd)
{
	PAINTSTRUCT ps;
	HDC hdc;
	HFONT hFont;
	DWORD dwScreenType;

	dwScreenType = g_dwScreenType;
	
	hFont = GetNewFont(FALSE);

	if ( hFont != l_hFont )
	{
		l_hFont = hFont;
	}

	// Begin the paint operation
	hdc = BeginPaint(hwnd,&ps);

	SelectObject(hdc,hFont);

	// If the lines array is allocated
	if ( lpLines != NULL )
	{
		int i;

		// Loop through all lines
		for(i=0;i<nMaxLines;i++)
		{
			// If the line is dirty
			if ( lpLines[i].bDirty )
			{
				// If the line is allocated
				if ( lpLines[i].lpszData != NULL )
				{
					RECT r = ps.rcPaint;

					// Compute bounding rectangle
					r.left = 5;
					r.top = i*nFontHeight+workarea.top;
					r.right = ps.rcPaint.right;
					r.bottom = r.top + nFontHeight;

#ifdef DEBUGMSG
					{
						TCHAR szMsg[MAX_PATH];

						TEXTSPRINTF(szMsg,
									TEXT("X = %d. y = %d, %s\r\n"),
									r.left,
									r.top,
									lpLines[i].lpszData);
						OutputDebugString(szMsg);
					}
#endif

					// Display the text
					if ( !ExtTextOut(hdc,
							   r.left,
							   r.top,
							   ETO_OPAQUE,
							   &r, //NULL,
							   lpLines[i].lpszData,
							   TEXTLEN(lpLines[i].lpszData),
							   NULL) )
					{
						TCHAR szMsg[MAX_PATH];

						// Output an error message
						TEXTSPRINTF(szMsg,TEXT("Error %d\r\n"),GetLastError());
						OutputDebugString(szMsg);
					}
				}
			}
		}
	}

	// End the paint operation
	EndPaint(hwnd,&ps);

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// InvalidateLine
//----------------------------------------------------------------------------

DWORD InvalidateLine(int nLine)
{
	RECT nRect;
	
	// Get the child window area
	GetChildArea(&nRect);

	// Compute vertical bounds of this line
	nRect.top += ( nLine * nFontHeight );
	nRect.bottom = nRect.top + nFontHeight;

	// Invalidate the rectangle containing the line
	InvalidateRect(g_hwnd,&nRect,TRUE);

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextPutLine
//----------------------------------------------------------------------------

DWORD TextPutLine(LPCTSTR lpszStr)
{
	DWORD dwResult = ERROR_SUCCESS;
	
	// Save current line as the target line
	int nLine = nCurLine;
	
	// If the current line is not at the bottom of the screen
	if ( nCurLine >= nMaxLines )
	{
		// Scroll the screen up one line
		TextScrollUp();

		// Move back one 
		nLine--;
// don't need to move back one line here, TextScrollUp already move it back
//		nCurLine--;
	}

	// Move the current line ahead one
	nCurLine++;

	// If the string to print is non-NULL
	if ( lpszStr != NULL )
	{
		// Allocate the line if needed
		dwResult = TextAllocLine(nLine,TEXTLEN(lpszStr));

		// If the allocatw was successful
		if ( dwResult == ERROR_SUCCESS )
		{
			// If the line is allocated
			if ( lpLines[nLine].lpszData != NULL )
			{
				// Copy the string into the line
				TEXTCPY(lpLines[nLine].lpszData,lpszStr);
			
				// Invalidate the area held by the line
				InvalidateLine(nLine);
			}
		}
	}

	// Return success
	return(dwResult);
}


//----------------------------------------------------------------------------
// TextClear
//----------------------------------------------------------------------------

DWORD TextClear(void)
{
	int i;

	// If no lines are allocated yet, then we are done
	if ( lpLines == NULL )
	{
		return(ERROR_SUCCESS);
	}
	
	// Loop through all lines
	for(i=0;i<nMaxLines;i++)
	{
		// Deallocate the line
		TextFreeLine(i);
	}

	// Invalidate the entire screen
	InvalidateRect(g_hwnd,NULL,TRUE);

	nCurLine = 0;

	// Return success
	return(ERROR_SUCCESS);
}


//----------------------------------------------------------------------------
// TextTerm
//----------------------------------------------------------------------------

DWORD TextTerm(void)
{
	DWORD dwResult = ERROR_SUCCESS;
	
	// Clear the screen
	dwResult = TextClear();

	// If the line array is allocated
	if ( lpLines != NULL )
	{
		// Deallocate the line array
		HeapFree(hHeap,
				 0,
				 (HLOCAL)lpLines);

		// Mark the line array deallocated
		lpLines = NULL;
	}

	// Deallocate the heap
	HeapDestroy(hHeap);

	// Mark the heap deallocated
	hHeap = NULL;

	// Return success
	return(dwResult);
}

