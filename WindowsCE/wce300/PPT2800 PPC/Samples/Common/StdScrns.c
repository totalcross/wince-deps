
//--------------------------------------------------------------------
// FILENAME:			StdScrns.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains standard functions for screen handling
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdMsg.h"
#include "..\common\StdFuncs.h"

#include "..\common\StdScrns.h"


#ifndef _WIN32_WCE
	#pragma comment(lib,"comctl32.lib")
#endif


typedef HRESULT (WINAPI *SHSIPINFOFUNC)(UINT uiAction,
										UINT uiParam,
										PVOID pvParam,
										UINT fWinIni);


typedef HRESULT (WINAPI *GETSIPINFOFUNC)(SIPINFO *);


static BOOL l_bForceScreenLeftDone = FALSE;
static BOOL l_bForceScreenTopDone = FALSE;
static BOOL l_bForceScreenRightDone = FALSE;
static BOOL l_bForceScreenBottomDone = FALSE;

static BOOL l_bAdjustScreenRightDone = FALSE;
static BOOL l_bAdjustScreenBottomDone = FALSE;

static int l_nForceScreenLeft = -1;
static int l_nForceScreenTop = -1;
static int l_nForceScreenRight = -1;
static int l_nForceScreenBottom = -1;

static BOOL l_dwUIStyle = UI_STYLE_DEFAULT;
static BOOL l_bUIStyleDone = FALSE;


SCREEN_TYPE ScreenTypeTable[] =
{
#ifdef _ST_PC
	{	ST_PC_STD,		UI_STYLE_PC,		640,480	, 18,500	},
#endif
#ifdef _ST_HPC
	{	ST_HPC_STD,		UI_STYLE_PEN,		480,200	, 18,500	},
#endif
#ifdef _ST_PPC
	{	ST_PPC_STD,		UI_STYLE_PEN,		240,294	, 14,500	},
	{	ST_PPC_SIP,		UI_STYLE_PEN,		240,214	, 14,500	},
#endif
#ifdef _ST_7200
	{	ST_7200_STD,	UI_STYLE_FINGER,	240,320	, 18,500	},
	{	ST_7200_TSK,	UI_STYLE_FINGER,	240,288	, 18,500	},
#endif
#ifdef _ST_7500
	{	ST_7500_STD,	UI_STYLE_KEYPAD,	240,160	, 14,500	},
	{	ST_7500_TSK,	UI_STYLE_KEYPAD,	240,128	, 14,500	},
#endif

	{	ST_UNKNOWN,		UI_STYLE_DEFAULT,	0,0		, 0,0		}
};


static int l_nSaveForceScreenLeft = -1;
static int l_nSaveForceScreenTop = -1;
static int l_nSaveForceScreenRight = -1;
static int l_nSaveForceScreenBottom = -1;

static BOOL l_bWasCalledBy = FALSE;

static int l_nDeltaHeight = 0;


void SetDeltaHeight(int nHeight)
{
	l_nDeltaHeight = nHeight;
}


int GetDeltaHeight(void)
{
	return(l_nDeltaHeight);
}


BOOL WasCalledBy(void)
{
	return(l_bWasCalledBy);
}


void SetCalledBy(BOOL bFlag)
{
	l_bWasCalledBy = bFlag;
}


static void ForceScreenSizeFromRegistry(LPRECT pr)
{
	GetRegistrySetting(&pr->left,
					   &l_bForceScreenLeftDone,
					   &l_nForceScreenLeft,
					   TEXT("ForceScreenLeft"));

	GetRegistrySetting(&pr->top,
					   &l_bForceScreenTopDone,
					   &l_nForceScreenTop,
					   TEXT("ForceScreenTop"));
	
	GetRegistrySetting(&pr->right,
					   &l_bForceScreenRightDone,
					   &l_nForceScreenRight,
					   TEXT("ForceScreenRight"));
	
	GetRegistrySetting(&pr->bottom,
					   &l_bForceScreenBottomDone,
					   &l_nForceScreenBottom,
					   TEXT("ForceScreenBottom"));
#ifndef _WIN32_WCE
	if ( l_bForceScreenRightDone && !l_bAdjustScreenRightDone )
	{
		l_nForceScreenRight += 2*GetSystemMetrics(SM_CXFRAME);
		pr->right = l_nForceScreenRight;
		l_bAdjustScreenRightDone = TRUE;
	}

	if ( l_bForceScreenBottomDone && !l_bAdjustScreenBottomDone )
	{
		l_nForceScreenBottom += GetSystemMetrics(SM_CYCAPTION);
		l_nForceScreenBottom += 2*GetSystemMetrics(SM_CYFRAME);
		if ( g_wc.lpszMenuName != NULL )
		{
			l_nForceScreenBottom += GetSystemMetrics(SM_CYMENUSIZE)+1;
		}
		pr->bottom = l_nForceScreenBottom;
		l_bAdjustScreenBottomDone = TRUE;
	}

#endif
}


static void ForceScreenTypeFromRegistry(LPRECT pr)
{
	int nWidth,nHeight;

	nWidth = pr->right - pr->left + 1;
	nHeight = pr->bottom - pr->top + 1;

	if ( ( nWidth <= 200 ) ||
		 ( nHeight <= 160 ) )
	{
		g_dwUIStyle = UI_STYLE_KEYPAD;
	}

	GetRegistrySetting(&l_dwUIStyle,
					   &l_bUIStyleDone,
					   &l_dwUIStyle,
					   TEXT("UIStyle"));

	if ( l_bUIStyleDone &&
		 ( l_dwUIStyle != UI_STYLE_DEFAULT ) )
	{
		g_dwUIStyle = l_dwUIStyle;
	}
}


static BOOL l_bSipIsUp = FALSE;


//----------------------------------------------------------------------------
// GetWorkArea
//----------------------------------------------------------------------------

void GetWorkArea(RECT *pr)
{
	// Get work area from system
	SystemParametersInfo(SPI_GETWORKAREA,0,(PVOID)pr,0);

#ifdef WIN32_PLATFORM_PSPC

	{
		HINSTANCE hDll;
		SIPINFO si;

		// Try and load the Palm-Sized PC / PocketPC shell dll
		hDll = LoadLibrary(TEXT("aygshell.dll"));

		// If Palm-Sized PC DLL was not loaded
		if ( hDll != NULL )
		{
			SHSIPINFOFUNC lpfnSHSipInfo;

			// Try and get the address of the SHSipInfo function
			lpfnSHSipInfo = (SHSIPINFOFUNC)GetProcAddress(hDll,(PVOID)TEXT("SHSipInfo"));

			// If the function was found
			if ( lpfnSHSipInfo != NULL )
			{
				HRESULT hResult;
				
				// Initialize the SIPINFO structure
				ZEROMEM(&si,sizeof(si));
				si.cbSize = sizeof(si);
				
				// Call the function
				hResult = (*lpfnSHSipInfo)(SPI_GETSIPINFO,0,(PVOID)&si,0);
				
				if ( hResult )
				{
					// Replace system work area with visble area not covered by SIP
					*pr = si.rcVisibleDesktop;

					l_bSipIsUp = (BOOL)(si.fdwFlags & 1);
				}
				else
				{
					DWORD dwError;
					dwError = GetLastError();
					dwError = dwError;
				}
			}
		}

		// Free the DLL
		FreeLibrary(hDll);
	}

#endif

#ifdef WIN32_PLATFORM_EMBEDDED
	{
		HINSTANCE hDll;
		SIPINFO si;

		// Load Coredll instead and hope it has the SIP stuff in it
		hDll = LoadLibrary(TEXT("coredll.dll"));

		// If the DLL was loaded
		if ( hDll != NULL )
		{
			GETSIPINFOFUNC lpfnGetSipInfo;

			// Try and get the address of the GetSipInfo function
			lpfnGetSipInfo = (GETSIPINFOFUNC)GetProcAddress(hDll,(PVOID)TEXT("GetSipInfo"));
			
			// If the function was found
			if ( lpfnGetSipInfo != NULL )
			{
				// Initialize the SIPINFO structure
				ZEROMEM(&si,sizeof(si));
				si.cbSize = sizeof(si);

				// Call the function
				(*lpfnGetSipInfo)(&si);
			
				// Replace system work area with visble area not covered by SIP
				*pr = si.rcVisibleDesktop;

				l_bSipIsUp = (BOOL)(si.fdwFlags & 1);
			}
		}

		// Free the DLL
		FreeLibrary(hDll);
	}
#endif

	ForceScreenSizeFromRegistry(pr);

	pr->bottom -= l_nDeltaHeight;
}



//----------------------------------------------------------------------------
// GetChildArea
//----------------------------------------------------------------------------

void GetChildArea(RECT *pr)
{
	int nWidth;
	int nHeight;
	int nMaxWidth = -1;
	int nMaxHeight = -1;
	LPSCREEN_TYPE lpst;
	DWORD dwLastScreenType;
	
	// Get work area
	GetWorkArea(pr);

	// If there is a main window
	if ( ( g_hwnd != NULL ) )
	{
		// Replace rectangle with client area
		GetClientRect(g_hwnd,pr);
	}

#ifdef _WIN32_WCE

	// If there is a command bar window
	if ( g_hwndCB != NULL )
	{
		// Compute the height of the command bar
		g_nCommandHeight = CommandBar_Height(g_hwndCB) + 1;
	}

	// Adjust top of rectangle to account for command bar
	pr->top += g_nCommandHeight;

#endif

	nWidth = pr->right - pr->left + 1;
	nHeight = pr->bottom - pr->top + 1;

#ifdef _WIN32_WCE

	if ( g_hwnd != NULL )
	{
		if ( g_nCaptionHeight > 0 )
		{
			nHeight += g_nCaptionHeight;

			nWidth += GetSystemMetrics(SM_CXFIXEDFRAME) * 2;
			nHeight += GetSystemMetrics(SM_CYFIXEDFRAME) * 2;
		}
	}

#else

	nHeight += g_nCaptionHeight;

#endif

#ifdef WIN32_PLATFORM_POCKETPC
	if ( l_bSipIsUp )
	{
		nHeight += POCKETPC_MENU_HEIGHT;
		g_nMenuHeight = 0;
	}
	else
	{
		g_nMenuHeight = POCKETPC_MENU_HEIGHT;
	}
#else
	nHeight += g_nCommandHeight;
#endif

	dwLastScreenType = g_dwScreenType;

	g_dwScreenType = ST_UNKNOWN;
	g_nFontHeight = 11;
	g_nFontWeight = 100;

	for(lpst=ScreenTypeTable;lpst->dwScreenType!=ST_UNKNOWN;lpst++)
	{
		if ( ( nWidth >= lpst->nWidth ) &&
			 ( nHeight >= lpst->nHeight ) &&
			 ( lpst->nWidth >= nMaxWidth ) &&
			 ( lpst->nHeight >= nMaxHeight ) )
		{
			g_dwScreenType = lpst->dwScreenType;
			
			g_dwUIStyle = lpst->dwUIStyle;

			nMaxWidth = lpst->nWidth;
			nMaxHeight = lpst->nHeight;

			g_nFontHeight = lpst->nFontHeight;
			g_nFontWeight = lpst->nFontWeight;
		}
	}

	ForceScreenTypeFromRegistry(pr);

	if ( g_dwScreenType != dwLastScreenType )
	{
		dwLastScreenType = g_dwScreenType;
	}
}



BOOL IsSipUp(void)
{
	return(l_bSipIsUp);
}

