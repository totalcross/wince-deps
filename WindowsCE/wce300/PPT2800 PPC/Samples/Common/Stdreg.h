//--------------------------------------------------------------------
// FILENAME:			StdReg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Registry Settings
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDREG_H_

#define STDREG_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------------
#ifdef USE_HKLM
    #define REG_SECTION     HKEY_LOCAL_MACHINE
#else
    #define REG_SECTION     HKEY_CURRENT_USER
#endif

#define REG_SETTINGS        TEXT("Software\\Symbol\\Settings")
    
//----------------------------------------------------------------------------
// Externals
//----------------------------------------------------------------------------



#ifdef __cplusplus
}
#endif

#endif	/* #ifndef MSG_H_	*/

