
//--------------------------------------------------------------------
// FILENAME:			ScanSel.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains window procedures for the scanner selector
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdWproc.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"

#include "..\common\Scan.h"

#include "..\common\SelctDlg.h"

#include "..\common\ScanSel.h"


#ifndef _WIN32_WCE_EMULATION

//----------------------------------------------------------------------------
// InitScanSelList
//----------------------------------------------------------------------------

static BOOL InitScanSelList(HWND hwnd,
							HWND hWndList)
{
	int iResult;
	LV_COLUMN lvC;
	RECT rect;
	int nWidth;
	SCAN_FINDINFO ScanFindInfo;
	HANDLE hFindHandle;
	DWORD dwResult;
	DWORD i;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;
	lvC.fmt = LVCFMT_LEFT;

	GetWindowRect(hWndList,&rect);
	nWidth = rect.right - rect.left + 1;
	
	nWidth -= GetSystemMetrics(SM_CXVSCROLL);

	lvC.iSubItem = 0;
	lvC.cx = nWidth * 98 / 100;
	iResult = ListView_InsertColumn(hWndList,0,&lvC);

	lvC.iSubItem = 1;
	lvC.cx = 0;
	iResult = ListView_InsertColumn(hWndList,1,&lvC);

	// Mark entire buffer allocated and not used
	SI_INIT(&ScanFindInfo);

	dwResult = SCAN_FindFirst(&ScanFindInfo,&hFindHandle);
	
	if ( ScanError(TEXT("Find First Scanner"),
				   dwResult,
				   E_SCN_SUCCESS) )
	{
		
		return(FALSE);
	}

	for(i=0;dwResult==E_SCN_SUCCESS;i++)
	{
		LV_ITEM lvI;

		lvI.mask = LVIF_TEXT;
		lvI.iItem = i;
		lvI.iSubItem = 0;
		lvI.pszText = ScanFindInfo.szFriendlyName;
		lvI.cchTextMax = TEXTLEN(lvI.pszText);
		ListView_InsertItem(hWndList,&lvI);

		lvI.mask = LVIF_TEXT;
		lvI.iItem = i;
		lvI.iSubItem = 1;
		lvI.pszText = ScanFindInfo.szDeviceName;
		lvI.cchTextMax = TEXTLEN(lvI.pszText);
		ListView_SetItem(hWndList,&lvI);

		dwResult = SCAN_FindNext(&ScanFindInfo,hFindHandle);
	};

	SCAN_FindClose(hFindHandle);
	
	if ( i == 1 )
	{
		PostMessage(hwnd,WM_COMMAND,IDOK,0L);
	}

	return(TRUE);
}


//----------------------------------------------------------------------------
// ScanSelDlgCallBack
//----------------------------------------------------------------------------

LRESULT ScanSelDlgCallBack(HWND hwnd,
						   UINT uMsg,
						   WPARAM wParam,
						   LPARAM lParam,
						   HWND hWndList)
{
	TCHAR szScannerName[MAX_PATH];

	switch(uMsg)
	{
		case WM_INITDIALOG:
			
			SetNewDialogFont(hWndList,140);

			PostMessage(hwnd,UM_INIT,0,0L);

			break;

		case UM_INIT:

			if ( !InitScanSelList(hwnd,hWndList) )
			{
				PostMessage(hwnd,UM_QUIT,0,0L);

				return(FALSE);
			}

			break;

		case WM_COMMAND:	

			// Select activty based on command
			switch (LOWORD(wParam))
            {
				case UM_POPUP:

					PostMessage(hwnd,WM_COMMAND,IDOK,0L);
					
					break;

				case IDOK:

					ListView_GetItemText(hWndList,
										 Select_GetSelectedItem(hWndList),
										 1,
										 szScannerName,
										 TEXTSIZEOF(szScannerName));

					ScanSetScannerName(szScannerName);
						
					break;
			}
	}
	
	return(FALSE);
}


static DWORD CountScanners(void)
{
	SCAN_FINDINFO ScanFindInfo;
	HANDLE hFindHandle = NULL;
	DWORD i = 0;
	DWORD dwResult;

	SI_INIT(&ScanFindInfo);

	dwResult = SCAN_FindFirst(&ScanFindInfo,&hFindHandle);

	while ( dwResult == E_SCN_SUCCESS )
	{
		i++;

		dwResult = SCAN_FindNext(&ScanFindInfo,hFindHandle);
	}

	if ( hFindHandle != NULL )
	{
		SCAN_FindClose(hFindHandle);
	}

	return(i);
}


//----------------------------------------------------------------------------
// ScanSel
//----------------------------------------------------------------------------

BOOL ScanSel(void)
{
	DWORD dwCount;
	
	dwCount = CountScanners();

	if ( dwCount < 1 )
	{
		return(FALSE);
	}

	if ( dwCount == 1 )
	{
		return(TRUE);
	}

	return(Select_Dialog(ScanSelDlgCallBack));
}

#endif


