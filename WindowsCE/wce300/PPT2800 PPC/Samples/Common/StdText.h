
//--------------------------------------------------------------------
// FILENAME:			StdText.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for StdText.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDTEXT_H_

#define STDTEXT_H_

#ifdef __cplusplus
extern "C"
{
#endif



DWORD TextInit(void);


DWORD TextPaint(HWND hwnd);


DWORD TextPutLine(LPCTSTR lpszStr);


DWORD TextClear(void);


DWORD TextTerm(void);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDTEXT_H_	*/


