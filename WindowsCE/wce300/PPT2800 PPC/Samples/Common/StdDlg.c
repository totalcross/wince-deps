
//--------------------------------------------------------------------
// FILENAME:			StdDlg.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Functions for standard dialog handling
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"
#include "..\common\StdMsg.h"
#include "..\common\StdExec.h"
#include "..\common\StdWProc.h"

#include "..\common\StdDlg.h"


#ifdef _WIN32_WCE

	#ifdef _WIN32_WCE_EMULATION

		#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
		#define DEFAULT_INKWIZ_PATH TEXT("\\InkWiz.exe")

	#else

		#ifdef WIN32_PLATFORM_GAZOO
			#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
			#define DEFAULT_INKWIZ_PATH TEXT("\\Application\\InkWiz.exe")
		#endif

		#ifdef WIN32_PLATFORM_EMERALD
			#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
			#define DEFAULT_INKWIZ_PATH TEXT("\\Application\\InkWiz.exe")
		#endif

		#ifdef WIN32_PLATFORM_7000
			
			#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
			#define DEFAULT_INKWIZ_PATH TEXT("\\Application\\User\\Demo\\InkWiz.exe")

		#endif

		#ifdef WIN32_PLATFORM_8100
			
			#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
			#define DEFAULT_INKWIZ_PATH TEXT("\\Application\\InkWiz.exe")

		#endif

		#ifdef WIN32_PLATFORM_PSPC
			
			#define DEFAULT_SAVE_FILE_PATH TEXT("\\")
			#define DEFAULT_INKWIZ_PATH TEXT("\\Application\\InkWiz.exe")

		#endif

	#endif

#else

	#define DEFAULT_SAVE_FILE_PATH TEXT("C:\\")
	#define DEFAULT_INKWIZ_PATH TEXT("InkWiz_W32.exe")

#endif


BOOL l_bHaveSaveFilePath = FALSE;
BOOL l_bHaveInkWizPath = FALSE;

TCHAR l_szSaveFilePath[MAX_PATH] = DEFAULT_SAVE_FILE_PATH;
TCHAR l_szInkWizPath[MAX_PATH] = DEFAULT_INKWIZ_PATH;


LPTSTR GetSaveFilePath(void)
{
	if ( !l_bHaveSaveFilePath )
	{
	}

	return(l_szSaveFilePath);
}


void SetSaveFilePath(LPTSTR lpszPath)
{
	l_bHaveSaveFilePath = TRUE;

	TEXTCPY(l_szSaveFilePath,lpszPath);
}


LPTSTR GetInkWizPath(void)
{
	if ( !l_bHaveInkWizPath )
	{
	}

	return(l_szInkWizPath);
}


void SetInkWizPath(LPTSTR lpszPath)
{
	l_bHaveInkWizPath = TRUE;

	TEXTCPY(l_szInkWizPath,lpszPath);
}


BOOL WriteSaveFile(LPTSTR lpszFileName,
				   LPTSTR lpszValue)
{
	HANDLE hFile;
	DWORD i;
	DWORD dwBytes;

	hFile = CreateFile(lpszFileName,
					   GENERIC_WRITE,
					   0,
					   NULL,
					   CREATE_ALWAYS,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);

	if ( hFile == INVALID_HANDLE_VALUE )
	{
		return(FALSE);
	}

	for(i=0;i<TEXTLEN(lpszValue);i++)
	{
		if ( !WriteFile(hFile,lpszValue+i,1,&dwBytes,NULL) ||
			 ( dwBytes != 1 ) )
		{
			CloseHandle(hFile);
			
			return(FALSE);
		}
	}

	if ( !WriteFile(hFile,TEXT("\r"),1,&dwBytes,NULL) ||
		 ( dwBytes != 1 ) )
	{
		CloseHandle(hFile);
		
		return(FALSE);
	}
	
	if ( !WriteFile(hFile,TEXT("\n"),1,&dwBytes,NULL) ||
		 ( dwBytes != 1 ) )
	{
		CloseHandle(hFile);

		return(FALSE);
	}
	
	CloseHandle(hFile);

	return(TRUE);
}


BOOL ReadSaveFile(LPTSTR lpszFileName,
				  LPTSTR lpszValue,
				  DWORD dwValueSize)
{
	HANDLE hFile;
	DWORD i;
	DWORD dwBytes;

	hFile = CreateFile(lpszFileName,
					   GENERIC_READ,
					   0,
					   NULL,
					   OPEN_EXISTING,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);

	if ( hFile == INVALID_HANDLE_VALUE )
	{
		return(FALSE);
	}

	for(i=0;i<dwValueSize;i++)
	{
		if ( !ReadFile(hFile,lpszValue+i,1,&dwBytes,NULL) ||
			 ( dwBytes != 1 ) )
		{
			CloseHandle(hFile);
			return(FALSE);
		}

		lpszValue[i] &= 0xFF;

		if ( ( lpszValue[i] == TEXT('\r') ) ||
			 ( lpszValue[i] == TEXT('\n') ) )
		{
			lpszValue[i] = TEXT('\0');

			CloseHandle(hFile);

			return(TRUE);
		}
	}

	CloseHandle(hFile);

	return(FALSE);
}


DWORD InvokeDialog(LPTSTR lpszDialogTitle,
				   LPTSTR lpszFileAndPath,
				   LPTSTR lpszReturnValue,
				   DWORD dwReturnValueSize)
{
	TCHAR szCommand[MAX_PATH];
	TCHAR szPassEvent[MAX_PATH];
	TCHAR szFailEvent[MAX_PATH];
	TCHAR szQuitEvent[MAX_PATH];
	TCHAR szFileName[MAX_PATH];
	TCHAR szFilePath[MAX_PATH];
	HANDLE hEvents[3];
	DWORD dwResult = WAIT_TIMEOUT;
	BOOL bSuccess = FALSE;
	BOOL bCreated = FALSE;

	TEXTCPY(szFileName,lpszDialogTitle);
	TEXTCAT(szFileName,TEXT(".sav"));

	TEXTCPY(szFilePath,GetSaveFilePath());
	TEXTCAT(szFilePath,szFileName);

	if ( WriteSaveFile(szFilePath,lpszFileAndPath) )
	{
		bCreated = TRUE;

		TEXTCPY(szCommand,TEXT("/CALL "));
		
		TEXTCAT(szCommand,TEXT("/SAVE:"));
		TEXTCAT(szCommand,szFilePath);

		TEXTCPY(szPassEvent,szFileName);
		TEXTCAT(szPassEvent,TEXT("-PASS"));

		TEXTCPY(szFailEvent,szFileName);
		TEXTCAT(szFailEvent,TEXT("-FAIL"));

		TEXTCPY(szQuitEvent,szFileName);
		TEXTCAT(szQuitEvent,TEXT("-QUIT"));

		hEvents[0] = CreateEvent(NULL,FALSE,FALSE,szPassEvent);
		hEvents[1] = CreateEvent(NULL,FALSE,FALSE,szFailEvent);
		hEvents[2] = CreateEvent(NULL,FALSE,FALSE,szQuitEvent);

		if ( ( hEvents[0] != NULL ) ||
			 ( hEvents[1] != NULL ) ||
			 ( hEvents[2] != NULL ) )
		{
			ShowWindow(g_hwnd,SW_HIDE);
			
			if ( Execute(GetInkWizPath(),szCommand) )
			{
				dwResult = WaitForMultipleObjects(3,hEvents,FALSE,INFINITE);
			}

			ShowWindow(g_hwnd,SW_SHOW);

			SetForegroundWindow(g_hwnd);

			SetWindowTitle(g_hwnd,NULL);

			Std_OnSettingChange(g_hwnd,SPI_SETSIPINFO);
		}

		CloseHandle(hEvents[0]);
		CloseHandle(hEvents[1]);
		CloseHandle(hEvents[2]);
	}

	if ( ( dwResult == WAIT_OBJECT_0 ) &&
		 ReadSaveFile(szFilePath,lpszReturnValue,dwReturnValueSize) )
	{
		bSuccess = TRUE;
	}

	if ( bCreated )
	{
		DeleteFile(szFilePath);
	}

	SetForegroundWindow(g_hwnd);

	return(bSuccess);
}


