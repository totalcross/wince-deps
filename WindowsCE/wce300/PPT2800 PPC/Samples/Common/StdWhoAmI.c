
//--------------------------------------------------------------------
// FILENAME:			StdWhoAmI.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Functions for standard WHOAMI box handling
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <winsock.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"

#include "..\common\StdWhoAmI.h"


//----------------------------------------------------------------------------
// GetLocalNameAndIpAddr
//----------------------------------------------------------------------------

static void GetLocalNameAndIpAddr(LPTSTR lpszName,
								  DWORD dwNameSize,
								  LPTSTR lpszIpAddr,
								  DWORD dwIpAddrSize)
{
	WSADATA wsadata;
	LPHOSTENT lphostent;
	CHAR szHostNameA[MAX_PATH];
	CHAR szIpAddrA[MAX_PATH];

	WSAStartup(0x101,&wsadata);

	if ( gethostname(szHostNameA,sizeof(szHostNameA)-1) == ERROR_SUCCESS )
	{
		szHostNameA[sizeof(szHostNameA)-1] = '\0';

#ifdef UNICODE
		mbstowcs(lpszName,szHostNameA,dwNameSize-1);
		lpszName[dwNameSize-1] = TEXT('\0');
#else
		strcpy(lpszName,szHostNameA);
#endif

		lphostent = gethostbyname(szHostNameA);

		if ( lphostent == NULL )
		{
			TEXTCPY(lpszIpAddr,TEXT("IP Addr = Unknown"));
		}
		else
		{
			IN_ADDR inaddr;
			LPSTR lpszAddr;

			inaddr.S_un.S_addr = *(ULONG *)(lphostent->h_addr_list[0]);			

			lpszAddr = inet_ntoa(inaddr);

			strncpy(szIpAddrA,lpszAddr,sizeof(szIpAddrA)-1);
			szIpAddrA[sizeof(szIpAddrA)-1] = '\0';

#ifdef UNICODE
			mbstowcs(lpszIpAddr,szIpAddrA,dwIpAddrSize-1);
			lpszIpAddr[dwIpAddrSize-1] = TEXT('\0');
#else
			strcpy(lpszIpAddr,szIpAddrA);
#endif
		}
	}
	else
	{
		TEXTCPY(lpszName,TEXT("Host = Unknown"));
		TEXTCPY(lpszIpAddr,TEXT("IP Addr = Unknown"));
	}

	WSACleanup();
}


//----------------------------------------------------------------------------
// WhoAmI
//----------------------------------------------------------------------------

void WhoAmI(HWND hwnd,
			int p_nAboutIconId)
{
	TCHAR szHostName[MAX_PATH];
	TCHAR szIpAddr[MAX_PATH];

	GetLocalNameAndIpAddr(szHostName,TEXTSIZEOF(szHostName),
						  szIpAddr,TEXTSIZEOF(szIpAddr));

	About(hwnd,(int)szIpAddr,(int)szHostName,p_nAboutIconId);
}


