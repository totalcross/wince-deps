
//--------------------------------------------------------------------
// FILENAME:			StdExec.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for StdExec.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDEXEC_H_

#define STDEXEC_H_

#ifdef __cplusplus
extern "C"
{
#endif



BOOL Execute(LPTSTR szName,
			 LPTSTR szCmd);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDEXEC_H_	*/


