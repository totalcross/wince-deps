
//--------------------------------------------------------------------
// FILENAME:        StdDebug.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     StdDebug.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef STDDEBUG_H_

#define STDDEBUG_H_

#ifdef __cplusplus
extern "C"
{
#endif


#if defined(_STDDEBUG) || defined(_STDDEBUGSTR)

	#ifdef _WIN32_WCE

		#ifdef _USE_RETAILMSG

			#define DEBUG_INIT(filename)

			#define DEBUG_STRING(cond,string) RETAILMSG(cond,(string))

			#define DEBUG_CLEAR

		#else

			#if defined(_USE_OUTPUTDEBUGSTRING)
		
				#define DEBUG_INIT(filename)

				#define DEBUG_STRING(cond,string) { if (cond) OutputDebugString((string)); }

				#define DEBUG_CLEAR

			#else				
				
				#define DEBUG_INIT(filename) Debug_Init(filename)
				void Debug_Init(LPTSTR lpszFileName);

				#define DEBUG_STRING(cond,string) { if (cond) Debug_String(string); }

				void Debug_String(LPTSTR lpszString);

				#define DEBUG_CLEAR Debug_Clear();

				void Debug_Clear(void);

			#endif

		#endif

	#else

		#define DEBUG_INIT(filename)

		#define DEBUG_STRING(cond,string) { if (cond) OutputDebugString(string); }

		#define DEBUG_CLEAR

	#endif

#else

	#define DEBUG_INIT(filename)

	#define DEBUG_STRING(cond,string)

	#define DEBUG_CLEAR

#endif


#ifdef _STDDEBUG

	#define DEBUG_HERE \
		{ \
			TCHAR szMsg[MAX_PATH]; \
			TEXTSPRINTF(szMsg, \
						TEXT("Debug at %s, line %d\r\n"), \
						 TEXT(__FILE__),\
						(WORD)(__LINE__)); \
			DEBUG_STRING(TRUE,szMsg); \
		}

	#define DEBUG_FMTMSG(text,umsg) \
			{ \
				if ( umsg == UM_DEBUG ) \
				{ \
					TEXTCPY(text,TEXT("UM_DEBUG")); \
				} \
				else if ( umsg == UM_INIT ) \
				{ \
					TEXTCPY(text,TEXT("UM_INIT")); \
				} \
				else if ( umsg == UM_RESIZE ) \
				{ \
					TEXTCPY(text,TEXT("UM_RESIZE")); \
				} \
				else if ( umsg == UM_ENTERDLG ) \
				{ \
					TEXTCPY(text,TEXT("ENTERDLG")); \
				} \
				else if ( umsg == UM_EXITDLG ) \
				{ \
					TEXTCPY(text,TEXT("EXITDLG")); \
				} \
				else if ( umsg >= UM_USER ) \
				{ \
					TEXTSPRINTF(text,TEXT("UM_USER+%04X"),umsg-UM_USER); \
				} \
				else if ( umsg >= WM_USER ) \
				{ \
					TEXTSPRINTF(text,TEXT("WM_USER+%04X"),umsg-WM_USER); \
				} \
				else if ( umsg >= WM_APP ) \
				{ \
					TEXTSPRINTF(text,TEXT("WM_APP+%04X"),umsg-WM_APP); \
				} \
				else \
				{ \
					TEXTSPRINTF(text,TEXT("UNKNOWN"),umsg-WM_APP); \
				} \
			}

	#define DEBUG_MSG(text,hwnd,umsg,wparam,lparam) \
		{ \
			TCHAR szMsg[MAX_PATH]; \
			TCHAR szMsgText[MAX_PATH]; \
			DEBUG_FMTMSG(szMsgText,umsg); \
			TEXTSPRINTF(szMsg, \
						TEXT("Debug Msg '%s'=%.8X, '%s'=%.8X, wparam=%.8X, lparam=%.8x\r\n"), \
						text,hwnd,szMsgText,umsg,wparam,lparam); \
			DEBUG_STRING(TRUE,szMsg); \
		}

#else

	#define DEBUG_HERE

	#define DEBUG_MSG(text,hwnd,umsg,wparam,lparam)

#endif


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef STDDEBUG_H_  */
