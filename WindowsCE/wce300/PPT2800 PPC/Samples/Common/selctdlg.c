
//--------------------------------------------------------------------
// FILENAME:			SelctDlg.c
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains window procedures for selection dialogs
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\Select\resource.h"

#include "..\common\StdGlobs.h"

#include "..\common\StdWproc.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"

#include "..\common\SelctDlg.h"


// Macro defining the state bits for "selected" items

#define UI_SELECTED ( LVNI_SELECTED | LVNI_FOCUSED )


static BOOL l_bOKed = FALSE;


static LPARAM ListView_GetSelectedParam(HWND hWndList);

static void ListView_GetSelectedText(HWND hWndList,LPTSTR lpszText,DWORD dwSize);

static void ListView_SelectItem(HWND hWndList,int iItem);


DIALOG_CHOICE SelectDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_SELECT_PC_STD,			NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_SELECT_HPC_STD,			NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
#ifdef WIN32_PLATFORM_POCKETPC
	{ ST_PPC_STD,	IDD_SELECT_POCKETPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SELECT_POCKETPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#else
	{ ST_PPC_STD,	IDD_SELECT_PPC_STD,			NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SELECT_PPC_SIP,			NULL,	UI_STYLE_DEFAULT },
#endif
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_SELECT_7200_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_SELECT_7200_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_SELECT_7500_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_SELECT_7500_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_SELECT_7500_TSK,		NULL,	UI_STYLE_DEFAULT }
};


//----------------------------------------------------------------------------
// Select_GetSelectedItem
//
// Get the currently selected item index of a list view
//----------------------------------------------------------------------------

int Select_GetSelectedItem(HWND hWndList)
{
	int iItem;
	int nState;
	
	iItem = ListView_GetNextItem(hWndList,-1,UI_SELECTED);

	nState = ListView_GetItemState(hWndList,iItem,UI_SELECTED);

	return(iItem);
}


//----------------------------------------------------------------------------
// ListView_GetSelectedParam
//
// Get the currently selected item parameter of a list view
//----------------------------------------------------------------------------

static LPARAM ListView_GetSelectedParam(HWND hWndList)
{
	int iItem;
	LV_ITEM lvI;

	iItem = Select_GetSelectedItem(hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	return(lvI.lParam);
}


//----------------------------------------------------------------------------
// ListView_GetSelectedText
//
// Get the currently selected item text of a list view
//----------------------------------------------------------------------------

static void ListView_GetSelectedText(HWND hWndList,
									 LPTSTR lpszText,
									 DWORD dwSize)
{
	int iItem;

	iItem = Select_GetSelectedItem(hWndList);

	ListView_GetItemText(hWndList,iItem,0,lpszText,dwSize);
}


//----------------------------------------------------------------------------
// ListView_SelectItem
//
// Select an item in a list view
//----------------------------------------------------------------------------

static void ListView_SelectItem(HWND hWndList,
								int iItem)
{
	SetFocus(hWndList);

	ListView_SetItemState(hWndList,
						  iItem,
						  LVIS_SELECTED|LVIS_FOCUSED,
						  LVIS_SELECTED|LVIS_FOCUSED);
}


//----------------------------------------------------------------------------
// Select_GetOK
//----------------------------------------------------------------------------

BOOL Select_GetOK(void)
{
	return(l_bOKed);
}


//----------------------------------------------------------------------------
// Select_SetOK
//----------------------------------------------------------------------------

void Select_SetOK(BOOL bOKed)
{
	l_bOKed = bOKed;
}

	
//----------------------------------------------------------------------------
// Select_DlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK Select_DlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	static BOOL bQuitOnExit = FALSE;
	static HWND hWndList = NULL;
	static LPLVDLGCB lpDlgCallBack = NULL;

	BOOL bFlag;

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(hWndList,Select_GetSelectedItem(hWndList));

			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			Select_SetOK(FALSE);
			
			MaxDialog(hwnd);

			lpDlgCallBack = (LPLVDLGCB)lParam;

			if ( lpDlgCallBack == NULL )
			{
				PostMessage(hwnd,WM_COMMAND,IDCANCEL,0L);

				return(FALSE);
			}

			hWndList = GetDlgItem(hwnd,IDC_SEL_LISTVIEW);

			break;

		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				switch(pLvdi->hdr.code)
				{
					case NM_DBLCLK:

						PostMessage(hwnd,WM_COMMAND,IDOK,0);
						
						break;

					case NM_CLICK:
						
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(hWndList,&lvhti);

#ifdef WIN32_PLATFORM_POCKETPC
							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}
#else
							break;
#endif

						}

					// Fall through

					case LVN_ITEMACTIVATE:
						
						PostMessage(hwnd,WM_COMMAND,IDOK,0);
						
						break;
				}
			}
			
			break;

        case UM_INIT:

			if ( lpDlgCallBack != NULL )
			{
				bFlag = lpDlgCallBack(hwnd,uMsg,wParam,lParam,hWndList);

				if ( !bFlag )
				{
					ListView_SelectItem(hWndList,0);
				}

				return(bFlag);
			}

			return(FALSE);

        case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_SEL_SCROLL_UP:
					
					SetFocus(hWndList);
					
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDC_SEL_SCROLL_DOWN:

					SetFocus(hWndList);
					
					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDOK:

					Select_SetOK(TRUE);

					// Fall through

				case IDCANCEL:

					if ( lpDlgCallBack != NULL )
					{
						bFlag = lpDlgCallBack(hwnd,uMsg,wParam,lParam,hWndList);

						if ( !bFlag )
						{
							ExitDialog();
						}

						return(bFlag);
					}

					return(FALSE);
            }

            break;
	}

	if ( lpDlgCallBack != NULL )
	{
		return(lpDlgCallBack(hwnd,uMsg,wParam,lParam,hWndList));
	}
	
	// Message was not processed
    return(FALSE);
}



BOOL Select_Dialog(LPLVDLGCB lpDlgCallBack)
{
	Select_SetOK(FALSE);

	g_hdlg = CreateChosenDialog(g_hInstance,
								SelectDialogChoices,
								g_hwnd,
								Select_DlgProc,
								(LPARAM)lpDlgCallBack);

	WaitUntilDialogDoneSkip(FALSE);

	return(Select_GetOK());
}

