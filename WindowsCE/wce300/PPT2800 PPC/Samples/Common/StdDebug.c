
//--------------------------------------------------------------------
// FILENAME:            StdDebug.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     For debugging use
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>

#include "..\common\StdStrng.h"

#include "StdDebug.h"


#ifdef _WIN32_WCE
	#define DEBUGFILE TEXT("\\Application\\DebugLog.txt")
#else
	#define DEBUGFILE TEXT("DebugLog.txt")
#endif


static TCHAR l_szFileName[MAX_PATH] = DEBUGFILE;


void Debug_Init(LPTSTR lpszFileName)
{
	if ( lpszFileName == NULL )
	{
		TEXTCPY(l_szFileName,DEBUGFILE);
	}
	else
	{
		TEXTCPY(l_szFileName,lpszFileName);
	}

	DEBUG_CLEAR;
}


void Debug_String(LPTSTR lpszString)
{
	HANDLE hFile;
	DWORD dwWritten;

	hFile = CreateFile(l_szFileName,
					   GENERIC_WRITE|GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_ALWAYS,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);

	if ( hFile != NULL )
	{
		SetFilePointer(hFile,0L,0L,FILE_END);
	}

	if ( hFile != NULL )
	{
		TCHAR szMsg[MAX_PATH];
		SYSTEMTIME systime;

		GetLocalTime(&systime);
			
		TEXTSPRINTF(szMsg,
					TEXT("%02d:%02d:%02d %s\r\n"),
					systime.wHour,
					systime.wMinute,
					systime.wSecond,
					lpszString);

#ifdef UNICODE

		{
			CHAR szBufferA[MAX_PATH];
			INT iBytesConverted;

			iBytesConverted = WideCharToMultiByte(CP_OEMCP,
												  0,
												  szMsg,
												  TEXTLEN(szMsg),
												  szBufferA,
												  sizeof(szBufferA),
												  NULL,
												  NULL);

			WriteFile(hFile,szBufferA,iBytesConverted,&dwWritten,NULL);
		}

#else

		WriteFile(hFile,szMsg,TEXTLEN(szMsg),&dwWritten,NULL);

#endif

		CloseHandle(hFile);
	}
}


void Debug_Clear(void)
{
	DeleteFile(l_szFileName);
}


#ifdef APP_CLASS
void ShowMemory(void)
{
	MEMORYSTATUS MemoryStatus;
	TCHAR szMsg[MAX_PATH];

	MemoryStatus.dwLength = sizeof(MemoryStatus);
	GlobalMemoryStatus(&MemoryStatus);

	TEXTSPRINTF(szMsg,
				TEXT("%s : %d free mem"),
				APP_CLASS,
				MemoryStatus.dwAvailPhys);

	SetWindowText(g_hwnd,szMsg);
	UpdateWindow(g_hwnd);
}
#endif


