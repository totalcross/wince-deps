
//--------------------------------------------------------------------
// FILENAME:			StdFuncs.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Standard helper functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#if defined(WIN32_PLATFORM_PSPC)
#include <aygshell.h>
#endif

#if defined(SYMBOL_AUDIO_API)
#include <audiocapi.h>
#pragma comment(lib,"audioapi32.lib")
#endif

#include "..\common\StdStrng.h"
#include "..\common\StdScrns.h"
#include "..\common\StdMsg.h"
#include "..\common\StdWProc.h"

#include "..\common\StdFuncs.h"

//#define _STDDEBUG
//#define _USE_RETAILMSG
#include "..\common\StdDebug.h"


typedef struct tagSAVE_DLG
{
	HWND hwnd;
	BOOL bDone;
	HFONT hFont;
	HINSTANCE hInstance;
	LPDIALOG_CHOICE lpDialogChoices;
	HWND hWndParent;
	DLGPROC lpDialogFunc;
	DWORD dwInitParam;
	LPTSTR lpTemplateName;
	LPVOID lpvDialogSaveInfo;

} SAVE_DLG;


static BOOL l_bResizing = FALSE;

static LPTSTR l_lpTemplateName;


#define MAX_DLGS 100

static SAVE_DLG g_SaveDlgs[MAX_DLGS];

static int g_nNextDlg = 0;


BOOL IsInDialog(void)
{
	if ( g_nNextDlg == 0 )
	{
		return(FALSE);
	}
	else
	{
		return(TRUE);
	}
}


static BOOL l_nPopUpCount = 0;

static HMENU l_hMenu = NULL;


BOOL IsInPopUpMenu(void)
{
	if ( ( l_hMenu != NULL ) && ( l_nPopUpCount > 0 ) )
	{
		return(TRUE);
	}

	return(FALSE);
}


void CancelPopUpMenu(void)
{
	SendMessage(HWND_BROADCAST, WM_CANCELMODE, 0, (LPARAM) 0);
}


void InvokePopUpMenu(HWND hwnd,
					 int nMenu,
					 int nIndex,
					 LONG x,
					 LONG y)
{
	HMENU hSubMenu = NULL;
	DWORD dwId = 0;

	if ( ( l_nPopUpCount > 0 ) || ( l_hMenu != NULL ) )
	{
		return;
	}

	l_hMenu = LoadMenu(g_hInstance,MAKEINTRESOURCE(nMenu));
		
	if ( nIndex < 0 )
	{
		switch(GetUIStyle())
		{
			case UI_STYLE_KEYPAD:
					
				hSubMenu = GetSubMenu(l_hMenu,1);

				break;

			case UI_STYLE_FINGER:
					
				hSubMenu = GetSubMenu(l_hMenu,0);
					
				break;

			case UI_STYLE_PEN:
			case UI_STYLE_PC:

				hSubMenu = GetSubMenu(l_hMenu,2);

				break;
		}
	}
	else
	{
		hSubMenu = GetSubMenu(l_hMenu,nIndex);
	}

	if ( hSubMenu != NULL )
	{
		DWORD dwLocation;

		switch(GetUIStyle())
		{
			case UI_STYLE_KEYPAD:
					
				keybd_event(VK_DOWN,0,0,0);
				keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);

				break;

			case UI_STYLE_FINGER:
					
				keybd_event(VK_DOWN,0,0,0);
				keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					
				break;

			case UI_STYLE_PEN:
			case UI_STYLE_PC:

				break;
		}

		l_nPopUpCount++;

		if ( y == -1 )
		{
			RECT rect;

			SystemParametersInfo(SPI_GETWORKAREA,0,(PVOID)&rect,0);

			y = rect.bottom - g_nMenuHeight;

			if ( x == -1 )
			{
				x = rect.right;

				dwLocation = TPM_RIGHTALIGN|TPM_BOTTOMALIGN|TPM_RETURNCMD;
			}
			else
			{
				dwLocation = TPM_LEFTALIGN|TPM_BOTTOMALIGN|TPM_RETURNCMD;
			}
		}
		else
		{
			dwLocation = TPM_LEFTALIGN|TPM_TOPALIGN|TPM_RETURNCMD;
		}

		dwId = TrackPopupMenuEx(hSubMenu,dwLocation,x,y,g_hwnd,NULL);

		l_nPopUpCount--;
	}

	if ( l_hMenu != NULL )
	{
		DestroyMenu(l_hMenu);

		l_hMenu = NULL;
	}

	SetForegroundWindow(g_hwnd);
	SetFocus(g_hwnd);
	UpdateWindow(g_hwnd);

	if ( dwId )
	{
		SendMessage(hwnd,WM_COMMAND,dwId,0L);
	}
}


BOOL EnterDialog(void)
{
	// If there is a child window
	if ( g_hwndChild != NULL )
	{
		// Hide the child window
		ShowWindow(g_hwndChild,SW_HIDE);
	}

	// If there is a current dialog
	if ( g_hdlg != NULL )
	{
		// Hide current dialog
		ShowWindow(g_hdlg,SW_HIDE);
	}

	// If there is not room for one more saved dialog
	if ( g_nNextDlg >= MAX_DLGS )
	{
		// Failure
		return(FALSE);
	}

	g_SaveDlgs[g_nNextDlg].hwnd = g_hdlg;

	g_nNextDlg++;

	g_SaveDlgs[g_nNextDlg].hwnd = NULL;
	g_SaveDlgs[g_nNextDlg].bDone = FALSE;
	g_SaveDlgs[g_nNextDlg].hFont = NULL;

	g_nFontScaleFactor = 0;

	SendMessage(g_hwnd,UM_ENTERDLG,0,0L);

	return(TRUE);
}


BOOL ExitDialog(void)
{
	// End the current dialog
	DestroyWindow(g_hdlg);

	if ( !l_bResizing )
	{
		// Indicate dialog is done
		SignalDialogDone();
	}

	// Indicate no dialog in progress
	g_hdlg = NULL;

	// If there was a previous dialog
	if ( g_nNextDlg > 1 )
	{
		// Pick up the exiting dialog's font, if any
		HFONT hFont = g_SaveDlgs[g_nNextDlg].hFont;

		DEBUG_HERE;
		
		// If the exiting dialog has a font
		if ( hFont != NULL )
		{
			// Mark it as not having a font
			g_SaveDlgs[g_nNextDlg].hFont = NULL;

			// Delete the font
			DeleteObject(hFont);
		}

		// Backup to previous dialog
		--g_nNextDlg;

		// Make it the new current dialog
		g_hdlg = g_SaveDlgs[g_nNextDlg].hwnd;
	}

	SendMessage(g_hwnd,UM_EXITDLG,0,0L);

	if ( g_hdlg != NULL )
	{
		if ( !l_bResizing )
		{
			// If there is a child window
			if ( g_hwndChild != NULL )
			{
				// Hide the child window
				ShowWindow(g_hwndChild,SW_HIDE);
			}
			
			// If there is a new dialog
			if ( g_hdlg != NULL )
			{
				// Show the new current dialog
				ShowWindow(g_hdlg,SW_SHOW);
		
				// Set the focus to the new current dialog
				SetFocus(g_hdlg);

				if ( HasDialogSizeChanged() )
				{
					// Indicate we need to resize the new dialog
					PostMessage(g_hwnd,WM_SETTINGCHANGE,SPI_SETWORKAREA,0);

					// If there is a child window
					if ( g_hwndChild != NULL )
					{
						ShowWindow(g_hwndChild,SW_HIDE);
					}
				}
				else
				{
					UpdateWindow(g_hdlg);
				}
			}
		}
	}
	else
	{
		DEBUG_HERE;
		
		--g_nNextDlg;

		g_hdlg = NULL;

		if ( !l_bResizing )
		{
			// If there is a child window
			if ( g_hwndChild )
			{
			// Show the child window

				ShowWindow(g_hwndChild,SW_SHOW);

				// Set the focus to the child window
				SetFocus(g_hwndChild);

#ifdef WIN32_PLATFORM_POCKETPC
				g_bForceResize = TRUE;
#endif
				
				// Indicate we may need to resize the child window
				PostMessage(g_hwnd,WM_SETTINGCHANGE,SPI_SETWORKAREA,0);
			}
			else
			{
				// If there is a main window
				if ( g_hwnd )
				{
					// Set the focus to the main window
					SetFocus(g_hwnd);

					UpdateWindow(g_hwnd);
				}
			}
		}
	}

	return(!l_bResizing);
}


LPTSTR ChooseDialog(LPDIALOG_CHOICE lpDialogChoices)
{
	RECT rect;
	LPDIALOG_CHOICE lpc;

	GetChildArea(&rect);
	rect = rect;

	for(lpc=lpDialogChoices;TRUE;lpc++)
	{
		if ( ( lpc->dwScreenType == g_dwScreenType ) ||
			 ( lpc->dwScreenType == ST_UNKNOWN ) )
		{
			if ( ( lpc->lpbCondition == NULL ) ||
				 *lpc->lpbCondition )
			{
				if ( lpc->dwUIStyle != UI_STYLE_DEFAULT )
				{
					g_dwUIStyle = lpc->dwUIStyle;
				}

				return(MAKEINTRESOURCE(lpc->nDialogId));
			}
		}
	}

	return(NULL);
}


//----------------------------------------------------------------------------
// WaitUntilDialogDone
//----------------------------------------------------------------------------

void WaitUntilDialogDone(void)
{
	int nDialogIndex = g_nNextDlg;

	if ( ( nDialogIndex >= 1 ) &&
		 ( g_hdlg != NULL ) )
	{
		MSG msg;

		while ( !g_SaveDlgs[nDialogIndex].bDone &&
				GetMessage(&msg,NULL,0,0) )
		{
			if ( g_hdlg != NULL )
			{
				if ( !g_bSkipAccel &&
					 TranslateAccelerator(g_hdlg,g_hAccel,(LPMSG)&msg) )
				{
					continue;
				}

				if ( IsDialogMessage(g_hdlg,&msg) )
				{
					continue;
				}
			}

			if ( !g_bSkipAccel &&
				 TranslateAccelerator(g_hwnd,g_hAccel,(LPMSG)&msg) )
			{
				continue;
			}

			// Translate the message if necessary
			TranslateMessage((LPMSG)&msg);

			// Dispatch the message to the appropriate handler
			DispatchMessage((LPMSG)&msg);
		}
	}
}


//----------------------------------------------------------------------------
// WaitUntilDialogDoneSkip
//----------------------------------------------------------------------------

void WaitUntilDialogDoneSkip(BOOL bSkipAccel)
{
	BOOL bSaveSkipAccel;
	
	bSaveSkipAccel = g_bSkipAccel;
	g_bSkipAccel = bSkipAccel;

	WaitUntilDialogDone();

	g_bSkipAccel = bSaveSkipAccel;
}


//----------------------------------------------------------------------------
// SignalDialogDone
//----------------------------------------------------------------------------

void SignalDialogDone(void)
{
	// If there is a dialog in progress
	if ( g_nNextDlg >= 0 )
	{
		// Mark it done
		g_SaveDlgs[g_nNextDlg].bDone = TRUE;
	}
}


//----------------------------------------------------------------------------
// HasDialogSizeChanged
//----------------------------------------------------------------------------

BOOL HasDialogSizeChanged(void)
{
	// If there is no dialog in progress
	if ( g_nNextDlg < 1 )
	{
		// Then it hasn't changed
		return(FALSE);
	}

	// If we have inadequate info to determine
	if ( g_SaveDlgs[g_nNextDlg].lpDialogChoices == NULL )
	{
		// Then it hasn't changed
		return(FALSE);
	}

	// Find out new best dialog choice
	l_lpTemplateName = ChooseDialog(g_SaveDlgs[g_nNextDlg].lpDialogChoices);

	// If it is the same as the last time
	if ( l_lpTemplateName == g_SaveDlgs[g_nNextDlg].lpTemplateName )
	{
		// Then it hasn't changed
		return(FALSE);
	}

	// It has changed
	return(TRUE);
}


//----------------------------------------------------------------------------
// IsDialogResizing
//----------------------------------------------------------------------------

BOOL IsDialogResizing(void)
{
	return(l_bResizing);
}


//----------------------------------------------------------------------------
// ResizeDialog
//----------------------------------------------------------------------------

BOOL ResizeDialog(LPVOID lpvDialogSaveInfo)
{
	HWND hdlg;

	SetCursor(LoadCursor(NULL,IDC_WAIT));

	if ( !HasDialogSizeChanged() )
	{
		SetCursor(NULL);

		return(FALSE);
	}

	l_bResizing = TRUE;
	
	SendMessage(g_hdlg,WM_COMMAND,IDCANCEL,0);

	EnterDialog();

	g_SaveDlgs[g_nNextDlg].lpTemplateName = l_lpTemplateName;
	g_SaveDlgs[g_nNextDlg].lpvDialogSaveInfo = lpvDialogSaveInfo;

	hdlg = CreateDialogParam(g_SaveDlgs[g_nNextDlg].hInstance,
							 g_SaveDlgs[g_nNextDlg].lpTemplateName,
							 g_SaveDlgs[g_nNextDlg].hWndParent,
							 g_SaveDlgs[g_nNextDlg].lpDialogFunc,
							 g_SaveDlgs[g_nNextDlg].dwInitParam);

	l_bResizing = FALSE;

	if ( hdlg == NULL )
	{
		LastError(TEXT("ResizeDialog"));
	}

	g_hdlg = hdlg;

	SetCursor(NULL);

	return(TRUE);
}


//----------------------------------------------------------------------------
// GetDialogSaveInfo
//----------------------------------------------------------------------------

LPVOID GetDialogSaveInfo(void)
{
	LPVOID lpvDialogSaveInfo;

	if ( g_nNextDlg < 1 )
	{
		return(NULL);
	}

	lpvDialogSaveInfo = g_SaveDlgs[g_nNextDlg].lpvDialogSaveInfo;

	g_SaveDlgs[g_nNextDlg].lpvDialogSaveInfo = NULL;
	
	return(lpvDialogSaveInfo);
}


#if 0
//----------------------------------------------------------------------------
// CreateNewDialog
//----------------------------------------------------------------------------

HWND CreateNewDialog(HINSTANCE hInstance,
					 LPTSTR lpTemplateName,
					 HWND hWndParent,
					 DLGPROC lpDialogFunc,
					 LPARAM dwInitParam)
{
	HWND hdlg;

	EnterDialog();

	g_SaveDlgs[g_nNextDlg].hInstance = hInstance;
	g_SaveDlgs[g_nNextDlg].lpDialogChoices = NULL;
	g_SaveDlgs[g_nNextDlg].hWndParent = hWndParent;
	g_SaveDlgs[g_nNextDlg].lpDialogFunc = lpDialogFunc;
	g_SaveDlgs[g_nNextDlg].dwInitParam = dwInitParam;

	g_SaveDlgs[g_nNextDlg].lpTemplateName = lpTemplateName;
	g_SaveDlgs[g_nNextDlg].lpvDialogSaveInfo = NULL;

	hdlg = CreateDialogParam(hInstance,
							 lpTemplateName,
							 hWndParent,
							 lpDialogFunc,
							 dwInitParam);

	if ( hdlg == NULL )
	{
		LastError(TEXT("CreateNewDialog"));
	}

	return(hdlg);
}
#endif


//----------------------------------------------------------------------------
// CreateChosenDialog
//----------------------------------------------------------------------------

HWND CreateChosenDialog(HINSTANCE hInstance,
						LPDIALOG_CHOICE lpDialogChoices,
						HWND hWndParent,
						DLGPROC lpDialogFunc,
						LPARAM dwInitParam)
{
	HWND hdlg;
	LPTSTR lpTemplateName;

	EnterDialog();

	g_SaveDlgs[g_nNextDlg].hInstance = hInstance;
	g_SaveDlgs[g_nNextDlg].lpDialogChoices = lpDialogChoices;
	g_SaveDlgs[g_nNextDlg].hWndParent = hWndParent;
	g_SaveDlgs[g_nNextDlg].lpDialogFunc = lpDialogFunc;
	g_SaveDlgs[g_nNextDlg].dwInitParam = dwInitParam;

	lpTemplateName = ChooseDialog(lpDialogChoices);

	g_SaveDlgs[g_nNextDlg].lpTemplateName = lpTemplateName;
	g_SaveDlgs[g_nNextDlg].lpvDialogSaveInfo = NULL;

	hdlg = CreateDialogParam(hInstance,
							 lpTemplateName,
							 hWndParent,
							 lpDialogFunc,
							 dwInitParam);

	if ( hdlg == NULL )
	{
		LastError(TEXT("CreateChosenDialog"));
	}

	return(hdlg);
}



void GetRegistrySetting(LPINT lpnResult,
						LPBOOL lpbDone,
						LPINT lpnValue,
						LPCTSTR lpszSetting)
{
	DWORD dwResult;
	HKEY hKey;
	DWORD dwType;
	DWORD dwValueLen;
	int nResult = -1;

	if ( *lpbDone )
	{
		nResult = *lpnValue;
	}
	else
	{
		dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
								TEXT("Software\\Symbol\\Settings"),
								0,
								KEY_ENUMERATE_SUB_KEYS
								  | KEY_EXECUTE
								  | KEY_QUERY_VALUE,
								&hKey);

		if ( dwResult == ERROR_SUCCESS )
		{
            dwValueLen = sizeof(DWORD);

			dwResult = RegQueryValueEx(hKey,
									   lpszSetting,
									   NULL,
									   &dwType,
                                       (LPBYTE)&nResult,	
									   &dwValueLen);
				
			RegCloseKey(hKey);
		}
	}

	if ( nResult != -1 )
	{
		*lpbDone = TRUE;

		*lpnValue = nResult;
	
		*lpnResult = nResult;
	}
}


static BOOL l_bFontHeightDone = FALSE;
static int l_nFontHeight; 
static BOOL l_bFontWeightDone = FALSE;
static int l_nFontWeight; 


HFONT GetNewFont(BOOL bForceNew)
{
	static HFONT hNewFont = NULL;
	static LOGFONT logfont;
	static int nLastFontHeight;
	static int nLastFontWeight;

	RECT rect;

	GetChildArea(&rect);
	
	GetRegistrySetting(&logfont.lfHeight,
					   &l_bFontHeightDone,
					   &l_nFontHeight,
					   TEXT("FontHeight"));

	GetRegistrySetting(&logfont.lfWeight,
					   &l_bFontWeightDone,
					   &l_nFontWeight,
					   TEXT("FontWeight"));

	if ( l_bFontHeightDone )
	{
		g_nFontHeight = l_nFontHeight;
	}

	if ( g_nFontScaleFactor > 0 )
	{
		g_nFontHeight = g_nFontHeight * g_nFontScaleFactor / 100;
	}

	if ( l_bFontWeightDone )
	{
		g_nFontWeight = l_nFontWeight;
	}

	if ( bForceNew )
	{
		hNewFont = NULL;
	}
	
	if ( hNewFont != NULL )
	{
		if ( ( g_nFontHeight != nLastFontHeight ) ||
			 ( g_nFontWeight != nLastFontWeight ) )
		{
			DeleteObject(hNewFont);

			hNewFont = NULL;
		}
	}
	
	if ( hNewFont == NULL )
	{
		logfont.lfHeight = g_nFontHeight;
		logfont.lfWidth = 0;
		logfont.lfOrientation = logfont.lfEscapement = 0;
		logfont.lfWeight = g_nFontWeight;
		logfont.lfItalic = FALSE;
		logfont.lfUnderline = FALSE;
		logfont.lfStrikeOut = FALSE;
		logfont.lfCharSet = DEFAULT_CHARSET;
		logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
		logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
		logfont.lfQuality = DEFAULT_QUALITY;
		logfont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;  //// FF_DONTCARE;
		logfont.lfFaceName[0] = TEXT('\0');

		hNewFont = CreateFontIndirect(&logfont);

		nLastFontHeight = g_nFontHeight;
		nLastFontWeight = g_nFontWeight;
	}

	return(hNewFont);
}


void MaxDialog(HWND hdlg)
{
#ifdef _WIN32_WCE

	RECT rect;

	GetChildArea(&rect);

	SetWindowPos(hdlg,
				 HWND_TOP,
				 rect.left,
				 rect.top,
				 rect.right-rect.left,
				 rect.bottom-rect.top,
				 SWP_DRAWFRAME|
					SWP_FRAMECHANGED|
					SWP_NOACTIVATE|
					SWP_NOZORDER|
					SWP_NOOWNERZORDER);


	UpdateWindow(hdlg);

#endif

	g_hdlg = hdlg;
}


HFONT SetNewWindowFont(HWND hWnd,
					   int nFontScaleFactor)
{
	HFONT hNewFont;

	if ( nFontScaleFactor > 0 )
	{
		g_nFontScaleFactor = nFontScaleFactor;
	}

	hNewFont = GetNewFont(FALSE);

	if ( hNewFont != NULL )
	{
		SetWindowFont(hWnd,hNewFont,FALSE);
	}

	return(hNewFont);
}


HFONT SetNewDialogFont(HWND hWnd,
					   int nFontScaleFactor)
{
	HFONT hNewFont;

	if ( nFontScaleFactor > 0 )
	{
		g_nFontScaleFactor = nFontScaleFactor;
	}

	hNewFont = GetNewFont(TRUE);

	if ( hNewFont != NULL )
	{
		SetWindowFont(hWnd,hNewFont,FALSE);
	}

	g_SaveDlgs[g_nNextDlg].hFont = hNewFont;

	return(hNewFont);
}


static BOOL GetNextFileName(LPTSTR lpszNext,
							LPTSTR lpszBase)
{
	TCHAR szFileSpec[MAX_PATH];
	TCHAR szFileAndExt[MAX_PATH];
	WIN32_FIND_DATA FindData;
	HANDLE hHandle;
	TCHAR szExt[MAX_PATH];
	TCHAR szPath[MAX_PATH];
	TCHAR szFile[MAX_PATH];

	// If there is no supplied name
	if ( lpszNext[0] == TEXT('\0') )
	{
		// Construct wildcard for default path
		TEXTCPY(lpszNext,lpszBase);
		TEXTCAT(lpszNext,TEXT("*.wav"));
	}

	// Decompose the name
	GetExtension(szExt,MAX_PATH,lpszNext);
	GetFileName(lpszNext,szFile);
	GetPathName(lpszNext,szPath);

	// Assume we will return same file
	TEXTCPY(lpszNext,szPath);
	TEXTCAT(lpszNext,szFile);
	TEXTCAT(lpszNext,szExt);

	// Setup wildcard file spec with path and same extension
	TEXTCPY(szFileSpec,szPath);
	TEXTCAT(szFileSpec,TEXT("*"));
	TEXTCAT(szFileSpec,szExt);

	// Setup filename and extension for compares
	TEXTCPY(szFileAndExt,szFile);
	TEXTCAT(szFileAndExt,szExt);
	
	// Find first file that matches wildcard
	hHandle = FindFirstFile(szFileSpec,&FindData);
	
	// If there are none
	if ( hHandle == INVALID_HANDLE_VALUE )
	{
		// Indicate we found none
		TEXTCPY(lpszNext,TEXT(""));
		return(FALSE);
	}

	// Assume we will return this file
	TEXTCPY(lpszNext,szPath);
	TEXTCAT(lpszNext,FindData.cFileName);

	// Advance until we find the one we want the next of
	while ( TEXTICMP(szFileAndExt,FindData.cFileName) != 0 )
	{
		// Advance, If there are no more
		if ( FindNextFile(hHandle,&FindData) == FALSE )
		{
			return(FALSE);
		}
	}

	// Advance to the next of the one we found, if no more
	if ( FindNextFile(hHandle,&FindData) == FALSE )
	{
		// Indicate we found none
		TEXTCPY(lpszNext,TEXT(""));
		return(FALSE);
	}

	// Return the one found
	TEXTCPY(lpszNext,szPath);
	TEXTCAT(lpszNext,FindData.cFileName);

	// Indicate we found one
	return(TRUE);
}


static BOOL GetPrevFileName(LPTSTR lpszPrev,
							LPTSTR lpszBase)
{
	TCHAR szFileSpec[MAX_PATH];
	TCHAR szFileAndExt[MAX_PATH];
	WIN32_FIND_DATA FindData;
	HANDLE hHandle;
	BOOL bFindLast = FALSE;
	TCHAR szExt[MAX_PATH];
	TCHAR szPath[MAX_PATH];
	TCHAR szFile[MAX_PATH];

	// If there is no supplied name
	if ( lpszPrev[0] == TEXT('\0') )
	{
		// Construct wildcard for default path
		TEXTCPY(lpszPrev,lpszBase);
		TEXTCAT(lpszPrev,TEXT("*.wav"));

		// Set flag to advance to last file
		bFindLast = TRUE;
	}

	// Decompose the name
	GetExtension(szExt,MAX_PATH,lpszPrev);
	GetFileName(lpszPrev,szFile);
	GetPathName(lpszPrev,szPath);

	// Assume we will return same file
	TEXTCPY(lpszPrev,szPath);
	TEXTCAT(lpszPrev,szFile);
	TEXTCPY(lpszPrev,szExt);

	// Setup wildcard file spec with path and same extension
	TEXTCPY(szFileSpec,szPath);
	TEXTCAT(szFileSpec,TEXT("*"));
	TEXTCAT(szFileSpec,szExt);

	// Setup filename and extension for compares
	TEXTCPY(szFileAndExt,szFile);
	TEXTCAT(szFileAndExt,szExt);
	
	// Find first file that matches wildcard
	hHandle = FindFirstFile(szFileSpec,&FindData);
	
	// If there are none
	if ( hHandle == INVALID_HANDLE_VALUE )
	{
		// Indicate we found none
		TEXTCPY(lpszPrev,TEXT(""));
		return(FALSE);
	}

	// If the first file is the one we want the previous of
	if ( TEXTICMP(szFileAndExt,FindData.cFileName) == 0 )
	{
		// Indicate we found none
		TEXTCPY(lpszPrev,TEXT(""));
		return(FALSE);
	}
	
	// Advance until we find the one we want the previous of
	while ( bFindLast ||
			( TEXTICMP(szFileAndExt,FindData.cFileName) != 0 ) )
	{
		// Assume we will return this file
		TEXTCPY(lpszPrev,szPath);
		TEXTCAT(lpszPrev,FindData.cFileName);

		// If there are no more
		if ( FindNextFile(hHandle,&FindData) == FALSE )
		{
			// Indicate we found none
//			TEXTCPY(lpszPrev,TEXT(""));
			return(FALSE);
		}
	}

	// Indicate we found one
	return(TRUE);
}


BOOL IncFileName(HWND hwnd,
				 int nInc,
				 LPTSTR lpszBase)
{
	TCHAR szWaveFile[MAX_PATH];

	GetWindowText(hwnd,szWaveFile,TEXTSIZEOF(szWaveFile));

	while ( nInc != 0 )
	{
		if ( nInc < 0 )
		{
			if ( !GetPrevFileName(szWaveFile,lpszBase) )
			{
				break;
			}

			nInc++;
		}
		else
		{
			if ( !GetNextFileName(szWaveFile,lpszBase) )
			{
				break;
			}

			nInc--;
		}
	}

	SetWindowText(hwnd,szWaveFile);
	SetFocus(hwnd);
	Edit_SetSel(hwnd,0,-1);

	return(szWaveFile[0] != 0);
}



DWORD GetUIStyle(void)
{
	RECT rect;

	GetChildArea(&rect);

	return(g_dwUIStyle);
}
					

BOOL CloseApplication(void)
{
	HWND hSaveDlg = NULL;

	g_bSystemIsExiting = TRUE;

	while ( ( g_hdlg != NULL ) && ( g_hdlg != hSaveDlg ) )
	{
		hSaveDlg = g_hdlg;
					
		// Send an OK to the dialog
		SendMessage(g_hdlg, WM_COMMAND, IDOK, 0L);
	}
			
	if ( ( g_hdlg == NULL ) && ( g_hwnd != NULL ) )
	{
		SendMessage(g_hwnd,UM_TERM,0,0L);
	
		DestroyWindow(g_hwnd);
					
		return(TRUE);
	}

	return(FALSE);
}


DWORD GetMainWindowStyle(void)
{
	DWORD dwStyle = 0;

	switch(GetUIStyle())
	{
		case UI_STYLE_KEYPAD:
		case UI_STYLE_FINGER:
			dwStyle |= WS_CAPTION;
			break;

		case UI_STYLE_PEN:
#ifndef WIN32_PLATFORM_POCKETPC
			dwStyle |= WS_CAPTION;
#endif
			break;

		case UI_STYLE_PC:
			dwStyle |= WS_CAPTION | WS_SYSMENU;
			break;
	}

	return(dwStyle);
}


static BOOL l_bMenuCreated = FALSE;
					

BOOL CreateMenuBar(SHORT nMenuId)
{
#ifdef _WIN32_WCE

	BOOL bFlag;

	if ( g_hwnd == NULL )
	{
		return(FALSE);
	}
	
#ifdef WIN32_PLATFORM_POCKETPC

	if ( !l_bMenuCreated )
	{
		SHMENUBARINFO mbi = {0};

		memset(&mbi, 0, sizeof(SHMENUBARINFO));
		mbi.cbSize     = sizeof(SHMENUBARINFO);
		mbi.hInstRes = g_hInstance;
		mbi.hwndParent = g_hwnd;
		mbi.nToolBarId = nMenuId;
		mbi.nBmpId     = 0;
		mbi.cBmpImages = 0;	

		if ( WasCalledBy() )
		{
			mbi.dwFlags = SHCMBF_EMPTYBAR || SHCMBF_HIDDEN;
		}

		bFlag = SHCreateMenuBar(&mbi);

		if ( bFlag )
		{
			g_nMenuHeight = POCKETPC_MENU_HEIGHT;

			l_bMenuCreated = TRUE;
		}
		else
		{
			DWORD dwError = GetLastError();

			SetRealMessageBox(TRUE);

			LastError(TEXT("CreateMenuBar"));

			CloseApplication();

			return(FALSE);
		}
	}

#else

	switch(GetUIStyle())
	{
		case UI_STYLE_PEN:

			if ( g_hwndCB != NULL )
			{
				return(FALSE);
			}

			// Create the command bar
			g_hwndCB = CommandBar_Create(g_hInstance,g_hwnd,1);

			// Insert the main menu into it
			bFlag = CommandBar_InsertMenubar(g_hwndCB,g_hInstance,nMenuId,0);

			// Add the close button
			bFlag = CommandBar_AddAdornments(g_hwndCB,0,0);

			// Show the command bar
			bFlag =CommandBar_Show(g_hwndCB,TRUE);

			// Save its height for later use
			g_nCommandHeight = CommandBar_Height(g_hwndCB) + 1;

			// Resize main window to account for added command bar
			Std_OnSize(g_hwnd,0,-1,-1);

			break;
	}

#endif

#endif

	return(TRUE);
}


BOOL DestroyMenuBar(void)
{
#ifdef _WIN32_WCE
	// Get rid of any existing command band/bar
	if ( g_hwndCB != NULL )
	{
		CommandBar_Destroy(g_hwndCB);

		g_hwndCB = NULL;

		g_nCommandHeight = 0;

		return(TRUE);
	}
#endif

	return(FALSE);
}


void SetDoneButtonState(HWND hwnd,
						BOOL bState)
{

#ifdef WIN32_PLATFORM_POCKETPC

	DWORD dwStyle;

	dwStyle = (DWORD)GetWindowLong(hwnd,GWL_STYLE);

	if ( bState )
	{
		dwStyle |= WS_EX_CAPTIONOKBTN;
	}
	else
	{
		dwStyle &= ~WS_EX_CAPTIONOKBTN;
	}

	SetWindowLong(hwnd,GWL_STYLE,(LONG)dwStyle);

	if ( bState )
	{
		SHDoneButton(hwnd,SHDB_SHOW);
	}
	else
	{
		SHDoneButton(hwnd,SHDB_HIDE);
	}

#endif

}


void SetWindowTitle(HWND hwnd,
					LPTSTR lpszTitle)
{
	TCHAR szTitle[MAX_PATH];

	if ( lpszTitle == NULL )
	{
		GetWindowText(hwnd,szTitle,TEXTSIZEOF(szTitle));

		lpszTitle = szTitle;
	}

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)
	SHSetNavBarText(hwnd,lpszTitle);
#endif
				
	SetWindowText(hwnd,lpszTitle);

	UpdateWindow(hwnd);
}


LPTSTR GetSystemPath(LPTSTR lpszPath,
					 DWORD dwSize)
{
	DWORD dwResult;
	HKEY hKey;
	DWORD dwType;
	DWORD dwValueLen;

	dwResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							TEXT("Loader"),
							0,
							KEY_ENUMERATE_SUB_KEYS
							  | KEY_EXECUTE
							  | KEY_QUERY_VALUE,
							&hKey);

	if ( dwResult != ERROR_SUCCESS )
	{
		return(NULL);
	}

	dwValueLen = dwSize;

	dwResult = RegQueryValueEx(hKey,
							   TEXT("SystemPath"),
							   NULL,
							   &dwType,
							   (LPBYTE)lpszPath,
							   &dwValueLen);
				
	RegCloseKey(hKey);

	if ( dwResult != ERROR_SUCCESS )
	{
		return(NULL);
	}

	return(lpszPath);
}


#define WINDOWS_DIR TEXT("\\Windows\\")


void LocateSystemExecutable(LPTSTR lpszPathAndFile)
{
	TCHAR szPath[MAX_PATH];
	TCHAR szExtension[MAX_PATH];
	TCHAR szFile[MAX_PATH];
	TCHAR szSystemPath[MAX_PATH];
	LPTSTR lpszSystemPath;

	// If we have a path, then its not a system executable
	if ( GetPathName(lpszPathAndFile,szPath) )
	{
		return;
	}

	// If it has no extension, then its not an executable
	if ( !GetExtension(szExtension,
					   TEXTSIZEOF(szExtension),
					   lpszPathAndFile) )
	{
		return;
	}

	// If the extension is not .EXE, then its not an executable
	if ( TEXTICMP(szExtension,TEXT(".exe")) != 0 )
	{
		return;
	}

	// Add \Windows path
	TEXTCPY(szFile,WINDOWS_DIR);
	TEXTCAT(szFile,lpszPathAndFile);

	// Is it in \Windows
	if ( IsFileAccessable(szFile) )
	{
		// Yes, use that as the path
		TEXTCPY(lpszPathAndFile,szFile);

		return;
	}

	// Get system path list from the registry
	lpszSystemPath = GetSystemPath(szSystemPath,sizeof(szSystemPath));
	
	// Scan through path items
	while ( ( lpszSystemPath != NULL ) &&
		    ( *lpszSystemPath != TEXT('\0') ) )
	{
		// Try this path item
		TEXTCPY(szFile,lpszSystemPath);
		TEXTCAT(szFile,lpszPathAndFile);

		// If we can now access the file
		if ( IsFileAccessable(szFile) )
		{
			// Then this is the proper path to the file
			TEXTCPY(lpszPathAndFile,szFile);

			return;
		}

		// Try next path
		lpszSystemPath = lpszSystemPath + TEXTLEN(lpszSystemPath) + 1;
	}
}


BOOL WaitUntilFileAccessable(LPTSTR lpszPath,
							 DWORD dwTimeOut)
{
	DWORD dwStartTime;
	BOOL bAccessable;

	dwStartTime = GetTickCount();

	while ( TRUE )
	{
		bAccessable = IsFileAccessable(lpszPath);

		if ( bAccessable )
		{
			break;
		}

		if ( ( GetTickCount() - dwStartTime ) >= dwTimeOut )
		{
			break;
		}

		Sleep(100);
	}

	return(bAccessable);
}

	
BOOL IsFileAccessable(LPTSTR lpszPath)
{

#ifdef _WIN32_WCE

#ifndef _WIN32_WCE_EMULATION

	{
		HANDLE hFile;
		WIN32_FIND_DATA find_data;

		hFile = FindFirstFile(lpszPath,&find_data);
	
		// Find the file, if we cannot
		if ( hFile == INVALID_HANDLE_VALUE )
		{
			// Return file is not accessable
			return(FALSE);
		}

		// Close the find handle
		FindClose(hFile);
	
		// See if the file is in ROM, if it is
		if ( ( find_data.dwFileAttributes & FILE_ATTRIBUTE_INROM ) ||
			 ( find_data.dwFileAttributes & FILE_ATTRIBUTE_ROMMODULE ) )
		{
			// Return file is accessable
			return(TRUE);
		}

		// Try and open the file
		hFile = CreateFile(lpszPath,
						   GENERIC_READ,
						   0,
						   NULL,
						   OPEN_EXISTING,
						   FILE_ATTRIBUTE_NORMAL,
						   NULL);

		// If we cannot open the file
		if ( hFile == INVALID_HANDLE_VALUE )
		{
			// Return file is not accessable
			return(FALSE);
		}

		// Close the file handle
		CloseHandle(hFile);
	}

#endif

#endif

	// Return file is accessable
	return(TRUE);

}


void UnhideWindow(HWND hwnd)
{
#ifdef WIN32_PLATFORM_PSPC

	ShowWindow(hwnd,SW_SHOW);

#endif

	SetForegroundWindow(hwnd);

#ifndef SYMBOL_OEM_SCAN_KIT
	SetDoneButtonState(hwnd,TRUE);
#endif
}


void HideWindow(HWND hwnd)
{
#ifndef SYMBOL_OEM_SCAN_KIT
	SetDoneButtonState(hwnd,FALSE);
#endif

#ifdef WIN32_PLATFORM_PSPC

	ShowWindow(hwnd,SW_HIDE);

#else

	{
		HWND hWndFg;
		HWND hWndTop;

		hWndFg = GetForegroundWindow();
		
		SetWindowPos(hwnd,
					 HWND_BOTTOM,
					 0,0,
					 0,0,
					 SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE);

		hWndTop = GetWindow(hwnd,GW_HWNDFIRST);

		if ( hwnd == hWndTop )
		{
			hWndTop = GetWindow(hwnd,GW_HWNDNEXT);
		}

		if ( hwnd == hWndFg )
		{
			SetForegroundWindow(hWndTop);
		}
	}

#endif
}


//----------------------------------------------------------------------------
// IsWindowHidden
//----------------------------------------------------------------------------

BOOL IsWindowHidden(HWND hwnd)
{
#ifdef WIN32_PLATFORM_PSPC

	return(!IsWindowVisible(hwnd));

#else

	return(GetForegroundWindow()!=hwnd);

#endif
}


//----------------------------------------------------------------------------
// GetDefaultWavFilePath
//----------------------------------------------------------------------------

#define WAV_FILE_PATH		TEXT("\\Application\\User\\Wav")

// look at HKCU\Software\Symbol\Settings\WavDirectory
static void GetDefaultWavFilePath(LPTSTR lpszPath)
{
    DWORD dwRetCode;
    HKEY hKey;
    DWORD dwLen = 0;
			
	dwRetCode = RegOpenKeyEx(HKEY_CURRENT_USER,TEXT("Software\\Symbol\\Settings"),0,KEY_READ, &hKey);
       
    if (dwRetCode == ERROR_SUCCESS)
    {
        DWORD dwType;
		TCHAR szData[MAX_PATH];
		
		// look for RegDirectory
        dwLen = sizeof(szData);
        dwRetCode = RegQueryValueEx(hKey,TEXT("WavDirectory"),NULL,&dwType,(LPBYTE)szData,&dwLen);
		TEXTCPY(lpszPath,szData);
	}

    // no entry found or error, use the default
    if ((dwRetCode != ERROR_SUCCESS) || (dwLen == 2))
    {
        TEXTCPY(lpszPath,WAV_FILE_PATH);
    }

	RegCloseKey(hKey);
	
}


//----------------------------------------------------------------------------
// GetWavePath
//----------------------------------------------------------------------------

void GetWavePath(LPTSTR lpszPath)
{
#ifdef _WIN32_WCE

#ifdef _WIN32_WCE_EMULATION

	TEXTCPY(lpszPath,TEXT("\\"));

#else
	GetDefaultWavFilePath(lpszPath);

	// append slash to the path
	TEXTCAT(lpszPath,TEXT("\\"));

#endif

#else

	TEXTCPY(lpszPath,TEXT("c:\\"));

#endif
}


BOOL GetFeedbackInfo(LPFEEDBACK_INFO lpFeedbackInfo)
{
#if defined(WIN32_PLATFORM_7500)

	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMinimum,2500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMaximum,3500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyDefault,2670);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyStep,10);

	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMaximum,5000);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationDefault,500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationStep,100);

#elif defined(WIN32_PLATFORM_8100)

	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMinimum,2500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMaximum,3500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyDefault,2670);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyStep,10);

	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMaximum,5000);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationDefault,500);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationStep,100);

#elif defined(WIN32_PLATFORM_2700) || defined(WIN32_PLATFORM_2800)

	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyDefault,1);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyStep,0);

	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationDefault,1);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationStep,0);

#else

	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyDefault,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperFrequencyStep,0);

	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationDefault,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperDurationStep,0);

#endif

#if defined(SYMBOL_AUDIO_API)

	{
	    int nBeeperVolumeMinimum	= 0;
	    int nBeeperVolumeMaximum	= 0;
	    int nBeeperVolumeDefault	= 0;
	    int nBeeperVolumeStep		= 0;

		DWORD dwBeeperVolumeLevels;
		DWORD dwBeeperVolume;

	    // If we can get the volume levels the system supports
	    if ( AUDIO_GetBeeperVolumeLevels(&dwBeeperVolumeLevels) == E_AUD_SUCCESS )
		{
			// Use it to set the maximum
			nBeeperVolumeMaximum = dwBeeperVolumeLevels - 1;

			// Set a non-zero step
			nBeeperVolumeStep = 1;
		}

	    // If we can get the volume levels the system supports
		if ( AUDIO_GetBeeperVolume(&dwBeeperVolume) == E_AUD_SUCCESS )
		{
			// Use it to set the default
			nBeeperVolumeDefault = dwBeeperVolume;
		}

		SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeMinimum,0);
		SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeMaximum,0);
		SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeDefault,0);
		SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeStep,0);
	}

#else

	SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeDefault,0);
	SI_SET_FIELD(lpFeedbackInfo,nBeeperVolumeStep,0);

#endif

	SI_SET_FIELD(lpFeedbackInfo,nLedDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nLedDurationMaximum,5000);
	SI_SET_FIELD(lpFeedbackInfo,nLedDurationDefault,500);
	SI_SET_FIELD(lpFeedbackInfo,nLedDurationStep,500);

	SI_SET_FIELD(lpFeedbackInfo,nLedCountMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nLedCountMaximum,20);
	SI_SET_FIELD(lpFeedbackInfo,nLedCountDefault,4);
	SI_SET_FIELD(lpFeedbackInfo,nLedCountStep,2);

#if defined(WIN32_PLATFORM_7500)

	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationMaximum,5000);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationDefault,500);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationStep,500);

#else

	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationMinimum,0);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationMaximum,0);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationDefault,0);
	SI_SET_FIELD(lpFeedbackInfo,nVibratorDurationStep,0);

#endif

	return(TRUE);
}


void PlayDefaultBeep(void)
{

#if defined(SYMBOL_AUDIO_API)

	FEEDBACK_INFO FeedbackInfo;
	int nBeeperFrequencyDefault;
	int nBeeperDurationDefault;
	AUDIO_INFO AudioInfo;

	nBeeperFrequencyDefault		= 2670;
	nBeeperDurationDefault		= 500;
	
	SI_INIT(&FeedbackInfo);

	if ( GetFeedbackInfo(&FeedbackInfo) )
	{
		SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyDefault,nBeeperFrequencyDefault);
		SI_GET_FIELD(&FeedbackInfo,nBeeperDurationDefault,nBeeperDurationDefault);

		AudioInfo.szSound[0] = 0;
		AudioInfo.dwFrequency = nBeeperFrequencyDefault;
		AudioInfo.dwDuration = nBeeperDurationDefault;

		AUDIO_PlayBeeper(&AudioInfo);
	}

#else

	MessageBeep(MB_OK);

#endif
}


void PlayBeeper(DWORD dwFrequency,
				DWORD dwDuration)
{
	AUDIO_INFO AudioInfo;

#if defined(SYMBOL_AUDIO_API)

	AudioInfo.szSound[0] = 0;
	AudioInfo.dwFrequency = dwFrequency;
	AudioInfo.dwDuration = dwDuration;

	AUDIO_PlayBeeper(&AudioInfo);

#endif
}

