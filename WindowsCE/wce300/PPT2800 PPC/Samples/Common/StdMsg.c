
//--------------------------------------------------------------------
// FILENAME:			StdMsg.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Standard messages and and error handling
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <winsock.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdScrns.h"

#include "..\common\StdMsg.h"


//#define _STDDEBUG
//#define _USE_RETAILMSG
#include "..\common\StdDebug.h"

#include "..\msg\resource.h"


#ifdef WIN32_PLATFORM_PC
#pragma comment(lib,"ws2_32.lib")
#endif


//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------


int nMsgStr1Id = IDC_MSGSTR1;
int nMsgStr2Id = IDC_MSGSTR2;


DIALOG_CHOICE MsgDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_MSG_PC_STD,		NULL },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_MSG_HPC_STD,	NULL },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_MSG_PPC_STD,	NULL },
	{ ST_PPC_SIP,	IDD_MSG_PPC_SIP,	NULL },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_MSG_7200_STD,	NULL },
	{ ST_7200_TSK,	IDD_MSG_7200_TSK,	NULL },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_MSG_7500_STD,	NULL },
	{ ST_7500_TSK,	IDD_MSG_7500_TSK,	NULL },
#endif
	{ ST_UNKNOWN,	IDD_MSG_7500_TSK,	NULL }
};


DIALOG_CHOICE YesNoDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_YESNO_PC_STD,	NULL },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_YESNO_HPC_STD,	NULL },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_YESNO_PPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_YESNO_PPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_YESNO_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_YESNO_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_YESNO_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_YESNO_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_YESNO_7500_TSK,	NULL,	UI_STYLE_DEFAULT }
};


static LPTSTR lpszMsg1;
static LPTSTR lpszMsg2;
static LPTSTR lpszTitle;

static TCHAR pszMessage[] = TEXT("Message");
static TCHAR pszError[] = TEXT("Error");
static TCHAR pszQuestion[] = TEXT("Question");
static TCHAR pszNull[] = TEXT("");

static int nResult = 0;

static BOOL bUseMsgBoxQueryDone = FALSE;

static BOOL bUseRealMessageBox = FALSE;


//----------------------------------------------------------------------------
// Local functions
//----------------------------------------------------------------------------


void SetRealMessageBox(BOOL bFlag)
{
	bUseRealMessageBox = bFlag;
}


//----------------------------------------------------------------------------
// UseRealMessageBox
//----------------------------------------------------------------------------

BOOL UseRealMessageBox(void)
{
	if ( !bUseMsgBoxQueryDone )
	{
		DWORD dwResult;
		HKEY hKey;
		DWORD dwType;
		TCHAR szValue[MAX_PATH];
		DWORD dwValueLen;
		
		dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
								TEXT("Software\\Symbol\\Settings"),
								0,
								KEY_ENUMERATE_SUB_KEYS
								  | KEY_EXECUTE
								  | KEY_QUERY_VALUE,
								&hKey);

		if ( dwResult == ERROR_SUCCESS )
		{
			dwValueLen = TEXTSIZEOF(szValue);

			dwResult = RegQueryValueEx(hKey,
									   TEXT("UseRealMessageBox"),
									   NULL,
									   &dwType,
									   (LPBYTE)szValue,
									   &dwValueLen);

			if ( ( dwResult == ERROR_SUCCESS ) &&
				 ( dwType == REG_SZ ) )
			{
				bUseRealMessageBox = TEXTTOL(szValue);
			}
			
			RegCloseKey(hKey);
		}
		
		bUseMsgBoxQueryDone = TRUE;
	}

	return(bUseRealMessageBox);
}


//----------------------------------------------------------------------------
// MsgDlgProc
//----------------------------------------------------------------------------

static LRESULT CALLBACK MsgDlgProc(HWND hwnd,
                   UINT uMsg,
                   WPARAM wParam,
                   LPARAM lParam)
{
    switch(uMsg)
    {
		case UM_RESIZE:

			ResizeDialog(NULL);

			break;

        case WM_INITDIALOG:
			
			{
				MaxDialog(hwnd);

				SetWindowText(hwnd,lpszTitle);
            
				SetDlgItemText(hwnd,nMsgStr1Id,lpszMsg1);
            
				SetDlgItemText(hwnd,nMsgStr2Id,lpszMsg2);

				SetFocus(GetDlgItem(hwnd,nMsgStr1Id));
			}
            
			return(FALSE);

        case WM_COMMAND:
			
			nResult = FALSE;
            
			switch ( LOWORD(wParam) )
            {
                case IDYES:
                case IDOK:

					nResult = TRUE;

					// Fall through

				case IDCANCEL:
				case IDNO:
					
					// Exit the dialog
					ExitDialog();
                    
					break;
            }
            
			return (TRUE);
    }
    
	return (FALSE);
}


//----------------------------------------------------------------------------
// MyError
//----------------------------------------------------------------------------

void MyError(LPTSTR pszMsg1,LPTSTR pszMsg2)
{
	lpszMsg1 = pszMsg1;
	lpszMsg2 = pszMsg2;
	lpszTitle = pszError;

	g_bInMsg = TRUE;

	if ( lpszMsg1 == NULL )
	{
		lpszMsg1 = pszNull;
	}

	if ( lpszMsg2 == NULL )
	{
		lpszMsg2 = pszNull;
	}

	DEBUG_HERE;

	DEBUG_STRING(TRUE,lpszMsg1);
	DEBUG_STRING(TRUE,TEXT("\r\n"));
	DEBUG_STRING(TRUE,lpszMsg2);
	DEBUG_STRING(TRUE,TEXT("\r\n"));
	
	if ( UseRealMessageBox() || ( g_hwnd == NULL ) )
	{
		DEBUG_HERE;

		MessageBox(NULL,
				   lpszMsg2,
				   lpszMsg1,
				   MB_OK
					| MB_ICONEXCLAMATION
				  );

		DEBUG_HERE;
	}
	else
	{
		DEBUG_HERE;

		ShowWindow(g_hwnd,SW_SHOW);

		g_hdlg = CreateChosenDialog(g_hInstance,
									MsgDialogChoices,
									g_hwnd,
									MsgDlgProc,
									0);
		
		DEBUG_HERE;

		WaitUntilDialogDoneSkip(TRUE);

		DEBUG_HERE;
	}

	g_bInMsg = FALSE;
}


//----------------------------------------------------------------------------
// ReportError
//----------------------------------------------------------------------------

void ReportError(LPTSTR pszFunction,
				 DWORD dwError)
{
	TCHAR szErr[256];
	TEXTSPRINTF(szErr,TEXT("Error %.8lX"),dwError);
	MyError(pszFunction,szErr);
}


//----------------------------------------------------------------------------
// LastError
//----------------------------------------------------------------------------

void LastError(LPTSTR pszFunction)
{
	ReportError(pszFunction,GetLastError());
}


//----------------------------------------------------------------------------
// MyMessage
//----------------------------------------------------------------------------

void MyMessage(LPTSTR pszMsg1,LPTSTR pszMsg2)
{
	lpszMsg1 = pszMsg1;
	lpszMsg2 = pszMsg2;
	lpszTitle = pszMessage;

	g_bInMsg = TRUE;

	if ( lpszMsg1 == NULL )
	{
		lpszMsg1 = pszNull;
	}

	if ( lpszMsg2 == NULL )
	{
		lpszMsg2 = pszNull;
	}

	if ( UseRealMessageBox() || ( g_hwnd == NULL ) )
	{
		MessageBox(NULL,
				   lpszMsg2,
				   lpszMsg1,
				   MB_OK
				     | MB_ICONINFORMATION
					 | MB_APPLMODAL
					 | MB_SETFOREGROUND
				  );
	}
	else
	{
		ShowWindow(g_hwnd,SW_SHOW);

		g_hdlg = CreateChosenDialog(g_hInstance,
									MsgDialogChoices,
									g_hwnd,
									MsgDlgProc,
									0);
		
		WaitUntilDialogDoneSkip(TRUE);
	}

	if ( g_hwndChild )
	{
		SetFocus(g_hwndChild);
	}
	else if ( g_hwnd )
	{
		SetFocus(g_hwnd);
	}

	g_bInMsg = FALSE;
}


//----------------------------------------------------------------------------
// MyMessageThenPost
//----------------------------------------------------------------------------

void MyMessageThenPost(LPTSTR lpszStr1,
					   LPTSTR lpszStr2,
					   HWND hwnd,
					   UINT umsg,
					   WPARAM wparam,
					   LPARAM lparam)
{
	MyMessage(lpszStr1,lpszStr2);

	PostMessage(hwnd,umsg,wparam,lparam);
}


//----------------------------------------------------------------------------
// MyYesNo
//----------------------------------------------------------------------------

BOOL MyYesNo(LPTSTR pszMsg1,
			 LPTSTR pszMsg2)
{
	lpszMsg1 = pszMsg1;
	lpszMsg2 = pszMsg2;
	lpszTitle = pszQuestion;

	g_bInMsg = TRUE;

	if ( lpszMsg1 == NULL )
	{
		lpszMsg1 = pszNull;
	}

	if ( lpszMsg2 == NULL )
	{
		lpszMsg2 = pszNull;
	}

	if ( UseRealMessageBox() || ( g_hwnd == NULL ) )
	{
		nResult = MessageBox(NULL,
							 lpszMsg2,
							 lpszMsg1,
							 MB_YESNO
							   | MB_ICONQUESTION
							);
	}
	else
	{
		ShowWindow(g_hwnd,SW_SHOW);

		g_hdlg = CreateChosenDialog(g_hInstance,
									YesNoDialogChoices,
									g_hwnd,
									MsgDlgProc,
									0);

		WaitUntilDialogDoneSkip(TRUE);
	}


	if ( g_hwndChild )
	{
		SetFocus(g_hwndChild);
	}
	else if ( g_hwnd )
	{
		SetFocus(g_hwnd);
	}

	g_bInMsg = FALSE;

	return(nResult);
}


//----------------------------------------------------------------------------
// WSAError
//----------------------------------------------------------------------------

void WSAError(LPTSTR pszFunction)
{
	DWORD err = WSAGetLastError();
	TCHAR szErr[256];
	
	TEXTSPRINTF(szErr,TEXT("Error %lu"),err);
	
	MyError(pszFunction,szErr);
}



//----------------------------------------------------------------------------
//
//  FUNCTION:   ErrorHandlerEx(WORD, LPSTR)
//
//  PURPOSE:    Calls GetLastError() and uses FormatMessage() to display the
//              textual information of the error code along with the file
//              and line number.
//
//  PARAMETERS:
//      wLine    - line number where the error occured
//      lpszFile - file where the error occured
//
//  RETURN VALUE:
//      none
//
//  COMMENTS:
//      This function has a macro ErrorHandler() which handles filling in
//      the line number and file name where the error occured.  ErrorHandler()
//      is always used instead of calling this function directly.
//
//----------------------------------------------------------------------------

void ErrorHandlerEx( WORD wLine, LPTSTR lpszFile )
{
   TCHAR  szBuffer[256];
   TCHAR  szBuffer2[256];

   TEXTSPRINTF(szBuffer, TEXT("An %ld error occured."), GetLastError());
   TEXTSPRINTF(szBuffer2, TEXT("Generic, Line=%d, File=%s"), wLine, lpszFile);

   MyError(szBuffer,szBuffer2);

   return;
}


