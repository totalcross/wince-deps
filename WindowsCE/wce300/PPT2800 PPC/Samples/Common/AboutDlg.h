
//--------------------------------------------------------------------
// FILENAME:        AboutDlg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     about dialogs
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef ABOUTDLG_H_

#define ABOUTDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif

// intention left blank

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef ABOUTDLG_H_  */
