

//--------------------------------------------------------------------
// FILENAME:			StdFuncs.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for StdFuncs.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDFUNCS_H_

#define STDFUNCS_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include <StrucInf.h>


//--------------------------------------------------------------------
// Conditionals
//--------------------------------------------------------------------

#ifndef SYMBOL_OEM_SCAN_KIT
	#define _PLATFORM_SPECIFIC
#endif

// Screen type inclusion flags

#ifdef _PLATFORM_SPECIFIC

	#ifdef WIN32_PLATFORM_PSPC
		#define _ST_PPC
	#endif

	#ifdef WIN32_PLATFORM_8100
		#define _ST_PPC
	#endif

	#ifdef WIN32_PLATFORM_7000
		#define _ST_7200
		#define _ST_7500
	#endif

	#ifdef WIN32_PLATFORM_HPCPRO
		#define _ST_HPC
	#endif

	#ifdef WIN32_PLATFORM_EMERALD
		#define _ST_7500
	#endif

	#ifdef WIN32_PLATFORM_GAZOO
		#define _ST_7200
	#endif

	#ifndef _WIN32_WCE
		#define _ST_PC
	#endif

#else

	#define _ST_PC
	#define _ST_HPC
	#define _ST_PPC
	#define _ST_7200
	#define _ST_7500

#endif


//--------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------


// Screen types
enum tagSCREEN_TYPES
{

#ifdef _ST_PC
	ST_PC_STD,
#endif

#ifdef _ST_HPC
	ST_HPC_STD,
#endif

#ifdef _ST_PPC
	ST_PPC_STD,		ST_PPC_SIP,	
#endif

#ifdef _ST_7200
	ST_7200_STD,	ST_7200_TSK,
#endif

#ifdef _ST_7500
	ST_7500_STD,	ST_7500_TSK,
#endif

	ST_UNKNOWN=-1

};


//--------------------------------------------------------------------
// Type definitions
//--------------------------------------------------------------------


// Screen type lookup table entry
typedef struct tagSCREEN_TYPE
{
	DWORD dwScreenType;

	DWORD dwUIStyle;

	int nWidth;
	int nHeight;

	int nFontHeight;
	int nFontWeight;

} SCREEN_TYPE;

typedef SCREEN_TYPE FAR * LPSCREEN_TYPE;


// Dialog selector table entry
typedef struct tagDIALOG_CHOICE
{
	// Type of screen this dialog is used for
	DWORD dwScreenType;
	
	// Resource ID for this dialog
	int nDialogId;

	// Pointer to condition flag required for this dialog
	LPBOOL lpbCondition;

	// UI style to be used by the dialog
	DWORD dwUIStyle;

} DIALOG_CHOICE;

typedef DIALOG_CHOICE FAR *LPDIALOG_CHOICE;


typedef struct tagFEEDBACK_INFO
{
	STRUCT_INFO	StructInfo;

	int nBeeperFrequencyMinimum;
	int nBeeperFrequencyMaximum;
	int nBeeperFrequencyDefault;
	int nBeeperFrequencyStep;

	int nBeeperDurationMinimum;
	int nBeeperDurationMaximum;
	int nBeeperDurationDefault;
	int nBeeperDurationStep;

	int nBeeperVolumeMinimum;
	int nBeeperVolumeMaximum;
	int nBeeperVolumeDefault;
	int nBeeperVolumeStep;

	int nLedDurationMinimum;
	int nLedDurationMaximum;
	int nLedDurationDefault;
	int nLedDurationStep;

	int nLedCountMinimum;
	int nLedCountMaximum;
	int nLedCountDefault;
	int nLedCountStep;

	int nVibratorDurationMinimum;
	int nVibratorDurationMaximum;
	int nVibratorDurationDefault;
	int nVibratorDurationStep;

} FEEDBACK_INFO;

typedef FEEDBACK_INFO FAR * LPFEEDBACK_INFO;


//--------------------------------------------------------------------
// Externs
//--------------------------------------------------------------------

extern SCREEN_TYPE ScreenTypeTable[];


//--------------------------------------------------------------------
// Exported functions
//--------------------------------------------------------------------

void InvokePopUpMenu(HWND hwnd,
					 int nMenu,
					 int nIndex,
					 LONG x,
					 LONG y);

BOOL IsInPopUpMenu(void);

void CancelPopUpMenu(void);


BOOL EnterDialog(void);


BOOL ExitDialog(void);


BOOL IsInDialog(void);


LPTSTR ChooseDialog(LPDIALOG_CHOICE lpDialogChoice);


void SizeRect(HWND g_hdlg);


void WaitUntilDialogDone(void);


void WaitUntilDialogDoneSkip(BOOL bSkipAccel);


void SignalDialogDone(void);


BOOL ResizeDialog(LPVOID lpvDialogSaveInfo);


BOOL IsDialogResizing(void);


BOOL HasDialogSizeChanged(void);


LPVOID GetDialogSaveInfo(void);


HWND CreateChosenDialog(HINSTANCE hInstance,
						LPDIALOG_CHOICE lpDialogChoices,
						HWND hWndParent,
						DLGPROC lpDialogFunc,
						LPARAM dwInitParam);


void GetRegistrySetting(LPINT lpnResult,
						LPBOOL lpbDone,
						LPINT lpnValue,
						LPCTSTR lpszSetting);


HFONT GetNewFont(BOOL bForceNew);


void MaxDialog(HWND hwnd);


HFONT SetNewWindowFont(HWND hWnd,
					   int nFontScaleFactor);


HFONT SetNewDialogFont(HWND hWnd,
					   int nFontScaleFactor);


BOOL IncFileName(HWND hwnd,
				 int nInc,
				 LPTSTR lpszBase);


DWORD GetUIStyle(void);


BOOL CloseApplication(void);


DWORD GetMainWindowStyle(void);


BOOL CreateMenuBar(SHORT nMenuId);


BOOL DestroyMenuBar(void);


void SetDoneButtonState(HWND hwnd,BOOL bState);


void SetWindowTitle(HWND hwnd,
					LPTSTR lpszTitle);


LPTSTR GetSystemPath(LPTSTR lpszPath,DWORD dwSize);


void LocateSystemExecutable(LPTSTR lpszPath);


BOOL IsFileAccessable(LPTSTR lpszPath);

BOOL WaitUntilFileAccessable(LPTSTR lpszPath,DWORD dwTimeOut);


void HideWindow(HWND hwnd);
void UnhideWindow(HWND hwnd);


BOOL IsWindowHidden(HWND hwnd);


void GetWavePath(LPTSTR lpszPath);


BOOL GetFeedbackInfo(LPFEEDBACK_INFO lpBeeperInfo);


void PlayDefaultBeep(void);


void PlayBeeper(DWORD dwFrequency,
				DWORD dwDuration);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDFUNCS_H_	*/

