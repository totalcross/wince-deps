
//--------------------------------------------------------------------
// FILENAME:			StdStrng.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for string handling functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef STDSTRNG_H_

#define STDSTRNG_H_

#ifdef __cplusplus
extern "C"
{
#endif



#ifdef UNICODE
LPTSTR __lpszStopString;
#define TEXTCPY wcscpy
#define TEXTCMP wcscmp
#define TEXTCAT wcscat
#define TEXTICMP wcsicmp
#define TEXTNCPY wcsncpy
#define TEXTNCMP wcsncmp
#define TEXTNICMP wcsnicmp
#define TEXTLEN wcslen
#define TEXTCHR wcschr
#define TEXTUPR _wcsupr
#define TEXTSPRINTF wsprintf
#define TEXTSCANF swscanf
#define TEXTSTR wcsstr
#define TEXTTOL _wtol
#define LTOTEXT _ltow
#define ULTOTEXT _ultow
#define ISALPHA _istalpha
#define TOUPPER towupper
#define TEXTTOF(a) _tcstod(a,&__lpszStopString)
#define ZEROMEM(ptr,size) memset(ptr,0,size)
#else
#define TEXTCPY strcpy
#define TEXTCMP strcmp
#define TEXTCAT strcat
#define TEXTICMP stricmp
#define TEXTNCPY strncpy
#define TEXTNCMP strncmp
#define TEXTNICMP strnicmp
#define TEXTLEN strlen
#define TEXTCHR strchr
#define TEXTUPR strupr
#define TEXTSPRINTF wsprintf
#define TEXTSCANF sscanf
#define TEXTSTR strstr
#define TEXTTOL atol
#define LTOTEXT _ltoa
#define ULTOTEXT _ultoa
#define TEXTTOF atof
#define ISALPHA isalpha
#define TOUPPER toupper
#define ZEROMEM(ptr,size) memset(ptr,0,size)
#endif


#ifdef _WIN32_WCE
typedef LPWSTR LPCMDLINE;
#else
typedef LPSTR LPCMDLINE;
#endif

#define TEXTSIZEOF(a) (sizeof(a)/sizeof(TCHAR))


void Replace(LPTSTR lpszBase,
			 LPTSTR lpszPat,
			 LPTSTR lpszRep);

void ReplaceEnv(LPTSTR lpszStr);

BOOL GetExtension(LPTSTR lpszExtension,
				  int nSize,
				  LPTSTR lpszFileSpec);

LPTSTR RemoveLeadingSpaces(LPTSTR lpszStr);

BOOL GetFileName(LPTSTR lpszPath,
				 LPTSTR lpszName);

void ProcessSlashes(LPTSTR lpszStr);

void ConvertAsciiToUnicode(LPTSTR lpszDest,
						   LPSTR lpszSrc);

void ConvertUnicodeToAscii(LPSTR lpszDest,
						   LPTSTR lpszSrc);

BOOL GetPathName(LPTSTR lpszPathAndFile,
				 LPTSTR lpszDest);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef STDSTRNG_H_  */

