
//--------------------------------------------------------------------
// FILENAME:			StdScrns.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for StdScrns.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef STDSCRNS_H_

#define STDSCRNS_H_


#ifdef __cplusplus
extern "C"
{
#endif

#if defined(WIN32_PLATFORM_PSPC)

    #include <sipapi.h>

#else

typedef struct _tagSipInfo {
    DWORD  cbSize;
    DWORD  fdwFlags;
    RECT   rcVisibleDesktop;
    RECT   rcSipRect;    
    DWORD  dwImDataSize;
    LPVOID pvImData;
} SIPINFO;

#endif

#define SPI_SETSIPINFO          224
#define SPI_GETSIPINFO          225
#define SPI_SETCURRENTIM        226

#define HANDLE_WM_SETTINGCHANGE(hwnd, wParam, lParam, fn) \
    ((fn)(hwnd,wParam), 0L)

void SetDeltaHeight(int nHeight);


int GetDeltaHeight(void);


void GetWorkArea(RECT *pr);

void GetChildArea(RECT *pr);

BOOL WasCalledBy(void);

void SetCalledBy(BOOL bFlag);

BOOL IsSipUp(void);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDSCRNS_H_	*/
