//--------------------------------------------------------------------
// FILENAME:			main.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains exported functions for main window procedure.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef STDMAIN_H_

#define STDMAIN_H_

#ifdef __cplusplus
extern "C"
{
#endif


LRESULT CALLBACK MainWndProc(HWND,UINT,WPARAM,LPARAM);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef STDMAIN_H_   */

