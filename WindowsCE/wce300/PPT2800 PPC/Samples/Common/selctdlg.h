//--------------------------------------------------------------------
// FILENAME:			SelctDlg.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for SelctDlg.C
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SELCTDLG_H_

#define SELCTDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "..\common\StdFuncs.h"


typedef LRESULT (*LPLVDLGCB)(HWND hwnd,
							 UINT uMsg,
							 WPARAM wParam,
							 LPARAM lParam,
							 HWND hWndList);


extern DIALOG_CHOICE SelectDialogChoices[];


int Select_GetSelectedItem(HWND hWndList);


LRESULT CALLBACK Select_DlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam);


BOOL Select_Dialog(LPLVDLGCB lpDlgCallBack);


BOOL Select_GetOK(void);


void Select_SetOK(BOOL bOKed);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SELCTDLG_H_   */

