
//--------------------------------------------------------------------
// FILENAME:			StdExec.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Standard functions for executing programs
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#ifndef _WIN32_WCE
#include <SHLOBJ.H>
#endif

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"

#include "..\common\StdExec.h"


#define DO_RUN_FILES


#ifndef _WIN32_WCE


//----------------------------------------------------------------------------
// SHGetShortcutTarget
//----------------------------------------------------------------------------

BOOL SHGetShortcutTarget(LPTSTR lpszShortCut,
						 LPTSTR lpszTarget,
						 int cbMax) 
{
    HRESULT hres; 
    IShellLink* psl; 
    TCHAR szGotPath[MAX_PATH]; 
    TCHAR szDescription[MAX_PATH]; 
    WIN32_FIND_DATA wfd; 
 
    // Assume failure
	*lpszTarget = 0; 
 
    // Initialize COM
	CoInitialize(NULL);
	
	// Get a pointer to the IShellLink interface. 
    hres = CoCreateInstance(&CLSID_ShellLink,
							NULL,
							CLSCTX_INPROC_SERVER,
							&IID_IShellLink,
							&psl); 

    // If we got the interface pointer
	if ( SUCCEEDED(hres) )
	{ 
        IPersistFile* ppf; 
 
        // Get a pointer to the IPersistFile interface. 
        hres = psl->lpVtbl->QueryInterface(psl,
										   &IID_IPersistFile, 
										   &ppf); 
        
		// If we got the interface pointer
		if ( SUCCEEDED(hres) )
		{ 

#ifdef UNICODE

			// Load the shortcut. 
            hres = ppf->lpVtbl->Load(ppf,lpszShortCut,STGM_READ); 
 
#else

            WORD wsz[MAX_PATH]; 
 
            // Convert the string to Unicode. 
            MultiByteToWideChar(CP_ACP,0,lpszShortCut,-1,wsz,MAX_PATH);

			// Load the shortcut. 
            hres = ppf->lpVtbl->Load(ppf,wsz,STGM_READ); 
 
#endif
            
            // If we got the shortcut
			if ( SUCCEEDED(hres) )
			{ 
                // Resolve the link. 
                hres = psl->lpVtbl->Resolve(psl, g_hwnd, SLR_ANY_MATCH);

                // If we resolved the link
				if ( SUCCEEDED(hres) )
				{ 
                    // Get the path to the link target. 
                    hres = psl->lpVtbl->GetPath(psl,
												szGotPath, 
												MAX_PATH,
												(WIN32_FIND_DATA *)&wfd,
												SLGP_SHORTPATH );

                    // If we could not get the path
					if ( !SUCCEEDED(hres) )
					{
						// Report an error
						LastError(TEXT("No shortcut path"));
					}
 
                    // Get the description of the target. 
                    hres = psl->lpVtbl->GetDescription(psl,
													   szDescription,
													   MAX_PATH); 
                    
					// If we could not get the description
					if ( !SUCCEEDED(hres) )
					{
						// Report an error
						LastError(TEXT("No shortcut description"));
					}
                    
					// Copy path into target
					TEXTCPY(lpszTarget,szGotPath); 
                } 
            }
			
			// Release the pointer to the IPersistFile interface. 
			ppf->lpVtbl->Release(ppf); 
        } 

		// Release the pointer to the IShellLink interface. 
		psl->lpVtbl->Release(psl); 
    }
	
    // Uninitalize COM
	CoUninitialize();

    // Return result
	return(SUCCEEDED(hres)); 
}


#endif


#ifdef DO_RUN_FILES

static BOOL ReadLine(HANDLE hFile,
					 LPTSTR lpszLine,
					 DWORD dwSize,
					 LPDWORD lpdwBytesRead)
{
	*lpdwBytesRead = 0;
	
	while ( dwSize > 0 )
	{
		DWORD dwBytesRead;

		if ( ReadFile(hFile,lpszLine,1,&dwBytesRead,FALSE) &&
			 ( dwBytesRead == 1 ) )
		{
			*lpszLine &= 0xFF;

			if ( ( *lpszLine == TEXT('\n') ) ||
				 ( *lpszLine == TEXT('\r') ) )
			{
				if ( (*lpdwBytesRead) > 0 )
				{
					break;
				}
				else
				{
					continue;
				}
			}
			
			(*lpdwBytesRead)++;
			lpszLine++;
			dwSize--;
		}
		else
		{
			*lpszLine = TEXT('\0');

			return(FALSE);
		}
	}

	*lpszLine = TEXT('\0');

	return(TRUE);
}


#endif


//----------------------------------------------------------------------------
// Execute
//----------------------------------------------------------------------------

BOOL Execute(LPTSTR lpszName,
			 LPTSTR lpszCmd)
{
	TCHAR szExtension[MAX_PATH];
	TCHAR szExtKey[MAX_PATH];
	TCHAR szCommand[MAX_PATH];
	TCHAR szFileSpec[MAX_PATH];

#ifdef _WIN32_WCE

	TCHAR szProgFile[MAX_PATH];

#endif

#ifdef DO_RUN_FILES

	TCHAR szFile[MAX_PATH];
	TCHAR szCmd[MAX_PATH];

#endif

	LPTSTR pszProgram;
	LPTSTR pszCmdLine;

	DWORD retCode;
	HKEY hKey;
	DWORD dwType;
	DWORD dwSize;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	// Make a copy of the program path
	TEXTCPY(szFileSpec,lpszName);
	
	// Extract extension from program path
	if ( !GetExtension(szExtension,
					   TEXTSIZEOF(szExtension),
					   szFileSpec) )
	{
		// Inform user of failure due to no extension
		MyError(TEXT("File has no extension"),
			    szFileSpec);

		// Indicate failure to execute
		return(FALSE);
	}

#if defined(WIN32_PLATFORM_HPCPRO) || defined(WIN32_PLATFORM_PSPC)

		// While it is a shortcut
		while ( TEXTICMP(szExtension,TEXT(".lnk")) == 0 )
		{
			int len;

			// Get the file the shortcut refers to
			if ( ! SHGetShortcutTarget(szFileSpec,
									   szCommand,
									   TEXTSIZEOF(szCommand)) )
			{
				// Report the error
				LastError(TEXT("No shortcut file"));

				// Indicate failure to execute
				return(FALSE);
			}

			// If the command has quotes around it
			if ( szCommand[0] == TEXT('"') )
			{
				// Get length of command
				len = TEXTLEN(szCommand);

				// Copy content of quotes as new file path
				TEXTNCPY(szFileSpec,szCommand+1,len-2);

				// Ensure string is null terminated
				szFileSpec[len-2] = TEXT('\0');
			}
			else
			{
				// No quotes, copy entire command as new file path
				TEXTCPY(szFileSpec,szCommand);
			}

			// Extract extension from new file path
			if ( !GetExtension(szExtension,
							   TEXTSIZEOF(szExtension),
							   szFileSpec) )
			{
				// Inform user of failure due to no extension
				MyError(TEXT("File has no extension"),
						szFileSpec);

				// Indicate failure to execute
				return(FALSE);
			}
		}
	

#endif

#ifdef DO_RUN_FILES

	while ( TEXTICMP(szExtension,TEXT(".run")) == 0 )
	{
		HANDLE hFile;

		hFile = CreateFile(szFileSpec,
						   GENERIC_READ,
						   0,
						   NULL,
						   OPEN_EXISTING,
						   FILE_ATTRIBUTE_NORMAL,
						   NULL);

		if ( hFile == INVALID_HANDLE_VALUE )
		{
			LastError(TEXT("Cannot open .RUN file"));

			// Indicate failure to execute
			return(FALSE);
		}
		else
		{
			DWORD dwBytes;

			if ( !ReadLine(hFile,szFile,sizeof(szFile),&dwBytes) )
			{
				LastError(TEXT("Cannot read .RUN file"));

				// Indicate failure to execute
				return(FALSE);
			}
			
			ReadLine(hFile,szCmd,sizeof(szFile),&dwBytes);

			CloseHandle(hFile);

			lpszName = szFile;
			lpszCmd = szCmd;
			
			// Make a copy of the program path
			TEXTCPY(szFileSpec,lpszName);
			
			// Extract extension from program path
			if ( !GetExtension(szExtension,
							   TEXTSIZEOF(szExtension),
							   szFileSpec) )
			{
				// Inform user of failure due to no extension
				MyError(TEXT("File has no extension"),
						szFileSpec);

				// Indicate failure to execute
				return(FALSE);
			}
		}
	}

#endif
	
	// Open the key for the extension
	retCode = RegOpenKeyEx(HKEY_CLASSES_ROOT,
						   szExtension,
						   0,
						   KEY_EXECUTE,
						   &hKey);
	
	// If we were not able to open the key
	if ( retCode )
	{
		// Inform user of failure due to no registered extension
		MyError(TEXT("Extension not registered"),
			    szExtension);

		// Indicate failure to execute
		return(FALSE);
	}
	
	// Get size of the key value buffer
	dwSize = TEXTSIZEOF(szExtKey);

	// Get the value of the key
	retCode = RegQueryValueEx(hKey,
							  NULL,
							  NULL,
							  &dwType,
							  (LPSTR)szExtKey,
							  &dwSize);

	// Close the key
	RegCloseKey(hKey);
	
	// If we were not able to read the key value
	if ( retCode )
	{
		// Inform user of failure due to no registered association
		MyError(TEXT("Association not found"),
			    szExtension);

		// Indicate failure to execute
		return(FALSE);
	}
	
	// Append sub keys to get to command needed to execute
	TEXTCAT(szExtKey,TEXT("\\Shell\\Open\\Command"));

	// Open the key
	retCode = RegOpenKeyEx(HKEY_CLASSES_ROOT,
						   szExtKey,
						   0,
						   KEY_EXECUTE,
						   &hKey);
	
	// If we could not open the key
	if ( retCode )
	{
		// Inform user of failure due to no registered association
		MyError(TEXT("Association not registered"),
			    szExtKey);

		// Indicate failure to execute
		return(FALSE);
	}
	
	// Get size of the key value buffer
	dwSize = TEXTSIZEOF(szExtKey);

	// Get the value of the key
	retCode = RegQueryValueEx(hKey,
							  NULL,
							  NULL,
							  &dwType,
							  (LPSTR)szCommand,
							  &dwSize);

	// Close the key
	RegCloseKey(hKey);
	
	if ( retCode )
    {
		// Inform user of failure due to no registered application
		MyError(TEXT("Application not registered"),
			    szExtKey);

		// Indicate failure to execute
		return(FALSE);
	}
 
	// Insert file path if needed
	Replace(szCommand,TEXT("%1"),szFileSpec);

	// Insert command line if needed
	Replace(szCommand,TEXT("%*"),lpszCmd);

#ifndef _WIN32_WCE

	ReplaceEnv(szCommand);

#endif
	
	// Save command as program to run
	pszProgram = szCommand;

	// If the command contains quotes
	if ( pszProgram[0] == '"' )
	{
		// Set command line to begin after close quote
		pszCmdLine = TEXTCHR(++pszProgram,'"');
	}
	else
	{
		// Set command line to begin after first space
		pszCmdLine = TEXTCHR(pszProgram,' ');
	}

	// If there is no command line
	if ( pszCmdLine == NULL )
	{
		// Indicate failure to execute
		return(FALSE);
	}

#ifdef _WIN32_WCE

	// Split command into two parts, program and arguments
	*pszCmdLine++ = 0;
	if ( *pszCmdLine == TEXT(' ') )
	{
		pszCmdLine++;
	}

#else

	// Leave command as a single line with program and arguments
	*pszCmdLine++ = TEXT(' ');
	pszCmdLine = pszProgram;
	pszProgram = NULL;

#endif

	// Initialize return structure for create process
	ZEROMEM(&si,sizeof(si));
	si.cb = sizeof(si);

	if ( !IsFileAccessable(lpszName) )
	{
		// Report to user that the file is not ready
		LastError(TEXT("File not ready!"));

		return(FALSE);
	}

#ifdef _WIN32_WCE

	TEXTCPY(szProgFile,pszProgram);

	LocateSystemExecutable(szProgFile);
	
	if ( !IsFileAccessable(szProgFile) )
	{
		// Report to user that the application is not ready
		LastError(TEXT("Application not ready!"));

		return(TRUE);
	}

	pszProgram = szProgFile;

#endif
	
	// Create process for program, if failed
	if ( !CreateProcess(pszProgram,
						pszCmdLine,
						NULL,
						NULL,
						FALSE,
						0,
						NULL,
						NULL,
						&si,
						&pi) )
	{
		// Report to user a failure to execute
		LastError(TEXT("Cannot execute application"));

		// Indicate failure to execute
		return(FALSE);
	}

	// Close thread and process handles returned
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	// Return success in executing
	return(TRUE);
}

