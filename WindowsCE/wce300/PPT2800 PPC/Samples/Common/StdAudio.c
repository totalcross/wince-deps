
//--------------------------------------------------------------------
// FILENAME:            STDAUDIO.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains audio play and record handling functions
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"

#include "..\common\StdAudio.h"

#define DO_MM_PLAY

#ifdef DO_MM_PLAY


#define MAX_TIME 20


#define RIFF_CHUNK_ID      mmioFOURCC ('R', 'I', 'F', 'F')     /*Chunk ID RIFF  */
#define WAVE_CHUNK_ID      mmioFOURCC ('W', 'A', 'V', 'E')     /*Chunk ID WAVE  */
#define FMT_CHUNK_ID       mmioFOURCC ('f', 'm', 't', ' ')     /*Chunk ID fmt   */
#define DATA_CHUNK_ID      mmioFOURCC ('d', 'a', 't', 'a')     /*Chunk ID data  */



typedef struct tagWAVEFILEHEADER
{
    DWORD riff_ckID;            /* chunk id 'RIFF'            4	 bytes */
    DWORD ckSize;				/* chunk size                 4		"	*/
    DWORD wave_ckID;			/* wave chunk id 'WAVE'       4		"	*/
    DWORD fmt_ckID;				/* format chunk id 'fmt '     4		"	*/
    DWORD fmt_ckSize;			/* format chunk size          4 	"	*/
    WORD  formatTag;			/* format tag currently pcm   2 	"	*/
    WORD  nChannels;			/* number of channels         2 	"	*/
    DWORD nSamplesPerSec;		/* sample rate in hz          4 	"	*/
    DWORD nAvgBytesPerSec;		/* average bytes per second   4 	"	*/
    WORD  nBlockAlign;			/* number of bytes per sample 2 	"	*/
    WORD  nBitsPerSample;		/* number of bits in a sample 2 	"	*/
    DWORD data_ckID;			/* data chunk id 'data'       4 	"	*/
    DWORD data_ckSize;			/* length of data chunk       4 	"	*/
								/*			Complete Header = 44 bytes  */
} WAVEFILEHEADER ;


static WAVEFILEHEADER l_WavePlayFileHeader;
static WAVEFILEHEADER l_WaveRecordFileHeader;

static HWAVEOUT l_hWavePlay = NULL;
static HWAVEIN l_hWaveRecord = NULL;

static HANDLE l_hWavePlayFile = INVALID_HANDLE_VALUE;
static HANDLE l_hWaveRecordFile = INVALID_HANDLE_VALUE;

static WAVEHDR l_WavePlayHeader;
static WAVEHDR l_WaveRecordHeader;

static HANDLE l_hPlayThread = NULL;
static HANDLE l_hRecordThread = NULL;

static HANDLE l_hPlayStopEvent = NULL;
static HANDLE l_hRecordStopEvent = NULL;

static TCHAR l_szPlaySoundName[MAX_PATH];
static TCHAR l_szRecordSoundName[MAX_PATH];

static HWND l_hPlayWnd = NULL;
static HWND l_hRecordWnd = NULL;

static UINT l_uPlayMsg;
static UINT l_uRecordMsg;


static void InitializePlay(void)
{
	l_hWavePlayFile = INVALID_HANDLE_VALUE;

	l_hWavePlay = NULL;

	l_WavePlayHeader.lpData = NULL;
}


static void TerminatePlay(HWND l_hPlayWnd,
						  LPTSTR lpszMsg,
						  DWORD dwError)
{
	if (l_hWavePlayFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(l_hWavePlayFile);

		l_hWavePlayFile = INVALID_HANDLE_VALUE;
	}

	if ( l_hWavePlay != NULL )
	{
		waveOutClose(l_hWavePlay);

		l_hWavePlay = NULL;
	}

	if ( l_WavePlayHeader.lpData != NULL )
	{
		LocalFree(l_WavePlayHeader.lpData);
	
		l_WavePlayHeader.lpData = NULL;
	}

    if ( lpszMsg != NULL )
	{
		if ( l_hPlayWnd != NULL )
		{
			TCHAR szMsg[MAX_PATH];
			
			wsprintf(szMsg,TEXT("ERROR %d - %s"),dwError,lpszMsg);

			SendMessage(l_hPlayWnd,l_uPlayMsg,(WPARAM)AUDIO_ERROR,(LPARAM)szMsg);
		}
	}

	l_hPlayThread = NULL;

	if ( l_hPlayStopEvent != NULL )
	{
		CloseHandle(l_hPlayStopEvent);

		l_hPlayStopEvent = NULL;
	}
}


static void InitializePlayWaveFormat(WAVEFORMATEX *lpWaveFormat)
{
    lpWaveFormat->wFormatTag = WAVE_FORMAT_PCM;

    lpWaveFormat->nChannels = l_WavePlayFileHeader.nChannels;

    lpWaveFormat->nSamplesPerSec = l_WavePlayFileHeader.nSamplesPerSec;

    lpWaveFormat->nAvgBytesPerSec = 
		l_WavePlayFileHeader.nSamplesPerSec * 
			((l_WavePlayFileHeader.nChannels * l_WavePlayFileHeader.nBitsPerSample) / 8);
    
	lpWaveFormat->nBlockAlign = 
		(l_WavePlayFileHeader.nChannels * l_WavePlayFileHeader.nBitsPerSample) / 8;
    
	lpWaveFormat->wBitsPerSample = l_WavePlayFileHeader.nBitsPerSample;
	
	lpWaveFormat->cbSize = 0;	
}


void CALLBACK PlayCallBack(HWAVEOUT hwo,
						   UINT umsg,
						   DWORD dwInstance,
						   DWORD dwParam1,
						   DWORD dwParam2)
{
	switch(umsg)
	{
		case WOM_OPEN:

			if ( l_hPlayWnd != NULL )
			{
				PostMessage(l_hPlayWnd,l_uPlayMsg,AUDIO_PLAY,0L);
			}
			
			break;

		case WOM_DONE:

			if ( l_hPlayWnd != NULL )
			{
				PostMessage(l_hPlayWnd,l_uPlayMsg,AUDIO_PROGRESS,100L);
				PostMessage(l_hPlayWnd,l_uPlayMsg,AUDIO_DONE,0L);
			}
			
			break;
	}
}


static DWORD WINAPI PlayThread(LPVOID lpvParam)
{
    WAVEFORMATEX WaveFormat;
    DWORD mmRet;
	DWORD dwBytesRead;
	DWORD dwBufferSize;
	
	InitializePlay();

	l_hWavePlayFile = CreateFile(l_szPlaySoundName,
							 GENERIC_READ,
							 FILE_SHARE_READ,
							 NULL,
							 OPEN_EXISTING,
							 FILE_ATTRIBUTE_NORMAL,
							 NULL);

	if (l_hWavePlayFile == INVALID_HANDLE_VALUE)
	{
    	TerminatePlay(l_hPlayWnd,TEXT("Cannot open wave file"),GetLastError());

		return(FALSE);
	}

	if ( !ReadFile(l_hWavePlayFile,
				   &l_WavePlayFileHeader,
				   sizeof(WAVEFILEHEADER),
				   &dwBytesRead,
				   NULL) )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot read wave file"),GetLastError());

		return(FALSE);
	}

	if ( ( l_WavePlayFileHeader.riff_ckID  != RIFF_CHUNK_ID ) || 
		 ( l_WavePlayFileHeader.wave_ckID  != WAVE_CHUNK_ID ) )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Bad wave file"),GetLastError());

		return(FALSE);
	}

	InitializePlayWaveFormat(&WaveFormat);

	dwBufferSize = WaveFormat.nAvgBytesPerSec * MAX_TIME;

	l_WavePlayHeader.lpData = (LPSTR)LocalAlloc(LPTR,dwBufferSize);

	if ( l_WavePlayHeader.lpData == NULL )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Failed allocation"),GetLastError());

		return(FALSE);
	}
	
	if ( !ReadFile(l_hWavePlayFile,
				   l_WavePlayHeader.lpData,
				   dwBufferSize,
				   &dwBytesRead,
				   NULL) )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot read wave file"),GetLastError());

		return(FALSE);
	}

	if ( l_WavePlayHeader.lpData == NULL)
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot read wave header"),GetLastError());

		return(FALSE);
	}

    if ( l_hPlayWnd == NULL )
	{
		mmRet = waveOutOpen(&l_hWavePlay,
							WAVE_MAPPER,
							&WaveFormat,
							(ULONG)l_hPlayWnd,
							0,
							WAVE_MAPPED);
	}
	else
	{
		mmRet = waveOutOpen(&l_hWavePlay,
							WAVE_MAPPER,
							&WaveFormat,
							(ULONG)PlayCallBack,
							0,
							CALLBACK_FUNCTION|WAVE_MAPPED);
	}

    if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot open wave device"),mmRet);

		return(FALSE);
    }

    if ( l_hWavePlay == NULL )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot open wave device"),mmRet);

		return(FALSE);
    }

	l_WavePlayHeader.dwBufferLength = dwBytesRead;
    
	l_WavePlayHeader.dwFlags = 0;
    
	mmRet = waveOutPrepareHeader(l_hWavePlay,
								 &l_WavePlayHeader,
								 sizeof(WAVEHDR));

    if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot prepare header"),mmRet);

		return(FALSE);
    }

	mmRet = waveOutWrite(l_hWavePlay,
						 &l_WavePlayHeader,
						 sizeof(WAVEHDR));

    if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot start playing"),mmRet);

		return(FALSE);
    }

	do
	{
		MMTIME mmtime;

		mmtime.wType = TIME_BYTES;

		mmRet = waveOutGetPosition(l_hWavePlay,&mmtime,sizeof(mmtime));

	    if ( mmRet == MMSYSERR_NOERROR )
		{
			DWORD dwResult;
			DWORD dwPercentDone;
			static DWORD dwLastPercentDone = 101;

			dwPercentDone = mmtime.u.cb*100/dwBytesRead;

			if ( dwPercentDone != dwLastPercentDone )
			{
				if ( l_hPlayWnd != NULL )
				{
					PostMessage(l_hPlayWnd,l_uPlayMsg,AUDIO_PROGRESS,(LPARAM)dwPercentDone);
				}
			}

			dwResult = WaitForSingleObject(l_hPlayStopEvent,1);

			if ( dwResult == WAIT_OBJECT_0 )
			{
				waveOutReset(l_hWavePlay);
			}
		}
		
		mmRet = waveOutUnprepareHeader(l_hWavePlay,
									   &l_WavePlayHeader,
									   sizeof(l_WavePlayHeader));

	} while ( mmRet == WAVERR_STILLPLAYING );

    if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminatePlay(l_hPlayWnd,TEXT("Cannot unprepare header"),mmRet);

		return(FALSE);
    }

	TerminatePlay(l_hPlayWnd,NULL,0);

	return(TRUE);
}


BOOL AudioStartPlay(LPTSTR lpszSoundName,
					HWND hwnd,
					UINT umsg)
{
	DWORD dwThreadId;
		
	if ( l_hPlayThread != NULL )
	{
		return(FALSE);
	}

	if ( lpszSoundName == NULL )
	{
		return(FALSE);
	}
	
	l_hPlayStopEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	
	if ( l_hPlayStopEvent == NULL )
	{
		return(FALSE);
	}
	
#ifdef UNICODE
	wcscpy(l_szPlaySoundName,lpszSoundName);
#else
	strcpy(l_szPlaySoundName,lpszSoundName);
#endif

	l_hPlayWnd = hwnd;
	l_uPlayMsg = umsg;

	l_hPlayThread = CreateThread(NULL,
								 0,
								 (LPTHREAD_START_ROUTINE)PlayThread,
								 NULL,
								 0,
								 &dwThreadId);

	if ( l_hPlayThread == NULL )
	{
		return(FALSE);
	}

	if ( l_hPlayWnd == NULL )
	{
		WaitForSingleObject(l_hPlayThread,30000);
	}

	CloseHandle(l_hPlayThread);

	return(TRUE);
}


BOOL AudioStopPlay(void)
{
	DWORD dwResult;
	
	if ( l_hPlayThread == NULL )
	{
		return(FALSE);
	}

	if ( l_hPlayStopEvent == NULL )
	{
		return(FALSE);
	}

	SetEvent(l_hPlayStopEvent);

	do
	{
		dwResult = WaitForSingleObject(l_hPlayThread,1000);

	} while ( ( l_hPlayThread != NULL ) &&
		      ( dwResult == WAIT_TIMEOUT ) );

	return(TRUE);
}


static void InitializeRecord(void)
{
	l_hWaveRecordFile = INVALID_HANDLE_VALUE;

	l_hWaveRecord = NULL;
}


static void TerminateRecord(HWND hwnd,
							LPTSTR lpszMsg,
							DWORD dwError)
{
    if ( l_hWaveRecord != NULL )
	{
        waveInClose(l_hWaveRecord);

		l_hWaveRecord = NULL;
	}

    if ( l_WaveRecordHeader.lpData )
    {
        LocalFree(l_WaveRecordHeader.lpData);
        
		l_WaveRecordHeader.lpData = NULL;
	}

    if ( lpszMsg != NULL )
	{
		if ( l_hRecordWnd != NULL )
		{
			TCHAR szMsg[MAX_PATH];
			
			wsprintf(szMsg,TEXT("ERROR %d - %s"),dwError,lpszMsg);

			SendMessage(l_hRecordWnd,l_uRecordMsg,(WPARAM)AUDIO_ERROR,(LPARAM)szMsg);
		}
	}

	l_hRecordThread = NULL;

	if ( l_hRecordStopEvent != NULL )
	{
		CloseHandle(l_hRecordStopEvent);

		l_hRecordStopEvent = NULL;
	}

	if ( l_hWaveRecordFile != INVALID_HANDLE_VALUE )
	{
		CloseHandle(l_hWaveRecordFile);

		l_hWaveRecordFile = INVALID_HANDLE_VALUE;
	}
}


void CALLBACK RecordCallBack(HWAVEOUT hwo,
							 UINT umsg,
							 DWORD dwInstance,
							 DWORD dwParam1,
							 DWORD dwParam2)
{
	switch(umsg)
	{
		case WIM_OPEN:

			if ( l_hRecordWnd != NULL )
			{
				PostMessage(l_hRecordWnd,l_uRecordMsg,AUDIO_RECORD,0L);
			}
			
			break;

		case WIM_DATA:

			if ( l_hRecordWnd != NULL )
			{
				PostMessage(l_hRecordWnd,l_uRecordMsg,AUDIO_PROGRESS,100L);
				PostMessage(l_hRecordWnd,l_uRecordMsg,AUDIO_DONE,0L);
			}
			
			break;
	}
}


static void InitializeRecordWaveFormat(WAVEFORMATEX *lpWaveFormat)
{
	lpWaveFormat->wFormatTag = WAVE_FORMAT_PCM;

	lpWaveFormat->nChannels = 1;
	
	lpWaveFormat->wBitsPerSample = 8;
	
	lpWaveFormat->nSamplesPerSec = 11025;
	
	lpWaveFormat->nAvgBytesPerSec = 11025;
	
	lpWaveFormat->nBlockAlign = 1;
	
	lpWaveFormat->cbSize = 0;

}


static void CreateWaveFileHeader( void ) 
{
	l_WaveRecordFileHeader.riff_ckID  = RIFF_CHUNK_ID ;					  // 'RIFF'
	l_WaveRecordFileHeader.ckSize	  = l_WaveRecordHeader.dwBytesRecorded + 36 ; // 36 = length of fmt & data	chunks
	l_WaveRecordFileHeader.wave_ckID  = WAVE_CHUNK_ID ;					  // 'WAVE'
	l_WaveRecordFileHeader.fmt_ckID   = FMT_CHUNK_ID  ;					  // 'fmt '
	l_WaveRecordFileHeader.fmt_ckSize =	0x10 ;
	l_WaveRecordFileHeader.formatTag  =	0x01 ;  // WAVE_FORMAT_PCM
	l_WaveRecordFileHeader.nChannels  =	1 ;
	l_WaveRecordFileHeader.nSamplesPerSec = 11025 ;
	l_WaveRecordFileHeader.nAvgBytesPerSec = 11025;
	l_WaveRecordFileHeader.nBlockAlign = 1;
	l_WaveRecordFileHeader.nBitsPerSample = 8;
	l_WaveRecordFileHeader.data_ckID = DATA_CHUNK_ID ;
	l_WaveRecordFileHeader.data_ckSize = l_WaveRecordHeader.dwBytesRecorded ;

}


static DWORD WINAPI RecordThread(LPVOID lpvParam)
{
    MMRESULT mmRet;
    DWORD dwBufferSize;
    WAVEFORMATEX WaveFormat;
	DWORD dwBytesWritten;

	InitializeRecordWaveFormat(&WaveFormat);

	InitializeRecord();

    dwBufferSize = WaveFormat.nAvgBytesPerSec * MAX_TIME;

    if ( l_hRecordWnd == NULL )
	{
		mmRet = waveInOpen(&l_hWaveRecord,
						   WAVE_MAPPER,
						   &WaveFormat,
						   (ULONG)l_hRecordWnd,
						   0,
						   CALLBACK_FUNCTION|WAVE_MAPPED);
	}
	else
	{
		mmRet = waveInOpen(&l_hWaveRecord,
						   WAVE_MAPPER,
						   &WaveFormat,
						   (ULONG)RecordCallBack,
						   0,
						   CALLBACK_FUNCTION|WAVE_MAPPED);
	}

    if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot open wave device"),mmRet);

		return(FALSE);
    }

    if ( l_hWaveRecord == NULL )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot open wave device"),mmRet);

		return(FALSE);
    }

    l_WaveRecordHeader.lpData = (LPSTR)LocalAlloc(LPTR,dwBufferSize);

	if ( l_WaveRecordHeader.lpData == NULL )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Failed allocation"),GetLastError());

		return(FALSE);
    }

    l_WaveRecordHeader.dwBufferLength = dwBufferSize;
    l_WaveRecordHeader.dwFlags = 0;

    mmRet = waveInPrepareHeader(l_hWaveRecord,
							    &l_WaveRecordHeader,
								sizeof(l_WaveRecordHeader));

	if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot prepare header"),mmRet);

		return(FALSE);
    }

    mmRet = waveInAddBuffer(l_hWaveRecord,
							&l_WaveRecordHeader,
							sizeof(l_WaveRecordHeader));

	if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot add record buffer"),mmRet);

		return(FALSE);
    }

    mmRet = waveInStart(l_hWaveRecord);

	if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot start recording"),mmRet);

		return(FALSE);
    }

	do
	{
		MMTIME mmtime;

		mmtime.wType = TIME_BYTES;

		mmRet = waveInGetPosition(l_hWaveRecord,&mmtime,sizeof(mmtime));

	    if ( mmRet == MMSYSERR_NOERROR )
		{
			DWORD dwResult;
			DWORD dwPercentDone;
			static DWORD dwLastPercentDone = 101;

			dwPercentDone = mmtime.u.cb*100/dwBufferSize;

			if ( dwPercentDone != dwLastPercentDone )
			{
				if ( l_hRecordWnd != NULL )
				{
					PostMessage(l_hRecordWnd,l_uRecordMsg,AUDIO_PROGRESS,(LPARAM)dwPercentDone);
				}
			}

			dwResult = WaitForSingleObject(l_hRecordStopEvent,1);

			if ( dwResult == WAIT_OBJECT_0 )
			{
				mmRet = waveInReset(l_hWaveRecord);
			}
		}
		
		mmRet = waveInUnprepareHeader(l_hWaveRecord,
									  &l_WaveRecordHeader,
									  sizeof(l_WaveRecordHeader));

	} while ( mmRet == WAVERR_STILLPLAYING );

    mmRet = waveInStop(l_hWaveRecord);

	if ( mmRet != MMSYSERR_NOERROR )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot stop recording"),mmRet);

		return(FALSE);
    }

	l_hWaveRecordFile = CreateFile(l_szRecordSoundName,
								   GENERIC_WRITE,
								   0,
								   NULL,
								   CREATE_ALWAYS,
								   FILE_ATTRIBUTE_NORMAL,
								   NULL);

	if ( l_hWaveRecordFile == INVALID_HANDLE_VALUE )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot open wave file"),mmRet);

		return(FALSE);
	}

	CreateWaveFileHeader() ;
	
	if ( !WriteFile(l_hWaveRecordFile,
				    &l_WaveRecordFileHeader,
				    sizeof(WAVEFILEHEADER),
				    &dwBytesWritten,
				    NULL) )
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot write wave file"),mmRet);

		return(FALSE);
	}

	if ( !WriteFile(l_hWaveRecordFile,
					l_WaveRecordHeader.lpData,
					l_WaveRecordHeader.dwBytesRecorded,
					&dwBytesWritten,NULL))
	{
		TerminateRecord(l_hRecordWnd,TEXT("Cannot write wave file"),mmRet);

		return(FALSE);
	}

	TerminateRecord(l_hRecordWnd,NULL,0);

	return(TRUE);
}


BOOL AudioStartRecord(LPTSTR lpszSoundName,
					  HWND hwnd,
					  UINT umsg)
{
	DWORD dwThreadId;
		
	if ( l_hRecordThread != NULL )
	{
		return(FALSE);
	}

	if ( lpszSoundName == NULL )
	{
		return(FALSE);
	}
	
	l_hRecordStopEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	
	if ( l_hRecordStopEvent == NULL )
	{
		return(FALSE);
	}
	
#ifdef UNICODE
	wcscpy(l_szRecordSoundName,lpszSoundName);
#else
	strcpy(l_szRecordSoundName,lpszSoundName);
#endif

	l_hRecordWnd = hwnd;
	l_uRecordMsg = umsg;

	l_hRecordThread = CreateThread(NULL,
								   0,
								   (LPTHREAD_START_ROUTINE)RecordThread,
								   NULL,
								   0,
								   &dwThreadId);

	if ( l_hRecordThread == NULL )
	{
		return(FALSE);
	}

	if ( l_hRecordWnd == NULL )
	{
		WaitForSingleObject(l_hRecordThread,30000);
	}

	CloseHandle(l_hRecordThread);

	return(TRUE);
}


BOOL AudioStopRecord(void)
{
	DWORD dwResult;
	
	if ( l_hRecordThread == NULL )
	{
		return(FALSE);
	}

	if ( l_hRecordStopEvent == NULL )
	{
		return(FALSE);
	}

	SetEvent(l_hRecordStopEvent);

	do
	{
		dwResult = WaitForSingleObject(l_hRecordThread,1000);

	} while ( ( l_hRecordThread != NULL ) &&
		      ( dwResult == WAIT_TIMEOUT ) );

	return(TRUE);
}


#else


BOOL AudioStartPlay(LPTSTR lpszSoundName,
					HWND hwnd)
{
	return(sndPlaySound(lpszSoundName,SND_SYNC));
}


BOOL AudioStopPlay(void)
{
	return(TRUE);
}


BOOL AudioStartRecord(HWND hwnd,UINT umsg)
{
	return(FALSE);
}


BOOL AudioStopRecord(void)
{
	return(FALSE);
}


#endif


BOOL AudioPlay(LPTSTR lpszSoundName)
{
	return(AudioStartPlay(lpszSoundName,NULL,0));
}


BOOL AudioStop(void)
{
	AudioStopPlay();

	AudioStopRecord();

	return(TRUE);
}


