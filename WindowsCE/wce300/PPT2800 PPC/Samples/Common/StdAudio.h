
//--------------------------------------------------------------------
// FILENAME:        STDAUDIO.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header for STDAUDIO.C
//
// NOTES:           Public
//
// 
// 
//--------------------------------------------------------------------

#ifndef STDAUDIO_H_

#define STDAUDIO_H_

#ifdef __cplusplus
extern "C"
{
#endif


enum tagPLAYTYPES
{
	AUDIO_IDLE,
	AUDIO_PLAY,
	AUDIO_RECORD,
	AUDIO_PROGRESS,
	AUDIO_DONE,
	AUDIO_ERROR,
};


BOOL AudioStartPlay(LPTSTR lpszSoundName,HWND hwnd,UINT uMsg);


BOOL AudioPlay(LPTSTR lpszSoundName);


BOOL AudioStartRecord(LPTSTR lpszSoundName,HWND hwnd,UINT umsg);


BOOL AudioStop(void);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef STDAUDIO_H_   */
