
//--------------------------------------------------------------------
// FILENAME:			StdWhoAmI.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for StdWhoAmI.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef STDWHOAMI_H_

#define STDWHOAMI_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Nested Includes
//----------------------------------------------------------------------------

#include "..\common\StdFuncs.h"

#include "..\common\StdAbout.h"


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

void WhoAmI(HWND hwnd,
			int p_nAboutIconId);


#ifdef __cplusplus
}
#endif

#endif	/* #ifndef STDWHOAMI_H_	*/

