
//--------------------------------------------------------------------
// FILENAME:			StdInit.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains standard application initialization routines.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMain.h"
#include "..\common\StdMsg.h"
#include "..\common\StdScrns.h"

#include "..\common\StdInit.h"


#ifndef _WIN32_WCE
	#pragma comment(lib,"comctl32.lib")
#endif


//----------------------------------------------------------------------------
// InitApplication
//----------------------------------------------------------------------------

BOOL InitApplication(HINSTANCE hInstance,
					 LPTSTR szAppClass,
					 HBRUSH hBackgroundBrush,
					 LPTSTR szMainMenu,
					 LPTSTR szAppIcon,
					 BOOL bMultipleInstances)
{
	HWND hWnd;
	RECT workarea;

	g_hInstance = hInstance;

	GetWorkArea(&workarea);

	// If this applications does not allow multiple running instances
	if ( !bMultipleInstances )
	{
		// Find another running instance of this application
		hWnd = FindWindow(szAppClass,NULL);

		// If one was found
		if ( hWnd != NULL )
		{
			// Make it visible
			UnhideWindow(hWnd);

			// And exit this instance
			return(FALSE);
		}
	}

    // Initialize basic window class information
	g_wc.style         = CS_VREDRAW | CS_HREDRAW;
    g_wc.lpfnWndProc   = MainWndProc;
    g_wc.cbClsExtra    = 0;
    g_wc.cbWndExtra    = 0;
    g_wc.hInstance     = hInstance;

#ifdef _WIN32_WCE

    // WCE has no cursor
    g_wc.hCursor       = NULL;

	// WCE has no menu (since it uses command bars/bands instead)
    g_wc.lpszMenuName  = NULL;

#else

    // Win32 uses a cursor
	g_wc.hCursor	   = LoadCursor(NULL,IDC_ARROW);

	// Win32 uses a menu
    g_wc.lpszMenuName  = szMainMenu;

#endif

#ifndef _WIN32_WCE
	// Set up application icon
	g_wc.hIcon		   = LoadIcon(hInstance,szAppIcon);
#endif

	// Set up background brush
    g_wc.hbrBackground = hBackgroundBrush;

	// Set up class name
    g_wc.lpszClassName = szAppClass;

    // If we could not register the class
	if ( !RegisterClass(&g_wc) )
	{
		// Report an error to the user
		LastError(TEXT("Register Class"));

		// Return failed to initialize
		return(FALSE);
	}

    // Return success in initializing
	return(TRUE);
}


//----------------------------------------------------------------------------
// InitInstance
//----------------------------------------------------------------------------

BOOL InitInstance(HINSTANCE hInstance,
				  int nCmdShow,
				  DWORD dwStyle,
				  LPTSTR szAppClass,
				  LPTSTR szAppTitle)
{
	RECT workarea;

	if ( dwStyle & WS_CAPTION )
	{
		g_nCaptionHeight = GetSystemMetrics(SM_CYCAPTION);
	}

	InitCommonControls();

#ifdef _WIN32_WCE

	// For WCE, get child area rectangle
	GetWorkArea(&workarea);

	// Create a visible window covering the entire child area
	g_hwnd = CreateWindow(szAppClass,
						  szAppTitle,
						  dwStyle | WS_VISIBLE,
						  workarea.left,
						  workarea.top,
						  workarea.right-workarea.left,
						  workarea.bottom-workarea.top,
						  NULL,
						  NULL,
						  g_hInstance,
						  NULL);

#else

	// For non WCE, get work area rectangle
	GetWorkArea(&workarea);

	if ( WasCalledBy() )
	{
		workarea.bottom += g_nCaptionHeight;
	}

	// For non-WCE, create a tiled window covering the chosen work area
	g_hwnd = CreateWindow(szAppClass,
						  szAppTitle,
						  dwStyle | (WS_TILEDWINDOW&~WS_SYSMENU),
						  workarea.left,
						  workarea.top,
						  workarea.right-workarea.left,
						  workarea.bottom-workarea.top,
						  NULL,
						  NULL,
						  g_hInstance,
						  NULL);

#endif

    // If no window was successfully created
	if ( g_hwnd == NULL )
	{
		// Report an error to the user
		LastError(TEXT("Create Window"));

		// Return failure to initialize
		return(FALSE);
	}

	// Show the window
	ShowWindow(g_hwnd,nCmdShow);
        
	// Update the window
	UpdateWindow(g_hwnd);

    // Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// StdDoMain
//----------------------------------------------------------------------------

int StdDoMain(int nAccel)
{
    MSG msg;

	// If we were passed an accelerator resource ID
	if ( nAccel )
	{
		// Load the accelerator
		g_hAccel = LoadAccelerators(g_hInstance,MAKEINTRESOURCE(nAccel));
	}

    // Get messages until we get a quit message
	while (GetMessage(&msg, NULL, 0, 0))
	{
		// If a dialog is active
		if ( g_hdlg != NULL )
		{
			// If we have an accelerator loaded
			if ( g_hAccel != NULL )
			{
				// If we are not suppressing accelerators
				if ( !g_bSkipAccel )
				{
					// If it was an accelerator, we are done
					if ( TranslateAccelerator(g_hdlg,g_hAccel,(LPMSG)&msg) )
					{
						continue;
					}
				}
			}
			
			// If the message is for the dialog, we are done
			if ( IsDialogMessage(g_hdlg,&msg) )
			{
				continue;
			}
		}

		// If we have an accelerator loaded
		if ( g_hAccel != NULL )
		{
			// If we are not suppressing accelerators
			if ( !g_bSkipAccel )
			{
				// If it was an accelerator, we are done
				if ( TranslateAccelerator(g_hwnd,g_hAccel,(LPMSG)&msg) )
				{
					continue;
				}
			}
		}

		// Translate the message if necessary
		TranslateMessage((LPMSG)&msg);

		// Dispatch the message to the appropriate handler
		DispatchMessage((LPMSG)&msg);
    }

    // Return the value passed in the quit message
	return((int)msg.wParam);
}


