//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by msrsamp.rc
//
#define TBSTYLE_BUTTON                  0x0000
#define	TBSTYLE_AUTOSIZE				0x0010
#define IDS_APP_TITLE                   1
#define IDS_HELLO                       2
#define IDS_MSROPENED                   2
#define IDC_MSRSAMP                     3
#define IDS_MSRNOTOPEN                  4
#define TBSTATE_ENABLED                 0x04
#define IDI_MSRSAMP                     101
#define IDM_MENU                        102
#define IDD_ABOUTBOX                    103
#define IDM_FILE_EXIT                   40002
#define IDM_HELP_ABOUT                  40003
#define ID_FILE                         40004
#define IDS_CAP_FILE                    40006
#define IDS_CAP_HELP                    40009
#define IDS_CAP_ABOUT                   40011
#define IDS_CAP_EXIT                    40013
#define IDS_CAP_MENUITEM40015           40016
#define IDS_CAP                         40019
#define I_IMAGENONE						(-2)

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40020
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
