
//--------------------------------------------------------------------
// FILENAME:			msrsamp.cpp
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Source file for the MSR Sample application
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include "stdafx.h"
#include "msrsamp.h"
#include <commctrl.h>
#include "msr3000.h"
#include <aygshell.h>		// Suneet 2000/03/31

#define MAX_LOADSTRING 100
#define CURRENT_ERROR_NO	27
#define MAX_ERROR_NUM_LEN	16


// thread id
DWORD dwThreadID;

// read thread is already started.
BOOL g_bThreadReadCreate = FALSE;

// event handle
HANDLE hEvent;

// status of unbuffer read
DWORD Unbuffered_Status = ERROR_SUCCESS;

// result of each operation
char g_strResult[500];

// handle of the read thread
HANDLE	g_hThreadRead = 0;

BOOL Msr3000_Opened = FALSE;

RECT grt;

// Global Variables:
HINSTANCE			hInst;			// The current instance
HWND				hwndCB;			// The command bar handle
HWND				gHwnd;

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass	(HINSTANCE hInstance, LPTSTR szWindowClass);
BOOL				InitInstance	(HINSTANCE, int);
LRESULT CALLBACK	WndProc			(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About			(HWND, UINT, WPARAM, LPARAM);


// read continuously thread
UINT PortReadThreadMgr(LPVOID );

void	Close_Msr3000();
void	ShowResult(DWORD);
void	GetMsr3000ErrorString(DWORD);



// Entry point for the Windows Application 

int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_MSRSAMP);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
	WNDCLASS	wc;

    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WndProc;
    wc.cbClsExtra		= 0;
    wc.cbWndExtra		= 0;
    wc.hInstance		= hInstance;
    wc.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MSRSAMP));
    wc.hCursor			= 0;
    wc.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName		= 0;
    wc.lpszClassName	= szWindowClass;

	return RegisterClass(&wc);
}

//
//  FUNCTION: InitInstance(HANDLE, int)
//
//  PURPOSE: Saves instance handle and creates main window
//
//  COMMENTS:
//
//    In this function, we save the instance handle in a global variable and
//    create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND		hWnd;
	TCHAR		szTitle[MAX_LOADSTRING];			// The title bar text
	TCHAR		szWindowClass[MAX_LOADSTRING];		// The window class name
	char		*MsrPort = "COM1:";					// Msr3000 is connected to COM1 port.
	DWORD		Status = 0x0;
	DWORD		Reserved;							//Reserved parameter for future use
	unsigned	char Dll_Version[20];				// dll version
	unsigned	char Firmware_Version[10];			// firmware version



	hInst = hInstance;		// Store instance handle in our global variable
	// Initialize global strings
	LoadString(hInstance, IDC_MSRSAMP, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance, szWindowClass);

	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0,0, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{	
		return FALSE;
	}

	gHwnd = hWnd;
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	if (hwndCB)
	{
		CommandBar_Show(hwndCB, TRUE);
	}
	// Open Msr3000 here
	Status = Msr3000_Open((unsigned char *)MsrPort, Dll_Version, Firmware_Version,(void *)&Reserved);
	if(Status != ERROR_SUCCESS)
	{
		ShowResult(Status);
		CommandBar_Destroy(hwndCB);
		PostQuitMessage(0);

	}
	else
	{
		// set the MSR3000 to unbuffer mode
		Status = Msr3000_SetBufferMode(UNBUFFERED_MODE);
		if(Status == ERROR_SUCCESS)
		{
			Status = Msr3000_SetTrackSelection(ANY_TRACK);
			if(Status == ERROR_SUCCESS)
			{
				Msr3000_Opened = TRUE;
				/* Repaint the window */
				InvalidateRect(gHwnd,&grt,TRUE);
				// gets the MSR3000 read notification.
				hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
				// Create a thread to read the card swipe data.
				g_hThreadRead = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PortReadThreadMgr, 0, 0,&dwThreadID);
			}
		};
	}
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	TCHAR szHello[MAX_LOADSTRING];

	// Added by Suneet 2000/03/31
#if _WIN32_WCE > 211 //Rapier devices
	BOOL bSuccess;
	SHMENUBARINFO mbi = {0};
#endif


	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_HELP_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_FILE_EXIT:
		 			DestroyWindow(hWnd);
					/*Terminate Read thread and close msr3000*/
					TerminateThread(g_hThreadRead, 0);
					Close_Msr3000();
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_CREATE:
			{
// Added by Suneet  2000/03/31
#if _WIN32_WCE > 211 //Rapier devices
			memset(&mbi, 0, sizeof(SHMENUBARINFO));
			mbi.cbSize     = sizeof(SHMENUBARINFO);
  			mbi.hInstRes = hInst;
			mbi.hwndParent = hWnd;
			mbi.nToolBarId = IDM_MENU;
			mbi.nBmpId     = 0;
			mbi.cBmpImages = 0;	
  			
			bSuccess = SHCreateMenuBar(&mbi);
			hwndCB = mbi.hwndMB;
#else
			hwndCB = CommandBar_Create(hInst, hWnd, 1);			
			CommandBar_InsertMenubar(hwndCB, hInst, IDM_MENU, 0);
			CommandBar_AddAdornments(hwndCB, 0, 0);
#endif
			SetFocus(hWnd);
			InvalidateRect(hWnd,NULL,TRUE);
			UpdateWindow(hWnd);
			break;
			}

		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &grt);
			if(Msr3000_Opened == FALSE)
			{
				LoadString(hInst,IDS_MSRNOTOPEN, szHello, MAX_LOADSTRING);
			}
			else
			{
				LoadString(hInst, IDS_MSROPENED, szHello, MAX_LOADSTRING);
			}
			DrawText(hdc, szHello, _tcslen(szHello), &grt, 
				DT_SINGLELINE | DT_VCENTER | DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			CommandBar_Destroy(hwndCB);
			PostQuitMessage(0);

			/*Terminate Read thread and close msr3000*/
			TerminateThread(g_hThreadRead, 0);
			Close_Msr3000();
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for the About box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;

	switch (message)
	{
		case WM_INITDIALOG:
			// trying to center the About dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the About box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}
			return TRUE;

		case WM_COMMAND:
			if ((LOWORD(wParam) == IDOK) || (LOWORD(wParam) == IDCANCEL))
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}

// Close the MSR3000 when postquit is called.
void Close_Msr3000() 
{
	DWORD Status = Msr3000_Close((UBYTE_PTR)"COM1:");
	if(Status)
		ShowResult(Status);
}

// unbuffered read thread
UINT PortReadThreadMgr(LPVOID param)
{
	while(1)
	{
		int index = 0;

		// Clear the dara buffer first.
		while(index < 500)
			g_strResult[index++] = 0x0;

		ResetEvent(hEvent);
		Unbuffered_Status = ERROR_SUCCESS;

		// read the MSR3000 in unbuffered mode.
		Unbuffered_Status = Msr3000_ReadMSRUnbuffer((unsigned char *)g_strResult, (void *)&hEvent);
		//WaitForSingleObject(hEvent, INFINITE);
		ShowResult(Unbuffered_Status);
		if(Unbuffered_Status != ERROR_SUCCESS)
		{
			break;
		}
	}
	return Unbuffered_Status;
}

// display the error or read result
void ShowResult(DWORD Status)
{
	LPCSTR	lpszA;
	TCHAR	lpszW[MAX_CARD_DATA*2];
	LPVOID lpMsgBuf;

	if(Status)
	{
		if(Status >= MSR3000_ERR_GLOBAL)
		{
			// MSR3000 error
			GetMsr3000ErrorString(Status);
		}
		else 
		{
			// WinCE error
			if(!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
				NULL, Status, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
				(LPTSTR) &lpMsgBuf, 0, NULL))
			{
				char Temp[MAX_ERROR_NUM_LEN];
				_itoa((int)Status, Temp, 10);
				strcpy(g_strResult, "WinCE Error no : ");
				strcat(g_strResult, Temp);
			}
			else
			{
				strcpy(g_strResult, (const char *)lpMsgBuf);
				LocalFree( lpMsgBuf );
			}
		}
	}
	
	lpszA	= (LPCSTR)g_strResult;
	
	/* convert an MBCS string in lpszA. */
	int	nLen = MultiByteToWideChar(CP_ACP, 0, lpszA, -1, NULL, NULL);
	/* validity check for the file name length */
    nLen = ((nLen > (MAX_CARD_DATA))? MAX_CARD_DATA:nLen);
	MultiByteToWideChar(CP_ACP, 0, lpszA, -1, lpszW, nLen);

	// show the result
	if(Status != ERROR_SUCCESS)
		MessageBox(gHwnd, lpszW, L"Error, Exit and start again", MB_OK);
	else
		MessageBox(gHwnd, lpszW, L"Press OK to swipe again.", MB_OK);
}

// format the error string
void GetMsr3000ErrorString(DWORD error)
{
	DWORD dwErrorNo ;

	char *strErr[CURRENT_ERROR_NO+1] = 
	{
		"MSR3000_SUCCESS"			,
		"MSR3000_ERR_GLOBAL" 	  	,
		"MSR3000_ERR_PARAMETER" 	,
		"MSR3000_ERR_NOTOPEN"   	,
		"MSR3000_ERR_STILLOPEN"		,
		"MSR3000_ERR_MEMORY"		,
		"MSR3000_ERR_SIZE"		    ,
		"MSR3000_ERR_NAK"			,
		"MSR3000_ERR_BAD_ANS"		,
		"MSR3000_ERR_TIMEOUT"    	,
		"MSR3000_ERR_ROM"        	,
		"MSR3000_ERR_RAM"	        ,
		"MSR3000_ERR_EEPROM"		,
		"MSR3000_ERR_RES"			,
		"MSR3000_ERR_CHECKSUM"		,
		"MSR3000_ERR_BADREAD"		,
		"MSR3000_ERR_NODATA"		,
		"MSR3000_ERR_NULLPOINTER"	,
		"MSR3000_ERR_BUSY"			,
		"MSR3000_ERR_WAKEUP"		,
	    "MSR3000_ERR_BUFFERED_MODE"	,
		"MSR3000_ERR_UNBUFFERED_MODE" ,
		"MSR3000_ERR_PORTEVENT"		,
		"MSR3000_ERR_OPENFAILURE"   ,
		"MSR3000_ERR_BATTERYLEVEL",
		"MSR3000_ERR_POWERDOWN"		,
		"MSR3000_ERR_OSERROR"		,
	};

	if((error >= MSR3000_ERR_GLOBAL) && (error <= MSR3000_ERR_OSERROR))
	{
		dwErrorNo = (error - MSR3000_ERR_GLOBAL)+1;
		strcpy(g_strResult, strErr[dwErrorNo]);
	}
	else
	{
		strcpy(g_strResult, "MSR3000 unknown error no.");
	}
}

