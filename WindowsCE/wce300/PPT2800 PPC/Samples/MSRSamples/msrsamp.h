
//--------------------------------------------------------------------
// FILENAME:			msrsamp.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for the MSR Sample application
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#if !defined(AFX_MSRSAMP_H__4EBF63D5_8C2B_11D3_95B2_0000E20E3335__INCLUDED_)
#define AFX_MSRSAMP_H__4EBF63D5_8C2B_11D3_95B2_0000E20E3335__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"


#endif // !defined(AFX_MSRSAMP_H__4EBF63D5_8C2B_11D3_95B2_0000E20E3335__INCLUDED_)
