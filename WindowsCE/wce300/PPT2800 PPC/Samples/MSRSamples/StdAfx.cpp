//--------------------------------------------------------------------
// FILENAME:			stdafx.cpp
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Source file that includes just the standard includes
//						msrsamp.pch will be the pre-compiled header 
//						stdafx.obj will contain the pre-compiled type information
//
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file
