
//--------------------------------------------------------------------
// FILENAME:        XFERDLG.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Heade file for XFERDLG.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef XFERDLG_H_

#define XFERDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif

extern int g_nXferIndex;

void DoXfer(int nType,int nIndex);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef XFERDLG_H_   */
