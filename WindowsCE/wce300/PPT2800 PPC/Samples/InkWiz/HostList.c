
//--------------------------------------------------------------------
// FILENAME:            HOSTLIST.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains routines to handle the host list
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include <winsock.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\StdExec.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdAbout.h"

#include "..\common\Treeview.h"

#include "listview.h"
#include "editdlg.h"

#include "hostlist.h"

#include "inkwiz.h"

#include "tcpip.h"


//----------------------------------------------------------------------------
// Local function prototypes
//----------------------------------------------------------------------------

static BOOL InitializeHostListView(HWND hWndList);
static BOOL GetHostListItem(int nRow,int nCol,LPTSTR lpszItem,DWORD dwSize);
static BOOL AddNewHostListItem(HWND hWndList);
static BOOL DeleteHostListItem(HWND hWndList,int nItem);
static BOOL EditHostListItem(HWND hWndList,int nItem);
static BOOL ExitHostListView(HWND hWndList);


//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static int l_nHostListCount = 0;

static int HostCounts[MAX_HOSTS+1];


static LISTPROCS HostListProcs =
{
	InitializeHostListView,
	GetHostListItem,
	AddNewHostListItem,
	DeleteHostListItem,
	EditHostListItem,
	ExitHostListView
};



//----------------------------------------------------------------------------
// Local functions
//----------------------------------------------------------------------------

static BOOL InitHostList(void)
{
	int i;

	memset(HostCounts,0,sizeof(HostCounts));

	for(i=0;i<MAX_HOSTS;i++)
	{
		TCHAR szHostName[MAX_PATH];

		GetHost(i,szHostName,TEXTSIZEOF(szHostName));

		if ( !szHostName[0] )
		{
			break;
		}

		if ( UsingDNS(szHostName) )
		{
			HostCounts[i]++;
		}
		else
		{
			int j;

			for(j=0;j<MAX_IPADDRS;j++)
			{
				TCHAR szIpAddr[MAX_PATH];

				GetIpAddr(szHostName,j,szIpAddr,TEXTSIZEOF(szIpAddr));

				if ( !szIpAddr[0] )
				{
					break;
				}

				HostCounts[i]++;
			}
		}
	}

	return(TRUE);
}


static BOOL InitializeHostListView(HWND hWndList)
{
	RECT rect;
	LV_COLUMN lvC;
	TCHAR szHostName[MAX_PATH];
	TCHAR szIpAddr[MAX_PATH];
	BOOL bResult;
	int nWidth;

	if ( hWndList == NULL )
	{
		return(FALSE);
	}

	GetClientRect(hWndList,&rect);

	nWidth = rect.right - rect.left;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM | LVCF_TEXT;
	lvC.fmt = LVCFMT_LEFT;

	lvC.iSubItem = 0;
	lvC.pszText = TEXT("Host Name");
	lvC.cx = nWidth/2;
	bResult = ListView_InsertColumn(hWndList,0,&lvC);
	bResult = FALSE;

	lvC.iSubItem = 1;
	lvC.pszText = TEXT("IP Address");
	lvC.cx = nWidth/2;
	bResult = ListView_InsertColumn(hWndList,1,&lvC);
	bResult = FALSE;

	InitHostList();
	
	for(l_nHostListCount=0;
		GetHostListItem(l_nHostListCount,0,szHostName,TEXTSIZEOF(szHostName)) &&
		GetHostListItem(l_nHostListCount,1,szIpAddr,TEXTSIZEOF(szIpAddr));
		l_nHostListCount++)
	{
		LV_ITEM lvI;

		lvI.mask = LVIF_TEXT;
		lvI.iItem = l_nHostListCount;
		lvI.iSubItem = 0;
		lvI.pszText = szHostName;
		lvI.cchTextMax = TEXTLEN(szHostName);
		bResult = ( ListView_InsertItem(hWndList,&lvI) >= 0 );

		if ( bResult )
		{
			lvI.mask = LVIF_TEXT;
			lvI.iItem = l_nHostListCount;
			lvI.iSubItem = 1;
			lvI.pszText = szIpAddr;
			lvI.cchTextMax = TEXTLEN(szIpAddr);
			bResult = ( ListView_SetItem(hWndList,&lvI) >= 0 );
		}
	}

	return(TRUE);
}


static BOOL GetHostListItem(int nRow,int nCol,LPTSTR lpszItem,DWORD dwSize)
{
	int i;

	for(i=0;i<MAX_HOSTS;i++)
	{
		if ( nRow < HostCounts[i] )
		{
			break;
		}

		nRow -= HostCounts[i];
	}

	switch(nCol)
	{
		case 0:
			
			GetHost(i,lpszItem,dwSize);
			
			break;
		
		case 1:
			{
				TCHAR szHostName[MAX_PATH] = TEXT("");

				GetHost(i,szHostName,TEXTSIZEOF(szHostName));

				if ( UsingDNS(szHostName) )
				{
					TEXTCPY(lpszItem,TEXT("DNS"));
				}
				else
				{
					GetIpAddr(szHostName,nRow,lpszItem,dwSize);
				}
			}
			
			break;
	}

	if ( *lpszItem )
	{
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}


static BOOL AddNewHostListItem(HWND hWndList)
{
	LV_ITEM lvI;
	TCHAR szTemp[MAX_PATH];
	BOOL bResult;

	lvI.mask = LVIF_TEXT;
	lvI.iItem = l_nHostListCount;
	lvI.iSubItem = 0;
	TEXTCPY(szTemp,TEXT("New Host Name"));
	lvI.pszText = szTemp;
	lvI.cchTextMax = TEXTLEN(szTemp);
	bResult = ( ListView_InsertItem(hWndList,&lvI) >= 0 );

	if ( bResult )
	{
		lvI.mask = LVIF_TEXT;
		lvI.iItem = l_nHostListCount;
		lvI.iSubItem = 1;
		TEXTCPY(szTemp,TEXT("DNS"));
		lvI.pszText = szTemp;
		lvI.cchTextMax = TEXTLEN(szTemp);
		bResult = ( ListView_SetItem(hWndList,&lvI) >= 0 );
	}

	if ( bResult )
	{
		ListView_SetItemState(hWndList,
							  l_nHostListCount,
							  LVIS_SELECTED|LVIS_FOCUSED,
							  LVIS_SELECTED|LVIS_FOCUSED);
	}
	
	if ( bResult )
	{
		l_nHostListCount++;
	}
	
	return(bResult);
}


static BOOL DeleteHostListItem(HWND hWndList,int nItem)
{
	BOOL bResult;

	bResult = ListView_DeleteItem(hWndList,nItem);

	if ( bResult )
	{
		l_nHostListCount--;

		if ( l_nHostListCount > 0 )
		{
			if ( nItem >= l_nHostListCount )
			{
				nItem = l_nHostListCount-1;
			}

			ListView_SetItemState(hWndList,
								  nItem,
								  LVIS_SELECTED|LVIS_FOCUSED,
								  LVIS_SELECTED|LVIS_FOCUSED);
		}
	}

	return(bResult);
}


static BOOL EditHostListItem(HWND hWndList,int nItem)
{
	TCHAR szText1[MAX_PATH];
	TCHAR szText2[MAX_PATH];
	LPTSTR szTexts[] = { szText1,
						 szText2,
						 TEXT("Host Name"),
						 TEXT("IP Address") };
	BOOL bResult;

	ListView_GetItemText(hWndList,nItem,0,szText1,TEXTSIZEOF(szText1));
	ListView_GetItemText(hWndList,nItem,1,szText2,TEXTSIZEOF(szText2));

	bResult = InvokeEditDialog(szTexts);

	if ( bResult )
	{
		ListView_SetItemText(hWndList,nItem,0,szText1);
		ListView_SetItemText(hWndList,nItem,1,szText2);
	}

	return(bResult);
}


BOOL ExitHostListView(HWND hWndList)
{
	int i;

	ClearAllHosts();

	for(i=0;i<l_nHostListCount;i++)
	{
		TCHAR szHostName[MAX_PATH];
		TCHAR szIpAddr[MAX_PATH];

		ListView_GetItemText(hWndList,i,0,szHostName,TEXTSIZEOF(szHostName));
		ListView_GetItemText(hWndList,i,1,szIpAddr,TEXTSIZEOF(szIpAddr));

		SetHostAndIpAddr(szHostName,szIpAddr,TRUE);
	}
	
#ifdef _WIN32_WCE

	if ( g_hwndCB != NULL )
	{
		HWND hwndBand;

		hwndBand = CommandBands_GetCommandBar(g_hwndCB,0);

		AddWinsockHostsToMenu(CommandBar_GetMenu(hwndBand,0),0,7,3,-1);
	}

#else

	AddWinsockHostsToMenu(GetMenu(g_hwnd),0,7,3,-1);

#endif

	return(TRUE);
}


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL InvokeHostListView(void)
{
	BOOL bResult;
	static BOOL bInProgress = FALSE;

	if ( bInProgress )
	{
		return(FALSE);
	}
	
	bInProgress = TRUE;
	
	bResult = InvokeListView(TEXT("Hosts List"),&HostListProcs);

	bInProgress = FALSE;

	return(bResult);
}

