//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.RC
//
#define IDDONE                          3
#define IDOK2                           3
#define IDC_EDITFIELD                   501
#define IDC_EDITPROMPT                  502
#define IDC_PARAMETERS                  504
#define IDD_LIST_PPC_STD                508
#define IDD_LIST_7500_STD               510
#define IDD_EDIT_PPC_SIP                528
#define IDD_EDIT_PPC_STD                529
#define IDD_LIST_7500_TSK               530
#define IDD_EDIT_7500_STD               535
#define IDD_EDIT_7500_TSK               536
#define IDD_LIST_7200_TSK               538
#define IDD_EDIT_7200_TSK               540
#define IDD_EDIT_7200_STD               541
#define IDD_LIST_7200_STD               545
#define IDC_ADVANCE                     546
#define IDC_RETARD                      547
#define IDC_ADD                         547
#define IDC_DELETE                      548
#define IDD_LIST_PPC_SIP                551
#define IDS_PROGRAM                     601
#define IDS_PROGRAMDESCRIPTION          602
#define IDC_FILENAME                    603
#define IDC_FILEPATH                    604
#define IDC_FILETYPE                    605
#define IDC_FILESIZE                    606
#define IDC_READONLY                    607
#define IDC_HIDDEN                      608
#define IDC_MESSAGE1                    609
#define IDC_MESSAGE2                    610
#define IDC_MESSAGE3                    611
#define IDC_PROGRESS1                   612
#define IDC_POPUP                       613
#define IDC_BEHIND                      614
#define IDC_PROGTEXT                    615
#define IDC_EXPAND                      616
#define IDC_COLLAPSE                    617
#define IDC_MINIMUM                     618
#define IDC_DEFAULT                     619
#define IDC_FILE_EXIT                   620
#define IDC_HELP_ABOUT                  621
#define IDC_FILE_OPEN                   622
#define IDC_FILE_DELETE                 623
#define IDC_SEND_IRSOCK                 624
#define IDC_FILE_PROPERTIES             625
#define IDC_RECV_IRSOCK                 626
#define IDC_EDIT_CUT                    627
#define IDC_EDIT_COPY                   628
#define IDC_EDIT_PASTE                  629
#define IDD_PROPERTIES_PPC_STD          631
#define IDD_PROPERTIES_7500_STD         634
#define IDD_XFER_PPC_STD                636
#define IDD_XFER_7500_STD               638
#define IDB_IMAGES                      639
#define IDB_IMAGES2                     640
#define IDB_CMDBAND                     641
#define IDB_IRBUTTONS                   642
#define IDI_PROGRAMICON                 643
#define IDM_MAIN                        644
#define IDM_POPUP                       645
#define IDA_ACCELERATOR1                646
#define IDD_PROPERTIES_PPC_SIP          647
#define IDD_XFER_PPC_SIP                648
#define IDD_XFER_7500_TSK               650
#define IDD_PROPERTIES_7500_TSK         653
#define IDD_XFER_7200_TSK               656
#define IDD_XFER_7200_STD               657
#define IDD_PROPERTIES_7200_TSK         659
#define IDD_PROPERTIES_7200_STD         660
#define IDC_VK_UP                       660
#define IDC_VK_DOWN                     661
#define IDD_PROPERTIES_PC_STD           661
#define IDC_VK_LEFT                     662
#define IDD_XFER_PC_STD                 662
#define IDC_VK_RIGHT                    663
#define IDD_PROPERTIES_HPC_STD          663
#define ID_MENUITEM40033                664
#define IDD_XFER_HPC_STD                664
#define IDD_LIST_PC_STD                 665
#define IDD_LIST_HPC_STD                666
#define IDC_SEND_IRCOMM                 667
#define IDD_EDIT_PC_STD                 667
#define IDC_RECV_IRCOMM                 668
#define IDD_EDIT_HPC_STD                668
#define IDC_SEND_CRADLE                 669
#define IDC_RECV_CRADLE                 670
#define IDC_BAUD_1200                   673
#define IDC_BAUD_9600                   674
#define IDC_BAUD_19200                  675
#define IDC_BAUD_38400                  676
#define IDC_BAUD_115200                 677
#define IDC_WHOAMI                      678
#define IDC_SEND_WINSOCK                680
#define IDC_EDITPROMPT_LABEL            680
#define IDM_LISTPOPUP                   680
#define IDC_EDITFIELD_LABEL             681
#define IDC_EDIT                        681
#define IDC_TEXT1                       683
#define IDC_TEXT2                       684
#define IDC_TEXT_SIZE                   685
#define IDC_RECV_WINSOCK                690
#define IDC_HOSTS                       691
#define IDC_CONNECTIONS                 692
#define IDC_BAUD_57600                  695
#define ID_FILE                         696
#define IDS_CAP_FILE                    698
#define ID_EDIT                         699
#define IDS_CAP_EDIT                    701
#define ID_HELP                         702
#define IDC_LEFT                        704
#define IDS_CAP_HELP                    704
#define IDC_RIGHT                       705
#define ID_MENUITEM705                  705
#define IDC_UP                          706
#define IDC_DOWN                        707
#define IDS_CAP_MENUITEM706             707
#define ID_MENUITEM708                  708
#define ID_MENUITEM709                  709
#define ID_TREE                         710
#define IDS_CAP_TREE                    712
#define IDM_MAIN_POCKET                 713
#define ID_WINDOW                       714
#define IDS_CAP_WINDOW                  716
#define IDC_FILE_RENAME                 717
#define IDC_REFRESH                     718
#define IDC_FILE_SAVE                   789
#define IDC_PREV_FILE                   1009
#define IDC_NEXT_FILE                   1010
#define IDC_HOTKEY1                     1022
#define IDC_SEND_SERIAL1                1023
#define IDC_SEND_SERIAL2                1024
#define IDC_SEND_SERIAL3                1025
#define IDC_SEND_SERIAL4                1026
#define IDC_RECV_SERIAL1                1027
#define IDC_RECV_SERIAL2                1028
#define IDC_RECV_SERIAL3                1029
#define IDC_RECV_SERIAL4                1030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        681
#define _APS_NEXT_COMMAND_VALUE         719
#define _APS_NEXT_CONTROL_VALUE         686
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
