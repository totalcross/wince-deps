
//--------------------------------------------------------------------
// FILENAME:        EditDlg.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains edit dialog handling functions.
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\StdExec.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"
#include "..\common\StdAbout.h"

//#define _STDDEBUG
#include "..\common\StdDebug.h"

#include "resource.h"

#include "editdlg.h"


//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static BOOL l_bDialogWasOKed;

static DIALOG_CHOICE EditDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_EDIT_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_EDIT_HPC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_EDIT_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_EDIT_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_EDIT_7200_STD,		NULL,	UI_STYLE_DEFAULT},
	{ ST_7200_TSK,	IDD_EDIT_7200_TSK,		NULL,	UI_STYLE_DEFAULT},
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_EDIT_7500_STD,		NULL,	UI_STYLE_DEFAULT},
	{ ST_7500_TSK,	IDD_EDIT_7500_TSK,		NULL,	UI_STYLE_DEFAULT},
#endif
	{ ST_UNKNOWN,	IDD_EDIT_7500_TSK,		NULL,	UI_STYLE_DEFAULT }
};


typedef struct tagRESIZE
{
	BOOL bPrompt1Visible;
	TCHAR szPrompt1Text[MAX_PATH];

	BOOL bField1Visible;
	TCHAR szField1Text[MAX_PATH];

	BOOL bPrompt2Visible;
	TCHAR szPrompt2Text[MAX_PATH];

	BOOL bField2Visible;
	TCHAR szField2Text[MAX_PATH];

} EDITRESIZE;

typedef EDITRESIZE FAR * LPEDITRESIZE;


//----------------------------------------------------------------------------
// Local functions
//----------------------------------------------------------------------------

LRESULT CALLBACK EditDlgProc(HWND hwnd,
							 UINT uMsg,
							 WPARAM wParam,
							 LPARAM lParam)
{
	static LPTSTR FAR * lplpszTexts;
	static HWND hWndPrompt1;
	static HWND hWndField1;
	static HWND hWndPrompt2;
	static HWND hWndField2;
	static HWND hWndCtl = NULL;

	LPEDITRESIZE lpEditResize;

#if 0

#define WM_NCACTIVATE                   0x0086
#define WM_NOTIFYFORMAT					0x0055

	DEBUG_STRING(TRUE,TEXT("EditDlgProc:"));

	switch(uMsg)
	{
		case WM_SETTINGCHANGE:
			DEBUG_STRING(TRUE,TEXT("WM_SETTINGCHANGE"));
			break;
		case WM_SIZE:
			DEBUG_STRING(TRUE,TEXT("WM_SIZE"));
			break;
		case WM_MOVE:
			DEBUG_STRING(TRUE,TEXT("WM_MOVE"));
			break;
		case WM_SETFOCUS:
			DEBUG_STRING(TRUE,TEXT("WM_SETFOCUS"));
			break;
		case WM_KILLFOCUS:
			DEBUG_STRING(TRUE,TEXT("WM_KILLFOCUS"));
			break;
		case WM_PAINT:
			DEBUG_STRING(TRUE,TEXT("WM_PAINT"));
			break;
		case WM_QUERYNEWPALETTE:
			DEBUG_STRING(TRUE,TEXT("WM_QUERYNEWPALETTE"));
			break;
		case WM_CTLCOLOREDIT:
			DEBUG_STRING(TRUE,TEXT("WM_CTLCOLOREDIT"));
			break;
		case WM_ERASEBKGND:
			DEBUG_STRING(TRUE,TEXT("WM_ERASEBKGND"));
			break;
		case WM_INITMENUPOPUP:
			DEBUG_STRING(TRUE,TEXT("WM_INITMENUPOPUP"));
			break;
		case WM_ENTERMENULOOP:
			DEBUG_STRING(TRUE,TEXT("WM_ENTERMENULOOP"));
			break;
		case WM_EXITMENULOOP:
			DEBUG_STRING(TRUE,TEXT("WM_EXITMENULOOP"));
			break;
		case WM_WINDOWPOSCHANGED:
			DEBUG_STRING(TRUE,TEXT("WM_WINDOWPOSCHANGED"));
			break;
		case WM_ACTIVATE:
			DEBUG_STRING(TRUE,TEXT("WM_ACTIVATE"));
			break;
		case WM_NCACTIVATE:
			DEBUG_STRING(TRUE,TEXT("WM_NCACTIVATE"));
			break;
		case WM_CLOSE:
			DEBUG_STRING(TRUE,TEXT("WM_CLOSE"));
			break;
		case WM_DESTROY:
			DEBUG_STRING(TRUE,TEXT("WM_DESTROY"));
			break;
		case WM_NOTIFY:
			DEBUG_STRING(TRUE,TEXT("WM_NOTIFY"));
			break;
		case WM_NOTIFYFORMAT:
			DEBUG_STRING(TRUE,TEXT("WM_NOTIFYFORMAT"));
			break;
		case WM_COMMAND:
			DEBUG_STRING(TRUE,TEXT("WM_COMMAND"));
			break;
		case WM_CREATE:
			DEBUG_STRING(TRUE,TEXT("WM_CREATE"));
			break;
		case WM_INITDIALOG:
			DEBUG_STRING(TRUE,TEXT("WM_INITDIALOG"));
			break;

		case UM_ENTERDLG:
			DEBUG_STRING(TRUE,TEXT("UM_ENTERDLG"));
			break;
		case UM_EXITDLG:
			DEBUG_STRING(TRUE,TEXT("UM_EXITDLG"));
			break;
		case UM_RESIZE:
			DEBUG_STRING(TRUE,TEXT("UM_RESIZE"));
			break;
		case UM_INIT:
			DEBUG_STRING(TRUE,TEXT("UM_INIT"));
			break;
		case UM_TERM:
			DEBUG_STRING(TRUE,TEXT("UM_TERM"));
			break;
		case UM_SELECT:
			DEBUG_STRING(TRUE,TEXT("UM_SELECT"));
			break;

		default:
			{
				TCHAR szMsg[MAX_PATH];

				TEXTSPRINTF(szMsg,
							TEXT("uMsg = %.8X, wParam = %.8X, lParam = %.8X"),
							uMsg,
							wParam,
							lParam);

				DEBUG_STRING(TRUE,szMsg);
			}
			break;
	}

#endif

    switch(uMsg)
    {
		case WM_KILLFOCUS:
			
			hWndCtl = GetFocus();
			
			break;

		case WM_SETFOCUS:
			
			if ( hWndCtl != NULL )
			{
				SetFocus(hWndCtl);
			}
			
			break;

		case UM_RESIZE:
			
			// Allocate save info for file name
			lpEditResize = (LPEDITRESIZE)LocalAlloc(LPTR,sizeof(EDITRESIZE));
			
			// If we allocated successfully
			if ( lpEditResize != NULL )
			{
				lpEditResize->bPrompt1Visible = IsWindowVisible(hWndPrompt1);
				
				if ( lpEditResize->bPrompt1Visible )
				{
					GetWindowText(hWndPrompt1,lpEditResize->szPrompt1Text,MAX_PATH);
				}

				lpEditResize->bField1Visible = IsWindowVisible(hWndField1);
				
				if ( lpEditResize->bField1Visible )
				{
					GetWindowText(hWndField1,lpEditResize->szField1Text,MAX_PATH);
				}

				lpEditResize->bPrompt2Visible = IsWindowVisible(hWndPrompt2);
				
				if ( lpEditResize->bPrompt2Visible )
				{
					GetWindowText(hWndPrompt2,lpEditResize->szPrompt2Text,MAX_PATH);
				}

				lpEditResize->bField2Visible = IsWindowVisible(hWndField2);
				
				if ( lpEditResize->bField2Visible )
				{
					GetWindowText(hWndField2,lpEditResize->szField2Text,MAX_PATH);
				}
			}

			// Resize this dialog, saving info if allocated
			ResizeDialog(lpEditResize);
			
			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);

			hWndPrompt1 = GetDlgItem(hwnd,IDC_EDITPROMPT_LABEL);
			hWndField1 = GetDlgItem(hwnd,IDC_EDITPROMPT);
			hWndPrompt2 = GetDlgItem(hwnd,IDC_EDITFIELD_LABEL);
			hWndField2 = GetDlgItem(hwnd,IDC_EDITFIELD);

			lpEditResize = (LPEDITRESIZE)GetDialogSaveInfo();

			if ( lpEditResize != NULL )
			{
				if ( lpEditResize->bPrompt1Visible )
				{
					SetWindowText(hWndPrompt1,lpEditResize->szPrompt1Text);
				}
				else
				{
					ShowWindow(hWndPrompt1,SW_HIDE);
				}

				if ( lpEditResize->bField1Visible )
				{
					SetWindowText(hWndField1,lpEditResize->szField1Text);
				}
				else
				{
					ShowWindow(hWndField1,SW_HIDE);
				}

				if ( lpEditResize->bPrompt2Visible )
				{
					SetWindowText(hWndPrompt2,lpEditResize->szPrompt2Text);
				}
				else
				{
					ShowWindow(hWndPrompt2,SW_HIDE);
				}

				if ( lpEditResize->bField2Visible )
				{
					SetWindowText(hWndField2,lpEditResize->szField2Text);
				}
				else
				{
					ShowWindow(hWndField2,SW_HIDE);
				}

				LocalFree(lpEditResize);
			}
			else
			{
				lplpszTexts = (LPTSTR FAR *)lParam;

				if ( lplpszTexts[0] == NULL )
				{
					ShowWindow(hWndField1,SW_HIDE);
				}
				else
				{
					Edit_SetText(hWndField1,lplpszTexts[0]);
				}
				
				if ( lplpszTexts[1] == NULL )
				{
					ShowWindow(hWndField2,SW_HIDE);
				}
				else
				{
					Edit_SetText(hWndField2,lplpszTexts[1]);
				}

				if ( lplpszTexts[2] == NULL )
				{
					ShowWindow(hWndPrompt1,SW_HIDE);
				}
				else
				{
					Edit_SetText(hWndPrompt1,lplpszTexts[2]);
				}

				if ( lplpszTexts[3] == NULL )
				{
					ShowWindow(hWndPrompt2,SW_HIDE);
				}
				else
				{
					Edit_SetText(hWndPrompt2,lplpszTexts[3]);
				}
			}

			PostMessage(hwnd,UM_INIT,0,0L);

			return(TRUE);

		case UM_INIT:
				
			// If field 1 exists and is visible
			if ( ( hWndField1 != NULL ) &&
				 ( IsWindowVisible(hWndField1) ) )
			{
				SetFocus(hWndField1);
			
				Edit_SetSel(hWndField1,0,-1);
			
				hWndCtl = hWndField1;

				// Return FALSE since focus was manually set
				return(FALSE);
			}
			else if ( ( hWndField2 != NULL ) &&
					  ( IsWindowVisible(hWndField2) ) )
			{
				SetFocus(hWndField2);
			
				Edit_SetSel(hWndField2,0,-1);
			
				hWndCtl = hWndField2;

				// Return FALSE since focus was manually set
				return(FALSE);
			}

			// Return TRUE to let system set focus
			return(TRUE);

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDOK:

					if ( lplpszTexts[0] != NULL )
					{
						Edit_GetText(hWndField1,lplpszTexts[0],MAX_PATH);
					}

					if ( lplpszTexts[1] != NULL )
					{
						Edit_GetText(hWndField2,lplpszTexts[1],MAX_PATH);
					}

					l_bDialogWasOKed = TRUE;

					// Exit the dialog
					ExitDialog();

					break;

				case IDC_BEHIND:
				case IDCANCEL:

					l_bDialogWasOKed = FALSE;

					// Exit the dialog
					ExitDialog();

					break;

				case IDC_POPUP:

					if ( ( GetFocus() == hWndField1 ) &&
						 IsWindowVisible(hWndField2) )
					{
						SetFocus(hWndField2);
						Edit_SetSel(hWndField2,0,-1);
						hWndCtl = hWndField2;
					}
					else
					{
						SendMessage(hwnd,WM_COMMAND,IDOK,0L);
					}

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------


BOOL InvokeEditDialog(LPTSTR FAR * lplpszTexts)
{
	g_hdlg = CreateChosenDialog(g_hInstance,
								EditDialogChoices,
								g_hwnd,
								EditDlgProc,
								(LPARAM)lplpszTexts);

	WaitUntilDialogDoneSkip(FALSE);

	return(l_bDialogWasOKed);
}

