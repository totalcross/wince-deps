
//--------------------------------------------------------------------
// FILENAME:        RS232.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header for RS232.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef RS232_H_

#define RS232_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define RS232_SEND_SERIAL


DWORD dwThreadId;


BOOL WINAPI Rs232ReceiveFile(LPVOID lpVoid);

BOOL WINAPI Rs232SendFile(LPVOID lpVoid);

void Rs232Abort(void);

BOOL IsRs232Done(void);

void Rs232SetBaud(int id);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef RS232_H_  */
