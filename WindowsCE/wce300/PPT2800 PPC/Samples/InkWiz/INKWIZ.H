
//--------------------------------------------------------------------
// FILENAME:        INKWIZ.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Global application header file
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef INKWIZ_H_

#define INKWIZ_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("InkWiz")
#define APP_TITLE TEXT("InkWIZ File Inquisition")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#define WM_GETFIRSTKEY	WM_APP+100

#define SMALL_CX_BITMAP		16
#define SMALL_CY_BITMAP		16
#define LARGE_CX_BITMAP		32
#define LARGE_CY_BITMAP		32

#define NUM_BITMAPS			3


//----------------------------------------------------------------------------
// Macros
//----------------------------------------------------------------------------

#define DIM(x) ( sizeof(x)/sizeof(x[0]) )

#define Progress_SetPos(hwndCtl,nPos) ((void)SendMessage((hwndCtl),PBM_SETPOS,(WPARAM)(nPos),0L))

#define WM_XFER_NOTIFY				WM_APP+200

#define XFER_HEADER			'H'
#define XFER_DATA			'D'
#define XFER_TRAILER		'T'
#define XFER_HEADER_REPLY	'h'
#define XFER_DATA_REPLY		'd'
#define XFER_END_ACK        'a'

#define XFER_IRSOCK_RECV			100
#define XFER_IRSOCK_SEND			101
#define XFER_SERIAL1_SEND			201
#define XFER_SERIAL2_SEND			202
#define XFER_SERIAL3_SEND			203
#define XFER_SERIAL4_SEND			204
#define XFER_SERIAL1_RECV			301
#define XFER_SERIAL2_RECV			302
#define XFER_SERIAL3_RECV			303
#define XFER_SERIAL4_RECV			304
#define XFER_IRCOMM_SEND			400
#define XFER_IRCOMM_RECV			401
#define XFER_CRADLE_SEND			500
#define XFER_CRADLE_RECV			501
#define XFER_WINSOCK_SEND			600
#define XFER_WINSOCK_RECV			601

#define MAXRETRIES 3 //10

#define XFER_CONNECTING_TO_RECV		1
#define XFER_CONNECTED_TO_RECV		3
#define XFER_AWAITING_HEADER		4
#define XFER_AWAITING_DATA			5
#define XFER_SET_PROGRESS			6
#define XFER_AWAITING_TRAILER		7
#define XFER_RECEIVE_COMPLETED		8
#define XFER_CONNECTING_TO_SEND		9
#define XFER_SENDING_TRAILER		11
#define XFER_ATTEMPT				12
#define XFER_CONNECTED_TO_SEND		13
#define XFER_SENDING_HEADER			14
#define XFER_SENDING_DATA			15
#define XFER_SEND_COMPLETED			16
#define XFER_START					17
#define XFER_STOP					18
#define XFER_CLEANUP				19
#define XFER_OKNOCANCEL				20
#define XFER_CANCELNOOK				21
#define XFER_AWAITING_REPLY			22
#define XFER_SENDING_REPLY			23
#define XFER_TITLE					24
#define XFER_MESSAGE				25
#define XFER_STATUS					26
#define XFER_DEBUG					27

#define MAXDATAINRECORD 2048

//----------------------------------------------------------------------------
// Typedefs
//----------------------------------------------------------------------------

typedef enum tagIMAGES
{
	IMAGE_CLOSED,
	IMAGE_WINDOW,
	IMAGE_CLASS,
	IMAGE_DOCUMENT,
	IMAGE_OPEN
} IMAGES;


typedef enum tagCOMMANDS
{
	COMMAND_ALL,

	COMMAND_OPEN,
	COMMAND_SAVE,
	COMMAND_PROPERTIES,
	COMMAND_DELETE,
	COMMAND_SEND,
	COMMAND_RECEIVE,
	COMMAND_CUT,
	COMMAND_COPY,
	COMMAND_PASTE,
	COMMAND_EXPAND,
	COMMAND_COLLAPSE,

	COMMAND_EXIT,

	NUMBER_OF_COMMANDS,
} COMMANDS;


typedef struct _XFERBASE
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
} XFERBASE, *LPXFERBASE;


typedef struct _XFERHEADER
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
	DWORD dwFileSize;
	char szFileName[64];
	char szRemoteName[64];
	char szIpAddr[16];
} XFERHEADER, *LPXFERHEADER;


typedef struct _XFERDATA
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
	int nDataLength;
	BYTE bData[MAXDATAINRECORD];
} XFERDATA, *LPXFERDATA;


typedef struct _XFERDATA_REPLY
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
	int nDataLength;
	BOOL bReceivedOK;
} XFERDATA_REPLY, *LPXFERDATA_REPLY;


typedef struct _XFERTRAILER
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
	DWORD dwFileSize;
	DWORD dwCheckSum;
} XFERTRAILER, *LPXFERTRAILER;


typedef struct _XFERENDACK
{
	char cRecordType;
	int nRecordLength;
	int nRecordNumber;
} XFERENDACK, *LPXFERENDACK;


//----------------------------------------------------------------------------
// Global Variables
//----------------------------------------------------------------------------


COMMANDS g_MainCommand;

HWND g_hxferdlg;

int g_nXferType;


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef INKWIZ_H_    */
