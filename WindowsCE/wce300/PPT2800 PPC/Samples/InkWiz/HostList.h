
//--------------------------------------------------------------------
// FILENAME:        HOSTLIST.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for HOSTLIST.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef HOSTLIST_H_

#define HOSTLIST_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL InvokeHostListView(void);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef HOSTLIST_H_ */


