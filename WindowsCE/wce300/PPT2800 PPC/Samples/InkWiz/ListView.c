
//--------------------------------------------------------------------
// FILENAME:        ListView.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains list view handling functions.
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\StdExec.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"
#include "..\common\StdAbout.h"

#include "resource.h"

#include "listview.h"

#include "inkwiz.h"


//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static TCHAR l_szListTitle[MAX_PATH];

static DIALOG_CHOICE ListDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_LIST_PC_STD,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_LIST_HPC_STD,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_LIST_PPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_LIST_PPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_LIST_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_LIST_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_LIST_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_LIST_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_LIST_7500_TSK,	NULL,	UI_STYLE_DEFAULT }
};


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

int ListView_GetSelectedItem(HWND hWndList)
{
	int iItem;
	
	iItem = ListView_GetNextItem(hWndList,-1,UI_SELECTED);

	return(iItem);
}


void ListView_SelectItem(HWND hWndList,
						 int iItem)
{
	SetFocus(hWndList);

	ListView_EnsureVisible(hWndList,iItem,FALSE);
	
	ListView_SetItemState(hWndList,
						  iItem,
						  LVIS_SELECTED|LVIS_FOCUSED,
						  LVIS_SELECTED|LVIS_FOCUSED);
	
	ListView_Update(hWndList,iItem);
}


LRESULT CALLBACK ListDlgProc(HWND hwnd,
							 UINT uMsg,
							 WPARAM wParam,
							 LPARAM lParam)
{
	static LPLISTPROCS lpListProcs = NULL;
	static BOOL bModified = FALSE;

	LPINT lpnSelectedItem;
	HWND hWndList;

	hWndList = GetDlgItem(hwnd,IDC_PARAMETERS);

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Allocate save info area for selected index in list
			lpnSelectedItem = LocalAlloc(LPTR,sizeof(int));

			// If the allocation was successful
			if ( lpnSelectedItem != NULL )
			{
				// Get the current list item
				*lpnSelectedItem = ListView_GetSelectedItem(hWndList);
			}

			// Resize this dialog, saving info if allocated
			ResizeDialog(lpnSelectedItem);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			SetFocus(hWndList);
			
			// Set focus to the selected list view item
			ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);
				
			lpListProcs = (LPLISTPROCS)lParam;

			if ( ( lpListProcs != NULL ) &&
				 ( lpListProcs->lpInitializeListView != NULL ) )
			{
				 lpListProcs->lpInitializeListView(hWndList);
			}
			
			lpnSelectedItem = (LPINT)GetDialogSaveInfo();

			if ( lpnSelectedItem != NULL )
			{
				// Set focus to list view and its first item
				ListView_SelectItem(hWndList,*lpnSelectedItem);

				LocalFree(lpnSelectedItem);
			}
			else
			{
				// Set focus to list view and its first item
				ListView_SelectItem(hWndList,0);
			}
			
			SetWindowText(hwnd,l_szListTitle);
	
			// Return FALSE since focus was manually set
			return(FALSE);

		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				
				switch(pLvdi->hdr.code)
				{
					case NM_CLICK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

							switch(GetUIStyle())
							{
								case UI_STYLE_PEN:
							
									if ( pLvdi->hdr.code != NM_DBLCLK )
									{
										ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
										break;
									}

									// Fall through

								case UI_STYLE_KEYPAD:
								case UI_STYLE_FINGER:
									
									PostMessage(hwnd,WM_COMMAND,IDC_EDIT,0);
									
									break;								
							}

						}
						break;

						// Fall through

					case LVN_ITEMACTIVATE:
						
						switch(GetUIStyle())
						{
							case UI_STYLE_KEYPAD:
							case UI_STYLE_FINGER:

								PostMessage(hwnd,WM_COMMAND,IDC_EDIT,0);
								
								break;
						}

						break;
					
				}
			}
			break;

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_UP:

					ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
					
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDC_DOWN:

					ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));

					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDC_RIGHT:
				case IDC_EDIT:
					
					SetFocus(hWndList);

					if ( ( lpListProcs != NULL ) &&
						 ( lpListProcs->lpEditListItem != NULL ) )
					{
						int iItem = ListView_GetSelectedItem(hWndList);

						if ( iItem >= 0 )
						{
							if ( lpListProcs->lpEditListItem(hWndList,iItem) )
							{
								bModified = TRUE;
							}
						}
					}
					
					break;

				case IDC_DELETE:
					
					SetFocus(hWndList);
					
					if ( ( lpListProcs != NULL ) &&
						 ( lpListProcs->lpDeleteListItem != NULL ) )
					{
						int iItem = ListView_GetSelectedItem(hWndList);

						if ( iItem >= 0 )
						{
							if ( lpListProcs->lpDeleteListItem(hWndList,iItem) )
							{
								bModified = TRUE;
							}
						}
					}

					break;

				case IDC_ADD:
					
					SetFocus(hWndList);
					
					if ( ( lpListProcs != NULL ) &&
						 ( lpListProcs->lpAddNewListItem != NULL ) )
					{
						if ( lpListProcs->lpAddNewListItem(hWndList) &&
							 ( lpListProcs->lpEditListItem != NULL ) )
						{
							if ( lpListProcs->lpEditListItem(hWndList,
														ListView_GetSelectedItem(hWndList)) )
							{
								bModified = TRUE;
							}
							else
							{
								if ( lpListProcs->lpDeleteListItem != NULL )
								{
									lpListProcs->lpDeleteListItem(hWndList,
																  ListView_GetSelectedItem(hWndList));
								}
							}
						}
					}
					
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

							InvokePopUpMenu(hwnd,IDM_LISTPOPUP,0,0,0);
							
							break;

						case UI_STYLE_PEN:
						case UI_STYLE_PC:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;
				
				case IDOK:

					if ( ( lpListProcs != NULL ) &&
						( lpListProcs->lpExitListView != NULL ) )
					{
						lpListProcs->lpExitListView(hWndList);
					}

					ExitDialog();

					break;

				case IDC_BEHIND:
				case IDCANCEL:

					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}



BOOL InvokeListView(LPTSTR lpszTitle,
					LPLISTPROCS lpListProcs)
{
	TEXTCPY(l_szListTitle,lpszTitle);

	g_hdlg = CreateChosenDialog(g_hInstance,
								ListDialogChoices,
								g_hwnd,
								ListDlgProc,
								(LPARAM)lpListProcs);

	WaitUntilDialogDone();
	
	return(TRUE);
}

