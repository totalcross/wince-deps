
//--------------------------------------------------------------------
// FILENAME:            PROPS.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains functions to get file properties
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\TreeView.h"

#include "inkwiz.h"


static BOOL bInProgress = FALSE;


DIALOG_CHOICE PropertiesDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_PROPERTIES_PC_STD,	NULL },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_PROPERTIES_HPC_STD,	NULL },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_PROPERTIES_PPC_STD,	NULL },
	{ ST_PPC_SIP,	IDD_PROPERTIES_PPC_SIP,	NULL },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_PROPERTIES_7200_STD,NULL },
	{ ST_7200_TSK,	IDD_PROPERTIES_7200_TSK,NULL },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_PROPERTIES_7500_STD,NULL },
	{ ST_7500_TSK,	IDD_PROPERTIES_7500_TSK,NULL },
#endif
	{ ST_UNKNOWN,	IDD_PROPERTIES_7500_TSK,NULL }
};


static WIN32_FIND_DATA find_data;


//----------------------------------------------------------------------------
// GetFileProperties
//----------------------------------------------------------------------------

BOOL GetFileProperties(HWND hwnd)
{
    HANDLE fileHandle;

	fileHandle = FindFirstFile(g_ii.szFullSpec,&find_data);
	
	// Find the file in the directory, if we cannot
	if ( fileHandle == INVALID_HANDLE_VALUE )
	{
		// Return failure to get properties
		return(FALSE);
	}

	// Close the find handle
	FindClose(fileHandle);
	
	// Get states for read-only, and hidden attributes
	if ( find_data.dwFileAttributes & FILE_ATTRIBUTE_READONLY )
	{
		g_ii.dwReadOnly = BST_CHECKED;
	}
	else
	{
		g_ii.dwReadOnly = BST_UNCHECKED;
	}
	if ( find_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN )
	{
		g_ii.dwHidden = BST_CHECKED;
	}
	else
	{
		g_ii.dwHidden = BST_UNCHECKED;
	}
	
	// Save file size 
	g_ii.dwFileSize = find_data.nFileSizeLow;

	// Format file size string
	TEXTSPRINTF(g_ii.szFileSize,TEXT("%lu"),find_data.nFileSizeLow);

	// Return success getting properties
	return(TRUE);
}
		

//----------------------------------------------------------------------------
// GetFolderProperties
//----------------------------------------------------------------------------

BOOL GetFolderProperties(HWND hwnd)
{
	ULARGE_INTEGER uliFBATC,uliTNB,uliTNFB;

    if ( TEXTCMP(g_ii.szFullSpec,TEXT("\\")) == 0 )
	{
		g_ii.dwReadOnly = BST_UNCHECKED;
		
		g_ii.dwHidden = BST_UNCHECKED;
		
		if ( !GetDiskFreeSpaceEx(NULL,&uliFBATC,&uliTNB,&uliTNFB) )
		{
			return(FALSE);
		}
	}
	else
	{
		HANDLE fileHandle;

		fileHandle = FindFirstFile(g_ii.szFullSpec,&find_data);
		
		// Find the file in the directory, if we cannot
		if ( fileHandle == INVALID_HANDLE_VALUE )
		{
			// Return failure to get properties
			return(FALSE);
		}

		// Close the find handle
		FindClose(fileHandle);
		
		// Get states for read-only, and hidden attributes
		if ( find_data.dwFileAttributes & FILE_ATTRIBUTE_READONLY )
		{
			g_ii.dwReadOnly = BST_CHECKED;
		}
		else
		{
			g_ii.dwReadOnly = BST_UNCHECKED;
		}
		if ( find_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN )
		{
			g_ii.dwHidden = BST_CHECKED;
		}
		else
		{
			g_ii.dwHidden = BST_UNCHECKED;
		}

		if ( !GetDiskFreeSpaceEx(g_ii.szPathName,&uliFBATC,&uliTNB,&uliTNFB) )
		{
			return(FALSE);
		}
	}

	// Get root directory free space
	g_ii.dwFileSize = uliTNFB.LowPart;

	// Format file size string
	TEXTSPRINTF(g_ii.szFileSize,TEXT("%lu"),g_ii.dwFileSize);

	// Return success getting properties
	return(TRUE);
}
		

//----------------------------------------------------------------------------
// DisplayFileProperties
//----------------------------------------------------------------------------

void DisplayFileProperties(DLGPROC PropertiesDlgProc)
{
	TCHAR szExtension[MAX_PATH];
	TCHAR szExtKey[MAX_PATH];

	DWORD retCode;
	HKEY hKey;
	DWORD dwType;
	DWORD dwSize;

	if ( bInProgress )
	{
		return;
	}
	
	bInProgress = TRUE;
	
	// Assume file type is unknown
	TEXTCPY(g_ii.szFileType,TEXT("Unknown"));

	// If the file has an extension
	if ( GetExtension(szExtension,
					  TEXTSIZEOF(szExtension),
					  g_ii.szFullSpec) )
	{
		// Open the extension as a registry key
		retCode = RegOpenKeyEx(HKEY_CLASSES_ROOT,
							   szExtension,
							   0,
							   KEY_EXECUTE,
							   &hKey);
		
		// If we successfully opened the key
		if ( !retCode )
		{
			// Set the size of the key value buffer
			dwSize = TEXTSIZEOF(szExtKey);

			// Get the key value
			retCode = RegQueryValueEx(hKey,
									  NULL,
									  NULL,
									  &dwType,
									  (LPSTR)szExtKey,
									  &dwSize);

			// Close the key
			RegCloseKey(hKey);

			// If we successfully read the key
			if ( !retCode )
			{
				// Open the value of the key as a new key
				retCode = RegOpenKeyEx(HKEY_CLASSES_ROOT,
									   szExtKey,
									   0,
									   KEY_EXECUTE,
									   &hKey);

				// If we successfully opened the key
				if ( !retCode )
				{
					// Set the size of the key value buffer
					dwSize = TEXTSIZEOF(szExtKey);

					// Get the key value, this is the application type
					retCode = RegQueryValueEx(hKey,
											  NULL,
											  NULL,
											  &dwType,
											  (LPSTR)g_ii.szFileType,
											  &dwSize);

				}

				// Close the key
				RegCloseKey(hKey);
			}
		}
	}

	// Get the properties information
	if ( GetFileProperties(g_hwnd) )
	{

		g_hdlg = CreateChosenDialog(g_hInstance,
									PropertiesDialogChoices,
									g_hwnd,
									PropertiesDlgProc,
									0);
		
		WaitUntilDialogDoneSkip(FALSE);
	}
	else
	{
		// Report the error to the user
		MyError(TEXT("Cannot get file info"),
			    g_ii.szFullSpec);

	}

	bInProgress = FALSE;
}


//----------------------------------------------------------------------------
// DisplayFolderProperties
//----------------------------------------------------------------------------

void DisplayFolderProperties(DLGPROC PropertiesDlgProc)
{
	if ( bInProgress )
	{
		return;
	}
	
	bInProgress = TRUE;
	
	// Assume file type is unknown
	TEXTCPY(g_ii.szFileType,TEXT("Folder"));

	// Get the properties information
	if ( GetFolderProperties(g_hwnd) )
	{

		g_hdlg = CreateChosenDialog(g_hInstance,
									PropertiesDialogChoices,
									g_hwnd,
									PropertiesDlgProc,
									0);
		
		WaitUntilDialogDoneSkip(FALSE);
	}
	else
	{
		// Report the error to the user
		MyError(TEXT("Cannot get folder info"),
			    g_ii.szFullSpec);

	}

	bInProgress = FALSE;
}


//----------------------------------------------------------------------------
// PropertiesDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK PropertiesDlgProc(HWND hwnd,
								   UINT uMsg,
								   WPARAM wParam,
								   LPARAM lParam)
{
    // Select activity based on message
	switch(uMsg)
    {
		case UM_RESIZE:
			
			ResizeDialog(NULL);

			break;

        // Initialize dialog message
		case WM_INITDIALOG:
			
			MaxDialog(hwnd);

			// Save window handle as window handle of active dialog globally
			g_hdlg = hwnd;

			SetFocus(g_hdlg);

			// Defer further initialization until window is active
			PostMessage(hwnd,WM_GETFIRSTKEY,0,0);

			if ( g_ii.bPathSelected )
			{
				SetWindowText(GetDlgItem(hwnd,IDC_TEXT_SIZE),TEXT("Free:"));
			}

	        // Return indication that message was processed
			return(TRUE);

		case WM_GETFIRSTKEY:
            
			// Fill in controls with property data
			SetDlgItemText(hwnd,IDC_FILENAME,g_ii.szItemName);
            SetDlgItemText(hwnd,IDC_FILEPATH,g_ii.szPathName);
            SetDlgItemText(hwnd,IDC_FILETYPE,g_ii.szFileType);
			SetDlgItemText(hwnd,IDC_FILESIZE,g_ii.szFileSize);
			Button_SetCheck(GetDlgItem(hwnd,IDC_READONLY),g_ii.dwReadOnly);
			Button_SetCheck(GetDlgItem(hwnd,IDC_HIDDEN),g_ii.dwHidden);

	        // Return indication that message was processed
			return(TRUE);

        case WM_COMMAND:
            
			// Select activity based on command id
			switch (LOWORD(wParam))
            {
				case IDC_BEHIND:	// use VK_ESCAPE to exit
				case IDC_POPUP:
                case IDOK:
				case IDCANCEL:
					
					// Exit the dialog
					ExitDialog();

                    break;
            }

			// Return indication that message was processed
            return (TRUE);
    }

	// Return indication that message was not processed
    return(FALSE);
}


