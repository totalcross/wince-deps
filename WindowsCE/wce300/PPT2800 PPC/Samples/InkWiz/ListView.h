
//--------------------------------------------------------------------
// FILENAME:        LISTVIEW.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for LISTVIEW.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef LISTVIEW_H_

#define LISTVIEW_H_

#ifdef __cplusplus
extern "C"
{
#endif



//----------------------------------------------------------------------------
// Global defines
//----------------------------------------------------------------------------

#define UI_SELECTED ( LVNI_SELECTED | LVNI_FOCUSED )


//----------------------------------------------------------------------------
// Global types
//----------------------------------------------------------------------------

typedef struct tagLISTPROCS
{
	BOOL (*lpInitializeListView)(HWND hWndList);
	BOOL (*lpGetListItem)(int nRow,int nCol,LPTSTR lpszItem,DWORD dwSize);
	BOOL (*lpAddNewListItem)(HWND hWndList);
	BOOL (*lpDeleteListItem)(HWND hWndList,int nItem);
	BOOL (*lpEditListItem)(HWND hWndList,int nItem);
	BOOL (*lpExitListView)(HWND hWndList);
	
} LISTPROCS;

typedef LISTPROCS FAR * LPLISTPROCS;


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

int ListView_GetSelectedItem(HWND hWndList);

void ListView_SelectItem(HWND hWndList,int iItem);

BOOL InvokeListView(LPTSTR lpszTitle,LPLISTPROCS lpListProcs);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef LISTVIEW_H_ */
