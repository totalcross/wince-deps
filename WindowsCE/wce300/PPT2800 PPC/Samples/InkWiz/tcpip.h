
//--------------------------------------------------------------------
// FILENAME:        TCPIP.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for TCPIP.H
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef TCPIP_H_

#define TCPIP_H_

#ifdef __cplusplus
extern "C"
{
#endif



//----------------------------------------------------------------------------
// Global defines
//----------------------------------------------------------------------------

#define MAX_HOSTS 10
#define MAX_IPADDRS 10


//----------------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------------

extern TCHAR g_szRemoteName[MAX_PATH];
extern TCHAR g_szIpAddr[MAX_PATH];


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL SetHeaderInfo(LPXFERHEADER lpXferHeader);

void ExtractHeaderInfo(LPXFERHEADER lpXferHeader);

void SetHostAndIpAddr(LPTSTR lpszHost,LPTSTR lpszIpAddr,BOOL bReportError);

void GetRegIpAddr(LPTSTR lpszHost,
				  int nIndex,
				  LPTSTR lpszIpAddr,
				  DWORD dwSize);

BOOL UsingDNS(LPTSTR lpszHost);

BOOL HostInReg(LPTSTR lpszHost);

BOOL ValidateIpAddr(LPTSTR lpszIpAddr);

void GetIpAddr(LPTSTR lpszHost,int nIndex,LPTSTR lpszIpAddr,DWORD dwSize);

void GetHost(int nIndex,LPTSTR lpszHost,DWORD dwSize);

void GetLocalNameAndIpAddr(LPTSTR lpszName,
						   DWORD dwNameSize,
						   LPTSTR lpszIpAddr,
						   DWORD dwIpAddrSize);

BOOL ClearAllHosts(void);


void AddWinsockHostsToMenu(HMENU hMainMenu,
						   int level1,
						   int level2,
						   int level3,
						   int level4);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef TCPIP_H_ */
