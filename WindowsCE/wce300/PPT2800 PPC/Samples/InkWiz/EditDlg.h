
//--------------------------------------------------------------------
// FILENAME:        EDITDLG.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header fiel for EDITDLG.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef EDITDLG_H_

#define EDITDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL InvokeEditDialog(LPTSTR FAR * lplpszTexts);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef EDITDLG_H_ */
