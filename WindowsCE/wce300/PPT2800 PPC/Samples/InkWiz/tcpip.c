
//--------------------------------------------------------------------
// FILENAME:        TCPIP.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains functions to handle TCP/IP addresses
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>

#include <winsock.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"

#include "inkwiz.h"

#include "tcpip.h"


//----------------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------------

TCHAR g_szRemoteName[MAX_PATH];
TCHAR g_szIpAddr[MAX_PATH];


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL SetHeaderInfo(LPXFERHEADER lpXferHeader)
{
	WSADATA wsadata;
	LPHOSTENT lphostent;
		
	lpXferHeader->szRemoteName[0] = '\0';
	lpXferHeader->szIpAddr[0] = '\0';

	WSAStartup(0x101,&wsadata);
	
	if ( gethostname(lpXferHeader->szRemoteName,
					 sizeof(lpXferHeader->szRemoteName)-1) != ERROR_SUCCESS )
	{
		WSAError(TEXT("gethostname"));

		WSACleanup();

		return(FALSE);
	}

	lpXferHeader->szRemoteName[sizeof(lpXferHeader->szRemoteName)-1] = '\0';

	lphostent = gethostbyname(lpXferHeader->szRemoteName);

	if ( lphostent == NULL )
	{
		WSAError(TEXT("gethostbyname"));

		WSACleanup();

		return(FALSE);
	}

	if ( lphostent->h_addr_list[0] != NULL )
	{
		IN_ADDR inaddr;
		LPSTR lpszIpAddr;

		inaddr.S_un.S_addr = *(ULONG *)(lphostent->h_addr_list[0]);			

		lpszIpAddr = inet_ntoa(inaddr);

		if ( lpszIpAddr == NULL )
		{
			WSAError(TEXT("inet_ntoa"));

			WSACleanup();

			return(FALSE);
		}

		strncpy(lpXferHeader->szIpAddr,lpszIpAddr,sizeof(lpXferHeader->szIpAddr)-1);
		lpXferHeader->szIpAddr[sizeof(lpXferHeader->szIpAddr)-1] = '\0';
	}

	WSACleanup();

	return(TRUE);
}


void ExtractHeaderInfo(LPXFERHEADER lpXferHeader)
{
#ifdef UNICODE

	mbstowcs(g_szRemoteName,lpXferHeader->szRemoteName,TEXTSIZEOF(g_szRemoteName)-1);
	g_szRemoteName[TEXTSIZEOF(g_szRemoteName)-1] = TEXT('\0');

	mbstowcs(g_szIpAddr,lpXferHeader->szIpAddr,TEXTSIZEOF(g_szIpAddr)-1);
	g_szIpAddr[TEXTSIZEOF(g_szIpAddr)-1] = TEXT('\0');
	
#else

	strncpy(g_szRemoteName,lpXferHeader->szRemoteName,sizeof(g_szRemoteName)-1);
	g_szRemoteName[sizeof(g_szRemoteName)-1] = '\0';
	
	strncpy(g_szIpAddr,lpXferHeader->szIpAddr,sizeof(g_szIpAddr)-1);
	g_szIpAddr[sizeof(g_szIpAddr)-1] = '\0';

#endif

	SetHostAndIpAddr(g_szRemoteName,g_szIpAddr,FALSE);

#ifdef _WIN32_WCE

	if ( g_hwndCB != NULL )
	{
		HWND hwndBand;

		hwndBand = CommandBands_GetCommandBar(g_hwndCB,0);

		AddWinsockHostsToMenu(CommandBar_GetMenu(hwndBand,0),0,7,3,-1);
	}

#else

	AddWinsockHostsToMenu(GetMenu(g_hwnd),0,7,3,-1);

#endif
}


static DWORD RegDeleteAllSubKeys(HKEY hKey)
{
	DWORD dwResult;

	while(TRUE)
	{
		TCHAR szName[MAX_PATH];
		DWORD dwTempSize = TEXTSIZEOF(szName);
		FILETIME FileTime;
		HKEY hSubKey;

		dwResult = RegEnumKeyEx(hKey,
								0,
								szName,
								&dwTempSize,
								NULL,
								NULL,
								NULL,
								&FileTime);

		if ( dwResult != ERROR_SUCCESS )
		{
			dwResult = ERROR_SUCCESS;

			break;
		}

		dwResult = RegOpenKeyEx(hKey,
								szName,
								0,
								KEY_ALL_ACCESS,
								&hSubKey);

		if ( dwResult != ERROR_SUCCESS )
		{
			break;
		}

		dwResult = RegDeleteAllSubKeys(hSubKey);

		if ( dwResult != ERROR_SUCCESS )
		{
			break;
		}

		RegCloseKey(hSubKey);

		dwResult = RegDeleteKey(hKey,szName);

		if ( dwResult != ERROR_SUCCESS )
		{
			break;
		}

	}

	return(dwResult);
}


static void SetHostAndIpAddrReg(LPTSTR lpszHost,
								LPTSTR lpszIpAddr)
{
	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;
	DWORD dwValueLen;
	DWORD dwDisp;

	TEXTCPY(szKey,TEXT("Software\\Symbol\\Hosts\\"));
	TEXTCAT(szKey,lpszHost);

	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_ALL_ACCESS,
							&hKey);

	if ( dwResult == ERROR_SUCCESS )
	{	
		RegDeleteAllSubKeys(hKey);

		RegCloseKey(hKey);
	}		
		
	TEXTCAT(szKey,TEXT("\\IpAddr"));

	dwResult = RegCreateKeyEx(HKEY_CURRENT_USER,
							  szKey,
							  0,
							  NULL,
							  REG_OPTION_NON_VOLATILE,
							  KEY_WRITE,
							  NULL,
							  &hKey,
							  &dwDisp);

	if ( dwResult == ERROR_SUCCESS )
	{
		dwValueLen = TEXTLEN(lpszIpAddr)*sizeof(TCHAR);

		dwResult = RegSetValueEx(hKey,
								lpszIpAddr,
								0,
								REG_SZ,
				                (LPBYTE)lpszIpAddr,
								dwValueLen);
	
		RegCloseKey(hKey);
	}
}
	


void SetHostAndIpAddr(LPTSTR lpszHost,
					  LPTSTR lpszIpAddr,
					  BOOL bReportError)
{
	if ( HostInReg(lpszHost) )
	{
		if ( bReportError )
		{
			MyMessage(TEXT("Duplicate Host"),lpszHost);
		}
		else
		{
			if ( ValidateIpAddr(lpszIpAddr) )
			{
				SetHostAndIpAddrReg(lpszHost,lpszIpAddr);
			}
		}
	}
	else
	{
		if ( ValidateIpAddr(lpszIpAddr) )
		{
			SetHostAndIpAddrReg(lpszHost,lpszIpAddr);
		}
		else
		{
			if ( bReportError )
			{
				MyMessage(lpszIpAddr,TEXT("Invalid IP Addr"));
			}
		}
	}
}


void GetHost(int nIndex,
			 LPTSTR lpszHost,
			 DWORD dwSize)
{
	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;
	FILETIME FileTime;

	*lpszHost = TEXT('\0');

	TEXTCPY(szKey,TEXT("Software\\Symbol\\Hosts"));

	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_READ,
							&hKey);

	if ( dwResult == ERROR_SUCCESS )
	{
		int i;
		
		for(i=0;i<=nIndex;i++)
		{
			DWORD dwTempSize = dwSize;

			dwResult = RegEnumKeyEx(hKey,
									i,
									lpszHost,
									&dwTempSize,
									NULL,
									NULL,
									NULL,
									&FileTime);

			if ( dwResult != ERROR_SUCCESS )
			{
				*lpszHost = TEXT('\0');

				break;
			}
		}

		RegCloseKey(hKey);
	}
}


void GetRegIpAddr(LPTSTR lpszHost,
				  int nIndex,
				  LPTSTR lpszIpAddr,
				  DWORD dwSize)
{
	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;
	DWORD dwTempSize;
	DWORD dwType;

	TEXTCPY(szKey,TEXT("Software\\Symbol\\Hosts\\"));
	TEXTCAT(szKey,lpszHost);
	TEXTCAT(szKey,TEXT("\\IpAddr"));

	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_READ,
							&hKey);

	if ( dwResult == ERROR_SUCCESS )
	{
		int i;

		for(i=0;i<=nIndex;i++)
		{
			TCHAR szName[MAX_PATH];
			DWORD dwNameSize = TEXTSIZEOF(szName);

			dwTempSize = dwSize;

			dwResult = RegEnumValue(hKey,
									i,
									lpszIpAddr,
									&dwTempSize,
									NULL,
									&dwType,
									(LPBYTE)szName,
									&dwNameSize);

			if ( dwResult != ERROR_SUCCESS )
			{
				break;
			}
		}
	}

	if ( dwResult != ERROR_SUCCESS )
	{
		*lpszIpAddr = TEXT('\0');
	}

	RegCloseKey(hKey);
}



BOOL HostInReg(LPTSTR lpszHost)
{
	TCHAR szIpAddr[MAX_PATH];

	GetRegIpAddr(lpszHost,0,szIpAddr,TEXTSIZEOF(szIpAddr));

	if ( szIpAddr[0] == TEXT('\0') )
	{
		return(FALSE);
	}

	return(TRUE);
}


BOOL ValidateIpAddr(LPTSTR lpszIpAddr)
{
	return(TRUE);
}


BOOL UsingDNS(LPTSTR lpszHost)
{
	TCHAR szIpAddr[MAX_PATH];

	GetRegIpAddr(lpszHost,0,szIpAddr,TEXTSIZEOF(szIpAddr));

	if ( szIpAddr[0] == TEXT('\0') )
	{
		return(FALSE);
	}

	if ( TEXTICMP(szIpAddr,TEXT("DNS")) == 0 )
	{
		return(TRUE);
	}

	return(FALSE);
}


void GetIpAddr(LPTSTR lpszHost,
			   int nIndex,
			   LPTSTR lpszIpAddr,
			   DWORD dwSize)
{
	static TCHAR szLastHostName[MAX_PATH] = TEXT("");
	static int nHostEntCount = 0;
	static TCHAR szIpAddrs[MAX_IPADDRS][MAX_PATH];

	LPHOSTENT lphostent;

	if ( !UsingDNS(lpszHost) )
	{
		GetRegIpAddr(lpszHost,nIndex,lpszIpAddr,dwSize);

		return;
	}

	if ( ( TEXTICMP(lpszHost,szLastHostName) != 0 ) ||
		 ( nHostEntCount == 0 ) || 
		 ( nIndex == 0 ) )
	{
		WSADATA wsadata;
		CHAR szHost[MAX_PATH];

		TEXTCPY(szLastHostName,lpszHost);

		WSAStartup(0x101,&wsadata);

#ifdef UNICODE
		wcstombs(szHost,lpszHost,sizeof(szHost)-1);
		szHost[sizeof(szHost)-1] = '\0';
#else
		strcpy(szHost,lpszHost);
#endif

		lphostent = gethostbyname(szHost);

		if ( lphostent == NULL )
		{
			DWORD dwErrNum;

			dwErrNum = WSAGetLastError();

			lpszIpAddr[0] = TEXT('\0');

			WSACleanup();

			return;
		}

		for(nHostEntCount=0;
			((lphostent->h_addr_list[nHostEntCount]!=NULL)&&
			 (nHostEntCount<MAX_IPADDRS));
			nHostEntCount++)
		{
			IN_ADDR inaddr;
			LPSTR lpszAddr;

			inaddr.S_un.S_addr = *(ULONG *)(lphostent->h_addr_list[nHostEntCount]);

			lpszAddr = inet_ntoa(inaddr);

#ifdef UNICODE
			mbstowcs(lpszIpAddr,lpszAddr,dwSize-1);
			lpszIpAddr[dwSize-1] = TEXT('\0');
#else
			strcpy(lpszIpAddr,lpszAddr);
#endif
			TEXTCPY(szIpAddrs[nHostEntCount],lpszIpAddr);
		}

		WSACleanup();
	}

	if ( nIndex >= nHostEntCount )
	{
		lpszIpAddr[0] = TEXT('\0');

		return;
	}

	TEXTCPY(lpszIpAddr,szIpAddrs[nIndex]);
}


BOOL ClearAllHosts(void)
{
	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;

	TEXTCPY(szKey,TEXT("Software\\Symbol\\Hosts"));

	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_ALL_ACCESS,
							&hKey);

	if ( dwResult == ERROR_SUCCESS )
	{
		dwResult = RegDeleteAllSubKeys(hKey);
		
		RegCloseKey(hKey);
	}

	if ( dwResult == ERROR_SUCCESS )
	{
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}


void AddWinsockHostsToMenu(HMENU hMainMenu,
						   int level1,
						   int level2,
						   int level3,
						   int level4)
{
	HMENU hlevel1;
	HMENU hlevel2;
	HMENU hlevel3;
	HMENU hlevel4;
	HMENU hbottom;

	int i;
	BOOL bAny = FALSE;

	if ( level1 != -1 )
	{
		hbottom = hlevel1 = GetSubMenu(hMainMenu,level1);

		if ( hlevel1 == NULL )
		{
			return;
		}

		if ( level2 != -1 )
		{
			hbottom = hlevel2 = GetSubMenu(hlevel1,level2);

			if ( hlevel2 == NULL )
			{
				return;
			}

			if ( level3 != -1 )
			{
				hbottom = hlevel3 = GetSubMenu(hlevel2,level3);

				if ( hlevel3 == NULL )
				{
					return;
				}

				if ( level4 != -1 )
				{
					hbottom = hlevel4 = GetSubMenu(hlevel3,level4);

					if ( hlevel4 == NULL )
					{
						return;
					}
				}
			}
		}
	}

	while ( DeleteMenu(hbottom,0,MF_BYPOSITION) )
	{
	}

	for(i=0;i<MAX_HOSTS;i++)
	{
		TCHAR szHostName[MAX_PATH];
		
		GetHost(i,szHostName,TEXTSIZEOF(szHostName));

		if ( szHostName[0] )
		{
			AppendMenu(hbottom,
					   MF_STRING|MF_ENABLED,
					   IDC_SEND_WINSOCK+i,
					   szHostName);

			bAny = TRUE;
		}
		else
		{
			break;
		}
	}

	if ( !bAny )
	{
		AppendMenu(hbottom,
				   MF_STRING|MF_GRAYED,
				   IDOK,
				   TEXT("<none>"));
	}
}


