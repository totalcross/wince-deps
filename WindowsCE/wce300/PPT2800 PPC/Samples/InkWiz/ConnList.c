
//--------------------------------------------------------------------
// FILENAME:            CONNLIST.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains routines to handle the network connections list
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include <winnetwk.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\StdExec.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdAbout.h"

#include "..\common\Treeview.h"
#include "..\common\Dir.h"

#include "listview.h"
#include "editdlg.h"

#include "connlist.h"

#include "inkwiz.h"

#include "tcpip.h"


//----------------------------------------------------------------------------
// Local defines
//----------------------------------------------------------------------------

#define NEWLOCALNAME	TEXT("New Local Name")
#define NEWUNCPATH		TEXT("New UNC Path")


//----------------------------------------------------------------------------
// Local function prototypes
//----------------------------------------------------------------------------

static BOOL InitConnectionList(void);
static void FreeConnectionList(void);

static BOOL InitializeConnectionListView(HWND hWndList);
static BOOL GetConnectionListItem(int nRow,int nCol,LPTSTR lpszItem,DWORD dwSize);
static BOOL AddNewConnectionListItem(HWND hWndList);
static BOOL DeleteConnectionListItem(HWND hWndList,int nItem);
static BOOL EditConnectionListItem(HWND hWndList,int nItem);
static BOOL ExitConnectionListView(HWND hWndList);


//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static int l_nConnectionListCount = 0;

static LPNETRESOURCE l_lpNetList = NULL;
static DWORD l_dwNetCount;
static DWORD l_dwNetSize;

static LISTPROCS ConnectionListProcs =
{
	InitializeConnectionListView,
	GetConnectionListItem,
	AddNewConnectionListItem,
	DeleteConnectionListItem,
	EditConnectionListItem,
	ExitConnectionListView
};


//----------------------------------------------------------------------------
// Local functions
//----------------------------------------------------------------------------


static BOOL AddConnectionListItem(HWND hWndList,
								  LPTSTR lpszLocalName,
								  LPTSTR lpszUncPath)
{
	LV_ITEM lvI;
	BOOL bResult;

	lvI.mask = LVIF_TEXT;
	lvI.iItem = l_nConnectionListCount;
	lvI.iSubItem = 0;
	lvI.pszText = lpszLocalName;
	lvI.cchTextMax = TEXTLEN(lpszLocalName);
	bResult = ( ListView_InsertItem(hWndList,&lvI) >= 0 );

	if ( bResult )
	{
		lvI.mask = LVIF_TEXT;
		lvI.iItem = l_nConnectionListCount;
		lvI.iSubItem = 1;
		lvI.pszText = lpszUncPath;
		lvI.cchTextMax = TEXTLEN(lpszUncPath);
        bResult = ( ListView_SetItem(hWndList,&lvI) >= 0 );
	}

	if ( bResult )
	{
		ListView_SetItemState(hWndList,
							  l_nConnectionListCount,
							  LVIS_SELECTED|LVIS_FOCUSED,
							  LVIS_SELECTED|LVIS_FOCUSED);

		l_nConnectionListCount++;
	}
	
	return(bResult);
}


static BOOL InitializeConnectionListView(HWND hWndList)
{
	RECT rect;
	LV_COLUMN lvC;
	TCHAR szLocalName[MAX_PATH];
	TCHAR szUncPath[MAX_PATH];
	BOOL bResult;
	int nWidth;

	if ( hWndList == NULL )
	{
		return(FALSE);
	}

	Button_Enable(GetDlgItem(g_hdlg,IDCANCEL),FALSE);

	GetClientRect(hWndList,&rect);

	nWidth = rect.right - rect.left;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM | LVCF_TEXT;
	lvC.fmt = LVCFMT_LEFT;

	lvC.iSubItem = 0;
	lvC.pszText = TEXT("Local Name");
	lvC.cx = nWidth/3;
	bResult = ListView_InsertColumn(hWndList,0,&lvC);
	bResult = FALSE;

	lvC.iSubItem = 1;
	lvC.pszText = TEXT("UNC Path");
	lvC.cx = nWidth*2/3;
	bResult = ListView_InsertColumn(hWndList,1,&lvC);
	bResult = FALSE;

	InitConnectionList();

	for(l_nConnectionListCount=0;
		GetConnectionListItem(l_nConnectionListCount,0,szLocalName,TEXTSIZEOF(szLocalName)) &&
		GetConnectionListItem(l_nConnectionListCount,1,szUncPath,TEXTSIZEOF(szUncPath));)
	{
		AddConnectionListItem(hWndList,szLocalName,szUncPath);
	}

	return(TRUE);
}


static BOOL InitConnectionList(void)
{
	DWORD dwResult;
	HANDLE hEnum;
	
	l_lpNetList = NULL;
	l_dwNetSize = 16384;
	l_dwNetCount = -1;

	SetCursor(LoadCursor(NULL,IDC_WAIT));

#ifdef _WIN32_WCE
	dwResult = WNetOpenEnum(RESOURCE_REMEMBERED,
							RESOURCETYPE_DISK,
							0,
							NULL,
							&hEnum);
#else
	dwResult = WNetOpenEnum(RESOURCE_CONNECTED,
							RESOURCETYPE_DISK,
							0,
							NULL,
							&hEnum);
#endif

	if ( dwResult == ERROR_SUCCESS )
	{
		l_lpNetList = LocalAlloc(LPTR,l_dwNetSize);

		if ( l_lpNetList != NULL )
		{
			dwResult = WNetEnumResource(hEnum,
										&l_dwNetCount,
										l_lpNetList,
										&l_dwNetSize);
		}

		WNetCloseEnum(hEnum);
	}

	SetCursor(NULL);

	if ( l_lpNetList == NULL )
	{
		return(FALSE);
	}
	else
	{
		return(TRUE);
	}
}



static void FreeConnectionList(void)
{
	if ( l_lpNetList != NULL )
	{
		LocalFree(l_lpNetList);

		l_lpNetList = NULL;
	}
}


void GetLocalName(int nIndex,LPTSTR lpszLocalName,DWORD dwSize)
{
#if 0

	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;
	FILETIME FileTime;

	*lpszLocalName = TEXT('\0');

	TEXTCPY(szKey,TEXT("Comm\\Redir\\Connections"));

#ifdef _WIN32_WCE
	dwResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							szKey,
							0,
							KEY_READ,
							&hKey);
#else
	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_READ,
							&hKey);
#endif

	if ( dwResult == ERROR_SUCCESS )
	{
		int i;
		
		for(i=0;i<=nIndex;i++)
		{
			TCHAR szValue[MAX_PATH];
			DWORD dwValueSize = TEXTSIZEOF(szValue);
			DWORD dwTempSize = dwSize;
			DWORD dwType;

			dwResult = RegEnumValue(hKey,
									i,
									lpszLocalName,
									&dwTempSize,
									NULL,
									&dwType,
									szValue,
									&dwValueSize);

			if ( dwResult != ERROR_SUCCESS )
			{
				*lpszLocalName = TEXT('\0');

				break;
			}
		}

		RegCloseKey(hKey);
	}

#else

	if ( nIndex < (int)l_dwNetCount )
	{
		if ( l_lpNetList[nIndex].lpLocalName == NULL )
		{
			TEXTCPY(lpszLocalName,TEXT("<unknown>"));
		}
		else
		{
			TEXTNCPY(lpszLocalName,l_lpNetList[nIndex].lpLocalName,dwSize-1);
			lpszLocalName[dwSize-1] = TEXT('\0');
		}
	}
	else
	{
		*lpszLocalName = TEXT('\0');
	}

#endif
}


void GetUncPath(int nIndex,LPTSTR lpszUncPath,DWORD dwSize)
{
#if 0

	DWORD dwResult;
	TCHAR szKey[MAX_PATH];
	HKEY hKey;

	TEXTCPY(szKey,TEXT("Comm\\Redir\\Connections"));

#ifdef _WIN32_WCE
	dwResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							szKey,
							0,
							KEY_READ,
							&hKey);
#else
	dwResult = RegOpenKeyEx(HKEY_CURRENT_USER,
							szKey,
							0,
							KEY_READ,
							&hKey);
#endif

	if ( dwResult == ERROR_SUCCESS )
	{
		DWORD dwTempSize = dwSize;
		DWORD dwType;

		dwResult = RegQueryValueEx(hKey,
								   lpszLocalName,
								   NULL,
								   &dwType,
								   lpszUncPath,
								   &dwTempSize);
	}

	if ( dwResult != ERROR_SUCCESS )
	{
		*lpszUncPath = TEXT('\0');
	}

	RegCloseKey(hKey);

#else

	if ( nIndex < (int)l_dwNetCount )
	{
		TEXTNCPY(lpszUncPath,l_lpNetList[nIndex].lpRemoteName,dwSize-1);
		lpszUncPath[dwSize-1] = TEXT('\0');
	}
	else
	{
		*lpszUncPath = TEXT('\0');
	}

#endif

}


static BOOL GetConnectionListItem(int nRow,int nCol,LPTSTR lpszItem,DWORD dwSize)
{
	switch(nCol)
	{
		case 0:
			
			GetLocalName(nRow,lpszItem,dwSize);
			
			break;
		
		case 1:
			{
				TCHAR szLocalName[MAX_PATH] = TEXT("");

				GetLocalName(nRow,szLocalName,TEXTSIZEOF(szLocalName));
			
				GetUncPath(nRow,lpszItem,dwSize);
			}
			
			break;
	}

	if ( *lpszItem )
	{
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}


static BOOL MakeConnection(HWND hWndList,
						   LPTSTR lpszLocalName,
						   LPTSTR lpszUncPath)
{
	DWORD dwResult;
	NETRESOURCE nr;
	BOOL bResult;

	ZEROMEM(&nr,sizeof(nr));
	nr.lpLocalName = lpszLocalName;
	nr.lpRemoteName = lpszUncPath;

	SetCursor(LoadCursor(NULL,IDC_WAIT));

	dwResult = WNetAddConnection3(g_hwnd,&nr,NULL,NULL,CONNECT_UPDATE_PROFILE);

	SetCursor(NULL);

	if ( dwResult != ERROR_SUCCESS )
	{
		ReportError(TEXT("WNetAddConnection3"),dwResult);
		return(FALSE);
	}

	bResult = AddConnectionListItem(hWndList,lpszLocalName,lpszUncPath);

	return(bResult);
}


static BOOL AddNewConnectionListItem(HWND hWndList)
{
	BOOL bResult;

	bResult = AddConnectionListItem(hWndList,NEWLOCALNAME,NEWUNCPATH);

	return(bResult);
}


static BOOL DeleteConnectionListItem(HWND hWndList,int nItem)
{
	BOOL bResult;
	TCHAR szLocalName[MAX_PATH];
	TCHAR szUncPath[MAX_PATH];

	ListView_GetItemText(hWndList,nItem,0,szLocalName,TEXTSIZEOF(szLocalName));
	ListView_GetItemText(hWndList,nItem,1,szUncPath,TEXTSIZEOF(szUncPath));
	
	if ( ( TEXTCMP(szLocalName,NEWLOCALNAME) != 0 ) ||
		 ( TEXTCMP(szUncPath,NEWUNCPATH) != 0 ) )
	{
		DWORD dwResult;

		SetCursor(LoadCursor(NULL,IDC_WAIT));

		dwResult = WNetCancelConnection2(szLocalName,CONNECT_UPDATE_PROFILE,FALSE);

		SetCursor(NULL);

		if ( dwResult != ERROR_SUCCESS )
		{
			return(FALSE);
		}
	}

	bResult = ListView_DeleteItem(hWndList,nItem);

	if ( bResult )
	{
		l_nConnectionListCount--;

		if ( l_nConnectionListCount > 0 )
		{
			if ( nItem >= l_nConnectionListCount )
			{
				nItem = l_nConnectionListCount-1;
			}

			ListView_SetItemState(hWndList,
								  nItem,
								  LVIS_SELECTED|LVIS_FOCUSED,
								  LVIS_SELECTED|LVIS_FOCUSED);
		}
	}

	return(bResult);
}


static BOOL EditConnectionListItem(HWND hWndList,int nItem)
{
	TCHAR szLocalName[MAX_PATH];
	TCHAR szUncPath[MAX_PATH];
	LPTSTR szTexts[] = { szLocalName,
						 szUncPath,
						 TEXT("Local Name"),
						 TEXT("UNC Path") };
	BOOL bResult;
	HWND hOldDlg;
	TCHAR szOldLocalName[MAX_PATH];
	TCHAR szOldUncPath[MAX_PATH];

	ListView_GetItemText(hWndList,nItem,0,szLocalName,TEXTSIZEOF(szLocalName));
	ListView_GetItemText(hWndList,nItem,1,szUncPath,TEXTSIZEOF(szUncPath));

	ListView_GetItemText(hWndList,nItem,0,szOldLocalName,TEXTSIZEOF(szOldLocalName));
	ListView_GetItemText(hWndList,nItem,1,szOldUncPath,TEXTSIZEOF(szOldUncPath));

	hOldDlg = g_hdlg;
	
	bResult = InvokeEditDialog(szTexts);

	if ( bResult )
	{
		if ( ( TEXTCMP(szLocalName,szOldLocalName) != 0 ) ||
			 ( TEXTCMP(szUncPath,szOldUncPath) != 0 ) )
		{
			if ( g_hdlg != hOldDlg )
			{
				hWndList = GetDlgItem(g_hdlg,IDC_PARAMETERS);
			}
			else
			{
				bResult = DeleteConnectionListItem(hWndList,nItem);
			}

			if ( bResult )
			{
				MakeConnection(hWndList,szLocalName,szUncPath);
			}
		}
	}

	return(bResult);
}


BOOL ExitConnectionListView(HWND hWndList)
{
	SetCursor(LoadCursor(NULL,IDC_WAIT));

	TreeView_DeleteAllItems(g_hwndChild);
	
	GetDrives(g_hwndChild,&g_indexes);

#ifdef _WIN32_WCE
	TreeView_Default(g_hwndChild);
#endif

	SetCursor(NULL);

	return(TRUE);
}


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL InvokeConnectionListView(void)
{
	BOOL bResult;
	static BOOL bInProgress = FALSE;

	if ( bInProgress )
	{
		return(FALSE);
	}
	
	bInProgress = TRUE;
	
	bResult = InvokeListView(TEXT("Connection List"),&ConnectionListProcs);

	FreeConnectionList();

	bInProgress = FALSE;

	return(bResult);
}

