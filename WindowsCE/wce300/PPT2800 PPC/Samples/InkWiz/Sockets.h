
//--------------------------------------------------------------------
// FILENAME:        Sockets.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for Sockets.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef SOCKETS_H_

#define SOCKETS_H_

#ifdef __cplusplus
extern "C"
{
#endif



DWORD dwThreadId;
DWORD dwNonBlocking;
int optval;


BOOL WINAPI SocketsReceiveFile(LPVOID lpVoid);

BOOL WINAPI SocketsSendFile(LPVOID lpVoidd);

void SocketsAbort(void);

BOOL IsSocketsDone(void);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SOCKETS_H_  */
