
//--------------------------------------------------------------------
// FILENAME:            SOCKETS.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains functions to implement IrSock/WinSock file transfer
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------


#ifdef _WIN32_WCE

	#include <winsock.h>

	#ifndef _WIN32_WCE_EMULATION

		#include <af_irda.h>

	#endif

#else

	#include <winsock2.h>

	#include <af_irda.h>

#endif


#include <windows.h>
#include <windowsx.h>

#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"

#include "..\common\Treeview.h"

#include "props.h"
#include "xferdlg.h"

#include "Sockets.h"

#include "inkwiz.h"

#include "tcpip.h"


#ifdef WIN32_PLATFORM_7000
	#include "PwrCapi.h"
#endif
//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static DWORD l_dwBeginTime;

static BOOL l_bSocketsReqAbort = FALSE;
static BOOL bSocketsDone = FALSE;

static HANDLE hFile = NULL;

static SOCKET ServerSocket = INVALID_SOCKET;
static SOCKET ClientSocket = INVALID_SOCKET;

#define INKWIZ_PORT 5000

static XFERHEADER header;
static XFERHEADER header_reply;
static XFERDATA data;
static XFERTRAILER trailer;
static XFERENDACK end_ack;


#ifdef WIN32_PLATFORM_7000

LPFNPWR_OPEN			lpfnPowerOpen;
LPFNPWR_CLOSE			lpfnPowerClose;
LPFNPWR_PROHBTDEVSTATE	lpfnPowerProhibitDevState;  
LPFNPWR_ALLWDEVSTATE	lpfnPowerAllowDevState;  

HINSTANCE hPwrLib = NULL;

#endif


static int l_nRecord;
static int l_nRecords;

static LPTSTR l_lpszFullSpec = NULL;

#define MAXSOCKETSDATAINRECORD MAXDATAINRECORD // 128

static int l_nDataMax = MAXSOCKETSDATAINRECORD;


static BOOL TermPowerMgmt(void)
{

#ifdef WIN32_PLATFORM_7000

	DWORD dwResult;

	if ( hPwrLib == NULL )
	{
		return(FALSE);
	}

	if ( lpfnPowerAllowDevState != NULL )
	{
		dwResult = lpfnPowerAllowDevState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);

		if ( dwResult != E_PWR_SUCCESS )
		{
			MyError(TEXT("TermPowerMgmt"),TEXT("Can't allow"));
		}
	}

	if ( lpfnPowerClose != NULL )
	{
		dwResult = lpfnPowerClose();

		if ( dwResult != E_PWR_SUCCESS )
		{
			MyError(TEXT("TermPowerMgmt"),TEXT("Can't close"));
		}
	}

	lpfnPowerClose = NULL;
	lpfnPowerAllowDevState = NULL;
	lpfnPowerProhibitDevState = NULL;
	lpfnPowerOpen = NULL;

	FreeLibrary(hPwrLib);
	hPwrLib = NULL;

#endif

	return(TRUE);
}


static BOOL InitPowerMgmt(void)
{

#ifdef WIN32_PLATFORM_7000

	DWORD dwResult;

	hPwrLib = LoadLibrary(TEXT("PwrApi32.dll"));
	
	if (hPwrLib == NULL)
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't load PwrApi32.dll"));

		return(FALSE);
	}

	lpfnPowerOpen = (LPFNPWR_OPEN)GetProcAddress(hPwrLib,PWR_OPEN);
	
	if ( lpfnPowerOpen == NULL )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't get open address"));

		TermPowerMgmt();

		return(FALSE);
	}

	lpfnPowerProhibitDevState = (LPFNPWR_SETDEVSTATE)GetProcAddress(hPwrLib,PWR_PROHBTDEVSTATE);
	
	if ( lpfnPowerProhibitDevState == NULL )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't get prohibit address"));

		TermPowerMgmt();

		return(FALSE);
	}

	lpfnPowerAllowDevState = (LPFNPWR_SETDEVSTATE)GetProcAddress(hPwrLib,PWR_ALLWDEVSTATE);

	if ( lpfnPowerAllowDevState == NULL )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't get allow address"));

		TermPowerMgmt();

		return(FALSE);
	}

	lpfnPowerClose = (LPFNPWR_CLOSE)GetProcAddress(hPwrLib,PWR_CLOSE);

	if ( lpfnPowerClose == NULL )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't get close address"));

		TermPowerMgmt();

		return(FALSE);
	}

	dwResult = lpfnPowerOpen();
	
	if ( dwResult != E_PWR_SUCCESS )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't open"));

		TermPowerMgmt();

		return(FALSE);
	}

	dwResult = lpfnPowerProhibitDevState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);

	if ( dwResult != E_PWR_SUCCESS )
	{
		MyError(TEXT("InitPowerMgmt"),TEXT("Can't prohibit"));

		TermPowerMgmt();

		return(FALSE);
	}

#endif

	return(TRUE);
}


//----------------------------------------------------------------------------
// SocketsCleanUp
//----------------------------------------------------------------------------

static int SocketsCleanUp(BOOL bSuccess)
{
	// If there is an open client socket
	if ( ClientSocket != INVALID_SOCKET )
	{
		// Close the socket
		closesocket(ClientSocket);

		// Mark it not open
		ClientSocket = INVALID_SOCKET;
	}
	
	// If there is an open server socket
	if ( ServerSocket != INVALID_SOCKET )
	{
		// Close the socket
		closesocket(ServerSocket);

		// Mark it not open
		ServerSocket = INVALID_SOCKET;
	}
	
	TermPowerMgmt();	

	// If there is an open file
	if ( hFile != NULL )
	{
		// Close the file
		CloseHandle(hFile);

		// Mark it not open
		hFile = NULL;
	}
	
	// Indicate that sockets shutdown is complete
	bSocketsDone = TRUE;

	// If there is an xfer dialog in existence
	if ( g_hxferdlg )
	{
		// If the shutdown is due to a successful completion
		if ( bSuccess )
		{
			SendMessage(g_hxferdlg,WM_XFER_NOTIFY,XFER_OKNOCANCEL,0L);
		}
		else
		{
			int i;

			// Otherwise, simply OK the xfer dialog, since the session is already aborted
			SendMessage(g_hxferdlg,WM_COMMAND,IDOK,0L);

			for(i=0;i<100;i++)
			{
				Sleep(1);
				
				if ( g_hxferdlg == NULL )
				{
					break;
				}
			}
		}
	}

	WSACleanup();
	
	// Return what was passed in
	return(bSuccess);
}


//----------------------------------------------------------------------------
// SocketsRecv
//----------------------------------------------------------------------------

static int SocketsRecv(LPVOID lpData,
					   int nSize,
					   char cType,
					   int nNumber)
{
	int nRead;
	char FAR *lpcData = (char FAR *)lpData;
	int nTotal = 0;

	// Loop
	do
	{
		// Receive a block of data
		nRead = recv(ClientSocket,
					 (char FAR*)lpcData,
					 nSize,
					 0);

		// If the receive failed
		if ( nRead == SOCKET_ERROR )
		{
			DWORD dwErrNum;

			dwErrNum = WSAGetLastError();
			
			// If it was a real error
			if ( dwErrNum != WSAEWOULDBLOCK )
			{
				// Abort the session
				SocketsCleanUp(FALSE);
				
				switch(dwErrNum)
				{
					case WSAECONNRESET:

						// Report an error to the user
						MyError(TEXT("recv"),TEXT("Connection lost"));
						
						break;
				
					default:
						
						// Report an error to the user
						ReportError(TEXT("recv"),dwErrNum);
						
						break;
				}

				// Return failure
				return(FALSE);
			}
		}
		else
		{
			// Advance buffer pointer past data read
			lpcData += nRead;

			// Reduce remaining size to receive by data read
			nSize -= nRead;

			// Calculate total bytes received.
			nTotal += nRead;
		}

		// If there is additional data to be read
		if ( nSize )
		{
			// Sleep for half a second
			Sleep(500);
		}
		else
		{
			// Otherwise sleep for a shorter time
			Sleep(10);
		}

	  // Continue looping until aborted or all data received
	} while ( !l_bSocketsReqAbort && nSize );

	// If aborted or not all data received
	if ( l_bSocketsReqAbort || nSize )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}
	
	// If data read does not meet the requested information
	if ( ( ((LPXFERBASE)lpData)->nRecordLength != nTotal ) ||
		 ( ((LPXFERBASE)lpData)->cRecordType != cType ) ||
		 ( ((LPXFERBASE)lpData)->nRecordNumber != nNumber ) )
	{
		TCHAR szMsg[MAX_PATH];
				  
		// Abort the session
		SocketsCleanUp(FALSE);
			
		// Report an error to the user
		TEXTSPRINTF(szMsg,TEXT("Type = %c"),cType);
		MyError(TEXT("Bad record"),szMsg);

		// Return failure
		return(FALSE);
	}

	// Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// SocketsSend
//----------------------------------------------------------------------------

static int SocketsSend(LPVOID lpData,int nSize)
{
	char FAR * lpcData = (char FAR *)lpData;
	int nSent;

	// Loop
	do
	{
		// Send a block of data
		nSent = send(ClientSocket,lpcData,nSize,0);

		// If the send failed
		if ( nSent == SOCKET_ERROR )
		{
			DWORD dwErrNum;

			dwErrNum = WSAGetLastError();
			
			// If it was a real error
			if ( dwErrNum != WSAEWOULDBLOCK )
			{
				// Abort the session
				SocketsCleanUp(FALSE);

				// Report an error to the user
				ReportError(TEXT("send"),dwErrNum);

				// Return failure
				return(FALSE);
			}
		}
		else
		{
		    // Advance buffer pointer past data sent
		    lpcData += nSent;

		    // Reduce remaining size to send by data sent
		    nSize -= nSent;
		}

		// If there is additional data to be sent
		if ( nSize )
		{
			// Sleep for half a second
			Sleep(500);
		}
		else
		{
			// Otherwise sleep for a shorter time
			Sleep(10);
		}

	  // Continue looping until aborted or all data sent
	} while ( !l_bSocketsReqAbort && nSize );

	// If aborted or not all data sent
	if ( l_bSocketsReqAbort || nSize )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}
	
	// Return success
	return(TRUE);
}


//#ifdef _WIN32_WCE
#ifndef _WIN32_WCE_EMULATION

//static SOCKADDR_IRDA IrdaAddress = { AF_IRDA, 0,0,0,0, "InkWiz" };
static SOCKADDR_IRDA IrdaAddress;
//static SOCKADDR_IN InetAddress = { AF_INET, INKWIZ_PORT, { 0,0,0,0 }, "InkWiz" };
static SOCKADDR_IN InetAddress;

static int nAddrType;  // AF_IRDA, AF_INET

#endif
//#endif


//----------------------------------------------------------------------------
// BindSocket
//----------------------------------------------------------------------------

static DWORD BindSocket(SOCKET sock)
{
	DWORD result;

	// Open a socket for server mode
	switch(g_nXferType)
	{
//#ifdef _WIN32_WCE
#ifndef _WIN32_WCE_EMULATION

		case XFER_IRSOCK_SEND:
		case XFER_IRSOCK_RECV:

			{
				IrdaAddress.irdaAddressFamily = nAddrType;
				memset(IrdaAddress.irdaDeviceID,0,sizeof(IrdaAddress.irdaDeviceID));
				strcpy(IrdaAddress.irdaServiceName,"InkWiz");

				// Bind the socket to the address
				result = bind(sock,
							  (struct sockaddr *)&IrdaAddress,
							  sizeof(IrdaAddress));
			}
			break;

#endif
//#endif

		case XFER_WINSOCK_SEND:
		case XFER_WINSOCK_RECV:

			{
				InetAddress.sin_family = nAddrType;
				InetAddress.sin_port = INKWIZ_PORT;
				InetAddress.sin_addr.s_addr = INADDR_ANY;

				// Bind the socket to the address
				result = bind(sock,
							  (struct sockaddr *)&InetAddress,
							  sizeof(InetAddress));
			}

			break;

		default:

			result = WSAENOTSOCK;

			break;
	}

	return(result);
}


#ifndef _WIN32_WCE


#define SERVICE_PROVIDER_IRDA TEXT("MSAFD Irda [IrDA]")

#define INVALID_ADDRESS_TYPE -1


//----------------------------------------------------------------------------
// GetDesktopIrdaAF
//----------------------------------------------------------------------------

static int GetDesktopIrdaAF(void)
{
    LPWSAPROTOCOL_INFO lpspi;
    DWORD dwSize;
	int nCount;
	int i;

	// Specify size of zero to get required size
	dwSize = 0;

    // Query Winsock for the number of protocols
    nCount = WSAEnumProtocols(NULL,NULL,&dwSize);
	
	// Allocate required space
	lpspi = (LPWSAPROTOCOL_INFO)LocalAlloc(LPTR,dwSize);

	// If we could not allocate
	if ( lpspi == NULL )
	{
		// Assume that IrDA is not supported
		return(INVALID_ADDRESS_TYPE);
	}

	// Enumerate the protocols
	nCount = WSAEnumProtocols(0,lpspi,&dwSize);

    // Cannot enumerate, assume IrDA not supported
	if ( nCount == SOCKET_ERROR )
    {
		return(INVALID_ADDRESS_TYPE);
	}

	// Loop through all protocols
	for(i=0;i<nCount;i++)
	{
		// If this protocol matches
		if ( TEXTCMP(lpspi[i].szProtocol,SERVICE_PROVIDER_IRDA) == 0 )
		{
			int iFamily = lpspi[i].iAddressFamily;

			// Free the memory
			LocalFree(lpspi);

			// Return the address family of the matching protocol
			return(iFamily);
		}
	}

	// Free the memory
	LocalFree(lpspi);

	// Failed to find a match, assume IrDA not supported
	return(INVALID_ADDRESS_TYPE);
}


#endif


//----------------------------------------------------------------------------
// OpenSocket
//----------------------------------------------------------------------------

SOCKET OpenSocket(void)
{
	WSADATA wsadata;
	SOCKET sock;
	
#ifdef _WIN32_WCE	
	WORD wVersionRequested=MAKEWORD(1,1); 
#else
	WORD wVersionRequested=MAKEWORD(2,2); 
#endif

	if ( WSAStartup(wVersionRequested,&wsadata) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();

		ReportError(TEXT("WSAStartup"),dwErrNum);
	}

	// Open a socket for server mode
	switch(g_nXferType)
	{
#ifndef _WIN32_WCE_EMULATION

		case XFER_IRSOCK_SEND:
		case XFER_IRSOCK_RECV:

#ifdef _WIN32_WCE
			
			nAddrType = AF_IRDA;

#else

			nAddrType = GetDesktopIrdaAF();

#endif

			
			sock = socket(nAddrType,SOCK_STREAM,0);

			break;

#endif

		case XFER_WINSOCK_SEND:
		case XFER_WINSOCK_RECV:

			nAddrType = AF_INET;
			
			sock = socket(nAddrType,SOCK_STREAM,0);

			break;

		default:

			sock = INVALID_SOCKET;

			break;
	}

	return(sock);
}


//----------------------------------------------------------------------------
// AddressSocket
//----------------------------------------------------------------------------

BOOL AddressSocket(SOCKET sock,
				   HWND hwnd)
{
	switch(g_nXferType)
	{
//#ifdef _WIN32_WCE
#ifndef _WIN32_WCE_EMULATION
			
		case XFER_IRSOCK_SEND:
		case XFER_IRSOCK_RECV:

			{
				int idx;
				DEVICELIST DevList;
				int DevListLen;
				int Retries = MAXRETRIES;

				DevList.numDevice = 0;

				// While the user has not aborted, we have found not devices, and retry count has not expired
				while ( !l_bSocketsReqAbort &&
						( DevList.numDevice == 0 ) &&
						Retries-- )
				{
					// Report to the xfer dialog that we are making an attempt to connect
					SendMessage(hwnd, WM_XFER_NOTIFY, XFER_ATTEMPT, (long)(MAXRETRIES-Retries));

					DevListLen = sizeof(DevList);

					// Get the device list found in enumeration of this socket, if we cannot
					if ( getsockopt(sock,
									SOL_IRLMP,
									IRLMP_ENUMDEVICES,
									(char *)&DevList,
									&DevListLen) )
					{
						// If its not a real error, just that the socket is busy
						if ( WSAGetLastError() == WSAEINPROGRESS )
						{
							// Don't use up a retry
							Retries++;
						}
						else
						{
							DWORD dwErrNum;

							dwErrNum = WSAGetLastError();
						
							// Abort the session
							SocketsCleanUp(FALSE);

							// Report the error to the user
							ReportError(TEXT("getsockopt"),dwErrNum);

							// Return failure
							return(FALSE);
						}
					}
				}

				// If an abort has been requested
				if ( l_bSocketsReqAbort )
				{
					// Abort the session
					SocketsCleanUp(FALSE);

					// Return failure
					return(FALSE);
				}

				SendMessage(hwnd, WM_XFER_NOTIFY, XFER_ATTEMPT, 0L);

				// If no devices were found in the enumeration
				if ( DevList.numDevice == 0 )
				{
					// Abort the session
					SocketsCleanUp(FALSE);
					
					// Report the error to the user
					MyError(TEXT("Error Connecting"),
							TEXT("No Server Found!"));

					// Return failure
					return(FALSE);
				}

				IrdaAddress.irdaAddressFamily = nAddrType;

				// Copy address of first device
				for(idx=0;idx<4;idx++)
				{
					IrdaAddress.irdaDeviceID[idx] = DevList.Device[0].irdaDeviceID[idx];
				}

				strcpy(IrdaAddress.irdaServiceName,"InkWiz");
			}

		break;

#endif
//#endif

	case XFER_WINSOCK_SEND:
	case XFER_WINSOCK_RECV:

		{
			TCHAR szHost[MAX_PATH];
			TCHAR szIpAddr[MAX_PATH];

			GetHost(g_nXferIndex,szHost,sizeof(szHost));
			if ( szHost[0] != TEXT('\0') )
			{
				SetCursor(LoadCursor(NULL,IDC_WAIT));

				GetIpAddr(szHost,0,szIpAddr,sizeof(szIpAddr));

				SetCursor(NULL);

				if ( szIpAddr[0] == TEXT('\0') )
				{
					// Abort the session
					SocketsCleanUp(FALSE);

					MyError(TEXT("No IP Address for"),szHost);

					return(FALSE);
				}
				else
				{
					CHAR szIp[MAX_PATH];
					ULONG S_addr;

#ifdef UNICODE
					wcstombs(szIp,(WCHAR *)szIpAddr,sizeof(szIp));
#else
					strcpy(szIp,szIpAddr);
#endif
					S_addr = inet_addr(szIp);

					if ( S_addr == INADDR_NONE )
					{
						// Abort the session
						SocketsCleanUp(FALSE);

						MyError(TEXT("IP Address"),TEXT("Not found"));

						return(FALSE);
					}

					InetAddress.sin_family = nAddrType;
					InetAddress.sin_port = INKWIZ_PORT;
					InetAddress.sin_addr.s_addr = S_addr;
				}
			}
		}

		break;
	}

	// Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// ConnectSocket
//----------------------------------------------------------------------------

static ConnectSocket(SOCKET sock)
{
	DWORD result;

	// Connect to socket
	switch(g_nXferType)
	{

//#ifdef _WIN32_WCE
#ifndef _WIN32_WCE_EMULATION

		case XFER_IRSOCK_SEND:
		case XFER_IRSOCK_RECV:

		
			result = connect(sock,
							 (struct sockaddr *)&IrdaAddress,
							 sizeof(IrdaAddress));

			break;

#endif
//#endif

		case XFER_WINSOCK_SEND:
		case XFER_WINSOCK_RECV:

			{
				result = connect(sock,
								 (struct sockaddr *)&InetAddress,
								 sizeof(InetAddress));
			}

			break;

		default:

			result = WSAENOTSOCK;

			break;
	}

	return(result);
}


//----------------------------------------------------------------------------
// WINSOCK_ReceiveFile
//----------------------------------------------------------------------------

BOOL WINAPI WINSOCK_ReceiveFile(HWND hwnd)
{
	int result;
	struct linger lingerinfo;

	// Assume no file created yet
	l_lpszFullSpec = NULL;

	// Start off with no abort requested or completed
	bSocketsDone = FALSE;
	l_bSocketsReqAbort  = FALSE;

	// Report to xfer dialog that we are connecting as server and listening for connection
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTING_TO_RECV,0L);
	
	// Open an appropriate socket
	ServerSocket = OpenSocket();
	
	// If we could not open the socker
	if ( ServerSocket == INVALID_SOCKET )
	{
		DWORD dwErrNum;
		
		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);
		
		switch(dwErrNum)
		{
			case ERROR_ALREADY_EXISTS:
				
				// Report the error to the user
				MyError(TEXT("socket"),TEXT("Port is in use"));
				
				break;
			
			default:
				
				// Report the error to the user
				ReportError(TEXT("socket"),dwErrNum);
				
				break;
		}

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Set non-blocking request flag
	dwNonBlocking = TRUE;

	// Modify socket to be non-blocking, if we cannot
	if ( ioctlsocket(ServerSocket,FIONBIO,&dwNonBlocking) )
	{
		DWORD dwErrNum;
		
		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("ioctlsocket"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Set socket reusable flag
	optval = TRUE;

	// Modify socket to be reusable, if we cannot
	if ( setsockopt(ServerSocket,
					SOL_SOCKET,
					SO_REUSEADDR,
					(const char *)&optval,
					sizeof(optval)) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("setsockopt"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Bind the socket to the appropriate address
	result = BindSocket(ServerSocket);
	
	// If we cannot bind the address
	if ( result )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("bind"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Listen for an incoming connection, if we cannot
	if ( listen(ServerSocket,1) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("listen"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Assume no client socket yet
	ClientSocket = INVALID_SOCKET;

	// Repeat while we don't have a valid client socket and the user has not aborted
	while ( ( ClientSocket == INVALID_SOCKET ) &&
		    !l_bSocketsReqAbort )
	{
		// Accept an incoming client socket
		ClientSocket = accept(ServerSocket,0,0);

		// If the socket is invalid
		if ( ClientSocket == INVALID_SOCKET )
		{
			// If it is a real error
			if ( WSAGetLastError() != WSAEWOULDBLOCK )
			{
				DWORD dwErrNum;

				dwErrNum = WSAGetLastError();
				
				// Abort the session
				SocketsCleanUp(FALSE);

				// Report the error to the user
				ReportError(TEXT("accept"),dwErrNum);

				// Return failure
				return(FALSE);
			}
		}
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	lingerinfo.l_onoff = TRUE;
	lingerinfo.l_linger = 10;

	// Modify socket to linger on close
	if ( setsockopt(ClientSocket,
					SOL_SOCKET,
					SO_LINGER,
					(const char *)&lingerinfo,
					sizeof(lingerinfo)) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("setsockopt"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to xfer dialog that we are connected to receive and awaiting file header
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTED_TO_RECV,0L);
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_HEADER,0L);

	// Receive the header, if we cannot
	if ( !SocketsRecv(&header,sizeof(header),XFER_HEADER,0) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	ExtractHeaderInfo(&header);
	
	// Start file spec of receive file with selected path
	TEXTCPY(g_ii.szFullSpec,g_ii.szPathName);

	// Append the received file name from the header
#ifdef UNICODE
	{
		TCHAR szFileName[MAX_PATH];
		mbstowcs(szFileName,header.szFileName,TEXTSIZEOF(szFileName)-1);
		szFileName[TEXTSIZEOF(szFileName)-1] = TEXT('\0');
		TEXTCAT(g_ii.szFullSpec,szFileName);
	}
#else
	TEXTCAT(g_ii.szFullSpec,header.szFileName);
#endif

	// Save the file size from the header
	g_ii.dwFileSize = header.dwFileSize;

	// Open the target file for writing
	hFile = CreateFile(g_ii.szFullSpec,
					   GENERIC_WRITE,
					   0,
					   NULL,
					   CREATE_NEW,
					   FILE_ATTRIBUTE_NORMAL,
					   0);

	// If the file could not be opened
	if ( hFile == INVALID_HANDLE_VALUE )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		LastError(TEXT("CreateFile - CREATE_NEW"));

		// Return failure
		return(FALSE);
	}
	
	// File exists now, may need to be deleted later
	l_lpszFullSpec = g_ii.szFullSpec;
	
	// Compute the number of records required for the file
	l_nRecords = (int) ( ( g_ii.dwFileSize + l_nDataMax - 1 )
					   / l_nDataMax );

	header_reply.cRecordType = XFER_HEADER_REPLY;
	header_reply.dwFileSize = header.dwFileSize;
	header_reply.nRecordLength = sizeof(XFERHEADER);
	header_reply.nRecordNumber = 0;

	SetHeaderInfo(&header_reply);

	// Send the header reply. if we cannot
	if ( !SocketsSend(&header_reply,sizeof(header_reply)) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}


	// Report to xfer dialog that we are awaiting data
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_DATA,(LPARAM)g_szRemoteName);

	l_dwBeginTime = GetTickCount();	
	
	// Loop for number of records to be received
	for(l_nRecord=1;l_nRecord<=l_nRecords;l_nRecord++)
	{
		DWORD dwSize = l_nDataMax;
		DWORD dwWritten;
		DWORD dwDesired;

		{
			TCHAR szMsg[MAX_PATH];
			DWORD dwTime;
			DWORD dwChars;
			DWORD dwCps;

			dwTime = ( GetTickCount() - l_dwBeginTime );
			dwChars = ( l_nRecord * dwSize );
			if ( dwTime > 0 )
			{
				dwCps   = ( dwChars * 1000 ) /  dwTime;
			}
			else
			{
				dwCps = 0;
			}
			
			TEXTSPRINTF(szMsg,
						TEXT("%d of %d @ %d"),
						l_nRecord,
						l_nRecords,
						dwCps);
			
			SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
		}

		// If there is less than a full record to be received
		if ( g_ii.dwFileSize < (DWORD)l_nDataMax )
		{
			// Make this a short record
			dwSize = g_ii.dwFileSize;
		}

		// Compute size of packet desired
		dwDesired = sizeof(data)-l_nDataMax+(int)dwSize;

		// Receive the next packet, if we cannot
		if ( !SocketsRecv(&data,
					   dwDesired,
					   XFER_DATA,
					   l_nRecord) )
		{
			// Abort the session
			SocketsCleanUp(FALSE);
			
			// Return failure
			return(FALSE);
		}

		// Reduce remaining file size of size of data received
		g_ii.dwFileSize -= dwSize;

		// Write the data to the file, if we cannot
		if ( !WriteFile(hFile,
						data.bData,
						dwSize,
						&dwWritten,
						NULL) )
		{
			// Abort the session
			SocketsCleanUp(FALSE);
			
			// Report the error to the user
			LastError(TEXT("WriteFile"));

			// Return failure
			return(FALSE);
		}

		// Report to the xfer dialog the progress so far
		{
			long nProgress = (long)( header.dwFileSize -
									 g_ii.dwFileSize )
								   * 100L
								   / (long) header.dwFileSize;
			SendMessage(hwnd, WM_XFER_NOTIFY, XFER_SET_PROGRESS, nProgress);
		}

		// If the size written to file is wrong
		if ( dwWritten != dwSize )
		{
			// Abort the session
			SocketsCleanUp(FALSE);

			// Report the error to the user
			MyError(TEXT("WriteFile"),
					TEXT("Size mismatch"));

			// Return failure
			return(FALSE);
		}
	}

	// Report to xfer dialog that we finished data and are awaiting trailer record
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_TRAILER,(LPARAM)g_szRemoteName);

	// Receive trailer record, if we cannot
	if ( !SocketsRecv(&trailer,sizeof(trailer),XFER_TRAILER,l_nRecords+1) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If the header and trailer sizes don't match
	if ( header.dwFileSize != trailer.dwFileSize )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		MyError(TEXT("File size mismatch"),
			    TEXT("Header and Trailer"));

		// Return failure
		return(FALSE);
	}
	
	// Close the file
	CloseHandle(hFile);

	// Mark it as not open
	hFile = NULL;

	end_ack.cRecordType = XFER_END_ACK;
	end_ack.nRecordLength = sizeof(XFERENDACK);

	// Send the end ack. if we cannot
	if ( !SocketsSend(&end_ack,sizeof(end_ack)) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	Sleep(2000);

	// End the session
	SocketsCleanUp(TRUE);
	
	// Report to the xfer dialog that the file was received successfully
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_RECEIVE_COMPLETED,(LPARAM)g_szRemoteName);

	// Return success
	return(TRUE);
}


static BOOL StartSendSocket(HWND hwnd)
{
	int result;
	int Retries = MAXRETRIES;
	struct linger lingerinfo;

	// Open an appropriate socket
	ClientSocket = OpenSocket();
	
	// If we could not open the socket
	if ( ClientSocket == INVALID_SOCKET )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		switch(dwErrNum)
		{
			case ERROR_ALREADY_EXISTS:
				
				// Report the error to the user
				MyError(TEXT("socket"),TEXT("Port is in use"));
				
				break;
			
			default:
				
				// Report the error to the user
				ReportError(TEXT("socket"),dwErrNum);
				
				break;
		}

		// Return failure
		return(FALSE);
	}

	// Set non-blocking request flag
	dwNonBlocking = TRUE;

	// Modify socket to be non-blocking, if we cannot
	if ( ioctlsocket(ClientSocket,FIONBIO,&dwNonBlocking) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("ioctlsocket"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	lingerinfo.l_onoff = TRUE;
	lingerinfo.l_linger = 10;

	// Modify socket to linger on close
	if ( setsockopt(ClientSocket,
					SOL_SOCKET,
					SO_LINGER,
					(const char *)&lingerinfo,
					sizeof(lingerinfo)) )
	{
		DWORD dwErrNum;

		dwErrNum = WSAGetLastError();
		
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("setsockopt"),dwErrNum);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Address the socket appropriately
	if ( !AddressSocket(ClientSocket,hwnd) )
	{
		// Return failure
		return(FALSE);
	}
	
	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Loop
	do
	{
		DWORD dwErrNum;
		
		result = ConnectSocket(ClientSocket);
		
		dwErrNum = WSAGetLastError();

		// If it failed
		if ( result )
		{
			switch(dwErrNum)
			{
				case WSAECONNABORTED:
					
					// Abort the session
					SocketsCleanUp(FALSE);

					// Report the error to the user
					MyError(TEXT("connect"),TEXT("Connection aborted"));
					
					// Return failure
					return(FALSE);

				case WSAEISCONN:
					
					// This indicates we finally succeeded					
					result = ERROR_SUCCESS;
					
					continue;
				
				default:
					
					continue;
				
			}
		}

	// Continue looping until success or abort
	} while ( result && !l_bSocketsReqAbort );	

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	return(TRUE);
}


//----------------------------------------------------------------------------
// SOCK_SendFile
//----------------------------------------------------------------------------

BOOL WINAPI SOCK_SendFile(HWND hwnd)
{
	bSocketsDone = FALSE;
	l_bSocketsReqAbort  = FALSE;
	
	// Report to the idra dialog that we are connecting as client and calling
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTING_TO_SEND,(LPARAM)g_szRemoteName);
					
	if ( !StartSendSocket(hwnd) )
	{
		return(FALSE);
	}
	
	// Report to the xfer dialog that we are connected as a client and sending header
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTED_TO_SEND,0L);
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_HEADER,0L);

	// Get the properties of the file to be sent, if we cannot
	if ( !GetFileProperties(hwnd) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		MyError(TEXT("Cannot get file info"),
			    g_ii.szFullSpec);

		// Return failure
		return(FALSE);
	}
	
	// Initialize header record with file name and information
	header.nRecordLength = sizeof(XFERHEADER);
	header.nRecordNumber = 0;
	header.cRecordType = XFER_HEADER;

#ifdef UNICODE
	wcstombs(header.szFileName,(LPTSTR)g_ii.szItemName,sizeof(header.szFileName)-1);
	header.szFileName[sizeof(header.szFileName)-1] = '\0';
#else
	TEXTNCPY(header.szFileName,g_ii.szItemName,TEXTSIZEOF(header.szFileName)-1);
	header.szFileName[TEXTSIZEOF(header.szFileName)-1] = TEXT('\0');
#endif

	header.dwFileSize = g_ii.dwFileSize;

	// Open the file for reading
	hFile = CreateFile(g_ii.szFullSpec,
					   GENERIC_READ,
					   0,
					   NULL,
					   OPEN_EXISTING ,
					   FILE_ATTRIBUTE_NORMAL,
					   0);

	// If we cannot open the file
	if ( hFile == INVALID_HANDLE_VALUE )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Report the error to the user
		LastError(TEXT("CreateFile"));

		// Return failure
		return(FALSE);
	}
	
	SetHeaderInfo(&header);

	// Send the header. if we cannot
	if ( !SocketsSend(&header,sizeof(header)) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Compute number of record to be sent for the file
	l_nRecords = (int) ( ( g_ii.dwFileSize + l_nDataMax - 1 )
					   / l_nDataMax );

	// Report to the xfer dialog that we are awaiting a reply
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_REPLY,0L);

	// Receive the header reply, if we cannot
	if ( !SocketsRecv(&header_reply,sizeof(header_reply),XFER_HEADER_REPLY,0) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	ExtractHeaderInfo(&header_reply);

	// If an abort has been requested
	if ( l_bSocketsReqAbort )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to xfer dialog that we are sending data
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_DATA,(LPARAM)g_szRemoteName);

	l_dwBeginTime = GetTickCount();	
	
	// Loop through the records to be sent
	for(l_nRecord=1;l_nRecord<=l_nRecords;l_nRecord++)
	{
		DWORD dwSize = l_nDataMax;
		DWORD dwRead;
		DWORD dwDesired;

		{
			TCHAR szMsg[MAX_PATH];
			DWORD dwTime;
			DWORD dwChars;
			DWORD dwCps;
			
			dwTime = ( GetTickCount() - l_dwBeginTime );
			dwChars = ( l_nRecord * dwSize );
			if ( dwTime > 0 )
			{
				dwCps   = ( dwChars * 1000 ) /  dwTime;
			}
			else
			{
				dwCps = 0;
			}
			
			TEXTSPRINTF(szMsg,
						TEXT("%d of %d @ %d"),
						l_nRecord,
						l_nRecords,
						dwCps);
			
			SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
		}

		// If the remaining data is less than a full record
		if ( g_ii.dwFileSize < (DWORD)l_nDataMax )
		{
			// Make it a short record
			dwSize = g_ii.dwFileSize;
		}

		// Compute size of packet to be sent
		dwDesired = sizeof(data)-l_nDataMax+(int)dwSize;

		// Fill in packet information
		data.nRecordLength = (int)dwDesired;
		data.nRecordNumber = l_nRecord;
		data.cRecordType = XFER_DATA;
		
		// Read file data into packet, if we cannot
		if ( !ReadFile(hFile,
					   data.bData,
					   dwSize,
					   &dwRead,
					   NULL) )
		{
			// Abort the session
			SocketsCleanUp(FALSE);

			// Report the error to the user
			LastError(TEXT("WriteFile"));

			// Return failure
			return(FALSE);
		}

		// If size read from file does not match
		if ( dwRead != dwSize )
		{
			// Abort the session
			SocketsCleanUp(FALSE);

			// Report the error to the user
			MyError(TEXT("ReadFile"),
					TEXT("Size mismatch"));

			// Return failure
			return(FALSE);
		}

		// Send the packet, if we cannot
		if ( !SocketsSend(&data,sizeof(data)-l_nDataMax+(int)dwSize) )
		{
			// Abort the session
			SocketsCleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		// Reduce remaining size by size sent
		g_ii.dwFileSize -= dwSize;

		// Report to the xfer dialog the progress so far
		{
			long nProgress = (long)( header.dwFileSize -
									 g_ii.dwFileSize )
								   * 100L
								   / (long) header.dwFileSize;
			SendMessage(hwnd, WM_XFER_NOTIFY, XFER_SET_PROGRESS, nProgress);
		}

	}

	// Report to the xfer dialog that we are done with data and sending trailer
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_TRAILER,(LPARAM)g_szRemoteName);

	// Fill in trailer information
	trailer.nRecordLength = sizeof(XFERTRAILER);
	trailer.nRecordNumber = l_nRecords+1;
	trailer.cRecordType = XFER_TRAILER;
	trailer.dwFileSize = header.dwFileSize;

	// Send the trailer, if we cannot
	if ( !SocketsSend(&trailer,sizeof(trailer)) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Close the file
	CloseHandle(hFile);

	// Mark it as not open
	hFile = NULL;

	// Report to the xfer dialog that we are awaiting a reply
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_REPLY,0L);

	// Receive the header reply, if we cannot
	if ( !SocketsRecv(&end_ack,sizeof(end_ack),XFER_END_ACK,0) )
	{
		// Abort the session
		SocketsCleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to the xfer dialog that we are done sending the file
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SEND_COMPLETED,(LPARAM)g_szRemoteName);

	// End the session
	SocketsCleanUp(TRUE);

	// Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// SocketsReceiveFile
//----------------------------------------------------------------------------

BOOL WINAPI SocketsReceiveFile(LPVOID lpVoid)
{
	BOOL bResult;

	if ( !InitPowerMgmt() )
	{
		return(FALSE);
	}
							
#ifdef _WIN32_WCE

#ifndef WIN32_PLATFORM_POCKETPC
	SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_BELOW_NORMAL);
#endif

#endif

	// Convert thread calling parameter to window handle and pass on
	bResult = WINSOCK_ReceiveFile((HWND)lpVoid);

	// If we failed
	if ( !bResult )
	{
		// If a file was created

		if ( l_lpszFullSpec != NULL )
		{
			// Delete the file
			DeleteFile(l_lpszFullSpec);

			// Indicate no file created
			l_lpszFullSpec = NULL;
		}
	}
	
	// Return the result
	return(bResult);
}


//----------------------------------------------------------------------------
// SocketsSendFile
//----------------------------------------------------------------------------

BOOL WINAPI SocketsSendFile(LPVOID lpVoid)
{
	DWORD bResult;

	if ( !InitPowerMgmt() )
	{
		return(FALSE);
	}
							
#ifdef _WIN32_WCE

#ifndef WIN32_PLATFORM_POCKETPC
	SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_BELOW_NORMAL);
#endif

#endif
							
	// Convert thread calling parameter to window handle and pass on
	bResult = SOCK_SendFile((HWND)lpVoid);

	return(bResult);
}


//----------------------------------------------------------------------------
// void SocketsAbort(void)
//----------------------------------------------------------------------------

void SocketsAbort(void)
{
	// Request abort when possible
	l_bSocketsReqAbort = TRUE;
}


//----------------------------------------------------------------------------
// void IsSocketsDone(void)
//----------------------------------------------------------------------------

BOOL IsSocketsDone(void)
{
	// Return whether finished aborting
	return(bSocketsDone);
}


