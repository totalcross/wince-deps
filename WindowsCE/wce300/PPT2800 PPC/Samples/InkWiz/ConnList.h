
//--------------------------------------------------------------------
// FILENAME:        CONNLIST.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for CONNLIST.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef CONNLIST_H_

#define CONNLIST_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------

BOOL InvokeConnectionListView(void);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CONNLIST_H_ */


