
//--------------------------------------------------------------------
// FILENAME:            XFERDLG.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains procedures for handling the xfer dialog
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdWproc.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"

#include "..\common\Treeview.h"
#include "..\common\Treedir.h"
#include "..\common\Dir.h"

#include "Sockets.h"

#include "rs232.h"

#include "xferdlg.h"

#include "inkwiz.h"


int g_nXferType;
int g_nXferIndex = 0;


DIALOG_CHOICE XferDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_XFER_PC_STD,	NULL },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_XFER_HPC_STD,	NULL },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_XFER_PPC_STD,	NULL },
	{ ST_PPC_SIP,	IDD_XFER_PPC_SIP,	NULL },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_XFER_7200_STD,	NULL },
	{ ST_7200_TSK,	IDD_XFER_7200_TSK,	NULL },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_XFER_7500_STD,	NULL },
	{ ST_7500_TSK,	IDD_XFER_7500_TSK,	NULL },
#endif
	{ ST_UNKNOWN,	IDD_XFER_7500_TSK,	NULL }
};


static HANDLE g_hThread;



void DisplayFromOrTo(HWND hwnd,
					 int nCtlId,
					 LPTSTR lpszWhat,
					 LPTSTR lpszFromOrTo,
					 LPTSTR lpszWho)
{
	TCHAR szMsg[MAX_PATH] = TEXT("");

	if ( lpszWhat != NULL )
	{
		TEXTCPY(szMsg,lpszWhat);
	}

	if ( lpszWho != NULL )
	{
		if ( lpszFromOrTo != NULL )
		{
			TEXTCAT(szMsg,TEXT(" "));
			TEXTCAT(szMsg,lpszFromOrTo);
			TEXTCAT(szMsg,TEXT(" "));
		}

		TEXTCAT(szMsg,lpszWho);
	}

	if ( *szMsg )
	{
		TEXTCAT(szMsg,TEXT("..."));
	}

	Static_SetText(GetDlgItem(hwnd,nCtlId),szMsg);
}
					
					
//----------------------------------------------------------------------------
// XferDlgProc
//----------------------------------------------------------------------------

static LRESULT CALLBACK XferDlgProc(HWND hwnd,
									UINT uMsg,
									WPARAM wParam,
									LPARAM lParam)
{
	switch(uMsg)
    {
		case UM_RESIZE:
			
			ResizeDialog(NULL);

			break;

		case WM_INITDIALOG:

			MaxDialog(hwnd);

			g_hxferdlg = hwnd;

			g_nXferType = (int)lParam;
			
			Progress_SetPos(GetDlgItem(hwnd,IDC_PROGRESS1),0);

			PostMessage(hwnd,WM_XFER_NOTIFY,XFER_START,0L);

	        return (0);

		case WM_XFER_NOTIFY:

			switch(wParam)
			{
				case XFER_START:
					
					switch(g_nXferType)
					{
						case XFER_SERIAL1_RECV:
						case XFER_SERIAL2_RECV:
						case XFER_SERIAL3_RECV:
						case XFER_SERIAL4_RECV:
						case XFER_IRCOMM_RECV:
						case XFER_CRADLE_RECV:
							
							// Start serial send on a separate thread
							g_hThread = CreateThread(NULL,
													 0,
													 (LPTHREAD_START_ROUTINE)Rs232ReceiveFile,
													 (LPVOID)hwnd,
													 0,
													 &dwThreadId); 

							break;
					
						case XFER_SERIAL1_SEND:
						case XFER_SERIAL2_SEND:
						case XFER_SERIAL3_SEND:
						case XFER_SERIAL4_SEND:
						case XFER_IRCOMM_SEND:
						case XFER_CRADLE_SEND:
							
							// Start serial send on a separate thread
							g_hThread = CreateThread(NULL,
													 0,
													 (LPTHREAD_START_ROUTINE)Rs232SendFile,
													 (LPVOID)hwnd,
													 0,
													 &dwThreadId); 

							break;
					
						case XFER_IRSOCK_RECV:
						case XFER_WINSOCK_RECV:
							
							// Start irda receive on a separate thread
							g_hThread = CreateThread(NULL,
													 0,
													 (LPTHREAD_START_ROUTINE)SocketsReceiveFile,
													 (LPVOID)hwnd,
													 0,
													 &dwThreadId);

							break;
						
						case XFER_IRSOCK_SEND:
						case XFER_WINSOCK_SEND:
							
							// Start irda receive on a separate thread
							g_hThread = CreateThread(NULL,
													 0,
													 (LPTHREAD_START_ROUTINE)SocketsSendFile,
													 (LPVOID)hwnd,
													 0,
													 &dwThreadId); 
							
							break;
					}
					
					PostMessage(g_hxferdlg,WM_XFER_NOTIFY,XFER_CANCELNOOK,0L);

					break;
				
				case XFER_STOP:

					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Stopping..."));

					switch(g_nXferType)
					{
						case XFER_WINSOCK_RECV:
						case XFER_WINSOCK_SEND:
						case XFER_IRSOCK_RECV:
						case XFER_IRSOCK_SEND:

							if ( IsSocketsDone() )
							{
								SendMessage(hwnd,WM_COMMAND,IDOK,0L);
							}
							else
							{
								SocketsAbort();

								Sleep(100);

								PostMessage(hwnd,WM_XFER_NOTIFY,XFER_STOP,0);
							}

							break;

						case XFER_SERIAL1_SEND:
						case XFER_SERIAL2_SEND:
						case XFER_SERIAL3_SEND:
						case XFER_SERIAL4_SEND:
						case XFER_SERIAL1_RECV:
						case XFER_SERIAL2_RECV:
						case XFER_SERIAL3_RECV:
						case XFER_SERIAL4_RECV:
						case XFER_IRCOMM_SEND:
						case XFER_IRCOMM_RECV:
						case XFER_CRADLE_SEND:
						case XFER_CRADLE_RECV:

							if ( IsRs232Done() )
							{
								SendMessage(hwnd,WM_COMMAND,IDOK,0L);
							}
							else
							{
								Rs232Abort();

								Sleep(100);

								PostMessage(hwnd,WM_XFER_NOTIFY,XFER_STOP,0);
							}
							
							break;
					}
					
					break;

				case XFER_OKNOCANCEL:

					SetFocus(hwnd);
					
					// Disable CANCEL button and Enable OK button on the xfer dialog
					Button_Enable(GetDlgItem(hwnd,IDCANCEL),FALSE);
					Button_Enable(GetDlgItem(hwnd,IDOK),TRUE);

					InvalidateRect(hwnd,NULL,TRUE);
					
					UpdateWindow(hwnd);
					
					break;

				case XFER_CANCELNOOK:

					SetFocus(hwnd);
					
					// Disable OK button and Enable CANCEL button on the xfer dialog
					Button_Enable(GetDlgItem(hwnd,IDOK),FALSE);
					Button_Enable(GetDlgItem(hwnd,IDCANCEL),TRUE);

					InvalidateRect(hwnd,NULL,TRUE);
					
					UpdateWindow(hwnd);
					
					break;

				case XFER_DEBUG:

					{
						TCHAR szMsg[MAX_PATH];

						TEXTSPRINTF(szMsg,TEXT("Debug %d"),lParam);

						Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
									   szMsg);
					}

					break;
				
				case XFER_CONNECTING_TO_RECV:

					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
								 TEXT("Connecting for recv:"));
					
					break;

				case XFER_CONNECTED_TO_RECV:
					
					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
								   TEXT("Connected for recv:"));
					
					break;

				case XFER_TITLE:
					
					if ( lParam != 0 )
					{
						Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
									   (LPTSTR)lParam);
					}
					
					break;

				case XFER_MESSAGE:
					
					if ( lParam != 0 )
					{
						Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
									   (LPTSTR)lParam);
					}
					
					break;

				case XFER_STATUS:
					
					if ( lParam != 0 )
					{
						Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE3),
									   (LPTSTR)lParam);
					}
					
					break;

				case XFER_AWAITING_HEADER:
					
					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Awaiting header..."));
					
					break;

				case XFER_AWAITING_REPLY:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Awaiting reply"),
									TEXT("from"),
									(LPTSTR)lParam);
					
					break;

				case XFER_SENDING_REPLY:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Sending reply"),
									TEXT("to"),
									(LPTSTR)lParam);

					break;

				case XFER_AWAITING_DATA:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Awaiting data"),
									TEXT("from"),
									(LPTSTR)lParam);
					
					break;

				case XFER_SET_PROGRESS:
					
					Progress_SetPos(GetDlgItem(hwnd,IDC_PROGRESS1),
									(int)lParam);
					
					break;

				case XFER_CLEANUP:

					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Cleaning up..."));
					
					break;

				case XFER_AWAITING_TRAILER:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Awaiting trlr"),
									TEXT("from"),
									(LPTSTR)lParam);
					
					break;

				case XFER_RECEIVE_COMPLETED:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE1,
									TEXT("Recv done"),
									TEXT("from"),
									(LPTSTR)lParam);

					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Successful"));
					
					ShowWindow(GetDlgItem(hwnd,IDC_PROGRESS1),
							   SW_HIDE);
					
					ShowWindow(GetDlgItem(hwnd,IDC_PROGTEXT),
							   SW_HIDE);
					
					// Refresh the tree item that received the file
					TreeView_RefreshItem(g_ii.hPath);

					break;

				case XFER_CONNECTING_TO_SEND:
					
					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
								 TEXT("Connecting to send:"));
					
					break;

				case XFER_SENDING_TRAILER:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Sending trlr"),
									TEXT("to"),
									(LPTSTR)lParam);
					
					break;

				case XFER_ATTEMPT:
					
					{
						TCHAR szRetries[64] = TEXT("");
						
						if ( lParam )
						{
							TEXTSPRINTF(szRetries,
									 TEXT("Attempt %d..."),
									 (int)lParam);
						}
						
						Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE3),
									 szRetries);
					}
					
					break;

				case XFER_CONNECTED_TO_SEND:
					
					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE1),
								 TEXT("Connected to send"));
					
					break;

				case XFER_SENDING_HEADER:
					
					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Sending header..."));
					
					break;

				case XFER_SENDING_DATA:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE2,
									TEXT("Sending data"),
									TEXT("to"),
									(LPTSTR)lParam);

					break;

				case XFER_SEND_COMPLETED:
					
					DisplayFromOrTo(hwnd,
									IDC_MESSAGE1,
									TEXT("Send done"),
									TEXT("to"),
									(LPTSTR)lParam);

					Static_SetText(GetDlgItem(hwnd,IDC_MESSAGE2),
								 TEXT("Successful"));
					
					ShowWindow(GetDlgItem(hwnd,IDC_PROGRESS1),
							   SW_HIDE);
					
					ShowWindow(GetDlgItem(hwnd,IDC_PROGTEXT),
							   SW_HIDE);
					
					break;
			}
			
			return(TRUE);

        case WM_COMMAND:
            
			switch (LOWORD(wParam))
            {
				case IDC_POPUP:
                case IDOK:
					
					// Exit the dialog
					ExitDialog();

					g_hxferdlg = NULL;
					
					break;

				case IDC_BEHIND:
				case IDCANCEL:
					
					PostMessage(hwnd,WM_XFER_NOTIFY,XFER_STOP,0);
					
					break;
            }
            
			return (TRUE);
    }
    
	return (FALSE);
}


void DoXfer(int nType,int nIndex)
{
	g_nXferIndex = nIndex;

	g_hdlg = CreateChosenDialog(g_hInstance,
								XferDialogChoices,
								g_hwnd,
								XferDlgProc,
								(DWORD)nType);
}


