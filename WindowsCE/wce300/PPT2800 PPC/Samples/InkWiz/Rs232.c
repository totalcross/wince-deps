
//--------------------------------------------------------------------
// FILENAME:            RS232.C
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains functions to implement RS232 file transfer
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------



#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <winsock.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"

#include "..\common\Treeview.h"

#include "props.h"

#include "rs232.h"

#include "inkwiz.h"

#include "tcpip.h"


#define MAX_IN_BUFFER 8192

#define MAX_RS232_RETRIES 1000

#define INQUEUESIZE 2048
#define OUTQUEUESIZE 2048

#define XFER_START_MARK	'['
#define XFER_END_MARK	']'
#define XFER_ACK		'a'
#define XFER_NAK		'n'

#define XFER_PAD		'X'

#define PAD_COUNT 5

#define IRCOMM_START '+'

//----------------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------------

static DWORD l_dwBeginTime;

static BOOL l_bRs232ReqAbort = FALSE;
static BOOL l_bRs232Done = FALSE;

static BOOL l_bWaitingForDone = FALSE;

static HANDLE l_hFile = NULL;

static XFERHEADER header;
static XFERHEADER header_reply;
static XFERDATA data;
static XFERTRAILER trailer;

static int l_nRecord;
static int l_nRecords;

static HANDLE l_hPort = NULL;

static LPTSTR l_lpszFullSpec = NULL;

static l_dwLastError = ERROR_SUCCESS;

static BYTE l_InBuffer[MAX_IN_BUFFER];
static DWORD l_dwInCount = 0;
static DWORD l_dwInHead = 0;
static DWORD l_dwInTail = 0;

static int l_nBaud = CBR_115200;

#define MAXRS232DATAINRECORD 480

static int l_nDataMax = MAXRS232DATAINRECORD;

static DWORD l_dwTimeout = 10;

static DWORD l_dwStartTimeout;
static DWORD l_dwDataTimeout;
static DWORD l_dwAckTimeout;
static DWORD l_dwPrgTimeout;

static int l_nAckCount = 0;
static int l_nNakCount = 0;
static int l_nRspCount = 0;
static int l_nTryCount = 0;


//#define DO_DEBUG
#ifdef DO_DEBUG

static DWORD MyWriteFile(HANDLE hFile,
						 LPCVOID lpBuffer,
						 DWORD nNumberOfBytesToWrite,
						 LPDWORD lpNumberOfBytesWritten,
						 LPOVERLAPPED lpOverlapped)
{
#ifdef UNICODE

	CHAR szBufferA[MAX_PATH];
	INT iBytesConverted;

	iBytesConverted = WideCharToMultiByte(CP_OEMCP,
										  0,
										  (LPTSTR)lpBuffer,
										  nNumberOfBytesToWrite,
										  szBufferA,
										  sizeof(szBufferA),
										  NULL,
										  NULL);

	return(WriteFile(hFile,
					 szBufferA,
					 iBytesConverted,
					 lpNumberOfBytesWritten,
					 lpOverlapped));

#else

	return(WriteFile(hFile,
					 lpBuffer,
					 nNumberOfBytesToWrite,
					 lpNumberOfBytesWritten,
					 lpOverlapped));

#endif
}


static Logit(LPTSTR lpszStr)
{
	HANDLE hFile;

#ifdef _WIN32_WCE
	hFile = CreateFile(TEXT("inkwiz.log"),
					   GENERIC_WRITE|GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_ALWAYS,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);
#else
	hFile = CreateFile(TEXT("c:\\a\\inkwiz.log"),
					   GENERIC_WRITE|GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_ALWAYS,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);
#endif

	if ( hFile != NULL )
	{
		SetFilePointer(hFile,0L,0L,FILE_END);
	}

	if ( hFile != NULL )
	{
		DWORD dwLen;
		TCHAR szMsg[MAX_PATH];

		TEXTSPRINTF(szMsg,TEXT("%.8X: %s"),GetTickCount(),lpszStr);

		MyWriteFile(hFile,szMsg,TEXTLEN(szMsg),&dwLen,NULL);
	}

	CloseHandle(hFile);
}

static void Logdw(LPTSTR lpszStr,DWORD dwValue)
{
	TCHAR szMsg[MAX_PATH];

	TEXTSPRINTF(szMsg,lpszStr,dwValue);

	Logit(szMsg);
}

static void Logbuf(LPVOID lpvData,DWORD dwCount)
{
	TCHAR szMsg[MAX_PATH];
	DWORD i;
	LPBYTE lpbData = (LPBYTE)lpvData;

	for(i=0;i<dwCount;i+=16)
	{
		DWORD j;

		TEXTCPY(szMsg,TEXT("  "));

		for(j=i;(j<i+16)&&(j<dwCount);j++)
		{
			BYTE b = (BYTE)(lpbData[j]&0xFF);

			TEXTSPRINTF(szMsg+TEXTLEN(szMsg),TEXT("%.2X "),b);
		}
				
		TEXTCAT(szMsg,TEXT("\r\n"));
				
		Logit(szMsg);
	}
}

#else

#define Logit(lpszStr)
#define Logdw(lpszStr,dwValue)
#define Logbuf(lpvData,dwCount)

#endif
	

//----------------------------------------------------------------------------
// Rs232SetLastError
//----------------------------------------------------------------------------

static void Rs232SetLastError(DWORD dwLastError)
{
	l_dwLastError = dwLastError;
}


void Rs232SetInputTimeout(DWORD dwTimeout)
{
	l_dwTimeout = dwTimeout;
}


void Rs232OutQueuePurge(void)
{
	PurgeComm(l_hPort,PURGE_TXCLEAR);
}


void Rs232SetBaud(int id)
{
	switch(id)
	{
		case IDC_BAUD_9600:
			l_nBaud = CBR_9600;
			break;
		case IDC_BAUD_19200:
			l_nBaud = CBR_19200;
			break;
		case IDC_BAUD_38400:
			l_nBaud = CBR_38400;
			break;
		case IDC_BAUD_57600:
			l_nBaud = CBR_57600;
			break;
		case IDC_BAUD_115200:
			l_nBaud = CBR_115200;
			break;
	}
}


//----------------------------------------------------------------------------
// Rs232Open
//----------------------------------------------------------------------------

static HANDLE Rs232Open(void)
{
	DCB PortDCB;
	TCHAR szPortName[MAX_PATH];
	HANDLE hPort;
	DWORD dwCommMask;
	BOOL bReady = FALSE;
	int i;
    COMMTIMEOUTS CommTimeouts;
	DWORD dwResult;

    l_nAckCount = 0;
	l_nNakCount = 0;
	l_nRspCount = 0;
	l_nTryCount = 0;
	
	PortDCB.DCBlength = sizeof(DCB);

	switch(g_nXferType)
	{
		case XFER_SERIAL1_SEND:
		case XFER_SERIAL1_RECV:
#ifdef _WIN32_WCE
			TEXTCPY(szPortName,TEXT("COM1:"));
#else
			TEXTCPY(szPortName,TEXT("COM1"));
#endif
			break;
		case XFER_SERIAL2_SEND:
		case XFER_SERIAL2_RECV:
#ifdef _WIN32_WCE
			TEXTCPY(szPortName,TEXT("COM2:"));
#else
			TEXTCPY(szPortName,TEXT("COM2"));
#endif
			break;
		case XFER_SERIAL3_SEND:
		case XFER_SERIAL3_RECV:
#ifdef _WIN32_WCE
			TEXTCPY(szPortName,TEXT("COM5:"));
#else
			TEXTCPY(szPortName,TEXT("COM3"));
#endif
			break;
		case XFER_SERIAL4_SEND:
		case XFER_SERIAL4_RECV:
			TEXTCPY(szPortName,TEXT("COM4"));
			break;
		case XFER_IRCOMM_SEND:
		case XFER_IRCOMM_RECV:
#ifdef _WIN32_WCE
			TEXTCPY(szPortName,TEXT("COM3:"));
#else
			TEXTCPY(szPortName,TEXT("COM4"));
#endif
			break;
		case XFER_CRADLE_SEND:
		case XFER_CRADLE_RECV:
#ifdef _WIN32_WCE
			TEXTCPY(szPortName,TEXT("COM6:"));
#else
			Rs232SetLastError(ERROR_BAD_UNIT);
			return(NULL);
#endif
			break;

		default:
			Rs232SetLastError(ERROR_BAD_UNIT);
			return(NULL);
	}

	{
		TCHAR szMsg[MAX_PATH];
		
		TEXTCPY(szMsg,szPortName);
		TEXTCAT(szMsg,TEXT(" opening..."));
		
		SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
	}

    hPort = CreateFile(szPortName,
					   GENERIC_READ | GENERIC_WRITE,
					   0,
					   NULL,
					   OPEN_EXISTING,
					   0,
					   NULL);

    if ( hPort == INVALID_HANDLE_VALUE )
	{
		
		Rs232SetLastError(GetLastError());

		return(NULL);
	}

	switch(g_nXferType)
	{
		case XFER_SERIAL1_SEND:
		case XFER_SERIAL2_SEND:
		case XFER_SERIAL3_SEND:
		case XFER_SERIAL4_SEND:
		case XFER_SERIAL1_RECV:
		case XFER_SERIAL2_RECV:
		case XFER_SERIAL3_RECV:
		case XFER_SERIAL4_RECV:
			
			SetupComm(hPort,INQUEUESIZE,OUTQUEUESIZE);
	
			GetCommState(hPort,&PortDCB);

			PortDCB.BaudRate			= l_nBaud;
			PortDCB.fBinary				= TRUE;
			PortDCB.fParity				= TRUE;
			PortDCB.fOutxCtsFlow		= FALSE;
			PortDCB.fOutxDsrFlow		= FALSE;
			PortDCB.fDtrControl			= DTR_CONTROL_ENABLE;
			PortDCB.fDsrSensitivity     = FALSE;
			PortDCB.fTXContinueOnXoff	= TRUE;
			PortDCB.fOutX				= FALSE;
			PortDCB.fInX				= FALSE;
			PortDCB.fErrorChar			= FALSE;
			PortDCB.fNull				= FALSE;
			PortDCB.fAbortOnError		= FALSE;
			PortDCB.ByteSize			= 8;
			PortDCB.Parity				= NOPARITY;
			PortDCB.StopBits			= ONESTOPBIT;
			PortDCB.fRtsControl         = RTS_CONTROL_ENABLE;
			PortDCB.XoffLim				= INQUEUESIZE/5;
			PortDCB.XonLim				= INQUEUESIZE/5;

			SetCommState(hPort,&PortDCB);

			l_dwStartTimeout =  100;
			l_dwDataTimeout  =  200;
			l_dwAckTimeout   =  400;
			l_dwPrgTimeout   =  100;
			
			break;

		case XFER_IRCOMM_SEND:
		case XFER_IRCOMM_RECV:

			l_dwStartTimeout =  400;
			l_dwDataTimeout  =  800;
			l_dwAckTimeout   = 1600;
			l_dwPrgTimeout   =  100;
			
			break;

		case XFER_CRADLE_SEND:
		case XFER_CRADLE_RECV:
			
			GetCommState(hPort,&PortDCB);

			PortDCB.BaudRate			= l_nBaud;
			PortDCB.fBinary				= TRUE;
			PortDCB.fParity				= TRUE;
			PortDCB.fOutxCtsFlow		= FALSE;
			PortDCB.fOutxDsrFlow		= FALSE;
			PortDCB.fDtrControl			= DTR_CONTROL_ENABLE;
			PortDCB.fDsrSensitivity     = FALSE;
			PortDCB.fTXContinueOnXoff	= TRUE;
			PortDCB.fOutX				= FALSE;
			PortDCB.fInX				= FALSE;
			PortDCB.fErrorChar			= FALSE;
			PortDCB.fNull				= FALSE;
			PortDCB.fAbortOnError		= FALSE;
			PortDCB.ByteSize			= 8;
			PortDCB.Parity				= NOPARITY;
			PortDCB.StopBits			= ONESTOPBIT;
			PortDCB.fRtsControl         = RTS_CONTROL_ENABLE;
			PortDCB.XoffLim				= INQUEUESIZE/5;
			PortDCB.XonLim				= INQUEUESIZE/5;

			SetCommState(hPort,&PortDCB);

			l_dwStartTimeout =  400;
			l_dwDataTimeout  =  800;
			l_dwAckTimeout   = 1600;
			l_dwPrgTimeout   =  100;
			
			break;
	}

    dwResult = GetCommTimeouts(hPort,&CommTimeouts);

    CommTimeouts.ReadIntervalTimeout = MAXDWORD;
    CommTimeouts.ReadTotalTimeoutMultiplier = 0;
    CommTimeouts.ReadTotalTimeoutConstant = 0;

    CommTimeouts.WriteTotalTimeoutMultiplier = 0;
    CommTimeouts.WriteTotalTimeoutConstant = 3000;

	dwResult = SetCommTimeouts(hPort,&CommTimeouts);

 	SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_MESSAGE,(LPARAM)TEXT("Wait for connect"));

	switch(g_nXferType)
	{
		case XFER_SERIAL1_SEND:
		case XFER_SERIAL2_SEND:
		case XFER_SERIAL3_SEND:
		case XFER_SERIAL4_SEND:
		case XFER_SERIAL1_RECV:
		case XFER_SERIAL2_RECV:
		case XFER_SERIAL3_RECV:
		case XFER_SERIAL4_RECV:
		case XFER_CRADLE_SEND:
		case XFER_CRADLE_RECV:

			for(i=0;i<300;i++)
			{
				if ( l_bRs232ReqAbort )
				{
					Rs232SetLastError(ERROR_NOT_READY);

					CloseHandle(hPort);
					
					return(NULL);
				}

				GetCommModemStatus(hPort,&dwCommMask);

				if ( dwCommMask & MS_DSR_ON )
				{
					bReady = TRUE;

					break;
				}

				Sleep(100);
			}

			break;

		case XFER_IRCOMM_SEND:

			for(i=0;i<300;i++)
			{
				BYTE Temp = IRCOMM_START;
				DWORD dwBytes;

				if ( l_bRs232ReqAbort )
				{
					Rs232SetLastError(ERROR_NOT_READY);

					CloseHandle(hPort);
					
					return(NULL);
				}

				if ( WriteFile(hPort,&Temp,sizeof(Temp),&dwBytes,NULL) &&
					 ( dwBytes == sizeof(Temp) ) )
				{
					bReady = TRUE;

					break;
				}

				Sleep(100);
			}

			break;
			
		case XFER_IRCOMM_RECV:

			for(i=0;i<300;i++)
			{
				BYTE Temp;
				DWORD dwBytes;

				if ( l_bRs232ReqAbort )
				{
					Rs232SetLastError(ERROR_NOT_READY);

					CloseHandle(hPort);
					
					return(NULL);
				}

				if ( ReadFile(hPort,&Temp,sizeof(Temp),&dwBytes,NULL) &&
					 ( dwBytes == sizeof(Temp) ) &&
					 ( Temp == IRCOMM_START ) )
				{
					bReady = TRUE;

					break;
				}

				Sleep(100);
			}
			
			break;
	}

	if ( l_bRs232ReqAbort || !bReady )
	{
		Rs232SetLastError(ERROR_NOT_READY);

		CloseHandle(hPort);
			
		return(NULL);
	}
	
	switch(g_nXferType)
	{
		case XFER_IRCOMM_SEND:

			Sleep(1000);
			
			break;

		case XFER_SERIAL1_SEND:
		case XFER_SERIAL2_SEND:
		case XFER_SERIAL3_SEND:
		case XFER_SERIAL4_SEND:
		case XFER_CRADLE_SEND:

			Sleep(1000);
			
			PurgeComm(hPort,PURGE_TXCLEAR|PURGE_RXCLEAR);

			break;

		case XFER_IRCOMM_RECV:

			break;

		case XFER_SERIAL1_RECV:
		case XFER_SERIAL2_RECV:
		case XFER_SERIAL3_RECV:
		case XFER_SERIAL4_RECV:
		case XFER_CRADLE_RECV:

			PurgeComm(hPort,PURGE_TXCLEAR|PURGE_RXCLEAR);

			break;
	}

	{
		TCHAR szMsg[MAX_PATH];
		
		TEXTCPY(szMsg,szPortName);
		TEXTCAT(szMsg,TEXT(" opened!"));
		
		SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
	}

	return(hPort);
}


//----------------------------------------------------------------------------
// Rs232RecvBlk
//----------------------------------------------------------------------------

static int Rs232RecvBlk(HANDLE hPort,
						LPSTR lpcData,
						int nSize)
{
	int nLen = 0;
	DWORD dwRead;
	DWORD dwStartTime;
	
	Rs232SetLastError(ERROR_SUCCESS);

	dwStartTime = GetTickCount();

	while ( ( nLen < nSize ) && !l_bRs232ReqAbort )
	{
		DWORD dwEllapsedTime = GetTickCount() - dwStartTime;

		if ( dwEllapsedTime >= l_dwTimeout )
		{
			return(nLen);
		}

		if ( !ReadFile(l_hPort,lpcData+nLen,nSize-nLen,&dwRead,0) )
		{
			Rs232SetLastError(GetLastError());

			dwRead = 0;
		}

		if ( dwRead > 0 )
		{
			Logdw(TEXT("Recv size = %d\r\n"),dwRead);
			Logbuf(lpcData+nLen,dwRead);

			nLen += dwRead;

			dwStartTime = GetTickCount();
		}
		else
		{
			Sleep(10);
		}

	}

	return(nLen);
}


void Rs232InQueuePurge(void)
{
	BYTE Temp;

	PurgeComm(l_hPort,PURGE_RXCLEAR);

	Rs232SetInputTimeout(l_dwPrgTimeout);
	
	while ( Rs232RecvBlk(l_hPort,&Temp,1) == 1 )
	{
		if ( l_bRs232ReqAbort )
		{
			break;
		}
	}
}


//----------------------------------------------------------------------------
// Rs232SendBlk
//----------------------------------------------------------------------------

static int Rs232SendBlk(HANDLE hPort,
						LPSTR lpcData,
						int nSize)
{
	int nLen = 0;
	DWORD dwWrite;

	Rs232SetLastError(ERROR_SUCCESS);

	while ( ( nLen < nSize ) && !l_bRs232ReqAbort )
	{
		if ( !WriteFile(l_hPort,lpcData+nLen,nSize-nLen,&dwWrite,0) )
		{
			Rs232SetLastError(GetLastError());

			dwWrite = 0;
		}

		if ( dwWrite == 0 )
		{
			break;
		}

		Logdw(TEXT("Send size = %d\r\n"),dwWrite);
		Logbuf(lpcData+nLen,dwWrite);

		nLen += dwWrite;
	}

	return(nLen);
}


//----------------------------------------------------------------------------
// Rs232GetLastError
//----------------------------------------------------------------------------

static DWORD Rs232GetLastError(void)
{
	return(l_dwLastError);
}


//----------------------------------------------------------------------------
// Rs232CleanUp
//----------------------------------------------------------------------------

static int Rs232CleanUp(BOOL bSuccess)
{
	Rs232Abort();

	// If there is an open port
	if ( l_hPort != NULL )
	{
		PurgeComm(l_hPort,PURGE_TXABORT|PURGE_TXCLEAR|PURGE_RXABORT|PURGE_RXCLEAR);
		
		// Close the port
		CloseHandle(l_hPort);

		// Mark it not open
		l_hPort = NULL;
	}
	
	// If there is an open file
	if ( l_hFile != NULL )
	{
		// Close the file
		CloseHandle(l_hFile);

		// Mark it not open
		l_hFile = NULL;
	}
	
	// Indicate that Rs232 shutdown is complete
	l_bRs232Done = TRUE;

	// If there is an xfer dialog in existence
	if ( g_hxferdlg )
	{
		// If the shutdown is due to a successful completion
		if ( bSuccess )
		{
			// Change to OK button to wait for user to acknowledge
			SendMessage(g_hxferdlg,WM_XFER_NOTIFY,XFER_OKNOCANCEL,0L);
		}
		else
		{
			int i;

			// Otherwise, simply OK the xfer dialog, since the session is already aborted
			SendMessage(g_hxferdlg,WM_COMMAND,IDOK,0L);

			for(i=0;i<100;i++)
			{
				Sleep(10);
				
				if ( g_hxferdlg == NULL )
				{
					break;
				}
			}
		}
	}

	// Return what was passed in
	return(bSuccess);
}


//----------------------------------------------------------------------------
// Rs232Recv
//----------------------------------------------------------------------------

static int Rs232Recv(LPVOID lpData,
					 int nSize,
					 char cType,
					 int nNumber)
{
	char FAR *lpcData = (char FAR *)lpData;
	char cStart;
	int nStart;
	int nRead;
	char cEnd;
	int nRetry;
	char cRsp;
	int nRsp;				

	for(nRetry=0;nRetry<MAX_RS232_RETRIES;nRetry++)
	{
		do
		{
			if ( l_bRs232ReqAbort )
			{
				// Abort the session
				Rs232CleanUp(FALSE);

				// Return failure
				return(FALSE);
			}

			Rs232SetInputTimeout(l_dwStartTimeout);

			nStart = Rs232RecvBlk(l_hPort,&cStart,1);

		} while ( ( nStart != 1 ) || ( cStart != XFER_START_MARK ) );

		if ( nStart != 1 )
		{
			Logit(TEXT("NAK, Bad start\r\n"));
						
			Rs232InQueuePurge();
		}
		else
		{
			Rs232SetInputTimeout(l_dwDataTimeout);

			// Receive a block of data
			nRead = Rs232RecvBlk(l_hPort,(char FAR*)lpcData,nSize);

			if ( nRead != nSize )
			{
				Logit(TEXT("NAK, Bad read size\r\n"));
						
				Rs232InQueuePurge();
			}
			else
			{
				int nRecvd;

				Rs232SetInputTimeout(l_dwDataTimeout);

				nRecvd = Rs232RecvBlk(l_hPort,&cEnd,1);

				if ( nRecvd != 1 )
				{
					Logit(TEXT("NAK, Missing end\r\n"));
						
					Rs232InQueuePurge();
				}
				else if ( cEnd != XFER_END_MARK )
				{
					Logit(TEXT("NAK, Bad end\r\n"));
						
					Rs232InQueuePurge();
				}
				else
				{
					LPXFERBASE lpxfer = (LPXFERBASE)lpData;

					if ( lpxfer->nRecordLength != nSize )
					{
						Logit(TEXT("NAK, Bad length\r\n"));
						
						Rs232InQueuePurge();
					}
					else if ( lpxfer->cRecordType != cType )
					{
						Logit(TEXT("NAK, Bad record type\r\n"));
						
						Rs232InQueuePurge();
					}
					else if ( lpxfer->nRecordNumber > nNumber )
					{
						Logit(TEXT("NAK, Advance record number\r\n"));
						
						Rs232InQueuePurge();
					}
					else
					{				
						if ( ( lpxfer->nRecordNumber < nNumber ) )
						{
							Logit(TEXT("Repeat record number\r\n"));

							l_nNakCount++;
						}

						cRsp = XFER_ACK;
						nRsp = Rs232SendBlk(l_hPort,&cRsp,1);

						l_nAckCount++;
						l_nRspCount++;
						l_nTryCount++;

						if ( nRsp == 1 )
						{
							return(TRUE);
						}
					}
				}
			}
		}

		Rs232InQueuePurge();

		cRsp = XFER_NAK;
		nRsp = Rs232SendBlk(l_hPort,&cRsp,1);

		l_nNakCount++;
		l_nRspCount++;
		l_nTryCount++;

		if ( nRsp != 1 )
		{
			return(FALSE);
		}
	}
	
	// If aborted
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Abort the session
	Rs232CleanUp(FALSE);
				
	MyError(TEXT("Recv"),TEXT("Retries exceeded"));

	// Return failure
	return(FALSE);
}


//----------------------------------------------------------------------------
// Rs232Send
//----------------------------------------------------------------------------

static int Rs232Send(LPVOID lpData,int nSize)
{
	char FAR * lpcData = (char FAR *)lpData;
	char cStart = XFER_START_MARK;
	int nStart;
	int nSent;
	char cEnd = XFER_END_MARK;
	char cPad = XFER_PAD;
	int nRetry;
	int nRsp;
	char cRsp;
	int i;

	Rs232InQueuePurge();

	for(nRetry=0;nRetry<MAX_RS232_RETRIES;nRetry++)
	{
		// If aborted
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		nStart = Rs232SendBlk(l_hPort,&cStart,1);
		
		// If aborted
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		if ( nStart != 1 )
		{
			// Abort the session
			Rs232CleanUp(FALSE);
				
			MyError(TEXT("Send"),TEXT("Send start mark failed"));

			return(FALSE);
		}

		// Send a block of data
		nSent = Rs232SendBlk(l_hPort,lpcData,nSize);

		// If aborted
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		if ( nSent != nSize )
		{
			// Abort the session
			Rs232CleanUp(FALSE);
			
			MyError(TEXT("Send"),TEXT("Send data failed"));

			return(FALSE);
		}

		nSent = Rs232SendBlk(l_hPort,&cEnd,1);

		// If aborted
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		if ( nSent != 1 )
		{
			// Abort the session
			Rs232CleanUp(FALSE);
			
			MyError(TEXT("Send"),TEXT("Send end mark failed"));

			return(FALSE);
		}

		for(i=0;i<PAD_COUNT;i++)
		{
			nSent = Rs232SendBlk(l_hPort,&cPad,1);

			// If aborted
			if ( l_bRs232ReqAbort )
			{
				// Abort the session
				Rs232CleanUp(FALSE);

				// Return failure
				return(FALSE);
			}

			if ( nSent != 1 )
			{
				// Abort the session
				Rs232CleanUp(FALSE);
				
				MyError(TEXT("Send"),TEXT("Send pad failed"));

				return(FALSE);
			}
		}

		Rs232SetInputTimeout(l_dwAckTimeout);

		nRsp = Rs232RecvBlk(l_hPort,&cRsp,1);

		// If aborted
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		l_nTryCount++;
			
		if ( nRsp != 1 )
		{
			Logit(TEXT("No response\r\n"));
			
			Rs232OutQueuePurge();

			continue;
		}

		l_nRspCount++;
			
		if ( cRsp == XFER_ACK )
		{
			l_nAckCount++;
			
			Logit(TEXT("ACK\r\n"));
			
			Rs232OutQueuePurge();
			
			return(TRUE);
		}

		if ( cRsp == XFER_NAK )
		{
			l_nNakCount++;
			
			Logit(TEXT("NAK\r\n"));
			
			Rs232OutQueuePurge();

			continue;
		}

		Logit(TEXT("Unknown response\r\n"));
			
		Rs232OutQueuePurge();

		continue;
	}

	// Abort the session
	Rs232CleanUp(FALSE);
				
	MyError(TEXT("Send"),TEXT("Retries exceeded"));

	// Return failure
	return(FALSE);
}


//----------------------------------------------------------------------------
// RS232_ReceiveFile
//----------------------------------------------------------------------------

static BOOL WINAPI RS232_ReceiveFile(HWND hwnd)
{
	int Retries = MAXRETRIES;

	// Assume no file created yet
	l_lpszFullSpec = NULL;

	// Start off with no abort requested or completed
	l_bRs232Done = FALSE;
	l_bRs232ReqAbort  = FALSE;

	// Report to xfer dialog that we are connecting as server and listening for connection
 	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTING_TO_RECV,0L);
	
	// Open serial port
	l_hPort = Rs232Open();

	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	if ( l_hPort == NULL )
	{
		DWORD dwErrNum;

		dwErrNum = Rs232GetLastError();
		
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("Rs232Open"),dwErrNum);
				
		// Return failure
		return(FALSE);
	}

	// Report to xfer dialog that we are connected to receive and awaiting file header
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTED_TO_RECV,0L);
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_HEADER,0L);

	// Receive the header, if we cannot
	if ( !Rs232Recv(&header,sizeof(header),XFER_HEADER,0) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	ExtractHeaderInfo(&header);
	
	// Start file spec of receive file with selected path
	TEXTCPY(g_ii.szFullSpec,g_ii.szPathName);

	// Append the received file name from the header
#ifdef UNICODE
	{
		TCHAR szFileName[MAX_PATH];
		mbstowcs(szFileName,header.szFileName,TEXTSIZEOF(szFileName)-1);
		szFileName[TEXTSIZEOF(szFileName)-1] = TEXT('\0');
		TEXTCAT(g_ii.szFullSpec,szFileName);
	}
#else
	TEXTCAT(g_ii.szFullSpec,header.szFileName);
#endif

	// Save the file size from the header
	g_ii.dwFileSize = header.dwFileSize;

	// Open the target file for writing
	l_hFile = CreateFile(g_ii.szFullSpec,
					   GENERIC_WRITE,
					   0,
					   NULL,
					   CREATE_NEW,
					   FILE_ATTRIBUTE_NORMAL,
					   0);

	// If the file could not be opened
	if ( l_hFile == INVALID_HANDLE_VALUE )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		LastError(TEXT("CreateFile - CREATE_NEW"));

		// Return failure
		return(FALSE);
	}
	
	// File exists now, may need to be deleted later
	l_lpszFullSpec = g_ii.szFullSpec;
	
	header_reply.cRecordType = XFER_HEADER_REPLY;
	header_reply.dwFileSize = header.dwFileSize;
	header_reply.nRecordLength = sizeof(XFERHEADER);
	header_reply.nRecordNumber = 0;

	SetHeaderInfo(&header_reply);

	l_nRecord = 0;
	l_nRecords = 0;
	
	// Send the header reply. if we cannot
	if ( !Rs232Send(&header_reply,sizeof(header_reply)) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to xfer dialog that we are awaiting data
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_DATA,(LPARAM)g_szRemoteName);

	// Compute the number of Rs232 records required for the file
	l_nRecords = (int) ( ( g_ii.dwFileSize + l_nDataMax - 1 )
					   / l_nDataMax );

	l_dwBeginTime = GetTickCount();	
	
	// Loop for number of records to be received
	for(l_nRecord=1;l_nRecord<=l_nRecords;l_nRecord++)
	{
		DWORD dwSize = l_nDataMax;
		DWORD dwWritten;
		DWORD dwDesired;

		{
			TCHAR szMsg[MAX_PATH];
			DWORD dwTime;
			DWORD dwChars;
			DWORD dwCps;
			
			dwTime = ( GetTickCount() - l_dwBeginTime );
			dwChars = ( l_nRecord * dwSize );
			if ( dwTime > 0 )
			{
				dwCps   = ( dwChars * 1000 ) /  dwTime;
			}
			else
			{
				dwCps = 0;
			}
			
			if ( l_nNakCount > 0 )
			{
				TEXTSPRINTF(szMsg,
							TEXT("%d of %d @ %d # %d"),
							l_nRecord,
							l_nRecords,
							dwCps,
							l_nNakCount);
			}
			else
			{
				TEXTSPRINTF(szMsg,
							TEXT("%d of %d @ %d"),
							l_nRecord,
							l_nRecords,
							dwCps);
			}
			
			SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
		}

		// If there is less than a full Rs232 record to be received
		if ( g_ii.dwFileSize < dwSize )
		{
			// Make this a short record
			dwSize = g_ii.dwFileSize;
		}

		// Compute size of Rs232 packet desired
		dwDesired = sizeof(data)-sizeof(data.bData)+(int)dwSize;

		// Receive the next Rs232 packet, if we cannot
		if ( !Rs232Recv(&data,dwDesired,XFER_DATA,l_nRecord) )
		{
			// Abort the session
			Rs232CleanUp(FALSE);
			
			// Return failure
			return(FALSE);
		}

		if ( data.nRecordNumber != l_nRecord )
		{
			l_nRecord--;

			l_nNakCount++;

			continue;
		}
		
		// Reduce remaining file size of size of data received
		g_ii.dwFileSize -= dwSize;

		// Write the data to the file, if we cannot
		if ( !WriteFile(l_hFile,
						data.bData,
						dwSize,
						&dwWritten,
						NULL) )
		{
			// Abort the session
			Rs232CleanUp(FALSE);
			
			// Report the error to the user
			LastError(TEXT("WriteFile"));

			// Return failure
			return(FALSE);
		}

		// Report to the xfer dialog the progress so far
		{
			long nProgress = (long)( header.dwFileSize -
									 g_ii.dwFileSize )
								   * 100L
								   / (long) header.dwFileSize;
			SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SET_PROGRESS,nProgress);
		}

		// If the size written to file is wrong
		if ( dwWritten != dwSize )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Report the error to the user
			MyError(TEXT("WriteFile"),
					TEXT("Size mismatch"));

			// Return failure
			return(FALSE);
		}

		// If an abort has been requested
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}
	}

	// Report to xfer dialog that we finished data and are awaiting trailer record
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_TRAILER,(LPARAM)g_szRemoteName);

	// Receive trailer record, if we cannot
	if ( !Rs232Recv(&trailer,sizeof(trailer),XFER_TRAILER,l_nRecords+1) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If the header and trailer sizes don't match
	if ( header.dwFileSize != trailer.dwFileSize )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		MyError(TEXT("File size mismatch"),
			    TEXT("Header and Trailer"));

		// Return failure
		return(FALSE);
	}
	
	// Report to the xfer dialog that the file was received successfully
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_RECEIVE_COMPLETED,(LPARAM)g_szRemoteName);

	Sleep(1000);
	
	// Close the file
	CloseHandle(l_hFile);

	// Mark it as not open
	l_hFile = NULL;

	// End the session
	Rs232CleanUp(TRUE);
	
	// Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// Rs232ReceiveFile
//----------------------------------------------------------------------------

BOOL WINAPI Rs232ReceiveFile(LPVOID lpVoid)
{
	BOOL bResult;

#ifdef _WIN32_WCE
#ifndef WIN32_PLATFORM_POCKETPC
	SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_BELOW_NORMAL);
#endif
#endif
							
	// Convert thread calling parameter to window handle and pass on
	bResult = RS232_ReceiveFile((HWND)lpVoid);

	// If we failed
	if ( !bResult )
	{
		// If a file was created

		if ( l_lpszFullSpec != NULL )
		{
			// Delete the file
			DeleteFile(l_lpszFullSpec);

			// Indicate no file created
			l_lpszFullSpec = NULL;
		}
	}
	
	// Return the result
	return(bResult);
}


//----------------------------------------------------------------------------
// RS232_SendFile
//----------------------------------------------------------------------------

static BOOL WINAPI RS232_SendFile(HWND hwnd)
{
	l_bRs232Done = FALSE;
	l_bRs232ReqAbort  = FALSE;
	
	// Report to the idra dialog that we are connecting as client and calling
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTING_TO_SEND,0L);
					
	// Open serial port
	l_hPort = Rs232Open();
		
	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	if ( l_hPort == NULL )
	{
		DWORD dwErrNum;

		dwErrNum = Rs232GetLastError();
		
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		ReportError(TEXT("Rs232Open"),dwErrNum);
				
		// Return failure
		return(FALSE);
	}

	// Report to the xfer dialog that we are connected as a client and sending header
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_CONNECTED_TO_SEND,0L);
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_HEADER,0L);

	// Get the properties of the file to be sent, if we cannot
	if ( !GetFileProperties(hwnd) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		MyError(TEXT("Cannot get file info"),
			    g_ii.szFullSpec);

		// Return failure
		return(FALSE);
	}
	
	// Initialize header record with file name and information
	header.nRecordLength = sizeof(XFERHEADER);
	header.nRecordNumber = 0;
	header.cRecordType = XFER_HEADER;

#ifdef UNICODE
	wcstombs(header.szFileName,(LPTSTR)g_ii.szItemName,sizeof(header.szFileName)-1);
	header.szFileName[sizeof(header.szFileName)-1] = '\0';
#else
	TEXTNCPY(header.szFileName,g_ii.szItemName,TEXTSIZEOF(header.szFileName)-1);
	header.szFileName[TEXTSIZEOF(header.szFileName)-1] = TEXT('\0');
#endif

	header.dwFileSize = g_ii.dwFileSize;

	SetHeaderInfo(&header);

	// Open the file for reading
	l_hFile = CreateFile(g_ii.szFullSpec,
					   GENERIC_READ,
					   0,
					   NULL,
					   OPEN_EXISTING ,
					   FILE_ATTRIBUTE_NORMAL,
					   0);

	// If we cannot open the file
	if ( l_hFile == INVALID_HANDLE_VALUE )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Report the error to the user
		LastError(TEXT("CreateFile"));

		// Return failure
		return(FALSE);
	}
	
	l_nRecord = 0;
	l_nRecords = 0;
	
	// Send the header. if we cannot
	if ( !Rs232Send(&header,sizeof(header)) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to the xfer dialog that we are awaiting a reply
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_AWAITING_REPLY,0L);

	// Receive the header reply, if we cannot
	if ( !Rs232Recv(&header_reply,sizeof(header_reply),XFER_HEADER_REPLY,0) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	ExtractHeaderInfo(&header_reply);

	// If an abort has been requested
	if ( l_bRs232ReqAbort )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Compute number of records to be sent for the file
	l_nRecords = (int) ( ( g_ii.dwFileSize + l_nDataMax - 1 )
					   / l_nDataMax );

	// Report to xfer dialog that we are sending data
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_DATA,(LPARAM)g_szRemoteName);

	l_dwBeginTime = GetTickCount();	
	
	// Loop through the records to be sent
	for(l_nRecord=1;l_nRecord<=l_nRecords;l_nRecord++)
	{
		DWORD dwSize = l_nDataMax;
		DWORD dwRead;
		DWORD dwDesired;

		{
			TCHAR szMsg[MAX_PATH];
			DWORD dwTime;
			DWORD dwChars;
			DWORD dwCps;
			
			dwTime = ( GetTickCount() - l_dwBeginTime );
			dwChars = ( l_nRecord * dwSize );
			if ( dwTime > 0 )
			{
				dwCps   = ( dwChars * 1000 ) /  dwTime;
			}
			else
			{
				dwCps = 0;
			}
			
			if ( l_nNakCount > 0 )
			{
				TEXTSPRINTF(szMsg,
							TEXT("%d of %d @ %d # %d"),
							l_nRecord,
							l_nRecords,
							dwCps,
							l_nNakCount);
			}
			else
			{
				TEXTSPRINTF(szMsg,
							TEXT("%d of %d @ %d"),
							l_nRecord,
							l_nRecords,
							dwCps);
			}
			
			SendMessage(g_hdlg,WM_XFER_NOTIFY,XFER_STATUS,(LPARAM)szMsg);
		}

		// If the remaining data is less than a full Rs232 record
		if ( g_ii.dwFileSize < dwSize )
		{
			// Make it a short record
			dwSize = g_ii.dwFileSize;
		}

		// Compute size of Rs232 packet to be sent
		dwDesired = sizeof(data)-sizeof(data.bData)+(int)dwSize;

		// Fill in packet information
		data.nRecordLength = (int)dwDesired;
		data.nRecordNumber = l_nRecord;
		data.cRecordType = XFER_DATA;
		
		// Read file data into packet, if we cannot
		if ( !ReadFile(l_hFile,
					   data.bData,
					   dwSize,
					   &dwRead,
					   NULL) )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Report the error to the user
			LastError(TEXT("WriteFile"));

			// Return failure
			return(FALSE);
		}

		// If size read from file does not match
		if ( dwRead != dwSize )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Report the error to the user
			MyError(TEXT("ReadFile"),
					TEXT("Size mismatch"));

			// Return failure
			return(FALSE);
		}

		// Send the packet, if we cannot
		if ( !Rs232Send(&data,sizeof(data)-sizeof(data.bData)+(int)dwSize) )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		// If an abort has been requested
		if ( l_bRs232ReqAbort )
		{
			// Abort the session
			Rs232CleanUp(FALSE);

			// Return failure
			return(FALSE);
		}

		// Reduce remaining size by size sent
		g_ii.dwFileSize -= dwSize;

		// Report to the xfer dialog the progress so far
		{
			long nProgress = (long)( header.dwFileSize -
									 g_ii.dwFileSize )
								   * 100L
								   / (long) header.dwFileSize;
			
			SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SET_PROGRESS,nProgress);
		}
	}

	// Report to the xfer dialog that we are done with data and sending trailer
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SENDING_TRAILER,(LPARAM)g_szRemoteName);

	// Fill in trailer information
	trailer.nRecordLength = sizeof(XFERTRAILER);
	trailer.nRecordNumber = l_nRecords+1;
	trailer.cRecordType = XFER_TRAILER;
	trailer.dwFileSize = header.dwFileSize;

	// Send the trailer, if we cannot
	if ( !Rs232Send(&trailer,sizeof(trailer)) )
	{
		// Abort the session
		Rs232CleanUp(FALSE);

		// Return failure
		return(FALSE);
	}

	// Report to the xfer dialog that we are done sending the file
	SendMessage(hwnd,WM_XFER_NOTIFY,XFER_SEND_COMPLETED,(LPARAM)g_szRemoteName);

	// Close the file
	CloseHandle(l_hFile);

	// Mark it as not open
	l_hFile = NULL;

	// End the session
	Rs232CleanUp(TRUE);

	// Return success
	return(TRUE);
}


//----------------------------------------------------------------------------
// Rs232SendFile
//----------------------------------------------------------------------------

BOOL WINAPI Rs232SendFile(LPVOID lpVoid)
{
	DWORD bResult;

#ifdef _WIN32_WCE
#ifndef WIN32_PLATFORM_POCKETPC
	SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_BELOW_NORMAL);
#endif
#endif
							
	// Convert thread calling parameter to window handle and pass on
	bResult = RS232_SendFile((HWND)lpVoid);

	return(bResult);
}


//----------------------------------------------------------------------------
// void Rs232Abort(void)
//----------------------------------------------------------------------------

void Rs232Abort(void)
{
	l_bRs232ReqAbort = TRUE;
}


//----------------------------------------------------------------------------
// void IsRs232Done(void)
//----------------------------------------------------------------------------

BOOL IsRs232Done(void)
{
	// Return whether Rs232 has finished aborting
	return(l_bRs232Done);
}

