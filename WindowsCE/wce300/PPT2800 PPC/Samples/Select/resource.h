//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_PROGRAMDESCR                1
#define IDC_SEL_SCROLL_UP               301
#define IDC_SEL_SCROLL_DOWN             302
#define IDC_SEL_LISTVIEW                303
#define IDD_SELECT_PPC_STD              304
#define IDD_SELECT_7500_STD             305
#define IDD_SELECT_PPC_SIP              306
#define IDD_SELECT_7500_TSK             307
#define IDD_SELECT_7200_TSK             308
#define IDD_SELECT_7200_STD             309
#define IDD_SELECT_PC_STD               310
#define IDD_SELECT_HPC_STD              311
#define IDD_SELECT_POCKETPC_STD         312
#define IDD_SELECT_POCKETPC_SIP         313

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        312
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
