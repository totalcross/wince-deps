
//--------------------------------------------------------------------
// FILENAME:			Select.h
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for Select.c
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SELECT_H_

#define SELECT_H_

#ifdef __cplusplus
extern "C"
{
#endif


//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------


#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("SelectSample")
#define APP_TITLE TEXT("Select Sample Program")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#ifdef __cplusplus
}
#endif

#endif	/* #ifndef SELECT_H_	*/

