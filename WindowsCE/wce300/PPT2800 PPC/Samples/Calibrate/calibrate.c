
//--------------------------------------------------------------------
// FILENAME:            calibrate.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Touch Screen Calibration
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
// calibrate.c : Defines the entry point for the application.
//
#include <windows.h>
BOOL WINAPI TouchCalibrate(void);


int WINAPI WinMain(	HINSTANCE hInstance, HINSTANCE hPrevInstance,LPTSTR lpCmdLine,int nCmdShow)
{
	HANDLE hTouchCalcThresholdEvent;

	// Run the coredll function to calibrate the screen and write to registry.
	TouchCalibrate();

	// After calibration...force touch driver to update control area threshold.
	// This is dont by signaling the named event TouchCalcThreshold

	hTouchCalcThresholdEvent = CreateEvent(	NULL,
											FALSE,
											FALSE,
											TEXT("TouchCalcThreshold")
											);

	// Signal event
	SetEvent(hTouchCalcThresholdEvent);


	// Close event handle
	CloseHandle(hTouchCalcThresholdEvent);

	return TRUE;
}

