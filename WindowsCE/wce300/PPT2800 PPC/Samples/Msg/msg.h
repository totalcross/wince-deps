
//--------------------------------------------------------------------
// FILENAME:        msg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     MSG.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef MSG_H_

#define MSG_H_

#ifdef __cplusplus
extern "C"
{
#endif

//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------

#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("MessageTest")
#define APP_TITLE TEXT("Message Text Program")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef MSG_H_   */
