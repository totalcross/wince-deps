//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_PROGRAMTITLE                200
#define IDC_MSGSTR1                     201
#define IDC_MSGSTR2                     202
#define IDD_MSG_PPC_STD                 204
#define IDD_YESNO_PPC_STD               206
#define IDD_YESNO_PPC_SIP               207
#define IDD_MSG_PPC_SIP                 208
#define IDD_YESNO_7500_TSK              209
#define IDD_YESNO_7500_STD              210
#define IDD_MSG_7500_TSK                213
#define IDD_MSG_7500_STD                214
#define IDD_YESNO_7200_TSK              217
#define IDD_YESNO_7200_STD              218
#define IDD_MSG_7200_TSK                220
#define IDD_MSG_7200_STD                221
#define IDD_MSG_PC_STD                  222
#define IDD_YESNO_PC_STD                223
#define IDD_MSG_HPC_STD                 224
#define IDD_YESNO_HPC_STD               225

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        230
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
