
//--------------------------------------------------------------------
// FILENAME:			MyInit.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains application specific initialization routines.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMain.h"
#include "..\common\StdMsg.h"
#include "..\common\StdScrns.h"
#include "..\common\StdStrng.h"

#include "CmdLine.h"

#include "ScanSamp2.h"


//----------------------------------------------------------------------------
// MyInitApplication
//----------------------------------------------------------------------------

BOOL MyInitApplication(HINSTANCE hInstance,
					   LPTSTR szAppClass,
					   HBRUSH hBackgroundBrush,
					   LPTSTR szMainMenu,
					   LPTSTR szAppIcon,
					   BOOL bMultipleInstances)
{
	HWND hWnd;

	RECT workarea;

	g_hInstance = hInstance;

	GetWorkArea(&workarea);

	// If this applications does not allow multiple running instances
	if ( !bMultipleInstances )
	{
		// Find another running instance of this application
		hWnd = FindWindow(szAppClass,NULL);

		// If one was found
		if ( hWnd != NULL )
		{
			// Make the other instance the foreground window
			SetForegroundWindow(hWnd);

			// Make it visible
			ShowWindow(hWnd,SW_SHOW);

			if ( g_bAutoStartTrigger )
			{
                PostMessage(hWnd,WM_COMMAND,IDC_TRIGGER,0L);
			}

			// And exit this instance
			return(FALSE);
		}
	}

    // Initialize basic window class information
	g_wc.style         = CS_VREDRAW | CS_HREDRAW;
    g_wc.lpfnWndProc   = MainWndProc;
    g_wc.cbClsExtra    = 0;
    g_wc.cbWndExtra    = 0;
    g_wc.hInstance     = hInstance;

#ifdef _WIN32_WCE

    // WCE has no cursor
    g_wc.hCursor       = NULL;

	// WCE has no menu (since it uses command bars/bands instead)
    g_wc.lpszMenuName  = NULL;

#else

    // Win32 uses a cursor
	g_wc.hCursor	   = LoadCursor(NULL,IDC_ARROW);

	// Win32 uses a menu
    g_wc.lpszMenuName  = szMainMenu;

#endif

	// Set up application icon
	g_wc.hIcon		   = LoadIcon(hInstance,szAppIcon);

	// Set up background brush
    g_wc.hbrBackground = hBackgroundBrush;

	// Set up class name
    g_wc.lpszClassName = szAppClass;

    // If we could not register the class
	if ( !RegisterClass(&g_wc) )
	{
		// Report an error to the user
		LastError(TEXT("Register Class"));

		// Return failed to initialize
		return(FALSE);
	}

    // Return success in initializing
	return(TRUE);
}


