
//--------------------------------------------------------------------
// FILENAME:			ScanDlg.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains window procedures for the scanning dialogs
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

//#define _STDDEBUG
#include "..\common\StdDebug.h"

#include "..\common\StdWproc.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdScrns.h"
#include "..\common\StdAbout.h"
#include "..\common\StdMsg.h"

#include "..\common\Scan.h"
#include "..\common\ScanSel.h"

#include "ScanParams.h"

#include "ScanSamp2.h"


#define _DO_SCANNING


#define SCAN_TIMEOUT 0

#define UPCEAN_GENERAL -1

DIALOG_CHOICE ScanDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_SCAN_PC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_SCAN_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_SCAN_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SCAN_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_SCAN_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_SCAN_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_SCAN_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_SCAN_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_SCAN_7500_TSK,	NULL,			UI_STYLE_DEFAULT }
};

DIALOG_CHOICE ScanDataDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_DATA_PC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_DATA_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_DATA_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_DATA_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_DATA_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_DATA_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_DATA_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_DATA_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_DATA_7500_TSK,	NULL,			UI_STYLE_DEFAULT }

};


// Hold area for saving previous dialog box handles
LPTSTR g_lpszScanStr = NULL;

//----------------------------------------------------------------------------
// ScanDataDlgProc
//----------------------------------------------------------------------------
LRESULT CALLBACK ScanDataDlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	static HWND hED4;
	
	switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:
			
			{    
				HWND hWndEdit1;

				MaxDialog(hwnd);

				hWndEdit1 = GetDlgItem((HWND)lParam,IDC_EDIT1);

				if ( hWndEdit1 != NULL )
				{
					DWORD dwLen;
					LPTSTR lpszText;

					dwLen = Edit_GetTextLength(hWndEdit1)+1;
				
					lpszText = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));

					if ( lpszText != NULL )
					{
						Edit_GetText(hWndEdit1,lpszText,dwLen);

						hED4 = GetDlgItem(hwnd,IDC_EDIT4);
			
						Edit_SetText(hED4,lpszText);

						Edit_SetSel(hED4,0,0);

						SetFocus(hED4);

						LocalFree((HLOCAL)lpszText);

						break;
					}
				}

				PostMessage(hwnd,WM_COMMAND,IDOK,0L);
			}
			
			// Return FALSE since focus was set manually
			return(FALSE);


         case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_RETARD:
					SendMessage(hED4,WM_HSCROLL,SB_LINELEFT,0L);
					SendMessage(hED4,WM_HSCROLL,SB_LINELEFT,0L);
					break;

				case IDC_ADVANCE:
					SendMessage(hED4,WM_HSCROLL,SB_LINERIGHT,0L);
					SendMessage(hED4,WM_HSCROLL,SB_LINERIGHT,0L);
					break;

				case IDC_POPUP:	// used as a mean to exit the dialog box
				case IDCANCEL:	// Clear Key
				case IDDONE:
				case IDOK:

					// Exit the dialog
					ExitDialog();

					break;
            }

		// Command was processed
        return (TRUE);
	}		
	// Message was not processed
    return (FALSE);
}


void ScanData(HWND hwnd)
{
	g_hdlg = CreateChosenDialog(g_hInstance,
								ScanDataDialogChoices,
								g_hwnd,
								ScanDataDlgProc,
								(LPARAM)hwnd);

	WaitUntilDialogDoneSkip(FALSE);
}


typedef struct tagSCANDLGSAVE
{
	BOOL bHadFocus;

	LPTSTR lpszEdit1;
	LPTSTR lpszEdit2;
	LPTSTR lpszEdit3;
	LPTSTR lpszEdit4;
	LPTSTR lpszEdit5;
	LPTSTR lpszEdit6;

} SCANDLGSAVE;

typedef SCANDLGSAVE FAR *LPSCANDLGSAVE;


//----------------------------------------------------------------------------
// ScanDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK ScanDlgProc(HWND hwnd,
							 UINT uMsg,
							 WPARAM wParam,
					         LPARAM lParam)
{
	HWND hCtl;
    LPSCANDLGSAVE lpScanDialogSave;

	//DEBUG_MSG(TEXT("ScanDlgProc"),hwnd,uMsg,wParam,lParam);

	switch(uMsg)
    {
		case UM_RESIZE:
			
			DEBUG_MSG(TEXT("ScanDlgProc UM_RESIZE"),hwnd,uMsg,wParam,lParam);

			lpScanDialogSave = LocalAlloc(LPTR,sizeof(SCANDLGSAVE));

			if ( lpScanDialogSave != NULL ) 
			{
				DWORD dwLen;

				ZEROMEM(lpScanDialogSave,sizeof(SCANDLGSAVE));

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT1);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit1 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit1 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit1,dwLen);
					}

					lpScanDialogSave->bHadFocus = ( GetFocus() == hCtl );
				}

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT2);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit2 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit2 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit2,dwLen);
					}
				}

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT3);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit3 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit3 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit3,dwLen);
					}
				}

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT4);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit4 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit4 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit4,dwLen);
					}
				}

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT5);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit5 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit5 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit5,dwLen);
					}
				}

				hCtl = GetDlgItem(g_hdlg,IDC_EDIT6);
				if ( hCtl != NULL )
				{
					dwLen = Edit_GetTextLength(hCtl)+1;				
					lpScanDialogSave->lpszEdit6 = (LPTSTR)LocalAlloc(LPTR,dwLen*sizeof(TCHAR));
					if ( lpScanDialogSave->lpszEdit6 != NULL )
					{
						Edit_GetText(hCtl,lpScanDialogSave->lpszEdit6,dwLen);
					}
				}
			}
			
			// Resize dialog, using save information
			ResizeDialog(lpScanDialogSave);

			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the scan edit field
			SetFocus(GetDlgItem(g_hdlg,IDC_EDIT1));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			DEBUG_MSG(TEXT("ScanDlgProc WM_INITDIALOG"),hwnd,uMsg,wParam,lParam);

			MaxDialog(hwnd);
			
			if ( IsDialogResizing() )
			{
				lpScanDialogSave = GetDialogSaveInfo();

				if ( lpScanDialogSave != NULL )
				{
					if ( lpScanDialogSave->lpszEdit1 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT1);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit1);

							if ( lpScanDialogSave->bHadFocus )
							{
								SetFocus(hCtl);
							}
						}

						LocalFree(lpScanDialogSave->lpszEdit1);
					}

					if ( lpScanDialogSave->lpszEdit2 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT2);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit2);
						}

						LocalFree(lpScanDialogSave->lpszEdit2);
					}

					if ( lpScanDialogSave->lpszEdit3 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT3);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit3);
						}

						LocalFree(lpScanDialogSave->lpszEdit3);
					}
					
					if ( lpScanDialogSave->lpszEdit4 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT4);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit4);
						}

						LocalFree(lpScanDialogSave->lpszEdit4);
					}

					if ( lpScanDialogSave->lpszEdit5 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT5);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit5);
						}

						LocalFree(lpScanDialogSave->lpszEdit5);
					}


					if ( lpScanDialogSave->lpszEdit6 != NULL )
					{
						hCtl = GetDlgItem(hwnd,IDC_EDIT6);

						if ( hCtl != NULL )
						{
							Edit_SetText(hCtl,lpScanDialogSave->lpszEdit6);
						}

						LocalFree(lpScanDialogSave->lpszEdit6);
					}

					LocalFree(lpScanDialogSave);
				}
			}
			else
			{
#ifdef _DO_SCANNING
				if ( ScanInit(hwnd) != E_SCN_SUCCESS )
				{
					PostMessage(hwnd,WM_COMMAND,IDCANCEL,0);
					break;
				}
#endif

				hCtl = GetDlgItem(hwnd,IDC_EDIT1);

				SetFocus(hCtl);
			}

			PostMessage(hwnd,UM_INIT,0,0L);

            // Return FALSE since focus will be set manually
			return(FALSE);

		case UM_INIT:

			DEBUG_MSG(TEXT("ScanDlgProc UM_INIT"),hwnd,uMsg,wParam,lParam);

			PostMessage(hwnd,UM_STARTSCANNING,0,0L);

			break;

		case UM_STARTSCANNING:

#ifdef _DO_SCANNING

#ifdef SYMBOL_SCAN_EVENTS
			ScanRegisterForEvents(g_hdlg,UM_EVENT);
#endif

			StartScanning(g_hdlg,UM_SCAN,SCAN_TIMEOUT);
#endif

			break;

		case UM_STOPSCANNING:

#ifdef _DO_SCANNING

#ifdef SYMBOL_SCAN_EVENTS
			ScanUnregisterForEvents();
#endif

			StopScanning();
#endif

			break;
				

		case WM_ACTIVATE:

#ifdef _DO_SCANNING
			Scan_OnActivate(wParam,hwnd,UM_SCAN,SCAN_TIMEOUT);
#endif

			break;

		case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_HELP_ABOUT:
					
					SendMessage(hwnd,UM_STOPSCANNING,0,0L);

					About(hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					PostMessage(hwnd,UM_STARTSCANNING,0,0L);

					break;

				case IDC_CONFIG:

					SendMessage(hwnd,UM_STOPSCANNING,0,0L);

					ScanCfg(FALSE);

					PostMessage(hwnd,UM_STARTSCANNING,0,0L);

					break;

				case IDC_POPUP:

#ifdef _WIN32_WCE

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							InvokePopUpMenu(hwnd,IDM_POPUP,2,0,0);

							break;

						case UI_STYLE_PEN:
						case UI_STYLE_FINGER:
							
							break;
					}

#endif

					break;

				case IDC_PARAMS:

					SendMessage(hwnd,UM_STOPSCANNING,0,0L);

					ScanPrm(FALSE);

					PostMessage(hwnd,UM_STARTSCANNING,0,0L);

					break;

				case IDC_DATA:

					SendMessage(hwnd,UM_STOPSCANNING,0,0L);

					ScanData(hwnd);

					PostMessage(hwnd,UM_STARTSCANNING,0,0L);

					break;

				case IDC_RETURN:
				
					keybd_event(VK_RETURN,0,0,0);
					keybd_event(VK_RETURN,0,KEYEVENTF_KEYUP,0);

					break;

				case IDC_TRIGGER:
					
#ifdef _DO_SCANNING
					SetFocus(GetDlgItem(hwnd,IDC_EDIT1));

					ScanSetSoftTrigger(FALSE);
					ScanSetSoftTrigger(TRUE);
#endif
					
					break;

				case IDOK:
				case IDCANCEL:

					SendMessage(hwnd,UM_STOPSCANNING,0,0L);

					if ( !IsDialogResizing() )
					{
						ScanSetSoftTrigger(FALSE);

						ScanTerm();
					}

					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
		case UM_SCAN:

			if ( hwnd != g_hdlg )
			{
				break;
			}

#ifdef _DO_SCANNING

			// If a popup menu is currently active
			if ( IsInPopUpMenu() )
			{
				// Start the process of dismissing the popup menu
				CancelPopUpMenu();

				// If we want to pop up a dialog, we must wait until later
				// when the popup menu is not active, so repost the message
				// to ourselves
				PostMessage(hwnd,uMsg,wParam,lParam);

				// Do nothing more for now, and check back later
				break;
			}

			// Get result of scan, may be completed, failed, or ignore
			switch(ScanGetResult(wParam,lParam))
			{
				// Scan completed successfully
				case SCAN_RESULT_COMPLETED:
					
					{
						LPTSTR lpszStr;
						TCHAR szLabelType[10];
						DWORD dwLabelType;
						
						Scan_SetForegroundWindow(g_hwnd);

						// Get the scan data, if successful
						if ( ( g_lpszScanStr = ScanGetData(wParam,lParam) ) != NULL )
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT1);
			
							// Set the data into the scanning field
							if ( hctl != NULL )
							{
								Edit_SetText(hctl,g_lpszScanStr);
							}
						}

						// Get the label type, if successful
						if ( ( dwLabelType = ScanGetType(wParam,lParam) ) != LABELTYPE_UNKNOWN )
						{
							// Format label type as a hex constant for display
							TEXTSPRINTF(szLabelType,TEXT("0x%.2X"),dwLabelType);
						}
						else
						{
							// Use ??? for unknown label type
							TEXTCPY(szLabelType,TEXT("???"));
						}

						if ( *szLabelType )
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT2);
							
							if ( hctl != NULL )
							{
								// Set the formatted label type string into the label type field
								Edit_SetText(hctl,szLabelType);
							}
						}
						
						// Get the data source string, if successful
						if ( ( lpszStr = ScanGetSource(wParam,lParam) ) != NULL )
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT3);
			
							if ( hctl != NULL )
							{
								// Set the data source string into the source field
								Edit_SetText(hctl,lpszStr);
							}
						}
						
						// Get the time stamp string, if successful
						if ( ( lpszStr = ScanGetTimeStamp(wParam,lParam) ) != NULL )
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT4);

							// Set the time stamp string into the time stamp field
							if ( hctl != NULL )
							{
								Edit_SetText(hctl,lpszStr);
							}
						}
					
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT5);

							// Set the length
							if ( hctl != NULL )
							{
								TCHAR szLen[MAX_PATH];

								TEXTSPRINTF(szLen,TEXT("%d"),ScanGetDataLength(wParam,lParam));
								Edit_SetText(hctl,szLen);
							}
						}
						
						{
							HWND hctl = GetDlgItem(hwnd,IDC_EDIT6);

							// Set the length
							if ( hctl != NULL )
							{
								TCHAR szLen[MAX_PATH];

								TEXTSPRINTF(szLen,TEXT("%d"),ScanGetDataLength(wParam,lParam));
								Edit_SetText(hctl,szLen);
							}
						}
						
						PostMessage(hwnd,UM_STARTSCANNING,0,0L);
					}
					
					break;

				// Scan failed with an error
				case SCAN_RESULT_FAILED:
					
					{
						ReportError(TEXT("ScanSubmitRead UM_SCAN"),
								    ScanGetStatus(wParam,lParam));
					
						if ( MyYesNo(TEXT("Do you want to"),
									 TEXT("Retry the scan?")) )
						{
							PostMessage(hwnd,UM_STARTSCANNING,0,0L);
						}
					}

					break;

				// Scan message should be ignored
				case SCAN_RESULT_IGNORE:

					break;
			}
#endif

			break;

#ifdef _DO_SCANNING

#ifdef SYMBOL_SCAN_EVENTS
		case UM_EVENT:
			{
				LPTSTR lpszStr;

				lpszStr = ScanGetEvent(wParam,lParam);

				if ( lpszStr != NULL )
				{
					HWND hctl = GetDlgItem(hwnd,IDC_EDIT6);
	
					// Set the data into the scanning field
					if ( hctl != NULL )
					{
						Edit_SetText(hctl,lpszStr);
					}
				}
			}
			break;
#endif

#endif

    }

	// Message was not processed
    return (FALSE);
}



//----------------------------------------------------------------------------
// ScanDialog
//----------------------------------------------------------------------------

void ScanDialog(HWND hwnd)
{
	if ( !ScanSel() )
	{
		return;
	}
	
	g_hdlg = CreateChosenDialog(g_hInstance,
								ScanDialogChoices,
								g_hwnd,
								ScanDlgProc,
								(LPARAM)0);

	WaitUntilDialogDoneSkip(FALSE);
}



