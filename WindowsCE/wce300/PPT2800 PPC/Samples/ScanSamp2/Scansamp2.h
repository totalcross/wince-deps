
//--------------------------------------------------------------------
// FILENAME:			ScanSmp2.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains variables and prototypes global to 
//						the application
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANSAMP2_H_

#define SCANSAMP2_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("ScanSmp2")
#define APP_TITLE TEXT("Scan Sample 2")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#define ID_LISTVIEW     1000

#define SMALL_BITMAP_WIDTH    16
#define SMALL_BITMAP_HEIGHT   16
#define LARGE_BITMAP_WIDTH 32
#define LARGE_BITMAP_HEIGHT 32

enum tagUSERMSGS
{
	UM_SCAN = UM_USER,
	UM_EVENT,
	UM_MEMORY,
	UM_STARTSCANNING,
	UM_STOPSCANNING,
};


//----------------------------------------------------------------------------
// Typedefs
//----------------------------------------------------------------------------

typedef struct tagFILENFO
{
    TCHAR szFile[MAX_PATH];
    TCHAR szDesc[MAX_PATH];
    TCHAR szCommand[2*MAX_PATH];
  int nHotKey;
  int nHotKeyModifier;
} FILEINFO;


typedef enum tagLISTMODES
{
  MODE_DETAILS,
  MODE_LIST,
  MODE_SMALL_ICONS,
  MODE_LARGE_ICONS,
} LISTMODES;


typedef struct tagPANELINFO
{
    int Mode;
  BOOL bTitleBar;
    TCHAR szTitleBar[MAX_PATH];
  BOOL bExit;
  TCHAR szExit[MAX_PATH];
  BOOL bQuitAfterLaunch;
  BOOL bHeadings;
  TCHAR szDescription[MAX_PATH];
  int nDescriptionWidth;
  TCHAR szName[MAX_PATH];
  int nNameWidth;
  TCHAR szCommand[MAX_PATH];
  int nCommandWidth;
} PANELINFO;


//----------------------------------------------------------------------------
// Global Variables

int gMaxListItems;

int g_Columns;

FILEINFO rgFILEINFO[512];

PANELINFO rgPANELINFO;

TCHAR szRegBasePath[MAX_PATH];

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANSAMP2_H_ */

