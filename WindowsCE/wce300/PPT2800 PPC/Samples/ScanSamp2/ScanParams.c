
//--------------------------------------------------------------------
// FILENAME:			ScanParams.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains window procedures for the scanning dialogs
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

//#define _STDDEBUG
#include "..\common\StdDebug.h"

#include "..\common\StdStrng.h"
#include "..\common\StdGlobs.h"
#include "..\common\StdAbout.h"
#include "..\common\Scan.h"

#include "ScanParams.h"

#include "ScanSamp2.h"


#define _DO_SCANNING


#define SCAN_TIMEOUT 0

#define UPCEAN_GENERAL -1

DIALOG_CHOICE ScanPrmDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_SCANPRM_PC_STD,		NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_SCANPRM_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_SCANPRM_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SCANPRM_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_SCANPRM_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_SCANPRM_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_SCANPRM_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_SCANPRM_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_SCANPRM_7500_TSK,	NULL,			UI_STYLE_DEFAULT }
};


DIALOG_CHOICE ScanCfgDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_SCANCFG_PC_STD,		NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_SCANCFG_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_SCANCFG_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SCANCFG_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_SCANCFG_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_SCANCFG_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_SCANCFG_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_SCANCFG_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_SCANCFG_7500_TSK,	NULL,			UI_STYLE_DEFAULT }
};


DIALOG_CHOICE DecParamDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_DECPARAM_PC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_DECPARAM_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_DECPARAM_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_DECPARAM_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_DECPARAM_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_DECPARAM_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_DECPARAM_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_DECPARAM_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_DECPARAM_7500_TSK,	NULL,			UI_STYLE_DEFAULT }
};


DIALOG_CHOICE ScanLenDialogChoices[] =
{
#ifdef _ST_HPC
	{ ST_HPC_STD,	IDD_SCANLEN_HPC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_SCANLEN_PC_STD,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_SCANLEN_PPC_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_SCANLEN_PPC_SIP,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_SCANLEN_7200_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_SCANLEN_7200_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_SCANLEN_7500_STD,	NULL,			UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_SCANLEN_7500_TSK,	NULL,			UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_SCANLEN_7500_TSK,	NULL,			UI_STYLE_DEFAULT }
};

// Hold area for saving previous dialog box handles
static HWND g_hCfgPrevDlg;
static HWND g_hPrmPrevDlg;
static HWND g_hLenPrevDlg;

// Image list and icon handles for list view

static HIMAGELIST g_hSmall = NULL;
static HIMAGELIST g_hLarge = NULL;

static HICON g_hSmallDisabledIcon = NULL;
static HICON g_hLargeDisabledIcon = NULL;

static HICON g_hSmallEnabledIcon = NULL;
static HICON g_hLargeEnabledIcon = NULL;
 
static BOOL l_bOKed = FALSE;

// Handles for scroll bars and edit boxes

static HWND l_hMinScrollbar;
static HWND l_hMaxScrollbar;
static HWND l_hMinEdit;
static HWND l_hMaxEdit;


static FEEDBACK_INFO FeedbackInfo;

static int nBeeperFrequencyMinimum;
static int nBeeperFrequencyMaximum;
static int nBeeperFrequencyStep;
static int nBeeperDurationMinimum;
static int nBeeperDurationMaximum;
static int nBeeperDurationStep;
static int nLedDurationMinimum;
static int nLedDurationMaximum;
static int nLedDurationStep;


// Macro defining the state bits for "selected" items

#define UI_SELECTED ( LVNI_SELECTED | LVNI_FOCUSED )


// Get the currently selected item index of a list view

int ListView_GetSelectedItem(HWND hWndList)
{
	int iItem;
	int nState;
	
	iItem = ListView_GetNextItem(hWndList,-1,UI_SELECTED);

	nState = ListView_GetItemState(hWndList,iItem,UI_SELECTED);

	return(iItem);
}


// Get the currently selected item parameter of a list view

LPARAM ListView_GetSelectedParam(HWND hWndList)
{
	int iItem;
	LV_ITEM lvI;

	iItem = ListView_GetSelectedItem(hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	return(lvI.lParam);
}


// Get the currently selected item text of a list view

void ListView_GetSelectedText(HWND hWndList,
							  LPTSTR lpszText,
							  DWORD dwSize)
{
	int iItem;

	iItem = ListView_GetSelectedItem(hWndList);

	ListView_GetItemText(hWndList,iItem,0,lpszText,dwSize);
}


// Select an item in a list view

void ListView_SelectItem(HWND hWndList,
						 int iItem)
{
	SetFocus(hWndList);

	ListView_SetItemState(hWndList,
						  iItem,
						  LVIS_SELECTED|LVIS_FOCUSED,
						  LVIS_SELECTED|LVIS_FOCUSED);
}


// Toggle the disable/enable image for the selected decoder

void ToggleSelectedDecoder(HWND hWndList)
{
	int iItem;
	LV_ITEM lvI;

	iItem = ListView_GetSelectedItem(hWndList);

	lvI.mask = LVIF_IMAGE | LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	if ( lvI.lParam != UPCEAN_GENERAL )
	{
		lvI.iImage = ! lvI.iImage;
		ListView_SetItem(hWndList,&lvI);
		ListView_Update(hWndList,iItem);
	}

	ListView_SelectItem(hWndList,iItem);
}


// Initialize the decoder list view

void InitializeDecoderListView(HWND hWndList)
{
	int iResult;
	HIMAGELIST hResult;
	LV_COLUMN lvC;
	RECT rect;
	int nWidth;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;
	lvC.fmt = LVCFMT_LEFT;

	GetWindowRect(hWndList,&rect);
	nWidth = rect.right - rect.left + 1;
	
	lvC.iSubItem = 0;
	lvC.cx = nWidth - 24;
	iResult = ListView_InsertColumn(hWndList,0,&lvC);

	if ( g_hLarge == NULL )
	{
		g_hLarge = ImageList_Create(LARGE_BITMAP_WIDTH,
									LARGE_BITMAP_HEIGHT,
									ILC_COLOR,
									2,
									0);

		if ( g_hLargeDisabledIcon == NULL )
		{
			g_hLargeDisabledIcon = LoadImage(g_hInstance,
										MAKEINTRESOURCE(IDI_DISABLEDICON),
										IMAGE_ICON,
										LARGE_BITMAP_WIDTH,
										LARGE_BITMAP_HEIGHT,
										LR_DEFAULTCOLOR);
		}

		iResult = ImageList_AddIcon(g_hLarge,g_hLargeDisabledIcon);

		if ( g_hLargeEnabledIcon == NULL )
		{
			g_hLargeEnabledIcon = LoadImage(g_hInstance,
									   MAKEINTRESOURCE(IDI_ENABLEDICON),
									   IMAGE_ICON,
									   LARGE_BITMAP_WIDTH,
									   LARGE_BITMAP_HEIGHT,
									   LR_DEFAULTCOLOR);
		}
	
		iResult = ImageList_AddIcon(g_hLarge,g_hLargeEnabledIcon);
	}

	if ( g_hSmall == NULL )
	{
		g_hSmall = ImageList_Create(SMALL_BITMAP_WIDTH,
									SMALL_BITMAP_HEIGHT,
									ILC_COLOR,
									2,
									0);

		if ( g_hSmallDisabledIcon == NULL )
		{
			g_hSmallDisabledIcon = LoadImage(g_hInstance,
											 MAKEINTRESOURCE(IDI_DISABLEDICON),
											 IMAGE_ICON,
											 SMALL_BITMAP_WIDTH,
											 SMALL_BITMAP_HEIGHT,
											 LR_DEFAULTCOLOR);
		}

		if ( g_hSmallEnabledIcon == NULL )
		{
			g_hSmallEnabledIcon = LoadImage(g_hInstance,
											MAKEINTRESOURCE(IDI_ENABLEDICON),
											IMAGE_ICON,
											SMALL_BITMAP_WIDTH,
											SMALL_BITMAP_HEIGHT,
											LR_DEFAULTCOLOR);
		}
	
		iResult = ImageList_AddIcon(g_hSmall,g_hSmallDisabledIcon);

		iResult = ImageList_AddIcon(g_hSmall,g_hSmallEnabledIcon);
	}

	switch(GetUIStyle())
	{
		case UI_STYLE_PEN:
		case UI_STYLE_KEYPAD:

			hResult = ListView_SetImageList(hWndList,g_hSmall,LVSIL_SMALL);

			break;

		case UI_STYLE_PC:
		case UI_STYLE_FINGER:

			hResult = ListView_SetImageList(hWndList,g_hLarge,LVSIL_SMALL);

			break;
	}
}


// Terminate the decoder list view

void TerminateDecoderListView(HWND hWndList)
{
	BOOL bResult;
	HIMAGELIST hResult;

	bResult = ListView_DeleteAllItems(hWndList);

	hResult = ListView_SetImageList(hWndList,NULL,LVSIL_SMALL);

	ImageList_Destroy(g_hSmall);
	g_hSmall = NULL;

	hResult = ListView_SetImageList(hWndList,NULL,LVSIL_NORMAL);

	ImageList_Destroy(g_hLarge);
	g_hLarge = NULL;
}


// Add a decoder to the decoder list view

void AddDecoderToListView(HWND hWndList,
						  LPTSTR lpsz,
						  BOOL f,
						  int i,
						  LPARAM lParam)
{
	if ( f )
	{
		LV_ITEM lvI;
		int iItem;

		lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lvI.iItem = 0;
		lvI.iSubItem = 0;
		lvI.cchTextMax = MAX_PATH;
		lvI.pszText = lpsz;
		lvI.iImage = i;
		lvI.lParam = lParam;
		
		iItem = ListView_InsertItem(hWndList,&lvI);

		iItem = iItem;
	}
}


// Enable a decoder in the decoder list view

void EnableDecoderInListView(HWND hWndList,
							 LPTSTR lpsz,
							 BOOL f)
{
	LV_FINDINFO lvF;
	int iItem;

	lvF.flags = LVFI_STRING;
	lvF.psz = lpsz;
	
	iItem = ListView_FindItem(hWndList,-1,&lvF);
	if ( iItem != -1 )
	{
		LV_ITEM lvI;
		lvI.mask = LVIF_IMAGE;
		lvI.iItem = iItem;
		lvI.iSubItem = 0;
		lvI.iImage = f;
		ListView_SetItem(hWndList,&lvI);
		ListView_Update(hWndList,iItem);
	}
}


// Return whether a decoder in the decoder list view is enabled

BOOL IsDecoderEnabledInListView(HWND hWndList,
								LPTSTR lpsz)
{
	LV_FINDINFO lvF;
	int iItem;

	lvF.flags = LVFI_STRING;
	lvF.psz = lpsz;
	
	iItem = ListView_FindItem(hWndList,-1,&lvF);
	if ( iItem == -1 )
	{
		return(FALSE);
	}
	else
	{
		LV_ITEM lvI;
		lvI.mask = LVIF_IMAGE;
		lvI.iItem = iItem;
		lvI.iSubItem = 0;
		ListView_GetItem(hWndList,&lvI);
		return(lvI.iImage);
	}
}



// Macro for enabling and disabling a button based on whether a decoder is supported
#define SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,decoder) \
	AddDecoderToListView(hWndList,TEXT(#decoder),ScanIsDecoderSupported(DECODER_##decoder),0,LABELTYPE_##decoder)


// Macro for checking and unchecking a button based on whether a decoder is enabled
#define ENABLED_DECODER_TO_ENABLED_UI(hWndList,decoder) \
	EnableDecoderInListView(hWndList,TEXT(#decoder),ScanIsDecoderEnabled(DECODER_##decoder))


// Macro for enabling or disabling a decoder based on the checked state of a button
#define ENABLED_UI_TO_ENABLED_DECODER(hWndList,decoder) \
{ \
	if ( IsDecoderEnabledInListView(hWndList,TEXT(#decoder)) ) \
		ScanEnableDecoder(DECODER_##decoder); \
	else \
		ScanDisableDecoder(DECODER_##decoder); \
}


typedef struct tagPARAMSELECTOR
{
	LPTSTR lpszText;
	LPTSTR FAR * lplpszPARAM_VALUES_LIST;
	DWORD dwCount;
	DWORD dwIndex;

} PARAMSELECTOR;
typedef PARAMSELECTOR FAR * LPPARAMSELECTOR;


typedef struct tagPARAMSELECTORLIST
{
	LPPARAMSELECTOR lpItemSelector;
	LPDECODER_PARAMS lpDecoderParams;
	DWORD dwSize;
	DWORD dwMinLength;
	DWORD dwMaxLength;
	LPTSTR lpszText;

} PARAMSELECTORLIST;
typedef PARAMSELECTORLIST FAR *LPPARAMSELECTORLIST;


typedef struct tagUPCEANSELECTORLIST
{
	LPPARAMSELECTOR lpItemSelector;
	LPUPC_EAN_PARAMS lpUpcEanParams;
	DWORD dwSize;
	LPTSTR lpszText;

} UPCEANSELECTORLIST;
typedef UPCEANSELECTORLIST FAR *LPUPCEANSELECTORLIST;


#define PARAM_COUNT(decoder) ( sizeof(Selector##decoder) / sizeof(PARAMSELECTOR) - 1 )
#define DPSIZE(n) (/*sizeof(STRUCT_INFO)+sizeof(DECODER)+*/n*sizeof(DWORD))
#define PARAM_SIZE(decoder) DPSIZE(PARAM_COUNT(decoder))

#define PARAM_VALUES_COUNT(a) ( sizeof(a) / sizeof(LPTSTR) - 1 )

#define PARAM_VALUES_LIST(a) a, PARAM_VALUES_COUNT(a)

#define ITEM_SELECTOR(a,b,c) { TEXT(a), TEXT(b), PARAM_VALUES_LIST(c) }
#define LAST_ITEM_SELECTOR { NULL, NULL, 0, 0 }

#define ITEM_LIST_ENTRY(decoder) { LABELTYPE_##decoder, &SelectorList##decoder }
#define LAST_ITEM_LIST_ENTRY { 0, NULL }

#define DEFINE_SELECTOR_LIST(decoder,min,max) \
	PARAMSELECTORLIST SelectorList##decoder = \
		{ Selector##decoder, &DecoderSpecific##decoder, PARAM_SIZE(decoder), min,max }


LPTSTR lpszBoolean[] =
{
	TEXT("False"),
	TEXT("True"),
	NULL
};

LPTSTR lpszPreamble[] =
{
	TEXT("None"),
	TEXT("System"),
	TEXT("Country"),
	NULL
};

LPTSTR lpszMSICheckDigitCount[] =
{
	TEXT("1"),
	TEXT("2"),
	NULL
};

LPTSTR lpszMSICheckDigitScheme[] =
{
	TEXT("Mod11"),
	TEXT("Mod10"),
	NULL
};

LPTSTR lpszI2OF5CheckDigitScheme[] =
{
	TEXT("None"),
	TEXT("USS"),
	TEXT("OPCC"),
	NULL
};

LPTSTR lpszCODE11CheckDigitCount[] =
{
	TEXT("None"),
	TEXT("1"),
	TEXT("2"),
	NULL
};

LPTSTR lpszUPCEANSecurityLevel[] =
{
	TEXT("None"),
	TEXT("Ambiguous"),
	TEXT("All"),
	NULL
};

LPTSTR lpszSupplementalMode[] =
{
	TEXT("None"),
	TEXT("Always"),
	TEXT("Auto"),
	NULL
};

LPTSTR lpszRetryCount[] =
{
	TEXT("2"),	// will have to add 2 to the value
	TEXT("3"),
	TEXT("4"),
	TEXT("5"),
	TEXT("6"),
	TEXT("7"),
	TEXT("8"),
	TEXT("9"),
	TEXT("10"),
	NULL
};



PARAMSELECTOR SelectorUPCE0[] =
{
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Preamble"), PARAM_VALUES_LIST(lpszPreamble) },
	{ TEXT("Cnvt to UPCA"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUPCE0 = { {sizeof(DECODER_PARAMS), 0}, DECODER_UPCE0};

DEFINE_SELECTOR_LIST(UPCE0,0,0);
				
PARAMSELECTOR SelectorUPCE1[] =
{
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Preamble"), PARAM_VALUES_LIST(lpszPreamble) },
	{ TEXT("Cnvt to UPCA"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUPCE1 = { {sizeof(DECODER_PARAMS), 0}, DECODER_UPCE1};

DEFINE_SELECTOR_LIST(UPCE1,0,0);
				

PARAMSELECTOR SelectorUPCA[] =
{
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Preamble"), PARAM_VALUES_LIST(lpszPreamble) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUPCA = { {sizeof(DECODER_PARAMS), 0}, DECODER_UPCA};

DEFINE_SELECTOR_LIST(UPCA,0,0);
				

PARAMSELECTOR SelectorMSI[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("# of Chk Dgts"), PARAM_VALUES_LIST(lpszMSICheckDigitCount) },
	{ TEXT("Chk Dgt Scheme"), PARAM_VALUES_LIST(lpszMSICheckDigitScheme) },
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificMSI = { {sizeof(DECODER_PARAMS), 0}, DECODER_MSI};

DEFINE_SELECTOR_LIST(MSI,0,55);
				

PARAMSELECTOR SelectorEAN8[] =
{
	{ TEXT("Cnvt to EAN13"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificEAN8 = { {sizeof(DECODER_PARAMS), 0}, DECODER_EAN8};

DEFINE_SELECTOR_LIST(EAN8,0,0);
				

PARAMSELECTOR SelectorEAN13[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificEAN13 = { {sizeof(DECODER_PARAMS), 0}, DECODER_EAN13};

DEFINE_SELECTOR_LIST(EAN13,0,0);
				

PARAMSELECTOR SelectorCODABAR[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("CLSI Edit"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("NOTIS Edit"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificCODABAR = { {sizeof(DECODER_PARAMS), 0}, DECODER_CODABAR};

DEFINE_SELECTOR_LIST(CODABAR,0,55);
				

PARAMSELECTOR SelectorCODE39[] =
{
	{ TEXT("Verify Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Concat"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Full ASCII"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Cnvt to CODE32"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("CODE32 Prefix"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificCODE39 = { {sizeof(DECODER_PARAMS), 0}, DECODER_CODE39};

DEFINE_SELECTOR_LIST(CODE39,0,55);
				

PARAMSELECTOR SelectorD2OF5[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificD2OF5 = { {sizeof(DECODER_PARAMS), 0}, DECODER_D2OF5};

DEFINE_SELECTOR_LIST(D2OF5,0,55);
				

PARAMSELECTOR SelectorI2OF5[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Verify Chk Dgt"), PARAM_VALUES_LIST(lpszI2OF5CheckDigitScheme) },
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Cnvt to EAN13"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificI2OF5 = { {sizeof(DECODER_PARAMS), 0}, DECODER_I2OF5};

DEFINE_SELECTOR_LIST(I2OF5,0,55);
				

PARAMSELECTOR SelectorCODE11[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("# of Chk Dgts"), PARAM_VALUES_LIST(lpszCODE11CheckDigitCount) },
	{ TEXT("Report Chk Dgt"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificCODE11 = { {sizeof(DECODER_PARAMS), 0}, DECODER_CODE11};

DEFINE_SELECTOR_LIST(CODE11,0,55);
				

PARAMSELECTOR SelectorCODE93[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificCODE93 = { {sizeof(DECODER_PARAMS), 0}, DECODER_CODE93};

DEFINE_SELECTOR_LIST(CODE93,0,55);
				

PARAMSELECTOR SelectorCODE128[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("EAN128"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("ISBT128"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Other 128"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificCODE128 = { {sizeof(DECODER_PARAMS), 0}, DECODER_CODE128};

DEFINE_SELECTOR_LIST(CODE128,0,55);
				

PARAMSELECTOR SelectorTRIOPTIC39[] =
{
	{ TEXT("Redundancy"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificTRIOPTIC39 = { {sizeof(DECODER_PARAMS), 0}, DECODER_TRIOPTIC39};

DEFINE_SELECTOR_LIST(TRIOPTIC39,0,0);
				

PARAMSELECTOR SelectorPDF417[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificPDF417 = { {sizeof(DECODER_PARAMS), 0}, DECODER_PDF417};

DEFINE_SELECTOR_LIST(PDF417,0,2710);
				

PARAMSELECTOR SelectorMICROPDF[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificMICROPDF = { {sizeof(DECODER_PARAMS), 0}, DECODER_MICROPDF};

DEFINE_SELECTOR_LIST(MICROPDF,0,366);
				

PARAMSELECTOR SelectorMAXICODE[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificMAXICODE = { {sizeof(DECODER_PARAMS), 0}, DECODER_MAXICODE};

DEFINE_SELECTOR_LIST(MAXICODE,0,138);
				

PARAMSELECTOR SelectorDATAMATRIX[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificDATAMATRIX = { {sizeof(DECODER_PARAMS), 0}, DECODER_DATAMATRIX};

DEFINE_SELECTOR_LIST(DATAMATRIX,0,3116);
				

PARAMSELECTOR SelectorQRCODE[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificQRCODE = { {sizeof(DECODER_PARAMS), 0}, DECODER_QRCODE};

DEFINE_SELECTOR_LIST(QRCODE,0,7089);
				

PARAMSELECTOR SelectorUSPOSTNET[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUSPOSTNET = { {sizeof(DECODER_PARAMS), 0}, DECODER_USPOSTNET};

DEFINE_SELECTOR_LIST(USPOSTNET,0,0);
				

PARAMSELECTOR SelectorUSPLANET[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUSPLANET = { {sizeof(DECODER_PARAMS), 0}, DECODER_USPLANET};

DEFINE_SELECTOR_LIST(USPLANET,0,0);
				

PARAMSELECTOR SelectorUKPOSTAL[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificUKPOSTAL = { {sizeof(DECODER_PARAMS), 0}, DECODER_UKPOSTAL};

DEFINE_SELECTOR_LIST(UKPOSTAL,0,0);
				

PARAMSELECTOR SelectorJAPPOSTAL[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificJAPPOSTAL = { {sizeof(DECODER_PARAMS), 0}, DECODER_JAPPOSTAL};

DEFINE_SELECTOR_LIST(JAPPOSTAL,0,0);
				

PARAMSELECTOR SelectorAUSPOSTAL[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificAUSPOSTAL = { {sizeof(DECODER_PARAMS), 0}, DECODER_AUSPOSTAL};

DEFINE_SELECTOR_LIST(AUSPOSTAL,0,0);
				

PARAMSELECTOR SelectorDUTCHPOSTAL[] =
{
	LAST_ITEM_SELECTOR
};

DECODER_PARAMS DecoderSpecificDUTCHPOSTAL = { {sizeof(DECODER_PARAMS), 0}, DECODER_DUTCHPOSTAL};

DEFINE_SELECTOR_LIST(DUTCHPOSTAL,0,0);
				

// UPC_EAN don't follow the same pattern
PARAMSELECTOR SelectorUPCEAN[] =
{
	{ TEXT("Security Level"), PARAM_VALUES_LIST(lpszUPCEANSecurityLevel) },
	{ TEXT("Supplemental 2"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Supplemental 5"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Supplemental Mode"), PARAM_VALUES_LIST(lpszSupplementalMode) },
	{ TEXT("Retry Count"), PARAM_VALUES_LIST(lpszRetryCount) },
	{ TEXT("Random Weight Chk"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Linear Decode"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Bookland"), PARAM_VALUES_LIST(lpszBoolean) },
	{ TEXT("Coupon"), PARAM_VALUES_LIST(lpszBoolean) },
	LAST_ITEM_SELECTOR
};

UPC_EAN_PARAMS Upc_ean_params = { {sizeof(UPC_EAN_PARAMS)} };

UPCEANSELECTORLIST SelectorListUPCEAN = { SelectorUPCEAN, &Upc_ean_params, PARAM_SIZE(UPCEAN) };
				

typedef struct tagITEMLISTENTRY
{
	LPARAM lParam;
	LPPARAMSELECTORLIST lpParamSelectorList;
} ITEMLISTENTRY;


ITEMLISTENTRY ItemList[] =
{
	ITEM_LIST_ENTRY(UPCE0),
	ITEM_LIST_ENTRY(UPCE1),
	ITEM_LIST_ENTRY(UPCA),
	ITEM_LIST_ENTRY(MSI),
	ITEM_LIST_ENTRY(EAN8),
	ITEM_LIST_ENTRY(EAN13),
	ITEM_LIST_ENTRY(CODABAR),
	ITEM_LIST_ENTRY(CODE39),
	ITEM_LIST_ENTRY(D2OF5),
	ITEM_LIST_ENTRY(I2OF5),
	ITEM_LIST_ENTRY(CODE11),
	ITEM_LIST_ENTRY(CODE93),
	ITEM_LIST_ENTRY(CODE128),
	ITEM_LIST_ENTRY(TRIOPTIC39),
	ITEM_LIST_ENTRY(PDF417),
	ITEM_LIST_ENTRY(MICROPDF),
	ITEM_LIST_ENTRY(MAXICODE),
	ITEM_LIST_ENTRY(DATAMATRIX),
	ITEM_LIST_ENTRY(QRCODE),
	ITEM_LIST_ENTRY(USPOSTNET),
	ITEM_LIST_ENTRY(USPLANET),
	ITEM_LIST_ENTRY(UKPOSTAL),
	ITEM_LIST_ENTRY(JAPPOSTAL),
	ITEM_LIST_ENTRY(AUSPOSTAL),
	ITEM_LIST_ENTRY(DUTCHPOSTAL),
	LAST_ITEM_LIST_ENTRY
};



LPPARAMSELECTORLIST GetItemSelectorList(LPARAM lParam,
									   LPTSTR lpszTitle)
{
	int i;

	for(i=0;ItemList[i].lpParamSelectorList;i++)
	{
		if ( ItemList[i].lParam == lParam )
		{
			if ( ItemList[i].lpParamSelectorList->lpszText == NULL )
			{
				ItemList[i].lpParamSelectorList->lpszText = (LPTSTR) LocalAlloc(LPTR,MAX_PATH*sizeof(TCHAR));
			}

			TEXTCPY(ItemList[i].lpParamSelectorList->lpszText,lpszTitle);

			return(ItemList[i].lpParamSelectorList);
		}
	}

	return(NULL);
}


VOID FreeItemSelector(LPPARAMSELECTORLIST lpisl)
{
	int i;

	for(i=0;ItemList[i].lpParamSelectorList;i++)
	{
		if ( ItemList[i].lpParamSelectorList->lpszText != NULL )
		{
			LocalFree(ItemList[i].lpParamSelectorList->lpszText);
			ItemList[i].lpParamSelectorList->lpszText = NULL;
		}
	}
}


VOID InitializeParamsListView(HWND hWndList,
							  LPPARAMSELECTORLIST lpisl)
{
	int i;
	LV_COLUMN lvC;
	RECT rect;
	int nWidth;
	int nMaxWidth = -1;
	int nMaxWidth1 = -1;
	int nMaxWidth2 = -1;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;
	lvC.fmt = LVCFMT_LEFT;

	GetWindowRect(hWndList,&rect);
	nWidth = (rect.right - rect.left + 1);

	SetNewDialogFont(hWndList,100);
            
	if ( lpisl != NULL )
	{
		int i,j;
		HDC hdc = GetDC(hWndList);
		
		for(i=0;lpisl->lpItemSelector[i].lpszText;i++)
		{
			SIZE size;
			LPTSTR lpszText;

			lpszText = lpisl->lpItemSelector[i].lpszText;

			GetTextExtentPoint32(hdc,
								 lpszText,
								 TEXTLEN(lpszText),
								 &size);
			
			if ( size.cx > nMaxWidth1 )
			{
				nMaxWidth1 = size.cx;
			}

			for(j=0;lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[j];j++)
			{
				lpszText = lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[j];

				GetTextExtentPoint32(hdc,
									 lpszText,
									 TEXTLEN(lpszText),
									 &size);
			
				if ( size.cx > nMaxWidth2 )
				{
					nMaxWidth2 = size.cx;
				}
			}
		}

		if ( ( nMaxWidth1 > -1 ) &&
			 ( nMaxWidth2 > -1 ) )
		{
			nMaxWidth = nMaxWidth1 * nWidth / ( nMaxWidth1 + nMaxWidth2 ); 
		}

		ReleaseDC(hWndList,hdc);
	}

	if ( ( nMaxWidth != -1 ) &&
		 ( nMaxWidth < nWidth ) )
	{
		lvC.iSubItem = 0;
		lvC.cx = nMaxWidth * 85 / 100;
		ListView_InsertColumn(hWndList,0,&lvC);

		lvC.iSubItem = 1;
		lvC.cx = (nWidth - nMaxWidth - 1) * 115 / 100;
		ListView_InsertColumn(hWndList,1,&lvC);
	}
	else
	{
		lvC.iSubItem = 0;
		lvC.cx = (int)( ((double)nWidth) / 3.0 * 2.0 );
		ListView_InsertColumn(hWndList,0,&lvC);

		lvC.iSubItem = 1;
		lvC.cx = (int)( ((double)nWidth) / 3.0 );
		ListView_InsertColumn(hWndList,1,&lvC);
	}

	if ( lpisl != NULL )
	{
		for(i=0;lpisl->lpItemSelector[i].lpszText;i++)
		{
			LV_ITEM lvI;

			lpisl->lpItemSelector[i].dwIndex = lpisl->lpDecoderParams->dec_specific.dwUntyped[i];
			
			lvI.mask = LVIF_TEXT | LVIF_PARAM;
			lvI.iItem = 0;
			lvI.iSubItem = 0;
			lvI.lParam = (LPARAM)&(lpisl->lpItemSelector[i]);
			lvI.pszText = lpisl->lpItemSelector[i].lpszText;
			lvI.cchTextMax = TEXTLEN(lvI.pszText);
			ListView_InsertItem(hWndList,&lvI);

			lvI.mask = LVIF_TEXT;
			lvI.iItem = 0;
			lvI.iSubItem = 1;
			lvI.pszText = lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[lpisl->lpItemSelector[i].dwIndex];
			lvI.cchTextMax = TEXTLEN(lvI.pszText);
			ListView_SetItem(hWndList,&lvI);
		}
	}
}


VOID InitializeUpcEanListView(HWND hWndList,
							  LPUPCEANSELECTORLIST lpisl)
{
	int i;
	LV_COLUMN lvC;
	RECT rect;
	int nWidth;
	int nMaxWidth = -1;
	int nMaxWidth1 = -1;
	int nMaxWidth2 = -1;

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;
	lvC.fmt = LVCFMT_LEFT;

	GetWindowRect(hWndList,&rect);
	nWidth = (rect.right - rect.left + 1);

	SetNewDialogFont(hWndList,100);
            
	if ( lpisl != NULL )
	{
		int i,j;
		HDC hdc = GetDC(hWndList);
		
		for(i=0;lpisl->lpItemSelector[i].lpszText;i++)
		{
			SIZE size;
			LPTSTR lpszText;

			lpszText = lpisl->lpItemSelector[i].lpszText;

			GetTextExtentPoint32(hdc,
								 lpszText,
								 TEXTLEN(lpszText),
								 &size);
			
			if ( size.cx > nMaxWidth1 )
			{
				nMaxWidth1 = size.cx;
			}

			for(j=0;lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[j];j++)
			{
				lpszText = lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[j];

				GetTextExtentPoint32(hdc,
									 lpszText,
									 TEXTLEN(lpszText),
									 &size);
			
				if ( size.cx > nMaxWidth2 )
				{
					nMaxWidth2 = size.cx;
				}
			}
		}

		if ( ( nMaxWidth1 > -1 ) &&
			 ( nMaxWidth2 > -1 ) )
		{
			nMaxWidth = nMaxWidth1 * nWidth / ( nMaxWidth1 + nMaxWidth2 ); 
		}

		ReleaseDC(hWndList,hdc);
	}

	if ( ( nMaxWidth != -1 ) &&
		 ( nMaxWidth < nWidth ) )
	{
		lvC.iSubItem = 0;
		lvC.cx = nMaxWidth * 85 / 100;
		ListView_InsertColumn(hWndList,0,&lvC);

		lvC.iSubItem = 1;
		lvC.cx = (nWidth - nMaxWidth - 1) * 115 / 100;
		ListView_InsertColumn(hWndList,1,&lvC);
	}
	else
	{
		lvC.iSubItem = 0;
		lvC.cx = (int)( ((double)nWidth) / 3.0 * 2.0 );
		ListView_InsertColumn(hWndList,0,&lvC);

		lvC.iSubItem = 1;
		lvC.cx = (int)( ((double)nWidth) / 3.0 );
		ListView_InsertColumn(hWndList,1,&lvC);
	}

	if ( lpisl != NULL )
	{
		for(i=0;lpisl->lpItemSelector[i].lpszText;i++)
		{
			LV_ITEM lvI;

			LPDWORD lpdwUntyped = (LPDWORD)(((LPBYTE)(lpisl->lpUpcEanParams))+sizeof(STRUCT_INFO));

			lpisl->lpItemSelector[i].dwIndex = lpdwUntyped[i];
			
			lvI.mask = LVIF_TEXT | LVIF_PARAM;
			lvI.iItem = 0;
			lvI.iSubItem = 0;
			lvI.lParam = (LPARAM)&(lpisl->lpItemSelector[i]);
			lvI.pszText = lpisl->lpItemSelector[i].lpszText;
			lvI.cchTextMax = TEXTLEN(lvI.pszText);
			ListView_InsertItem(hWndList,&lvI);

			lvI.mask = LVIF_TEXT;
			lvI.iItem = 0;
			lvI.iSubItem = 1;
			lvI.pszText = lpisl->lpItemSelector[i].lplpszPARAM_VALUES_LIST[lpisl->lpItemSelector[i].dwIndex];
			lvI.cchTextMax = TEXTLEN(lvI.pszText);
			ListView_SetItem(hWndList,&lvI);
		}
	}
}


void ToggleCurrentParameter(HWND hWndList,
							int nInc)
{
	LV_ITEM lvI;
	LPPARAMSELECTOR lpItemSelector;

	int iItem = ListView_GetSelectedItem(hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	lpItemSelector = (LPPARAMSELECTOR)lvI.lParam;

	if ( ( lpItemSelector->dwIndex == 0 ) &&
		 ( nInc < 0 ) )
	{
		lpItemSelector->dwIndex = lpItemSelector->dwCount - 1;
	}
	else if ( ( lpItemSelector->dwIndex >= lpItemSelector->dwCount-1 ) &&
		      ( nInc > 0 ) )
	{
		lpItemSelector->dwIndex = 0;
	}
	else
	{
		lpItemSelector->dwIndex += nInc;
	}

	lvI.mask = LVIF_TEXT;
	lvI.iSubItem = 1;
	lvI.pszText = lpItemSelector->lplpszPARAM_VALUES_LIST[lpItemSelector->dwIndex];
	lvI.cchTextMax = TEXTLEN(lvI.pszText);
	ListView_SetItem(hWndList,&lvI);

	ListView_SelectItem(hWndList,iItem);
}


VOID TerminateParamsListView(LPPARAMSELECTORLIST lpisl)
{
	int i;
	DWORD	dwResult = 0;

	if ( lpisl != NULL )
	{
		for(i=0;lpisl->lpItemSelector[i].lpszText;i++)
		{
			lpisl->lpDecoderParams->dec_specific.dwUntyped[i] = lpisl->lpItemSelector[i].dwIndex;
		}
	}

	dwResult = ScanSetDecoderParams(
		lpisl->lpDecoderParams->cDecoder,
		&(lpisl->lpDecoderParams->dec_specific),
		lpisl->dwSize);

	ScanError(TEXT("ScanSetDecoderParams Error"),
			  dwResult,
			  E_SCN_SUCCESS);
}


void InitMinMaxBars(HWND hwnd,
					DWORD dwMin,
					DWORD dwMax,
					HWND hMinText,
					HWND hMinEdit,
					HWND hMaxText,
					HWND hMaxEdit,
					int nMin,
					int nMax,
					int nPage)
{
	HFONT hNewFont;
	
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL,0,55+10-1,10,0 };

	si.nMin = nMin;
	si.nMax = nMax;
	si.nPage = nPage;

	hNewFont = SetNewDialogFont(hwnd,100);

	if ( hMinText != NULL )
	{
		SetWindowFont(hMinText,hNewFont,FALSE);
	}
	
	l_hMinEdit = hMinEdit;
	if ( l_hMinEdit != NULL )
	{
		SetWindowFont(l_hMinEdit,hNewFont,FALSE);
	}

	if ( hMaxText != NULL )
	{
		SetWindowFont(hMaxText,hNewFont,FALSE);
	}
	
	l_hMaxEdit = hMaxEdit;
	if ( l_hMaxEdit != NULL )
	{
		SetWindowFont(l_hMaxEdit,hNewFont,FALSE);
	}

	
	l_hMinScrollbar = GetDlgItem(hwnd,IDC_SCROLLBAR1);
	if ( l_hMinScrollbar != NULL )
	{
		si.nPos = dwMin;
		SetScrollInfo(l_hMinScrollbar,SB_CTL,&si,TRUE);
	}

	l_hMaxScrollbar = GetDlgItem(hwnd,IDC_SCROLLBAR2);
	if ( l_hMaxScrollbar != NULL )
	{
		si.nPos = dwMax;
		SetScrollInfo(l_hMaxScrollbar,SB_CTL,&si,TRUE);
	}
}


void HandleMinMaxBars(WPARAM wParam,
					  LPARAM lParam)
{
	HWND hSB = (HWND)lParam;
	HWND hED;
	int nNewPos = -1;
	int nPage;
	TCHAR szText[MAX_PATH];
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };

	if ( hSB == l_hMinScrollbar )
	{
		hED = l_hMinEdit;
	}
	else if ( hSB == l_hMaxScrollbar )
	{
		hED = l_hMaxEdit;
	}
	else
	{
		return;
	}

	switch(LOWORD(wParam))
	{
		case SB_LINELEFT:
		case SB_LINERIGHT:
		case SB_PAGELEFT:
		case SB_PAGERIGHT:
			
			GetScrollInfo(hSB,SB_CTL,&si);
			
			if (0 == si.nMax)
				break;
			
			switch(LOWORD(wParam))
			{
				case SB_LINELEFT:
				case SB_LINERIGHT:
					nPage = 1;
					break;
				case SB_PAGELEFT:
				case SB_PAGERIGHT:
					nPage = si.nPage;
					break;
			}

			switch(LOWORD(wParam))
			{
				case SB_LINELEFT:
				case SB_PAGELEFT:
					si.nPos -= nPage;
					if ( si.nPos < si.nMin )
					{
						si.nPos = si.nMin;
					}
					break;
				case SB_LINERIGHT:
				case SB_PAGERIGHT:
					si.nPos += nPage;
					if ( si.nPos > (si.nMax - (int)si.nPage + 1) )
					{
						si.nPos = (si.nMax - (int)si.nPage + 1);
					}
					break;
			}

			si.fMask = SIF_POS;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);
							
			nNewPos = si.nPos;
					
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:

			nNewPos = HIWORD(wParam);

			si.fMask = SIF_POS;
			si.nPos = nNewPos;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);

			break;
					
		case SB_ENDSCROLL:

			GetScrollInfo(hSB,SB_CTL,&si);
						
			si.fMask = SIF_POS;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);

			nNewPos = si.nPos;
	}

	if ( nNewPos != -1 )
	{
		TCHAR szCurText[MAX_PATH];

		Edit_GetText(hED,szCurText,TEXTSIZEOF(szCurText));

		TEXTSPRINTF(szText,TEXT("%d"),nNewPos);

		if ( ( hED != NULL ) && ( TEXTCMP(szText,szCurText) != 0 ) )
		{
			Edit_SetText(hED,szText);
			Edit_SetSel(hED,0,-1);
			SetFocus(hED);
		}
	}

}


//----------------------------------------------------------------------------
// ScanLenDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK ScanLenDlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	static LPPARAMSELECTORLIST lpParamSelectorList;
	static HWND hED1;
	static HWND hED2;
	static HWND hCtl = NULL;

	HWND hWndList = GetDlgItem(hwnd,IDC_PARAMETERS);

    switch(uMsg)
    {
		case WM_SETFOCUS:
			
			if ( hCtl != NULL )
			{
				SetFocus(hCtl);
			}
			
			break;

		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);

			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:
			{    
				TCHAR szText[MAX_PATH];
				int nLimitMin;
				int nLimitPage;
				int nLimitMax;
				
				MaxDialog(hwnd);

				lpParamSelectorList = (LPPARAMSELECTORLIST)lParam;
			
				TEXTSPRINTF(szText,
							TEXT("%s Lengths"),
							lpParamSelectorList->lpszText);
				
				SetWindowText(hwnd,szText);

				hED1 = GetDlgItem(hwnd,IDC_EDIT1);
				if ( hED1 != NULL )
				{
					TEXTSPRINTF(szText,TEXT("%d"),lpParamSelectorList->lpDecoderParams->dwMinLength);
					Edit_SetText(hED1,szText);
				}

				hED2 = GetDlgItem(hwnd,IDC_EDIT2);
				if ( hED2 != NULL )
				{
					TEXTSPRINTF(szText,TEXT("%d"),lpParamSelectorList->lpDecoderParams->dwMaxLength);
					Edit_SetText(hED2,szText);
				}

				nLimitMin = lpParamSelectorList->dwMinLength;
				nLimitMax = lpParamSelectorList->dwMaxLength;
				nLimitPage = ((nLimitMax-nLimitMin)/10+9)/10*10;
				nLimitMax = nLimitMax + nLimitPage - 1;

				InitMinMaxBars(hwnd,
							   lpParamSelectorList->lpDecoderParams->dwMinLength,
							   lpParamSelectorList->lpDecoderParams->dwMaxLength,
							   GetDlgItem(hwnd,IDC_TEXT1),
							   hED1,
							   GetDlgItem(hwnd,IDC_TEXT2),
							   hED2,
							   nLimitMin,
							   nLimitMax,
							   nLimitPage);

				hCtl = NULL;

				if ( hED1 != NULL )
				{
					SetFocus(hED1);
					Edit_SetSel(hED1,0,-1);
				}

			}
			
			// Return FALSE since focus was set manually
			return(FALSE);

        case WM_HSCROLL:
			HandleMinMaxBars(wParam,lParam);
			break;

		case WM_COMMAND:
            
			if ( HIWORD(wParam) == EN_SETFOCUS )
			{
				hCtl = (HWND)lParam;
				
				break;
			}

			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_MIN_DEC:
				case IDC_MIN_INC:
				case IDC_MAX_DEC:
				case IDC_MAX_INC:
					
					{
						HWND hSB = NULL;
						WPARAM wpCmd = 0;

						switch(LOWORD(wParam))
						{
							case IDC_MIN_DEC:
								wpCmd = SB_LINELEFT;
								hSB = l_hMinScrollbar;
								break;
							case IDC_MIN_INC:
								wpCmd = SB_LINERIGHT;
								hSB = l_hMinScrollbar;
								break;
							case IDC_MAX_DEC:
								wpCmd = SB_LINELEFT;
								hSB = l_hMaxScrollbar;
								break;
							case IDC_MAX_INC:
								wpCmd = SB_LINERIGHT;
								hSB = l_hMaxScrollbar;
								break;
						}

						if ( hSB != NULL )
						{
							HandleMinMaxBars(wpCmd,(LPARAM)hSB);
						}
					}

					break;

				case IDC_HELP_ABOUT:
					
					About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					break;

				case IDC_POPUP:
					{
						if ( hCtl == hED1 )
						{
							TCHAR szText[MAX_PATH];
							DWORD dwMinLength;
							
							Edit_GetText(GetDlgItem(hwnd,IDC_EDIT1),szText,MAX_PATH);
							dwMinLength = TEXTTOL(szText);

							if ( ( dwMinLength >= lpParamSelectorList->dwMinLength ) &&
								 ( dwMinLength <= lpParamSelectorList->dwMaxLength ) )
							{
								SetFocus(GetDlgItem(hwnd,IDC_EDIT2));
								Edit_SetSel(GetDlgItem(hwnd,IDC_EDIT2),0,-1);
							}
							else
							{
								MessageBeep(MB_ICONEXCLAMATION);
							}
						}
						else if ( hCtl = hED2 )
						{
							TCHAR szText[MAX_PATH];
							DWORD dwMaxLength;
							
							Edit_GetText(GetDlgItem(hwnd,IDC_EDIT2),szText,MAX_PATH);
							dwMaxLength = TEXTTOL(szText);

							if ( ( dwMaxLength >= lpParamSelectorList->dwMinLength ) &&
								 ( dwMaxLength <= lpParamSelectorList->dwMaxLength ) )
							{
								PostMessage(hwnd,WM_COMMAND,IDOK,0);
							}
							else
							{
								MessageBeep(MB_ICONEXCLAMATION);
							}
						}
					}
					break;

				case IDOK:

					{
						TCHAR szText[MAX_PATH];
						DWORD dwMinLength;
						DWORD dwMaxLength;

						Edit_GetText(GetDlgItem(hwnd,IDC_EDIT1),szText,MAX_PATH);
						dwMinLength = TEXTTOL(szText);

						Edit_GetText(GetDlgItem(hwnd,IDC_EDIT2),szText,MAX_PATH);
						dwMaxLength = TEXTTOL(szText);

						if ( ( dwMinLength >= lpParamSelectorList->dwMinLength ) &&
							 ( dwMinLength <= lpParamSelectorList->dwMaxLength ) &&
						     ( dwMaxLength >= lpParamSelectorList->dwMinLength ) &&
							 ( dwMaxLength <= lpParamSelectorList->dwMaxLength ) )
						{
							lpParamSelectorList->lpDecoderParams->dwMinLength = dwMinLength;
							lpParamSelectorList->lpDecoderParams->dwMaxLength = dwMaxLength;
							
							ScanSetDecoderLengths(
								lpParamSelectorList->lpDecoderParams->cDecoder,
								lpParamSelectorList->lpDecoderParams->dwMinLength,
								lpParamSelectorList->lpDecoderParams->dwMaxLength);
						}
						else
						{
							MessageBeep(MB_ICONEXCLAMATION);
							break;
						}
					}
					
					// Fall through

				case IDCANCEL:

					FreeItemSelector(lpParamSelectorList);

					// Exit the dialog
					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


//----------------------------------------------------------------------------
// ScanLen
//----------------------------------------------------------------------------

void ScanLen(LPTSTR lpszDecoder,
			 LPARAM lParam)
{
	LPPARAMSELECTORLIST lpParamSelectorList;
	
	lpParamSelectorList = GetItemSelectorList(lParam,lpszDecoder);

	if ( lpParamSelectorList != NULL )
	{
		ScanGetDecoderLengths(
			lpParamSelectorList->lpDecoderParams->cDecoder,
			&(lpParamSelectorList->lpDecoderParams->dwMinLength),
			&(lpParamSelectorList->lpDecoderParams->dwMaxLength));

		g_hdlg = CreateChosenDialog(g_hInstance,
									ScanLenDialogChoices,
									g_hwnd,
									ScanLenDlgProc,
									(LPARAM)lpParamSelectorList);
	}
}


//----------------------------------------------------------------------------
// DecParamDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK DecParamDlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	static LPPARAMSELECTORLIST lpParamSelectorList;
	HWND hWndList = GetDlgItem(hwnd,IDC_PARAMETERS);

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);

			{    
				TCHAR szText[MAX_PATH];
			
				lpParamSelectorList = (LPPARAMSELECTORLIST)lParam;

				TEXTSPRINTF(szText,
							TEXT("%s Parameters"),
							lpParamSelectorList->lpszText);
				SetWindowText(hwnd,szText);

				InitializeParamsListView(hWndList,lpParamSelectorList);
			}
			
			// Set focus to list view and its first item
			ListView_SelectItem(hWndList,0);
            
			// Return FALSE since focus was manually set
			return(FALSE);

 		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				switch(pLvdi->hdr.code)
				{
					case NM_DBLCLK:
						break;

					case NM_CLICK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

						}

						// Fall through

					case LVN_ITEMACTIVATE:
						PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0);
						break;
				}
			}
			break;

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_SCROLL_UP:
					SetFocus(hWndList);
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_SCROLL_DOWN:
					SetFocus(hWndList);
					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_SCROLL_LEFT:
				case IDC_RETARD:
					SetFocus(hWndList);
					ToggleCurrentParameter(hWndList,-1);
					break;

				case IDC_SCROLL_RIGHT:
				case IDC_ADVANCE:
					SetFocus(hWndList);
					ToggleCurrentParameter(hWndList,1);
					break;

				case IDC_HELP_ABOUT:
					
					About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							InvokePopUpMenu(hwnd,IDM_POPUP,1,0,0);

							break;

						case UI_STYLE_FINGER:
						case UI_STYLE_PEN:

							PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0L);
							
							break;
					}

					break;

				case IDC_TOGGLE:

					ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));

					ToggleCurrentParameter(hWndList,1);
					
					break;
					
				case IDOK:

					TerminateParamsListView(lpParamSelectorList);

					FreeItemSelector(lpParamSelectorList);

					// Fall through

				case IDCANCEL:

					// Exit the dialog
					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


//----------------------------------------------------------------------------
// DecParam
//----------------------------------------------------------------------------

void DecParam(LPTSTR lpszDecoder,
			 LPARAM lParam)
{
	LPPARAMSELECTORLIST lpParamSelectorList;
	lpParamSelectorList = GetItemSelectorList(lParam,lpszDecoder);

	if ( (lpParamSelectorList != NULL) && (lpParamSelectorList->dwSize>0) )
	{
		DWORD dwResult = 0;

		dwResult = ScanGetDecoderParams(
			lpParamSelectorList->lpDecoderParams->cDecoder,
			&(lpParamSelectorList->lpDecoderParams->dec_specific),
			lpParamSelectorList->dwSize);

		if ( !ScanError(TEXT("ScanGetDecoderParams Error"),
						dwResult,
						E_SCN_SUCCESS) )
		{		
			g_hdlg = CreateChosenDialog(g_hInstance,
										DecParamDialogChoices,
										g_hwnd,
										DecParamDlgProc,
										(LPARAM)lpParamSelectorList);
		}
	}
}


//----------------------------------------------------------------------------
// DecParamDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK UpceanParamDlgProc(HWND hwnd,
									UINT uMsg,
									WPARAM wParam,
									LPARAM lParam)
{
	HWND hWndList = GetDlgItem(hwnd,IDC_PARAMETERS);

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);

			SetWindowText(hwnd,(LPTSTR)lParam);

			InitializeUpcEanListView(hWndList,&SelectorListUPCEAN);
			
			// Set focus to list view and its first item
			ListView_SelectItem(hWndList,0);
            
			// Return FALSE since focus was manually set
			return(FALSE);

 		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				switch(pLvdi->hdr.code)
				{
					case NM_DBLCLK:
						break;

					case NM_CLICK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

						}

						// Fall through

					case LVN_ITEMACTIVATE:
						PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0);
						break;
				}
			}
			break;

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_SCROLL_UP:
					SetFocus(hWndList);
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_SCROLL_DOWN:
					SetFocus(hWndList);
					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_SCROLL_LEFT:
				case IDC_RETARD:
					SetFocus(hWndList);
					ToggleCurrentParameter(hWndList,-1);
					break;

				case IDC_SCROLL_RIGHT:
				case IDC_ADVANCE:
					SetFocus(hWndList);
					ToggleCurrentParameter(hWndList,1);
					break;

				case IDC_HELP_ABOUT:
					
					About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							InvokePopUpMenu(hwnd,IDM_POPUP,1,0,0);

							break;

						case UI_STYLE_FINGER:
						case UI_STYLE_PEN:

							PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0L);
							
							break;
					}

					break;

				case IDC_TOGGLE:

					ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));

					ToggleCurrentParameter(hWndList,1);
					
					break;
					
				case IDOK:
					{
						int i;
						LPDWORD lpdwUntyped = (LPDWORD)(((LPBYTE)(&Upc_ean_params))+sizeof(STRUCT_INFO));

						// copy data into UPC_EAN_PARAM
						for(i=0;SelectorListUPCEAN.lpItemSelector[i].lpszText;i++)
						{
							lpdwUntyped[i] = SelectorListUPCEAN.lpItemSelector[i].dwIndex;
						}

						// undo retry count offset
						Upc_ean_params.dwRetryCount += 2;
						
						ScanSetUPCEANParams(&Upc_ean_params);
					}

				// Fall through

				case IDCANCEL:

					// Exit the dialog
					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


//----------------------------------------------------------------------------
// DecParam
//----------------------------------------------------------------------------

void UpceanParam(LPTSTR lpszText)
{
	ScanGetUPCEANParams(&Upc_ean_params);

	// fix retry count so starts at 0 (instead of 2-10 it becomes 0-8)
	Upc_ean_params.dwRetryCount -= 2;
	
	g_hdlg = CreateChosenDialog(g_hInstance,
								DecParamDialogChoices,
								g_hwnd,
								UpceanParamDlgProc,
								(LPARAM)lpszText);
}


static LPTSTR szCodeIdTypes[] =
{
	TEXT("None"),
	TEXT("Symbol"),
	TEXT("AIM"),
};


static LPTSTR szScanTypes[] =
{
	TEXT("Foreground"),
	TEXT("Background"),
	TEXT("Monitor"),
};


DWORD TextToIndex(LPTSTR FAR *lplpszTexts,
				  LPTSTR lpszText,
				  DWORD dwMin,
				  DWORD dwMax)
{
	DWORD dwValue;

	for(dwValue=dwMin;dwValue<=dwMax;dwValue++)
	{
		if ( TEXTCMP(lplpszTexts[dwValue],lpszText) == 0 )
		{
			return(dwValue);
		}
	}

	return(dwMin);
}


DWORD IncWithWrap(DWORD dwValue,
				  DWORD dwMin,
				  DWORD dwMax,
				  int nInc)
{
	if ( ( nInc < 0 ) && ( dwValue <= dwMin ) )
	{
		dwValue = dwMax;
	}
	else if ( ( nInc > 0 ) & ( dwValue >= dwMax ) )
	{
		dwValue = dwMin;
	}
	else
	{
		dwValue += nInc;
	}

	return(dwValue);
}


LPTSTR IndexToText(LPTSTR FAR *lplpszTexts,
				   DWORD dwValue)
{
	return(lplpszTexts[dwValue]);
}


void UpdateValue(HWND hWnd,
				 DWORD dwMin,
				 DWORD dwMax,
				 int nInc)
{
	DWORD dwValue;
	TCHAR szMsg[MAX_PATH];

	GetWindowText(hWnd,szMsg,TEXTSIZEOF(szMsg));

	dwValue = TEXTTOL(szMsg);

	dwValue = IncWithWrap(dwValue,dwMin,dwMax,nInc);

	TEXTSPRINTF(szMsg,TEXT("%ld"),dwValue);
	SetWindowText(hWnd,szMsg);

	SetFocus(hWnd);
	Edit_SetSel(hWnd,0,-1);
}


static HWND hWndCodeIdType;
static HWND hWndScanType;
static HWND hWndWaveFile;
static HWND hWndDecodeBeepTime;
static HWND hWndDecodeBeepFrequency;
static HWND hWndDecodeLedTime;

static DWORD dwCodeIdType;
static DWORD dwScanType;
static BOOL bLocalFeedback;
static DWORD dwDecodeBeepTime;
static DWORD dwDecodeBeepFrequency;
static DWORD dwDecodeLedTime;
static TCHAR szWaveFile[MAX_PATH];


void IncCodeIdType(int nInc)
{
	TCHAR szMsg[MAX_PATH];

	GetWindowText(hWndCodeIdType,szMsg,TEXTSIZEOF(szMsg));

	dwCodeIdType = TextToIndex(szCodeIdTypes,
							   szMsg,
							   CODE_ID_TYPE_NONE,
							   CODE_ID_TYPE_AIM);

	dwCodeIdType = IncWithWrap(dwCodeIdType,
							   CODE_ID_TYPE_NONE,
							   CODE_ID_TYPE_AIM,
							   nInc);

	SetWindowText(hWndCodeIdType,
				  IndexToText(szCodeIdTypes,dwCodeIdType));
	Edit_SetSel(hWndCodeIdType,0,-1);
}


void IncScanType(int dwInc)
{
	TCHAR szMsg[MAX_PATH];
	DWORD dwScanType;

	GetWindowText(hWndScanType,szMsg,TEXTSIZEOF(szMsg));

	dwScanType = TextToIndex(szScanTypes,
							 szMsg,
							 SCAN_TYPE_FOREGROUND,
							 SCAN_TYPE_MONITOR);
	
	dwScanType = IncWithWrap(dwScanType,
							 SCAN_TYPE_FOREGROUND,
							 SCAN_TYPE_MONITOR,
							 dwInc);

	SetWindowText(hWndScanType,
				  IndexToText(szScanTypes,dwScanType));
	Edit_SetSel(hWndScanType,0,-1);
}


BOOL SetupFeedbackInformation(void)
{
	nBeeperFrequencyMinimum = 2500;
	nBeeperFrequencyMaximum = 3500;
	nBeeperFrequencyStep	= 10;

	nBeeperDurationMinimum	= 0;
	nBeeperDurationMaximum	= 5000;
	nBeeperDurationStep		= 100;

	nLedDurationMinimum		= 0;
	nLedDurationMaximum		= 5000;
	nLedDurationStep		= 100;

	SI_INIT(&FeedbackInfo);

	if ( !GetFeedbackInfo(&FeedbackInfo) )
	{
		return(FALSE);
	}

	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMinimum,nBeeperFrequencyMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMaximum,nBeeperFrequencyMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyStep,nBeeperFrequencyStep);

	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMinimum,nBeeperDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMaximum,nBeeperDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationStep,nBeeperDurationStep);

	SI_GET_FIELD(&FeedbackInfo,nLedDurationMinimum,nLedDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationMaximum,nLedDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationStep,nLedDurationStep);

	return(TRUE);
}


void TestDriveBeep(HWND hWndDecodeBeepFrequency,
				   HWND hWndDecodeBeepTime)
{
	TCHAR szText[MAX_PATH];
	DWORD dwDecodeBeepFrequency;
	DWORD dwDecodeBeepTime;

	Edit_GetText(hWndDecodeBeepFrequency,szText,TEXTSIZEOF(szText));
	dwDecodeBeepFrequency = TEXTTOL(szText);

	Edit_GetText(hWndDecodeBeepTime,szText,TEXTSIZEOF(szText));
	dwDecodeBeepTime = TEXTTOL(szText);

	if ( ( dwDecodeBeepFrequency != 0 ) &&
		 ( dwDecodeBeepTime != 0) )
	{
		PlayBeeper(dwDecodeBeepFrequency,dwDecodeBeepTime);
	}
}


//----------------------------------------------------------------------------
// ScanPrmDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK ScanPrmDlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	TCHAR szWavePath[MAX_PATH];
	
	switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);

			{
				DWORD dwResult;

				TCHAR szMsg[MAX_PATH];

				SetupFeedbackInformation();

				dwResult = ScanGetScanParams(&dwCodeIdType,
											 &dwScanType,
											 &bLocalFeedback,
											 &dwDecodeBeepTime,
											 &dwDecodeBeepFrequency,
											 &dwDecodeLedTime,
											 szWaveFile);

				hWndCodeIdType = GetDlgItem(hwnd,IDC_CODEIDTYPE);
				hWndScanType = GetDlgItem(hwnd,IDC_SCANTYPE);
				hWndWaveFile = GetDlgItem(hwnd,IDC_WAVEFILE);
				hWndDecodeBeepTime = GetDlgItem(hwnd,IDC_DBT);
				hWndDecodeBeepFrequency = GetDlgItem(hwnd,IDC_DBF);
				hWndDecodeLedTime = GetDlgItem(hwnd,IDC_DLT);

				SetWindowText(hWndCodeIdType,
							  IndexToText(szCodeIdTypes,dwCodeIdType));

				SetWindowText(hWndScanType,
							  IndexToText(szScanTypes,dwScanType));

				SetWindowText(hWndWaveFile,szWaveFile);

				if ( nBeeperFrequencyStep )
				{
					TEXTSPRINTF(szMsg,TEXT("%ld"),dwDecodeBeepFrequency);
					SetWindowText(hWndDecodeBeepFrequency,szMsg);
				}
				else
				{
					ShowWindow(hWndDecodeBeepFrequency,SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DBFDN),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DBFUP),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_STATIC_DBF),SW_HIDE);
				}

				if ( nBeeperDurationStep )
				{
					TEXTSPRINTF(szMsg,TEXT("%ld"),dwDecodeBeepTime);
					SetWindowText(hWndDecodeBeepTime,szMsg);
				}
				else
				{
					ShowWindow(hWndDecodeBeepTime,SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DBTDN),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DBTUP),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_STATIC_DBT),SW_HIDE);
				}

				if ( nLedDurationStep )
				{
					TEXTSPRINTF(szMsg,TEXT("%ld"),dwDecodeLedTime);
					SetWindowText(hWndDecodeLedTime,szMsg);
				}
				else
				{
					ShowWindow(hWndDecodeLedTime,SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DLTDN),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_DLTUP),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_STATIC_LDT),SW_HIDE);
				}

				SetFocus(hWndCodeIdType);
				Edit_SetSel(hWndCodeIdType,0,-1);
			}

			// Return FALSE since focus was manually set
			return(FALSE);

        case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_HELP_ABOUT:
					
					About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					break;

				case IDC_RETARD:
					switch(GetDlgCtrlID(GetFocus()))
					{
						case IDC_CODEIDTYPE:
							IncCodeIdType(-1);
							break;
						case IDC_SCANTYPE:
							IncScanType(-1);
							break;
						case IDC_WAVEFILE:
							GetWavePath(szWavePath);
							if ( IncFileName(hWndWaveFile,-1,szWavePath) )
							{
								TCHAR szText[MAX_PATH];
								Edit_GetText(hWndWaveFile,szText,TEXTSIZEOF(szText));
								sndPlaySound(szText,SND_ASYNC);
							}
							break;
						case IDC_DBT:
							UpdateValue(hWndDecodeBeepTime,
										nBeeperDurationMinimum,
										nBeeperDurationMaximum,
										-nBeeperDurationStep);
							TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);
							break;
						case IDC_DBF:
							UpdateValue(hWndDecodeBeepFrequency,
										nBeeperFrequencyMinimum,
										nBeeperFrequencyMaximum,
										-nBeeperFrequencyStep);
							TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);
							break;
						case IDC_DLT:
							UpdateValue(hWndDecodeLedTime,
										nLedDurationMinimum,
										nLedDurationMaximum,
										-nLedDurationStep);
							break;
					}
					break;

				case IDC_ADVANCE:
					switch(GetDlgCtrlID(GetFocus()))
					{
						case IDC_CODEIDTYPE:
							IncCodeIdType(1);
							break;
						case IDC_SCANTYPE:
							IncScanType(1);
							break;
						case IDC_WAVEFILE:
							GetWavePath(szWavePath);
							if ( IncFileName(hWndWaveFile,1,szWavePath) )
							{
								TCHAR szText[MAX_PATH];
								Edit_GetText(hWndWaveFile,szText,TEXTSIZEOF(szText));
								sndPlaySound(szText,SND_ASYNC);
							}
							break;
						case IDC_DBT:
							UpdateValue(hWndDecodeBeepTime,
										nBeeperDurationMinimum,
										nBeeperDurationMaximum,
										nBeeperDurationStep);
							TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);
							break;
						case IDC_DBF:
							UpdateValue(hWndDecodeBeepFrequency,
										nBeeperFrequencyMinimum,
										nBeeperFrequencyMaximum,
										nBeeperFrequencyStep);
							TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);
							break;
						case IDC_DLT:
							UpdateValue(hWndDecodeLedTime,
										nLedDurationMinimum,
										nLedDurationMaximum,
										nLedDurationStep);
							break;
					}
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							switch(GetDlgCtrlID(GetFocus()))
							{
								case IDC_CODEIDTYPE:
									SetFocus(hWndScanType);
									Edit_SetSel(hWndScanType,0,-1);
									break;
								case IDC_SCANTYPE:
									SetFocus(hWndWaveFile);
									Edit_SetSel(hWndWaveFile,0,-1);
									break;
								case IDC_WAVEFILE:
									SetFocus(hWndDecodeBeepTime);
									Edit_SetSel(hWndDecodeBeepTime,0,-1);
									break;
								case IDC_DBT:
									SetFocus(hWndDecodeBeepFrequency);
									Edit_SetSel(hWndDecodeBeepFrequency,0,-1);
									break;
								case IDC_DBF:
									SetFocus(hWndDecodeLedTime);
									Edit_SetSel(hWndDecodeLedTime,0,-1);
									break;
								case IDC_DLT:
									PostMessage(hwnd,WM_COMMAND,IDOK,0L);
									break;
							}
							
							break;

						case UI_STYLE_PEN:
						case UI_STYLE_FINGER:

							break;
					}

					break;
				
				case IDC_CODEIDTYPE:

					if ( HIWORD(wParam) == 0 )
					{
						IncCodeIdType(1);
					}

					break;
					
				case IDC_SCANTYPE:

					if ( HIWORD(wParam) == 0 )
					{
						IncScanType(1);
					}

					break;
					
				case IDC_WAVEFILE:
					
					if ( HIWORD(wParam) == 0 )
					{
						GetWavePath(szWavePath);
						if ( IncFileName(hWndWaveFile,1,szWavePath) )
						{
							TCHAR szText[MAX_PATH];
							Edit_GetText(hWndWaveFile,szText,TEXTSIZEOF(szText));
							sndPlaySound(szText,SND_ASYNC);
						}
					}
					
					break;
					
				case IDC_DBTDN:
					
					UpdateValue(hWndDecodeBeepTime,
								nBeeperDurationMinimum,
								nBeeperDurationMaximum,
								-nBeeperDurationStep);
					
					TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);

					break;

				case IDC_DBTUP:

					UpdateValue(hWndDecodeBeepTime,
								nBeeperDurationMinimum,
								nBeeperDurationMaximum,
								nBeeperDurationStep);
					
					TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);

					break;
					
				case IDC_DBFDN:

					UpdateValue(hWndDecodeBeepFrequency,
								nBeeperFrequencyMinimum,
								nBeeperFrequencyMaximum,
								-nBeeperFrequencyStep);
					
					TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);

					break;

				case IDC_DBFUP:

					UpdateValue(hWndDecodeBeepFrequency,
								nBeeperFrequencyMinimum,
								nBeeperFrequencyMaximum,
								nBeeperFrequencyStep);
					
					TestDriveBeep(hWndDecodeBeepFrequency,hWndDecodeBeepTime);

					break;
					
				case IDC_DLTDN:
					
					UpdateValue(hWndDecodeLedTime,
								nLedDurationMinimum,
								nLedDurationMaximum,
								-nLedDurationStep);

					break;

				case IDC_DLTUP:

					UpdateValue(hWndDecodeLedTime,0,5000,100);
					
					break;
					
				case IDOK:

					{
						DWORD dwResult;

						TCHAR szMsg[MAX_PATH];

						GetWindowText(hWndCodeIdType,szMsg,MAX_PATH);
						dwCodeIdType = TextToIndex(szCodeIdTypes,
												   szMsg,
												   CODE_ID_TYPE_NONE,
												   CODE_ID_TYPE_AIM);

						GetWindowText(hWndScanType,szMsg,MAX_PATH);
						dwScanType = TextToIndex(szScanTypes,
												 szMsg,
												 SCAN_TYPE_FOREGROUND,
												 SCAN_TYPE_MONITOR);

						GetWindowText(hWndWaveFile,szWaveFile,MAX_PATH);

						GetWindowText(hWndDecodeBeepTime,szMsg,MAX_PATH);
						dwDecodeBeepTime = TEXTTOL(szMsg);

						GetWindowText(hWndDecodeBeepFrequency,szMsg,MAX_PATH);
						dwDecodeBeepFrequency = TEXTTOL(szMsg);
						
						GetWindowText(hWndDecodeLedTime,szMsg,MAX_PATH);
						dwDecodeLedTime = TEXTTOL(szMsg);

						dwResult = ScanSetScanParams(dwCodeIdType,
													 dwScanType,
													 bLocalFeedback,
													 dwDecodeBeepTime,
													 dwDecodeBeepFrequency,
													 dwDecodeLedTime,
													 szWaveFile);

						if ( ScanError(TEXT("ScanSetScanParams"),
									   dwResult,
									   E_SCN_SUCCESS) )
						{
							break;
						}
					}

					// Fall through

				case IDCANCEL:

					// Exit the dialog
					ExitDialog();

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}



//----------------------------------------------------------------------------
// ScanPrm
//----------------------------------------------------------------------------

void ScanPrm(BOOL bQuitOnExit)
{
	g_hdlg = CreateChosenDialog(g_hInstance,
								ScanPrmDialogChoices,
								g_hwnd,
								ScanPrmDlgProc,
								(LPARAM)bQuitOnExit);

	WaitUntilDialogDoneSkip(FALSE);

	if ( bQuitOnExit )
	{
		// Terminate application
		PostMessage(g_hwnd,WM_CLOSE,0,0);
	}
}


//----------------------------------------------------------------------------
// ScanCfgDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK ScanCfgDlgProc(HWND hwnd,
								UINT uMsg,
								WPARAM wParam,
								LPARAM lParam)
{
	static BOOL bQuitOnExit = FALSE;

	HWND hWndList = GetDlgItem(hwnd,IDC_DECODERS);

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);

			bQuitOnExit = (BOOL)lParam;

			SetNewDialogFont(hWndList,140);

			InitializeDecoderListView(hWndList);
			
			// Enable check boxes based on supported decoders
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,UPCE0);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,UPCE1);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,UPCA);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,EAN8);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,EAN13);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,CODABAR);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,MSI);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,CODE39);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,CODE11);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,CODE93);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,CODE128);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,D2OF5);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,I2OF5);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,PDF417);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,TRIOPTIC39);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,MICROPDF);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,MAXICODE);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,DATAMATRIX);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,QRCODE);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,USPOSTNET);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,USPLANET);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,UKPOSTAL);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,JAPPOSTAL);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,AUSPOSTAL);
			SUPPORTED_DECODER_TO_SUPPORTED_UI(hWndList,DUTCHPOSTAL);

			// Set check boxes based on enabled decoders
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,UPCE0);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,UPCE1);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,UPCA);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,EAN8);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,EAN13);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,CODABAR);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,MSI);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,CODE39);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,CODE11);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,CODE93);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,CODE128);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,D2OF5);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,I2OF5);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,PDF417);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,TRIOPTIC39);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,MICROPDF);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,MAXICODE);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,DATAMATRIX);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,QRCODE);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,USPOSTNET);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,USPLANET);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,UKPOSTAL);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,JAPPOSTAL);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,AUSPOSTAL);
			ENABLED_DECODER_TO_ENABLED_UI(hWndList,DUTCHPOSTAL);

			AddDecoderToListView(hWndList,TEXT("Upc/Ean General"),TRUE,0,UPCEAN_GENERAL);
			EnableDecoderInListView(hWndList,TEXT("Upc/Ean General"),FALSE);


			// Set focus to list view and its first item
			ListView_SelectItem(hWndList,0);
            
			// Return FALSE since focus was manually set
			return(FALSE);

		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				switch(pLvdi->hdr.code)
				{
					case NM_DBLCLK:
						break;

					case NM_CLICK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

						}
					case LVN_ITEMACTIVATE:
						PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0);
						break;
				}
			}
			break;

        case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_SCROLL_UP:
					SetFocus(hWndList);
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_SCROLL_DOWN:
					SetFocus(hWndList);
					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					break;

				case IDC_HELP_ABOUT:
					
					About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
					
					break;

				case IDC_PARAMS:
					{
						int iItem;
						LV_ITEM lvI;
						TCHAR szItemText[MAX_PATH];

						iItem = ListView_GetSelectedItem(hWndList);

						lvI.mask = LVIF_PARAM | LVIF_TEXT;
						lvI.iItem = iItem;
						lvI.iSubItem = 0;
						lvI.cchTextMax = TEXTSIZEOF(szItemText);
						lvI.pszText = szItemText;
						ListView_GetItem(hWndList,&lvI);

						if ( lvI.lParam == UPCEAN_GENERAL )
						{
							UpceanParam(szItemText);
						}
						else
						{
							DecParam(szItemText,ListView_GetSelectedParam(hWndList));
						}
					}

					break;

				case IDC_LENGTHS:
					{
						TCHAR szItemText[MAX_PATH];

						ListView_GetSelectedText(hWndList,
												 szItemText,
												 TEXTSIZEOF(szItemText));

						ScanLen(szItemText,ListView_GetSelectedParam(hWndList));
					}
					break;
				
				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							InvokePopUpMenu(hwnd,IDM_POPUP,0,0,0);

							break;

						case UI_STYLE_PEN:
						case UI_STYLE_FINGER:
							
							PostMessage(hwnd,WM_COMMAND,IDC_TOGGLE,0L);

							break;
					}

					break;
				
				case IDC_TOGGLE:

//					ListView_SelectItem(hWndList,ListView_GetSelectedItem(hWndList));

					ToggleSelectedDecoder(hWndList);
					
					break;

				case IDOK:

					// Set check boxes based on enabled decoders
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,UPCE0);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,UPCE1);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,UPCA);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,EAN8);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,EAN13);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,CODABAR);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,MSI);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,CODE39);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,CODE11);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,CODE93);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,CODE128);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,D2OF5);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,I2OF5);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,PDF417);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,TRIOPTIC39);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,MICROPDF);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,MAXICODE);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,DATAMATRIX);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,QRCODE);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,USPOSTNET);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,USPLANET);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,UKPOSTAL);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,JAPPOSTAL);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,AUSPOSTAL);
					ENABLED_UI_TO_ENABLED_DECODER(hWndList,DUTCHPOSTAL);

					// Fall through

				case IDCANCEL:

					TerminateDecoderListView(hWndList);
					
					// Exit the dialog
					ExitDialog();

					if ( bQuitOnExit )
					{
						// Terminate application
						PostMessage(g_hwnd,WM_CLOSE,0,0);
					}

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}



//----------------------------------------------------------------------------
// ScanCfg
//----------------------------------------------------------------------------

void ScanCfg(BOOL bQuitOnExit)
{
	g_hdlg = CreateChosenDialog(g_hInstance,
								ScanCfgDialogChoices,
								g_hwnd,
								ScanCfgDlgProc,
								(LPARAM)bQuitOnExit);

	WaitUntilDialogDoneSkip(FALSE);
}

