
//--------------------------------------------------------------------
// FILENAME:			MyInit.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains exported functions and variables from
//						MyInit.c.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef MYINIT_H_

#define MYINIT_H_

#ifdef __cplusplus
extern "C"
{
#endif



//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------


#include "..\common\StdInit.h"


//----------------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------------


BOOL MyInitApplication(HINSTANCE hInstance,
					   LPTSTR szAppClass,
					   HBRUSH hBackgroundBrush,
					   LPTSTR szMainMenu,
					   LPTSTR szAppIcon,
					   BOOL bMultipleInstances);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef MYINIT_H_    */

