
//--------------------------------------------------------------------
// FILENAME:			ScanDlg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for scanning dialog window procedures.
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANDLG_H_

#define SCANDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif


void ScanDialog(HWND hwnd);

void ScanPrm(BOOL bQuitOnExit);

void ScanCfg(BOOL bQuitOnExit);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANDLG_H_   */

