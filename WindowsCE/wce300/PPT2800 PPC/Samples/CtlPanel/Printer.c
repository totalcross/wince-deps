
//--------------------------------------------------------------------
// FILENAME:            Printer.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to set printer settings
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _PRINTERCTL

#include "printer.h"

//----------------------------------------------------------------------------
// Printer Functions
//----------------------------------------------------------------------------

#define REG_SYMPRINT TEXT("Drivers\\BuiltIn\\SymPrint")

#define REG_SYMPRINT1 TEXT("Drivers\\BuiltIn\\SymPrint1")


TCHAR l_szPrinter[MAX_PATH32];
TCHAR l_szPrinterParameter[MAX_PATH32];
static DWORD g_dwPrinterCount = 0;
PRINT_VERSION_INFO l_PrinterVersionInfo;

void SetupPrinterName(void)
{
    static TCHAR szPrinterName[10][MAX_PATH32];
    TCHAR szPrinter[MAX_PATH32];

    DWORD dwPrinterIndex;
    DWORD dwSize, dwRetCode;
    FILETIME ft;
    HKEY hKey;
			
        // look for the supported printers
        dwRetCode = RegOpenKeyEx(REG_SECTION,REG_SYMPRINT,0,KEY_ALL_ACCESS, &hKey);
        // get how many printers supported
        if (dwRetCode == ERROR_SUCCESS)
        {
            RegQueryInfoKey(hKey,NULL,NULL,0,&g_dwPrinterCount,NULL,
                            NULL,NULL,NULL,NULL,NULL,NULL);
        }
    
        if (g_dwPrinterCount)   // if there are printers, allocate strings
        {
            for (dwPrinterIndex = 0; dwPrinterIndex < g_dwPrinterCount; dwPrinterIndex++)
            {
                // now query for the subkey(s)
                dwSize = countof(szPrinter);
                // get all the subkey
                RegEnumKeyEx(hKey,dwPrinterIndex,szPrinter,&dwSize,NULL,NULL,0,&ft);
                
                // 
                TEXTCPY(szPrinterName[dwPrinterIndex],szPrinter);
                
                lpszValuesPrinterName[dwPrinterIndex] = (LPTSTR)szPrinterName[dwPrinterIndex];
                
            }
            
            lpszValuesPrinterName[dwPrinterIndex] = NULL;
        }

        RegCloseKey(hKey);
}

// get the index of the default printer
void GetPrinterIndex(void)
{
    DWORD dwRetCode = 0;
    DWORD dwIndex = 0;
    HKEY hKey;
    TCHAR szDefaultPrinter[MAX_PATH32];

    // get the symbol printer infomation
    dwRetCode = RegOpenKeyEx(REG_SECTION,REG_SYMPRINT1,0,KEY_READ, &hKey);
    
    if (dwRetCode == ERROR_SUCCESS)
    {
        // query for "DeviceName"--default printer
        DWORD dwLen;
        DWORD dwType;

        dwLen = countof(szDefaultPrinter);
        dwRetCode = RegQueryValueEx(hKey,TEXT("DeviceName"),NULL,&dwType,(LPBYTE)szDefaultPrinter,&dwLen);
        RegCloseKey(hKey);
        
        // no default, use the first one on the list
        if ((dwRetCode != ERROR_SUCCESS) || (dwLen == 2))
        {
            //update the global printer
            TEXTCPY(l_szPrinter,lpszValuesPrinterName[0]);
        }
        else    // go through the list to get the default index
        {
            //update the global printer
            TEXTCPY(l_szPrinter,szDefaultPrinter);
        }
    }

}

// get the index of the default printer
DWORD GetCEDefaultPrinter(LPITEMSELECTOR lpis)
{
	DWORD i;

    for (i=0; i<g_dwPrinterCount;i++)
    {
    	if (TEXTCMP(l_szPrinter,lpszValuesPrinterName[i]) == 0)
        {
        	return (i);
        }
    }

    // default to the first one
    return (0);
}

void SetDefaultPrinter(LPTSTR lpszPrinter)
{
    DWORD dwRetCode=0;
    HKEY hKey;

    if (lpszPrinter != NULL)
    {
        // open the registry for setting
        dwRetCode = RegOpenKeyEx(REG_SECTION,REG_SYMPRINT1,0,KEY_ALL_ACCESS, &hKey);
        
        if (dwRetCode == ERROR_SUCCESS)
        {
            RegSetValueEx(hKey,TEXT("DeviceName"),0,REG_SZ,(LPBYTE)lpszPrinter,((TEXTLEN(lpszPrinter)+1)*sizeof(TCHAR)));
            //(_tcslen(lpszPrinter)+1)*sizeof(TCHAR));
            RegCloseKey(hKey);     

			if (l_dwPersistence == PERSISTENCE_PERMANENT)
			{
				dwRetCode = WriteRegFile(REG_SYMPRINT1,
	   								 TEXT("DeviceName"),
	    							 REG_SZ,
		    						 lpszPrinter,
			    					 countof(lpszPrinter),
									 0);

				if (dwRetCode != ERROR_SUCCESS)
				{
					ReportError(TEXT("ERROR WriteReg"),dwRetCode);
				}
			}

        }
    }

}
    

void SelectPrinter(LPITEMSELECTOR lpis,
						   DWORD dwSelection)
{
    LPTSTR lpszText = GetTextItem(lpis,dwSelection);    
    
    if ( lpszText != NULL )
	{
        TEXTCPY(l_szPrinter,lpszText);
    }

}

void EnterPrinterSetup(LPITEMSELECTOR lpis,
							 DWORD dwSelection)
{
	LoadString(g_hInstance,IDS_PRINTER,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;
    
    SetupPrinterName();
	// get the default printer name
	GetPrinterIndex();

    
    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)PrinterSelector);
}


void ExitPrinterSetup(LPITEMSELECTOR lpis,
						DWORD dwSelection)
{
    // if we accepted the change then update the registry setting
    if ( l_bDialogWasOKed )
	{
		SetDefaultPrinter(l_szPrinter);
    }

	l_bDialogWasOKed = FALSE;
	l_lpExitFunction = NULL;

}

void GetDefaultPrinterParameter(void)
{
    DWORD dwRetCode;
    HKEY hKey;
    TCHAR szRegKey[MAX_PATH64];


    // we have l_szPrinter as the current selected printer
    // first, we query the current printer info and get the index from the list
    // contruct the reg path
    TEXTCPY(szRegKey,REG_SYMPRINT);
    TEXTCAT(szRegKey,TEXT("\\"));
    TEXTCAT(szRegKey,l_szPrinter);

    dwRetCode = RegOpenKeyEx(REG_SECTION,szRegKey,0,KEY_ALL_ACCESS, &hKey);
    if (dwRetCode == ERROR_SUCCESS)
    {
        // query for "PrintPort"--Printer port setup
        DWORD dwLen;
        DWORD dwType;
        TCHAR szPrinterParameter[MAX_PATH32];


        // query the current printer info and get the index from the list
        dwLen = countof(szPrinterParameter);
        dwRetCode = RegQueryValueEx(hKey,TEXT("PrintPort"),NULL,&dwType,(LPBYTE)szPrinterParameter,&dwLen);
        RegCloseKey(hKey);

        // can't find the info, use default (index 0)
        if ((dwRetCode != ERROR_SUCCESS) || (dwLen == 2))
        {
            TEXTCPY(l_szPrinterParameter,lpszPrinterParameters[0]);
        }
        else // found the value, now update the global variable
        {
            TEXTCPY(l_szPrinterParameter,szPrinterParameter);
        }
    }
}

DWORD GetPrinterParameter(LPITEMSELECTOR lpis)
{

    DWORD i;

    for (i=0; i<MAX_PRINTER_PARAMETER; i++)
    {
		if (TEXTCMP(lpszPrinterParameters[i],l_szPrinterParameter)==0)
        {
			// got it, return the current index
            return i;
        }
    }
            
	return (DWORD)(MAX_PRINTER_PARAMETER);  // not supported
}


void SetDefaultPrinterParameter(LPTSTR lpsz)
{
    DWORD dwRetCode;
    HKEY hKey;
    TCHAR szRegKey[MAX_PATH];


    // Update the PrintPort for the current selected printer
    // contruct the reg path
    TEXTCPY(szRegKey,REG_SYMPRINT);
    TEXTCAT(szRegKey,TEXT("\\"));
    TEXTCAT(szRegKey,l_szPrinter);

    if (lpsz != NULL)
    {

        dwRetCode = RegOpenKeyEx(REG_SECTION,szRegKey,0,KEY_ALL_ACCESS, &hKey);
        if (dwRetCode == ERROR_SUCCESS)
        {
            // open the registry for setting
            RegSetValueEx(hKey,TEXT("PrintPort"),0,REG_SZ,(LPBYTE)lpsz,((TEXTLEN(lpsz) + 1)*sizeof(TCHAR)));
            RegCloseKey(hKey);

			if (l_dwPersistence == PERSISTENCE_PERMANENT)
			{
				dwRetCode = WriteRegFile(szRegKey,
	   								 TEXT("PrintPort"),
	    							 REG_SZ,
		    						 lpsz,
			    					 countof(lpsz),
									 0);

				if (dwRetCode != ERROR_SUCCESS)
				{
					ReportError(TEXT("ERROR WriteReg"),dwRetCode);
				}
			}

        }
    }
}

void SelectPrinterParameter(LPITEMSELECTOR lpis,
						   DWORD dwSelection)
{
    LPTSTR lpszText = GetTextItem(lpis,dwSelection);    
    
    if( ( lpszText != NULL ) && (dwSelection < MAX_PRINTER_PARAMETER))
	{
        TEXTCPY(l_szPrinterParameter,lpszText);
    }
}

void EnterPrinterParameters(LPITEMSELECTOR lpis,
							 DWORD dwSelection)
{

	LoadString(g_hInstance,IDS_PRINTERPARAMETER,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	GetDefaultPrinterParameter();
    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)PrinterParameterSelector);
}


void ExitPrinterParameters(LPITEMSELECTOR lpis,
							DWORD dwSelection)
{
    // update registry setting if accepted
    if (l_bDialogWasOKed)
    {
        SetDefaultPrinterParameter(l_szPrinterParameter);
    }

	l_bDialogWasOKed = FALSE;

    // now set the exit function to exitprintersetup where
    // we need to check should we accepted the changes for that printer
    l_lpExitFunction = ExitPrinterSetup;    
}


LPTSTR GetPrinterMDDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PrinterVersionInfo.dwMddVersion),LOWORD(l_PrinterVersionInfo.dwMddVersion));
    lpsz = szMsg;    

    return lpsz;
}

LPTSTR GetPrinterCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PrinterVersionInfo.dwCAPIVersion),LOWORD(l_PrinterVersionInfo.dwCAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}

LPTSTR GetPrinterPDDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PrinterVersionInfo.dwPddVersion),LOWORD(l_PrinterVersionInfo.dwPddVersion));
    lpsz = szMsg;    

    return lpsz;
}

LPTSTR GetPrinterTLDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PrinterVersionInfo.dwTldVersion),LOWORD(l_PrinterVersionInfo.dwTldVersion));
    lpsz = szMsg;    

    return lpsz;
}

void EnterPrinterVersion(LPITEMSELECTOR lpis,DWORD dwSelection)
{

    // A MUST for all the struct info call.
    // 1. Set dwAllocated = sizeof(Printer_VERSION_INFO);
    // 2. Set dwUsed = 0;
    
	{
	HDC	hDC = NULL;

	hDC = PRINT_CreateDC(NULL,l_szPrinter,NULL,NULL);
    if (hDC == NULL)
    {
        MyError(TEXT("Failed to Create DC"),TEXT("PrinterAPI"));
        return;
    }
        
    SI_ALLOC_ALL(&l_PrinterVersionInfo);
    l_PrinterVersionInfo.StructInfo.dwUsed = 0;    
	PRINT_GetVersion(hDC,&l_PrinterVersionInfo);
	PRINT_DeleteDC(hDC);

	}

	LoadString(g_hInstance,IDS_GETVERSION,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)PrinterVersionSelector);

}

void ExitPrinterVersion(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bDialogWasOKed = FALSE;
    // now set the exit function to next exit function.
    l_lpExitFunction = ExitPrinterSetup;    

}

#endif
