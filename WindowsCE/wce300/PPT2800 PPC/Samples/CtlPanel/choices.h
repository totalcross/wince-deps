
//--------------------------------------------------------------------
// FILENAME:        choices.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     CtlPanel
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef CHOICES_H_

#define CHOICES_H_

#ifdef __cplusplus
extern "C"
{
#endif


static DIALOG_CHOICE CtlPanelDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_CTLPANEL_PC_STD,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_CTLPANEL_PPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_CTLPANEL_PPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_CTLPANEL_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_CTLPANEL_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_CTLPANEL_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_CTLPANEL_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_CTLPANEL_7500_TSK,	NULL,	UI_STYLE_DEFAULT }
};



static DIALOG_CHOICE EditDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_EDIT_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_EDIT_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_EDIT_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_EDIT_7200_STD,		NULL,	UI_STYLE_DEFAULT},
	{ ST_7200_TSK,	IDD_EDIT_7200_TSK,		NULL,	UI_STYLE_DEFAULT},
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_EDIT_7500_STD,		NULL,	UI_STYLE_DEFAULT},
	{ ST_7500_TSK,	IDD_EDIT_7500_TSK,		NULL,	UI_STYLE_DEFAULT},
#endif
	{ ST_UNKNOWN,	IDD_EDIT_7500_TSK,		NULL,	UI_STYLE_DEFAULT }
};


static DIALOG_CHOICE HelpDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_HELP_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_HELP_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_HELP_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_HELP_7200_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_HELP_7200_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_HELP_7500_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_HELP_7500_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_HELP_7500_TSK,		NULL,	UI_STYLE_DEFAULT }
};



#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CHOICES_H_   */
