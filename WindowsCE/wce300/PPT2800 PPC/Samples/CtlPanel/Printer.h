
//--------------------------------------------------------------------
// FILENAME:        Printer.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Printer.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef PRINTER_H_

#define PRINTER_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

#include "printapi.h"

#define MAX_PRINTER_PARAMETER 13


// use lpvoid to pass pointer
typedef HDC (WINAPI *FPPRINT_CREATEDC)(LPCTSTR, LPCTSTR, LPCTSTR,LPVOID);
typedef BOOL(WINAPI *FPPRINT_DELETEDC)(HDC);

typedef int (WINAPI *FPPRINT_GETVERSION)(HDC, LPPRINT_VERSION_INFO);

static LPTSTR lpszValuesPrinterName[11];

static LPTSTR lpszPrinterParameters[] =
{
	TEXT("COM1: 38400"),
	TEXT("COM1: 19200"),
	TEXT("COM1: 9600"),
	TEXT("COM2: 38400"),
	TEXT("COM2: 19200"),
	TEXT("COM2: 9600"),
	TEXT("COM3: 38400"),
	TEXT("COM3: 19200"),
	TEXT("COM3: 9600"),
	TEXT("COM4: 38400"),
	TEXT("COM4: 19200"),
	TEXT("COM4: 9600"),
	TEXT("LPT1:"),
    TEXT("Not Supported"),
	NULL
};



DWORD GetCEDefaultPrinter(LPITEMSELECTOR lpis);
void SelectPrinter(LPITEMSELECTOR lpis,DWORD dwSelection);


void EnterPrinterParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPrinterParameters(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetPrinterParameter(LPITEMSELECTOR lpis);
void SelectPrinterParameter(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterPrinterVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPrinterVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
// Printer Version Functions
LPTSTR GetPrinterMDDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPrinterCAPIVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPrinterPDDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPrinterTLDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);


ITEMSELECTOR PrinterParameterSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_PRINTERPARAMETER),
		l_lpszHelpFileName,
		STRING_LIST(lpszPrinterParameters,GetPrinterParameter,SelectPrinterParameter,NULL)
    },
	END_ITEMSELECTOR
};

ITEMSELECTOR PrinterVersionSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetPrinterCAPIVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_PDDVERSION),
		NO_HELP,
		GET_STRING(GetPrinterPDDVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_MDDVERSION),
		NO_HELP,
		GET_STRING(GetPrinterMDDVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_TLDVERSION),
		NO_HELP,
		GET_STRING(GetPrinterTLDVersion,NULL)
	},
	
	END_ITEMSELECTOR

};

ITEMSELECTOR PrinterSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_SELECTEDPRINTER),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesPrinterName,GetCEDefaultPrinter,SelectPrinter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_PRINTERPARAMETER),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPrinterParameters,ExitPrinterParameters)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_GETVERSION),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPrinterVersion,ExitPrinterVersion)
    },
	END_ITEMSELECTOR
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef PRINTER_H_   */
