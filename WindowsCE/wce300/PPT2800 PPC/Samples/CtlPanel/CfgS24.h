
//--------------------------------------------------------------------
// FILENAME:        CfgS24.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for CfgS24.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef CFGS24_H_

#define CFGS24_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

BOOL bAssociated = FALSE;
BOOL g_bNoDHCP = TRUE;
BOOL bDS = TRUE;		// default to DS driver
BOOL bFH = FALSE;
BOOL bFuncMode = FALSE;

DIALOG_CHOICE S24SignalDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_S24SIGNAL_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_S24SIGNAL_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_S24SIGNAL_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_S24SIGNAL_7200_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_S24SIGNAL_7200_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_S24SIGNAL_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_S24SIGNAL_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_S24SIGNAL_7500_STD,	NULL,	UI_STYLE_DEFAULT },
};

DIALOG_CHOICE S24PingDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_S24PINGTEST_PC_STD,			NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_S24PINGTEST_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_S24PINGTEST_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_S24PINGTEST_7200_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_S24PINGTEST_7200_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_S24PINGTEST_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_S24PINGTEST_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_S24PINGTEST_7500_STD,	NULL,	UI_STYLE_DEFAULT },
};


// used by the range
LPTSTR lpszValuesNoYes[] =
{
	RESOURCE_STRING(IDS_NO),
	RESOURCE_STRING(IDS_YES),
	NULL
};

LPTSTR lpszValuesPSPCAM[] =
{
    TEXT("CAM"),
    TEXT("PSP"),
    NULL
};

LPTSTR lpszValuesRateControl[] =
{
    TEXT("Not Supported"),		// 0000
	TEXT("1 MBit"),				// 0001
    TEXT("2 MBits"),			// 0010
    TEXT("1 and 2 MBits"),		// 0011

    TEXT("5.5 MBits"),			// 0100
    TEXT("1 and 5.5 MBits"),		// 0101
    TEXT("2 and 5.5 MBits"),		// 0110
    TEXT("1, 2, 5.5 MBits"),		// 0111
    
	TEXT("11 MBits"),			// 1000
    TEXT("1 and 11 MBits"),		// 1001
    TEXT("2 and 11 MBits"),		// 1010
    TEXT("1, 2, 11 MBits"),		// 1011
    
	TEXT("5.5 and 11 MBits"),		// 1100
    TEXT("1, 5.5, 11 MBits"),		// 1101
    TEXT("2, 5.5, 11 MBits"),		// 1110
    TEXT("1,2,5.5,11 MBits"),		// 1111
    
	NULL
};

LPTSTR lpszValuesBeacon[] =
{
	TEXT("PSP 1"),
	TEXT("PSP 2"),
	TEXT("PSP 3"),
	TEXT("PSP 4"),
	TEXT("PSP 5"),
	TEXT("PSP 6"),
	TEXT("PSP 7"),
	TEXT("PSP 8"),
	TEXT("PSP 9"),
    TEXT("PSP 10"),
	TEXT("PSP 11"),
	TEXT("PSP 12"),
	NULL
};

LPTSTR lpszValuesPowerIndex[] =
{
	TEXT("PI 0"),
	TEXT("PI 1"),
	TEXT("PI 2"),
	TEXT("PI 3"),
	TEXT("PI 4"),
	TEXT("PI 5"),
	NULL
};

LPTSTR lpszFunctionMode[] =
{
    TEXT("MU"),
    TEXT("MicroAP"),
    NULL
};

LPTSTR lpszAdapterType[] =
{
    TEXT("PCMCIA"),
    TEXT("ISA"),
    NULL
};

LPTSTR lpszFirmwareType[] =
{
    TEXT("Spring"),
    TEXT("802.11"),
    NULL
};

LPTSTR lpszAssociated[] =
{
    TEXT("No"),
    TEXT("Yes"),
    NULL
};

LPTSTR lpszValuesDisableEnable[] = 
{
	TEXT("Disable"),
	TEXT("Enable"),
	NULL
};

// S24 Network Functions

/////////////////////////////////////////////////////////
// --- Main Menu ---
ITEMSELECTOR ConfigS24Selector[] =
{
    // view S24 Configuration
	{
		NULL,
		RESOURCE_STRING(IDS_VIEWCONFIG),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterViewConfig,ExitViewConfig)
    },
    // set S24 related (set registry as well)
	// This including: ESSID, PowerMode, Function Mode, BeaconParamter,
    // Diversity, and Supported Data Rate.
    {
		NULL,
		RESOURCE_STRING(IDS_SETS24),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterSetS24,ExitSetS24)
    },
    
    // This area let user view the signal stregth, network connection, and roaming count
	{
		NULL,
		RESOURCE_STRING(IDS_S24SIGNAL),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterS24Signal,ExitS24Signal)
    },
    
    // This area let user set Ping infomation (ICMP and WNMP) and perform a ping test
	{
		NULL,
		RESOURCE_STRING(IDS_S24PINGTEST),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterS24PingTest,ExitS24PingTest)
    },
    // This area let user set different network stuff
    // including: DHCP, IP, Subnet, GateWay, DNS, and WINS    
	{
		NULL,
		RESOURCE_STRING(IDS_S24NETWORK),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterS24Network,ExitS24Network)
	},
    // This area lets user set WEP encryption algoritm, index, and keys
	{
		NULL,
		RESOURCE_STRING(IDS_S24WEP),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterS24WEP,ExitS24WEP)
	},

	END_ITEMSELECTOR
};

// View Config
ITEMSELECTOR ViewConfigSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_S24DRIVERVERSION),
		NO_HELP,
    	GET_STRING(ShowDriverVersion,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24FIRMWAREVERSION),
		NO_HELP,
    	GET_STRING(ShowFirmwareVersion,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24IEEEADDRESS),
		NO_HELP,
    	GET_STRING(ShowMUAddress, NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24ESSID),
		NO_HELP,
    	GET_STRING(ShowESSID,NULL)
    },
	{
		&bFH,		//show power mode setting only for FH driver
		RESOURCE_STRING(IDS_S24POWERMODE),
		NO_HELP,
    	GET_STRING(ShowPowerMode,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24DIVERSITY),
		NO_HELP,
    	GET_STRING(ShowDiversity,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24SUPPORTEDRATE),
		NO_HELP,
    	GET_STRING(ShowDataRate,NULL)
    },
// if Trilogy use PowerPerformabe Index,
// else use BeaconAlgorithm
	{
        &bDS,
		RESOURCE_STRING(IDS_S24PERFORMANCEINDEX),
		NO_HELP,
		GET_STRING(ShowPowerIndex,NULL)
    },
	{
		&bFH,		//show BeaconAlgorithm setting only for FH driver
		RESOURCE_STRING(IDS_S24BEACONALGORITHM),
		NO_HELP,
    	GET_STRING(ShowBeaconAlgorithm,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24FUNCTIONMODE),
		NO_HELP,
    	GET_STRING(ShowFunctionMode,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24COUNTRYTEXT),
		NO_HELP,
    	GET_STRING(ShowCountryText,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24ADAPTERTYPE),
		NO_HELP,
    	GET_STRING(ShowAdapterType,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24FIRMWARETYPE),
		NO_HELP,
    	GET_STRING(ShowFirmwareType,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24ASSOCIATION),
		NO_HELP,
    	GET_STRING(ShowAssociation,NULL)
    },
	{
		&bAssociated,		// show speed only if associated
		RESOURCE_STRING(IDS_S24LINKSPEED),
		NO_HELP,
    	GET_STRING(ShowLinkSpeed,NULL)
    },
	{
		&bAssociated,		// show quality only if associated
		RESOURCE_STRING(IDS_S24QUALITY),
		NO_HELP,
    	GET_STRING(ShowQuality,NULL)
    },

	END_ITEMSELECTOR
};

ITEMSELECTOR SetS24Selector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_S24ESSID),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetESSID,SetESSID)
    },
	{
        NULL,
		RESOURCE_STRING(IDS_S24DIVERSITY),
		NO_HELP,
		STRING_LIST(lpszValuesNoYes,GetDiversity,SetDiversity,NULL)
    },
	{
		&bFH,		//show power mode setting only for FH driver
		RESOURCE_STRING(IDS_S24POWERMODE),
		NO_HELP,
		STRING_LIST(lpszValuesPSPCAM,GetPowerMode,SetPowerMode,NULL)
    },

	{
        &bFH,		//show Beacon ALgorithm only for FH driver
		RESOURCE_STRING(IDS_S24BEACONALGORITHM),
		NO_HELP,
		STRING_LIST(lpszValuesBeacon,GetBeaconAlgorithm,SetBeaconAlgorithm,NULL)
    },

	{
        &bDS,
		RESOURCE_STRING(IDS_S24PERFORMANCEINDEX),
		NO_HELP,
		STRING_LIST(lpszValuesPowerIndex,GetPowerIndex,SetPowerIndex,NULL)
    },

	{
		NULL,
		RESOURCE_STRING(IDS_S24SUPPORTEDRATE),
		NO_HELP,
		STRING_LIST(lpszValuesRateControl,GetRateControl,SetRateControl,NULL)
    },
	{
		&bFuncMode,		// show only if func mode supported
		RESOURCE_STRING(IDS_S24FUNCTIONMODE),
		NO_HELP,
		STRING_LIST(lpszFunctionMode,GetFunctionMode,SetFunctionMode,NULL)
    },
	END_ITEMSELECTOR
};

ITEMSELECTOR S24PingTestSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_WNMPPING),
		NO_HELP,
// this test will send ping info the associated AP address and report the result
// setting can be set via the PingSetting
		STRUCTURE_LIST(EnterWNMPPing,ExitWNMPPing)
//   		STRING_EDIT(TEXT(""),GetWNMPPing,SendWNMPPing)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_ICMPPING),
		NO_HELP,
// this test will usw ICMP to ping to the preselected IP address and report the result
// setting can be set via the PingSetting
		STRUCTURE_LIST(EnterICMPPing,ExitICMPPing)
//   		STRING_EDIT(TEXT(""),GetICMPPing,SendICMPPing)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_ICMPPINGSETTING),
        NO_HELP,
		STRUCTURE_LIST(EnterPingSetting,ExitPingSetting)
//   		STRING_EDIT(TEXT(""),GetPingSetting,SetPingSetting)
    },
	END_ITEMSELECTOR
};

ITEMSELECTOR S24PingSettingSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_PINGADDRESS),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetPingAddress,SetPingAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_PINGSIZE),
		NO_HELP,
		// select data size from the list (128, 516, 1024, 2048, 4096)
		STRING_LIST(lpszPingDataSize,GetPingSize,SetPingSize,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_PINGCOUNT),
        NO_HELP,
		// get the ping count from the list(3, 10, 20, 100, 500, forever)
		STRING_LIST(lpszPingCount,GetPingCount,SetPingCount,NULL)
    },
	END_ITEMSELECTOR
};
// --- S24 System ---

// --- S24 Network

ITEMSELECTOR S24NetworkSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_S24DHCP),
		NO_HELP,
		STRING_LIST(lpszValuesDisableEnable,GetDHCP,SetDHCP,NULL)
    },
	{
		&g_bNoDHCP,
		RESOURCE_STRING(IDS_S24MUIP),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetMUIPAddress,SetMUIPAddress)
    },
	{
		&g_bNoDHCP,
		RESOURCE_STRING(IDS_S24SUBNETMASK),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetSubnetMask,SetSubnetMask)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24PDNS),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetPDNSAddress,SetPDNSAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24SDNS),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetSDNSAddress,SetSDNSAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24PWINS),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetPWINSAddress,SetPWINSAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24SWINS),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetSWINSAddress,SetSWINSAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24GATEWAY),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetPGatewayAddress,SetPGatewayAddress)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24SGATEWAY),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetSGatewayAddress,SetSGatewayAddress)
    },
	END_ITEMSELECTOR
};


ITEMSELECTOR S24WEPSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_S24WEPALGORITHM),
		NO_HELP,
		STRING_LIST(lpszValuesNoneLowHigh,GetWEPAlgorithm,SetWEPAlgorithm,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24WEPINDEX),
		NO_HELP,
		STRING_RANGE(lpszRange1to4by1,GetWEPIndex,SetWEPIndex)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_S24WEPKEYS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterS24WEPKeys,ExitS24WEPKeys)
	},
	END_ITEMSELECTOR
};


ITEMSELECTOR S24WEPKeysSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_S24KEY1),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetWEPKey1,SetWEPKey1)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24KEY2),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetWEPKey2,SetWEPKey2)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24KEY3),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetWEPKey3,SetWEPKey3)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_S24KEY4),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetWEPKey4,SetWEPKey4)
    },
#ifdef SYMBOL_ACCESS_CODE
	{
		NULL,
		RESOURCE_STRING(IDS_S24ACCESSCODE),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetSerialKey,SetSerialKey)
    },
#endif
	END_ITEMSELECTOR
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CFGS24_H_    */
