
//--------------------------------------------------------------------
// FILENAME:        DateTime.h
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for DataTime.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef DATETIME_H_

#define DATETIME_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

static LPTSTR lpszTimeZoneInfo[] =
{
	TEXT("GMT -12:00"),			//0
	TEXT("GMT -11:00"),
	TEXT("GMT -10:00"),
	TEXT("GMT -09:00"),
	TEXT("GMT -08:00"),
	TEXT("GMT -07:00"),
	TEXT("GMT -06:00"),
	TEXT("GMT -05:00"),
	TEXT("GMT -04:00"),
	TEXT("GMT -03:00"),
	TEXT("GMT -02:00"),
	TEXT("GMT -01:00"),
	TEXT("GMT +00:00"),			//12
	TEXT("GMT +01:00"),
	TEXT("GMT +02:00"),
	TEXT("GMT +03:00"),
	TEXT("GMT +04:00"),
	TEXT("GMT +05:00"),
	TEXT("GMT +06:00"),
	TEXT("GMT +07:00"),
	TEXT("GMT +08:00"),
	TEXT("GMT +09:00"),
	TEXT("GMT +10:00"),
	TEXT("GMT +11:00"),
	TEXT("GMT +12:00"),			//24
	NULL
};

#define	STANDARDTIME		TEXT("Standard Time")
#define DAYLIGHTTIME		TEXT("Daylight Time")
#define FILETIME_TO_MINUTES	((_int64) 600000000L)
#define TIMEZONE			TEXT("Time")
#define TIMER_EVENT			1001

void EnterSetDateTime(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitSetDateTime(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetCurrentTimeZone(LPITEMSELECTOR lpis);
void SelectTimeZone(LPITEMSELECTOR lpis,DWORD dwSelection);


ITEMSELECTOR DateTimeSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_SELECTEDTIMEZONE),
		NULL,
		STRING_LIST(lpszTimeZoneInfo,GetCurrentTimeZone,SelectTimeZone,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SETDATETIME),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterSetDateTime,ExitSetDateTime)
    },
	END_ITEMSELECTOR
};

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef DATETIME_H_  */
