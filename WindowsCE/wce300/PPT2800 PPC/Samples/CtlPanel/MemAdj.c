//--------------------------------------------------------------------
// FILENAME:            MemAdj.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains control panel Memory adjustment dialog functions
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

#include "ctlpanel.h"
#include "choices.h"

#include "values.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _MEMORY_ADJUSTMENT

#include <memory32.h>

#define MEMORY_UPDATE		101		// for timer

static HWND			g_hTotalStore, g_hTotalRam;
static DWORD		g_dwDeltaPages;
static DWORD		g_dwStorePages;
static MEMSTRUCT	mst;

static DIALOG_CHOICE MemoryDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_MEMORY_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_MEMORY_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_MEMORY_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_MEMORY_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_MEMORY_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_MEMORY_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_MEMORY_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_MEMORY_7500_STD,	NULL,	UI_STYLE_DEFAULT },
};


void MemUpdateRegistry(DWORD dwStorePages)
{
	HKEY	hKey;
	DWORD	dwRetCode;
	DWORD	dwDisp;

	// open the key, if not there, create one.
	dwRetCode = RegCreateKeyEx( HKEY_CURRENT_USER,TEXT("Software\\Symbol\\SymShell\\Settings"),
								0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 
								NULL, &hKey, &dwDisp);

    if (dwRetCode == ERROR_SUCCESS)
    {
        dwRetCode = RegSetValueEx(hKey,TEXT("SystemStorePages"),0,REG_DWORD,
					(LPBYTE)&dwStorePages,sizeof(DWORD));
	
		if (dwRetCode != ERROR_SUCCESS)
			MyError(TEXT("Errer Set Key"),TEXT("SystemStorePages"));

        RegCloseKey(hKey);
			
		if (l_dwPersistence == PERSISTENCE_PERMANENT)
		{
			dwRetCode = WriteRegFile(TEXT("Software\\Symbol\\SymShell\\Settings"),
	   								 TEXT("SystemStorePages"),
	    							 REG_DWORD,
		    						 &dwStorePages,
			    					 sizeof(DWORD),
									 1);	// on HKCU

			if (dwRetCode != ERROR_SUCCESS)
			{
				ReportError(TEXT("ERROR WriteReg"),dwRetCode);
			}
		}

	}

}

void UpdatePosition (DWORD dwPosition)
{

	DWORD	dwSize;
	TCHAR	szText[MAX_PATH];

	dwSize = dwPosition * g_dwDeltaPages * mst.dwPageSize / 1024;

	wsprintf(szText,TEXT("%dKB"),dwSize);
	Static_SetText(g_hTotalStore,szText);

	dwSize = ( ( mst.dwStorePages + mst.dwRamPages ) 
			   - ( dwPosition * g_dwDeltaPages) ) 
			  * mst.dwPageSize / 1024;

	wsprintf(szText,TEXT("%dKB"),dwSize);
	Static_SetText(g_hTotalRam,szText);

}

// Initialize the scroll bar's range (min to max), page, and current position
// hwnd		-- handle of the scroll bar
// nMin		-- Minimum position
// nMax		-- Maximum position
// nPage	-- Page size
// nPos		-- Current position
void InitEditScrollBar(HWND hwnd, int nMin, int nMax, int nPage, int nPos)
{

	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };

	si.nMin = nMin;

	si.nPage = nPage;

	// set to nMax + nPage - 1 to allow 
	si.nMax = nMax + si.nPage - 1;

	si.nPos = nPos;

	SetScrollInfo(hwnd,SB_CTL,&si,TRUE);
}


void HandleEditScrollBars(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	HWND hSB = (HWND)lParam;
	int nNewPos = -1;
	int nPage;
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };

	switch(LOWORD(wParam))
	{
		case SB_LINELEFT:
		case SB_LINERIGHT:
		case SB_PAGELEFT:
		case SB_PAGERIGHT:
			
			GetScrollInfo(hSB,SB_CTL,&si);
			
			switch(LOWORD(wParam))
			{
				case SB_LINELEFT:
				case SB_LINERIGHT:
					nPage = 1;
					break;
				case SB_PAGELEFT:
				case SB_PAGERIGHT:
					nPage = si.nPage;
					break;
			}

			switch(LOWORD(wParam))
			{
				case SB_LINELEFT:
				case SB_PAGELEFT:
					si.nPos -= nPage;
					if ( si.nPos < si.nMin )
					{
						si.nPos = si.nMin;
					}
					break;
				case SB_LINERIGHT:
				case SB_PAGERIGHT:
					si.nPos += nPage;
					if ( si.nPos > si.nMax )
					{
						si.nPos = si.nMax;
					}
					break;
			}

			si.fMask = SIF_POS;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);
							
			nNewPos = si.nPos;
					
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:

			nNewPos = HIWORD(wParam);

			si.fMask = SIF_POS;
			si.nPos = nNewPos;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);

			break;
					
		case SB_ENDSCROLL:

			GetScrollInfo(hSB,SB_CTL,&si);
						
			si.fMask = SIF_POS;
			SetScrollInfo(hSB,SB_CTL,&si,TRUE);

			nNewPos = si.nPos;
	}

	if ( nNewPos != -1 )
	{
		UpdatePosition(nNewPos);
	}

}


//----------------------------------------------------------------------------
// MemoryDlgProc
//----------------------------------------------------------------------------

static LRESULT CALLBACK MemoryDlgProc( HWND hwnd,
									   UINT uMsg,
									   WPARAM wParam,
									   LPARAM lParam)
{

	static HWND hMemBar, hUsedStore, hUsedRam;
	static int nMax, nMin, nPage, nPos;
	static BOOL bNoScrollBar = FALSE;
	DWORD dwResult;
	TCHAR szText[MAX_PATH];
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };
//	LPINT	lpInt;
	static LPSCROLLINFO lpsi;
    
    switch(uMsg)
    {
		case UM_RESIZE:
			
			lpsi = (LPSCROLLINFO)LocalAlloc(LPTR,sizeof(SCROLLINFO));
			// get the current pos
			lpsi->cbSize = sizeof (SCROLLINFO);
			lpsi->fMask = SIF_ALL;

			GetScrollInfo(hMemBar,SB_CTL,lpsi);
			// Resize this dialog, no save info required
			ResizeDialog(lpsi);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			//hFont = SetNewDialogFont(hwnd,100);

			MaxDialog(hwnd);
			
			// display the current setting
			g_hTotalStore	= GetDlgItem(hwnd,IDC_TOTAL_STORE);
			g_hTotalRam		= GetDlgItem(hwnd, IDC_TOTAL_RAM);
			hUsedStore		= GetDlgItem(hwnd, IDC_USED_STORE);
			hUsedRam		= GetDlgItem(hwnd, IDC_USED_RAM);

			// check if we have ScrollBar
			hMemBar = GetDlgItem(hwnd,IDC_SCROLLBAR1);
				
			if (hMemBar == NULL)
			{
				bNoScrollBar = TRUE;
			}

			SI_ALLOC_ALL(&mst);
			mst.StructInfo.dwUsed = 0;

			dwResult = GetMemoryDivision(&mst);

			if (dwResult != E_MEM_SUCCESS)
			{
				ReportError(TEXT("Error GetMemoryDivision"),dwResult);
				break;
			}
			// set the delta page size to 64KB
			if (mst.dwPageSize > 1024)
			{
				g_dwDeltaPages = 16;
			}
			else
			{
				g_dwDeltaPages = 64;
			}

			lpsi = (LPSCROLLINFO)GetDialogSaveInfo();
			
			if (lpsi == NULL)	// no resize
			{
				l_bDialogWasOKed = FALSE;


				nMin = mst.dwUsedStoreSize / (mst.dwPageSize * g_dwDeltaPages);
					
				nMax = ( mst.dwRamPages + mst.dwStorePages ) / g_dwDeltaPages 
					   - (mst.dwUsedRamSize / (mst.dwPageSize * g_dwDeltaPages) );

				nPos = mst.dwStorePages / g_dwDeltaPages;

				nPage = 5;

				if (!bNoScrollBar)
					InitEditScrollBar(hMemBar, nMin, nMax, nPage, nPos);

				wsprintf(szText,TEXT("%dKB"), (mst.dwStorePages * mst.dwPageSize / 1024)); 
				Static_SetText(g_hTotalStore,szText);

				wsprintf(szText,TEXT("%dKB"), (mst.dwRamPages * mst.dwPageSize / 1024)); 
				Static_SetText(g_hTotalRam,szText);
			}
			else
			{
				lpsi->fMask = SIF_ALL;
				SetScrollInfo(hMemBar,SB_CTL,lpsi,TRUE);
				UpdatePosition(lpsi->nPos);
				LocalFree(lpsi);
			}

				
			// update the position if necessary
			GetMemoryDivision(&mst);
			wsprintf(szText,TEXT("%dKB"), (mst.dwUsedStoreSize / 1024)); 
			Static_SetText(hUsedStore,szText);

			wsprintf(szText,TEXT("%dKB"), (mst.dwUsedRamSize / 1024)); 
			Static_SetText(hUsedRam,szText);

			if (!bNoScrollBar)
				SetFocus(hMemBar);

			SetWindowText(hwnd,l_szLastItem);

			// fire a time for memory usage update (every 3 seconds)
			SetTimer(hwnd,MEMORY_UPDATE, 3000, NULL);

			// Return FALSE since focus was manually set
			return(FALSE);
        
       case WM_HSCROLL:
			HandleEditScrollBars(hwnd,wParam,lParam);
			break;

	   case WM_KEYDOWN:
			switch (wParam)
			{
				case VK_UP:
					SendMessage(hwnd,WM_HSCROLL,SB_PAGERIGHT,(LPARAM)hMemBar);
					break;
				case VK_DOWN:
					SendMessage(hwnd,WM_HSCROLL,SB_PAGELEFT,(LPARAM)hMemBar);
					break;
				case VK_LEFT:
					SendMessage(hwnd,WM_HSCROLL,SB_LINELEFT,(LPARAM)hMemBar);
					break;
				case VK_RIGHT:
					SendMessage(hwnd,WM_HSCROLL,SB_LINERIGHT,(LPARAM)hMemBar);
					break;
				default:
					break;
			}
			break;

       case WM_TIMER:
			if (wParam == MEMORY_UPDATE)
			{
				// update the position if necessary
				GetMemoryDivision(&mst);

				wsprintf(szText,TEXT("%dKB"), (mst.dwUsedStoreSize / 1024)); 
				Static_SetText(hUsedStore,szText);

				wsprintf(szText,TEXT("%dKB"), (mst.dwUsedRamSize / 1024)); 
				Static_SetText(hUsedRam,szText);
			}
			break;

       case WM_COMMAND:

			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				// simulate scroll page up
				case IDC_UP:
					SendMessage(hwnd,WM_HSCROLL,SB_PAGERIGHT,(LPARAM)hMemBar);
					break;
				case IDC_DOWN:
					SendMessage(hwnd,WM_HSCROLL,SB_PAGELEFT,(LPARAM)hMemBar);
					break;
				case IDC_LEFT:
					SendMessage(hwnd,WM_HSCROLL,SB_LINELEFT,(LPARAM)hMemBar);
					break;
				case IDC_RIGHT:
					SendMessage(hwnd,WM_HSCROLL,SB_LINERIGHT,(LPARAM)hMemBar);
					break;

				case IDC_ADVANCE:
					SendMessage(hwnd,WM_HSCROLL,SB_LINERIGHT,(LPARAM)hMemBar);
					break;

				case IDC_RETARD:
					SendMessage(hwnd,WM_HSCROLL,SB_LINELEFT,(LPARAM)hMemBar);
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

								InvokePopUpMenu(hwnd,IDM_POPUP,2,0,0);
							
							break;

						case UI_STYLE_PEN:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;

				case IDC_HELP_HELP:
					
					break;

				case IDOK:
					l_bDialogWasOKed = TRUE;
					KillTimer(hwnd,MEMORY_UPDATE);

					// get the current pos
					si.cbSize = sizeof (SCROLLINFO);
					GetScrollInfo(hMemBar,SB_CTL,&si);
					g_dwStorePages = si.nPos * g_dwDeltaPages;

					dwResult = SetMemoryDivision(g_dwStorePages);
					if (dwResult == SYSMEM_FAILED)
					{
						MyError(TEXT("Error SetMemoryDivision"),TEXT("Memory Adjustment"));
					}
					else
						if (dwResult == SYSMEM_MUSTREBOOT) 
							MyError(TEXT("Reboot to take effect"),TEXT("Memory Adjustment"));
						else
						{
							// update symshell registry setting to maintain consitnece
							MemUpdateRegistry(g_dwStorePages);
						}
					// Fall through

				case IDCANCEL:

					KillTimer(hwnd,MEMORY_UPDATE);

					// Exit the dialog
					if (ExitDialog())
					{
						if ( l_lpExitFunction != NULL )
						{
							(*l_lpExitFunction)(l_lpItemSelector,l_dwIndex);
						}

					}
					break;

				case IDDONE:
					SendMessage(g_hwnd,WM_CLOSE,0,0L);
					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}

void EnterMemoryAdjustment(LPITEMSELECTOR lpis,
							 DWORD dwSelection)
{
	LoadString(g_hInstance,IDS_MEMORYADJUSTMENT,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								MemoryDialogChoices,
								g_hwnd,
								MemoryDlgProc,
								0L);
}


void ExitMemoryAdjustment(LPITEMSELECTOR lpis,
				DWORD dwSelection)
{
	if ( l_bDialogWasOKed )
	{

	}

	l_bDialogWasOKed = FALSE;
}

#endif
