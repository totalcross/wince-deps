
//--------------------------------------------------------------------
// FILENAME:        S24.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for S24 related files
//
// NOTES:           Public
//
//  to header
// 
//    Rev 1.2   Mar 14 2000 15:47:34   cheung
// Remove reference for PowerType
// 
//    Rev 1.1   Feb 23 2000 18:15:20   cheung
// Update copyright date.
// 
//    Rev 1.0   Dec 30 1999 11:31:52   cheung
// Initial revision.
//
// %End
//--------------------------------------------------------------------

#ifndef S24_H_

#define S24_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

#include <strucinf.h>

#include "S24SDK32.h"
#include "S24API.h"

#define S24_SIGNAL_EVENT	1
#define S24_ICMP_PING_EVENT	2
#define S24_WNMP_PING_EVENT	3

#define ONEMBIT		0
#define TWOMBIT		2
#define FIVEMBIT	4
#define ELEVENMBIT	8


#define REG_SLACE1_TCPIP	TEXT("Comm\\SLACE1\\Parms\\TcpIp")
#define REG_SLACE1_PARMS	TEXT("Comm\\SLACE1\\Parms")
#define SLACE1			TEXT("SLACE1")
#define SLA41ND41		TEXT("SLA41ND41")

#define IPADDRESS		TEXT("IpAddress")
#define SUBNETMASK		TEXT("Subnetmask")
#define WINS			TEXT("WINS")
#define DNS				TEXT("DNS")
#define GATEWAY			TEXT("DefaultGateway")
#define DHCP			TEXT("EnableDHCP")

#define DHCPIPADDRESS	TEXT("DhcpIpAddress")
#define DHCPSUBNETMASK	TEXT("DhcpSubnetmask")
#define DHCPWINS		TEXT("DhcpWINS")
#define DHCPDNS			TEXT("DhcpDNS")
#define DHCPGATEWAY		TEXT("DhcpDefaultGateway")


typedef struct tagS24INFO
{ 
    STRUCT_INFO StructInfo;

    // IEEE address
    BYTE bMUAddress[6];   // 00,a0,f8,76,b3,a5
    BYTE bAPAddress[6];   // 00,a0,f8,76,b3,a5

    // --- Versions ---
    // Firmware version & date string
    BYTE bFirmwareVersion[S24_VD_MAX_FW_INFO];
	BYTE bCountryText[36];
    BYTE bS24ESSID[32];         // maximum 32 ASCII chars

    DWORD dwDriverVersion;

    WORD nDiversity;            // on or off
    WORD nPowerMode;            // PSP or CAM
    WORD nPowerIndex;            // PSP or CAM
    WORD nAdapterType;          // PCMCIA or ISA
    WORD nFirmwareType;         // 802 or Spring
    
    WORD nPercentMissed;        // used for signal strength
    WORD nTxRetries;            //
    WORD nRoamCount;            // check new BSSID with Old BSSID
    WORD nLinkSpeed;            //
	WORD nCountryCode;
    
    WORD nBeaconAlgorithm;
	WORD nBeaconMin;
	WORD nBeaconMax;

	WORD n1MBit;
	WORD n2MBit;
	WORD n5MBit;
	WORD n11MBit;

    BOOL bMicroAP;              // MU or MAP
    BOOL bAssociated;    

} S24INFO;

typedef S24INFO FAR * LPS24INFO;

typedef struct tagS24SETTING
{ 
    STRUCT_INFO StructInfo;

    BYTE bS24ESSID[32];         // maximum 32 ASCII chars
    WORD nDiversity;            // on or off
    WORD nPowerMode;            // PSP or CAM
	WORD nPowerIndex;
    WORD nBeaconAlgorithm;
    WORD nBeaconMin;
    WORD nBeaconMax;
	WORD n1MBit;
	WORD n2MBit;
	WORD n5MBit;
	WORD n11MBit;
	BOOL bMicroAP;			// MU or MicroAP

} S24SETTING;

typedef S24SETTING FAR * LPS24SETTING;


typedef struct tagS24ICMPINFO
{ 
	// remote IP Address
	TCHAR szIPAddr[32];
	DWORD dwDataSize;
	DWORD dwPingCount;

} S24ICMPINFO;

typedef S24ICMPINFO FAR * LPS24ICMPINFO;

// all strings are in unicode, need to convert to ASICC 
// when calling network function
typedef struct tagS24NETWORKINFO
{ 
	TCHAR	szIPAddr[MAX_PATH128];			// terminal IP address for S24		
	TCHAR	szNetMask[MAX_PATH128];			// terminal Netmask
	TCHAR	szDNS[2][MAX_PATH128];			// 
	TCHAR	szWINS[2][MAX_PATH128];
	TCHAR	szGateway[2][MAX_PATH128];

	DWORD	dwDHCPEnable;

} S24NETWORKINFO;

typedef S24NETWORKINFO FAR * LPS24NETWORKINFO;


static LPTSTR lpszPingDataSize[] =
{
	TEXT("128"),
	TEXT("256"),
	TEXT("512"),
	TEXT("1024"),
	TEXT("2048"),
	TEXT("4096"),
	NULL
};

static LPTSTR lpszPingCount[] =
{
	TEXT("3"),
	TEXT("10"),
	TEXT("50"),
	TEXT("100"),
	TEXT("500"),
	TEXT("0"),	// forever
	NULL
};

            
static LPTSTR lpszRange1to4by1[] =
{
	RESOURCE_STRING(IDS_1),
	RESOURCE_STRING(IDS_4),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszValuesNoneLowHigh[] =
{
	RESOURCE_STRING(IDS_NONE_CLEAR),
	RESOURCE_STRING(IDS_LOW_40),
	RESOURCE_STRING(IDS_HIGH_128),
	NULL
};


// Ctl Config S24 
void EnterConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);

// Main Selection
void EnterViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterSetS24(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitSetS24(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterS24Network(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitS24Network(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterS24WEP(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitS24WEP(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterS24WEPKeys(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitS24WEPKeys(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection);

// View Config Functions
LPTSTR ShowDriverVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowFirmwareVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowMUAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowESSID(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowPowerMode(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowDiversity(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowDataRate(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowPowerIndex(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowFunctionMode(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowCountryText(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowAdapterType(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowPowerType(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowFirmwareType(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowAssociation(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowLinkSpeed(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR ShowQuality(LPITEMSELECTOR lpis,DWORD dwIndex);

// SetS24 Functions
void SetESSID(LPITEMSELECTOR lpis, DWORD dwSelection);
DWORD GetESSID(LPITEMSELECTOR lpis);
// S24 System Functions
void SetDiversity(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetDiversity(LPITEMSELECTOR lpis);
void SetPowerMode(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetPowerMode(LPITEMSELECTOR lpis);
void SetBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetBeaconAlgorithm(LPITEMSELECTOR lpis);
void SetPowerIndex(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetPowerIndex(LPITEMSELECTOR lpis);
void SetRateControl(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetRateControl(LPITEMSELECTOR lpis);
void SetFunctionMode(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetFunctionMode(LPITEMSELECTOR lpis);

void SetPingAddress(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetPingAddress(LPITEMSELECTOR lpis);
void SetPingSize(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetPingSize(LPITEMSELECTOR lpis);
void SetPingCount(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetPingCount(LPITEMSELECTOR lpis);

/////// Network functions
DWORD GetDHCP(LPITEMSELECTOR lpis);
void SetDHCP(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetSubnetMask(LPITEMSELECTOR lpis);
void SetSubnetMask(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetMUIPAddress(LPITEMSELECTOR lpis);
void SetMUIPAddress(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetPDNSAddress(LPITEMSELECTOR lpis);
void SetPDNSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetPWINSAddress(LPITEMSELECTOR lpis);
void SetPWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetPGatewayAddress(LPITEMSELECTOR lpis);
void SetPGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex);


DWORD GetSDNSAddress(LPITEMSELECTOR lpis);
void SetSDNSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetSWINSAddress(LPITEMSELECTOR lpis);
void SetSWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetSGatewayAddress(LPITEMSELECTOR lpis);
void SetSGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex);


DWORD GetWEPAlgorithm(LPITEMSELECTOR lpis);
void SetWEPAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetWEPIndex(LPITEMSELECTOR lpis);
void SetWEPIndex(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetWEPKey1(LPITEMSELECTOR lpis);
void SetWEPKey1(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetWEPKey2(LPITEMSELECTOR lpis);
void SetWEPKey2(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetWEPKey3(LPITEMSELECTOR lpis);
void SetWEPKey3(LPITEMSELECTOR lpis,DWORD dwIndex);

DWORD GetWEPKey4(LPITEMSELECTOR lpis);
void SetWEPKey4(LPITEMSELECTOR lpis,DWORD dwIndex);

#ifdef SYMBOL_ACCESS_CODE
DWORD GetSerialKey(LPITEMSELECTOR lpis);
void SetSerialKey(LPITEMSELECTOR lpis,DWORD dwIndex);
#endif


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CFGS24_H_    */
