//--------------------------------------------------------------------
// FILENAME:            DateTime.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains control panel Data and Time dialog functions
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

#include "ctlpanel.h"
#include "choices.h"

#ifdef _DATEANDTIME

#include "values.h"
#include "datetime.h"


DIALOG_CHOICE DateAndTimeDialogChoices[] =
{
#ifdef _ST_PC
	{ ST_PC_STD,	IDD_DATE_PC_STD,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_DATE_PPC_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_DATE_PPC_SIP,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_DATE_7200_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_DATE_7200_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_DATE_7500_STD,		NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_DATE_7500_TSK,		NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_DATE_7500_STD,		NULL,	UI_STYLE_DEFAULT },
};

SYSTEMTIME l_SystemTime;
TIME_ZONE_INFORMATION g_tzinfo;
DWORD l_TimeZoneID;


// -------------------------------------------------------------
//  FUNCTION: DoDateTimeChange
//
//  PROTOTYPE: void WINAPI DoDateTimeChange(LPNMDATETIMECHANGE lpChange);
//
//  PARAMETERS: lpChange: pointer to the DTP DateTimeChanged structure
//
//  DESCRIPTION: update the global system to the new setting.
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void WINAPI DoDateTimeChange(LPNMDATETIMECHANGE lpChange)
{
    // update time field
    if (IDC_TIMEPICKER == lpChange->nmhdr.idFrom)
    {
        l_SystemTime.wHour           = lpChange->st.wHour;
        l_SystemTime.wMilliseconds   = lpChange->st.wMilliseconds;
        l_SystemTime.wMinute         = lpChange->st.wMinute;
        l_SystemTime.wSecond         = lpChange->st.wSecond;
    }
    else    // update date field
    if (IDC_DATEPICKER == lpChange->nmhdr.idFrom)
    {
        l_SystemTime.wDay       = lpChange->st.wDay;
        l_SystemTime.wDayOfWeek = lpChange->st.wDayOfWeek;
        l_SystemTime.wMonth     = lpChange->st.wMonth;
        l_SystemTime.wYear      = lpChange->st.wYear;
    }
}


// -------------------------------------------------------------
//  FUNCTION: ChangePicker
//
//  PROTOTYPE: void ChangePicker(HWND hWndCtl, BYTE bVk);
//
//  PARAMETERS: hWndCtl: handle to the DateTimePicker
//				bVK: 
//
//  DESCRIPTION: Change the DTP focus
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void ChangePicker(HWND hWndCtl, BYTE bVk)
{
	if ( hWndCtl != GetFocus() )
	{
		SetFocus(hWndCtl);
	}
	SendMessage(hWndCtl,WM_KEYDOWN,bVk,0L);
}

//----------------------------------------------------------------------------
// DateAndTimeDlgProc
//----------------------------------------------------------------------------
static LRESULT CALLBACK DateAndTimeDlgProc(HWND hwnd,
										   UINT uMsg,
										   WPARAM wParam,
										   LPARAM lParam)
{
	static HWND hWndDate;
	static HWND hWndTime;
	LPINT	lpInt;
	static BOOL	bTimerKilled = FALSE;

    switch(uMsg)
    {
		case UM_RESIZE:
			
			lpInt = LocalAlloc(LPTR,sizeof(int));
			if (bTimerKilled)
				*lpInt = 1;
			else
				*lpInt = 0;
			// Resize this dialog, no save info required
			ResizeDialog(lpInt);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				HFONT hFont;
			
				switch(GetUIStyle())
				{
					case UI_STYLE_PEN:
						hFont = SetNewDialogFont(hwnd,120);
						break;
					case UI_STYLE_FINGER:
						hFont = SetNewDialogFont(hwnd,130);
						break;
					case UI_STYLE_KEYPAD:
						hFont = SetNewDialogFont(hwnd,110);
						break;
					default:
						hFont = SetNewDialogFont(hwnd,100);
						break;
				}

				MaxDialog(hwnd);

				hWndDate = GetDlgItem(hwnd,IDC_DATEPICKER);

				SendMessage(hWndDate,DTM_SETFORMAT,0,(LPARAM)TEXT("MMMM d,yyy"));

				hWndTime = GetDlgItem(hwnd,IDC_TIMEPICKER);

				SetWindowFont(hWndDate,hFont,FALSE);
				SetWindowFont(hWndTime,hFont,FALSE);

				SetWindowFont(GetDlgItem(hwnd,IDC_DATETEXT),hFont,FALSE);
				SetWindowFont(GetDlgItem(hwnd,IDC_TIMETEXT),hFont,FALSE);

				// Set focus to date picker
				SetFocus(hWndDate);
							
				SetWindowText(hwnd,l_szLastItem);

				// fire the timer to update the time
				lpInt = (LPINT)GetDialogSaveInfo();

				if (lpInt == NULL)
				{
					l_bDialogWasOKed = FALSE;
					SetTimer(hwnd,TIMER_EVENT,1000,NULL);
					bTimerKilled = FALSE;
				}
				else
				{
					// if we haven't get a change yet, restart the timer
					if (*lpInt==0)
					{
		            	// get uptodate date and time
						GetLocalTime(&l_SystemTime);
						// fire the timer
						SetTimer(hwnd,TIMER_EVENT,1000,NULL);
						bTimerKilled = FALSE;
					}

					// update the DateTimePicker control
					DateTime_SetSystemtime(hWndDate, GDT_VALID, &l_SystemTime);
	                DateTime_SetSystemtime(hWndTime, GDT_VALID, &l_SystemTime);
					LocalFree(lpInt);

				}

			}
			
			// Return FALSE since focus was manually set
			return(FALSE);
        
        case WM_NOTIFY:
        {
	        LPNMHDR hdr = (LPNMHDR) lParam;

        	switch (hdr->code)
	        {
	            case DTN_DATETIMECHANGE:
		        {
			        LPNMDATETIMECHANGE lpChange = (LPNMDATETIMECHANGE) lParam;
					if (!bTimerKilled)
					{
						KillTimer(hwnd,TIMER_EVENT);
						bTimerKilled = TRUE;
					}

			        DoDateTimeChange(lpChange);
		        }
		        break;
            }
            break;
        }

		case WM_TIMER:
			// update the date and time as long as user haven't change anything
            if(wParam == TIMER_EVENT)
            {
            	GetLocalTime(&l_SystemTime);
                DateTime_SetSystemtime(hWndDate, GDT_VALID, &l_SystemTime);
                DateTime_SetSystemtime(hWndTime, GDT_VALID, &l_SystemTime);
	            return (TRUE); 
            }
			break;

       case WM_COMMAND:

			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_UP:
					ChangePicker(hWndDate,VK_UP);
					break;
				case IDC_DOWN:
					ChangePicker(hWndDate,VK_DOWN);
					break;
				case IDC_LEFT:
					ChangePicker(hWndDate,VK_LEFT);
					break;
				case IDC_RIGHT:
					ChangePicker(hWndDate,VK_RIGHT);
					break;
				case IDC_UP2:
					ChangePicker(hWndTime,VK_UP);
					break;
				case IDC_DOWN2:
					ChangePicker(hWndTime,VK_DOWN);
					break;
				case IDC_LEFT2:
					ChangePicker(hWndTime,VK_LEFT);
					break;
				case IDC_RIGHT2:
					ChangePicker(hWndTime,VK_RIGHT);
					break;

				case IDC_ADVANCE:

					ChangePicker(GetFocus(),VK_RIGHT);

					break;

				case IDC_RETARD:

					ChangePicker(GetFocus(),VK_LEFT);

					break;

				case IDC_TOGGLE:

					SetFocus(hWndDate);

					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

							if ( GetFocus() == hWndDate )
							{
								SetFocus(hWndTime);
							}
							else
							{
								InvokePopUpMenu(hwnd,IDM_POPUP,2,0,0);
							}
							
							break;

						case UI_STYLE_PEN:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;

				case IDC_HELP_HELP:
					
					break;

				case IDOK:

					l_bDialogWasOKed = TRUE;

					// Fall through

				case IDCANCEL:

					KillTimer(hwnd,TIMER_EVENT);

					// Exit the dialog
					ExitDialog();
				
					if ( l_lpExitFunction != NULL )
					{
						(*l_lpExitFunction)(l_lpItemSelector,l_dwIndex);
					}

					break;

				case IDDONE:
					SendMessage(g_hwnd,WM_CLOSE,0,0L);
					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


// -------------------------------------------------------------
//  FUNCTION: GetDefaultTimeInfo
//
//  PROTOTYPE:BOOL GetDefaultTimeInfo(LPTIME_ZONE_INFORMATION lptzinfo);
//
//  PARAMETERS: lptzinfo: pointer to the timezone structure.
//
//  DESCRIPTION:Get the system default timezone structure
//
//  RETURNS: TRUE if success; FALSE otherwise
//
//  NOTES: 
// -------------------------------------------------------------
BOOL GetDefaultTimeInfo(LPTIME_ZONE_INFORMATION lptzinfo)
{
    DWORD dwLen, dwType, dwRetCode;
	HKEY	hKey;
	TIME_ZONE_INFORMATION	tzinfo;
	
	ZEROMEM(&tzinfo,sizeof(TIME_ZONE_INFORMATION));
	
	l_TimeZoneID = GetTimeZoneInformation(&tzinfo);
	// first we look at the registry to see do we have a entry in 
	// HKLM\Time\TimeZoneInformation

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,TIMEZONE,0,KEY_READ, &hKey);
	if (dwRetCode != ERROR_SUCCESS)
	{
		DWORD dwDisp;
		// we don't have the registry entry, use GetTimeZoneInformation to get the
		// system info and dump the binary to the registry
		// create the registry entry
		dwRetCode = RegCreateKeyEx( HKEY_LOCAL_MACHINE, TIMEZONE, 0, NULL, 
									REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 
									NULL, &hKey, &dwDisp);
		if (dwRetCode == ERROR_SUCCESS)
		{
			// set the key
			dwRetCode = RegSetValueEx(hKey,TEXT("TimeZoneInformation"),0,REG_BINARY,
									  (LPBYTE)&tzinfo,sizeof(tzinfo));
			
			RegCloseKey(hKey);
	        if (dwRetCode != ERROR_SUCCESS)
		    {
			    MyError(TEXT("Error Set Key"),TEXT("TimeZoneInfo"));
			    return FALSE;
			}
		}
		else
		{
			    MyError(TEXT("Error Create Key"),TEXT("TimeZoneInfo"));
			    return FALSE;
		}

	}
	else	// if we have one in the registry, use that one instead
	{
		// read the timezoneinfo from registry
		dwRetCode = RegQueryValueEx(hKey,TEXT("TimeZoneInformation"),NULL,
					&dwType, (LPBYTE)&tzinfo, &dwLen);
		RegCloseKey(hKey);
	}

	memcpy(lptzinfo,&tzinfo,sizeof(TIME_ZONE_INFORMATION));
	return TRUE;


}

// -------------------------------------------------------------
//  FUNCTION: UpdateTimeZoneInfo
//
//  PROTOTYPE: BOOL UpdateTimeZoneInfo(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Update timezone setting in the registry
//
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL UpdateTimeZoneInfo(void)
{
    DWORD	dwRetCode;
	HKEY	hKey;
	
	
	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,TIMEZONE,0,KEY_ALL_ACCESS, &hKey);

	if (dwRetCode == ERROR_SUCCESS)
	{
		DWORD dwRet;
		// set the key
		dwRetCode = RegSetValueEx(hKey,TEXT("TimeZoneInformation"),0,REG_BINARY,
								  (LPBYTE)&g_tzinfo,sizeof(g_tzinfo));
			
		RegCloseKey(hKey);
        if (dwRetCode != ERROR_SUCCESS)
	    {
		    MyError(TEXT("Error Set Key"),TEXT("TimeZoneInfo"));
		    return FALSE;
		}
		
		if (l_dwPersistence == PERSISTENCE_PERMANENT)
		{
			dwRet = WriteRegFile(TIMEZONE,
   							 TEXT("TimeZoneInformation"),
    						 REG_BINARY,
	    					 &g_tzinfo,
		    				 sizeof(g_tzinfo),
							 0);

			if (dwRet != ERROR_SUCCESS)
			{
				ReportError(TEXT("ERROR WriteReg"),dwRet);
			}
		}

	}

	return TRUE;
}

// -------------------------------------------------------------
//  FUNCTION: EnterDataAndTime
//
//  PROTOTYPE: void EnterDataAndTime(LPITEMSELECTOR lpis,
//										 DWORD dwSelection);
//
//  PARAMETERS:		lpis: pointer to the item selector
//					dwSelection: number of selection on the list
//
//  DESCRIPTION:	Entry point for the Timezone setting
//
//  RETURNS: None 
//
//  NOTES:the change on timezone and daylight should take effect 
//		  immediately to reject the changes, we must reset 
//		  it to the saved info;
// -------------------------------------------------------------
void EnterDateAndTime(LPITEMSELECTOR lpis, DWORD dwSelection)
{

	InitCtlPanel(lpis,dwSelection,IDS_DATEANDTIME);

// default to enable daylight saving and with EST timezone
	ZEROMEM(&g_tzinfo,sizeof(TIME_ZONE_INFORMATION));
	if (!GetDefaultTimeInfo(&g_tzinfo))
		MyError(TEXT("GetDefaultTimeZone Error"),TEXT("DateTime"));

    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)DateTimeSelector);
}


// -------------------------------------------------------------
//  FUNCTION: ExitDataAndTime
//
//  PROTOTYPE:void ExitDateAndTime(LPITEMSELECTOR lpis, DWORD dwSelection);
//
//  PARAMETERS:	lpis: pointer to the item selector
//			   	dwSelection: number of selection on the list
//
//  DESCRIPTION: Exit point for the Date Time setting
//
//  RETURNS:  None
//
//  NOTES:
// -------------------------------------------------------------
void ExitDateAndTime(LPITEMSELECTOR lpis, DWORD dwSelection)
{
	if ( l_bDialogWasOKed )
	{
    }

	
	l_bDialogWasOKed = FALSE;   

	l_lpExitFunction = NULL;

}


// -------------------------------------------------------------
//  FUNCTION: EnterSetDateTime
//
//  PROTOTYPE: void EnterSetDateTime(LPITEMSELECTOR lpis,
//										 DWORD dwSelection);
//
//  PARAMETERS:		lpis: pointer to the item selector
//					dwSelection: number of selection on the list
//
//  DESCRIPTION:	Entry point for the Date Time setting
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void EnterSetDateTime(LPITEMSELECTOR lpis,
							 DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_DATEANDTIME);

	{
		INITCOMMONCONTROLSEX IccEx =
		{
			sizeof(INITCOMMONCONTROLSEX),
			ICC_DATE_CLASSES
		};
		
		InitCommonControlsEx(&IccEx);
	}

	// fill up the structure first.
	GetLocalTime(&l_SystemTime);

	g_hdlg = CreateChosenDialog(g_hInstance,
								DateAndTimeDialogChoices,
								g_hwnd,
								DateAndTimeDlgProc,
								(LPARAM)&l_SystemTime);
}


// -------------------------------------------------------------
//  FUNCTION: ExitSetDateTime
//
//  PROTOTYPE:void ExitSetDateTime(LPITEMSELECTOR lpis, DWORD dwSelection);
//
//  PARAMETERS:	lpis: pointer to the item selector
//			   	dwSelection: number of selection on the list
//
//  DESCRIPTION: Exit point for the Date Time setting
//
//  RETURNS:  None
//
//  NOTES:
// -------------------------------------------------------------
void ExitSetDateTime(LPITEMSELECTOR lpis,
							DWORD dwSelection)
{
	if ( l_bDialogWasOKed )
	{
		if (!SetLocalTime(&l_SystemTime))
		ReportError(TEXT("Set System Time"),GetLastError());
	}

	l_bDialogWasOKed = FALSE;

	l_lpExitFunction = ExitDateAndTime;
	
}


// -------------------------------------------------------------
//  FUNCTION: GetCurrentTimeZone
//
//  PROTOTYPE: DWORD GetCurrentTimeZone(LPITEMSELECTOR lpis);
//
//  PARAMETERS:	lpis: pointer to the item selector
//
//  DESCRIPTION: Get the current timezone string
//
//  RETURNS: index to the timezone string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetCurrentTimeZone(LPITEMSELECTOR lpis)
{
	// returning the index into our string array
	return (DWORD)12 - (g_tzinfo.Bias / 60);
}

// -------------------------------------------------------------
//  FUNCTION: SelectTimeZone
//
//  PROTOTYPE:void SelectTimeZone(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS:	 lpis: pointer to the item selector
//				 dwSelection: number of selection on the list
//
//  DESCRIPTION: Set the timezone info
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void SelectTimeZone(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	LONG lNewBias, lOldBias;
	TCHAR	szTimeZoneName[MAX_PATH];
	SYSTEMTIME	OldSystemTime, NewSystemTime;
	_int64	OldFileTime, NewFileTime;

	// convert the index into bias.
	// we knew dwSelection 0 => GMT - 12 (720), 0 = GMT (0), 
	//	24 => GMT + 12 (-720)
	lNewBias = (LONG) (12 - dwSelection ) * 60 + g_tzinfo.StandardBias;
	
	lOldBias = g_tzinfo.Bias + g_tzinfo.StandardBias;

	// we need to update the name as well (both standard and daylight)
	LoadString(g_hInstance,IDS_TIMEZONENAME0 + dwSelection, szTimeZoneName, TEXTSIZEOF(szTimeZoneName));

	TEXTCAT(szTimeZoneName,TEXT(" "));
	TEXTCAT(szTimeZoneName,STANDARDTIME);

#ifdef UNICODE
	TEXTCPY(g_tzinfo.StandardName,szTimeZoneName);
#else
	mbstowcs(g_tzinfo.StandardName,szTimeZoneName,TEXTLEN(szTimeZoneName));
#endif

	LoadString(g_hInstance,IDS_TIMEZONENAME0 + dwSelection, szTimeZoneName, TEXTSIZEOF(szTimeZoneName));

	TEXTCAT(szTimeZoneName,TEXT(" "));
	TEXTCAT(szTimeZoneName,DAYLIGHTTIME);

#ifdef UNICODE
	TEXTCPY(g_tzinfo.DaylightName,szTimeZoneName);
#else
	mbstowcs(g_tzinfo.StandardName,szTimeZoneName,TEXTLEN(szTimeZoneName));
#endif
	
	GetLocalTime(&OldSystemTime);
	SystemTimeToFileTime(&OldSystemTime, (FILETIME*)&OldFileTime);

	// new local time => old local time + (OldBias - NewBias)
	NewFileTime = OldFileTime + (((_int64)(lOldBias - lNewBias))*FILETIME_TO_MINUTES);

	FileTimeToSystemTime((FILETIME*)&NewFileTime, &NewSystemTime);

	SetLocalTime(&NewSystemTime);
	
	// go set the timezoneinformation
	g_tzinfo.Bias = lNewBias;

	SetTimeZoneInformation(&g_tzinfo);
	// update the registry if necessary
	UpdateTimeZoneInfo();
}

#endif
