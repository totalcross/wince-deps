
//--------------------------------------------------------------------
// FILENAME:            S24Signal.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to show S24 signal strength, roaming count 
//					and netwrok range.
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>

// Symbol SDK common includes
#include "..\common\StdGlobs.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// Ctlpanel includes
#include "ctlpanel.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _S24CTL

#include "S24.h"


// progressbar habdle
static	HWND hWndQuality;
static 	HWND hWndRoaming;
static 	HWND hWndRange;
static	HWND hWndSignalCount;
static	HWND hWndAPMacAddr;
static	BOOL bInTimerTask = FALSE;
extern S24INFO S24Info;


void Update_Display(WORD nPos, WORD nRoamCount, BOOL bAssociated)
{
	TCHAR szText[MAX_PATH32];

	SendMessage(hWndQuality, PBM_SETPOS, nPos, 0); 
	
	wsprintf(szText,TEXT("%d %%"),nPos);
	Static_SetText(hWndSignalCount, szText);
	
	wsprintf(szText,TEXT("%d"),nRoamCount);
	Static_SetText(hWndRoaming, szText);
				
	if (bAssociated)
	{
		// S24Info.bAPAddress has the AP address
		wsprintf(szText,TEXT("%02X:%02X:%02X:%02X:%02X:%02X"),
							  S24Info.bAPAddress[0],
							  S24Info.bAPAddress[1],
							  S24Info.bAPAddress[2],
							  S24Info.bAPAddress[3],
							  S24Info.bAPAddress[4],
							  S24Info.bAPAddress[5]);

		Static_SetText(hWndAPMacAddr,szText);
		Static_SetText(hWndRange,TEXT("In Range"));
	}
	else
	{
		Static_SetText(hWndAPMacAddr,TEXT("00:00:00:00:00:00"));
		Static_SetText(hWndRange,TEXT("Out of Range"));
	}
	
	return;
}

void Update_S24Signal(void)
{

	if (S24_IsAssociated())
    {
		DWORD dwReturn;
		BYTE bAPIEEE[6];

		S24Info.bAssociated = TRUE;
        dwReturn = S24_GetAPMacAddress(&bAPIEEE[0]);
		if (dwReturn == RC_SUCCESS)
		{

		if (memcmp(&bAPIEEE, S24Info.bAPAddress, 6) != 0)
		{
			S24Info.nRoamCount++;
			// we need to update the AP address
			memcpy(S24Info.bAPAddress, &bAPIEEE, 6);	// always 6 bytes.
		}

		dwReturn = S24_GetChannelQualityValues(&S24Info.nPercentMissed,
                                           &S24Info.nTxRetries);
        if (dwReturn != RC_SUCCESS)
				return;
		}
		else
		{
			// got error, return
			return;
        }

	}
	else
	{
		// reset if not associated
		S24Info.nRoamCount = 0;
		S24Info.nPercentMissed = 100;
		S24Info.bAssociated = FALSE;
	}

	Update_Display((WORD)(100-S24Info.nPercentMissed), S24Info.nRoamCount, S24Info.bAssociated);
	
	return;
}


LRESULT CALLBACK S24SignalProc(HWND hwnd,
									  UINT uMsg,
									  WPARAM wParam,
									  LPARAM lParam)
{

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save info required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				HFONT hFont;
			
				switch(GetUIStyle())
				{
					case UI_STYLE_PEN:
						hFont = SetNewDialogFont(hwnd,120);
						break;
					case UI_STYLE_FINGER:
						hFont = SetNewDialogFont(hwnd,130);
						break;
					case UI_STYLE_KEYPAD:
						hFont = SetNewDialogFont(hwnd,110);
						break;
					default:
						hFont = SetNewDialogFont(hwnd,100);
						break;
				}

				MaxDialog(hwnd);
				
				hWndQuality = GetDlgItem(hwnd,IDC_S24SIGNALQUALITY);
				hWndRoaming = GetDlgItem(hwnd,IDC_S24ROAMING);
				hWndRange = GetDlgItem(hwnd,IDC_S24RANGE);
				hWndSignalCount = GetDlgItem(hwnd,IDC_SIGNALCOUNT);
				hWndAPMacAddr = GetDlgItem(hwnd, IDC_APMACADDR);

				// initial roamcount to 0
				S24Info.nRoamCount = 0;

				// get the first AP address
				S24_GetAPMacAddress(S24Info.bAPAddress);

				SendMessage(hWndQuality, PBM_SETRANGE, 0, MAKELPARAM(0, 100)); 
				Update_Display((WORD)(100-S24Info.nPercentMissed), S24Info.nRoamCount, S24Info.bAssociated);
				// Set focus to date picker
				SetFocus(hWndQuality);
				
				SetWindowText(hwnd,l_szLastItem);

				// use 200 to increase response time
				SetTimer(hwnd,S24_SIGNAL_EVENT, 200, NULL);
			}
			
			// Return FALSE since focus was manually set
			return(FALSE);
      
		case WM_TIMER:

			switch (wParam)
			{
			// process S24_EVENT
			case S24_SIGNAL_EVENT:
			/* This function is invoked by a timer task, should not execute it twice */
				if (!bInTimerTask)
				{
					bInTimerTask = TRUE;
					// update S24Quality, Roaming status, and association
					Update_S24Signal();
					bInTimerTask = FALSE;
				}
					
				break;
				
			}
			return 0;

		
        case WM_COMMAND:

			// Select acitivty based on command
			switch (LOWORD(wParam))
            {

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

								InvokePopUpMenu(hwnd,IDM_POPUP,3,0,0);	// back or refresh
							
							break;

						case UI_STYLE_PEN:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;

				case IDC_REFRESH:
					KillTimer(hwnd, S24_SIGNAL_EVENT);

					PostMessage(hwnd,WM_INITDIALOG,0L,0L);
					break;

				case IDOK:

					l_bDialogWasOKed = TRUE;	

					// Fall through

				case IDCANCEL:

					KillTimer(hwnd, S24_SIGNAL_EVENT);

					// Exit the dialog
					if (ExitDialog())
					{
						if ( l_lpExitFunction != NULL )
						{
							(*l_lpExitFunction)(l_lpItemSelector,l_dwIndex);
						}

					}
					break;

				case IDDONE:
					SendMessage(g_hwnd,WM_CLOSE,0,0L);
					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}

#endif
