
//--------------------------------------------------------------------
// FILENAME:            paneldlg.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains control panel dialog functions
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

// 
#include <winsock.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"
#include "..\common\StdAbout.h"
#include "..\common\StdScrns.h"
#include "..\common\Scan.h"

#include "paneldlg.h"

#include "ctlpanel.h"

#include "choices.h"
#include "values.h"


#ifdef _UUID

#include <rcmcapi.h>

#define UUID_CHUNK_SIZE 4

#define _TRCMAPI32						TEXT("RcmAPI32.dll")

#define _TRCMGETVERSION					TEXT("RCM_GetVersion")
#define _TRCMGETUNIQUEUNITID			TEXT("RCM_GetUniqueUnitId")
#define _TRCMGETUNIQUEUNITIDEX			TEXT("RCM_GetUniqueUnitIdEx")

typedef DWORD (WINAPI* LPFNRCMGETVERSION)(LPRCM_VERSION_INFO lpVersionInfo);
typedef DWORD (WINAPI* LPFNRCMGETUNIQUEUNITID)(LPUNITID lpUnitId);
typedef DWORD (WINAPI* LPFNRCMGETUNIQUEUNITIDEX)(LPUNITID_EX lpUnitIdEx);

BOOL l_bUUID0 = FALSE;
BOOL l_bUUID1 = FALSE;
BOOL l_bUUID2 = FALSE;
BOOL l_bUUID3 = FALSE;
BOOL l_bUUID4 = FALSE;
BOOL l_bUUID5 = FALSE;

#endif



// Global Variables -- Headers should include those as extern

TCHAR l_lpszHelpFileName[] = HELP_FILE_NAME;

LPEXITFUNCTION l_lpExitFunction = NULL;
LPITEMSELECTOR l_lpItemSelector = NULL;
BOOL l_dwNestCount = 0;
TCHAR l_szLastItem[MAX_PATH];
DWORD l_dwIndex = 0;
DWORD l_dwPersistence;
BOOL l_bDialogWasOKed = FALSE;
BOOL l_bUseWavFile = TRUE;
BOOL l_bUseS24 = FALSE;
BOOL l_bValidateDword = TRUE;
BOOL bReadOnly = FALSE;
TCHAR g_szRegFilePath[MAX_PATH128];


void ReportInvalidDword(LPTSTR lpszValue)
{
	HWND hwndFocus;
	
	hwndFocus = GetFocus();

	MyError(TEXT("Invalid Dword"),lpszValue);

	Edit_SetText(hwndFocus,lpszValue);
	Edit_SetSel(hwndFocus,0,-1);
	SetFocus(hwndFocus);
}


// retuen TRUE is the string is a valid DWORD;
BOOL IsValidDword(LPTSTR lpszText)
{
	int nLen;
	int i;

	if ( lpszText == NULL )
	{
		return(FALSE);
	}
	
	nLen = TEXTLEN(lpszText);

	if ( nLen == 0 )
	{
		return(FALSE);
	}

	for(i=0;i<nLen;i++)
	{
		TCHAR c = lpszText[i];
		c = TOUPPER(c);
        if ( ( c < TEXT('0') ) || ( c > TEXT('9') ) && ( c < TEXT('A')) || ( c > TEXT('F')) )
        //if ( ( c < TEXT('0') ) || ( c > TEXT('9') ) )
        {
			return(FALSE);
		}
	}

	return(TRUE);
}

// Get the currently selected item index of a list view

int ListView_GetSelectedItem(HWND hWndList)
{
	int iItem;
	int nState;
	
	iItem = ListView_GetNextItem(hWndList,-1,UI_SELECTED);

	nState = ListView_GetItemState(hWndList,iItem,UI_SELECTED);

	return(iItem);
}


// Get the currently selected item parameter of a list view

LPARAM ListView_GetSelectedParam(HWND hWndList)
{
	int iItem;
	LV_ITEM lvI;

	iItem = ListView_GetSelectedItem(hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	return(lvI.lParam);
}


// Get the currently selected item text of a list view

void ListView_GetSelectedText(HWND hWndList,
							  LPTSTR lpszText,
							  DWORD dwSize)
{
	int iItem;

	iItem = ListView_GetSelectedItem(hWndList);

	ListView_GetItemText(hWndList,iItem,0,lpszText,dwSize);
}


// Select an item in a list view

void ListView_SelectItem(HWND hWndList,
						 int iItem)
{
	SetFocus(hWndList);

	ListView_EnsureVisible(hWndList,iItem,FALSE);
	
	ListView_SetItemState(hWndList,
						  iItem,
						  LVIS_SELECTED|LVIS_FOCUSED,
						  LVIS_SELECTED|LVIS_FOCUSED);
	
	ListView_Update(hWndList,iItem);
}


LPTSTR FetchStringResource(LPTSTR lpszString)
{
	static TCHAR szString[MAX_PATH128];
	DWORD dwId = (DWORD)lpszString;

	if ( HIWORD(dwId) == 0 )
	{
		LoadString(g_hInstance,dwId,szString,TEXTSIZEOF(szString));
		return(szString);
	}
	else
	{
		return(lpszString);
	}
}


void EnterStructure(LPITEMSELECTOR lpis,
						   DWORD dwSelection)
{
	if ( ( lpis != NULL ) &&
		 ( lpis->lpEnterFunction != NULL ) )
	{
		(*lpis->lpEnterFunction)(lpis,dwSelection);
	}
}

// get the text from the list
LPTSTR GetTextItem(LPITEMSELECTOR lpis,
						  DWORD dwIndex)
{
	LPTSTR lpszText = NULL;

	if ( lpis->lplpszParamValuesList != NULL )
	{
		lpszText = lpis->lplpszParamValuesList[dwIndex];
		lpszText = FetchStringResource(lpszText);
	}

	return(lpszText);
}


LPTSTR GetEditText(LPITEMSELECTOR lpis,
						  DWORD dwIndex)
{
	return(lpis->szText);
}


DWORD GetEditCount(LPITEMSELECTOR lpis)
{
	return(1);
}


BOOL ReadLine(HANDLE hFile,
					 LPTSTR lpszLine,
					 DWORD dwSize,
					 LPDWORD lpdwBytesRead)
{
	*lpdwBytesRead = 0;
	
	while ( dwSize > 0 )
	{
		DWORD dwBytesRead;

		if ( ReadFile(hFile,lpszLine,1,&dwBytesRead,FALSE) &&
			 ( dwBytesRead == 1 ) )
		{
			if ( *lpszLine == TEXT('\n') )
			{
				(*lpdwBytesRead)++;
				lpszLine++;
				dwSize--;
				break;
			}
			
			(*lpdwBytesRead)++;
			lpszLine++;
			dwSize--;
		}
		else
		{
			return(FALSE);
		}
	}

	*lpszLine = TEXT('\0');

	return(TRUE);
}


BOOL FindSection(HANDLE hFile,
						LPTSTR lpszText,
						DWORD dwSize,
						LPTSTR lpszSection)
{
	DWORD dwBytesRead;

	while ( ReadLine(hFile,lpszText,dwSize,&dwBytesRead) )
	{
		DWORD dwLength = TEXTLEN(lpszText);

		if ( ( lpszText[0] == TEXT('[') ) &&
			 ( lpszText[dwLength-3] == TEXT(']') ) )
		{
			if ( TEXTNCMP(lpszText+1,
						  lpszSection,
						  max(TEXTLEN(lpszSection),dwLength-4)) == 0 )
			{
				return(TRUE);
			}
		}
	}

	return(FALSE);
}


BOOL ReadSectionLine(HANDLE hFile,
							LPTSTR FAR * lplpszText,
							DWORD dwSize,
							LPDWORD lpdwBytesUsed)
{
	DWORD dwBytesRead;

	if ( ReadLine(hFile,
				  (*lplpszText)+(*lpdwBytesUsed),
				  dwSize,
				  &dwBytesRead) )
	{
		if ( (*lplpszText)[(*lpdwBytesUsed)] == TEXT('[') )
		{
			(*lplpszText)[(*lpdwBytesUsed)] = TEXT('\0');
			return(FALSE);
		}
		else
		{
			(*lpdwBytesUsed) += dwBytesRead;
			(*lplpszText)[(*lpdwBytesUsed)] = TEXT('\0');
			return(TRUE);
		}
	}
	else
	{
		(*lpdwBytesUsed) += dwBytesRead;
		(*lplpszText)[(*lpdwBytesUsed)] = TEXT('\0');
		(*lpdwBytesUsed)++;
		return(FALSE);
	}
}


LRESULT CALLBACK HelpDlgProc(HWND hwnd,
									UINT uMsg,
									WPARAM wParam,
									LPARAM lParam)
{
	static HWND hWndEdit;
	static HANDLE hFile = NULL;
	static LPTSTR lpszText = NULL;

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				LPTSTR FAR * szTexts = (LPTSTR FAR *)lParam;
				LPTSTR lpszHelpFile = szTexts[0];
				LPTSTR lpszHelpItem = szTexts[1];
				
				MaxDialog(hwnd);
				
				SetWindowText(hwnd,lpszHelpItem);

				hWndEdit = GetDlgItem(hwnd,IDC_HELPEDIT);

				SetNewDialogFont(hWndEdit,100);

				hFile = CreateFile(lpszHelpFile,
								   GENERIC_READ,
								   FILE_SHARE_READ,
								   NULL,
								   OPEN_EXISTING,
								   FILE_ATTRIBUTE_NORMAL,
								   NULL);

				if ( hFile == INVALID_HANDLE_VALUE )
				{
					Edit_SetText(hWndEdit,TEXT("Cannot open help file"));
				}
				else
				{
					DWORD dwFileSize;
					
					dwFileSize = GetFileSize(hFile,NULL);
					
					lpszText = LocalAlloc(LMEM_FIXED,(dwFileSize+1)*sizeof(TCHAR));
					
					if ( lpszText == NULL )
					{
						Edit_SetText(hWndEdit,TEXT("Cannot allocate memory"));
					}
					else
					{
						DWORD dwBytesUsed = 0;

						if ( FindSection(hFile,
										 lpszText,
										 dwFileSize,
										 lpszHelpItem) )
						{
							while ( ReadSectionLine(hFile,
													&lpszText,
													dwFileSize-dwBytesUsed,
													&dwBytesUsed) )
							{
							}
							
							Edit_SetText(hWndEdit,lpszText);
						}
						else
						{
							Edit_SetText(hWndEdit,TEXT("Cannot read help file"));
						}
					}
				}

				SendMessage(hwnd,DM_SETDEFID,IDOK,0L);

				SetFocus(hWndEdit);
			}
			
			// Return FALSE since focus was manually set
			return(FALSE);

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDDONE:
				case IDOK:
				case IDCANCEL:

					// Exit the dialog
					ExitDialog();

					if ( hFile != NULL )
					{
						CloseHandle(hFile);
						hFile = NULL;
					}

					if ( lpszText != NULL )
					{
						LocalFree(lpszText);
						lpszText = NULL;
					}

					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


void BringUpHelp(void)
{
	LV_ITEM lvI;
	LPITEMSELECTOR lpItemSelector;

	int iItem = ListView_GetSelectedItem(l_hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(l_hWndList,&lvI);

	lpItemSelector = (LPITEMSELECTOR)lvI.lParam;

	if ( lpItemSelector->lpszHelpFile != NULL )
	{
		LPTSTR lpszHelpItem = FetchStringResource(lpItemSelector->lpszText);
		LPTSTR szTexts[] = { lpItemSelector->lpszHelpFile, lpszHelpItem };
		
		g_hdlg = CreateChosenDialog(g_hInstance,
									HelpDialogChoices,
									g_hwnd,
									HelpDlgProc,
									(LPARAM)szTexts);
	
		WaitUntilDialogDoneSkip(FALSE);
	}
}

LRESULT CALLBACK EditDlgProc(HWND hwnd,
									UINT uMsg,
									WPARAM wParam,
									LPARAM lParam)
{
	static LPTSTR FAR * szTexts;
	static HWND hWndEdit;
	static HWND hWndText;
    static DWORD dwTimeOut = MAXTIMEOUT;

	LPTSTR lpszText;

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Allocate save info for file name
			lpszText = LocalAlloc(LPTR,MAX_PATH64*sizeof(TCHAR));
			
			// If we allocated successfully
			if ( lpszText != NULL )
			{
				// Get file name into save buffer
				Edit_GetText(hWndEdit,lpszText,MAX_PATH64);
			}

			// Resize this dialog, saving info if allocated
			ResizeDialog(lpszText);
			
			break;

		case WM_SETFOCUS:

			SetFocus(hWndEdit);
			
			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				HFONT hNewFont;

				MaxDialog(hwnd);
				

				szTexts = (LPTSTR FAR *)lParam;

				hWndEdit = GetDlgItem(hwnd,IDC_EDITFIELD);
				hWndText = GetDlgItem(hwnd,IDC_EDITPROMPT);

				hNewFont = SetNewDialogFont(hwnd,100);

				SetWindowFont(hWndEdit,hNewFont,FALSE);
				SetWindowFont(hWndText,hNewFont,FALSE);
                
				lpszText = (LPTSTR)GetDialogSaveInfo();

				if ( lpszText != NULL )
				{
					Edit_SetText(hWndEdit,lpszText);
					LocalFree(lpszText);
				}
				else
				{

					Edit_SetText(hWndEdit,szTexts[1]);
				}

                if (!l_bUseWavFile)
                {

				    SetWindowFont(GetDlgItem(hwnd,IDC_PREV_FILE),hNewFont,FALSE);
				    SetWindowFont(GetDlgItem(hwnd,IDC_NEXT_FILE),hNewFont,FALSE);
                    Button_SetText(GetDlgItem(hwnd,IDC_PREV_FILE),TEXT("Decrement"));
                    Button_SetText(GetDlgItem(hwnd,IDC_NEXT_FILE),TEXT("Increment"));
					ShowWindow(GetDlgItem(hwnd,IDC_PREV_FILE),SW_HIDE);
					ShowWindow(GetDlgItem(hwnd,IDC_NEXT_FILE),SW_HIDE);

                }
                
				if ((!l_bUseWavFile) && (!l_bUseS24) )// convert it to number
                {
                    TCHAR szCurText[MAX_PATH64];
                    
                    Edit_GetText(hWndEdit,szCurText,TEXTSIZEOF(szCurText));

                    dwTimeOut = TEXTTOL(szCurText);

                }
				
				Edit_SetSel(hWndEdit,0,-1);
				SetFocus(hWndEdit);
				Edit_SetText(hWndText,szTexts[0]);

                // if we use number then we set the edit box to use integer only
			}
			
			// Return FALSE since focus was manually set
			return(FALSE);

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				// for wave file only
                case IDC_PREV_FILE:
                    if ((!l_bUseWavFile) && (!l_bUseS24) )   // used as number
                    {
                        TCHAR szText[MAX_PATH64];

                        dwTimeOut = dwTimeOut - 1000;   // in second decrement
                        if (dwTimeOut >= MAXTIMEOUT)
                            dwTimeOut = MAXTIMEOUT;
                		
                        ULTOTEXT(dwTimeOut,szText,10);

    					Edit_SetText(hWndEdit,szText);
        				Edit_SetSel(hWndEdit,0,-1);
		        		SetFocus(hWndEdit);
                        break;
                    }
				case IDC_NEXT_FILE:

#ifdef _SCANCTL
                    if (l_bUseWavFile)
					{
						TCHAR szBasePath[MAX_PATH64];

						GetWavePath(szBasePath);
						
						if ( IncFileName(hWndEdit,
										 (LOWORD(wParam)==IDC_NEXT_FILE)-
										 (LOWORD(wParam)==IDC_PREV_FILE),
										 szBasePath) )
						{
							TCHAR szText[MAX_PATH];

							Edit_GetText(hWndEdit,szText,TEXTSIZEOF(szText));

							sndPlaySound(szText,SND_ASYNC);
						}
					}
                    else
#endif
                    {
                        TCHAR szText[MAX_PATH64];

                        dwTimeOut = dwTimeOut + 1000;   // in second increment
                        if (dwTimeOut >= MAXTIMEOUT)
                            dwTimeOut = 0;

                		ULTOTEXT(dwTimeOut,szText,10);
    					Edit_SetText(hWndEdit,szText);
        				Edit_SetSel(hWndEdit,0,-1);
		        		SetFocus(hWndEdit);
                    }
					break;

				case IDC_POPUP:
				case IDOK:

					if ( l_bUseWavFile || l_bUseS24 )
					{
						// just return the string value
						Edit_GetText(hWndEdit,szTexts[1],MAX_PATH128);
					}
					else
                    {
						// we are expecting DWORD value
						// may need to check for valid dword value
						// before updating szText[1]
						if ( l_bValidateDword )
						{
							TCHAR szTemp[MAX_PATH128];

							Edit_GetText(hWndEdit,szTemp,MAX_PATH128);

	                        if ( IsValidDword(szTemp) )
		                    {
								DWORD nLen, i;

								TEXTCPY(szTexts[1],szTemp);

                           		nLen = TEXTLEN(szTexts[1]);

                        		for(i=0;i<nLen;i++)
                        		{
									szTexts[1][i] = TOUPPER(szTexts[1][i]);
                        		}
							}
							else
							{
								ReportInvalidDword(szTemp);
								
								break;
							}
                        }
						else
						{
							// just return the string value
							Edit_GetText(hWndEdit,szTexts[1],MAX_PATH128);
						}
                    }

					// Fall through
				case IDCANCEL:

					// Exit the dialog
					ExitDialog();


					break;
            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}

void EditTextItem(LPITEMSELECTOR lpis,
						 DWORD dwSelection)
{
	TCHAR szText1[MAX_PATH];
	TCHAR szText2[MAX_PATH];
	LPTSTR szTexts[] = { szText1, szText2 };

	ListView_GetItemText(l_hWndList,dwSelection,0,szText1,TEXTSIZEOF(szText1));
	ListView_GetItemText(l_hWndList,dwSelection,1,szText2,TEXTSIZEOF(szText2));

	g_hdlg = CreateChosenDialog(g_hInstance,
								EditDialogChoices,
								g_hwnd,
								EditDlgProc,
								(LPARAM)szTexts);

	// we want to skip the accelerator in EditDlgProc
	WaitUntilDialogDoneSkip(TRUE);

	TEXTCPY(lpis->szText,szText2);
	ListView_SetItemText(l_hWndList,dwSelection,1,szText2);
	
	if ( lpis->lpExitFunction != NULL )
	{
		(*lpis->lpExitFunction)(lpis,dwSelection);

		ListView_SetItemText(l_hWndList,dwSelection,1,lpis->szText);
	}

}


DWORD GetRangeCount(LPITEMSELECTOR lpis)
{
	DWORD dwCount = 0;

	if ( lpis->lplpszParamValuesList != NULL )
	{
		DWORD dwMin = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[0]));
		DWORD dwMax = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[1]));
		DWORD dwInc = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[2]));
		dwCount = ( dwMax - dwMin ) / dwInc;
		if ( dwMax == ( dwMin + dwCount * dwInc ) )
		{
			dwCount++;
		}
	}

	return(dwCount);
}


LPTSTR GetRangeItem(LPITEMSELECTOR lpis,
						   DWORD dwIndex)
{
	static TCHAR szTemp[MAX_PATH128];
	DWORD dwMin = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[0]));
	DWORD dwMax = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[1]));
	DWORD dwInc = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[2]));

	DWORD dwNew = dwMin + dwIndex * dwInc;
	if ( dwNew > dwMax )
	{
		dwNew = dwMin;
	}
	LTOTEXT(dwNew,szTemp,10);

	return(szTemp);
}



LPTSTR GetString(LPITEMSELECTOR lpis,
						DWORD dwIndex)
{
	LPTSTR lpszText = NULL;

	if ( lpis->lpGetValueFunction != NULL )
	{
        lpszText = (*lpis->lpGetValueFunction)(lpis,dwIndex);
	}

	return(lpszText);
}



DWORD GetInitial(LPITEMSELECTOR lpis)
{
	DWORD dwInitial = 0;

	if ( lpis->lpGetInitialIndexFunction != NULL )
	{
		dwInitial = (*lpis->lpGetInitialIndexFunction)(lpis);
	}

	return(dwInitial);
}


DWORD GetCount(LPITEMSELECTOR lpis)
{
	DWORD dwCount = 0;

	if ( lpis->lpGetCountFunction != NULL )
	{
		dwCount = (*lpis->lpGetCountFunction)(lpis);
	}
	else
	{
		if ( lpis->lplpszParamValuesList != NULL )
		{
			int j;

			for(j=0;lpis->lplpszParamValuesList[j];j++)
			{
				dwCount++;
			}
		}
	}

	return(dwCount);
}



void SetValue(HWND hWndList,
			  DWORD dwItem,
			  LPITEMSELECTOR lpis,
			  DWORD dwIndex)
{
	LV_ITEM lvI;

	lvI.mask = LVIF_TEXT;
	lvI.iItem = dwItem;
	lvI.iSubItem = 1;
	lvI.pszText = GetString(lpis,dwIndex);
		
	if ( lvI.pszText != NULL )
	{
		BOOL bResult;
		lvI.cchTextMax = TEXTLEN(lvI.pszText);
        bResult = ListView_SetItem(hWndList,&lvI);
		bResult = bResult;
	}
}


DWORD GetIndexFromValue(LPITEMSELECTOR lpis,
						DWORD dwValue)
{
	DWORD dwIndex = dwValue;

	switch(lpis->dwValueType)
	{
		case VT_STRINGRANGE:

			if ( lpis->lplpszParamValuesList != NULL )
			{
				DWORD dwMin = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[0]));
				DWORD dwMax = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[1]));
				DWORD dwInc = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[2]));
				
				if ( ( dwValue >= dwMin ) &&
					 ( dwValue <= dwMax ) )
				{
					dwIndex = ( dwValue - dwMin + ( dwInc - 1 ) / 2 ) / dwInc;
				}
			}
			
			break;
	}

	return(dwIndex);
}


DWORD GetValueFromIndex(LPITEMSELECTOR lpis,
						DWORD dwIndex)
{
	DWORD dwValue = dwIndex;

	switch(lpis->dwValueType)
	{
		case VT_STRINGRANGE:

			if ( lpis->lplpszParamValuesList != NULL )
			{
				DWORD dwMin = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[0]));
				DWORD dwMax = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[1]));
				DWORD dwInc = TEXTTOL(FetchStringResource(lpis->lplpszParamValuesList[2]));
				
				dwValue = dwMin + dwIndex * dwInc;
			}
			
			break;
	}

	return(dwValue);
}


VOID GetWidths(HWND hWndList,
			   LPITEMSELECTOR lpis,
			   LPINT lpnWidth1,
			   LPINT lpnWidth2)
{
	int nWidth1 = 0;
	int nWidth2 = 0;
	int nScrollBarWidth;
	int nHeight1 = 0;
	int nWidth;
	int nHeight;
	int i;
	int nExtra;
	RECT rect;
	HDC hdc;
	HFONT hNewFont;
	HFONT hOldFont;
	TEXTMETRIC tm;
	BOOL bTwoColumns = FALSE;
	int	nFontScaleFactor;
	SIZE size;

	nScrollBarWidth = GetSystemMetrics(SM_CXVSCROLL);

	for(i=0;lpis[i].lpszText;i++)
	{
		LPTSTR lpszText=NULL;

		lpszText = GetString(&lpis[i],0);
			
		if ( lpszText != NULL )
		{
			bTwoColumns = TRUE;
		}
	}

	if ( bTwoColumns )
	{
		switch(GetUIStyle())
		{
			case UI_STYLE_PEN:
				nFontScaleFactor = 100;
				break;
			case UI_STYLE_KEYPAD:
				nFontScaleFactor = 100;
				break;
			case UI_STYLE_FINGER:
				nFontScaleFactor = 115;
				break;
			default:
				nFontScaleFactor = 100;
				break;
		}
	}
	else
	{
		switch(GetUIStyle())
		{
			case UI_STYLE_PEN:
				nFontScaleFactor = 160;
				break;
			case UI_STYLE_KEYPAD:
				nFontScaleFactor = 140;
				break;
			case UI_STYLE_FINGER:
				nFontScaleFactor = 140;
				break;
			default:
				nFontScaleFactor = 140;
				break;
		}
	}

	hNewFont = SetNewDialogFont(hWndList,nFontScaleFactor);

	GetClientRect(hWndList,&rect);

	nWidth = rect.right - rect.left;
	nHeight = rect.bottom - rect.top
				- GetSystemMetrics(SM_CYCAPTION)
				- 2*GetSystemMetrics(SM_CYFIXEDFRAME);
	
	hdc = GetDC(hWndList);
		
	hOldFont = SelectObject(hdc,hNewFont);

	GetTextMetrics(hdc,&tm);

	if ( ( ListView_GetExtendedListViewStyle(hWndList) & LVS_NOCOLUMNHEADER ) == 0 )
	{
		LPTSTR pszText;

		pszText = FetchStringResource(RESOURCE_STRING(IDS_PARAMETER));

		GetTextExtentPoint32(hdc,
							 pszText,
							 TEXTLEN(pszText),
							 &size);

		nWidth1 = size.cx * 150 / 100;

		if ( bTwoColumns )
		{
			pszText = FetchStringResource(RESOURCE_STRING(IDS_VALUE));

			GetTextExtentPoint32(hdc,
								 pszText,
								 TEXTLEN(pszText),
								 &size);

			nWidth2 = size.cx * 150 / 100;
		}
	}
		
	for(i=0;lpis[i].lpszText;i++)
	{
		GetInitial(&lpis[i]);
		
		if ( ( lpis[i].lpbEnabled == NULL ) ||
			 *(lpis[i].lpbEnabled) )
		{
			LPTSTR lpszText;
			
			lpszText = FetchStringResource(lpis[i].lpszText);

			GetTextExtentPoint32(hdc,
								 lpszText,
								 TEXTLEN(lpszText),
								 &size);
			
			if ( size.cx > nWidth1 )
			{
				nWidth1 = size.cx;
			}

			nHeight1 += ( size.cy + tm.tmExternalLeading + 1 );

			lpszText = GetString(&lpis[i],0);
			
			if ( lpszText != NULL )
			{
				GetTextExtentPoint32(hdc,
									 lpszText,
									 TEXTLEN(lpszText),
									 &size);
				
				if ( ( size.cx > nWidth2 ) &&
					 ( size.cx < nWidth*100/80 ) )
				{
					nWidth2 = size.cx;
				}
			}
		}
	}

	SelectObject(hdc,hOldFont);
		
	ReleaseDC(hWndList,hdc);
	
	if ( nHeight1 > nHeight )
	{
		nWidth -= ( nScrollBarWidth + 1 );
	}

	if ( nWidth2 > 0 )
	{
		nExtra = ( nWidth - nWidth1 - nWidth2 ) / 2;
		nWidth1 += nExtra;
		nWidth2 += nExtra;
	}
	else
	{
		nWidth1 = nWidth;
	}

	*lpnWidth1 = nWidth1;
	*lpnWidth2 = nWidth2;
}


VOID InitializeCtlPanelListView(HWND hWndList,
								LPITEMSELECTOR lpis)
{
	LV_COLUMN lvC;
	int nWidth1;
	int nWidth2;
	int i;
	int j;

	if ( ( lpis == NULL ) || ( hWndList == NULL ) )
	{
		return;
	}
	
	GetWidths(hWndList,lpis,&nWidth1,&nWidth2);

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM | LVCF_TEXT;
	lvC.fmt = LVCFMT_LEFT;      // alignment

	lvC.iSubItem = 0;
	lvC.pszText = FetchStringResource(RESOURCE_STRING(IDS_PARAMETER));
	lvC.cx = nWidth1;           // width of the column in pixel
	ListView_InsertColumn(hWndList,0,&lvC);

	if ( nWidth2 > 0 )
	{
		lvC.iSubItem = 1;
		lvC.pszText = FetchStringResource(RESOURCE_STRING(IDS_VALUE));
		lvC.cx = nWidth2;
		ListView_InsertColumn(hWndList,1,&lvC);
	}

	j = 0;
	for(i=0;lpis[i].lpszText;i++)
	{
		GetInitial(&lpis[i]);
		
		if ( ( lpis[i].lpbEnabled == NULL ) ||
			 *(lpis[i].lpbEnabled) )
		{
			LV_ITEM lvI;

			lvI.mask = LVIF_TEXT | LVIF_PARAM;
			lvI.iItem = j;
			lvI.iSubItem = 0;
			lvI.lParam = (LPARAM)&lpis[i];
			lvI.pszText = FetchStringResource(lpis[i].lpszText);
			lvI.cchTextMax = TEXTLEN(lvI.pszText);
			{
				BOOL bResult = ListView_InsertItem(hWndList,&lvI);
				bResult = bResult;
			}

			lpis[i].dwCount = 0;

			lpis[i].dwIndex = GetInitial(&lpis[i]);

			SetValue(hWndList,j,&lpis[i],lpis[i].dwIndex);

			lpis[i].dwCount = GetCount(&lpis[i]);

			if ( ( lpis[i].dwRefreshTime != 0 ) &&
				 ( lpis[i].lpRefreshFunction != NULL ) )
			{
				SetTimer(g_hdlg,j+1000,lpis[i].dwRefreshTime,NULL);
			}

			j++;
		}
	}
}


VOID TerminateCtlPanelListView(HWND hWndList,
							   LPITEMSELECTOR lpis)
{
	int i;
	int j;

	j = 0;
	for(i=0;lpis[i].lpszText;i++)
	{
		if ( ( lpis[i].lpbEnabled == NULL ) ||
			 *(lpis[i].lpbEnabled) )
		{
			if ( ( lpis[i].dwRefreshTime != 0 ) &&
				 ( lpis[i].lpRefreshFunction != NULL ) )
			{
				KillTimer(g_hdlg,j+1000);
			}

			j++;
		}
	}
}


void AdvanceIndex(LPITEMSELECTOR lpis)
{
	lpis->dwIndex++;
	if ( lpis->dwIndex >= lpis->dwCount )
	{
		lpis->dwIndex = 0;
	}
}
		

void DispatchTimer(WPARAM wParam)
{
	LV_ITEM lvI;
	LPITEMSELECTOR lpItemSelector;

	int iItem = ((int)wParam)-1000;

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(l_hWndList,&lvI);

	lpItemSelector = (LPITEMSELECTOR)lvI.lParam;

	if ( lpItemSelector->lplpszParamValuesList != NULL )
	{
		if ( lpItemSelector->lpRefreshFunction != NULL )
		{
			if ( (*lpItemSelector->lpRefreshFunction)(lpItemSelector,
													  lpItemSelector->dwIndex) )
			{
				int iPreviousItem = ListView_GetSelectedItem(l_hWndList);
				
				SetValue(l_hWndList,iItem,lpItemSelector,lpItemSelector->dwIndex);

				ListView_SelectItem(l_hWndList,iItem);

				if ( lpItemSelector->lpSelectValueFunction != NULL )
				{
					(*lpItemSelector->lpSelectValueFunction)(lpItemSelector,lpItemSelector->dwIndex);
				}
				
				ListView_SelectItem(l_hWndList,iPreviousItem);
			}
		}
	}
}


void AdvanceCurrentParameter(void)
{
	LV_ITEM lvI;
	LPITEMSELECTOR lpItemSelector;

	int iItem = ListView_GetSelectedItem(l_hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(l_hWndList,&lvI);

	lpItemSelector = (LPITEMSELECTOR)lvI.lParam;

	if ( lpItemSelector->lplpszParamValuesList != NULL )
	{
		AdvanceIndex(lpItemSelector);

		SetValue(l_hWndList,iItem,lpItemSelector,lpItemSelector->dwIndex);

		ListView_SelectItem(l_hWndList,iItem);

		if ( lpItemSelector->lpSelectValueFunction != NULL )
		{
			(*lpItemSelector->lpSelectValueFunction)(lpItemSelector,lpItemSelector->dwIndex);
		}
	}
	else
	{
		if ( lpItemSelector->lpSelectValueFunction != NULL )
		{
			(*lpItemSelector->lpSelectValueFunction)(lpItemSelector,iItem);
		}
	}
}


void RetardIndex(LPITEMSELECTOR lpis)
{
	if ( lpis->dwIndex > 0 )
	{
		lpis->dwIndex--;
	}
	else
	{
		lpis->dwIndex = lpis->dwCount - 1;
		if (lpis->dwIndex < 0)
			lpis->dwIndex=0;
	}
}
		

void RetardCurrentParameter(void)
{
	LV_ITEM lvI;
	LPITEMSELECTOR lpItemSelector;

	int iItem = ListView_GetSelectedItem(l_hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(l_hWndList,&lvI);

	lpItemSelector = (LPITEMSELECTOR)lvI.lParam;

	if ( lpItemSelector->lplpszParamValuesList != NULL )
	{
		RetardIndex(lpItemSelector);

		SetValue(l_hWndList,iItem,lpItemSelector,lpItemSelector->dwIndex);

		ListView_SelectItem(l_hWndList,iItem);

		if ( lpItemSelector->lpSelectValueFunction != NULL )
		{
			(*lpItemSelector->lpSelectValueFunction)(lpItemSelector,lpItemSelector->dwIndex);
		}
	}
	else
	{
		if ( lpItemSelector->lpSelectValueFunction != NULL )
		{
			(*lpItemSelector->lpSelectValueFunction)(lpItemSelector,iItem);
		}
	}
}

//----------------------------------------------------------------------------
// CtlPanelDlgProc
//----------------------------------------------------------------------------

LRESULT CALLBACK CtlPanelDlgProc(HWND hwnd,
										UINT uMsg,
										WPARAM wParam,
										LPARAM lParam)
{
	LPINT lpnSelectedItem;
	
	static LPITEMSELECTOR l_lpis;

	l_hWndList = GetDlgItem(hwnd,IDC_PARAMETERS);

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Allocate save info area for selected index in list
			lpnSelectedItem = LocalAlloc(LPTR,sizeof(int));

			// If the allocation was successful
			if ( lpnSelectedItem != NULL )
			{
				// Get the current list item
				*lpnSelectedItem = ListView_GetSelectedItem(l_hWndList);
			}

			// Resize this dialog, saving info if allocated
			ResizeDialog(lpnSelectedItem);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		case WM_TIMER:

			DispatchTimer(wParam);
			
			break;
			
		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			MaxDialog(hwnd);
				
			l_lpis = (LPITEMSELECTOR)lParam;
			InitializeCtlPanelListView(l_hWndList,l_lpis);
			
			if ( l_dwNestCount > 0 )
			{
				Button_Show(GetDlgItem(hwnd,IDOK),TRUE);
				Button_Show(GetDlgItem(hwnd,IDCANCEL),TRUE);
				Button_Show(GetDlgItem(hwnd,IDDONE),FALSE);
				Button_Show(GetDlgItem(hwnd,IDC_RETARD),TRUE);
				Button_Show(GetDlgItem(hwnd,IDC_ADVANCE),TRUE);
			}
			else
			{
				Button_Show(GetDlgItem(hwnd,IDOK),FALSE);
				Button_Show(GetDlgItem(hwnd,IDCANCEL),FALSE);
				Button_Show(GetDlgItem(hwnd,IDDONE),TRUE);
				Button_Show(GetDlgItem(hwnd,IDC_RETARD),FALSE);
				Button_Show(GetDlgItem(hwnd,IDC_ADVANCE),TRUE);
			}

			lpnSelectedItem = (LPINT)GetDialogSaveInfo();

			if ( lpnSelectedItem != NULL )
			{
				// Set focus to list view and its first item
				ListView_SelectItem(l_hWndList,*lpnSelectedItem);

				LocalFree(lpnSelectedItem);
			}
			else
			{
				l_bDialogWasOKed = FALSE;		//TRUE;
				// Set focus to list view and its first item
				ListView_SelectItem(l_hWndList,0);
			}
			
			SetWindowText(hwnd,l_szLastItem);
	
			l_dwNestCount++;
			
			// Return FALSE since focus was manually set
			return(FALSE);

		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				
				switch(pLvdi->hdr.code)
				{
					case NM_DBLCLK:

						// Fall through

					case NM_CLICK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(l_hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(l_hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

							switch(GetUIStyle())
							{
								case UI_STYLE_PEN:
							
#ifndef WIN32_PLATFORM_POCKETPC
									if ( pLvdi->hdr.code != NM_DBLCLK )
									{
										ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));
										break;
									}
#endif

									// Fall through

								case UI_STYLE_KEYPAD:
								case UI_STYLE_FINGER:
									
									PostMessage(hwnd,WM_COMMAND,IDC_ADVANCE,0);
									
									break;								
							}

						}

						break;

						// Fall through

					case LVN_ITEMACTIVATE:
						
						switch(GetUIStyle())
						{
							case UI_STYLE_KEYPAD:
							case UI_STYLE_FINGER:

								PostMessage(hwnd,WM_COMMAND,IDC_ADVANCE,0);
								
								break;
						}

						break;
					
				}
			}
			break;

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_UP:

					ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));
					
					keybd_event(VK_UP,0,0,0);
					keybd_event(VK_UP,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDC_DOWN:

					ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));

					keybd_event(VK_DOWN,0,0,0);
					keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
					
					break;

				case IDC_RIGHT:
				case IDC_ADVANCE:
					
					if (!bReadOnly)
					{
						SetFocus(l_hWndList);
					
						AdvanceCurrentParameter();
					}
					break;

				case IDC_LEFT:
				case IDC_RETARD:
					if (!bReadOnly)
					{
					
						SetFocus(l_hWndList);
					
						RetardCurrentParameter();
					}
					break;

				case IDC_HELP_HELP:
					
					ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));
					
					BringUpHelp();
					
					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

							if ( l_dwNestCount > 1 )
							{
								InvokePopUpMenu(hwnd,IDM_POPUP,1,0,0);
							}
							else
							{
								InvokePopUpMenu(hwnd,IDM_POPUP,0,0,0);
							}
							
							break;

						case UI_STYLE_PEN:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;
				
				case IDOK:

					l_bDialogWasOKed = TRUE;

					if ( ( l_dwNestCount == 1 ) && !g_bSystemIsExiting )
					{
						break;
					}


					// Fall through

				case IDCANCEL:

					TerminateCtlPanelListView(l_hWndList,l_lpis);
					
					l_dwNestCount--;

					// Exit the dialog
					if ( ExitDialog() )
					{
						if ( l_dwNestCount >= 0 )
						{
							if ( l_lpExitFunction != NULL )
							{
								(*l_lpExitFunction)(l_lpItemSelector,l_dwIndex);
							}
						}

						if ( l_dwNestCount <= 0 )
						{
							SendMessage(g_hwnd,WM_CLOSE,0,0L);
						}
					}
					
					break;

				case IDDONE:
					SendMessage(g_hwnd,WM_CLOSE,0,0L);
					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


#ifdef _PERSISTCTL


BOOL MyCreateDirectory(LPTSTR lpszPath,
					   BOOL bCreateParent)
{
	DWORD dwAttrib;
	TCHAR szTemp[MAX_PATH];
	LPTSTR lpszBegin;
	LPTSTR lpszStart;
	LPTSTR lpszSlash;

	// If supplied path is NULL
	if ( lpszPath == NULL )
	{
		// Return failure
		return(FALSE);
	}
	
	// If supplied path is empty
	if ( *lpszPath == TEXT('\0') )
	{
		// Return failure
		return(FALSE);
	}
	
	// See if the directory already exists
	dwAttrib = GetFileAttributes(lpszPath);

	// Yes
	if (dwAttrib != -1)
	{
		// Return success
		return(TRUE);
	}

	// Try and create the directory, if success
	if ( CreateDirectory(lpszPath,NULL) )
	{
		// Return success
		return(TRUE);
	}

	// If we are not allowed to create the parent
	if ( !bCreateParent )
	{
		// Report failure to create directory
		LastError(TEXT("CreateDirectory"));

		// Return failure
		return(FALSE);
	}

	// Skip leading backslash
	lpszStart = lpszBegin = lpszPath+1;

	// Find last backslash in path
	do
	{
		// Find next backslash in path
		lpszSlash = TEXTCHR(lpszStart,TEXT('\\'));

		// If there isn't a backslash
		if ( lpszSlash == NULL )
		{
			// If we found no backslashes
			if ( lpszStart == lpszBegin )
			{
				// Report failure to create directory
				LastError(TEXT("CreateDirectory"));

				// Return failure
				return(FALSE);
			}

			// Last backslash found, stop looking
			break;
		}

		// Continue looking for next slash
		lpszStart = lpszSlash+1;

	} while (TRUE);

	// Extract parent path
	TEXTCPY(szTemp,lpszPath);
	szTemp[lpszStart-lpszPath-1] = TEXT('\0');

	// Try and create parent of desired path
	if ( !MyCreateDirectory(szTemp,TRUE) )
	{
		// Return failure
		return(FALSE);
	}

	// Try again to create desired path
	if ( !MyCreateDirectory(lpszPath,FALSE) )
	{
		// Return failure
		return(FALSE);
	}

	// Return success
	return(TRUE);
}


void SelectPersistence(LPITEMSELECTOR lpis,
					   DWORD dwSelection)
{
	l_dwPersistence = dwSelection;

	// if we chose permanent, need to verify the directory exist
	if (l_dwPersistence == PERSISTENCE_PERMANENT)
	{
		// Create directory if it doesn't already exist
		MyCreateDirectory(g_szRegFilePath,TRUE);
	}
}


DWORD GetInitialPersistence(LPITEMSELECTOR lpis)
{
	return(l_dwPersistence);
}

#endif

void DisplaySelection(DWORD dwSelection)
{
	TCHAR szMsg[MAX_PATH32];

	TEXTSPRINTF(szMsg,TEXT("Selection = %ld"),dwSelection);
	MyMessage(TEXT("Selection Made"),szMsg);
}


//----------------------------------------------------------------------------
// Calibration Functions
//----------------------------------------------------------------------------

#ifdef _TOUCH

BOOL WINAPI TouchCalibrate(void);

void EnterTouchCalibrate(LPITEMSELECTOR lpis,DWORD dwSelection)
{
#if defined(_WIN32_WCE) && !defined(_WIN32_WCE_EMULATION)
    TouchCalibrate();
#endif
}

void ExitTouchCalibrate(LPITEMSELECTOR lpis,DWORD dwSelection)
{
}

#endif


//----------------------------------------------------------------------------
// Unit ID
//----------------------------------------------------------------------------

#ifdef _UUID


RCM_VERSION_INFO l_RCMVersionInfo;


LPTSTR GetRCMCAPIVersion(LPITEMSELECTOR lpis,
						 DWORD dwIndex)
{
    static TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,
				TEXT("%02d.%02d"),
				HIWORD(l_RCMVersionInfo.dwCAPIVersion),
				LOWORD(l_RCMVersionInfo.dwCAPIVersion));
    
    return(szMsg);
}


LPTSTR GetResCoordVersion(LPITEMSELECTOR lpis,
						  DWORD dwIndex)
{
    static TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,
				TEXT("%02d.%02d"),
				HIWORD(l_RCMVersionInfo.dwResCoordVersion),
				LOWORD(l_RCMVersionInfo.dwResCoordVersion));

    return(szMsg);
}


LPTSTR GetUUIDVersion(LPITEMSELECTOR lpis,
					  DWORD dwIndex)
{
    static TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,
				TEXT("%02d.%02d"),
				HIWORD(l_RCMVersionInfo.dwUUIDVersion),
				LOWORD(l_RCMVersionInfo.dwUUIDVersion));

    return(szMsg);
}


LPTSTR GetTemperatureVersion(LPITEMSELECTOR lpis,
							 DWORD dwIndex)
{
    static TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,
				TEXT("%02d.%02d"),
				HIWORD(l_RCMVersionInfo.dwTemperatureVersion),
				LOWORD(l_RCMVersionInfo.dwTemperatureVersion));

    return(szMsg);
}


static LPTSTR GetUnitID(LPITEMSELECTOR lpis,
						DWORD dwIndex)
{
    static TCHAR szUnitId[MAX_PATH64];
	
    DWORD dwResult = 0;
	HANDLE hInstRcmAPI = NULL;
	LPFNRCMGETUNIQUEUNITIDEX lpfnRcmGetUniqueUnitIdEx = NULL;
	LPFNRCMGETUNIQUEUNITID lpfnRcmGetUniqueUnitId = NULL;
	UNITID_EX UnitIdEx;
	UNITID UnitId;
	BOOL bExtended = FALSE;
	DWORD i;

#ifdef _SIMULATE_UUID
	TEXTCPY(szUnitId,_SIMULATE_UUID);
#endif

	hInstRcmAPI = LoadLibrary(_TRCMAPI32);

#ifndef _SIMULATE_UUID
	if ( NULL == hInstRcmAPI )
	{
		MyError(TEXT("Cannot load DLL"),_TRCMAPI32);

		return(NULL);
	}
#endif

	if ( NULL != hInstRcmAPI )
	{
		lpfnRcmGetUniqueUnitIdEx =
			(LPFNRCMGETUNIQUEUNITIDEX) GetProcAddress(hInstRcmAPI, _TRCMGETUNIQUEUNITIDEX);
	}

	if ( NULL != lpfnRcmGetUniqueUnitIdEx )
	{
		SI_INIT(&UnitIdEx);
		
		dwResult = lpfnRcmGetUniqueUnitIdEx(&UnitIdEx);

		if ( E_RCM_SUCCESS == dwResult )
		{
			bExtended = TRUE;

			for (i=0; i<(UnitIdEx.StructInfo.dwUsed - sizeof(STRUCT_INFO)); i++)
			{
				TEXTSPRINTF(szUnitId+2*i,TEXT("%.2X"),UnitIdEx.byUUID[i]);
			}
		}
	}

	if ( !bExtended )
	{
		if ( NULL != hInstRcmAPI )
		{
			lpfnRcmGetUniqueUnitId =
				(LPFNRCMGETUNIQUEUNITID) GetProcAddress(hInstRcmAPI, _TRCMGETUNIQUEUNITID);
		}

#ifndef _SIMULATE_UUID
		if ( NULL == lpfnRcmGetUniqueUnitId )
		{
			MyError(TEXT("Cannot access function"),_TRCMGETUNIQUEUNITID);

			FreeLibrary(hInstRcmAPI);

			return(NULL);
		}
#endif

		if ( NULL != lpfnRcmGetUniqueUnitId )
		{
			dwResult = lpfnRcmGetUniqueUnitId(&UnitId);

			if ( E_RCM_SUCCESS == dwResult )
			{
				for (i=0; i<sizeof(UnitId); i++)
				{
					TEXTSPRINTF(szUnitId+2*i,TEXT("%.2X"),UnitId[i]);
				}
			}
		}
	}

	if ( NULL != hInstRcmAPI )
	{
		FreeLibrary(hInstRcmAPI);
	}

	if ( dwResult == E_RCM_WIN32ERROR )
	{
		dwResult = GetLastError();
	}

#ifndef _SIMULATE_UUID
    if ( E_RCM_SUCCESS != dwResult )
    {
		ReportError(TEXT("Cannot get unit ID"),dwResult);

		return(NULL);
    }
#endif

    return(szUnitId);
}


static BOOL InitUnitIdInfo(void)
{
	HANDLE hInstRcmAPI = NULL;
	LPFNRCMGETVERSION lpfnRcmGetVersion = NULL;
	LPTSTR lpszText = NULL;
	DWORD dwSize = 0;

	hInstRcmAPI = LoadLibrary(_TRCMAPI32);

#ifndef _SIMULATE_UUID
	if ( NULL == hInstRcmAPI )
	{
		MyError(TEXT("Cannot load DLL"),_TRCMAPI32);

		return(FALSE);
	}
#endif

	if ( NULL != hInstRcmAPI )
	{
		lpfnRcmGetVersion =
			(LPFNRCMGETVERSION) GetProcAddress(hInstRcmAPI, _TRCMGETVERSION);
	}

#ifndef _SIMULATE_UUID
	if ( NULL == lpfnRcmGetVersion )
	{
		MyError(TEXT("Cannot access function"),_TRCMGETVERSION);

		FreeLibrary(hInstRcmAPI);

		return(FALSE);
	}
#endif

	SI_INIT(&l_RCMVersionInfo);

#ifdef _SIMULATE_UUID

	l_RCMVersionInfo.dwCAPIVersion = 0x00090001;
	l_RCMVersionInfo.dwResCoordVersion = 0x00090002;
	l_RCMVersionInfo.dwUUIDVersion = 0x00090003;
	l_RCMVersionInfo.dwTemperatureVersion = 0x00090004;

#endif

	if ( NULL != lpfnRcmGetVersion )
	{
		lpfnRcmGetVersion(&l_RCMVersionInfo);
	}

	if ( NULL != hInstRcmAPI )
	{
		FreeLibrary(hInstRcmAPI);
	}

	lpszText = GetUnitID(NULL,0);

#ifndef _SIMULATE_UUID
	if ( NULL == lpszText )
	{
		return(FALSE);
	}
#endif
	
	if ( NULL != lpszText )
	{
		dwSize = TEXTLEN(lpszText);
	}

#ifndef _SIMULATE_UUID
	if ( 0 == dwSize )
	{
		return(FALSE);
	}
#endif
	
	if ( dwSize > 0*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID0 = TRUE;
	}

	if ( dwSize > 1*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID1 = TRUE;
	}

	if ( dwSize > 2*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID2 = TRUE;
	}

	if ( dwSize > 3*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID3 = TRUE;
	}

	if ( dwSize > 4*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID4 = TRUE;
	}
	
	if ( dwSize > 5*UUID_CHUNK_SIZE*2 )
	{
		l_bUUID5 = TRUE;
	}

	return(TRUE);
}


static LPTSTR GetUnitIDChunk(LPITEMSELECTOR lpis,
							 DWORD dwIndex,
							 DWORD dwStart)
{
	static TCHAR szMsg[] = TEXT("");

	LPTSTR lpszText = NULL;
	DWORD dwSize = 0;
	DWORD dwChunk;
	DWORD dwOffset;
	
	dwChunk = UUID_CHUNK_SIZE * 2;

	dwOffset = dwStart * dwChunk;
	
	lpszText = GetUnitID(lpis,dwIndex);

	if ( NULL == lpszText )
	{
		return(szMsg);
	}

	if ( NULL != lpszText )
	{
		dwSize = TEXTLEN(lpszText);
	}

	if ( 0 == dwSize )
	{
		return(szMsg);
	}

	lpszText += dwOffset;

	dwSize -= dwOffset;

	if ( dwSize > dwChunk )
	{
		lpszText[dwChunk] = 0;
	}

	return(lpszText);
}


LPTSTR GetUnitID0(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,0));
}


LPTSTR GetUnitID1(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,1));
}


LPTSTR GetUnitID2(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,2));
}


LPTSTR GetUnitID3(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,3));
}


LPTSTR GetUnitID4(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,4));
}


LPTSTR GetUnitID5(LPITEMSELECTOR lpis,
				  DWORD dwIndex)
{
	return(GetUnitIDChunk(lpis,dwIndex,5));
}


void EnterUnitID(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if ( !InitUnitIdInfo() )
	{
		return;
	}

	LoadString(g_hInstance,IDS_ID,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)UnitIDSelector);

}


void ExitUnitID(LPITEMSELECTOR lpis,DWORD dwSelection)
{
}


#endif


DWORD MyWriteFile(HANDLE hFile,
				  LPCVOID lpBuffer,
				  DWORD nNumberOfBytesToWrite,
				  LPDWORD lpNumberOfBytesWritten,
				  LPOVERLAPPED lpOverlapped)
{
#ifdef UNICODE

	CHAR szBufferA[MAX_PATH];
	INT iBytesConverted;

	iBytesConverted = WideCharToMultiByte(CP_OEMCP,
										  0,
										  (LPTSTR)lpBuffer,
										  nNumberOfBytesToWrite,
										  szBufferA,
										  sizeof(szBufferA),
										  NULL,
										  NULL);

	return(WriteFile(hFile,
					 szBufferA,
					 iBytesConverted,
					 lpNumberOfBytesWritten,
					 lpOverlapped));

#else

	return(WriteFile(hFile,
					 lpBuffer,
					 nNumberOfBytesToWrite,
					 lpNumberOfBytesWritten,
					 lpOverlapped));

#endif
}


DWORD WriteRegFile(LPTSTR lpszKeyName,
				   LPTSTR lpszValueName,
				   DWORD dwType,
				   LPVOID lpvData,
				   DWORD dwSize,
				   BOOL	 bhkcu)
{
	DWORD dwResult = ERROR_SUCCESS;
	HANDLE hFile;
	TCHAR szFilePath[MAX_PATH];
	TCHAR szFileName[MAX_PATH];
	DWORD i;

//	TEXTCPY(szFilePath,REG_FILE_PATH);
	TEXTCPY(szFilePath,g_szRegFilePath);
	TEXTCAT(szFilePath,TEXT("\\"));

	TEXTCPY(szFileName,lpszKeyName);
	TEXTCAT(szFileName,TEXT("."));
	TEXTCAT(szFileName,lpszValueName);

	TEXTCAT(szFileName,TEXT(".REG"));

	for(i=0;i<TEXTLEN(szFileName);i++)
	{
		if ( szFileName[i] == TEXT('\\') )
		{
			szFileName[i] = TEXT('.');
		}
	}

	TEXTCAT(szFilePath,szFileName);

	hFile = CreateFile(szFilePath,
					   GENERIC_WRITE|GENERIC_READ,  // need read access as well
					   0,
					   NULL,
					   CREATE_ALWAYS,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);
	
	if ( hFile == INVALID_HANDLE_VALUE )
	{
		dwResult = GetLastError();
	}

	if ( dwResult == ERROR_SUCCESS )
	{
        TCHAR szBuffer[MAX_PATH128];
        DWORD dwBytesWritten;

// since we are using RegMerge in the CE terminal, we don't need REGEDIT4
#ifndef _WIN32_WCE

		TEXTCPY(szBuffer,TEXT("REGEDIT4\r\n\r\n"));

		if ( !MyWriteFile(hFile,
						  szBuffer,
						  TEXTLEN(szBuffer),
						  &dwBytesWritten,
						  NULL) )
		{
			dwResult = GetLastError();
		}
#endif		

		if ( dwResult == ERROR_SUCCESS )
		{
			if (bhkcu)
				TEXTCPY(szBuffer,REG_HKCU_BASE_NAME);
			else
				TEXTCPY(szBuffer,REG_HKLM_BASE_NAME);

			TEXTCAT(szBuffer,lpszKeyName);
			TEXTCAT(szBuffer,TEXT("]\r\n"));

			if ( !MyWriteFile(hFile,
							  szBuffer,
							  TEXTLEN(szBuffer),
							  &dwBytesWritten,
							  NULL) )
			{
				dwResult = GetLastError();
			}
		}

		if ( dwResult == ERROR_SUCCESS )
		{
			TEXTCPY(szBuffer,TEXT("\""));
			TEXTCAT(szBuffer,lpszValueName);
			TEXTCAT(szBuffer,TEXT("\"="));

			switch(dwType)
			{
				case REG_BINARY:
					TEXTCAT(szBuffer,TEXT("hex:\\\r\n"));
					break;
                case REG_DWORD:
                    TEXTCAT(szBuffer,TEXT("dword:"));
                    break;
				case REG_SZ:
					TEXTCAT(szBuffer,TEXT("\""));
					break;
				case REG_MULTI_SZ:	// regmerge recognize multi_sz as hex(7)
					TEXTCAT(szBuffer,TEXT("multi_sz:\""));
					break;
				default:
					dwResult = ERROR_INVALID_PARAMETER;
					break;
			}
		}

		if ( dwResult == ERROR_SUCCESS )
		{

			if ( !MyWriteFile(hFile,
							  szBuffer,
							  TEXTLEN(szBuffer),
							  &dwBytesWritten,
							  NULL) )
			{
				dwResult = GetLastError();
			}
		}

        if ( dwType == REG_DWORD )
        {
            DWORD dwOffset = 0;
			LPDWORD lpdwData = (LPDWORD)lpvData;

            dwOffset += TEXTSPRINTF(szBuffer + dwOffset,TEXT("%08x"),*lpdwData);
            
            dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT("\r\n"));

                        if ( !MyWriteFile(hFile,
										  szBuffer,
										  TEXTLEN(szBuffer),
										  &dwBytesWritten,
										  NULL) )
						{
							dwResult = GetLastError();
						}

        }
		
		if (( dwType == REG_SZ) || (dwType == REG_MULTI_SZ) )
		{
			LPTSTR lpszData = (LPTSTR)lpvData;
			DWORD	dwLen = 0;

			TEXTCPY(szBuffer,lpszData);
            TEXTCAT(szBuffer,TEXT("\""));

			dwLen = TEXTLEN(lpszData);	// only upto the first portion
			// if we have only one string, then lpszData + dwLen should be 0
			if ((lpszData[dwLen+1] != 0x0) && (dwType == REG_MULTI_SZ))
			{
				// we have more
				TEXTCAT(szBuffer,TEXT(",\""));
				TEXTCAT(szBuffer,lpszData+dwLen+1);
				TEXTCAT(szBuffer,TEXT("\""));
			}

            if ( !MyWriteFile(hFile,
							  szBuffer,
							  TEXTLEN(szBuffer),
							  &dwBytesWritten,
							  NULL) )
			{
				dwResult = GetLastError();
			}

        }

		if ( dwType == REG_BINARY )
		{
			DWORD i;
			DWORD dwBytesPerLine = 16;
			LPBYTE lpbData = (LPBYTE)lpvData;
				
			for(i=0;((i<dwSize)&&(dwResult==ERROR_SUCCESS));i+=dwBytesPerLine)
			{
				DWORD j;
				DWORD dwOffset = 0;

				for(j=i;((j<dwSize)&&(j<(i+16))&&(dwResult==ERROR_SUCCESS));j++)
				{
					if ( j == i )
					{
						dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT("  "));
					}

					dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT("%.2X"),lpbData[j]);

					if ( j != ( dwSize - 1 ) )
					{
						dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT(","));

						if ( j == ( i + dwBytesPerLine - 1 ) )
						{
							dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT("\\"));
						}
					}

					if ( ( j == ( i + dwBytesPerLine - 1 ) ) ||
						 ( j == ( dwSize - 1 ) ) )
					{
						dwOffset += TEXTSPRINTF(szBuffer+dwOffset,TEXT("\r\n"));

                        if ( !MyWriteFile(hFile,
										  szBuffer,
										  TEXTLEN(szBuffer),
										  &dwBytesWritten,
										  NULL) )
						{
							dwResult = GetLastError();
						}

					}
				}
			}
		}

		CloseHandle(hFile);
	}

	return(dwResult);
}


LPTSTR GetRegSection(LPTSTR lpszRegKeyFullPath,
					 LPHKEY lphKey)
{
	LPTSTR lpszRegKeyPath = NULL;

	if ( ( lpszRegKeyFullPath != NULL ) &&
		 ( lphKey != NULL ) )
	{
		if ( TEXTNICMP(lpszRegKeyFullPath,TEXT("HKLM\\"),5) == 0 )
		{
			*lphKey = HKEY_LOCAL_MACHINE;
			
			lpszRegKeyPath = lpszRegKeyFullPath + 5;
		}
		else if ( TEXTNICMP(lpszRegKeyFullPath,TEXT("HKCU\\"),5) == 0 )
		{
			*lphKey = HKEY_CURRENT_USER;

			lpszRegKeyPath = lpszRegKeyFullPath + 5;
		}
		else
		{
			*lphKey = NULL;

			MyError(TEXT("Bad section"),lpszRegKeyFullPath);
		}
	}

	return(lpszRegKeyPath);
}


BOOL ReadFromRegistry(LPTSTR lpszRegKeyFullPath,
					  LPTSTR lpszRegValueName,
					  LPVOID lpvData,
					  LPDWORD lpdwSize,
					  LPDWORD lpdwType)
{
	LPTSTR lpszRegKeyPath;
	HKEY hRegSection;
	DWORD dwRetCode;
	HKEY hKey;
	DWORD dwActualType;
	BOOL bResult;

	lpszRegKeyPath = GetRegSection(lpszRegKeyFullPath,&hRegSection);
	
	if ( ( lpszRegKeyPath == NULL ) ||
		 ( lpszRegValueName == NULL ) ||
		 ( lpdwType == NULL ) )
	{
		MyError(TEXT("NULL Pointer"),NULL);

		return(FALSE);
	}

	dwRetCode = RegOpenKeyEx(hRegSection,lpszRegKeyPath,0,KEY_ALL_ACCESS,&hKey);
     
    if ( dwRetCode != ERROR_SUCCESS )
    {
		MyError(TEXT("Key not found"),lpszRegKeyPath);

		return(FALSE);
	}

	dwRetCode = RegQueryValueEx(hKey,
								lpszRegValueName,
								NULL,
								&dwActualType,
								(LPBYTE)lpvData,
								lpdwSize);
     
	RegCloseKey(hKey);
    
	if ( dwRetCode != ERROR_SUCCESS )
    {
		MyError(TEXT("Value not found"),lpszRegKeyPath);

		return(FALSE);
	}

	if ( ( *lpdwType == dwActualType ) ||
		 ( *lpdwType == REG_NONE ) )
	{
		bResult = TRUE;
	}
	else
	{
		bResult = FALSE;
	}

	*lpdwType = dwActualType;
	
	return(bResult);
}


BOOL MakePersistent(LPTSTR lpszRegKeyFullPath,
					LPTSTR lpszRegValueName)
{
	DWORD dwType;
	DWORD dwLen;
	TCHAR szValue[MAX_PATH];
	HKEY hRegSection;
	LPTSTR lpszRegKeyPath;

	if ( l_dwPersistence != PERSISTENCE_PERMANENT )
	{
		return(FALSE);
	}

	dwType = REG_NONE;
	dwLen = TEXTSIZEOF(szValue);
	if ( !ReadFromRegistry(lpszRegKeyFullPath,
						   lpszRegValueName,
						   szValue,
						   &dwLen,
						   &dwType) )
	{
		return(FALSE);
	}
	
	lpszRegKeyPath = GetRegSection(lpszRegKeyFullPath,&hRegSection);

	return(WriteRegFile(lpszRegKeyPath,
						lpszRegValueName,
						dwType,
						szValue,
						dwLen,
						(hRegSection==HKEY_CURRENT_USER)));
}


BOOL WriteToRegistry(LPTSTR lpszRegKeyFullPath,
					 LPTSTR lpszRegValueName,
					 LPVOID lpvData,
					 DWORD dwLen,
					 DWORD dwType)
{
	LPTSTR lpszRegKeyPath;
	HKEY hRegSection;
    DWORD dwRetCode;
    HKEY hKey;

	lpszRegKeyPath = GetRegSection(lpszRegKeyFullPath,&hRegSection);
	
	if ( ( lpszRegKeyPath == NULL ) ||
		 ( lpszRegValueName == NULL ) )
	{
		MyError(TEXT("NULL Pointer"),NULL);

		return(FALSE);
	}

	dwRetCode = RegOpenKeyEx(hRegSection,lpszRegKeyPath,0,KEY_ALL_ACCESS,&hKey);
     
    if ( dwRetCode != ERROR_SUCCESS )
    {
		MyError(TEXT("Key not found"),lpszRegKeyPath);

		return(FALSE);
	}

	dwRetCode = RegSetValueEx(hKey,lpszRegValueName,0,dwType,(LPBYTE)lpvData,dwLen);

	RegCloseKey(hKey);     
	
	if ( dwRetCode != ERROR_SUCCESS )
    {
		MyError(TEXT("Cannot write key"),lpszRegKeyPath);

		return(FALSE);
	}

	return(MakePersistent(lpszRegKeyFullPath,lpszRegValueName));
}


// look at HKCU\Software\Symbol\Settings\RegDirectory
void GetDefaultRegFilePath(void)
{
    DWORD dwRetCode;
    HKEY hKey;
    DWORD dwLen = 0;
			
	dwRetCode = RegOpenKeyEx(HKEY_CURRENT_USER,TEXT("Software\\Symbol\\Settings"),0,KEY_READ, &hKey);
       
    if (dwRetCode == ERROR_SUCCESS)
    {
        DWORD dwType;

		// look for RegDirectory
        dwLen = countof(g_szRegFilePath);
        dwRetCode = RegQueryValueEx(hKey,TEXT("RegsDirectory"),NULL,&dwType,(LPBYTE)&g_szRegFilePath,&dwLen);
	}

        // no entry found or error, use the default
    if ((dwRetCode != ERROR_SUCCESS) || (dwLen == 2))
    {
        TEXTCPY(g_szRegFilePath,REG_FILE_PATH);
    }

	RegCloseKey(hKey);
	
}


// initialize control panel setting for each module
void InitCtlPanel(LPITEMSELECTOR lpis, DWORD dwSelection, UINT uID)
{
	LoadString(g_hInstance,uID,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;
}


//----------------------------------------------------------------------------
// CtlPanel
//----------------------------------------------------------------------------

void CtlPanel(void)
{
	// get the default file path in here.  We can also do it when we select permanent
	GetDefaultRegFilePath();

	LoadString(g_hInstance,IDS_CONTROLPANEL,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	g_hdlg = CreateChosenDialog(g_hInstance,
							 CtlPanelDialogChoices,
							 g_hwnd,
							 CtlPanelDlgProc,
							 (LPARAM)CtlPanelSelector);

}


#ifdef _ABOUTCTL

void EnterAbout(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	About(g_hwnd,IDS_PROGRAM,IDS_PROGRAMDESCRIPTION,IDI_PROGRAMICON);
}


#endif

