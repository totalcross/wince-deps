
//--------------------------------------------------------------------
// FILENAME:            Audio.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Audio Module for CtlPanel
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
// 
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

// Symbol SDK common includes

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// CtlPanel includes
#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before audio.h
#ifdef _AUDIOCTL
// Symbol CAPI include
#include <audiocapi.h>
#include "audio.h"

// Audio
DWORD g_dwBeeperVolume = 0;
DWORD g_dwEarSaveDelay = 0;
LPAUDIO_VERSION_INFO    l_lpAudioVersionInfo;


void Audio_SaveParams(void)
{
    AUDIO_GetBeeperVolume(&g_dwBeeperVolume);
    AUDIO_GetEarSaveDelay(&g_dwEarSaveDelay);
}

void Audio_RestoreParams()
{
    AUDIO_SetBeeperVolume(g_dwBeeperVolume);
    AUDIO_SetEarSaveDelay(g_dwEarSaveDelay);
}

void EnterAudioParameters(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	DWORD dwResult = 0;

	LoadString(g_hInstance,IDS_AUDIOPARAMETERS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    // save the old parameter
    Audio_SaveParams();

    l_lpAudioVersionInfo = (AUDIO_VERSION_INFO *)LocalAlloc(LMEM_FIXED,sizeof(AUDIO_VERSION_INFO));

	SI_ALLOC_ALL(l_lpAudioVersionInfo);
	l_lpAudioVersionInfo->StructInfo.dwUsed = 0;

	dwResult = AUDIO_GetVersion(l_lpAudioVersionInfo);
	if ( dwResult != E_AUD_SUCCESS)
	{
		ReportError(TEXT("AUDIO_GetVersion Error"),dwResult);
		return;
	}
	
	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)AudioParametersSelector);
}


void ExitAudioParameters(LPITEMSELECTOR lpis,
								DWORD dwSelection)
{
    // accepted and is persistence
    if (( l_bDialogWasOKed ) && (l_dwPersistence == PERSISTENCE_PERMANENT))
    {
        DWORD dwRetCode;
        TCHAR szKey[MAX_PATH];
        DWORD dwVolume=0;
        DWORD dwEarSaveDelay=0;
        DWORD dwResult=0;

    	// construct key name 
        TEXTNCPY(szKey,TEXT("Software\\Symbol\\Audio"),TEXTSIZEOF(szKey));
	    szKey[TEXTSIZEOF(szKey)-1] = TEXT('\0');

        // get the current volume
        AUDIO_GetBeeperVolume(&dwVolume);
        
        // create a reg file
        dwRetCode = WriteRegFile(szKey,
	    						 TEXT("BeeperVolume"),
		    					 REG_DWORD,
			    				 &dwVolume,
				    			 sizeof(DWORD),
								 0);

    	if ( dwRetCode != ERROR_SUCCESS )
	    {
		    ReportError(TEXT("RegSetValueEx"),dwRetCode);
	    }	
        

    	// construct key name 
        TEXTNCPY(szKey,TEXT("ControlPanel\\Volume"),TEXTSIZEOF(szKey));
	    szKey[TEXTSIZEOF(szKey)-1] = TEXT('\0');

        dwResult = AUDIO_GetEarSaveDelay(&dwEarSaveDelay);
        
        if (dwResult == E_AUD_SUCCESS)
        {
            dwRetCode = WriteRegFile(szKey,
	        						 TEXT("EarSaveDelay"),
		        					 REG_BINARY,
			        				 &dwEarSaveDelay,
				        			 sizeof(DWORD),
									 1);
        }
    }
    else  
        if (!l_bDialogWasOKed)	// it's rejected
            Audio_RestoreParams();

	LocalFree(l_lpAudioVersionInfo);
    l_lpExitFunction = NULL;

}




DWORD GetInitialVolume(LPITEMSELECTOR lpis)
{
    DWORD dwResult;
	DWORD dwBeeperVolume;
    DWORD dwVolumeLevels = 0;
    
	bAudioLowMedHigh    = FALSE;
	bAudioLowHigh       = FALSE;
	bAudioTest			= FALSE;

    // get the levels
    dwResult = AUDIO_GetBeeperVolumeLevels(&dwVolumeLevels);

	if ( dwResult != E_AUD_SUCCESS )
	{
		return(0);
	}

    switch (dwVolumeLevels)
    {
		case 3:
			bAudioLowMedHigh    = TRUE;
			break;
		case 2:
			bAudioLowHigh       = TRUE;
			break;
		case 1:
	        bAudioTest			= TRUE;
	        break;
    }

	dwResult = AUDIO_GetBeeperVolume(&dwBeeperVolume);

	if ( dwResult != E_AUD_SUCCESS )
	{
		return(0);
	}

	if ( dwBeeperVolume > dwVolumeLevels-1 )
	{
		dwBeeperVolume = dwVolumeLevels-1;
	}

	return(dwBeeperVolume);
}


void SelectBeeperVolume(LPITEMSELECTOR lpis,
								DWORD dwSelection)
{

    DWORD dwVolumeLevels;
    
    // Get volume levels the system supports
    AUDIO_GetBeeperVolumeLevels(&dwVolumeLevels);

    // If beyond highest, set to highest
	if ( dwSelection > dwVolumeLevels - 1 )
	{
		dwSelection = dwVolumeLevels - 1;
	}

    // Set desired volume
	AUDIO_SetBeeperVolume(dwSelection);

	// Play default beep as feedback of change made
	PlayDefaultBeep();
}


// ESD not supported in 7000 terminals
DWORD GetInitialESD(LPITEMSELECTOR lpis)
{
    DWORD dwResult;
    DWORD dwEarSaveDelay=0;
    
    dwResult = AUDIO_GetEarSaveDelay(&dwEarSaveDelay);

    if (dwResult == E_AUD_SUCCESS)
    {
        bAudioESDSupport = TRUE;

        return (dwEarSaveDelay);
    }
    else
    {
        bAudioESDSupport = FALSE;

        return ((DWORD)0);
    }

}


void SelectEarSaveDelay(LPITEMSELECTOR lpis,
							   DWORD dwSelection)
{
	AUDIO_SetEarSaveDelay(dwSelection);
}

static LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpAudioVersionInfo->dwCAPIVersion),LOWORD(l_lpAudioVersionInfo->dwCAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetNotifyCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpAudioVersionInfo->dwNotifyAPIVersion),LOWORD(l_lpAudioVersionInfo->dwNotifyAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}


#endif	// ifdef _AUDIOCTL
