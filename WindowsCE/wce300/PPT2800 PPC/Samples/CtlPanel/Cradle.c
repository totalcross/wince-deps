
//--------------------------------------------------------------------
// FILENAME:            Cradle.c
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Cradle Module for CtlPanel
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
// 
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

// Symbol SDK common includes

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// CtlPanel includes
#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before audio.h
#ifdef _CRADLECTL

// Symbol CAPI include
#include <crdlcapi.h>
#include "cradle.h"

LPCRADLE_VERSION_INFO   l_lpCradleVersionInfo;
LPFNCRDL_GETVER         lpfnCrdlGetVersion;
LPFNCRDL_OPEN       	lpfnCrdlOpen;
LPFNCRDL_CLOSE      	lpfnCrdlClose;
LPFNCRDL_GETMODE    	lpfnCrdlGetMode;
LPFNCRDL_SETMODE    	lpfnCrdlSetMode;
LPFNCRDL_GETTYPE        lpfnCrdlGetType;
LPFNCRDL_GETSTATUS      lpfnCrdlGetStatus;

HINSTANCE				hCrdlLib = NULL;
DWORD					dwCradleType = 0;
DWORD					dwCradleMode = CRDL_ACCESS_SINGLE;
BOOL					bCradlePresent = TRUE;

BOOL Load_CradleAPI(void)
{

    hCrdlLib = LoadLibrary(_TCRDLAPI);
    
    if (hCrdlLib == NULL)
        return FALSE;
    
	lpfnCrdlOpen	    = (LPFNCRDL_OPEN)GetProcAddress(hCrdlLib,CRDL_OPEN);
	lpfnCrdlClose	    = (LPFNCRDL_CLOSE)GetProcAddress(hCrdlLib,CRDL_CLOSE);
	lpfnCrdlGetVersion  = (LPFNCRDL_GETVER)GetProcAddress(hCrdlLib,CRDL_GETVER);
  	lpfnCrdlGetMode     = (LPFNCRDL_GETMODE)GetProcAddress(hCrdlLib,CRDL_GETMODE);
  	lpfnCrdlSetMode     = (LPFNCRDL_SETMODE)GetProcAddress(hCrdlLib,CRDL_SETMODE);
    lpfnCrdlGetType     = (LPFNCRDL_GETTYPE)GetProcAddress(hCrdlLib,CRDL_GETTYPE);
    lpfnCrdlGetStatus   = (LPFNCRDL_GETSTATUS)GetProcAddress(hCrdlLib,CRDL_GETSTATUS);


	if ((lpfnCrdlGetVersion == NULL)
        || (lpfnCrdlOpen == NULL)
		|| (lpfnCrdlClose == NULL)
        || (lpfnCrdlGetMode == NULL)
        || (lpfnCrdlSetMode == NULL)
        || (lpfnCrdlGetType == NULL)
		|| (lpfnCrdlGetStatus == NULL) ) 
	{
		FreeLibrary(hCrdlLib);
		return FALSE;
	}

    return TRUE;
}

void GetCradleSettings(void)
{
	HANDLE			hCradle = NULL;
	DWORD			dwError,dwCradleStatus;
    
    dwError = lpfnCrdlGetMode(&dwCradleMode);
    
	dwError = lpfnCrdlGetStatus(&dwCradleStatus);

	if (dwCradleStatus)
	{
		bCradlePresent = TRUE;
	}
	else
	{
        bCradlePresent = FALSE;
	}

	if (bCradlePresent)
	{
		SetCursor(LoadCursor(NULL, IDC_WAIT));
		dwError = lpfnCrdlOpen(&hCradle);
		SetCursor(NULL);
		// if we got error, assume no cradle or the port is already opened by someone.
		if (dwError != E_CRDL_SUCCESS) 
		{
			ReportError(TEXT("Cradle Open Error"),dwError);
		}
	}

	l_lpCradleVersionInfo = (LPCRADLE_VERSION_INFO)LocalAlloc(LMEM_FIXED,
							sizeof(CRADLE_VERSION_INFO));

    SI_INIT(l_lpCradleVersionInfo);

	dwError = lpfnCrdlGetVersion(l_lpCradleVersionInfo);

   // we are OK on cradle Open, now get the type
    if (bCradlePresent)
    {
		dwCradleType = 0;

        // do one more getversion if we in the cradle.
    	dwError = lpfnCrdlGetVersion(l_lpCradleVersionInfo);

		dwError = lpfnCrdlGetType(&dwCradleType);

		// if the cradletype is 0, then we rather not show the type
		// it may means the cardle is not ready yet!
		if (dwCradleType == 0)
			bCradlePresent = FALSE;

	    // don't need the cradle port open anymore
		dwError = lpfnCrdlClose();
		hCradle = INVALID_HANDLE_VALUE;
    }

}

void EnterCradleSetting(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	DWORD dwResult = 0;

	LoadString(g_hInstance,IDS_CRADLESETTING,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;


    // load cradle api library
    if (Load_CradleAPI())
    {
	    GetCradleSettings();
    }
	else
	{
		MyError(TEXT("Cradle API Error!"),TEXT("Cradle"));
		return;
	}


	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)CradleSelector);
}


void ExitCradleSetting(LPITEMSELECTOR lpis,
								DWORD dwSelection)
{
    
    // accepted and is persistence
    if ( l_bDialogWasOKed )
    {
	    DWORD	dwError;
		// set the cradle mode
		dwError = lpfnCrdlSetMode(dwCradleMode);
	    if (dwError != E_CRDL_SUCCESS)
	    {
			// something wrong, either the port is still open !
			ReportError(TEXT("Set Cradle Mode Error"),dwError);
	    }
		else
		if (l_dwPersistence == PERSISTENCE_PERMANENT)
		{

	        // create a reg file
		    WriteRegFile(_TCradleSettingsKey,
						 _TAccessMode,
						 REG_DWORD,
						 &dwCradleMode,
						 sizeof(DWORD),
						 0);
		}

    }
	
	l_bDialogWasOKed = FALSE;

	FreeLibrary(hCrdlLib);
	LocalFree(l_lpCradleVersionInfo);

    l_lpExitFunction = NULL;

}

DWORD GetCradleMode(LPITEMSELECTOR lpis)
{
    return (dwCradleMode);
}

void SetCradleMode(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	// update the global variable.
	dwCradleMode = dwSelection;    
}

// need to convert the type info to english
// we just look at the HighWord of the cradle type, it should shows
//

static LPTSTR GetCradleType(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH64];

	// check for dwCradleType
	if (HIWORD(dwCradleType) < CRDL_7200_Single)
		dwCradleType = CRDL_7200_Single << 16;

	TEXTCPY(szMsg,lpszCradleType[HIWORD(dwCradleType) - CRDL_7200_Single]);    
	lpsz = szMsg;
    return lpsz;
}


static LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwCradleAPIVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwCradleAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetHardwareVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwCradleHardwareVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwCradleHardwareVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetFirmwareVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwCradleFirmwareVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwCradleFirmwareVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetIrDAVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwIrDADriverVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwIrDADriverVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetIrCommVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwIRCommDriverVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwIRCommDriverVersion));
    lpsz = szMsg;    

    return lpsz;
}

static LPTSTR GetDriverVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_lpCradleVersionInfo->dwCradleDriverVersion),
                                        LOWORD(l_lpCradleVersionInfo->dwCradleDriverVersion));
    lpsz = szMsg;    

    return lpsz;
}

#endif	// ifdef _CRADLECTL
