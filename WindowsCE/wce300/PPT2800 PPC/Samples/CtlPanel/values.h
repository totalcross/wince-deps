
//--------------------------------------------------------------------
// FILENAME:            values.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Header file for CtlPanel. Contains #define's, typedef's
//                  global exported functions.
//
// NOTES:               Public
//
// 
//--------------------------------------------------------------------

#ifndef VALUES_H_

#define VALUES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "resource.h"

// Program specific #defines
#ifdef _WIN32_WCE

	#define REG_SECTION HKEY_LOCAL_MACHINE
	#define REG_BASE_NAME TEXT("[HKEY_LOCAL_MACHINE\\")

	#define HELP_FILE_NAME TEXT("\\Help.txt")
	#define REG_FILE_PATH TEXT("\\")

    #ifndef _WIN32_WCE_EMULATION

		#if defined(WIN32_PLATFORM_7000)

			#undef HELP_FILE_NAME
			#undef REG_FILE_PATH

			#define HELP_FILE_NAME TEXT("\\Application\\User\\Demo\\Help.txt")
			#define REG_FILE_PATH TEXT("\\Application\\User\\Regs")

		#elif defined(WIN32_PLATFORM_SYMBOL) && !defined(SYMBOL_OEM_SCAN_KIT)

			#undef HELP_FILE_NAME
			#undef REG_FILE_PATH

			#define HELP_FILE_NAME TEXT("\\Application\\Help.txt")
			#define REG_FILE_PATH TEXT("\\Application")

		#endif

	#endif

#else

	#define REG_SECTION HKEY_CURRENT_USER
	#define REG_BASE_NAME TEXT("[HKEY_CURRENT_USER\\")
    #define HELP_FILE_NAME TEXT("e:\\WinCE SDK_APPS\\Sdkapps\\Help.txt")
    #define REG_FILE_PATH TEXT("d:\\sdkapps\\sdkapps")

#endif



#define RESOURCE_STRING(a)      ((LPTSTR)a)
#define RESOURCE_ID(p)			((DWORD)(p->lpszText))
#define countof(x)              (sizeof(x)/sizeof(x[0]))
#define PERSISTENCE_TEMPORARY	0
#define PERSISTENCE_PERMANENT	1
#define MAXTIMEOUT              MAXDWORD       // ~49.5 days.

// Typedef's 
typedef struct tagITEMSELECTOR ITEMSELECTOR;
typedef ITEMSELECTOR FAR * LPITEMSELECTOR;
typedef LPTSTR(*LPGETVALUEFUNCTION)(LPITEMSELECTOR lpis,DWORD dwIndex);
typedef DWORD (*LPGETCOUNTFUNCTION)(LPITEMSELECTOR lpis);
typedef void (*LPSELECTVALUEFUNCTION)(LPITEMSELECTOR lpis,DWORD dwSelection);
typedef DWORD (*LPGETINITIALINDEXFUNCTION)(LPITEMSELECTOR lpis);
typedef void (*LPENTERFUNCTION)(LPITEMSELECTOR lpis,DWORD dwSelection);
typedef void (*LPEXITFUNCTION)(LPITEMSELECTOR lpis,DWORD dwSelection);
typedef BOOL (*LPREFRESHFUNCTION)(LPITEMSELECTOR lpis,DWORD dwSelection);

enum tagVALUETYPE
{
	VT_STRINGLIST,
	VT_STRINGRANGE,
	VT_STRINGEDIT,
	VT_STRUCTURE,
	VT_NOVALUE,
	VT_END
};

typedef struct tagITEMSELECTOR
{
	LPBOOL lpbEnabled;
	LPTSTR lpszText;
	LPTSTR lpszHelpFile;

	DWORD dwValueType;
	// VT_STRINGLIST, VT_STRINGRANGE
	LPTSTR FAR * lplpszParamValuesList;
	DWORD dwCount;
	DWORD dwIndex;
	LPGETVALUEFUNCTION lpGetValueFunction;
	LPGETCOUNTFUNCTION lpGetCountFunction;
	LPGETINITIALINDEXFUNCTION lpGetInitialIndexFunction;
	LPSELECTVALUEFUNCTION lpSelectValueFunction;
	// VT_STRINGEDIT
	TCHAR szText[MAX_PATH128];
	// VT_STRUCTURE
	LPENTERFUNCTION lpEnterFunction;
	LPEXITFUNCTION lpExitFunction;

	LPREFRESHFUNCTION lpRefreshFunction;
	DWORD dwRefreshTime;


} ITEMSELECTOR;
typedef ITEMSELECTOR FAR * LPITEMSELECTOR;



extern TCHAR l_lpszHelpFileName[];
extern LPEXITFUNCTION l_lpExitFunction;
extern LPITEMSELECTOR l_lpItemSelector;
extern TCHAR l_szLastItem[MAX_PATH];
extern DWORD l_dwIndex;
extern DWORD l_dwPersistence;
extern BOOL l_bDialogWasOKed;
extern BOOL l_bUseWavFile;
extern BOOL l_bUseS24;
extern BOOL l_bValidateDword;
extern BOOL bReadOnly;


// Exported functions
LRESULT CALLBACK CtlPanelDlgProc(HWND hwnd,	UINT uMsg,
								 WPARAM wParam, LPARAM lParam);


DWORD WriteRegFile(LPTSTR lpszKeyName,  LPTSTR lpszValueName,
				   DWORD dwType, LPVOID lpvData, DWORD dwSize, BOOL bhkcu);   

BOOL MakePersistent(LPTSTR lpszRegKeyFullPath,
					LPTSTR lpszRegValueName);

BOOL WriteToRegistry(LPTSTR lpszRegKeyFullPath,
					 LPTSTR lpszRegValueName,
					 LPVOID lpvData,
					 DWORD dwLen,
					 DWORD dwType);

BOOL ReadFromRegistry(LPTSTR lpszRegKeyFullPath,
					  LPTSTR lpszRegValueName,
					  LPVOID lpvData,
					  LPDWORD lpdwSize,
					  LPDWORD lpdwType);

DWORD GetIndexFromValue(LPITEMSELECTOR lpis, DWORD dwValue);
DWORD GetValueFromIndex(LPITEMSELECTOR lpis, DWORD dwIndex);

void InitCtlPanel(LPITEMSELECTOR lpis, DWORD dwSelection, UINT uID);

// --- STRING_LIST ---
LPTSTR GetTextItem(LPITEMSELECTOR lpis, DWORD dwIndex);

// --- STRING_EDIT ---
LPTSTR GetEditText(LPITEMSELECTOR lpis, DWORD dwIndex);
DWORD GetEditCount(LPITEMSELECTOR lpis);
void EditTextItem(LPITEMSELECTOR lpis,  DWORD dwSelection);

// --- STRING_RANGE ---
DWORD GetRangeCount(LPITEMSELECTOR lpis);
LPTSTR GetRangeItem(LPITEMSELECTOR lpis, DWORD dwIndex);

// --- STRUCTURE_LIST
void EnterStructure(LPITEMSELECTOR lpis, DWORD dwSelection);

// --- substructure for ITEMSELECTOR ---
#define NO_HELP					    NULL

#define NO_VALUE				    VT_NOVALUE, \
								    NULL, \
								    0, \
								    0, \
								    NULL, \
								    NULL, \
								    NULL, \
								    NULL, \
								    TEXT(""), \
								    NULL, \
								    NULL

#define GET_STRING(gv,sf)      	    VT_NOVALUE, \
								    NULL, \
								    0, \
								    0, \
								    gv, \
								    NULL, \
								    NULL, \
								    sf , \
								    TEXT(""), \
								    NULL, \
								    NULL

#define STRING_LIST(vl,ii,sf,ic)	VT_STRINGLIST, \
									vl, \
									(sizeof(vl)/sizeof(LPTSTR)-1), \
									0, \
									GetTextItem, \
									ic, \
									ii, \
									sf, \
									TEXT(""), \
									NULL, \
									NULL

#define STRING_RANGE(vl,ii,sf)		VT_STRINGRANGE, \
									vl, \
									(sizeof(vl)/sizeof(LPTSTR)-1), \
									0, \
									GetRangeItem, \
									GetRangeCount, \
									ii, \
									sf, \
									TEXT(""), \
									NULL, \
									NULL

#define STRING_EDIT(sv,ii,sf)		VT_STRINGEDIT, \
									NULL, \
									0, \
									0, \
									GetEditText, \
									GetEditCount, \
									ii, \
									EditTextItem , \
									sv, \
									NULL, \
									sf

#define STRUCTURE_LIST(en,ex)		VT_STRUCTURE, \
									NULL, \
									0, \
									0, \
									NULL, \
									NULL, \
									NULL, \
									EnterStructure, \
									TEXT(""), \
									en, \
									ex

#define END_ITEMSELECTOR	{		NULL, \
									NULL, \
									NULL, \
									VT_END, \
									NULL, \
									0, \
									0, \
									NULL, \
									NULL, \
									NULL, \
									NULL, \
									TEXT(""), \
									NULL, \
									NULL }





#ifdef __cplusplus
}
#endif

#endif  /* #ifndef VALUES_H_    */

