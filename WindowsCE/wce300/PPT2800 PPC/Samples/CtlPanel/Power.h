
//--------------------------------------------------------------------
// FILENAME:        Power.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Power.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef POWER_H_

#define POWER_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"
#include <strucinf.h>


#define POWER_DRIVER_REG TEXT("hklm\\Drivers\\BuiltIn\\PowerDrv\\Load\\")
#define POWER_DRIVER_SYSTEM TEXT("System")
#define POWER_WARMBOOTTIMEOUT TEXT("WarmBootTimeout")
#define POWER_COLDBOOTTIMEOUT TEXT("ColdBootTimeout")
#define POWER_POWERUPACTIVITIES TEXT("PowerUpActivities")


typedef struct tagDEVICEATTRIBUTE
{ 
    STRUCT_INFO StructInfo;

    TCHAR szDeviceName[MAX_PATH32];
    
    DWORD dwDeviceState;            // hold current device state
    DWORD dwSupportedStates;
    
    struct tagActivityMask
    {
        DWORD HyperDeviceMask;     
        DWORD ActiveDeviceMask;
        DWORD DozeDeviceMask;       // N/A for Backlight
        DWORD SleepDeviceMask;
    } ActivityMask;

    DWORD dwWakeUpSource;       // N/A for Backlight
    
    struct tagTimeOut
    {
        DWORD ActiveTimeOut;    
        DWORD DozeTimeOut;      // N/A for backlight
        DWORD SleepTimeOut;     // N/A for Backlight
        DWORD SuspendTimeOut;   // N/A for Backlight 
    } TimeOut;

} DEVICEATTRIBUTE;

typedef DEVICEATTRIBUTE FAR * LPDEVICEATTRIBUTE;

#define BACKLIGHT   TEXT("Backlight")
#define SYSTEM      TEXT("SYSTEM")

// indicate the power info
#define BATTERY_TYPE        1
#define BATTERY_LEVEL       2
#define BATTERY_SOURCE      3

#define BATTERY_INTERNAL    5
#define BATTERY_EXTERNAL    6
#define BATTERY_GOOD        7
#define BATTERY_LOW         8
#define BATTERY_VERYLOW     9
#define BATTERY_NO_BATTERY  10

// variable used only on power.c
static BOOL bDozeSupported = TRUE;
static BOOL bSuspendSupported = TRUE;
static BOOL bHyperSupported = TRUE;
static BOOL bSleepSupported = TRUE;
static BOOL bWakeupSupported = TRUE;
static BOOL bSystemDevice = TRUE;
static BOOL bBacklightDevice = FALSE;


static LPTSTR lpszRange3to31by1[] =
{
	RESOURCE_STRING(IDS_3),
	RESOURCE_STRING(IDS_31),
	RESOURCE_STRING(IDS_1),
	NULL
};


static LPTSTR lpszValuesNoYes[] =
{
	RESOURCE_STRING(IDS_NO),
	RESOURCE_STRING(IDS_YES),
	NULL
};


LPTSTR lpszPowerInfo[] =
{
	TEXT("No Battery"),
	TEXT("Alkaline"),
	TEXT("NICAD"),
	TEXT("NIMH"),
	TEXT("Lithium ION"),
	TEXT("Internal"),   // 5
	TEXT("External"),
    TEXT("Good"),
    TEXT("Low"),
    TEXT("Very Low"),
    TEXT("No Battery"),
	NULL
};

LPTSTR lpszSystemDeviceState[] =
{
	TEXT("Hyper"),
	TEXT("Active"),
	TEXT("Doze"),
	TEXT("Sleep"),
	TEXT("Suspend"),
	NULL
};

LPTSTR lpszBacklightDeviceState[] =
{
	TEXT("Active"),
	TEXT("Sleep"),
	TEXT("Suspend"),
	NULL,
   	TEXT("Not Supported")

};

/*
// activities not included=> PCMCIA0, PCMCIA1
LPTSTR lpszDeviceMask[] =
{
	TEXT("Com1 Ring"),
	TEXT("Com4 Ring"),
	TEXT("Com5 Ring"),
	TEXT("Com1 Data"),
	TEXT("Com4 Data"),
	TEXT("Com5 Data"),
	TEXT("Alarm"),
	TEXT("TouchScreen"),
	TEXT("Keyboard"),
	TEXT("Trigger 1"),
	TEXT("Trigger 2"),
	TEXT("Trigger 3"),
	TEXT("Cradle Insertion"),
	TEXT("Cradle Removal"),
	TEXT("AC Connection"),
	TEXT("Power Switch"),
	TEXT("Battery Insertion"),
	TEXT("Battery Eject"),
	TEXT("Software Activity"),
	NULL
};
*/
// Ctl Power Management 
void EnterPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection);

// Power Management Selection
void EnterPowerVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPowerVersion(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterPowerInfo(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPowerInfo(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterSystemDevice(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitSystemDevice(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterBacklightDevice(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitBacklightDevice(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterRebootTimes(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitRebootTimes(LPITEMSELECTOR lpis,DWORD dwSelection);

// Power Version Functions
LPTSTR GetPowerMDDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPowerCAPIVersion(LPITEMSELECTOR lpis,DWORD dwIndex);

// Power Info Functions
LPTSTR GetBatteryType(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetBatteryLevel(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPowerSource(LPITEMSELECTOR lpis,DWORD dwIndex);

// Device Attribute Selection
void SetDeviceState(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialState(LPITEMSELECTOR lpis);

void EnterDeviceActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitDeviceActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterWakeUpSource(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitWakeUpSource(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterDeviceTimeOut(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitDeviceTimeOut(LPITEMSELECTOR lpis,DWORD dwSelection);

// Activity Mask Functions
// Wakeup Source Functions
void EnterActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection);

void SetMaskValue(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialMaskValue(LPITEMSELECTOR lpis);

// TimeOut Selection
void SetTimeOut(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialTimeOut(LPITEMSELECTOR lpis);

// Warm Boot Time Selection
void SetWarmBootTime(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetWarmBootTime(LPITEMSELECTOR lpis);

// Cold Boot Time Selection
void SetColdBootTime(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetColdBootTime(LPITEMSELECTOR lpis);

// Set device attribute
ITEMSELECTOR DeviceAttributeSelector[] =
{
	{
		&bSystemDevice,
		RESOURCE_STRING(IDS_STATE),
		l_lpszHelpFileName,
    	STRING_LIST(lpszSystemDeviceState,GetInitialState,SetDeviceState, NULL)
    },
	{
		&bBacklightDevice,
		RESOURCE_STRING(IDS_STATE),
		l_lpszHelpFileName,
    	STRING_LIST(lpszBacklightDeviceState,GetInitialState,SetDeviceState, NULL)
    },

	{
		NULL,
		RESOURCE_STRING(IDS_ACTIVITYMASK),
		l_lpszHelpFileName,
    	STRUCTURE_LIST(EnterDeviceActivityMask,ExitDeviceActivityMask)
    },
	{
		&bWakeupSupported,
		RESOURCE_STRING(IDS_WAKEUPSOURCE),
		l_lpszHelpFileName,
    	STRUCTURE_LIST(EnterWakeUpSource,ExitWakeUpSource)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_TIMEOUT),
		l_lpszHelpFileName,
    	STRUCTURE_LIST(EnterDeviceTimeOut,ExitDeviceTimeOut)
    },

	END_ITEMSELECTOR
};


ITEMSELECTOR RebootTimesSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_WARMBOOTTIME),
		NO_HELP,
		STRING_RANGE(lpszRange3to31by1,GetWarmBootTime,SetWarmBootTime)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_COLDBOOTTIME),
		NO_HELP,
		STRING_RANGE(lpszRange3to31by1,GetColdBootTime,SetColdBootTime)
    },

	END_ITEMSELECTOR
};


ITEMSELECTOR TimeOutSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_ACTIVETIMEOUT),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetInitialTimeOut,SetTimeOut)
    },
	{
		&bDozeSupported,
		RESOURCE_STRING(IDS_DOZETIMEOUT),
        NO_HELP,
   		STRING_EDIT(TEXT(""),GetInitialTimeOut,SetTimeOut)
    },
	{
		&bSleepSupported,
		RESOURCE_STRING(IDS_SLEEPTIMEOUT),
		NO_HELP,
   		STRING_EDIT(TEXT(""),GetInitialTimeOut,SetTimeOut)
    },
	{
		&bSuspendSupported,
		RESOURCE_STRING(IDS_SUSPENDTIMEOUT),
        NO_HELP,
   		STRING_EDIT(TEXT(""),GetInitialTimeOut,SetTimeOut)
    },

	END_ITEMSELECTOR
};

ITEMSELECTOR ActivityMaskSelector[] =
{
	{

        NULL,
        //		&bHyperSupported,
		RESOURCE_STRING(IDS_HYPERSTATE),
		NO_HELP,
        STRUCTURE_LIST(EnterActivityMask,ExitActivityMask)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_ACTIVESTATE),
		NO_HELP,
        STRUCTURE_LIST(EnterActivityMask,ExitActivityMask)
    },
	{
//        NULL,
		&bDozeSupported,
		RESOURCE_STRING(IDS_DOZESTATE),
		NO_HELP,
        STRUCTURE_LIST(EnterActivityMask,ExitActivityMask)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_SLEEPSTATE),
		NO_HELP,
        STRUCTURE_LIST(EnterActivityMask,ExitActivityMask)
    },
	END_ITEMSELECTOR
};


BOOL l_bTouchAllowed = TRUE;


ITEMSELECTOR ActivitySelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_COM1RING),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		&bDozeSupported,
		RESOURCE_STRING(IDS_COM4RING),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_COM5RING),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_COM1DATA),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		&bDozeSupported,
		RESOURCE_STRING(IDS_COM4DATA),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_COM5DATA),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_ALARM),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		&l_bTouchAllowed,
		RESOURCE_STRING(IDS_TOUCH),
        NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_KEYBOARD),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_TRIGGER1),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_TRIGGER2),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_TRIGGER3),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_CRADLEINSERTION),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_CRADLEREMOVAL),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_ACCONNECTION),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_POWERSWITCH),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_BATTERYINSERT),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_BATTERYEJECT),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_SOFTWAREACTIVITY),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_PCMCIA0),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_PCMCIA1),
		NO_HELP,
    	STRING_LIST(lpszValuesNoYes,GetInitialMaskValue,SetMaskValue,NULL)
    },
	END_ITEMSELECTOR
};

ITEMSELECTOR PowerInfoSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_BATTERYTYPE),
		NO_HELP,
        GET_STRING(GetBatteryType,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_BATTERYLEVEL),
		NO_HELP,
		GET_STRING(GetBatteryLevel,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POWERSOURCE),
		NO_HELP,
		GET_STRING(GetPowerSource,NULL)
	},
	
	END_ITEMSELECTOR

};

ITEMSELECTOR PowerVersionSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_MDDVERSION),
		NO_HELP,
		GET_STRING(GetPowerMDDVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetPowerCAPIVersion,NULL)
	},
	
	END_ITEMSELECTOR

};
ITEMSELECTOR PowerManagementSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_GETVERSION),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPowerVersion,ExitPowerVersion)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_POWERGETINFO),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPowerInfo,ExitPowerInfo)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_SYSTEM),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterSystemDevice,ExitSystemDevice)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_POWERBKLITE),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterBacklightDevice,ExitBacklightDevice)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_REBOOTTIMES),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterRebootTimes,ExitRebootTimes)
    },

	END_ITEMSELECTOR
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef POWER_H_ */
