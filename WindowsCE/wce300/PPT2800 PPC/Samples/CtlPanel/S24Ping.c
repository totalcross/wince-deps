//--------------------------------------------------------------------
// FILENAME:            S24Ping.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to provide Ping functionality to control panel
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>
#include <winsock.h>

// Symbol SDK common includes

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// Ctlpanel includes
#include "ctlpanel.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _S24CTL

#include "S24Ping.h"
#include "S24.h"

#ifdef _POWERCTL

#include "PwrCapi.h"

#endif

// defined in S24API.H
S24PINGINFO	S24PingInfo;
S24ICMPINFO S24IcmpInfo;
PBYTE		pData;


extern S24INFO S24Info;

HANDLE hWndPingFrom;
HANDLE hWndPingTo;
HANDLE hWndPingDataSize;
HANDLE hWndPingStatus;


#ifdef _POWERCTL

DWORD	dwPwrResult = 0;
BOOL	bSuspendRequested = FALSE;
POWER_INFO	PowerInfo;

#endif


BOOL ICMPPing(PWORD pnRoundTripTime)
{
	HANDLE	hIcmp;
	DWORD	dwReplyCount = 0;
	char	chIpAddr[16];
	BYTE	Reply[sizeof(ICMP_ECHO_REPLY) + 8];

	hIcmp = IcmpCreateFile();

	if (INVALID_HANDLE_VALUE == hIcmp)
	{
		MyError(TEXT("ICMPPing"),TEXT("Icmp CreateFile failed"));
	}

	*pnRoundTripTime = 0;			// initialize to 0

	wcstombs(chIpAddr, S24IcmpInfo.szIPAddr, 15);

	dwReplyCount = IcmpSendEcho( hIcmp, inet_addr(chIpAddr),
								pData, sizeof(pData),
								NULL, Reply,
								sizeof(Reply), 3000);

	IcmpCloseHandle(hIcmp);

	if (dwReplyCount == 0)
		return FALSE;

	*pnRoundTripTime = (WORD)(( ICMP_ECHO_REPLY *) &Reply) ->RoundTripTime;

	return TRUE;
}
						

// lParam tell which ping to use
LRESULT CALLBACK S24PingProc(HWND hwnd,
									  UINT uMsg,
									  WPARAM wParam,
									  LPARAM lParam)
{
	DWORD dwResult;
	TCHAR szMsg[MAX_PATH];
	static DWORD dwCount=0;
	static DWORD dwRcvLength;
	static WORD  nPingParm = 0;
	static DWORD dwTotalPingTime;
	static DWORD dwAveragePingTime;
	static WORD	nMinTime,nMaxTime;

	DWORD i;
	WORD nRoundTripTime;

    switch(uMsg)
    {
		case UM_RESIZE:
			
			// Resize this dialog, no save info required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				HFONT hFont;
			
				switch(GetUIStyle())
				{
					case UI_STYLE_PEN:
						hFont = SetNewDialogFont(hwnd,120);
						break;
					case UI_STYLE_FINGER:
						hFont = SetNewDialogFont(hwnd,130);
						break;
					case UI_STYLE_KEYPAD:
						hFont = SetNewDialogFont(hwnd,110);
						break;
					default:
						hFont = SetNewDialogFont(hwnd,100);
						break;
				}

				MaxDialog(hwnd);
				
				l_bDialogWasOKed = FALSE;

				hWndPingFrom = GetDlgItem(hwnd,IDC_BASEADDRESS);
				hWndPingTo = GetDlgItem(hwnd,IDC_REMOTEADDRESS);
				hWndPingDataSize = GetDlgItem(hwnd,IDC_PINGDATASIZE);
				hWndPingStatus = GetDlgItem(hwnd,IDC_PINGSTATUS);

				// if it's ICMP ping, convert the IP addree for display

				// if it's WNMP ping, convert the MU address
				dwCount = 0;
				nMinTime = 9999;
				nMaxTime = 0;
				//PJK 07-18-01
				//Initialize static variables to 0
				dwTotalPingTime = 0;
				dwAveragePingTime = 0;
	
#ifdef _POWERCTL

				POWER_Open();

				// Prohibit suspend during any socket activity...
				dwPwrResult = POWER_ProhibitDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
				if(E_PWR_SUCCESS != dwPwrResult)
				{
					MyError(TEXT("Ping"),TEXT("Prohibit suspend failed!!"));
				}

#endif
	
				nPingParm = (WORD)lParam;
				if (nPingParm == 1)	//ICMP
				{
					char chHostName[32];
					char *chIpAddr;
					int  nError;
					HOSTENT	*pHost;
					IN_ADDR	in_addr;

					// get terminal IP address
					nError = gethostname(chHostName,31);
					if (nError)
					{
						//	ReportError(TEXT("gethostname error"),nError);
					}

					pHost = gethostbyname(chHostName);

					if (pHost == NULL)
					{
						//	ReportError(TEXT("gethostbyname error"),nError);
					}
					
					memcpy(&in_addr, pHost->h_addr, pHost->h_length);

					chIpAddr = inet_ntoa(in_addr);
					
					mbstowcs(szMsg,chIpAddr,16);
					
					Static_SetText(hWndPingFrom,szMsg);

					Static_SetText(hWndPingTo,S24IcmpInfo.szIPAddr);
					
					wsprintf(szMsg,TEXT("%d"),S24IcmpInfo.dwDataSize);
					Static_SetText(hWndPingDataSize,szMsg);

					Static_SetText(hWndPingStatus,TEXT("Waiting..."));
					pData = (PBYTE)LocalAlloc(LMEM_FIXED,S24IcmpInfo.dwDataSize);
					if (pData == NULL)
					{
						MyError(TEXT("ICMPPing"),TEXT("Failed to Allocate Buffer"));
						return 0;
					}

					for (i=0;i<S24IcmpInfo.dwDataSize; i++)
						pData[i] = 0x5A;

					SetTimer(hwnd, S24_ICMP_PING_EVENT, 1000, NULL);
					
				}
				else
				{
				S24PingInfo.hWnd = NULL;
				S24PingInfo.nPingSequence = 0x1001;
				S24PingInfo.nSizeOfPing = 300;
				for (i=0; i<S24PingInfo.nSizeOfPing; i++)
					S24PingInfo.PingInputBuffer[i] = 0xA5;

				wsprintf(szMsg,TEXT("%02X%02X%02X%02X%02X%02X%"),
						  S24Info.bMUAddress[0],
						  S24Info.bMUAddress[1],
						  S24Info.bMUAddress[2],
						  S24Info.bMUAddress[3],
						  S24Info.bMUAddress[4],
						  S24Info.bMUAddress[5]);

				Static_SetText(hWndPingFrom,szMsg);

				memcpy(S24PingInfo.bMacAddress, S24Info.bMUAddress,6);

				Static_SetText(hWndPingTo,szMsg);

				wsprintf(szMsg,TEXT("%d"),S24PingInfo.nSizeOfPing);
				Static_SetText(hWndPingDataSize,szMsg);

				Static_SetText(hWndPingStatus,TEXT("Waiting..."));
				SetTimer(hwnd, S24_WNMP_PING_EVENT, 3000, NULL);
				}
				
				SetFocus(hWndPingStatus);
				
				SetWindowText(hwnd,l_szLastItem);

			}
			
			// Return FALSE since focus was manually set
			return(FALSE);
      
		case WM_TIMER:

			switch (wParam)
			{
			// process S24_EVENT
			case S24_WNMP_PING_EVENT:
					// update S24Quality, roaming status, and association
				dwResult = S24_SendWNMP(&S24PingInfo);
				switch (dwResult)
				{
					TCHAR szMsg[MAX_PATH];

				case 0x8000:
					wsprintf(szMsg,TEXT("Ping Sent and Received OK %d"),dwCount++);
					Static_SetText(hWndPingStatus,szMsg);
					break;
				case 0x8001:
					Static_SetText(hWndPingStatus,TEXT("Bad Ping Data"));
					break;
				case RC_PENDING:
					Static_SetText(hWndPingStatus,TEXT("Pending..."));
					break;

				default:
					wsprintf(szMsg,TEXT("Error... %d"),dwResult);
					Static_SetText(hWndPingStatus,szMsg);
					break;
				}
				break;
			case S24_ICMP_PING_EVENT:
				
				if (ICMPPing(&nRoundTripTime))
				 {
					 TCHAR szMsg[MAX_PATH];

					 // update display
					 wsprintf(szMsg,TEXT("Ping OK, RTT = %d ms"),nRoundTripTime);
					 Static_SetText(hWndPingStatus,szMsg);
					 if (nRoundTripTime < nMinTime)
						 nMinTime = nRoundTripTime;
					 if (nRoundTripTime > nMaxTime)
						 nMaxTime = nRoundTripTime;
				 }
				 else
				 {
					 Static_SetText(hWndPingStatus, TEXT("Ping Error"));
				 }

				 dwCount++;
				 dwTotalPingTime += nRoundTripTime;
				 dwAveragePingTime = dwTotalPingTime / dwCount;

#ifdef _POWERCTL

				// We can suspend now if requested...
				if(TRUE == bSuspendRequested)
				{

					// Stop the timer...
					KillTimer(hwnd,S24_ICMP_PING_EVENT);

					// Re-allow suspends...
					dwPwrResult = POWER_AllowDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
					if(E_PWR_SUCCESS != dwPwrResult)
					{
						MyError(TEXT("ICMPPing"),TEXT("Allow suspend#1 failed!"));
					}

#if 0					
RETAILMSG(1,(TEXT("Ping: About to suspend.\r\n")));
#endif					
					
					// Perform the suspend...
					dwPwrResult = POWER_SetDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);

					if(E_PWR_SUCCESS != dwPwrResult)
					{
						MyError(TEXT("ICMPPing"),TEXT("Suspend attempt failed!"));
					}

					// Now re-prohibit suspends...
					dwPwrResult = POWER_ProhibitDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
					if(E_PWR_SUCCESS != dwPwrResult)
					{
						MyError(TEXT("ICMPPing"),TEXT("Prohibit suspend failed!"));
					}

#if 0
RETAILMSG(1,(TEXT("Ping: Back from Re-prohibit.\r\n")));
#endif					
					
					// Suspend sequence complete...
					bSuspendRequested = FALSE;

					// Restart timer...
					SetTimer(hwnd, S24_ICMP_PING_EVENT, 1000, NULL);
				}

				// Check for low / very low battery...
				PowerInfo.StructInfo.dwAllocated = sizeof(PowerInfo);
				PowerInfo.StructInfo.dwUsed = 0;
				if (POWER_GetPowerInfo(&PowerInfo) == E_PWR_SUCCESS)
				{
					if ((PowerInfo.dwBatteryLevel & POWER_STATUS_LOW) ||
						(PowerInfo.dwBatteryLevel & POWER_STATUS_VERY_LOW))
					{
						// Battery is low or very low. Tell user, kill timer...
						Static_SetText(hWndPingStatus, TEXT("Low Battery"));

						KillTimer(hwnd,S24_ICMP_PING_EVENT);
					
						// Re-allow suspends so we can go to sleep if we timeout...
						dwPwrResult = POWER_AllowDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
						if(E_PWR_SUCCESS != dwPwrResult)
						{
							MyError(TEXT("ICMPPing"),TEXT("Allow suspend#2 failed!"));
						}						

						break;
					}
				}

#endif


				if ((dwCount >= S24IcmpInfo.dwPingCount) && ( S24IcmpInfo.dwPingCount))
				{
					 KillTimer(hwnd,S24_ICMP_PING_EVENT);
				}
				break;
				
			}
			return 0;
		
        case WM_COMMAND:

			// Select activity based on command
			switch (LOWORD(wParam))
            {

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:
						case UI_STYLE_FINGER:

								InvokePopUpMenu(hwnd,IDM_POPUP,3,0,0);
							
							break;

						case UI_STYLE_PEN:
							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;
				case IDC_REFRESH:
				
					KillTimer(hwnd,S24_WNMP_PING_EVENT);
					KillTimer(hwnd,S24_ICMP_PING_EVENT);
					LocalFree(pData);

#ifdef _POWERCTL

					// Allow suspend again - end of socket activity...
					dwPwrResult = POWER_AllowDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
					if(E_PWR_SUCCESS != dwPwrResult)
					{
						MyError(TEXT("PPing"),TEXT("Allow suspend#3 failed!!"));
					}
	
					POWER_Close();

#endif

					PostMessage(hwnd,WM_INITDIALOG,0,(long)nPingParm);

					break;

				case IDC_STAT:	// show statistic
					{
						// total ping = dwCount;
						// average ping = dwAveragePingTime
						TCHAR szTotal[MAX_PATH];
						TCHAR szAverage[MAX_PATH];

						// kill the timer first
						KillTimer(hwnd,S24_WNMP_PING_EVENT);
						KillTimer(hwnd,S24_ICMP_PING_EVENT);
						LocalFree(pData);

						//PJK 07-17-01
						//The S24 function that is called for the WNMP (MAC level)
						//ping does not return the time that the ping took.  For this
						//reason, we cannot display a Min and Max value or calculate 
						//the average.  In the case of a WNMP ping the value for the
						//nMinTime will still be set to the value to which it was 
						//initialized in WM_INITDIALOG (nMinTime = 9999).  If this 
						//is the case, we will initialize the text string for Min Time
						//MaxTime and Average to N/A				
						if(9999 == nMinTime)
						{
							//This is a WNMP ping
							wsprintf(szTotal,TEXT("Total Ping counts: %ld\r\nMin/Max Time: N/A"),
									dwCount);
							wsprintf(szAverage,TEXT("Average Ping time: N/A"));
						}
						else
						{
							wsprintf(szTotal,TEXT("Total Ping counts: %ld\r\nMin/Max Time: %ld ms/ %ld ms"),
									dwCount,nMinTime,nMaxTime);
							wsprintf(szAverage,TEXT("Average Ping time: %ld ms"),dwAveragePingTime);
						}
						MyMessage(szTotal,szAverage);
						break;
					}

				case IDOK:

					l_bDialogWasOKed = TRUE;

					// Fall through

				case IDCANCEL:

					KillTimer(hwnd,S24_WNMP_PING_EVENT);
					KillTimer(hwnd,S24_ICMP_PING_EVENT);
					LocalFree(pData);

#ifdef _POWERCTL

					// Allow suspend again - end of socket activity...
					dwPwrResult = POWER_AllowDeviceState(TEXT("SYSTEM"),DEVICE_STATE_SUSPEND);
	
					if(E_PWR_SUCCESS != dwPwrResult)
					{
						MyError(TEXT("Ping"),TEXT("Allow suspend#4 failed!!"));
					}
	
					POWER_Close();

#endif

					// Exit the dialog
					if (ExitDialog())
					{
				
						if ( l_lpExitFunction != NULL )
						{
							(*l_lpExitFunction)(l_lpItemSelector,l_dwIndex);
						}

					}
					
					break;

				case IDDONE:
					SendMessage(g_hwnd,WM_CLOSE,0,0L);
					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}

void SetPingAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	// check to see is szText is a valid IP address
    char szAdd[16];
	DWORD dwRetCode;
    
    // check to see if it's a valid IP address type
    // assume dot notation
    wcstombs(szAdd,lpis->szText,15);
    if ((dwRetCode = inet_addr(szAdd)) == INADDR_NONE)
    {
        MyError(TEXT("Invalid IP Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24IcmpInfo.szIPAddr);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
        return;
    }

	TEXTCPY(S24IcmpInfo.szIPAddr,lpis->szText);
}

// get the initial IP address
DWORD GetPingAddress(LPITEMSELECTOR lpis)
{
    TEXTCPY(lpis->szText,S24IcmpInfo.szIPAddr);
	return 0;
}

void SetPingSize(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24IcmpInfo.dwDataSize = TEXTTOL(lpszPingDataSize[dwSelection]);
}


DWORD GetPingSize(LPITEMSELECTOR lpis)
{

	DWORD dwIndex = 0;
	DWORD dwValue;
	int i;

	i=0;
	while (lpszPingDataSize[i])
	{
		dwValue = TEXTTOL(lpszPingDataSize[i]);
		if (dwValue == S24IcmpInfo.dwDataSize)
		{
			dwIndex = i;
			break;
		}
		i++;
	}

	return(dwIndex);

}

void SetPingCount(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24IcmpInfo.dwPingCount = TEXTTOL(lpszPingCount[dwSelection]);
}

DWORD GetPingCount(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;
	DWORD dwValue;
	int i;

	i=0;
	while (lpszPingCount[i])
	{
		dwValue = TEXTTOL(lpszPingCount[i]);
		if (dwValue == S24IcmpInfo.dwPingCount)
		{
			dwIndex = i;
			break;
		}
		i++;
	}

	return(dwIndex);

}
#endif
