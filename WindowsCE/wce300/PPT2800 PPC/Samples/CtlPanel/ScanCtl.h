
//--------------------------------------------------------------------
// FILENAME:        ScanCtl.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for ScanCtl.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANCTL_H_

#define SCANCTL_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"


extern BOOL l_bScannerV2Plus;


static LPTSTR lpszRange0to10by1[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_10),
	RESOURCE_STRING(IDS_1),
	NULL
};


static LPTSTR lpszRange0to100by1[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_100),
	RESOURCE_STRING(IDS_1),
	NULL
};


static LPTSTR lpszRange0to500by10[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_500),
	RESOURCE_STRING(IDS_10),
	NULL
};


#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)

static TCHAR szBeeperFrequencyMinimum[MAX_PATH];
static TCHAR szBeeperFrequencyMaximum[MAX_PATH];
static TCHAR szBeeperFrequencyStep[MAX_PATH];

static LPTSTR lpszRangeBeeperFrequency[] =
{
	szBeeperFrequencyMinimum,
	szBeeperFrequencyMaximum,
	szBeeperFrequencyStep,
	NULL
};


static TCHAR szBeeperDurationMinimum[MAX_PATH];
static TCHAR szBeeperDurationMaximum[MAX_PATH];
static TCHAR szBeeperDurationStep[MAX_PATH];

static LPTSTR lpszRangeBeeperDuration[] =
{
	szBeeperDurationMinimum,
	szBeeperDurationMaximum,
	szBeeperDurationStep,
	NULL
};

#endif


static TCHAR szLedDurationMinimum[MAX_PATH];
static TCHAR szLedDurationMaximum[MAX_PATH];
static TCHAR szLedDurationStep[MAX_PATH];

static LPTSTR lpszRangeLedDuration[] =
{
	szLedDurationMinimum,
	szLedDurationMaximum,
	szLedDurationStep,
	NULL
};


static LPTSTR lpszRange0to60000by1000[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_60000),
	RESOURCE_STRING(IDS_1000),
	NULL
};


static LPTSTR lpszRange0to1000by50[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_1000),
	RESOURCE_STRING(IDS_50),
	NULL
};

static LPTSTR lpszRange0to1000by100[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_1000),
	RESOURCE_STRING(IDS_100),
	NULL
};


static LPTSTR lpszRange1to32by1[] =
{
	RESOURCE_STRING(IDS_1),
	RESOURCE_STRING(IDS_32),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszValuesLowHigh[] =
{
	RESOURCE_STRING(IDS_LOW),
	RESOURCE_STRING(IDS_HIGH),
	NULL
};

static LPTSTR lpszRange0to60000by100[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_60000),
	RESOURCE_STRING(IDS_100),
	NULL
};

static LPTSTR lpszValueLaserReaderType[] =
{
	RESOURCE_STRING(IDS_LASER),
	NULL
};


static LPTSTR lpszValuesAimType[] =
{
	RESOURCE_STRING(IDS_AIMTRIGGER),
	RESOURCE_STRING(IDS_AIMTIMEDHOLD),
	RESOURCE_STRING(IDS_AIMTIMEDRELEASE),
	NULL
};


static LPTSTR lpszValuesAimMode[] =
{
	RESOURCE_STRING(IDS_NONE),
	RESOURCE_STRING(IDS_DOT),
	RESOURCE_STRING(IDS_SLAB),
	RESOURCE_STRING(IDS_RETICLE),
	NULL
};


static LPTSTR lpszValuesBeamWidth[] =
{
	RESOURCE_STRING(IDS_NORMAL),
	RESOURCE_STRING(IDS_NARROW),
	NULL
};


static LPTSTR lpszValuesRasterMode[] =
{
	RESOURCE_STRING(IDS_NONE),
	RESOURCE_STRING(IDS_OPENALWAYS),
	RESOURCE_STRING(IDS_SMART),
	RESOURCE_STRING(IDS_CYCLONE),
	NULL
};


static LPTSTR lpszValuesFalseTrue[] =
{
	RESOURCE_STRING(IDS_FALSE),
	RESOURCE_STRING(IDS_TRUE),
	NULL
};


static LPTSTR lpszValuesLinearSecurity[] =
{
	RESOURCE_STRING(IDS_REDANDLEN),
	RESOURCE_STRING(IDS_SHORTORCODABAR),
	RESOURCE_STRING(IDS_ALLTWICE),
	RESOURCE_STRING(IDS_LONGANDSHORT),
	RESOURCE_STRING(IDS_ALLTHRICE),
	NULL
};


static LPTSTR lpszValueContactReaderType[] =
{
	RESOURCE_STRING(IDS_CONTACT),
	NULL
};

static LPTSTR lpszValueImagerReaderType[] =
{
	RESOURCE_STRING(IDS_IMAGER),
	NULL
};

static LPTSTR lpszValuesCodeIdType[] =
{
	RESOURCE_STRING(IDS_NONE),
	RESOURCE_STRING(IDS_SYMBOL),
	RESOURCE_STRING(IDS_AIM),
	NULL
};


static LPTSTR lpszValuesScanType[] =
{
	RESOURCE_STRING(IDS_FOREGROUND),
	RESOURCE_STRING(IDS_BACKGROUND),
	RESOURCE_STRING(IDS_MONITOR),
	NULL
};


static LPTSTR lpszValuesQuietZoneRatio[] =
{
	RESOURCE_STRING(IDS_1TO1),
	RESOURCE_STRING(IDS_2TO1),
	RESOURCE_STRING(IDS_4TO1),
	RESOURCE_STRING(IDS_8TO1),
	NULL
};


static LPTSTR lpszValueQSNACInterfaceType[] =
{
	RESOURCE_STRING(IDS_QSNAC),
	NULL
};


static LPTSTR lpszValueSSIInterfaceType[] =
{
	RESOURCE_STRING(IDS_SSI),
	NULL
};


static LPTSTR lpszValueLS48XXInterfaceType[] =
{
	RESOURCE_STRING(IDS_LS48XX),
	NULL
};


static LPTSTR lpszValuesRedundancy[] =
{
	RESOURCE_STRING(IDS_NONE),
	RESOURCE_STRING(IDS_BIDIR),
	NULL
};



static LPTSTR lpszValuesScannerName[11];

/*static LPTSTR lpszValuesScannerName[] =
{
	TEXT("SCN1:"),
	TEXT("SCN2:"),
	TEXT("SCN3:"),
	TEXT("SCN4:"),
	TEXT("SCN5:"),
	TEXT("SCN6:"),
	TEXT("SCN7:"),
	TEXT("SCN8:"),
	TEXT("SCN9:"),
	TEXT("SCN0:"),
	NULL
};
*/

static LPTSTR lpszValueSupportedImageFormats[] =
{
	TEXT("None"),
	TEXT("JPEG"),
	NULL
};

DWORD OpenEnableScanner(void);
DWORD CloseDisableScanner(void);

void EnterScannerVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitScannerVersion(LPITEMSELECTOR lpis,DWORD dwSelection);

LPTSTR GetMaxImageRect(LPITEMSELECTOR lpis,DWORD dwIndex);
DWORD GetDeviceInfo(LPITEMSELECTOR lpis);

LPTSTR GetHardwareVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetDecoderVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetPDDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetMDDVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis,DWORD dwIndex);

void EnterReaderParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void EnterInterfaceParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void EnterScanParameters(LPITEMSELECTOR lpis,DWORD dwSelection);

void ExitReaderParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitInterfaceParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitScanParameters(LPITEMSELECTOR lpis,DWORD dwSelection);

void EnterDeviceInfo(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitDeviceInfo(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetScannerNameCount(LPITEMSELECTOR lpis);
void SelectScannerName(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetReaderParameter(LPITEMSELECTOR lpis);
void SetReaderParameter(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetInterfaceParameter(LPITEMSELECTOR lpis);
void SetInterfaceParameter(LPITEMSELECTOR lpis,DWORD dwSelection);

DWORD GetScanParameter(LPITEMSELECTOR lpis);
void SetScanParameter(LPITEMSELECTOR lpis,DWORD dwSelection);

ITEMSELECTOR ScannerParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_SELECTEDSCANNER),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesScannerName,NULL,SelectScannerName,GetScannerNameCount)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_READERPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterReaderParameters,ExitReaderParameters)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_INTERFACEPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterInterfaceParameters,ExitInterfaceParameters)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SCANPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterScanParameters,ExitScanParameters)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_DEVICEINFO),
		NO_HELP,		// no help file for now
		STRUCTURE_LIST(EnterDeviceInfo,ExitDeviceInfo)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SCANNERVERSION),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterScannerVersion,ExitScannerVersion)
	},

	END_ITEMSELECTOR
};


ITEMSELECTOR LaserReaderParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_READERTYPE),
		NO_HELP,
		STRING_LIST(lpszValueLaserReaderType,NULL,NULL,NULL),
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMTYPE),
		NO_HELP,
		STRING_LIST(lpszValuesAimType,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMDURATION),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMMODE),
		NO_HELP,
		STRING_LIST(lpszValuesAimMode,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_BEAMWIDTH),
		NO_HELP,
		STRING_LIST(lpszValuesBeamWidth,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_RASTERMODE),
		NO_HELP,
		STRING_LIST(lpszValuesRasterMode,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_BEAMTIMER),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CONTROLSCANLED),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SCANLEDLOGICLEVEL),
		NO_HELP,
		STRING_LIST(lpszValuesLowHigh,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CLASSONE),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_REDUNDANCY),
		NO_HELP,
		STRING_LIST(lpszValuesRedundancy,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_LINEARSECURITY),
		NO_HELP,
		STRING_LIST(lpszValuesLinearSecurity,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POINTERTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	
	END_ITEMSELECTOR
};


ITEMSELECTOR ContactReaderParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_READERTYPE),
		NO_HELP,
		STRING_LIST(lpszValueContactReaderType,NULL,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_QUIETZONERATIO),
		NO_HELP,
		STRING_LIST(lpszValuesQuietZoneRatio,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_INITIALSCANTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by1000,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_PULSEDELAY),
		NO_HELP,
		STRING_RANGE(lpszRange0to1000by100,GetReaderParameter,SetReaderParameter)
	},
	
	END_ITEMSELECTOR
};

ITEMSELECTOR ImagerReaderParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_READERTYPE),
		NO_HELP,
		STRING_LIST(lpszValueImagerReaderType,NULL,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMTYPE),
		NO_HELP,
		STRING_LIST(lpszValuesAimType,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMDURATION),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMMODE),
		NO_HELP,
		STRING_LIST(lpszValuesAimMode,GetReaderParameter,SetReaderParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_BEAMTIMER),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POINTERTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_IMAGECAPTURETIMEOUT),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_IMAGECOMPRESSIONTIMEOUT),
		NO_HELP,
		STRING_RANGE(lpszRange0to60000by100,GetReaderParameter,SetReaderParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_LINEARSECURITY),
		NO_HELP,
		STRING_LIST(lpszValuesLinearSecurity,GetReaderParameter,SetReaderParameter,NULL)
	},
	
	END_ITEMSELECTOR
};

ITEMSELECTOR QSNACInterfaceParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_INTERFACETYPE),
		NO_HELP,
		STRING_LIST(lpszValueQSNACInterfaceType,NULL,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_ENABLESETTLINGTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to500by10,GetInterfaceParameter,SetInterfaceParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_INVERSELABELDATA),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetInterfaceParameter,SetInterfaceParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_WHITELOGICLEVEL),
		NO_HELP,
		STRING_LIST(lpszValuesLowHigh,GetInterfaceParameter,SetInterfaceParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_TRANSITIONRESOLUTION),
		NO_HELP,
		STRING_RANGE(lpszRange1to32by1,GetInterfaceParameter,SetInterfaceParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POWERSETTLINGTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to500by10,GetInterfaceParameter,SetInterfaceParameter)
	},
	
	END_ITEMSELECTOR
};


ITEMSELECTOR SSIInterfaceParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_INTERFACETYPE),
		NO_HELP,
		STRING_LIST(lpszValueSSIInterfaceType,NULL,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POWERSETTLINGTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to1000by50,GetInterfaceParameter,SetInterfaceParameter)
	},
	
	END_ITEMSELECTOR
};


ITEMSELECTOR LS48XXInterfaceParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_INTERFACETYPE),
		NO_HELP,
		STRING_LIST(lpszValueLS48XXInterfaceType,NULL,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_POWERSETTLINGTIME),
		NO_HELP,
		STRING_RANGE(lpszRange0to1000by50,GetInterfaceParameter,SetInterfaceParameter)
	},
	
	END_ITEMSELECTOR
};


ITEMSELECTOR ScanParametersSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_CODEIDTYPE),
		NO_HELP,
		STRING_LIST(lpszValuesCodeIdType,GetScanParameter,SetScanParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SCANTYPE),
		NO_HELP,
		STRING_LIST(lpszValuesScanType,GetScanParameter,SetScanParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_LOCALFEEDBACK),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetScanParameter,SetScanParameter,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_WAVEFILENAME),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		NULL,
		RESOURCE_STRING(IDS_DECODEBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_DECODEBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		NULL,
		RESOURCE_STRING(IDS_DECODELEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_STARTWAVEFILE),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_STARTBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_STARTBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_STARTLEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_INTERMEDIATEWAVEFILE),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_INTERMEDIATEBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_INTERMEDIATEBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_INTERMEDIATELEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_FATALWAVEFILE),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_FATALBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_FATALBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_FATALLEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_NONFATALWAVEFILE),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_NONFATALBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_NONFATALBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_NONFATALLEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_ACTIVITYWAVEFILE),
		NO_HELP,
		STRING_EDIT(TEXT(""),GetScanParameter,SetScanParameter)
	},
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_ACTIVITYBEEPTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperDuration,GetScanParameter,SetScanParameter)
	},
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_ACTIVITYBEEPFREQUENCY),
		NO_HELP,
		STRING_RANGE(lpszRangeBeeperFrequency,GetScanParameter,SetScanParameter)
	},
#endif
	{
		&l_bScannerV2Plus,
		RESOURCE_STRING(IDS_ACTIVITYLEDTIME),
		NO_HELP,
		STRING_RANGE(lpszRangeLedDuration,GetScanParameter,SetScanParameter)
	},
	
	END_ITEMSELECTOR
};

ITEMSELECTOR DeviceInfoSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_BEAMWIDTH),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetDeviceInfo,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_AIMMODE),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetDeviceInfo,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SCANDIRECTION),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetDeviceInfo,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_FEEDBACK),
		NO_HELP,
		STRING_LIST(lpszValuesFalseTrue,GetDeviceInfo,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_SUPPORTEDIMAGEFORMATS),
		NO_HELP,
		STRING_LIST(lpszValueSupportedImageFormats,GetDeviceInfo,NULL,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_MAXIMAGERECT),
		NO_HELP,
		GET_STRING(GetMaxImageRect,NULL)
	},
	
	END_ITEMSELECTOR
};

ITEMSELECTOR ScannerVersionSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_HARDWAREVERSION),
		NO_HELP,
        GET_STRING(GetHardwareVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_DECODERVERSION),
		NO_HELP,
		GET_STRING(GetDecoderVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_PDDVERSION),
		NO_HELP,
		GET_STRING(GetPDDVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_MDDVERSION),
		NO_HELP,
		GET_STRING(GetMDDVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetCAPIVersion,NULL)
	},
	
	END_ITEMSELECTOR

};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANCTL_H_   */
