
//--------------------------------------------------------------------
// FILENAME:            Comm.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

// Symbol SDK common includes

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// Ctlpanel include
#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _COMMCTL

#include "Comm.h"


//----------------------------------------------------------------------------
// Communication Functions
//----------------------------------------------------------------------------
// Comm Settings

void SetupCommSettings(void)
{
    static TCHAR szCommSettings[MAX_COMDEV][MAX_PATH];
    TCHAR szComm[MAX_PATH];

    DWORD dwIndex;
    DWORD dwSize, dwRetCode;
    FILETIME ft;
    HKEY hKey;
			
	dwRetCode = RegOpenKeyEx(HKEY_CURRENT_USER,TEXT("Comm\\RasBook"),0,KEY_READ, &hKey);
       
    if (dwRetCode == ERROR_SUCCESS)
    {
        dwIndex = 0;
        // 
            while (dwRetCode != ERROR_NO_MORE_ITEMS)
            {
                dwSize = countof(szComm);
                // get all the subkey
                dwRetCode = RegEnumKeyEx(hKey,dwIndex,szComm,&dwSize,NULL,NULL,0,&ft);
                // 
                if( (dwRetCode == ERROR_NO_MORE_ITEMS) || (dwRetCode != ERROR_SUCCESS))
                    break;

                TEXTCPY(szCommSettings[dwIndex],szComm);
                
                lpszCommSettings[dwIndex] = (LPTSTR)szCommSettings[dwIndex];
                dwIndex++;
            }
            // NULL 
            lpszCommSettings[dwIndex] = NULL;
            
	}

	RegCloseKey(hKey);
    
}

void SetCommRasBook(LPTSTR lpsz)
{
    DWORD dwRetCode;
    HKEY hKey;

    // make sure no NULL pointer and at least 2 chars long 
    if ((lpsz != NULL) && (countof(lpsz) > 1))
    {
        dwRetCode = RegOpenKeyEx(HKEY_CURRENT_USER,TEXT("ControlPanel\\Comm"),0,KEY_ALL_ACCESS, &hKey);
        if (dwRetCode == ERROR_SUCCESS)
        {
            // open the registry for setting
            dwRetCode = RegSetValueEx(hKey,TEXT("Cnct"),0,REG_SZ,(LPBYTE)lpsz,((TEXTLEN(lpsz)+1)*sizeof(TCHAR)));
			
			if (dwRetCode != ERROR_SUCCESS)
				MyError(TEXT("Errer Set Key"),lpsz);

            RegCloseKey(hKey);
			
			if (l_dwPersistence == PERSISTENCE_PERMANENT)
			{
				DWORD dwRet;
				dwRet = WriteRegFile(TEXT("ControlPanel\\Comm"),
	   								 TEXT("Cnct"),
	    							 REG_SZ,
		    						 lpsz,
			    					 countof(lpsz),
									 1);

				if (dwRet != ERROR_SUCCESS)
				{
					ReportError(TEXT("ERROR WriteReg"),dwRet);
				}
			}

        }
		else
		{
			ReportError(TEXT("Error Open Set Key"),dwRetCode);
		}
    }

}

void GetDefaultCommSettings(void)
{

    DWORD dwRetCode = 0;
    HKEY hKey;
	TCHAR szDefaultComm[MAX_PATH];

	// if we don't already have the default setting, let's get it
    if (l_szCommSetting[0] == '\0')
	{
	    dwRetCode = RegOpenKeyEx(HKEY_CURRENT_USER,TEXT("ControlPanel\\Comm"),0,KEY_ALL_ACCESS, &hKey);
	    if (dwRetCode == ERROR_SUCCESS)
	    {
	        // query for "Cnct"--Comm setting
	        DWORD dwLen;
	        DWORD dwType;

	        dwLen = countof(szDefaultComm);
	        dwRetCode = RegQueryValueEx(hKey,TEXT("Cnct"),NULL,&dwType,(LPBYTE)szDefaultComm,&dwLen);
	        RegCloseKey(hKey);
	        
	        // no default, use the first one on the list
	        if ((dwRetCode != ERROR_SUCCESS) || (dwLen == 2))
	        {
	            ReportError(TEXT("Error Query Key"),dwRetCode);
				TEXTCPY(l_szCommSetting,lpszCommSettings[0]);
	        }
	        else    
	        {
				TEXTCPY(l_szCommSetting,szDefaultComm);
	        }
		}
    }
    
}


void SelectCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    LPTSTR lpszText = GetTextItem(lpis,dwSelection);    
    
    if ( lpszText != NULL ) 
	{
        TEXTCPY(l_szCommSetting,lpszText);
    }

}

DWORD GetCommSettings(LPITEMSELECTOR lpis)
{
	DWORD dwIndex;


    for (dwIndex=0; dwIndex < MAX_COMDEV; dwIndex++)
    {
	    if (lpszCommSettings[dwIndex] == NULL)
        {
            dwIndex = 0;		// if we can't find the match, 
								// use the default one 
			break;
        }

		if (TEXTCMP(l_szCommSetting,lpszCommSettings[dwIndex]) == 0)
		{
			break;
		}
    }

	return (dwIndex);

}

void EnterCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection)
{
   	LoadString(g_hInstance,IDS_COMMSETTINGS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    SetupCommSettings();

	GetDefaultCommSettings();

    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)CommSettingsSelector);

}

void ExitCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection)
{

	if (l_bDialogWasOKed)
    {
        SetCommRasBook(l_szCommSetting);
    }

	l_bDialogWasOKed = FALSE;


	l_szCommSetting[0]='\0';

    // set next exit function
    l_lpExitFunction = NULL;    


}

#endif
