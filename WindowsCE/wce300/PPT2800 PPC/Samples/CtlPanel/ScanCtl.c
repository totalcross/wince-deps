
//--------------------------------------------------------------------
// FILENAME:            ScanCtl.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to get/set scanning parameters
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"
#include "..\common\Scan.h"

#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _SCANCTL

#include <scancapi.h>
#include "ScanCtl.h"


static TCHAR l_szScannerName[MAX_PATH32];
static HANDLE l_hScanner;

static READER_PARAMS        l_ReaderParams;
static INTERFACE_PARAMS     l_InterfaceParams;
static SCAN_PARAMS          l_ScanParams;
static BOOL					l_bScannerV2Plus = TRUE;
static SCAN_VERSION_INFO    l_ScanVersionInfo;
static DEVICE_INFO			l_DeviceInfo;


static FEEDBACK_INFO FeedbackInfo;

#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)

static int nBeeperFrequencyMinimum;
static int nBeeperFrequencyMaximum;
static int nBeeperFrequencyStep;
static int nBeeperDurationMinimum;
static int nBeeperDurationMaximum;
static int nBeeperDurationStep;

#endif

static int nLedDurationMinimum;
static int nLedDurationMaximum;
static int nLedDurationStep;


void FillDeviceNames(void);


void EnterScannerParameters(LPITEMSELECTOR lpis,
						    DWORD dwSelection)
{
	LoadString(g_hInstance,IDS_SCANNERPARAMETERS,l_szLastItem,TEXTSIZEOF(l_szLastItem));
	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	FillDeviceNames();

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ScannerParametersSelector);
}


void ExitScannerParameters(LPITEMSELECTOR lpis,
						   DWORD dwSelection)
{
	l_bDialogWasOKed = FALSE;

}

void EnterScannerVersion(LPITEMSELECTOR lpis,
								   DWORD dwSelection)
{

	if(OpenEnableScanner() != E_SCN_SUCCESS)
	{
		return;
	}

	SI_ALLOC_ALL(&l_ScanVersionInfo);
	l_ScanVersionInfo.StructInfo.dwUsed = 0;

	if ( SCAN_GetVersionInfo(l_hScanner,&l_ScanVersionInfo) != E_SCN_SUCCESS )
    {
		TCHAR szMsg[MAX_PATH64];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get Version Info %s"),l_szScannerName);
		MyError(TEXT("EnterScannerVersion"),szMsg);
		CloseDisableScanner();
		return;
	}

	LoadString(g_hInstance,IDS_SCANNERVERSION,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ScannerVersionSelector);
}


void ExitScannerVersion(LPITEMSELECTOR lpis,
								  DWORD dwSelection)
{
	if ( l_hScanner != NULL )
	{
		if(CloseDisableScanner() != E_SCN_SUCCESS)
			return;

		l_lpExitFunction = NULL;
	}

	l_bDialogWasOKed = FALSE;
}



LPTSTR GetHardwareVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    static TCHAR szMsg[MAX_PATH8];
    LPTSTR lpsz = NULL;

    
    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_ScanVersionInfo.dwHardwareVersion),LOWORD(l_ScanVersionInfo.dwHardwareVersion));
    lpsz = (LPTSTR)szMsg;

    return lpsz;
}
    
LPTSTR GetDecoderVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static    TCHAR szMsg[MAX_PATH8];
    lpsz = szMsg;

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_ScanVersionInfo.dwDecoderVersion),LOWORD(l_ScanVersionInfo.dwDecoderVersion));
    lpsz = (LPTSTR)szMsg;

    return lpsz;
}

LPTSTR GetPDDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_ScanVersionInfo.dwPddVersion),LOWORD(l_ScanVersionInfo.dwPddVersion));
    lpsz = (LPTSTR)szMsg;

    return lpsz;
}

 LPTSTR GetMDDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_ScanVersionInfo.dwMddVersion),LOWORD(l_ScanVersionInfo.dwMddVersion));
    lpsz = szMsg;    

    return lpsz;
}

LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_ScanVersionInfo.dwCAPIVersion),LOWORD(l_ScanVersionInfo.dwCAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}

DWORD GetScannerNameCount(LPITEMSELECTOR lpis)
{
	DWORD dwCount = 0;
	SCAN_FINDINFO ScanFindInfo;
	HANDLE hScanFind;
	DWORD dwResult;

	SI_ALLOC_ALL(&ScanFindInfo);
	ScanFindInfo.StructInfo.dwUsed = 0;
	
	dwResult = SCAN_FindFirst(&ScanFindInfo,&hScanFind);
	while ( dwResult == E_SCN_SUCCESS )
	{
		dwResult = SCAN_FindNext(&ScanFindInfo,hScanFind);
		dwCount++;
	}

	SelectScannerName(lpis,0);
    SCAN_FindClose(hScanFind);
	return(dwCount);
}


void SelectScannerName(LPITEMSELECTOR lpis,
							  DWORD dwSelection)
{
	LPTSTR lpszText = GetTextItem(lpis,dwSelection);
	if ( lpszText != NULL )
	{
		TEXTCPY(l_szScannerName,lpszText);
	}
}


DWORD GetReaderType(void)
{
	return(l_ReaderParams.dwReaderType);
}


LPARAM GetReaderParamsSelector(void)
{
	LPARAM lParam = (LPARAM)NULL;

	switch(GetReaderType())
	{
		case READER_TYPE_LASER:
			lParam = (LPARAM)LaserReaderParametersSelector;
			break;
		case READER_TYPE_CONTACT:
			lParam = (LPARAM)ContactReaderParametersSelector;
			break;
		case READER_TYPE_IMAGER:
			lParam = (LPARAM)ImagerReaderParametersSelector;
			break;
	}

	return(lParam);
}


void EnterReaderParameters(LPITEMSELECTOR lpis,
								  DWORD dwSelection)
{
	LPARAM lParam;


	if(OpenEnableScanner() != E_SCN_SUCCESS)
	{
		return;
	}
	
	SI_INIT(&l_ReaderParams);

	if ( SCAN_GetReaderParams(l_hScanner,&l_ReaderParams) != E_SCN_SUCCESS )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get Params %s"),l_szScannerName);
		MyError(TEXT("EnterReaderParameters"),szMsg);
		CloseDisableScanner();
		return;
	}
	
	LoadString(g_hInstance,IDS_READERPARAMETERS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	lParam = GetReaderParamsSelector();

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								lParam);
}


DWORD GetScannerBaseKey(LPTSTR lpszKey,
							   DWORD dwSize,
							   LPTSTR lpszScannerName)
{

	SCAN_FINDINFO ScanFindInfo;
	HANDLE hScanFind;
	DWORD dwResult;

	TEXTNCPY(lpszKey,TEXT("Drivers\\BuiltIn\\BarCodeReader"),dwSize);
	lpszKey[dwSize-1] = TEXT('\0');

	SI_ALLOC_ALL(&ScanFindInfo);
	ScanFindInfo.StructInfo.dwUsed = 0;

	dwResult = SCAN_FindFirst(&ScanFindInfo,&hScanFind);
	while ( dwResult == E_SCN_SUCCESS )
	{
		if ( TEXTCMP(ScanFindInfo.szDeviceName,lpszScannerName) == 0 )
		{
			TEXTNCPY(lpszKey,ScanFindInfo.szRegistryBasePath,dwSize);
			lpszKey[dwSize-1] = TEXT('\0');

			SCAN_FindClose(hScanFind);

			return(ERROR_SUCCESS);
		}

		dwResult = SCAN_FindNext(&ScanFindInfo,hScanFind);
	}

    SCAN_FindClose(hScanFind);

	return(ERROR_SUCCESS);
}

DWORD SetScannerKey(LPTSTR lpszScannerName,
						   LPTSTR lpszKeyExtension,
						   LPTSTR lpszValueName,
						   LPVOID lpvData,
						   DWORD dwSize,
						   DWORD dwPersistence)
{
	TCHAR szKey[MAX_PATH];
	HKEY hKey;
	DWORD dwRetCode;
	DWORD dwDisp;

	dwRetCode = GetScannerBaseKey(szKey,
								  TEXTSIZEOF(szKey),
								  lpszScannerName);
	
	if ( dwRetCode != ERROR_SUCCESS )
	{
		//ReportError(TEXT("GetScannerBaseKey"),dwRetCode);
		return(dwRetCode);
	}

	TEXTCAT(szKey,lpszKeyExtension);
	
	dwRetCode = RegOpenKeyEx(REG_SECTION,
							 szKey,
							 0,
							 KEY_SET_VALUE,
							 &hKey);


	if ( dwRetCode != ERROR_SUCCESS )
	{
		//ReportError(TEXT("RegOpenKeyEx"),dwRetCode);
		dwRetCode = RegCreateKeyEx(REG_SECTION,
								   szKey,
								   0,
								   TEXT("REG_BINARY"),
								   REG_OPTION_NON_VOLATILE,
								   KEY_WRITE,
								   NULL,
								   &hKey,
								   &dwDisp);
	}
	
	if ( dwRetCode != ERROR_SUCCESS )
	{
		//ReportError(TEXT("RegOpenKeyEx/RegCreateKeyEx"),dwRetCode);
		return(dwRetCode);
	}

	dwRetCode = RegSetValueEx(hKey,
							  lpszValueName,
							  0,
							  REG_BINARY,
							  lpvData,
							  dwSize);

	if ( dwRetCode != ERROR_SUCCESS )
	{
		//ReportError(TEXT("RegSetValueEx"),dwRetCode);

		RegCloseKey(hKey);

		return(dwRetCode);
	}

	dwRetCode = RegCloseKey(hKey);

	if ( dwRetCode != ERROR_SUCCESS )
	{
		//ReportError(TEXT("RegCloseKey"),dwRetCode);
		return(dwRetCode);
	}

	if ( dwPersistence != PERSISTENCE_PERMANENT )
	{
		return(ERROR_SUCCESS);
	}

	dwRetCode = WriteRegFile(szKey,
							 lpszValueName,
							 REG_BINARY,
							 lpvData,
							 dwSize,
							 0);

	if ( dwRetCode != ERROR_SUCCESS )
	{
		ReportError(TEXT("WriteRegFile"),dwRetCode);
		return(dwRetCode);
	}	
	
	return(dwRetCode);
}


void ExitReaderParameters(LPITEMSELECTOR lpis,
								 DWORD dwSelection)
{
	if ( l_hScanner != NULL )
	{
		if ( l_bDialogWasOKed )
		{
			DWORD dwRetCode;

			dwRetCode = SCAN_SetReaderParams(l_hScanner,&l_ReaderParams);

			if ( dwRetCode != E_SCN_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];

				//ReportError(TEXT("SCAN_SetReaderParams"),dwRetCode);

				TEXTSPRINTF(szMsg,TEXT("Cannot Set Params %s"),l_szScannerName);
				MyError(TEXT("ExitReaderParameters"),szMsg);
				return;
			}

			dwRetCode = SetScannerKey(l_szScannerName,
									  TEXT("\\Settings"),
									  TEXT("ReaderParams"),
									  &l_ReaderParams,
									  sizeof(l_ReaderParams),
									  l_dwPersistence);

			if ( dwRetCode != ERROR_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];

				//ReportError(TEXT("SetScannerKey"),dwRetCode);

				TEXTSPRINTF(szMsg,TEXT("Cannot Set Registry %s"),l_szScannerName);
				MyError(TEXT("ExitReaderParameters"),szMsg);

				return;
			}
		}

		if(CloseDisableScanner() != E_SCN_SUCCESS)
			return;
		
		l_lpExitFunction = NULL;
	}

	l_bDialogWasOKed = FALSE;

}


DWORD GetReaderParameter(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		LPITEMSELECTOR lprs;
		DWORD dwSelector;
		DWORD dwValue;

		lprs = (LPITEMSELECTOR)GetReaderParamsSelector();
		dwSelector = (lpis - lprs) - 1;
		dwValue = l_ReaderParams.ReaderSpecific.dwUntyped[dwSelector];

		dwIndex = GetIndexFromValue(lpis,dwValue);
	}

	return(dwIndex);
}


void SetReaderParameter(LPITEMSELECTOR lpis,
							   DWORD dwSelection)
{
	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		LPITEMSELECTOR lprs;
		DWORD dwValue;
		DWORD dwIndex;

		lprs = (LPITEMSELECTOR)GetReaderParamsSelector();
		dwIndex = (lpis - lprs) - 1;
		dwValue = GetValueFromIndex(lpis,dwSelection);
		l_ReaderParams.ReaderSpecific.dwUntyped[dwIndex] = dwValue;
	}
}


DWORD GetInterfaceType(void)
{
	return(l_InterfaceParams.dwInterfaceType);
}


LPARAM GetInterfaceParamsSelector(void)
{
	LPARAM lParam = (LPARAM)NULL;

	switch(GetInterfaceType())
	{
		case INTERFACE_TYPE_QSNAC:
			lParam = (LPARAM)QSNACInterfaceParametersSelector;
			break;
		case INTERFACE_TYPE_SSI:
			lParam = (LPARAM)SSIInterfaceParametersSelector;
			break;
		case INTERFACE_TYPE_LS48XX:
			lParam = (LPARAM)LS48XXInterfaceParametersSelector;
			break;
	}

	return(lParam);
}


void EnterInterfaceParameters(LPITEMSELECTOR lpis,
									 DWORD dwSelection)
{
	LPARAM lParam;

	if(OpenEnableScanner() != E_SCN_SUCCESS)
	{
		return;
	}
	
	SI_INIT(&l_InterfaceParams);
	
	if ( SCAN_GetInterfaceParams(l_hScanner,&l_InterfaceParams) != E_SCN_SUCCESS )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get Params %s"),l_szScannerName);
		MyError(TEXT("EnterInterfaceParameters"),szMsg);
		CloseDisableScanner();
		return;
	}
	LoadString(g_hInstance,IDS_INTERFACEPARAMETERS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	lParam = GetInterfaceParamsSelector();

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								lParam);
}


void ExitInterfaceParameters(LPITEMSELECTOR lpis,
									DWORD dwSelection)
{
	if ( l_hScanner != NULL )
	{
		if ( l_bDialogWasOKed )
		{
			if ( SCAN_SetInterfaceParams(l_hScanner,&l_InterfaceParams) != E_SCN_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];
				TEXTSPRINTF(szMsg,TEXT("Cannot Set Params %s"),l_szScannerName);
				MyError(TEXT("ExitInterfaceParameters"),szMsg);
				CloseDisableScanner();
				return;
			}

			if ( SetScannerKey(l_szScannerName,
							   TEXT("\\Settings"),
							   TEXT("InterfaceParams"),
							   &l_InterfaceParams,
							   sizeof(l_InterfaceParams),
							   l_dwPersistence) != ERROR_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];
				TEXTSPRINTF(szMsg,TEXT("Cannot Set Registry %s"),l_szScannerName);
				MyError(TEXT("ExitInterfaceParameters"),szMsg);
				return;
			}
		}

		if(CloseDisableScanner() != E_SCN_SUCCESS)
			return;

		l_lpExitFunction = NULL;
	}
	l_bDialogWasOKed = FALSE;

}


DWORD GetInterfaceParameter(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		LPITEMSELECTOR lprs;
		DWORD dwSelector;
		DWORD dwValue;

		lprs = (LPITEMSELECTOR)GetInterfaceParamsSelector();
		dwSelector = (lpis - lprs) - 1;
		dwValue = l_InterfaceParams.InterfaceSpecific.dwUntyped[dwSelector];

		dwIndex = GetIndexFromValue(lpis,dwValue);
	}

	return(dwIndex);
}


void SetInterfaceParameter(LPITEMSELECTOR lpis,
								  DWORD dwSelection)
{
	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		LPITEMSELECTOR lprs;
		DWORD dwValue;
		DWORD dwIndex;

		lprs = (LPITEMSELECTOR)GetInterfaceParamsSelector();
		dwIndex = (lpis - lprs) - 1;
		dwValue = GetValueFromIndex(lpis,dwSelection);
		l_InterfaceParams.InterfaceSpecific.dwUntyped[dwIndex] = dwValue;
	}
}


BOOL SetupFeedbackInformation(void)
{
#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)

	nBeeperFrequencyMinimum		= 2500;
	nBeeperFrequencyMaximum		= 3500;
	nBeeperFrequencyStep		= 10;

	nBeeperDurationMinimum		= 0;
	nBeeperDurationMaximum		= 5000;
	nBeeperDurationStep			= 100;

#endif

	nLedDurationMinimum			= 0;
	nLedDurationMaximum			= 5000;
	nLedDurationStep			= 500;

	SI_INIT(&FeedbackInfo);

	if ( !GetFeedbackInfo(&FeedbackInfo) )
	{
		return(FALSE);
	}

#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)

	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMinimum,nBeeperFrequencyMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMaximum,nBeeperFrequencyMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyStep,nBeeperFrequencyStep);

	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMinimum,nBeeperDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMaximum,nBeeperDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationStep,nBeeperDurationStep);

#endif

	SI_GET_FIELD(&FeedbackInfo,nLedDurationMinimum,nLedDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationMaximum,nLedDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationStep,nLedDurationStep);

#if defined (WIN32_PLATFORM_7500) || defined(WIN32_PLATFORM_8100)

	TEXTSPRINTF(szBeeperFrequencyMinimum,TEXT("%d"),nBeeperFrequencyMinimum);
	TEXTSPRINTF(szBeeperFrequencyMaximum,TEXT("%d"),nBeeperFrequencyMaximum);
	TEXTSPRINTF(szBeeperFrequencyStep,TEXT("%d"),nBeeperFrequencyStep);
	
	TEXTSPRINTF(szBeeperDurationMinimum,TEXT("%d"),nBeeperDurationMinimum);
	TEXTSPRINTF(szBeeperDurationMaximum,TEXT("%d"),nBeeperDurationMaximum);
	TEXTSPRINTF(szBeeperDurationStep,TEXT("%d"),nBeeperDurationStep);

#endif

	TEXTSPRINTF(szLedDurationMinimum,TEXT("%d"),nLedDurationMinimum);
	TEXTSPRINTF(szLedDurationMaximum,TEXT("%d"),nLedDurationMaximum);
	TEXTSPRINTF(szLedDurationStep,TEXT("%d"),nLedDurationStep);
	
	return(TRUE);
}


void EnterScanParameters(LPITEMSELECTOR lpis,
								DWORD dwSelection)
{
	SetupFeedbackInformation();

	if(OpenEnableScanner() != E_SCN_SUCCESS)
	{
		return;
	}
	
	SI_INIT(&l_ScanVersionInfo);

	if ( SCAN_GetVersionInfo(l_hScanner,&l_ScanVersionInfo) == E_SCN_SUCCESS )
    {
		if ( HIWORD(l_ScanVersionInfo.dwMddVersion) < 2 )
		{
			l_bScannerV2Plus = FALSE;
		}
	}
	
	SI_INIT(&l_ScanParams);

	if ( !l_bScannerV2Plus )
	{
		l_ScanParams.StructInfo.dwAllocated = SI_FIELD_EXTENT(&l_ScanParams,szWaveFile);
	}

	if ( SCAN_GetScanParameters(l_hScanner,&l_ScanParams) != E_SCN_SUCCESS )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get Params %s"),l_szScannerName);
		MyError(TEXT("EnterScanParameters"),szMsg);
		CloseDisableScanner();
		return;
	}
	
	LoadString(g_hInstance,IDS_SCANPARAMETERS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    l_bUseWavFile = TRUE;
//	l_bUseS24 = FALSE;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ScanParametersSelector);
}


void ExitScanParameters(LPITEMSELECTOR lpis,
							   DWORD dwSelection)
{
	if ( l_hScanner != NULL )
	{
		if ( l_bDialogWasOKed )
		{
			if ( SCAN_SetScanParameters(l_hScanner,&l_ScanParams) != E_SCN_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];
				TEXTSPRINTF(szMsg,TEXT("Cannot Set Params %s"),l_szScannerName);
				MyError(TEXT("ExitScanParameters"),szMsg);
				CloseDisableScanner();
				return;
			}

			if ( SetScannerKey(l_szScannerName,
							   TEXT("\\Settings"),
							   TEXT("ScanParams"),
							   &l_ScanParams,
							   l_ScanParams.StructInfo.dwAllocated,
							   l_dwPersistence) != ERROR_SUCCESS )
			{
				TCHAR szMsg[MAX_PATH32];
				TEXTSPRINTF(szMsg,TEXT("Cannot Set Registry %s"),l_szScannerName);
				MyError(TEXT("ExitScanParameters"),szMsg);
				return;
			}

        }
		
		if(CloseDisableScanner() != E_SCN_SUCCESS)
			return;

		l_lpExitFunction = NULL;
	}

	l_bDialogWasOKed = FALSE;
}


DWORD GetScanParameter(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		DWORD dwValue;

		switch(RESOURCE_ID(lpis))
		{
			case IDS_CODEIDTYPE:
				dwValue = l_ScanParams.dwCodeIdType;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_SCANTYPE:
				dwValue = l_ScanParams.dwScanType;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_LOCALFEEDBACK:
				dwValue = l_ScanParams.bLocalFeedback;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_DECODEBEEPTIME:
				dwValue = l_ScanParams.dwDecodeBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_DECODEBEEPFREQUENCY:
				dwValue = l_ScanParams.dwDecodeBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_DECODELEDTIME:
				dwValue = l_ScanParams.dwDecodeLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_WAVEFILENAME:
				TEXTCPY(lpis->szText,l_ScanParams.szWaveFile);
				dwIndex = 0;
				break;
			case IDS_STARTBEEPTIME:
				dwValue = l_ScanParams.dwStartBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_STARTBEEPFREQUENCY:
				dwValue = l_ScanParams.dwStartBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_STARTLEDTIME:
				dwValue = l_ScanParams.dwStartLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_STARTWAVEFILE:
				TEXTCPY(lpis->szText,l_ScanParams.szStartWaveFile);
				dwIndex = 0;
				break;
			case IDS_INTERMEDIATEBEEPTIME:
				dwValue = l_ScanParams.dwIntermedBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_INTERMEDIATEBEEPFREQUENCY:
				dwValue = l_ScanParams.dwIntermedBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_INTERMEDIATELEDTIME:
				dwValue = l_ScanParams.dwIntermedLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_INTERMEDIATEWAVEFILE:
				TEXTCPY(lpis->szText,l_ScanParams.szIntermedWaveFile);
				dwIndex = 0;
				break;
			case IDS_FATALBEEPTIME:
				dwValue = l_ScanParams.dwFatalBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_FATALBEEPFREQUENCY:
				dwValue = l_ScanParams.dwFatalBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_FATALLEDTIME:
				dwValue = l_ScanParams.dwFatalLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_FATALWAVEFILE:
				TEXTCPY(lpis->szText,l_ScanParams.szFatalWaveFile);
				dwIndex = 0;
				break;
			case IDS_NONFATALBEEPTIME:
				dwValue = l_ScanParams.dwNonfatalBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_NONFATALBEEPFREQUENCY:
				dwValue = l_ScanParams.dwNonfatalBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_NONFATALLEDTIME:
				dwValue = l_ScanParams.dwNonfatalLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_NONFATALWAVEFILE:
				TEXTCPY(lpis->szText,l_ScanParams.szNonfatalWaveFile);
				dwIndex = 0;
				break;
			case IDS_ACTIVITYBEEPTIME:
				dwValue = l_ScanParams.dwActivityBeepTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_ACTIVITYBEEPFREQUENCY:
				dwValue = l_ScanParams.dwActivityBeepFrequency;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_ACTIVITYLEDTIME:
				dwValue = l_ScanParams.dwActivityLedTime;
				dwIndex = GetIndexFromValue(lpis,dwValue);
				break;
			case IDS_ACTIVITYWAVEFILE:
				TEXTCPY(lpis->szText,l_ScanParams.szActivityWaveFile);
				dwIndex = 0;
				break;
		}
	}

	return(dwIndex);
}


void SetScanParameter(LPITEMSELECTOR lpis,
								  DWORD dwSelection)
{
	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		DWORD dwValue;

		switch(RESOURCE_ID(lpis))
		{
			case IDS_CODEIDTYPE:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwCodeIdType = dwValue;
				break;
			case IDS_SCANTYPE:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwScanType = dwValue;
				break;
			case IDS_LOCALFEEDBACK:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.bLocalFeedback = dwValue;
				break;
			case IDS_DECODEBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwDecodeBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwDecodeBeepFrequency,
						   l_ScanParams.dwDecodeBeepTime);
				break;
			case IDS_DECODEBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwDecodeBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwDecodeBeepFrequency,
						   l_ScanParams.dwDecodeBeepTime);
				break;
			case IDS_DECODELEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwDecodeLedTime = dwValue;
				break;
			case IDS_WAVEFILENAME:
				TEXTCPY(l_ScanParams.szWaveFile,lpis->szText);
				break;
			case IDS_STARTBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwStartBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwStartBeepFrequency,
						   l_ScanParams.dwStartBeepTime);
				break;
			case IDS_STARTBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwStartBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwStartBeepFrequency,
						   l_ScanParams.dwStartBeepTime);
				break;
			case IDS_STARTLEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwStartLedTime = dwValue;
				break;
			case IDS_STARTWAVEFILE:
				TEXTCPY(l_ScanParams.szStartWaveFile,lpis->szText);
				break;
			case IDS_INTERMEDIATEBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwIntermedBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwIntermedBeepFrequency,
						   l_ScanParams.dwIntermedBeepTime);
				break;
			case IDS_INTERMEDIATEBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwIntermedBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwIntermedBeepFrequency,
						   l_ScanParams.dwIntermedBeepTime);
				break;
			case IDS_INTERMEDIATELEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwIntermedLedTime = dwValue;
				break;
			case IDS_INTERMEDIATEWAVEFILE:
				TEXTCPY(l_ScanParams.szIntermedWaveFile,lpis->szText);
				break;
			case IDS_FATALBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwFatalBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwFatalBeepFrequency,
						   l_ScanParams.dwFatalBeepTime);
				break;
			case IDS_FATALBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwFatalBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwFatalBeepFrequency,
						   l_ScanParams.dwFatalBeepTime);
				break;
			case IDS_FATALLEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwFatalLedTime = dwValue;
				break;
			case IDS_FATALWAVEFILE:
				TEXTCPY(l_ScanParams.szFatalWaveFile,lpis->szText);
				break;
			case IDS_NONFATALBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwNonfatalBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwNonfatalBeepFrequency,
						   l_ScanParams.dwNonfatalBeepTime);
				break;
			case IDS_NONFATALBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwNonfatalBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwNonfatalBeepFrequency,
						   l_ScanParams.dwNonfatalBeepTime);
				break;
			case IDS_NONFATALLEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwNonfatalLedTime = dwValue;
				break;
			case IDS_NONFATALWAVEFILE:
				TEXTCPY(l_ScanParams.szNonfatalWaveFile,lpis->szText);
				break;
			case IDS_ACTIVITYBEEPTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwActivityBeepTime = dwValue;
				PlayBeeper(l_ScanParams.dwActivityBeepFrequency,
						   l_ScanParams.dwActivityBeepTime);
				break;
			case IDS_ACTIVITYBEEPFREQUENCY:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwActivityBeepFrequency = dwValue;
				PlayBeeper(l_ScanParams.dwActivityBeepFrequency,
						   l_ScanParams.dwActivityBeepTime);
				break;
			case IDS_ACTIVITYLEDTIME:
				dwValue = GetValueFromIndex(lpis,dwSelection);
				l_ScanParams.dwActivityLedTime = dwValue;
				break;
			case IDS_ACTIVITYWAVEFILE:
				TEXTCPY(l_ScanParams.szActivityWaveFile,lpis->szText);
				break;
		}
	}
}


DWORD GetDeviceInfo(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( ( lpis != NULL ) &&
		 ( l_hScanner != NULL ) )
	{
		LPITEMSELECTOR lprs;
		DWORD dwSelector;
		DWORD dwValue;

		dwValue = 0;
		lprs = (LPITEMSELECTOR)DeviceInfoSelector;
		dwSelector = (lpis - lprs);
		switch (dwSelector)
		{
			case 0:	//BeamWidth
				if (SI_FIELD_VALID(&l_DeviceInfo,bBeamWidth))
					dwValue = l_DeviceInfo.bBeamWidth;
				break;
			case 1:	//Aimmode
				if (SI_FIELD_VALID(&l_DeviceInfo,bAimMode))
					dwValue = l_DeviceInfo.bAimMode;
				break;
			case 2:	//ScanDirection
				if (SI_FIELD_VALID(&l_DeviceInfo,bDirection))
					dwValue = l_DeviceInfo.bDirection;
				break;
			case 3:	//FeedBack
				if (SI_FIELD_VALID(&l_DeviceInfo,bFeedback))
					dwValue = l_DeviceInfo.bFeedback;
				break;
			case 4: //Supportedformat
				if (SI_FIELD_VALID(&l_DeviceInfo,dwSupportedImageFormats))
					dwValue = l_DeviceInfo.dwSupportedImageFormats;
				break;
			default:
				break;
		}

		dwIndex = GetIndexFromValue(lpis,dwValue);
	}
		
	return(dwIndex);
}

LPTSTR GetMaxImageRect(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH32];

    if (SI_FIELD_VALID(&l_DeviceInfo,MaxImageRect))
	    TEXTSPRINTF(szMsg,TEXT("(%d,%d,%d,%d)"),
    				l_DeviceInfo.MaxImageRect.left,
    				l_DeviceInfo.MaxImageRect.top,
    				l_DeviceInfo.MaxImageRect.right,
    				l_DeviceInfo.MaxImageRect.bottom);
	else
	   TEXTSPRINTF(szMsg,TEXT("Not Supported!"));	
    				
    lpsz = szMsg;    

    return lpsz;

}

void EnterDeviceInfo(LPITEMSELECTOR lpis, DWORD dwSelection)
{

	DWORD dwError;

	if(OpenEnableScanner() != E_SCN_SUCCESS)
	{
		return;
	}
	
	SI_INIT(&l_DeviceInfo);

	dwError = SCAN_GetDeviceInfo(l_hScanner,&l_DeviceInfo);
	if (dwError != E_SCN_SUCCESS )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get DeviceInfo %s"),l_szScannerName);
		ReportError(szMsg,dwError);
		CloseDisableScanner();
		return;
	}
	
	LoadString(g_hInstance,IDS_DEVICEINFO,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	bReadOnly = TRUE;
	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)DeviceInfoSelector);

}

void ExitDeviceInfo(LPITEMSELECTOR lpis, DWORD dwSelection)
{
	bReadOnly = FALSE;

	if ( l_hScanner != NULL )
	{

		if(CloseDisableScanner() != E_SCN_SUCCESS)
			return;
		
		l_lpExitFunction = NULL;
	}

}

DWORD OpenEnableScanner(void)
{
	DWORD dwResult = E_SCN_SUCCESS;

	dwResult = SCAN_Open(l_szScannerName,&l_hScanner);
	if ( ( dwResult != E_SCN_SUCCESS ) ||  ( l_hScanner == NULL ) )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Open %s"),l_szScannerName);
		MyError(TEXT("OpenEnableScanner"),szMsg);
		return dwResult;
	}

    dwResult = SCAN_Enable(l_hScanner);

    if (dwResult != E_SCN_SUCCESS)
	{
		TCHAR szMsg[MAX_PATH64];
		TEXTSPRINTF(szMsg,TEXT("Cannot Enable %s (%x)"),l_szScannerName,dwResult);
		MyError(TEXT("OpenEnableScanner"),szMsg);
		SCAN_Close(l_hScanner);
		l_hScanner = NULL;
	}

	return dwResult;
}

DWORD CloseDisableScanner(void)
{
	DWORD dwResult = E_SCN_SUCCESS;

	SCAN_Disable(l_hScanner);

	dwResult = SCAN_Close(l_hScanner);

	if ( dwResult != E_SCN_SUCCESS )
	{
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("Cannot Close %s"),l_szScannerName);
		MyError(TEXT("CloseDisableScanner"),szMsg);
	}

	l_hScanner = NULL;

	return dwResult;

}

void FillDeviceNames(void)
{
	DWORD dwCount = 0;
	SCAN_FINDINFO ScanFindInfo;
	HANDLE hScanFind;
	DWORD dwResult;
	static TCHAR szScannerName[11][MAX_PATH32];
	
	SI_ALLOC_ALL(&ScanFindInfo);
	ScanFindInfo.StructInfo.dwUsed = 0;
	
	dwResult = SCAN_FindFirst(&ScanFindInfo,&hScanFind);
	while ( dwResult == E_SCN_SUCCESS )
	{
		TEXTCPY(szScannerName[dwCount],ScanFindInfo.szDeviceName);
		lpszValuesScannerName[dwCount] = (LPTSTR)szScannerName[dwCount];
		dwResult = SCAN_FindNext(&ScanFindInfo,hScanFind);
		dwCount++;
	}

	lpszValuesScannerName[dwCount] = NULL;
    SCAN_FindClose(hScanFind);
}


#endif
