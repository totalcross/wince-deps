
//--------------------------------------------------------------------
// FILENAME:            Power.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to get/set power management settings
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------


#include <windows.h>
#include <windowsx.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _POWERCTL

#include <pwrcapi.h>
#include "power.h"


//----------------------------------------------------------------------------
// Power Management Functions
//  This power management module includes functions to 
//  (1) Displaying Power API version.
//  (2) Displaying Power Info -- Battery Type, Battery Level, and Power Source
//  (3) Get/Set System device settings: including device's State; Activity Mask
//      for Hyper, Active, Doze, and Sleep states; Wakeup sources; and Timeout
//      for Active, Doze, Sleep, and Suspend state.
//  (4) Get/Set Backlight device settings: includeing device's State; Activity 
//      Mask for Active, Sleep, and Suspend; Timeout for Active State.
//----------------------------------------------------------------------------

static DWORD                l_dwTempMask;      
static LPDEVICEATTRIBUTE    l_lpDevice = NULL;
static POWER_VERSION_INFO   l_PowerVersionInfo;
static POWER_INFO           l_PowerInfo;       
static BOOL bTempUseWaveFile;
static BOOL bTempValidate;

// Set device's timeout.   
// NOTE: some state may not be supported by particular device
//       but the timeout value is always there.  For example
//       DOZE state is not supported for backlight; setting the
//       DOZE timeout would not cause any problem.
//
// return 0 if success; otherwise return 1;
DWORD SetDeviceTimeOut(void)
{
    DWORD dwDeviceState;
    DWORD dwTimeOut;
    DWORD dwResult = 0;

    // set timeout for all states.  Since we saved the original timeout when
    // entering the DeviceTimeout, we will set the unchanged timeout as well 
    for (dwDeviceState = DEVICE_STATE_ACTIVE; dwDeviceState<=DEVICE_STATE_SUSPEND;
         dwDeviceState = dwDeviceState * 2)
    {
        TCHAR szValueName[MAX_PATH];
			 
		switch (dwDeviceState)
        {
        case DEVICE_STATE_ACTIVE:
            dwTimeOut = l_lpDevice->TimeOut.ActiveTimeOut;
			TEXTCPY(szValueName,TEXT("ActiveTimeout"));
            break;
        case DEVICE_STATE_DOZE: 
            dwTimeOut = l_lpDevice->TimeOut.DozeTimeOut;
			TEXTCPY(szValueName,TEXT("DozeTimeout"));
            break;
        case DEVICE_STATE_SLEEP:
            dwTimeOut = l_lpDevice->TimeOut.SleepTimeOut;
			TEXTCPY(szValueName,TEXT("SleepTimeout"));
            break;
        case DEVICE_STATE_SUSPEND:
            dwTimeOut = l_lpDevice->TimeOut.SuspendTimeOut;
			TEXTCPY(szValueName,TEXT("SuspendTimeout"));
            break;
        }

        dwResult = POWER_SetTimerValue(l_lpDevice->szDeviceName,dwDeviceState,dwTimeOut);
        
		if ( dwResult == E_PWR_SUCCESS )
		{
			TCHAR szPath[MAX_PATH];

			TEXTSPRINTF(szPath,
						POWER_DRIVER_REG TEXT("%s"),
						l_lpDevice->szDeviceName);
			
			WriteToRegistry(szPath,
							szValueName,
							&dwTimeOut,
							sizeof(dwTimeOut),
							REG_DWORD);
		}
		else
		{
            TCHAR szMsg[MAX_PATH64];
		    TEXTSPRINTF(szMsg,TEXT("State (%x) Error: %x "),dwDeviceState,dwResult);
		    MyError(TEXT("SetDeviceTimer"),szMsg);
            return 1;
        }
	}
    
    return 0;

}


#define MAX_DWORD ((DWORD)0xFFFFFFFF)


static BOOL ParseTimeout(LPDWORD lpdwTimeout,
						 LPTSTR lpszString)
{
	TCHAR szTemp[MAX_PATH] = TEXT("");

	LoadString(g_hInstance,IDS_INFINITE,szTemp,TEXTSIZEOF(szTemp));

	if ( ( *lpszString == TEXT('\0') ) ||
		 ( TEXTICMP(lpszString,szTemp) == 0 ) )
	{
		TEXTCPY(lpszString,szTemp);
		
		*lpdwTimeout = MAX_DWORD;

		return(TRUE);
	}
	else
	{
		if ( IsValidDword(lpszString) )
		{
			*lpdwTimeout = TEXTTOL(lpszString);

			return(TRUE);
		}
	}

	ReportInvalidDword(lpszString);

	return(FALSE);
}


// This function will only update the device's timeout stored in the
// device structure.  It won't actually set the device's timeout unless
// the user accepted the changes.
void SetTimeOut(LPITEMSELECTOR lpis,
						   DWORD dwSelection)
{
    LPITEMSELECTOR lprs;
	DWORD dwSelector;

	lprs = TimeOutSelector;
	dwSelector = (lpis - lprs); // get which state's timeout we are dealing with

    // NOTE: the order of the state is depending on the order 
    // appears on TimeOutSelector[] declared in power.h
    switch (dwSelector)
    {
    case 0: // Active
    default:
        ParseTimeout(&l_lpDevice->TimeOut.ActiveTimeOut,lpis->szText);
        break;
    case 1: //Doze
        ParseTimeout(&l_lpDevice->TimeOut.DozeTimeOut,lpis->szText);
        break;
    case 2: // Sleep
        ParseTimeout(&l_lpDevice->TimeOut.SleepTimeOut,lpis->szText);
        break;
    case 3: // Suspend
        ParseTimeout(&l_lpDevice->TimeOut.SuspendTimeOut,lpis->szText);
        break;
    }

}


static FormatTimeout(DWORD dwTimeout,
					 LPTSTR lpszString,
					 DWORD dwSize)
{
	if ( dwTimeout == MAX_DWORD )
	{
		LoadString(g_hInstance,IDS_INFINITE,lpszString,dwSize);
	}
	else
	{
        ULTOTEXT(dwTimeout,lpszString,10);
	}
}


// This function will be called to get the initial timeout value for 
// particular state.
DWORD GetInitialTimeOut(LPITEMSELECTOR lpis)
{
    TCHAR szTimeOut[MAX_PATH32];
    LPITEMSELECTOR lprs;
	DWORD dwSelector;

	lprs = TimeOutSelector;
    // get which state's timeout we are dealing with
    dwSelector = (lpis - lprs);
    
    // NOTE: See NOTE on SetTimeOut()
    switch (dwSelector)
    {
		case 0: // Active
		default:
			FormatTimeout(l_lpDevice->TimeOut.ActiveTimeOut,szTimeOut,TEXTSIZEOF(szTimeOut));
			break;
		case 1: //Doze
			FormatTimeout(l_lpDevice->TimeOut.DozeTimeOut,szTimeOut,TEXTSIZEOF(szTimeOut));
			break;
		case 2: // Sleep
			FormatTimeout(l_lpDevice->TimeOut.SleepTimeOut,szTimeOut,TEXTSIZEOF(szTimeOut));
			break;
		case 3: // Suspend
			FormatTimeout(l_lpDevice->TimeOut.SuspendTimeOut,szTimeOut,TEXTSIZEOF(szTimeOut));
			break;
    }

    // update the display
    TEXTCPY(lpis->szText,szTimeOut);

    return 0;   
}

// This function get the actual device's time out from Power API
// For Win32, it will use the 30 secs for all the timeout.
void GetDeviceTimeOut (void)
{
    DWORD dwDeviceState;
    DWORD dwTimeOut = 30;
    DWORD dwResult = 0;
    
    for (dwDeviceState = DEVICE_STATE_ACTIVE; dwDeviceState <=DEVICE_STATE_SUSPEND;
        dwDeviceState = dwDeviceState * 2)
    {
        dwResult =POWER_GetTimerValue(l_lpDevice->szDeviceName,
                            dwDeviceState,&dwTimeOut);

        if (dwResult != E_PWR_SUCCESS)
        {
    		TCHAR szMsg[MAX_PATH32];
	    	TEXTSPRINTF(szMsg,TEXT("State (%x), Error (%x)")
                ,dwDeviceState, dwResult);
		    MyError(TEXT("PowerGetTimer"),szMsg);
        }

        switch (dwDeviceState)
        {
        case DEVICE_STATE_ACTIVE:
            l_lpDevice->TimeOut.ActiveTimeOut = dwTimeOut;
            break;
        case DEVICE_STATE_DOZE:
            l_lpDevice->TimeOut.DozeTimeOut = dwTimeOut;
            break;
        case DEVICE_STATE_SLEEP:
            l_lpDevice->TimeOut.SleepTimeOut = dwTimeOut;
            break;
        case DEVICE_STATE_SUSPEND:
            l_lpDevice->TimeOut.SuspendTimeOut = dwTimeOut;
            break;
        }
    }

}

// 
void EnterDeviceTimeOut(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	bTempValidate = l_bValidateDword;
	l_bValidateDword = FALSE;

    LoadString(g_hInstance,IDS_TIMEOUT,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    // indicated we would like to get the timeout value not the
    // wave file name
    bTempUseWaveFile = l_bUseWavFile;
	l_bUseWavFile = FALSE;      // used for editdlg box

    GetDeviceTimeOut();

    g_hdlg = CreateChosenDialog(g_hInstance,
	    						CtlPanelDialogChoices,
			    				g_hwnd,
				    			CtlPanelDlgProc,
					    		(LPARAM)TimeOutSelector);

}

void ExitDeviceTimeOut(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bValidateDword = bTempValidate;

    if (l_bDialogWasOKed)   
    {
        // accepted, so make changes
        SetDeviceTimeOut();
    }

    l_bDialogWasOKed = FALSE;   

    // now set the exit function to next exit function.
    if (TEXTCMP(l_lpDevice->szDeviceName,SYSTEM)==0)
        l_lpExitFunction = ExitSystemDevice;
    else
        l_lpExitFunction = ExitBacklightDevice;
	l_bUseWavFile = bTempUseWaveFile;
}


// Get the Power Info in string value so we can display it
LPTSTR GetBatteryInfo(DWORD dwIndex)
{
    // OK, we got the power info now,
    switch (dwIndex)
    {
        case BATTERY_TYPE:
            return lpszPowerInfo[l_PowerInfo.dwBatteryType];
            break;
        case BATTERY_LEVEL:
            {
                TCHAR szMsg[MAX_PATH64];

             if (l_PowerInfo.dwBatteryLevel & POWER_STATUS_GOOD)
                return lpszPowerInfo[BATTERY_GOOD];
             if (l_PowerInfo.dwBatteryLevel & POWER_STATUS_LOW)
                return lpszPowerInfo[BATTERY_LOW];
             if (l_PowerInfo.dwBatteryLevel & POWER_STATUS_VERY_LOW)
                return lpszPowerInfo[BATTERY_VERYLOW];
             if (l_PowerInfo.dwBatteryLevel & POWER_STATUS_NO_BATTERY)
                return lpszPowerInfo[BATTERY_NO_BATTERY];
    	
             TEXTSPRINTF(szMsg,TEXT("BatteryLevel %x"),l_PowerInfo.dwBatteryLevel);
	    	 MyError(TEXT("GetBatteryInfo"),szMsg);
            
             break;
            }
        case BATTERY_SOURCE:
            return (lpszPowerInfo[BATTERY_INTERNAL+l_PowerInfo.dwPowerSource]);
            break;
        default:
            return NULL;
    }

    return NULL;
}


LPTSTR GetBatteryType(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    return(GetBatteryInfo(BATTERY_TYPE));
}

LPTSTR GetBatteryLevel(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    return(GetBatteryInfo(BATTERY_LEVEL));
}

LPTSTR GetPowerSource(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    return(GetBatteryInfo(BATTERY_SOURCE));
}

void EnterPowerInfo(LPITEMSELECTOR lpis,DWORD dwSelection)
{

    DWORD dwResult = 0;

    // A MUST for all the struct info call.
    // 1. Set dwAllocated = sizeof(POWERINFO);
    // 2. Set dwUsed = 0;
    SI_ALLOC_ALL(&l_PowerInfo);
    l_PowerInfo.StructInfo.dwUsed = 0;    

    dwResult = POWER_GetPowerInfo(&l_PowerInfo);
    
    if (dwResult != E_PWR_SUCCESS )
    {
		TCHAR szMsg[MAX_PATH64];
		TEXTSPRINTF(szMsg,TEXT("Cannot Get Power Info (%x)"),dwResult);
		MyError(TEXT("EnterPowerInfo"),szMsg);
		return;
	}

	LoadString(g_hInstance,IDS_POWERGETINFO,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)PowerInfoSelector);

}

void ExitPowerInfo(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bDialogWasOKed = FALSE;
    // now set the exit function to next exit function.
    l_lpExitFunction = ExitPwrMgmt;    

}


LPTSTR GetPowerMDDVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PowerVersionInfo.dwMddVersion),LOWORD(l_PowerVersionInfo.dwMddVersion));
    lpsz = szMsg;    

    return lpsz;
}

LPTSTR GetPowerCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
    static	TCHAR szMsg[MAX_PATH8];

    TEXTSPRINTF(szMsg,TEXT("%02d.%02d"),HIWORD(l_PowerVersionInfo.dwCAPIVersion),LOWORD(l_PowerVersionInfo.dwCAPIVersion));
    lpsz = szMsg;    

    return lpsz;
}

void EnterPowerVersion(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    DWORD dwResult = 0;

    // A MUST for all the struct info call.
    // 1. Set dwAllocated = sizeof(POWER_VERSION_INFO);
    // 2. Set dwUsed = 0;
    SI_ALLOC_ALL(&l_PowerVersionInfo);
    l_PowerVersionInfo.StructInfo.dwUsed = 0;    
    
    dwResult = POWER_GetVersion(&l_PowerVersionInfo);
    if (dwResult != E_PWR_SUCCESS)
    {
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("GetPowerVersion (%x)"),dwResult);
		MyError(TEXT("EnterPowerVersion"),szMsg);
		return;
	}

	LoadString(g_hInstance,IDS_GETVERSION,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)PowerVersionSelector);

}

void ExitPowerVersion(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bDialogWasOKed = FALSE;
    // now set the exit function to next exit function.
    l_lpExitFunction = ExitPwrMgmt;    

}


// This is a lookup table to convert the control panel index to actual 
// system device state
DWORD SystemIndexToStateLookUp[5] =
{
    DEVICE_STATE_HYPER,
    DEVICE_STATE_ACTIVE,
    DEVICE_STATE_DOZE,
    DEVICE_STATE_SLEEP,
    DEVICE_STATE_SUSPEND
};

// This is a lookup table to convert the control panel index to actual 
// backlight device state
DWORD BacklightIndexToStateLookUp[5] =
{
    DEVICE_STATE_ACTIVE,
    DEVICE_STATE_SLEEP,
    DEVICE_STATE_SUSPEND,
    0,
    0
};


void SetDeviceState(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    if (TEXTCMP(l_lpDevice->szDeviceName,SYSTEM) == 0) 
//    if (bSystemDevice)
    {
        l_lpDevice->dwDeviceState = SystemIndexToStateLookUp[dwSelection];
    }
    else
        l_lpDevice->dwDeviceState = BacklightIndexToStateLookUp[dwSelection];

}

// This is a lookup table to convert the system device state to 
// control panel index
DWORD SystemStateToIndexLookUp[17] =
{
  //0,1,2,3,4,5,6,7,8,9
    0,0,1,0,2,0,0,0,3,0,
    0,0,0,0,0,0,4
};

// This is a lookup table to convert the backlight device state to 
// control panel index
DWORD BacklightStateToIndexLookUp[17] =
{
  //0,1,2,3,4,5,6,7,8,9
    4,4,0,4,4,4,4,4,1,4,
    4,4,4,4,4,4,2
};

    
DWORD GetInitialState(LPITEMSELECTOR lpis)
{

    if (TEXTCMP(l_lpDevice->szDeviceName,SYSTEM) == 0) 
//    if (bSystemDevice)
        return (SystemStateToIndexLookUp[l_lpDevice->dwDeviceState]);
    else
        return (BacklightStateToIndexLookUp[l_lpDevice->dwDeviceState]);

}

// Set device's activity mask based on the selection
void SetDeviceMask(DWORD dwSelection)
{
    DWORD dwDeviceState;
    DWORD dwResult = 0;
    TCHAR szValueName[MAX_PATH];

    // NOTE: the order of the selection is based on the order 
    // of the ActivityMaskSelector[].
    switch (dwSelection)
    {
        case 0: // Hyper
        default:
            l_lpDevice->ActivityMask.HyperDeviceMask = l_dwTempMask;
            dwDeviceState = DEVICE_STATE_HYPER;
			TEXTCPY(szValueName,TEXT("HyperDeviceMask"));
            break;
        case 1: // active
            l_lpDevice->ActivityMask.ActiveDeviceMask = l_dwTempMask;
            dwDeviceState = DEVICE_STATE_ACTIVE;
			TEXTCPY(szValueName,TEXT("ActiveDeviceMask"));
            break;
        case 2: // doze
            l_lpDevice->ActivityMask.DozeDeviceMask = l_dwTempMask;
            dwDeviceState = DEVICE_STATE_DOZE;
			TEXTCPY(szValueName,TEXT("DozeDeviceMask"));
            break;
        case 3: // sleep
            l_lpDevice->ActivityMask.SleepDeviceMask = l_dwTempMask;
            dwDeviceState = DEVICE_STATE_SLEEP;
			TEXTCPY(szValueName,TEXT("SleepDeviceMask"));
            break;
    }

    dwResult = POWER_SetDeviceMask(l_lpDevice->szDeviceName,
                                    dwDeviceState,
									l_dwTempMask);
    if (dwResult == E_PWR_SUCCESS)
	{
		TCHAR szPath[MAX_PATH];

		TEXTSPRINTF(szPath,
					POWER_DRIVER_REG TEXT("%s"),
					l_lpDevice->szDeviceName);
			
		WriteToRegistry(szPath,
						szValueName,
						&l_dwTempMask,
						sizeof(l_dwTempMask),
						REG_DWORD);
	}
	else
    {
  		TCHAR szMsg[MAX_PATH64];
    	TEXTSPRINTF(szMsg,TEXT("State (%x), Error (%x)")
               ,dwDeviceState, dwResult);
        MyError(TEXT("PowerSetDeviceMask"),szMsg);
    }
}


// Get the actual device mask
void GetDeviceMask(void)
{
    DWORD dwDeviceState;
    DWORD dwResult = 0;
    DWORD dwDeviceMask = 0xFFFFFFFF;

    // set up all the initial activity mask
    for (dwDeviceState = DEVICE_STATE_HYPER; dwDeviceState <=DEVICE_STATE_SLEEP;
        dwDeviceState = dwDeviceState * 2)
    {
        dwResult = POWER_GetDeviceMask(l_lpDevice->szDeviceName,
                                        dwDeviceState,&dwDeviceMask);
        if (dwResult != E_PWR_SUCCESS)
        {
    		TCHAR szMsg[MAX_PATH64];
	    	TEXTSPRINTF(szMsg,TEXT("State (%x), Error (%x)")
                ,dwDeviceState, dwResult);
		    MyError(TEXT("PowerGetDeviceMask"),szMsg);
        }

        switch (dwDeviceState)
        {
        case DEVICE_STATE_HYPER:
            l_lpDevice->ActivityMask.HyperDeviceMask = dwDeviceMask;
            break;
        case DEVICE_STATE_ACTIVE:
            l_lpDevice->ActivityMask.ActiveDeviceMask = dwDeviceMask;
            break;
        case DEVICE_STATE_DOZE:
            l_lpDevice->ActivityMask.DozeDeviceMask = dwDeviceMask;
            break;
        case DEVICE_STATE_SLEEP:
            l_lpDevice->ActivityMask.SleepDeviceMask = dwDeviceMask;
            break;
        }
    }

}

// This is a look up table to convert control panel index to 
// actual activity index defined in pwrdef.h.  Wakeup Source 
// use the same activity mask.
DWORD ActivityLookUp[24]=
{
    ACTIVITY_COM1RING,ACTIVITY_COM4RING,ACTIVITY_COM5RING,
	ACTIVITY_COM1DATA,ACTIVITY_COM4DATA,ACTIVITY_COM5DATA,
	ACTIVITY_ALARM, ACTIVITY_TOUCH, ACTIVITY_KEYBOARD,
	ACTIVITY_TRIGGER1,ACTIVITY_TRIGGER2,ACTIVITY_TRIGGER3,
	ACTIVITY_CRADLEREMOVAL,ACTIVITY_CRADLEINSERT,ACTIVITY_ACCONNECT,
	ACTIVITY_POWERSWITCH,ACTIVITY_BATTERYINSERT,ACTIVITY_BATTERYEJECT,
	ACTIVITY_SOFTWARE,ACTIVITY_PCMCIA0,ACTIVITY_PCMCIA1
};

// update the temp mask based on the selection
void SetMaskValue(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    LPITEMSELECTOR lprs;
	DWORD dwSelector;

	lprs = ActivitySelector;
    // get which one is selected
	dwSelector = (lpis - lprs);
    
    if (dwSelection)    // if chose yes
        l_dwTempMask = l_dwTempMask | ActivityLookUp[dwSelector];
    else
        l_dwTempMask = l_dwTempMask & (~ActivityLookUp[dwSelector]);

}


DWORD GetInitialMaskValue(LPITEMSELECTOR lpis)
{
    LPITEMSELECTOR lprs;
	DWORD dwSelector;

	lprs = ActivitySelector;
    // dwSelector shows which activity or wakeup source we wanted
	dwSelector = (lpis - lprs); 

    if ( l_dwTempMask & ActivityLookUp[dwSelector])
       return 1;
    else
       return 0;
}

// Activity Mask Functions
void EnterActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    LoadString(g_hInstance,IDS_ACTIVITYMASK,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

// dwSelection = which state we are in
    switch (dwSelection)
    {
        case 0: // Hyper
        default:
            l_dwTempMask = l_lpDevice->ActivityMask.HyperDeviceMask;
            break;
        case 1: // active
            l_dwTempMask = l_lpDevice->ActivityMask.ActiveDeviceMask;
            break;
        case 2: // doze
            l_dwTempMask = l_lpDevice->ActivityMask.DozeDeviceMask;
            break;
        case 3: // sleep
            l_dwTempMask = l_lpDevice->ActivityMask.SleepDeviceMask;
            break;
    }

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ActivitySelector);


}

void ExitActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection)
{

    if (l_bDialogWasOKed)
    {
        SetDeviceMask(dwSelection);
    }

	l_bDialogWasOKed = FALSE;

    // now set the exit function to next exit function.
    l_lpExitFunction = ExitDeviceActivityMask;    

}

void EnterDeviceActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    LoadString(g_hInstance,IDS_ACTIVITYMASK,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    // set up activity mask
    GetDeviceMask();
    
	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ActivityMaskSelector);

}

void ExitDeviceActivityMask(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    // now set the exit function to next exit function.
    if (TEXTCMP(l_lpDevice->szDeviceName,SYSTEM)==0)
        l_lpExitFunction = ExitSystemDevice;
    else
        l_lpExitFunction = ExitBacklightDevice;

	l_bDialogWasOKed = FALSE;

}

void SetWakeUpMask(void)
{
    DWORD dwValue = l_lpDevice->dwWakeUpSource;

	WriteToRegistry(POWER_DRIVER_REG POWER_DRIVER_SYSTEM,
				    POWER_POWERUPACTIVITIES,
					&dwValue,
					sizeof(dwValue),
					REG_DWORD);

	MyError(TEXT("WakeUp Source Requires"),
			TEXT("Warm Boot to take effect"));
}

DWORD GetWakeUpMask(void)
{
    DWORD dwRetCode = 0;
    HKEY hKey;

    // get the symbol printer infomation
    dwRetCode = RegOpenKeyEx(REG_SECTION,TEXT("Drivers\\BuiltIn\\PowerDrv\\Load\\System"),0,KEY_READ, &hKey);
    
    if (dwRetCode == ERROR_SUCCESS)
    {
        // query for "PowerUpActivities"
        DWORD dwLen;
        DWORD dwType;
        DWORD dwValue;

        dwLen = sizeof(DWORD);
        dwRetCode = RegQueryValueEx(hKey,TEXT("PowerUpActivities"),NULL,&dwType,(LPBYTE)&dwValue,&dwLen);
        RegCloseKey(hKey);
        
        // no default, use the first one on the list
        if ((dwRetCode != ERROR_SUCCESS))
        {
            dwValue = 0x10000;
        }
        return dwValue;
    }
    else
        return (DWORD)0x10000;  // use default is fail to open reg key

}

void EnterWakeUpSource(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    LoadString(g_hInstance,IDS_WAKEUPSOURCE,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    l_lpDevice->dwWakeUpSource = l_dwTempMask = GetWakeUpMask();
    
	l_bTouchAllowed = FALSE;
	
	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ActivitySelector);


}

void ExitWakeUpSource(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bTouchAllowed = TRUE;
	
    if (l_bDialogWasOKed)
    {
        l_lpDevice->dwWakeUpSource = l_dwTempMask;
        SetWakeUpMask();
    }

	l_bDialogWasOKed = FALSE;

    // now set the exit function to next exit function.
    if (TEXTCMP(l_lpDevice->szDeviceName,SYSTEM)==0)
        l_lpExitFunction = ExitSystemDevice;
    else
        l_lpExitFunction = ExitBacklightDevice;

}


void EnterBacklightDevice(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    DWORD dwResult = 0;
    DWORD dwDeviceState = DEVICE_STATE_ACTIVE;

    LoadString(g_hInstance,IDS_BACKLIGHT_INTENSITY,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

//    bDozeSupported = bSuspendSupported = bWakeupSupported = bSleepSupported = FALSE;
    bDozeSupported = bSuspendSupported = bWakeupSupported = FALSE;
	bSleepSupported = TRUE;
    bSystemDevice = FALSE;
    bBacklightDevice = TRUE;
    
    TEXTCPY(l_lpDevice->szDeviceName,BACKLIGHT);
    l_lpDevice->dwSupportedStates = 0x1A;       // got this from reg, should be calculated
    
    dwResult = POWER_GetDeviceState(BACKLIGHT,&dwDeviceState);
    
    if (dwResult != E_PWR_SUCCESS)
    {
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("PwrGetState (%x)"),dwResult);
		MyError(TEXT("EnterBacklightDevice"),szMsg);
		return;
	}

    if (!(l_lpDevice->dwSupportedStates & dwDeviceState))
    {
 		TCHAR szMsg[MAX_PATH64];
		TEXTSPRINTF(szMsg,TEXT("DeviceState Not Supported(%x)"),dwDeviceState);
		MyError(TEXT("BacklightDevice"),szMsg);
    }

    l_lpDevice->dwDeviceState = dwDeviceState;
	
    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)DeviceAttributeSelector);

}


void ExitBacklightDevice(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    if (l_bDialogWasOKed)
    {
		DWORD dwState,dwErr;

		dwErr = POWER_GetDeviceState(BACKLIGHT,&dwState);
		if (dwErr == E_PWR_SUCCESS)
		{
        	if (dwState != l_lpDevice->dwDeviceState)
	        	POWER_SetDeviceState(BACKLIGHT,l_lpDevice->dwDeviceState);
		}
    }

	l_bDialogWasOKed = FALSE;
    // now set the exit function to next exit function.
    l_lpExitFunction = ExitPwrMgmt;    

}


DWORD l_dwWarmBootTime;
DWORD l_dwColdBootTime;


void SetBootTimes(void)
{
	WriteToRegistry(POWER_DRIVER_REG POWER_DRIVER_SYSTEM,
					POWER_WARMBOOTTIMEOUT,
					&l_dwWarmBootTime,
					sizeof(DWORD),
					REG_DWORD);

	WriteToRegistry(POWER_DRIVER_REG POWER_DRIVER_SYSTEM,
					POWER_COLDBOOTTIMEOUT,
					&l_dwColdBootTime,
					sizeof(DWORD),
					REG_DWORD);
}


void GetBootTimes(void)
{
	DWORD dwType;
	DWORD dwSize;

	l_dwWarmBootTime = 6;
	l_dwColdBootTime = 15;

	dwType = REG_DWORD;
	dwSize = sizeof(DWORD);
	ReadFromRegistry(POWER_DRIVER_REG POWER_DRIVER_SYSTEM,
					 POWER_WARMBOOTTIMEOUT,
					 &l_dwWarmBootTime,
					 &dwSize,
					 &dwType);

	dwType = REG_DWORD;
	dwSize = sizeof(DWORD);
	ReadFromRegistry(POWER_DRIVER_REG POWER_DRIVER_SYSTEM,
					 POWER_COLDBOOTTIMEOUT,
					 &l_dwColdBootTime,
					 &dwSize,
					 &dwType);
}


void SetWarmBootTime(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if ( lpis != NULL )
	{
		l_dwWarmBootTime = GetValueFromIndex(lpis,dwSelection);
	}
}


DWORD GetWarmBootTime(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( lpis != NULL )
	{
		dwIndex = GetIndexFromValue(lpis,l_dwWarmBootTime);
	}

	return(dwIndex);
}


void SetColdBootTime(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if ( lpis != NULL )
	{
		l_dwColdBootTime = GetValueFromIndex(lpis,dwSelection);
	}
}


DWORD GetColdBootTime(LPITEMSELECTOR lpis)
{
	DWORD dwIndex = 0;

	if ( lpis != NULL )
	{
		dwIndex = GetIndexFromValue(lpis,l_dwColdBootTime);
	}

	return(dwIndex);
}


void EnterRebootTimes(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    DWORD dwResult = 0;

    LoadString(g_hInstance,IDS_REBOOTTIMES,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	GetBootTimes();

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)RebootTimesSelector);
}


void ExitRebootTimes(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    if (l_bDialogWasOKed)
    {
		SetBootTimes();
    }

	l_bDialogWasOKed = FALSE;

    // now set the exit function to next exit function.
    l_lpExitFunction = ExitPwrMgmt;    
}


void EnterSystemDevice(LPITEMSELECTOR lpis,DWORD dwSelection)
{

    DWORD dwDeviceState=DEVICE_STATE_ACTIVE;
    DWORD dwResult = 0;

    LoadString(g_hInstance,IDS_SYSTEM,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    bDozeSupported = bSuspendSupported = bWakeupSupported = bSleepSupported = TRUE;
    bSystemDevice = TRUE;
    bBacklightDevice = FALSE;

    TEXTCPY(l_lpDevice->szDeviceName,SYSTEM);
    
    l_lpDevice->dwSupportedStates = 0x1F;       // got this from reg, should be calculated
    
    dwResult = POWER_GetDeviceState(SYSTEM,&dwDeviceState);
    
    if (dwResult != E_PWR_SUCCESS)
    {
		TCHAR szMsg[MAX_PATH32];
		TEXTSPRINTF(szMsg,TEXT("PwrGetState (%x)"),dwResult);
		MyError(TEXT("EnterSystemDevice"),szMsg);
		return;
	}

    if (!(l_lpDevice->dwSupportedStates & dwDeviceState))
    {
 		TCHAR szMsg[MAX_PATH64];
		TEXTSPRINTF(szMsg,TEXT("DeviceState Not Supported(%x)"),dwDeviceState);
		MyError(TEXT("SystemDevice"),szMsg);
    }

    l_lpDevice->dwDeviceState = dwDeviceState;

    g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)DeviceAttributeSelector);


}

void ExitSystemDevice(LPITEMSELECTOR lpis,DWORD dwSelection)
{

    if (l_bDialogWasOKed)
    {
        POWER_SetDeviceState(SYSTEM,l_lpDevice->dwDeviceState);
    }

   	l_bDialogWasOKed = FALSE;
	// now set the exit function to next exit function.
    l_lpExitFunction = ExitPwrMgmt;    

}


void EnterPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    DWORD dwResult = 0;

	LoadString(g_hInstance,IDS_POWERMGMT,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

    l_lpDevice = (DEVICEATTRIBUTE *)LocalAlloc(LMEM_FIXED,sizeof(DEVICEATTRIBUTE));

    dwResult = POWER_Open();
    
    if (dwResult == E_PWR_SUCCESS)
    {

	    g_hdlg = CreateChosenDialog(g_hInstance,
		    						CtlPanelDialogChoices,
				    				g_hwnd,
					    			CtlPanelDlgProc,
						    		(LPARAM)PowerManagementSelector);
    }
    else
        ReportError(TEXT("POWER_Open"),dwResult);

}

void ExitPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	l_bDialogWasOKed = FALSE;

    POWER_Close();

    LocalFree((HLOCAL)l_lpDevice);
}


#endif
