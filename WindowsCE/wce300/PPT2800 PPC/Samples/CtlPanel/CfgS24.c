
//--------------------------------------------------------------------
// FILENAME:            CfgS24.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Module to view and config S24 settings
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>

#include <ctype.h>

// for inet_add()
#include <winsock.h>

// Symbol SDK common includes

#include "..\common\StdGlobs.h"

#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"

// Ctlpanel includes
#include "ctlpanel.h"
#include "choices.h"

// *** This #ifdef MUST BE PLACED after ctlpanel.h and before module header
#ifdef _S24CTL

#include "S24.h"
#include "CfgS24.h"


#define USE_UPDATE_REG


// global variable to hold S24 information
S24INFO      S24Info;
S24NETWORKINFO	S24NetworkInfo;
BOOL bTemp;
TCHAR szParmsRegBase[MAX_PATH];
TCHAR szTcpIpRegBase[MAX_PATH];

S24ICMPINFO		OldS24IcmpInfo;

extern BOOL bDS;
extern BOOL bFH;	
extern BOOL bFuncMode;
extern S24ICMPINFO S24IcmpInfo;
static S24SETTING	OldS24Setting;

static BOOL g_bS24IDOKed = FALSE;
static BOOL g_bS24SystemOKed = FALSE;
static BOOL g_bNeedUpdate = TRUE;		// default we need to update s24info
static HINSTANCE hLib;

extern LRESULT CALLBACK S24SignalProc(HWND hwnd, UINT uMsg,
									  WPARAM wParam, LPARAM lParam);

extern LRESULT CALLBACK S24PingProc(HWND hwnd,  UINT uMsg,
									  WPARAM wParam, LPARAM lParam);
BOOL IpClear(void);


// -------------------------------------------------------------
//  FUNCTION: SetRFRegBase
//
//  PROTOTYPE: BOOL SetRFRegBase(LPTSTR lpsz);
//
//  PARAMETERS: lpsz: pointer to the string contain the S24 linkage
//					 (for example, SLACE, or SLA41ND41) 
//
//  DESCRIPTION: set the regbase for parms and TcpIp based on the 
//				 HKLM\Comm\Linkage\Route
//
//  RETURNS: 	TRUE if success; FALSE otherwise
//
//  NOTES:	
// -------------------------------------------------------------
BOOL SetRFRegBase(LPTSTR lpsz)
{
	DWORD	dwRetCode = 0;
	HKEY	hkCommLinkage;
	TCHAR	szRegPath[MAX_PATH64];

	if (lpsz == NULL)
	{
		MyError(TEXT("NULL Pointer"),TEXT("SetRFRegBase"));
		return FALSE;
	}

	// construct the key 
	//	[HKEY_LOCAL_MACHINE\Comm\SLACE\Linkage]
	//   "Route"=multi_sz:"SLACE1"
	wsprintf(szRegPath,TEXT("Comm\\%s\\Linkage"),lpsz);
	dwRetCode = RegOpenKeyEx (HKEY_LOCAL_MACHINE, szRegPath, 0, KEY_ALL_ACCESS, &hkCommLinkage);
	if (dwRetCode == ERROR_SUCCESS)
	{
        DWORD dwLen;
        DWORD dwType;
		TCHAR szTemp[MAX_PATH64];

        dwLen = countof(szTemp);
		ZEROMEM(szTemp,sizeof(szTemp));

        dwRetCode = RegQueryValueEx (hkCommLinkage, TEXT("Route"), NULL, 
									&dwType, (LPBYTE) szTemp, &dwLen);
		
        // no data found
        if( (dwRetCode != ERROR_SUCCESS) || (dwLen <=2) )
        {
			// use the default value
			TEXTCPY(szTemp,SLACE1);
        }

		// construct the regbase key
		wsprintf(szParmsRegBase,TEXT("Comm\\%s\\Parms"),szTemp);
		wsprintf(szTcpIpRegBase,TEXT("Comm\\%s\\Parms\\TcpIp"),szTemp);
		
		RegCloseKey(hkCommLinkage);
	}

	return TRUE;
}


// -------------------------------------------------------------
//  FUNCTION: UpdateRegistry
//
//  PROTOTYPE: BOOL UpdateRegistry(LPTSTR lpszKeyName, 
//							LPTSTR lpszPath, DWORD dwType, LPVOID lpvData);
//
//  PARAMETERS: lpszKeyName: pointer to the Reg group key (except root key)
//				lpszPath: which entry key
//				dwType: what type (REG_DWORD, REG_SZ, etc)
//				lpvData: pointer to the data (can be dword, or tchar)
//
//  DESCRIPTION: Update the system registry setting and create registry file
//				 if permanent option is selected
//
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL UpdateRegistry(LPTSTR lpszKeyName, LPTSTR lpszPath, DWORD dwType, LPVOID lpvData)
{
	DWORD	dwRetCode = 0;
	HKEY	hKey;

	if ((lpszPath == NULL) || (lpszKeyName == NULL))
	{
		MyError(TEXT("NULL Pointer"),NULL);
		return FALSE;
	}

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,lpszKeyName,0,KEY_ALL_ACCESS, &hKey);
     
    if (dwRetCode == ERROR_SUCCESS)
    {
		// only works with REG_SZ and REG_DWORD
		if (dwType == REG_SZ)
		{
			LPTSTR lpszData = (LPTSTR)lpvData;

			dwRetCode = RegSetValueEx(hKey,lpszPath,0,dwType,
									  (LPBYTE)lpszData,((TEXTLEN(lpszData)+1)*sizeof(TCHAR)));
			
	        if (dwRetCode != ERROR_SUCCESS)
		    {
			    MyError(TEXT("Error Set Key"),lpszPath);
				RegCloseKey(hKey);
			    return FALSE;
			}

			// if we set permanent option, create the reg file on \Platform
			if (l_dwPersistence == PERSISTENCE_PERMANENT)
			{
				dwRetCode = WriteRegFile(szParmsRegBase,
		   								 lpszPath,
		    							 REG_SZ,
			    						 lpszData,
				    					 countof(lpszData),
										 0);

				if (dwRetCode != ERROR_SUCCESS)
				{
					ReportError(TEXT("ERROR WriteReg"),dwRetCode);
				}
			}
		}
		else
		{
			LPWORD lpdwData = (LPWORD)lpvData;
			
			dwRetCode = RegSetValueEx(hKey,lpszPath,0,dwType, (LPBYTE)lpdwData,sizeof(DWORD));
        
	        if (dwRetCode != ERROR_SUCCESS)
		    {
			    MyError(TEXT("Error Set Key"),lpszPath);
				RegCloseKey(hKey);
			    return FALSE;
			}
			// if we set permanent option, create the reg file on \Platform
	        if (l_dwPersistence == PERSISTENCE_PERMANENT)
			{
				dwRetCode = WriteRegFile(szParmsRegBase,
		   							     lpszPath,
		    						     REG_DWORD,
			    					     lpdwData,
				    					 sizeof(DWORD),
										 0);

				if (dwRetCode != ERROR_SUCCESS)
				{
					ReportError(TEXT("ERROR WriteReg"),dwRetCode);
				}
			}
		}

		RegCloseKey(hKey);

		return TRUE;

	}
	else
		return FALSE;
}


// -------------------------------------------------------------
//  FUNCTION: Update_S24_Registry
//
//  PROTOTYPE:	BOOL Update_S24_Registry(DWORD dwFunCode);
//
//  PARAMETERS:	dwFunCode:	S24 Mudll function number
//
//  DESCRIPTION: update S24 related registry setting based 
//				on the S24 mudll function code
//				valid function codes are:
//					S24_SET_ADAPTER_ESS_ID, S24_SET_ADAPTER_POWER_MODE,
//					S24_SET_ANTENNA_DIVERSITY, S24_SET_BEACON_PARAMETERS,
//					S24_SET_FUNCTIONAL_MODE, 
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES: This is a workaround for the bug on S24_COMMIT_PARAMETER_CHANGES
//		   to store the changes we make in the system regsitry 
//		    Another note to S24_SET_POWER_MODE, the valid registry values for
//			the power mode are 1 for CAM, 2 for PSP; however, it's not
//			the same as the API value (which is 0 for CAM, 1 for PSP)!

//  PJK 07-23-01: This function did not operate correctly when processing the 
//  S24_SET_SUPPORTED_DATA_RATES constant.  Apparently the default line in the
//  switch was added after the code to write the data to the registry.  As a result  
//  If the S24_SET_SUPPORTED_DATA_RATES was the dwFunCode
//  value, the default case statement value will be entered and the function will 
//  return FALSE.  In order to correct this problem, an entry for S24_SET_SUPPORTED_DATA_RATES
//  has been added.
//
// -------------------------------------------------------------
BOOL Update_S24_Registry(DWORD dwFunCode)
{
	static TCHAR szPath[MAX_PATH64];
	static TCHAR szVal[MAX_PATH64];
	static DWORD dwVal,dwType;
	
	switch (dwFunCode)
	{
	case S24_SET_ADAPTER_ESS_ID:
		TEXTCPY(szPath,TEXT("ESS_ID"));
#ifdef UNICODE
		// need to convert the bS24ESSID to UNICODE
	    MultiByteToWideChar(CP_OEMCP, MB_PRECOMPOSED,
							S24Info.bS24ESSID,
							sizeof(S24Info.bS24ESSID),
							szVal,
							countof(szVal));
#else
		TEXTCPY(szVal,S24Info.bS24ESSID);
#endif
		dwType = REG_SZ;
		break;

	case S24_SET_ADAPTER_POWER_MODE:
		TEXTCPY(szPath,TEXT("PowerMode"));
		dwType = REG_DWORD;
		// !!!
		// !!! when the S24 driver read in the registry setting, 
		// !!! it uses 1 for CAM and 2 for PSP 
		// !!!
		dwVal = S24Info.nPowerMode + 1;
		//
		// in case of power index, since we already make call to setPowermode
		// with the power index, PowerMode is update to date with power index
		//
		break;
	
	case S24_SET_ANTENNA_DIVERSITY:
		TEXTCPY(szPath, TEXT("Diversity"));
		dwType =REG_DWORD;
		dwVal = S24Info.nDiversity;
		break;
	
	case S24_SET_BEACON_PARAMETERS:
		TEXTCPY(szPath,TEXT("BeaconAlgorithm"));
		dwType = REG_DWORD;
		dwVal = S24Info.nBeaconAlgorithm;
		break;
	// this function need to be verified
    case S24_SET_FUNCTIONAL_MODE:
        TEXTCPY(szPath,TEXT("MicroAP"));
        dwType = REG_DWORD;
        if (S24Info.bMicroAP)   // MicroAP
            dwVal = 0x04;
        else
            dwVal = 0;
        break;    
	//PJK 07-23-01
	case S24_SET_SUPPORTED_DATA_RATES:
		//Processing is done later in the function.		
	break;
	default:
		// if passed in invalid function code, return
		return (FALSE);
		break;   			
	}
		
	// Update the registry
	if (dwType == REG_SZ)
		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)szVal);
	else
		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);

	// Special process for some function code
	
	// need to do something special for Beacon
	if (dwFunCode == S24_SET_BEACON_PARAMETERS)
	{
		// we need to set the min and max as well
		TEXTCPY(szPath,TEXT("BeaconMinimum"));
		dwVal = S24Info.nBeaconMin;

		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);
		TEXTCPY(szPath,TEXT("BeaconMaximum"));
		dwVal = S24Info.nBeaconMax;
		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);
	}
			

	if (dwFunCode == S24_SET_ADAPTER_POWER_MODE)
	{
		// for PowerMode, we need to do something else if DS driver set is loaded
		// In DS driver, it uses PowerIndex instead of Power mode
		if (S24_IsDS())
		{
			TEXTCPY(szPath,TEXT("PowerIndex"));
			dwType = REG_DWORD;

			dwVal = S24Info.nPowerIndex;

//			ReportError(TEXT("SetRegValue"),dwVal);

			UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);
		}
	}

	// need to do something special for Beacon
	if (dwFunCode == S24_SET_SUPPORTED_DATA_RATES)
	{
		// DS driver support 5.5 and 11 MBit as well
		if (S24_IsDS())
		{
			TEXTCPY(szPath,TEXT("MBIT_11"));
			dwVal = S24Info.n11MBit;
			UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);

			TEXTCPY(szPath,TEXT("MBIT_5_5"));
			dwVal = S24Info.n5MBit;
			UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);
		}

		TEXTCPY(szPath,TEXT("MBIT_2"));
		dwVal = S24Info.n2MBit;
		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);

		TEXTCPY(szPath,TEXT("MBIT_1"));
		dwVal = S24Info.n1MBit;
		UpdateRegistry(szParmsRegBase, szPath, dwType, (LPVOID)&dwVal);

	}

	return TRUE;
}


// -------------------------------------------------------------
//  FUNCTION: DS_GetPowerIndex
//
//  PROTOTYPE: DWORD DS_GetPowerIndex(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Return the Power Index for DS driver
//
//  RETURNS: Power Index 
//
//  NOTES:
// -------------------------------------------------------------
DWORD DS_GetPowerIndex(void)
{
	DWORD	dwRetCode = 0;
	HKEY	hKey;
	DWORD	dwType,dwVal,dwLen;

	dwVal = 0;

	// just to makesure we are dealing with DS driver
	if (bDS)
	{

		dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,szParmsRegBase,0,
								 KEY_ALL_ACCESS, &hKey);

		if (dwRetCode == ERROR_SUCCESS)
		{
			// go read the power index
			dwLen = sizeof(DWORD);
			dwRetCode = RegQueryValueEx(hKey,TEXT("PowerIndex"),NULL,
										&dwType,(LPBYTE)&dwVal,&dwLen);

			RegCloseKey(hKey);
		}
	}

	return dwVal;

}


// -------------------------------------------------------------
//  FUNCTION: SetS24IP
//
//  PROTOTYPE: void SetS24IP(LPTSTR lpszPath, LPTSTR lpszVal, DWORD dwLen);
//
//  PARAMETERS: lpszPath: pointer to the registry subkey
//				lpszVal: pointer to the data
//				dwLen:	sizeof the data
//
//  DESCRIPTION: This function set the S24 IP network releated registry settings
//				 including: IPAddress, subnetmask, DNS, WINS, and Gateway
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetS24IP(LPTSTR lpszPath, LPTSTR lpszVal, DWORD dwLen)
{
    DWORD dwRetCode=0;
    HKEY hKey;

	if ((lpszPath == NULL) || (lpszVal == NULL))
	{
		MyError(TEXT("Null Pointer"),NULL);
		return;
	}

    dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,szTcpIpRegBase,0,KEY_ALL_ACCESS, &hKey);
      
    if (dwRetCode == ERROR_SUCCESS)
    {
        // we got to be careful about the sizeof the string
		//dwRetCode = RegSetValueEx(hKey,lpszPath,0,REG_MULTI_SZ,(LPBYTE)lpszVal,(TEXTLEN(lpszVal)+1)*sizeof(TCHAR));
		dwRetCode = RegSetValueEx(hKey,lpszPath,0,REG_MULTI_SZ,(LPBYTE)lpszVal,dwLen);


        if (dwRetCode != ERROR_SUCCESS)
        {
            MyError(TEXT("Error Set Key"),lpszPath);
	        RegCloseKey(hKey);
            return;
        }

        if (l_dwPersistence == PERSISTENCE_PERMANENT)
		{
			DWORD dwRet;
			dwRet = WriteRegFile(szTcpIpRegBase,
	   							 lpszPath,
	    						 REG_MULTI_SZ,
		    					 lpszVal,
			    				 countof(lpszVal),
								 0);

			if (dwRet != ERROR_SUCCESS)
			{
				ReportError(TEXT("ERROR WriteReg"),dwRet);
			}
		}


	}
	else
	{
		ReportError(TEXT("Error Open IP Key"),dwRetCode);
	}

	RegCloseKey(hKey);          
}


// -------------------------------------------------------------
//  FUNCTION: GetS24IP
//
//  PROTOTYPE:void GetS24IP(LPTSTR lpszPath, LPTSTR lpszVal);
//
//  PARAMETERS:		lpszPath: pointer to which key to get
//					lpszVal: pointer to store the data
//
//  DESCRIPTION: Read the registry key from system.
//
//  RETURNS:	None 
//
//  NOTES: If can't open the key, this function will create the
//			key and set the dummy value.
// -------------------------------------------------------------
void GetS24IP(LPTSTR lpszPath, LPTSTR lpszVal)
{
    DWORD dwRetCode=0;
    HKEY hKey;

	// make sure we got the valid pointers
	if ((lpszPath == NULL) || (lpszVal == NULL))
	{
		MyError(TEXT("Null Pointer"),NULL);
		return;
	}

    dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,szTcpIpRegBase,0,KEY_ALL_ACCESS, &hKey);
    
	// if we can't open the key, then create it
	if (dwRetCode != ERROR_SUCCESS)
	{
		DWORD dwDisp;
		
		dwRetCode = RegCreateKeyEx( HKEY_LOCAL_MACHINE, szTcpIpRegBase, 0, NULL, 
									REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 
									NULL, &hKey, &dwDisp);

		if (dwRetCode == ERROR_SUCCESS)
		{
			TCHAR	szData[MAX_PATH32] = TEXT("0.0.0.0");
			DWORD	dwLen;

			dwLen = (TEXTLEN(szData)+1)*sizeof(TCHAR);

			// set it the dummy value: 0.0.0.0
			dwRetCode = RegSetValueEx(hKey,lpszPath,0,REG_SZ,(LPBYTE)szData,dwLen);
			
			if (dwRetCode != ERROR_SUCCESS)
			{
				ReportError(TEXT("Error Set Key"),dwRetCode);
			}
		}

	}

    if (dwRetCode == ERROR_SUCCESS)
    {
    	// read the subnetmask and ipaddress
        DWORD dwLen;
        DWORD dwType;
		TCHAR szTemp[MAX_PATH128];

        dwLen = countof(szTemp);
		ZEROMEM(szTemp,sizeof(szTemp));

        dwRetCode = RegQueryValueEx(hKey,lpszPath,NULL,&dwType,(LPBYTE)szTemp,&dwLen);
        
        // no data found
        if( (dwRetCode != ERROR_SUCCESS) || (dwLen <=2) )
        {
			ZEROMEM(lpszVal,sizeof(szTemp));
	        RegCloseKey(hKey);
            return;
        }
		else
		{
		// use memcpy instead of strcpy so that we can copy the NULL character as well
			memcpy(lpszVal,szTemp,dwLen);
		}
			
		// we'll let the calling function takecare of the multiple string
	}

	RegCloseKey(hKey);          
}


// -------------------------------------------------------------
//  FUNCTION: SetS24DHCP
//
//  PROTOTYPE: void SetS24DHCP(BOOL bEnable);
//
//  PARAMETERS: bEnable: enable or disable DHCP
//
//  DESCRIPTION: To set the registry to either enable or disable 
//				S24 DHCP setting.  
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void SetS24DHCP(BOOL bEnable)
{
	DWORD	dwRetCode;
	HKEY	hKey;


    dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,szTcpIpRegBase,0,KEY_ALL_ACCESS, &hKey);
      
    if (dwRetCode == ERROR_SUCCESS)
    {
		DWORD dwVal;

		if (bEnable)
			dwVal = 1;
		else
			dwVal = 0;

        dwRetCode = RegSetValueEx(hKey,DHCP,0,REG_DWORD,(LPBYTE)&dwVal,sizeof(DWORD));

        // no data found
        if (dwRetCode != ERROR_SUCCESS)
        {
            MyError(TEXT("Error Set Key"),TEXT("EnableDHCP"));
	        RegCloseKey(hKey);
            return;
        }

		// need to clear IP and Netmask as well
		if (bEnable)
		{
			DWORD dwLen;

			dwLen = (TEXTLEN(TEXT("0.0.0.0"))+1)*sizeof(TCHAR);

			SetS24IP(IPADDRESS, TEXT("0.0.0.0"), dwLen);	// size = 8 (with null char)
			SetS24IP(SUBNETMASK,TEXT("0.0.0.0"), dwLen);
		}
		else
		{
			// call IPClear to clear the DHCP setting
			IpClear();
		}

        if (l_dwPersistence == PERSISTENCE_PERMANENT)
		{
			dwRetCode = WriteRegFile(szTcpIpRegBase,
									 DHCP,
	    							 REG_DWORD,
		    						 &bEnable,
			    					 sizeof(DWORD),
									 0);

			if (dwRetCode != ERROR_SUCCESS)
			{
				ReportError(TEXT("ERROR WriteReg"),dwRetCode);
			}
		}
	}
	else
	{
		ReportError(TEXT("Error Open TCP/IP Key"),dwRetCode);
	}

	RegCloseKey(hKey);          
}
	

// -------------------------------------------------------------
//  FUNCTION: GetS24DHCP
//
//  PROTOTYPE:	BOOL GetS24DHCP(LPDWORD lpdwDHCP);
//
//  PARAMETERS: lpdwDHCP: dword pointer to hold the registry value
//
//  DESCRIPTION: Get the current DHCP setting
//
//  RETURNS:	TRUE if success, FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL GetS24DHCP(LPDWORD lpdwDHCP)
{
    DWORD	dwRetCode;
	HKEY	hKey;
	// set to zero
	*lpdwDHCP = 0;

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,szTcpIpRegBase,0,KEY_ALL_ACCESS, &hKey);
      
    if (dwRetCode == ERROR_SUCCESS)
    {
    	// read the subnetmask and ipaddress
        DWORD dwLen;
        DWORD dwType;
		DWORD dwVal;

        dwLen = sizeof(DWORD);
        dwRetCode = RegQueryValueEx(hKey,DHCP,NULL,&dwType,(LPBYTE)&dwVal,&dwLen);
        
        // no data found
        if (dwRetCode != ERROR_SUCCESS)
        {
            ReportError(TEXT("Error Query DHCP Key"),dwRetCode);
	        RegCloseKey(hKey);
            return FALSE;
        }

		*lpdwDHCP = dwVal;
		return TRUE;
	}
    RegCloseKey(hKey);
	return FALSE;
}


// -------------------------------------------------------------
//  FUNCTION: GetCurrentS24NetworkConfig
//
//  PROTOTYPE: GetCurrentS24NetworkConfig(LPS24NETWORKINFO lpS24NetworkInfo);
//
//  PARAMETERS: lpS24NetworkInfo: pointer to the S24 Network information
//				structure (see S24.h for Structure typedef) 
//
//  DESCRIPTION: Get the current S24 network settings into the structure
//
//  RETURNS:	TRUE if success, FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL GetCurrentS24NetworkConfig(LPS24NETWORKINFO lpS24NetworkInfo)
{
	DWORD dwVal;
	TCHAR szVal[MAX_PATH];
	
	if (NULL == lpS24NetworkInfo)
	{
		MyError(TEXT("NULL pointer"),NULL);
		return FALSE;
	}

	ZEROMEM(lpS24NetworkInfo, sizeof(S24NETWORKINFO));

	// get the DHCP
	if (!GetS24DHCP(&dwVal))
		MyError(TEXT("Error GetS24DHCP"),NULL);

	// if DHCP is enabled, check DHCPIpAddress ... 
	lpS24NetworkInfo->dwDHCPEnable = dwVal;
	
	// get the rest
	GetS24IP(IPADDRESS,lpS24NetworkInfo->szIPAddr);
	GetS24IP(SUBNETMASK,lpS24NetworkInfo->szNetMask);

	GetS24IP(DNS,szVal);
	TEXTCPY(lpS24NetworkInfo->szDNS[0],szVal);
	if (szVal[TEXTLEN(szVal) + 1] != TEXT('\0'))
	{
		TEXTCPY(lpS24NetworkInfo->szDNS[1], (LPTSTR)szVal+(TEXTLEN(szVal) + 1));
//		MyError(TEXT("Secondary DNS"),lpS24NetworkInfo->szDNS[1]);
	}


	GetS24IP(WINS,szVal);
	TEXTCPY(lpS24NetworkInfo->szWINS[0],szVal);
	if (szVal[TEXTLEN(szVal) + 1] != TEXT('\0'))
	{
		TEXTCPY(lpS24NetworkInfo->szWINS[1], (LPTSTR)szVal+(TEXTLEN(szVal) + 1));
//		MyError(TEXT("Secondary WINS"),lpS24NetworkInfo->szWINS[1]);
	}

	GetS24IP(GATEWAY,szVal);
	TEXTCPY(lpS24NetworkInfo->szGateway[0],szVal);
	if (szVal[TEXTLEN(szVal) + 1] != TEXT('\0'))
	{
		TEXTCPY(lpS24NetworkInfo->szGateway[1], (LPTSTR)szVal+(TEXTLEN(szVal) + 1));
//		MyError(TEXT("Secondary Gateway"),lpS24NetworkInfo->szGateway[1]);
	}
	return TRUE;
    
}


// -------------------------------------------------------------
//  FUNCTION: S24InfoInit
//
//  PROTOTYPE: void S24InfoInit(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Initialize the S24Info strucutre (see S24.h for 
//				 Structure typedef)
//
//  RETURNS: 
//
//  NOTES:
// -------------------------------------------------------------
void S24InfoInit(void)
{
    // initialize S24Info variable to known state
    SI_ALLOC_ALL(&S24Info);

    ZEROMEM(S24Info.bMUAddress,6);
    ZEROMEM(S24Info.bAPAddress,6);
    ZEROMEM(S24Info.bFirmwareVersion,S24_VD_MAX_FW_INFO);
    ZEROMEM(S24Info.bS24ESSID,32);
    ZEROMEM(S24Info.bCountryText,36);
    
    S24Info.nDiversity = S24Info.nPowerMode = S24Info.nAdapterType = S24Info.nPowerIndex = 0;
	S24Info.nFirmwareType = S24Info.nTxRetries = S24Info.nRoamCount = 0;
	S24Info.nLinkSpeed = S24Info.nCountryCode = 0;
    S24Info.nBeaconAlgorithm = S24Info.nBeaconMin = S24Info.nBeaconMax = 0;
	S24Info.n1MBit = S24Info.n2MBit = S24Info.n5MBit = S24Info.n11MBit = 0;
    S24Info.bAssociated = S24Info.bMicroAP = FALSE;
    S24Info.dwDriverVersion = 0;
    S24Info.nPercentMissed = 100;

		// initialize the Ping Setting once here
	TEXTCPY(S24IcmpInfo.szIPAddr,TEXT("127.0.0.1"));
	S24IcmpInfo.dwDataSize = 128;
	S24IcmpInfo.dwPingCount = 3;

}


// -------------------------------------------------------------
//  FUNCTION: GetS24Info
//
//  PROTOTYPE: BOOL GetS24Info(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Call S24 API to get the current S24 information
//				including: MU Mac Address, Firmware version
//				Driver version, Country code, and firmware type 
//
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL GetS24Info(void)
{
	// those thing that cannot change
    DWORD dwReturn;
    // Get the information that doesn't require association
    dwReturn = S24_GetMuMacAddress(S24Info.bMUAddress);
    if (dwReturn != RC_SUCCESS)
    {
        //ReportError(TEXT("GetMacAddress Error"),dwReturn);
        MyError(TEXT("Cannot access the"),TEXT("Spectrum 24 Card!"));
		return FALSE;
    }    

    dwReturn = S24_GetFirmwareVersion(S24Info.bFirmwareVersion);
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetFirmwareVersion Error"),dwReturn);
		return FALSE;
    }    
    
    dwReturn = S24_GetDriverVersion(&S24Info.dwDriverVersion); 
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetDriverVersion Error"),dwReturn);
		return FALSE;
    }

    dwReturn = S24_GetCountryTextAndCode(S24Info.bCountryText, &S24Info.nCountryCode); 
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetCountryTextandCode Error"),dwReturn);
		return FALSE;
    }
    
    if (S24_Is802())
        S24Info.nFirmwareType = S24_PROTOCOL_80211;
    else
        S24Info.nFirmwareType = S24_PROTOCOL_SPRING;

	return TRUE;
}


// -------------------------------------------------------------
//  FUNCTION: UpdateS24Info
//
//  PROTOTYPE: BOOL UpdateS24Info(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: 
//
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL UpdateS24Info(void)
{
    DWORD dwReturn;
    
    dwReturn = S24_GetESSID(S24Info.bS24ESSID);
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetESSID Error"),dwReturn);
		return FALSE;
    }    
    
	if (bDS)
	{
		dwReturn = S24_GetPowerIndex(&S24Info.nPowerIndex);
		if (dwReturn != RC_SUCCESS)
		{
			ReportError(TEXT("GetPowerIndex Error"),dwReturn);
			return FALSE;
		}
	}
	else
	{
		dwReturn = S24_GetPowerMode(&S24Info.nPowerMode); 
		if (dwReturn != RC_SUCCESS)
		{
			ReportError(TEXT("GetPowerMode Error"),dwReturn);
			return FALSE;
		}
	}

	dwReturn = S24_GetDiversity(&S24Info.nDiversity); 
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetDiversity Error"),dwReturn);
		return FALSE;
    }
    
	dwReturn = S24_GetBeaconParameters(&S24Info.nBeaconAlgorithm, 
                                   &S24Info.nBeaconMin,
                                   &S24Info.nBeaconMax); 
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetBeaconParamters Error"),dwReturn);
		return FALSE;
    }
    
    dwReturn = S24_GetSupportedRates (&S24Info.n1MBit, &S24Info.n2MBit,
									  &S24Info.n5MBit, &S24Info.n11MBit); 
    if (dwReturn != RC_SUCCESS)
    {
        ReportError(TEXT("GetSupportedRates Error"),dwReturn);
		return FALSE;
    }

    if (S24_IsMicroAP())
        S24Info.bMicroAP = TRUE;
    else
        S24Info.bMicroAP = FALSE;

    if (S24_IsAssociated())
    {
        // set the associated byte
        S24Info.bAssociated = TRUE;
        
		bAssociated = TRUE;		// use to turn on display of LinkSpeed and Quality

        dwReturn = S24_GetAPMacAddress(S24Info.bAPAddress);
        if (dwReturn != RC_SUCCESS)
        {
            ReportError(TEXT("GetAPAddress Error"),dwReturn);
			return FALSE;
        }
        
        dwReturn = S24_GetLinkSpeed(&S24Info.nLinkSpeed);
        if (dwReturn != RC_SUCCESS)
        {
            ReportError(TEXT("GetLinkSpeed Error"),dwReturn);
    		return FALSE;
	    }
        
        dwReturn = S24_GetChannelQualityValues(&S24Info.nPercentMissed,
                                           &S24Info.nTxRetries);
        if (dwReturn != RC_SUCCESS)
        {
            ReportError(TEXT("GetQualityValues Error"),dwReturn);
        	return FALSE;
		}
        
    }        
	else
	{
        S24Info.bAssociated = FALSE;

		bAssociated = FALSE;	// use to turn off display of LinkSpeed and Quality
	}

	g_bNeedUpdate = FALSE;
	
	return TRUE;
}    


// -------------------------------------------------------------
//  FUNCTION: Get_Current_Config
//
//  PROTOTYPE: void Get_Current_Config(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Save the current S24 configuration as Old settings
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void Get_Current_Config(void)
{
	// we need to save ESSID, nDiversity, nPowerMode, nDataRate, nBeaconAl
	memcpy(OldS24Setting.bS24ESSID,S24Info.bS24ESSID,sizeof(S24Info.bS24ESSID));

	OldS24Setting.nPowerMode = S24Info.nPowerMode;
	OldS24Setting.nPowerIndex = S24Info.nPowerIndex;
	OldS24Setting.nDiversity = S24Info.nDiversity;

	OldS24Setting.bMicroAP = S24Info.bMicroAP;

	OldS24Setting.nBeaconAlgorithm = S24Info.nBeaconAlgorithm;
	OldS24Setting.nBeaconMin = S24Info.nBeaconMin;
	OldS24Setting.nBeaconMax = S24Info.nBeaconMax;

	OldS24Setting.n1MBit = S24Info.n1MBit;
	OldS24Setting.n2MBit = S24Info.n2MBit;
	OldS24Setting.n5MBit = S24Info.n5MBit;
	OldS24Setting.n11MBit = S24Info.n11MBit;


}


// -------------------------------------------------------------
//  FUNCTION: Set_S24Config
//
//  PROTOTYPE: void Set_S24Config(void);
//
//  PARAMETERS:	None
//
//  DESCRIPTION: Set the s24 configuration only if they have changed
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void Set_S24Config(void)
{
	// I only want to update the one have been changed.
	// we need to save ESSID, nDiversity, nPowerMode, nDataRate, nBeaconAl
	if (memcmp(OldS24Setting.bS24ESSID,S24Info.bS24ESSID,sizeof(S24Info.bS24ESSID)) != 0)
	{
		// OK we need to set ESSID;
		if (!S24_SetESSID(S24Info.bS24ESSID))
			MyError(TEXT("Set_S24Config"),TEXT("Set ESSID Error"));
		else
			if (!Update_S24_Registry(S24_SET_ADAPTER_ESS_ID))
				MyError(TEXT("Update_S2_Registry"),TEXT("Set ESSID Error"));

	}

	if (bDS)
	{
		if (OldS24Setting.nPowerIndex != S24Info.nPowerIndex) 
		{
			if (!S24_SetPowerIndex(S24Info.nPowerIndex))
				MyError(TEXT("Set_S24Config"),TEXT("Set PowerIndex Error"));
			else
				if (!Update_S24_Registry(S24_SET_ADAPTER_POWER_MODE))
					MyError(TEXT("Update_S2_Registry"),TEXT("Set PowerIndex Error"));  	

		}

	}	
	else
	{
		if (OldS24Setting.nPowerMode != S24Info.nPowerMode)
		{
			if (!S24_SetPowerMode(S24Info.nPowerMode))
				MyError(TEXT("Set_S24Config"),TEXT("Set PowerMode Error"));
			else
				if (!Update_S24_Registry(S24_SET_ADAPTER_POWER_MODE))
					MyError(TEXT("Update_S2_Registry"),TEXT("Set PowerMode Error"));	
		}
	}

	if (OldS24Setting.nDiversity != S24Info.nDiversity)
	{
		if (!S24_SetDiversity(S24Info.nDiversity))
			MyError(TEXT("Set_S24Config"),TEXT("Set Diversity Error"));
		else
			if (!Update_S24_Registry(S24_SET_ANTENNA_DIVERSITY))
				MyError(TEXT("Update_S2_Registry"),TEXT("Set Diversity Error"));
	}

	if (bFuncMode)	// only if we support it
	{
	if (OldS24Setting.bMicroAP != S24Info.bMicroAP)
	{
		WORD nMode;

		if (S24Info.bMicroAP)
			nMode = S24_FUNCTIONAL_MODE_MAP;
		else
			nMode = S24_FUNCTIONAL_MODE_MU;	

		if (!S24_SetFunctionMode(nMode))
			MyError(TEXT("Set_S24Config"),TEXT("Set FunctionMode Error"));
		else
			if (!Update_S24_Registry(S24_SET_FUNCTIONAL_MODE))
				MyError(TEXT("Update_S2_Registry"),TEXT("Set Functional Error"));

	}
	}

	if (OldS24Setting.nBeaconAlgorithm != S24Info.nBeaconAlgorithm)
	{
		if (!S24_SetBeaconParameters(S24Info.nBeaconAlgorithm,
									 S24Info.nBeaconMin,
									 S24Info.nBeaconMax))
			MyError(TEXT("Set_S24Config"),TEXT("Set Beacon Error"));
		else
			if (!Update_S24_Registry(S24_SET_BEACON_PARAMETERS))
				MyError(TEXT("Update_S2_Registry"),TEXT("Set BeaconAlgorithm Error"));

	}

	if ( (OldS24Setting.n1MBit != S24Info.n1MBit)
		||(OldS24Setting.n2MBit != S24Info.n2MBit) 
		||(OldS24Setting.n5MBit != S24Info.n5MBit)
		||(OldS24Setting.n11MBit != S24Info.n11MBit) )
	{
		if (!S24_SetSupportedDataRates (S24Info.n1MBit, S24Info.n2MBit,
										S24Info.n5MBit, S24Info.n11MBit))
			MyError(TEXT("Set_S24Config"),TEXT("Set Supported Rate Error"));

		if (!Update_S24_Registry(S24_SET_SUPPORTED_DATA_RATES))
			MyError(TEXT("Update_S24_Registry"),TEXT("Set Supported DataRates Error"));

	}

}


// -------------------------------------------------------------
//  FUNCTION: InitS24Cfg
//
//  PROTOTYPE: BOOL InitS24Cfg(void);
//
//  PARAMETERS:	None
//
//  DESCRIPTION: Initialize the S24 driver
//
//  RETURNS: TRUE if success; FALSE otherwise
//
//  NOTES:
// -------------------------------------------------------------
BOOL InitS24Cfg(void)
{
	DWORD dwRet;

	if (!S24_InitS24Driver(hLib))
	{
		MyError(TEXT("Failed to initialize S24 driver"),NULL);
		S24_FreeS24Driver(hLib);
		SetCursor(NULL);
		return FALSE;
	}

	if (!S24_ResearchAdapters())
	{
		MyError(TEXT("S24_ResearchAdapters Failed"),NULL);
		S24_FreeS24Driver(hLib);
		SetCursor(NULL);
		return FALSE;
	}

	// use this API to get the MU MAC Address!!!
	dwRet = S24_GetAdapterList();

	if (dwRet != RC_SUCCESS)
	{
		ReportError(TEXT("S24_GetAdapterList Failed"),dwRet);
		S24_FreeS24Driver(hLib);
		SetCursor(NULL);
		return FALSE;
	}
	
	return TRUE;

}


// -------------------------------------------------------------
//  FUNCTION: EnterConfigS24
//
//  PROTOTYPE: void EnterConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for S24 Configuration menu
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void EnterConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	LPTSTR lpsz = NULL;
	TCHAR szLinkage[MAX_PATH64];

	InitCtlPanel(lpis,dwSelection,IDS_CONFIGS24);

	// initialize S24Info structure;
	S24InfoInit();

	hLib = S24_LoadS24Driver();
	if (hLib == NULL)
	{
		MyError(TEXT("Failed to Load S24mudll"),NULL);
		return;
	}

	SetCursor(LoadCursor(NULL, IDC_WAIT));

	if (!InitS24Cfg())
	{
		return;
	}

	SetCursor(NULL);

	// default to S24
	lpsz = szLinkage;
	lstrcpy(lpsz,SLACE1);

	FoundRFNetWork(lpsz);

	SetRFRegBase(lpsz);

	bTemp = l_bUseWavFile;
	l_bUseWavFile = FALSE;
	l_bUseS24 = TRUE;

	// find out DS or FH
	bDS = S24_IsDS();
	bFH = !bDS;

	// we loaded the S24 driver, now let's update the S24Info    
	if (!GetS24Info())
	{
		S24_FreeS24Driver(hLib);
		return;
	}
	
	if (!UpdateS24Info())
	{
		S24_FreeS24Driver(hLib);
		return;
	}

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ConfigS24Selector);
}


// -------------------------------------------------------------
//  FUNCTION: ExitConfigS24
//
//  PROTOTYPE: void ExitConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for S24 Configuration menu
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void ExitConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	

	S24_FreeS24Driver(hLib);

	l_bUseWavFile = bTemp;
	l_bUseS24 = FALSE;
	l_bDialogWasOKed = FALSE;
	l_lpExitFunction = NULL;

}


// -------------------------------------------------------------
//  FUNCTION: EnterViewConfig
//
//  PROTOTYPE: void EnterViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the View Config
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void EnterViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_VIEWCONFIG);

	if (g_bNeedUpdate)
		UpdateS24Info();

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)ViewConfigSelector);


}


// -------------------------------------------------------------
//  FUNCTION: ExitViewConfig
//
//  PROTOTYPE: void ExitViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the View Config
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void ExitViewConfig(LPITEMSELECTOR lpis,DWORD dwSelection)
{

	l_bDialogWasOKed = FALSE;

	l_lpExitFunction = ExitConfigS24;
}


// -------------------------------------------------------------
//  FUNCTION: EnterSetS24
//
//  PROTOTYPE: void EnterSetS24(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the Setting S24 parameter
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void EnterSetS24(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_SETS24);

	g_bS24IDOKed = FALSE;
	
	if (g_bNeedUpdate)
		UpdateS24Info();

	// get and save the current config
	Get_Current_Config();

    g_hdlg = CreateChosenDialog(g_hInstance,
	    						CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)SetS24Selector);


}


// -------------------------------------------------------------
//  FUNCTION: ExitSetS24
//
//  PROTOTYPE: void ExitSetS24(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the Setting S24 parameter
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void ExitSetS24(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{
		g_bS24IDOKed = TRUE;
		Set_S24Config();
		g_bNeedUpdate = TRUE;	// we just change something update it
	}

    l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitConfigS24;

}


// -------------------------------------------------------------
//  FUNCTION: EnterS24Signal
//
//  PROTOTYPE: void EnterS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 signal strength
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void EnterS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_S24SIGNAL);

	if (g_bNeedUpdate)
		UpdateS24Info();

	g_hdlg = CreateChosenDialog(g_hInstance,
								S24SignalDialogChoices,
								g_hwnd,
								S24SignalProc,
								(LPARAM)0);


}


// -------------------------------------------------------------
//  FUNCTION: ExitS24Signal
//
//  PROTOTYPE: void ExitS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 signal strength
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void ExitS24Signal(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{
	}

    
	l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitConfigS24;

}


// -------------------------------------------------------------
//  FUNCTION: EnterS24PingTest
//
//  PROTOTYPE: void EnterS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 Ping Test
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void EnterS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_S24PINGTEST);

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)S24PingTestSelector);

}


// -------------------------------------------------------------
//  FUNCTION: ExitS24PingTest
//
//  PROTOTYPE: void ExitS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 Ping Test
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void ExitS24PingTest(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{

	}

    l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitConfigS24;

}


// -------------------------------------------------------------
//  FUNCTION: EnterWNMPPing
//
//  PROTOTYPE: void EnterWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 Send WNMP ping 
//
//  RETURNS: None 
//
//  NOTES: !!!This function only work with the local adapter!!!
// -------------------------------------------------------------
void EnterWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_S24PINGTEST);

	// setup a unique system message

	g_hdlg = CreateChosenDialog(g_hInstance,
								S24PingDialogChoices,
								g_hwnd,
								S24PingProc,
								(LPARAM)0);
}


// -------------------------------------------------------------
//  FUNCTION: ExitWNMPPing
//
//  PROTOTYPE: void ExitWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 Send WNMP ping 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void ExitWNMPPing(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{
		// TODO: Add code to process ACCEPT
	}

    
	l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitS24PingTest;

}


// -------------------------------------------------------------
//  FUNCTION: EnterICMPPing
//
//  PROTOTYPE: void EnterICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 Send ICMP ping 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void EnterICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_S24PINGTEST);

	g_hdlg = CreateChosenDialog(g_hInstance,
								S24PingDialogChoices,
								g_hwnd,
								S24PingProc,
								(LPARAM)1);
}


// -------------------------------------------------------------
//  FUNCTION: ExitICMPPing
//
//  PROTOTYPE: void ExitICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 Send ICMP ping 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void ExitICMPPing(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{
		// TODO: Add code to process ACCEPT ecit
	}

    l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitS24PingTest;
}


// -------------------------------------------------------------
//  FUNCTION: EnterPingSetting
//
//  PROTOTYPE: void EnterPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 Ping setting 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void EnterPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	InitCtlPanel(lpis,dwSelection,IDS_S24PINGTEST);

	// make a copy of the S24IcmpInfo
	OldS24IcmpInfo = S24IcmpInfo;

	g_hdlg = CreateChosenDialog(g_hInstance,
								CtlPanelDialogChoices,
								g_hwnd,
								CtlPanelDlgProc,
								(LPARAM)S24PingSettingSelector);
}


// -------------------------------------------------------------
//  FUNCTION: ExitPingSetting
//
//  PROTOTYPE: void ExitPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 ping setting
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void ExitPingSetting(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if (l_bDialogWasOKed)
	{
		// TODO: Add code to process ACCEPT exit
		// update the info
		OldS24IcmpInfo = S24IcmpInfo;
	}
	else
    {
		S24IcmpInfo = OldS24IcmpInfo;

	}

	l_bDialogWasOKed = FALSE;   
	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitS24PingTest;

}


// -------------------------------------------------------------
//  FUNCTION: EnterS24Network
//
//  PROTOTYPE: void EnterS24Network(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Entry point for the S24 Network setting 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void EnterS24Network(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	SetCursor(LoadCursor(NULL,IDC_WAIT));

	InitCtlPanel(lpis,dwSelection,IDS_S24NETWORK);

	// initialize S24 Network info
	GetCurrentS24NetworkConfig(&S24NetworkInfo);

	// tell the program not to display IP address and subnetmask setting
	if (S24NetworkInfo.dwDHCPEnable)
	{
		g_bNoDHCP = FALSE;
	}
	else
	{
		g_bNoDHCP = TRUE;
		IpClear();
	}

	g_hdlg = CreateChosenDialog(g_hInstance,
	    						CtlPanelDialogChoices,
			    				g_hwnd,
				    			CtlPanelDlgProc,
					    		(LPARAM)S24NetworkSelector);

	SetCursor(NULL);
}


// -------------------------------------------------------------
//  FUNCTION: S24MergeAddress
//
//  PROTOTYPE:DWORD S24MergeAddress(LPTSTR lpszResult, 
//					LPTSTR lpszAdd1, LPTSTR lpszAdd2);
//
//  PARAMETERS:	lpszResult: pointer to the result multi_sz string
//				lpszAdd1: pointer to the 1st string
//				lpszAdd2: pointer to the 2nd string
//
//  DESCRIPTION: this function will merge two strings into one single 
//				 multi string to be used in setting the MULTI_SZ registry value.
//
//  RETURNS: total length of the multi_sz string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD S24MergeAddress(LPTSTR lpszResult, LPTSTR lpszAdd1, LPTSTR lpszAdd2)
{
	DWORD dwIndex=0, dwTextLen=0;

	if ( (lpszResult == NULL)
	   ||(lpszAdd1 == NULL)
	   ||(lpszAdd2 == NULL))
	{
		return 0;
	}

	dwTextLen = TEXTLEN(lpszAdd1) + 1;

	TEXTCPY(lpszResult,lpszAdd1);
	dwIndex += dwTextLen;
	// advance pass the first null terminated character
	dwTextLen = TEXTLEN(lpszAdd2) + 1;

	TEXTCPY(lpszResult + dwIndex, lpszAdd2);

	dwIndex += dwTextLen;
	lpszResult[dwIndex] = 0;

	return dwIndex;

}


static void UpdateDHCPWins(LPTSTR lpszVal,
						   DWORD dwValLen)
{
	TCHAR szPath[MAX_PATH];
	DWORD dwRetCode;
	HKEY hKey;
	DWORD dwType;
	DWORD dwWINS;
	DWORD dwLen;

	// If DHCP is not enabled
	if ( !S24NetworkInfo.dwDHCPEnable )
	{
		// Don't write the DhcpWins key
		return;
	}

	// Build path to DhcpOptions key
    TEXTCPY(szPath,szTcpIpRegBase);
	TEXTCAT(szPath,TEXT("\\DhcpOptions"));

	// Open the key
	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							 szPath,
							 0,
							 KEY_ALL_ACCESS,
							 &hKey);
    
	// If we can open the key
	if (dwRetCode == ERROR_SUCCESS)
	{
		// Try and get the value of WINS (44)
		dwRetCode = RegQueryValueEx(hKey,
									TEXT("44"),
									NULL,
									&dwType,
									(LPBYTE)&dwWINS,
									&dwLen);

		// If we can get the value
		if (dwRetCode == ERROR_SUCCESS)
		{
			// Don't write the DhcpWins key
			return;
		}

		// Write the DhcpWins key
		SetS24IP(DHCPWINS,lpszVal,dwValLen);
	} 
}


// -------------------------------------------------------------
//  FUNCTION: ExitS24Network
//
//  PROTOTYPE: void ExitS24Network(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Exit point for the S24 Network setting 
//
//  RETURNS: None 
//
//  NOTES: 
// -------------------------------------------------------------
void ExitS24Network(LPITEMSELECTOR lpis,DWORD dwSelection)
{

	if (l_bDialogWasOKed)
	{
		DWORD dwLen;
		LPTSTR lpszVal = NULL;
	    
		lpszVal = (LPTSTR) LocalAlloc(0, MAX_PATH*sizeof(TCHAR));

		if (S24NetworkInfo.dwDHCPEnable)
		{
			SetS24DHCP(TRUE);
		}
		else
		{
			// disable DHCP, got to set IP and Subnetmask
			SetS24DHCP(FALSE);
			// setup IP and subnet mask
			dwLen = (TEXTLEN(S24NetworkInfo.szIPAddr)+1)*sizeof(TCHAR);

			SetS24IP(IPADDRESS, S24NetworkInfo.szIPAddr, dwLen);	// size = 8 (with null char)
			
			dwLen = (TEXTLEN(S24NetworkInfo.szNetMask)+1)*sizeof(TCHAR);
			SetS24IP(SUBNETMASK,S24NetworkInfo.szNetMask, dwLen);
		}

		// we need to set both Primary and Secondary DNS
		dwLen = S24MergeAddress(lpszVal, S24NetworkInfo.szDNS[0],
							S24NetworkInfo.szDNS[1]);
		if (dwLen == 0)
			MyError(TEXT("Error SetDNS"),NULL);
		else
		{
			dwLen = (dwLen + 1) * sizeof(TCHAR);
			SetS24IP(DNS,lpszVal,dwLen);
		}

		dwLen = S24MergeAddress(lpszVal, S24NetworkInfo.szWINS[0],
							S24NetworkInfo.szWINS[1]);
		if (dwLen == 0)
			MyError(TEXT("Error SetWINS"),NULL);
		else
		{
			dwLen = (dwLen + 1) * sizeof(TCHAR);
			SetS24IP(WINS,lpszVal,dwLen);

			UpdateDHCPWins(lpszVal,dwLen);
		}


		dwLen = S24MergeAddress(lpszVal, S24NetworkInfo.szGateway[0],
							S24NetworkInfo.szGateway[1]);
		if (dwLen == 0)
			MyError(TEXT("Error SetGateway"),NULL);
		else
		{
			dwLen = (dwLen + 1) * sizeof(TCHAR);
			SetS24IP(GATEWAY,lpszVal,dwLen);
		}
	}

    l_bDialogWasOKed = FALSE;   

	// need to set the next function to NULL, otherwise, this exit function
	// will be called when exit control panel
	l_lpExitFunction = ExitConfigS24;


}


// -------------------------------------------------------------
//  FUNCTION: ShowDriverVersion
//
//  PROTOTYPE:LPTSTR ShowDriverVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the version string of the S24 Driver (MUDLL)
//
//  RETURNS: The pointer to the version string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowDriverVersion(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];
	
//	ReportError(TEXT("DriverVersion"),S24Info.dwDriverVersion);

	wsprintf(szMsg,TEXT("%02d.%02d"),HIWORD(S24Info.dwDriverVersion), 
			 LOWORD(S24Info.dwDriverVersion));

	lpsz = szMsg;    

	return lpsz;

}


// -------------------------------------------------------------
//  FUNCTION: ShowFirmwareVersion
//
//  PROTOTYPE:LPTSTR ShowFirmwareVersion(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the version string of the S24 firmware
//
//  RETURNS: The pointer to the version string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowFirmwareVersion(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH32];

// since the driver return ASCII char, we need to convert them to Unicode
#ifdef UNICODE
    MultiByteToWideChar(	CP_OEMCP, MB_PRECOMPOSED,
							S24Info.bFirmwareVersion,
							sizeof(S24Info.bFirmwareVersion),
							szMsg,
							countof(szMsg));
#else
    TEXTCPY(szMsg,S24Info.bFirmwareVersion);
#endif

	lpsz = szMsg;    

    return lpsz;

}


// -------------------------------------------------------------
//  FUNCTION: ShowMUAddress
//
//  PROTOTYPE:LPTSTR ShowMUAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the Mu Address of the S24 
//
//  RETURNS: The pointer to the MU Address 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowMUAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH32];
	BYTE    bIEEE[6];

	memcpy(&bIEEE,S24Info.bMUAddress,6);

	wsprintf(szMsg,TEXT("%02X%02X%02X%02X%02X%02X%"),bIEEE[0],bIEEE[1],
			   bIEEE[2],bIEEE[3],bIEEE[4],bIEEE[5]);

	lpsz = szMsg;    

	return lpsz;

}


// -------------------------------------------------------------
//  FUNCTION: ShowESSID
//
//  PROTOTYPE:LPTSTR ShowESSID(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 ESSID
//
//  RETURNS: The pointer to the ESSID string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowESSID(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH64];

#ifdef UNICODE
    MultiByteToWideChar(	CP_OEMCP, MB_PRECOMPOSED,
							S24Info.bS24ESSID,
							sizeof(S24Info.bS24ESSID),
							szMsg,
							countof(szMsg));
#else
    TEXTCPY(szMsg,S24Info.bS24ESSID);
#endif
    lpsz = szMsg;    

    return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: ShowPowerMode
//
//  PROTOTYPE:LPTSTR ShowPowerMode(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the Power mode of the S24 
//
//  RETURNS: The pointer to the Power mode string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowPowerMode(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];

	if (S24Info.nPowerMode)
		TEXTCPY(szMsg,TEXT("PSP"));
	else
		TEXTCPY(szMsg,TEXT("CAM"));

	lpsz = szMsg;    

	return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: ShowDiversity
//
//  PROTOTYPE:LPTSTR ShowDiversity(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Antenna diversity
//
//  RETURNS: The pointer to the diversity string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowDiversity(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];

	if (S24Info.nDiversity)
		TEXTCPY(szMsg,TEXT("Yes"));
	else
		TEXTCPY(szMsg,TEXT("No"));

    lpsz = szMsg;    

    return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: GetRateControl
//
//  PROTOTYPE:DWORD GetRateControl(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: get the supported data rates 
//
//  RETURNS: Index to the supported data rate string
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetRateControl(LPITEMSELECTOR lpis)
{
	DWORD dwRateMask = 0;

	// if mandatory, then return madatory
	if (S24Info.n1MBit == S24_DATA_RATE_MANDATORY)
		return ONEMBIT;

	if (S24Info.n2MBit == S24_DATA_RATE_MANDATORY)
		return TWOMBIT;

	if (S24Info.n5MBit == S24_DATA_RATE_MANDATORY)
		return FIVEMBIT;

	if (S24Info.n11MBit == S24_DATA_RATE_MANDATORY)
		return ELEVENMBIT;

	// use mask to indicate which baud rate is supported
	// 0000 0000	= > Data Rate Not supported
	// 0000 0001	= > 1 Mbit
	// 0000 0010
	// 0000 0011
	// 0000 0100
	// 0000 0101
	// 0000 0110
	// 0000 0111

	// 0000 1000
	// 0000 1001
	// 0000 1010
	// 0000 1011
	// 0000 1100
	// 0000 1101
	// 0000 1110
	// 0000 1111
	
	dwRateMask = S24Info.n11MBit << 3 |	S24Info.n5MBit << 2 | S24Info.n2MBit << 1 |	S24Info.n1MBit;

	// clear the hiword and hi nibble
	dwRateMask = dwRateMask & 0x000F;

	return (dwRateMask);

}


// -------------------------------------------------------------
//  FUNCTION: ShowDataRate
//
//  PROTOTYPE:LPTSTR ShowDataRate(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 data rate
//
//  RETURNS: The pointer to the data rate string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowDataRate(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    dwIndex = GetRateControl(lpis);
    return (lpszValuesRateControl[dwIndex]);
}


// -------------------------------------------------------------
//  FUNCTION: ShowBeaconAlgorithm
//
//  PROTOTYPE:LPTSTR ShowBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Beacon Algorithm
//
//  RETURNS: The pointer to the Beacon Algorithm string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];

	TEXTSPRINTF(szMsg,TEXT("PSP %d"),S24Info.nBeaconAlgorithm);

	lpsz = szMsg;    

	return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: ShowPowerIndex
//
//  PROTOTYPE:LPTSTR ShowPowerIndex(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Power Index
//
//  RETURNS: The pointer to the Power Index string 
//
//  NOTES: This is for Trilogy driver (or DS) only
// -------------------------------------------------------------
LPTSTR ShowPowerIndex(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];

	TEXTSPRINTF(szMsg,TEXT("PI %d"),S24Info.nPowerIndex);

    lpsz = szMsg;    

    return lpsz;
}


LPTSTR ShowFunctionMode(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	return(lpszFunctionMode[S24Info.bMicroAP]);

}

// -------------------------------------------------------------
//  FUNCTION: ShowCountryText
//
//  PROTOTYPE:LPTSTR ShowCountryText(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Country text
//
//  RETURNS: The pointer to the Country text string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowCountryText(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH128];

#ifdef UNICODE
    MultiByteToWideChar(	CP_OEMCP, MB_PRECOMPOSED,
							S24Info.bCountryText,
							sizeof(S24Info.bCountryText),
							szMsg,
							countof(szMsg));
#else
    TEXTCPY(szMsg,S24Info.bCountryText);
#endif

    lpsz = szMsg;    

    return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: ShowAdapterType
//
//  PROTOTYPE:LPTSTR ShowAdapterType(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Adapter type string
//
//  RETURNS: The pointer to the Adapter type string
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowAdapterType(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    return(lpszAdapterType[S24Info.nAdapterType]);
}


// -------------------------------------------------------------
//  FUNCTION: ShowFirmwareType
//
//  PROTOTYPE:LPTSTR ShowFirmwareType(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Firmware type string
//
//  RETURNS: The pointer to the Firmware type string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowFirmwareType(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    return(lpszFirmwareType[S24Info.nFirmwareType]);
}


// -------------------------------------------------------------
//  FUNCTION: ShowAssociation
//
//  PROTOTYPE:LPTSTR ShowAssociation(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 association status
//
//  RETURNS: The pointer to the association status string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowAssociation(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	return(lpszAssociated[S24Info.bAssociated]);
}


// -------------------------------------------------------------
//  FUNCTION: ShowLinkSpeed
//
//  PROTOTYPE:LPTSTR ShowLinkSpeed(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 Link speed
//
//  RETURNS: The pointer to the Link speed string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowLinkSpeed(LPITEMSELECTOR lpis,DWORD dwIndex)
{
    LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH32];

	wsprintf(szMsg,TEXT("%d Mbps"),S24Info.nLinkSpeed);

	lpsz = szMsg;    

	return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: ShowQuality
//
//  PROTOTYPE:LPTSTR ShowQuality(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Show the S24 quality string
//
//  RETURNS: The pointer to the quality string 
//
//  NOTES:
// -------------------------------------------------------------
LPTSTR ShowQuality(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	LPTSTR lpsz = NULL;
	static	TCHAR szMsg[MAX_PATH8];

	wsprintf(szMsg,TEXT("%d %%"),(100-S24Info.nPercentMissed));
    lpsz = szMsg;    

    return lpsz;
}


// -------------------------------------------------------------
//  FUNCTION: GetESSID
//
//  PROTOTYPE:DWORD GetESSID(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the S24 ESSID string
//
//  RETURNS: 0 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetESSID(LPITEMSELECTOR lpis)
{

#ifdef UNICODE
    MultiByteToWideChar(	CP_OEMCP, MB_PRECOMPOSED,
							S24Info.bS24ESSID,
							sizeof(S24Info.bS24ESSID),
							lpis->szText,
							countof(lpis->szText));
#else
    TEXTCPY(lpis->szText,S24Info.bS24ESSID);
#endif

    return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetESSID
//
//  PROTOTYPE:void SetESSID(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the S24 ESSID
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the ESSID yet, the
//			ID will be set when exit the menu
// -------------------------------------------------------------
void SetESSID(LPITEMSELECTOR lpis,DWORD dwSelection)
{

#ifdef UNICODE
	// lpis->szText has the ESSID string in UNICODE
	// We need to convert it to the ansi
	wcstombs(S24Info.bS24ESSID,lpis->szText,32);
#else
	TEXTNCPY(S24Info.bS24ESSID,lpis->szText,32);
#endif

}


// -------------------------------------------------------------
//  FUNCTION: SetDiversity
//
//  PROTOTYPE:void SetDiversity(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the diversity
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the actual value yet
//			actual value will be set when exit the menu
// -------------------------------------------------------------
void SetDiversity(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24Info.nDiversity = (WORD)dwSelection;

}


// -------------------------------------------------------------
//  FUNCTION: GetDiversity
//
//  PROTOTYPE:DWORD GetDiversity(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the current diversity string
//
//  RETURNS: The index to the diversity string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetDiversity(LPITEMSELECTOR lpis)
{
    return (S24Info.nDiversity);
}


// -------------------------------------------------------------
//  FUNCTION: SetPowerMode
//
//  PROTOTYPE:void SetPowerMode(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the power mode
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the actual value yet
//			actual value will be set when exit the menu
// -------------------------------------------------------------
void SetPowerMode(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24Info.nPowerMode = (WORD)dwSelection;
}


// -------------------------------------------------------------
//  FUNCTION: GetPowerMode
//
//  PROTOTYPE:DWORD GetPowerMode(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the current power mode string
//
//  RETURNS: The index to the Power mode string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetPowerMode(LPITEMSELECTOR lpis)
{
	// 0 for CAM 1 for PSP
    return (S24Info.nPowerMode);
}


// -------------------------------------------------------------
//  FUNCTION: SetBeaconAlgorithm
//
//  PROTOTYPE:void SetBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the Beacon Algorithm
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the actual value yet
//			actual value will be set when exit the menu
// -------------------------------------------------------------
void SetBeaconAlgorithm(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24Info.nBeaconAlgorithm = (WORD)dwSelection + 1;
}


// -------------------------------------------------------------
//  FUNCTION: GetBeaconAlgorithm
//
//  PROTOTYPE:DWORD GetBeaconAlgorithm(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the current Beacon Algorithm string
//
//  RETURNS: The index to the Beacon Algorithm string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetBeaconAlgorithm(LPITEMSELECTOR lpis)
{
    return ((S24Info.nBeaconAlgorithm - 1));
}


// -------------------------------------------------------------
//  FUNCTION: SetPowerIndex
//
//  PROTOTYPE:void SetPowerIndex(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the Power Index
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the actual value yet
//			actual value will be set when exit the menu
//			! This function only apply to Trilogy driver !
// -------------------------------------------------------------
void SetPowerIndex(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	S24Info.nPowerIndex = (WORD)dwSelection;
}


// -------------------------------------------------------------
//  FUNCTION: GetPowerIndex
//
//  PROTOTYPE:DWORD GetPowerIndex(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the current Power Index string
//
//  RETURNS: The index to the PowerIndex string 
//
//  NOTES: ! This function only apply to Trilogy driver !
// -------------------------------------------------------------
DWORD GetPowerIndex(LPITEMSELECTOR lpis)
{
	return (S24Info.nPowerIndex);
}


// -------------------------------------------------------------
//  FUNCTION: SetRateControl
//
//  PROTOTYPE:void SetRateControl(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the Rate Control
//
//  RETURNS: None
//
//  NOTES: This function won't actually set the actual value yet
//			actual value will be set when exit the menu
// -------------------------------------------------------------
void SetRateControl(LPITEMSELECTOR lpis,DWORD dwSelection)
{
// 0000 xxxx

	// clear the datarate first
	S24Info.n11MBit = S24Info.n5MBit = S24Info.n2MBit = S24Info.n1MBit = 0;

	// now set the new datarate setting
	if (dwSelection & 1)	// 1Mbit
		S24Info.n1MBit = S24_DATA_RATE_SUPPORTED;

	if (dwSelection & 2)	// 2Mbit
		S24Info.n2MBit = S24_DATA_RATE_SUPPORTED;

	if (dwSelection & 4)	// 5Mbit
		S24Info.n5MBit = S24_DATA_RATE_SUPPORTED;
	
	if (dwSelection & 8)	// 11Mbit
		S24Info.n11MBit = S24_DATA_RATE_SUPPORTED;

#if 0
	// check for mandatory, we should have only one manadatory
	switch (dwSelection)
	{
	case ONEMBIT:
		S24Info.n1MBit = S24_DATA_RATE_MANDATORY;
		break;
	case TWOMBIT:
		S24Info.n2MBit = S24_DATA_RATE_MANDATORY;
		break;
	case FIVEMBIT:
		S24Info.n5MBit = S24_DATA_RATE_MANDATORY;
		break;
	case ELEVENMBIT:
		S24Info.n11MBit = S24_DATA_RATE_MANDATORY;
		break;
	default:
		break;
	}
#endif

}


// -------------------------------------------------------------
//  FUNCTION: SetFunctionMode
//
//  PROTOTYPE:void SetFunctionMode(LPITEMSELECTOR lpis,DWORD dwSelection);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwSelection: index on the list
//
//  DESCRIPTION: Set the Function Mode (MU or AP)
//
//  RETURNS: None 
//
//  NOTES: !Not supported!
// -------------------------------------------------------------
void SetFunctionMode(LPITEMSELECTOR lpis,DWORD dwSelection)
{
    if (dwSelection)
        S24Info.bMicroAP = TRUE;
    else
        S24Info.bMicroAP = FALSE;
}


// -------------------------------------------------------------
//  FUNCTION: GetFunctionMode
//
//  PROTOTYPE:DWORD GetFunctionMode(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the function mode setting
//
//  RETURNS: The index to the function mode string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetFunctionMode(LPITEMSELECTOR lpis)
{
	return ((DWORD)S24Info.bMicroAP);
}


// S24 Network Functions

// -------------------------------------------------------------
//  FUNCTION: GetDHCP
//
//  PROTOTYPE:DWORD GetDHCP(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the DHCP setting (enabled or disabled)
//
//  RETURNS: The index to the DHCP string 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetDHCP(LPITEMSELECTOR lpis)
{
    return S24NetworkInfo.dwDHCPEnable;
}


// -------------------------------------------------------------
//  FUNCTION: SetDHCP
//
//  PROTOTYPE:void SetDHCP(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the S24 DHCP setting
//
//  RETURNS: None
//
//  NOTES:
// -------------------------------------------------------------
void SetDHCP(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	S24NetworkInfo.dwDHCPEnable = dwIndex;
}


// -------------------------------------------------------------
//  FUNCTION: ValidateIPAddress
//
//  PROTOTYPE:BOOL ValidateIPAddress(LPTSTR lpszIPAddr);
//
//  PARAMETERS: lpszIPAddr: pointer to the ip address
//
//  DESCRIPTION: Check if we got valid IP address
//
//  RETURNS: TRUE if yes, FALSE otherwise
//
//  NOTES: Use winsock API to check it
// -------------------------------------------------------------
BOOL ValidateIPAddress(LPTSTR lpszIPAddr)
{
	CHAR szAdd[MAX_PATH64];
	DWORD	dwRetCode = 0;

	if (lpszIPAddr == NULL)
		return FALSE;

#ifdef UNICODE
    wcstombs(szAdd,lpszIPAddr,31);
#else
	TEXTCPY(szAdd,lpszIPAddr);
#endif

    if ((dwRetCode = inet_addr(szAdd)) == INADDR_NONE)
    {
        return FALSE;
    }
	else
	{
		return TRUE;
	}

}


//	6 - Gateway, 7 - Secondary
// -------------------------------------------------------------
//  FUNCTION: SetS24NetworkInfo
//
//  PROTOTYPE:void SetS24NetworkInfo(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS:
// 		lpszVal -> value to set; dwIndex, what to set
// 		dwIndex, 0 - IP, 1 - Netmask, 2 - PDNS, 3 - SDNS, 4 - PWINS, 5 - SWINS
//
//  DESCRIPTION: Set the S24 related network/TCPIP settings including:
//				IP, subnetmask, DNS, WINS, and Gateway address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
BOOL SetS24NetworkInfo(LPTSTR lpszVal, DWORD dwIndex)
{
	if (lpszVal == NULL)
		return FALSE;

	if (!ValidateIPAddress(lpszVal))
        return FALSE;
     
	switch (dwIndex)
	{
	case 0:	// IP
		TEXTCPY(S24NetworkInfo.szIPAddr,lpszVal);
		break;
	case 1:	// netmask
		TEXTCPY(S24NetworkInfo.szNetMask,lpszVal);
		break;
	case 2:	// PDNS
		TEXTCPY(S24NetworkInfo.szDNS[0],lpszVal);
		break;
	case 3:	// SDNS
		TEXTCPY(S24NetworkInfo.szDNS[1],lpszVal);
		break;
	case 4:	// PWINS
		TEXTCPY(S24NetworkInfo.szWINS[0],lpszVal);
		break;
	case 5:	// SWINS
		TEXTCPY(S24NetworkInfo.szWINS[1],lpszVal);
		break;
	case 6:	// GATEWAY
		TEXTCPY(S24NetworkInfo.szGateway[0],lpszVal);
		break;
	case 7:	// SGATEWAY
		TEXTCPY(S24NetworkInfo.szGateway[1],lpszVal);
		break;
	default:
		return FALSE;
	}
	return TRUE;
}


// -------------------------------------------------------------
//  FUNCTION: GetSubnetMask
//
//  PROTOTYPE:DWORD GetSubnetMask(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Get the MU SubnetMask
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetSubnetMask(LPITEMSELECTOR lpis)
{
    // update the display
    TEXTCPY(lpis->szText,S24NetworkInfo.szNetMask);

    return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetSubnetMask
//
//  PROTOTYPE:void SetSubnetMask(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the MU SubnetMask
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetSubnetMask(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,1))
	{
        MyError(TEXT("Invalid SubnetMask"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szNetMask);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
        return;
    }
      
}


// -------------------------------------------------------------
//  FUNCTION: GetMUIPAddress
//
//  PROTOTYPE:void GetMUIPAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the MU IP address
//
//  RETURNS: always 0 
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetMUIPAddress(LPITEMSELECTOR lpis)
{
    TEXTCPY(lpis->szText,S24NetworkInfo.szIPAddr);
    return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetMUIPAddress
//
//  PROTOTYPE:void SetMUIPAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the MU IP address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetMUIPAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,0))
    {
        MyError(TEXT("Invalid IP Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szIPAddr);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
        return;
    }

}


// -------------------------------------------------------------
//  FUNCTION: GetPDNSAddress
//
//  PROTOTYPE:DWORD GetPDNSAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Primary DNS address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetPDNSAddress(LPITEMSELECTOR lpis)
{
   TEXTCPY(lpis->szText,S24NetworkInfo.szDNS[0]);
	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetPWINSAddress
//
//  PROTOTYPE:void SetPWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the primary DNS address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetPDNSAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,2))
	{
		MyError(TEXT("DNS Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szDNS[0]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}
	
}


// -------------------------------------------------------------
//  FUNCTION: GetSDNSAddress
//
//  PROTOTYPE:DWORD GetSWINSAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Secondary DNS address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetSDNSAddress(LPITEMSELECTOR lpis)
{
   TEXTCPY(lpis->szText,S24NetworkInfo.szDNS[1]);
	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetSDNSAddress
//
//  PROTOTYPE:void SetSDNSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the Secondary DNS address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetSDNSAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,3))
	{
		MyError(TEXT("DNS Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szDNS[1]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}
	
}


// -------------------------------------------------------------
//  FUNCTION: GetPWINSAddress
//
//  PROTOTYPE:DWORD GetPWINSAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Primary WINS address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetPWINSAddress(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,S24NetworkInfo.szWINS[0]);

	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetSWINSAddress
//
//  PROTOTYPE:void SetSWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the primary WINS address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetPWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,4))
	{
		MyError(TEXT("Invalid WINS Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szWINS[0]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}

}


// -------------------------------------------------------------
//  FUNCTION: GetSWINSAddress
//
//  PROTOTYPE:DWORD GetSWINSAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Secondary WINS address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetSWINSAddress(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,S24NetworkInfo.szWINS[1]);

	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetSWINSAddress
//
//  PROTOTYPE:void SetSWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the Secondary WINS address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetSWINSAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,5))
	{
		MyError(TEXT("Invalid WINS Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szWINS[1]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}
}


// -------------------------------------------------------------
//  FUNCTION: GetPGatewayAddress
//
//  PROTOTYPE:DWORD GetPGatewayAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Primary Gateway address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetPGatewayAddress(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,S24NetworkInfo.szGateway[0]);
	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetPGatewayAddress
//
//  PROTOTYPE:void SetPGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the primary gateway address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetPGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,6))
	{
		MyError(TEXT("Invalid Gateway Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szGateway[0]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}

}


// -------------------------------------------------------------
//  FUNCTION: GetSGatewayAddress
//
//  PROTOTYPE:DWORD GetSGatewayAddress(LPITEMSELECTOR lpis);
//
//  PARAMETERS: lpis: pointer to the item selector
//
//  DESCRIPTION: Get the Secondary Gateway address from registry
//
//  RETURNS: always 0
//
//  NOTES:
// -------------------------------------------------------------
DWORD GetSGatewayAddress(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,S24NetworkInfo.szGateway[1]);
	return 0;
}


// -------------------------------------------------------------
//  FUNCTION: SetSGatewayAddress
//
//  PROTOTYPE:void SetSGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex);
//
//  PARAMETERS: lpis: pointer to the item selector
//				dwIndex: index on the list
//
//  DESCRIPTION: Set the secondary gateway address
//
//  RETURNS: None 
//
//  NOTES:
// -------------------------------------------------------------
void SetSGatewayAddress(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if (!SetS24NetworkInfo(lpis->szText,7))
	{
		MyError(TEXT("Invalid Gateway Address"),lpis->szText);
		TEXTCPY(lpis->szText,S24NetworkInfo.szGateway[1]);
		ListView_SetItemText(l_hWndList,dwIndex,1,lpis->szText);
		return;
	}

}


// -------------------------------------------------------------
//  FUNCTION: IpClear
//
//  PROTOTYPE:BOOL IpClear(void);
//
//  PARAMETERS: None
//
//  DESCRIPTION: Clear the DHCP related registry entries
//
//  RETURNS: TRUE if success; FALSE otherwise 
//
//  NOTES:
// -------------------------------------------------------------
BOOL IpClear(void)
{
	DWORD retCode;
	HKEY hKey;

	retCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						   szTcpIpRegBase,
						   0,
						   0,
						   &hKey);

	if ( retCode != ERROR_SUCCESS )
	{
		return(FALSE);
	}

	RegDeleteValue(hKey,TEXT("PrevReqOptions"));
	RegDeleteValue(hKey,TEXT("T2"));
	RegDeleteValue(hKey,TEXT("T1"));
	RegDeleteValue(hKey,TEXT("Lease"));
	RegDeleteValue(hKey,TEXT("LeaseObtainedHigh"));
	RegDeleteValue(hKey,TEXT("LeaseObtainedLow"));
	RegDeleteValue(hKey,TEXT("DhcpWINS"));
	RegDeleteValue(hKey,TEXT("DhcpDNS"));
	RegDeleteValue(hKey,TEXT("DhcpDefaultGateway"));
	RegDeleteValue(hKey,TEXT("DhcpServer"));
	RegDeleteValue(hKey,TEXT("DhcpSubnetMask"));
	RegDeleteValue(hKey,TEXT("DhcpIPAddress"));

	RegCloseKey(hKey);

	return(TRUE);
}


// S24 WEP Defines

#define MAX_MASK_IDX ((sizeof(dwMaskValues) / sizeof(dwMaskValues[0])) - 1)

#define ENCRYPT_OUT(n) (n*2+2+4)

#define DECRYPT_OUT(n) ((n-2-4)/2)

#define ENCRYPTION_NONE 1
#define ENCRYPTION_LOW  2
#define ENCRYPTION_HIGH 3

#define ENCRYPTION_KEY1 1
#define ENCRYPTION_KEY2 2
#define ENCRYPTION_KEY3 3
#define ENCRYPTION_KEY4 4

#define KEY_LOW_LEN (40/4)
//#define KEY_HIGH_LEN (128/4)
#define KEY_HIGH_LEN 26


#define KEY1_DEFAULT TEXT("1")
#define KEY2_DEFAULT TEXT("2")
#define KEY3_DEFAULT TEXT("3")
#define KEY4_DEFAULT TEXT("4")

#define SERIAL_KEY_PREFIX 4
#define SERIAL_KEY_DASH1 SERIAL_KEY_PREFIX
#define SERIAL_KEY_SEG 7
#define SERIAL_KEY_DASH2 SERIAL_KEY_PREFIX + 1 + SERIAL_KEY_SEG
#define SERIAL_KEY_LEN SERIAL_KEY_PREFIX + 1 + SERIAL_KEY_SEG + 1 + SERIAL_KEY_SEG


// S24 WEP Variables

static DWORD l_dwEncryptionAlgorithm;
static DWORD l_dwEncryptionKeyId;
static TCHAR l_szEncryptionKey1[KEY_HIGH_LEN+1];
static TCHAR l_szEncryptionKey2[KEY_HIGH_LEN+1];
static TCHAR l_szEncryptionKey3[KEY_HIGH_LEN+1];
static TCHAR l_szEncryptionKey4[KEY_HIGH_LEN+1];

#ifdef SYMBOL_ACCESS_CODE
static TCHAR l_szSerialKey[SERIAL_KEY_LEN+1];
#endif

static DWORD dwMaskValues[64] =
{
  0xA4, 0xE2, 0xFE, 0x37, 0x80, 0x54, 0x21, 0x09, 
  0xAB, 0xCD, 0xEE, 0x54, 0xDD, 0x11, 0x31, 0x32,
  0xF1, 0xD2, 0xC5, 0x67, 0x89, 0x90, 0x33, 0x75,
  0x22, 0x6E, 0xB4, 0x5E, 0x9A, 0x0A, 0xB2, 0xCC,
  0xA6, 0xAD, 0xE2, 0x84, 0xDD, 0x06, 0x21, 0x3C,
  0x22, 0x6E, 0xA4, 0x5E, 0x4A, 0xAA, 0x12, 0xCC,
  0xF2, 0xA2, 0xC5, 0xC7, 0x89, 0x90, 0x33, 0x75,
  0xFF, 0xE2, 0xFE, 0x37, 0x81, 0x55, 0x21, 0xE9, 
};


// S24 WEP Functions

DWORD HexToBin(LPTSTR lpszHex,
			   DWORD dwCount)
{
	DWORD i;
	DWORD dwBin;

	dwBin = 0;

	for(i=0;i<dwCount;i++)
	{
		dwBin *= 16;

		if ( iswdigit(lpszHex[i]) )
		{
			dwBin += (DWORD)( lpszHex[i] - TEXT('0') + 0x00 );
		}
		else if ( iswupper(lpszHex[i]) )
		{
			dwBin += (DWORD)( lpszHex[i] - TEXT('A') + 0x0A );
		}
		else
		{
			dwBin += (DWORD)( lpszHex[i] - TEXT('a') + 0x0A );
		}
	}

	return(dwBin);
}


DWORD Checksum16(LPTSTR lpszValue,
				 DWORD dwCount)
{
	DWORD dwSum;
	DWORD i;
	
	dwSum = 0;

	for(i=0;i<dwCount;i++)
	{
	    dwSum += lpszValue[i];
	}

	return(dwSum);
}


BOOL ValidateHex(LPTSTR lpszValue,
				 DWORD dwCount,
				 DWORD dwRequired)
{
	DWORD i;

	if ( dwRequired != 0 )
	{
		if ( TEXTLEN(lpszValue) != dwRequired )
		{
			return(FALSE);
		}
	}
	
	for(i=0;i<dwCount;i++)
	{
		if ( !iswxdigit(lpszValue[i]) )
		{
			return(FALSE);
		}
	}

	return(TRUE);
}


BOOL EncryptHexData(LPTSTR lpszOutValue,
					DWORD dwOutSize,
					LPTSTR lpszInValue)
{
	DWORD dwInLen;
	DWORD dwOutLen;
	DWORD dwMaskIdx;
	DWORD i;

	// Validate pointer parameters
	if ( ( lpszOutValue == NULL ) ||
		 ( lpszInValue == NULL ) )
	{
		return(FALSE);
	}

	// Get input length
	dwInLen = TEXTLEN(lpszInValue);
	
	// Validate output size and input len
	if ( ( dwOutSize < ENCRYPT_OUT(dwInLen) ) ||
		 ( ( dwInLen % 2 ) != 0 ) )
	{
		return(FALSE);
	}

	// Get random index into mask table as 2 hex digits
	dwMaskIdx = rand() % 64;
	TEXTSPRINTF(lpszOutValue,TEXT("%.2X"),dwMaskIdx);

	// Start running output length
	dwOutLen = TEXTLEN(lpszOutValue);

	// Loop through string 1 hex pair at a time
	for(i=0;lpszInValue[i]!=TEXT('\0');i+=2)
	{
		// Validate hex pair
		if ( !ValidateHex(lpszInValue+i,2,0) )
		{
			return(FALSE);
		}
		
		// Put next encrypted hex pair
		TEXTSPRINTF(lpszOutValue+dwOutLen,
					TEXT("%.2X"),
					HexToBin(lpszInValue+i,2) ^ dwMaskValues[dwMaskIdx]);

		// Keep running output length
		dwOutLen = TEXTLEN(lpszOutValue);

		// Add random hex pair for obfuscation
		TEXTSPRINTF(lpszOutValue+dwOutLen,
					TEXT("%.2X"),
					rand() % 255);

		// Keep running output length
		dwOutLen = TEXTLEN(lpszOutValue);

		// Increment to next mask value.
		dwMaskIdx++;

		// Roll over if necessary.
		if ( dwMaskIdx > MAX_MASK_IDX)
		{
			dwMaskIdx = 0;
		}
	}
	
	// Put checksum value as 4 hex digits
	TEXTSPRINTF(lpszOutValue+dwOutLen,
				TEXT("%.4X"),
				Checksum16(lpszOutValue,dwOutLen));

	return(TRUE);
}


BOOL DecryptHexData(LPTSTR lpszOutValue,
					DWORD dwOutSize,
					LPTSTR lpszInValue)
{
	DWORD dwInLen;
	DWORD dwCompCheckSum;
	DWORD dwInCheckSum;
	DWORD dwMaskIdx;
	DWORD i,j;

	// Validate pointer parameters
	if ( ( lpszOutValue == NULL ) ||
		 ( lpszInValue == NULL ) )
	{
		return(FALSE);
	}

	// Get input length
	dwInLen = TEXTLEN(lpszInValue);
	
	// Validate output size and input len
	if ( ( dwOutSize < DECRYPT_OUT(dwInLen) ) ||
		 ( ( dwInLen % 2 ) != 0 ) ||
		 ( dwInLen < ENCRYPT_OUT(2) ) )
	{
		return(FALSE);
	}

	dwMaskIdx = HexToBin(lpszInValue,2);

	dwInCheckSum = HexToBin(lpszInValue+dwInLen-4,4);

	dwCompCheckSum = Checksum16(lpszInValue,dwInLen-4);

	if ( dwInCheckSum != dwCompCheckSum )
	{
		return(FALSE);
	}

	for(i=0,j=2;i<DECRYPT_OUT(dwInLen);i+=2,j+=4)
	{
		// Validate hex pair and following random hex pair
		if ( !ValidateHex(lpszInValue+j,4,0) )
		{
			return(FALSE);
		}
		
		TEXTSPRINTF(lpszOutValue+i,
					TEXT("%.2X"),
					HexToBin(lpszInValue+j,2) ^ dwMaskValues[dwMaskIdx]);

		// Increment to next mask value.
		dwMaskIdx++;

		// Roll over if necessary.
		if ( dwMaskIdx > MAX_MASK_IDX)
		{
			dwMaskIdx = 0;
		}
	}

	return(TRUE);
}


BOOL EncryptClearText(LPTSTR lpszOutValue,
					  DWORD dwOutSize,
					  LPTSTR lpszInValue)
{
	TCHAR szTemp[MAX_PATH];
	DWORD i;
	
	// Validate pointer parameters
	if ( ( lpszOutValue == NULL ) ||
		 ( lpszInValue == NULL ) )
	{
		return(FALSE);
	}

	for(i=0;i<TEXTLEN(lpszInValue);i++)
	{
		if ( i*2 >= MAX_PATH+1 )
		{
			return(FALSE);
		}

		TEXTSPRINTF(szTemp+2*i,TEXT("%.2X"),(BYTE)lpszInValue[i]);
	}

	return(EncryptHexData(lpszOutValue,dwOutSize,szTemp));
}


BOOL DecryptClearText(LPTSTR lpszOutValue,
					  DWORD dwOutSize,
					  LPTSTR lpszInValue)
{
	BOOL bResult;
	TCHAR szTemp[MAX_PATH];
	DWORD i;

	// Validate pointer parameters
	if ( ( lpszOutValue == NULL ) ||
		 ( lpszInValue == NULL ) )
	{
		return(FALSE);
	}

	bResult = DecryptHexData(szTemp,MAX_PATH,lpszInValue);

	if ( !bResult )
	{
		return(FALSE);
	}

	for(i=0;i<TEXTLEN(szTemp)/2;i++)
	{
		lpszOutValue[i] = (TCHAR)HexToBin(szTemp+2*i,2);
		lpszOutValue[i+1] = TEXT('\0');
	}

	return(TRUE);
}


void EnterS24WEP(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	DWORD dwRetCode;
	DWORD dwLen;
	DWORD dwType;
	HKEY hKey;

	SetCursor(LoadCursor(NULL,IDC_WAIT));

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							 szParmsRegBase,
							 0,
							 KEY_ALL_ACCESS,
							 &hKey);
      
    if ( dwRetCode != ERROR_SUCCESS )
    {
		hKey = NULL;
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(l_dwEncryptionAlgorithm);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("MUEncryptionAlgorithm"),
									NULL,
									&dwType,
									(LPBYTE)&l_dwEncryptionAlgorithm,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) )
    {
		l_dwEncryptionAlgorithm = ENCRYPTION_NONE;
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(l_dwEncryptionKeyId);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("EncryptionKeyId"),
									NULL,
									&dwType,
									(LPBYTE)&l_dwEncryptionKeyId,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) )
    {
		l_dwEncryptionKeyId = ENCRYPTION_KEY1;
	}

	RegCloseKey(hKey);

	LoadString(g_hInstance,IDS_S24WEP,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
	    						CtlPanelDialogChoices,
			    				g_hwnd,
				    			CtlPanelDlgProc,
					    		(LPARAM)S24WEPSelector);

	SetCursor(NULL);
}


void ExitS24WEP(LPITEMSELECTOR lpis,DWORD dwSelection)
{
#ifndef USE_UPDATE_REG
	DWORD dwRetCode;
	DWORD dwType;
	HKEY hKey;
#endif
	BOOL bWasOKed = l_bDialogWasOKed;

	l_bDialogWasOKed = FALSE;   

	l_lpExitFunction = ExitConfigS24;

	if ( !bWasOKed )
	{
		return;
	}

	SetCursor(LoadCursor(NULL,IDC_WAIT));

#ifdef USE_UPDATE_REG

	UpdateRegistry(szParmsRegBase,TEXT("MUEncryptionAlgorithm"),
				   REG_DWORD,
				   (LPVOID)&l_dwEncryptionAlgorithm);

	UpdateRegistry(szParmsRegBase,
				   TEXT("EncryptionKeyId"),
				   REG_DWORD,
				   (LPVOID)&l_dwEncryptionKeyId);

#else

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							 szParmsRegBase,
							 0,
							 KEY_ALL_ACCESS,
							 &hKey);
      
    if ( dwRetCode != ERROR_SUCCESS )
    {
		SetCursor(NULL);

		return;
	}

	dwType = REG_DWORD;

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("MUEncryptionAlgorithm"),
							  0,
							  dwType,
							  (LPBYTE)&l_dwEncryptionAlgorithm,
							  sizeof(l_dwEncryptionAlgorithm));
			
	dwType = REG_DWORD;

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("EncryptionKeyId"),
							  0,
							  dwType,
							  (LPBYTE)&l_dwEncryptionKeyId,
							  sizeof(l_dwEncryptionKeyId));
			
	RegCloseKey(hKey);

#endif

	SetCursor(NULL);
}


void FetchWEPKeys(void)
{
	DWORD dwRetCode;
	DWORD dwLen;
	DWORD dwType;
	HKEY hKey;
	TCHAR szEncrypt[MAX_PATH];

	if ( l_dwEncryptionAlgorithm == ENCRYPTION_NONE )
	{
		return;
	}
	
	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							 szParmsRegBase,
							 0,
							 KEY_ALL_ACCESS,
							 &hKey);
      
    if ( dwRetCode != ERROR_SUCCESS )
    {
		hKey = NULL;
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(szEncrypt);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("EncryptionKey1"),
									NULL,
									&dwType,
									(LPBYTE)szEncrypt,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) ||
		 ( !DecryptHexData(l_szEncryptionKey1,TEXTSIZEOF(l_szEncryptionKey1),szEncrypt) ) )
    {
		l_szEncryptionKey1[0] = TEXT('\0');
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(szEncrypt);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("EncryptionKey2"),
									NULL,
									&dwType,
									(LPBYTE)szEncrypt,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) ||
		 ( !DecryptHexData(l_szEncryptionKey2,TEXTSIZEOF(l_szEncryptionKey2),szEncrypt) ) )
    {
		l_szEncryptionKey2[0] = TEXT('\0');
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(szEncrypt);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("EncryptionKey3"),
									NULL,
									&dwType,
									(LPBYTE)szEncrypt,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) ||
		 ( !DecryptHexData(l_szEncryptionKey3,TEXTSIZEOF(l_szEncryptionKey3),szEncrypt) ) )
    {
		l_szEncryptionKey3[0] = TEXT('\0');
	}

	if ( hKey != NULL )
	{
		dwLen = sizeof(szEncrypt);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("EncryptionKey4"),
									NULL,
									&dwType,
									(LPBYTE)szEncrypt,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) ||
		 ( !DecryptHexData(l_szEncryptionKey4,TEXTSIZEOF(l_szEncryptionKey4),szEncrypt) ) )
	{
		l_szEncryptionKey4[0] = TEXT('\0');
	}

#ifdef SYMBOL_ACCESS_CODE
	if ( hKey != NULL )
	{
		dwLen = sizeof(szEncrypt);

		dwRetCode = RegQueryValueEx(hKey,
									TEXT("SerialKey"),
									NULL,
									&dwType,
									(LPBYTE)szEncrypt,
									&dwLen);
	}
	
    if ( ( hKey == NULL ) ||
		 ( dwRetCode != ERROR_SUCCESS ) ||
		 ( !DecryptClearText(l_szSerialKey,TEXTSIZEOF(l_szSerialKey),szEncrypt) ) )
	{
		l_szSerialKey[0] = TEXT('\0');
	}
#endif

	RegCloseKey(hKey);

	// Pad key values out to max length with default value
	while ( TEXTLEN(l_szEncryptionKey1) < KEY_HIGH_LEN )
	{
		TEXTCAT(l_szEncryptionKey1,KEY1_DEFAULT);
	}
	while ( TEXTLEN(l_szEncryptionKey2) < KEY_HIGH_LEN )
	{
		TEXTCAT(l_szEncryptionKey2,KEY2_DEFAULT);
	}
	while ( TEXTLEN(l_szEncryptionKey3) < KEY_HIGH_LEN )
	{
		TEXTCAT(l_szEncryptionKey3,KEY3_DEFAULT);
	}
	while ( TEXTLEN(l_szEncryptionKey4) < KEY_HIGH_LEN )
	{
		TEXTCAT(l_szEncryptionKey4,KEY4_DEFAULT);
	}

	// Determine proper key length
	if ( l_dwEncryptionAlgorithm == ENCRYPTION_LOW )
	{
		dwLen = KEY_LOW_LEN;
	}
	else
	{
		dwLen = KEY_HIGH_LEN;
	}

	// Trim to appropriate length
	l_szEncryptionKey1[dwLen] = TEXT('\0');
	l_szEncryptionKey2[dwLen] = TEXT('\0');
	l_szEncryptionKey3[dwLen] = TEXT('\0');
	l_szEncryptionKey4[dwLen] = TEXT('\0');
}


void StoreWEPKeys(void)
{
#ifndef USE_UPDATE_REG
	DWORD dwRetCode;
	DWORD dwType;
	HKEY hKey;
#endif

	TCHAR szEncrypt[MAX_PATH];

	if ( l_dwEncryptionAlgorithm == ENCRYPTION_NONE )
	{
		return;
	}
	
#ifdef USE_UPDATE_REG

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey1);

	UpdateRegistry(szParmsRegBase,TEXT("EncryptionKey1"),
				   REG_SZ,
				   (LPVOID)szEncrypt);

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey2);

	UpdateRegistry(szParmsRegBase,TEXT("EncryptionKey2"),
				   REG_SZ,
				   (LPVOID)szEncrypt);

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey2);

	UpdateRegistry(szParmsRegBase,TEXT("EncryptionKey2"),
				   REG_SZ,
				   (LPVOID)szEncrypt);

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey3);

	UpdateRegistry(szParmsRegBase,TEXT("EncryptionKey3"),
				   REG_SZ,
				   (LPVOID)szEncrypt);

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey4);

	UpdateRegistry(szParmsRegBase,TEXT("EncryptionKey4"),
				   REG_SZ,
				   (LPVOID)szEncrypt);

#ifdef SYMBOL_ACCESS_CODE
	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szSerialKey);

	UpdateRegistry(szParmsRegBase,TEXT("SerialKey"),
				   REG_SZ,
				   (LPVOID)szEncrypt);
#endif

#else

	dwRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
							 szParmsRegBase,
							 0,
							 KEY_ALL_ACCESS,
							 &hKey);
      
    if ( dwRetCode != ERROR_SUCCESS )
    {
		return;
	}

	dwType = REG_SZ;

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey1);

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("EncryptionKey1"),
							  0,
							  dwType,
							  (LPBYTE)szEncrypt,
							  ((TEXTLEN(szEncrypt)+1)*sizeof(TCHAR)));

	dwType = REG_SZ;

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey2);

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("EncryptionKey2"),
							  0,
							  dwType,
							  (LPBYTE)szEncrypt,
							  ((TEXTLEN(szEncrypt)+1)*sizeof(TCHAR)));

	dwType = REG_SZ;

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey3);

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("EncryptionKey3"),
							  0,
							  dwType,
							  (LPBYTE)szEncrypt,
							  ((TEXTLEN(szEncrypt)+1)*sizeof(TCHAR)));

	dwType = REG_SZ;

	EncryptHexData(szEncrypt,TEXTSIZEOF(szEncrypt),l_szEncryptionKey4);

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("EncryptionKey4"),
							  0,
							  dwType,
							  (LPBYTE)szEncrypt,
							  ((TEXTLEN(szEncrypt)+1)*sizeof(TCHAR)));

	dwType = REG_SZ;

#ifdef SYMBOL_ACCESS_CODE
	EncryptClearText(szEncrypt,TEXTSIZEOF(szEncrypt),l_szSerialKey);

	dwRetCode = RegSetValueEx(hKey,
							  TEXT("SerialKey"),
							  0,
							  dwType,
							  (LPBYTE)szEncrypt,
							  ((TEXTLEN(szEncrypt)+1)*sizeof(TCHAR)));
#endif

	RegCloseKey(hKey);

#endif

}


void EnterS24WEPKeys(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	if ( l_dwEncryptionAlgorithm == ENCRYPTION_NONE )
	{
		return;
	}
	
	SetCursor(LoadCursor(NULL,IDC_WAIT));

	FetchWEPKeys();
	
	LoadString(g_hInstance,IDS_S24WEPKEYS,l_szLastItem,TEXTSIZEOF(l_szLastItem));

    l_lpExitFunction = lpis->lpExitFunction;
	l_lpItemSelector = lpis;
	l_dwIndex = dwSelection;

	g_hdlg = CreateChosenDialog(g_hInstance,
	    						CtlPanelDialogChoices,
			    				g_hwnd,
				    			CtlPanelDlgProc,
					    		(LPARAM)S24WEPKeysSelector);

	SetCursor(NULL);
}


void ExitS24WEPKeys(LPITEMSELECTOR lpis,DWORD dwSelection)
{
	BOOL bWasOKed = l_bDialogWasOKed;

	l_bDialogWasOKed = FALSE;   

	l_lpExitFunction = ExitS24WEP;

	if ( !bWasOKed )
	{
		return;
	}

	SetCursor(LoadCursor(NULL,IDC_WAIT));

	StoreWEPKeys();

	SetCursor(NULL);
}


DWORD GetWEPAlgorithm(LPITEMSELECTOR lpis)
{
    return(l_dwEncryptionAlgorithm-1);
}

void SetWEPAlgorithm(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	StoreWEPKeys();

	l_dwEncryptionAlgorithm = dwIndex + 1;

	FetchWEPKeys();
}


DWORD GetWEPIndex(LPITEMSELECTOR lpis)
{
    return(l_dwEncryptionKeyId-1);
}

void SetWEPIndex(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	l_dwEncryptionKeyId = dwIndex + 1;
}


BOOL ValidateWEPKey(LPTSTR lpszValue,LPTSTR lpszName)
{
	DWORD dwLen;
	
	// Determine proper key length
	if ( l_dwEncryptionAlgorithm == ENCRYPTION_LOW )
	{
		dwLen = KEY_LOW_LEN;
	}
	else
	{
		dwLen = KEY_HIGH_LEN;
	}

	// Validate entered keys to appropriate length
	if ( !ValidateHex(lpszValue,TEXTLEN(lpszValue),dwLen) )
	{
		MyError(TEXT("Invalid value"),lpszName);

		return(FALSE);
	}

	return(TRUE);
}


#ifdef SYMBOL_ACCESS_CODE

BOOL ValidateSerialKey(LPTSTR lpszValue,LPTSTR lpszName)
{
	DWORD i;
	
	if ( TEXTLEN(lpszValue) != SERIAL_KEY_LEN )
	{
		MyError(TEXT("Invalid length"),lpszName);

		return(FALSE);
	}

	if ( ( lpszValue[SERIAL_KEY_DASH1] != TEXT('-') ) ||
		 ( lpszValue[SERIAL_KEY_DASH2] != TEXT('-') ) )
	{
		MyError(TEXT("Invalid delimeter"),lpszName);

		return(FALSE);
	}

	for(i=0;i<SERIAL_KEY_PREFIX;i++)
	{
		if ( !iswalpha(lpszValue[i]) )
		{
			MyError(TEXT("Invalid prefix"),lpszName);

			return(FALSE);
		}
	}
	
	for(i=0;i<SERIAL_KEY_SEG;i++)
	{
		if ( ( !iswdigit(lpszValue[SERIAL_KEY_DASH1+1+i]) ) ||
			 ( !iswdigit(lpszValue[SERIAL_KEY_DASH2+1+i]) ) )
		{
			MyError(TEXT("Invalid value"),lpszName);

			return(FALSE);
		}
	}
	
	return(TRUE);
}
#endif


DWORD GetWEPKey1(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,l_szEncryptionKey1);
	
    return(0);
}

void SetWEPKey1(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if ( ValidateWEPKey(lpis->szText,TEXT("Key 1")) )
	{
		TEXTCPY(l_szEncryptionKey1,lpis->szText);
	}
	else
	{
		TEXTCPY(lpis->szText,l_szEncryptionKey1);
	}
}

DWORD GetWEPKey2(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,l_szEncryptionKey2);
	
    return(0);
}

void SetWEPKey2(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if ( ValidateWEPKey(lpis->szText,TEXT("Key 2")) )
	{
		TEXTCPY(l_szEncryptionKey2,lpis->szText);
	}
	else
	{
		TEXTCPY(lpis->szText,l_szEncryptionKey2);
	}
}

DWORD GetWEPKey3(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,l_szEncryptionKey3);
	
    return(0);
}

void SetWEPKey3(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if ( ValidateWEPKey(lpis->szText,TEXT("Key 3")) )
	{
		TEXTCPY(l_szEncryptionKey3,lpis->szText);
	}
	else
	{
		TEXTCPY(lpis->szText,l_szEncryptionKey3);
	}
}

DWORD GetWEPKey4(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,l_szEncryptionKey4);
	
    return(0);
}

void SetWEPKey4(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if ( ValidateWEPKey(lpis->szText,TEXT("Key 4")) )
	{
		TEXTCPY(l_szEncryptionKey4,lpis->szText);
	}
	else
	{
		TEXTCPY(lpis->szText,l_szEncryptionKey4);
	}
}

#ifdef SYMBOL_ACCESS_CODE

DWORD GetSerialKey(LPITEMSELECTOR lpis)
{
	TEXTCPY(lpis->szText,l_szSerialKey);
	
    return(0);
}

void SetSerialKey(LPITEMSELECTOR lpis,DWORD dwIndex)
{
	if ( l_dwEncryptionAlgorithm == ENCRYPTION_HIGH )
	{
		if ( ValidateSerialKey(lpis->szText,TEXT("Access Code")) )
		{
			TEXTCPY(l_szSerialKey,lpis->szText);
		}
		else
		{
			TEXTCPY(lpis->szText,l_szSerialKey);
		}
	}
	else
	{
		TEXTCPY(lpis->szText,l_szSerialKey);
	}
}

#endif

#endif
