
//--------------------------------------------------------------------
// FILENAME:       Cradle.h
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Cradle.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef CRADLE_H_

#define CRADLE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

#define	_TCradleSettingsKey	TEXT("Drivers\\Builtin\\Cradle\\Settings")
#define	_TAccessMode		TEXT("Access Mode")

#define _TCRDLAPI			TEXT("CrdlApi32.dll")
#define CRDL_OPEN			TEXT("CRADLE_Open")
#define CRDL_CLOSE			TEXT("CRADLE_Close")
#define CRDL_GETVER			TEXT("CRADLE_GetVersion")
#define CRDL_GETMODE		TEXT("CRADLE_GetMode")
#define CRDL_SETMODE		TEXT("CRADLE_SetMode")
#define CRDL_GETTYPE		TEXT("CRADLE_GetType")
#define CRDL_GETSTATUS		TEXT("CRADLE_GetInCradleStatus")

typedef DWORD (CRADLEAPI*	LPFNCRDL_OPEN)(LPHANDLE lphCradle);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_CLOSE)(void);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_GETVER)(LPCRADLE_VERSION_INFO lpCradleVersionInfo);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_GETMODE)(LPDWORD lpdwMode);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_SETMODE)(DWORD dwMode);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_GETTYPE)(LPDWORD lpdwType);	
typedef DWORD (CRADLEAPI*	LPFNCRDL_GETSTATUS)(LPDWORD lpdwStatus);	


extern BOOL    bCradlePresent;

LPTSTR lpszCradleMode[] =
{
	RESOURCE_STRING(IDS_MANUALMODE),
	RESOURCE_STRING(IDS_SINGLEMODE),
	RESOURCE_STRING(IDS_MULTIMODE),
	NULL
};

LPTSTR lpszCradleType[] =
{
    TEXT("7200 Single Slot"),
    TEXT("7200 Four Slot"),
    TEXT("5700 Vehicle Slot"),
    TEXT("5700 Single Slot"),
    TEXT("5700 Four Slot"),
    TEXT("7500 Vehicle Slot"),
    TEXT("7500 Single Slot"),
    TEXT("7500 Four Slot"),
    NULL
};
    
// we need GetVersion, Get Type and Get/Set Mode
DWORD GetCradleMode(LPITEMSELECTOR lpis);
void SetCradleMode(LPITEMSELECTOR lpis,DWORD dwSelection);

LPTSTR GetCradleType(LPITEMSELECTOR lpis, DWORD dwIndex);

LPTSTR GetHardwareVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetFirmwareVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetIrDAVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetIrCommVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetDriverVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex);

ITEMSELECTOR CradleSelector[] =
{

	{
		NULL,
		RESOURCE_STRING(IDS_CRDLMODE),
		NO_HELP,
		STRING_LIST(lpszCradleMode,GetCradleMode,SetCradleMode,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_CRDLTYPE),
		NO_HELP,
		GET_STRING(GetCradleType,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetCAPIVersion,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_CRDLDRIVERVERSION),
		NULL,
		GET_STRING(GetDriverVersion,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_CRDLFIRMWAREVERSION),
		NULL,
		GET_STRING(GetFirmwareVersion,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_HARDWAREVERSION),
		NO_HELP,
		GET_STRING(GetHardwareVersion,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_CRDLIRDAVERSION),
		NULL,
		GET_STRING(GetIrDAVersion,NULL)
	},
	{
		&bCradlePresent,
		RESOURCE_STRING(IDS_CRDLIRCOMMVERSION),
		NULL,
		GET_STRING(GetIrCommVersion,NULL)
	},
	
	END_ITEMSELECTOR
};

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef AUDIO_H_    */
