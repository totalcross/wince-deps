
//--------------------------------------------------------------------
// FILENAME:        Audio.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Audio.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef AUDIO_H_

#define AUDIO_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"

static BOOL bAudioSubsystemPresent = TRUE;
static BOOL bAudioTest = FALSE;
static BOOL bAudioLowHigh = FALSE;
static BOOL bAudioLowMedHigh = FALSE;
static BOOL bAudioESDSupport = FALSE;

static LPTSTR lpszRange0to60000by100[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_60),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszValuesLowMedHigh[] =
{
	RESOURCE_STRING(IDS_LOW),
    RESOURCE_STRING(IDS_MED),
	RESOURCE_STRING(IDS_HIGH),
	NULL
};

static LPTSTR lpszValuesLowHigh[] =
{
	RESOURCE_STRING(IDS_LOW),
	RESOURCE_STRING(IDS_HIGH),
	NULL
};

static LPTSTR lpszValuesTest[] =
{
	RESOURCE_STRING(IDS_TEST),
	NULL
};



DWORD GetInitialVolume(LPITEMSELECTOR lpis);
void SelectBeeperVolume(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialESD(LPITEMSELECTOR lpis);
void SelectEarSaveDelay(LPITEMSELECTOR lpis,DWORD dwSelection);
static LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
static LPTSTR GetNotifyCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex);

ITEMSELECTOR AudioParametersSelector[] =
{
	{
		&bAudioTest,
		RESOURCE_STRING(IDS_BEEPERVOLUME),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesTest,GetInitialVolume,SelectBeeperVolume,NULL)
	},
	
	{
		&bAudioLowHigh,
		RESOURCE_STRING(IDS_BEEPERVOLUME),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesLowHigh,GetInitialVolume,SelectBeeperVolume,NULL)
	},
	
	{
		&bAudioLowMedHigh,
		RESOURCE_STRING(IDS_BEEPERVOLUME),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesLowMedHigh,GetInitialVolume,SelectBeeperVolume,NULL)
	},

	{
		&bAudioESDSupport,
		RESOURCE_STRING(IDS_EARSAVEDELAY),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to60000by100,GetInitialESD,SelectEarSaveDelay)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetCAPIVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_NOTIFYCAPIVERSION),
		NO_HELP,
		GET_STRING(GetNotifyCAPIVersion,NULL)
	},
	
	END_ITEMSELECTOR
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef AUDIO_H_    */
