
//--------------------------------------------------------------------
// FILENAME:        S24API.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for S24API.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef S24API_H_

#define S24API_H_

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(1)

#ifndef S24_RESEARCH_ADAPTERS
	#define S24_RESEARCH_ADAPTERS						112
#endif


typedef struct tagS24EXTENSION
{
    BYTE    bMacAddress[6];
    DWORD   dwFunctionCode;
    HWND    hClientWindow;      // used for event message, 
                                // set to 0 for synchronous action
    DWORD   dwUserToken;
    DWORD   dwMaxWaitForComplete;   // time out for sync action
    LPVOID  lpInputBuffer;
    DWORD   dwInputBufferLength;
    LPVOID  lpOutputBuffer;
    DWORD   dwOutputBufferLength;
    LPDWORD lpReturnedLength;
    LPDWORD lpReturnCode;

} S24EXTENSION;

typedef S24EXTENSION FAR * LPS24EXTENSION;

typedef struct tagS24PINGINFO
{ 
    // IEEE address
    BYTE	bMacAddress[6];   // 00,a0,f8,76,b3,a5

	BYTE	PingInputBuffer[2000];
	BYTE	PingResultBuffer[2000];
	
	WORD	nSizeOfPing;
	WORD	nPingSequence;
	HWND	hWnd;

} S24PINGINFO;

typedef S24PINGINFO FAR * LPS24PINGINFO;

// S24 driver function
typedef BOOL (WINAPI *FPS24DRIVEREXTENSION)(PBYTE,		// MAC Address
								     DWORD,				// Function
								     HWND,				// Client Window Handle
								     DWORD,				// Client Token
								     DWORD,				// Synchronous timeout
								     LPVOID,			// Pointer to Input Buffer
								     DWORD,				// Length of Input Buffer
								     LPVOID,			// Pointer to Output Buffer
								     DWORD,				// Length of Output Buffer
								     LPDWORD,			// Returned Length
								     LPDWORD			// Return Code			
									 );			


typedef BOOL (WINAPI *FPS24DRIVERINIT) (void);

#pragma pack()

#define S24_GOODPING		0x8001
#define S24_BADPINGDATA		0x8002
#define S24_NOTASSOCIATED	0x8003
#define S24_NOTCONNECTED	0x8004
#define S24_ROAMING			0x8005

BOOL FoundRFNetWork(LPTSTR lpszRFNetWork);
BOOL S24_LoadEventMonitor(void);
HINSTANCE S24_LoadS24Driver(void);
void S24_FreeS24Driver(HINSTANCE hLib);
BOOL S24_InitS24Driver(HINSTANCE hLib);

void S24_InitCommand( LPS24EXTENSION pCmd, DWORD dwFunctionCode);
BOOL S24_CallS24Driver(	LPS24EXTENSION pCmd);

BOOL S24_IsConnected(void);
BOOL S24_IsAssociated(void);
BOOL S24_Is802(void);
BOOL S24_IsMicroAP(void);
BOOL S24_IsDS(void);

DWORD S24_GetMuMacAddress(LPBYTE pMacBuf);
//DWORD S24_GetESSID(LPTSTR lpszESSID);
//DWORD S24_GetFirmwareVersion(LPTSTR lpszFirmware);
DWORD S24_GetESSID(LPBYTE pESSIDBuf);
DWORD S24_GetFirmwareVersion(LPBYTE pFWVBuf);
DWORD S24_GetDriverVersion(LPDWORD lpdwVersion);
DWORD S24_GetPowerMode(LPWORD lpnPowerMode);
DWORD S24_GetBeaconParameters ( LPWORD lpnBeaconAlgorithm,
                        	LPWORD lpnBeaconMin,
	                        LPWORD lpnBeaconMax);
DWORD S24_GetAPMacAddress(LPBYTE pMacBuf);
DWORD S24_GetLinkSpeed(LPWORD lpnLinkSpeed);
DWORD S24_GetSupportedRates(LPWORD lpn1MBit, LPWORD lpn2MBit, LPWORD lpn5MBit, LPWORD lpn11MBit);
DWORD S24_GetDiversity(LPWORD lpnDiversity);
DWORD S24_GetCountryTextAndCode(LPBYTE pCountryText, LPWORD lpnCountryCode);
DWORD S24_GetChannelQualityValues( LPWORD lpnPercentBeaconsMissed,
                               LPWORD lpnPercentTxRetries);
DWORD S24_GetActivity(LPWORD lpnTxCount, LPWORD lpnRxCount);
DWORD S24_GetAdapterList(void);
DWORD S24_GetPowerIndex(LPWORD lpPowerIndex);
BOOL S24_SetESSID(LPBYTE pESSID);
BOOL S24_SetPowerMode(WORD nPowerMode);
BOOL S24_SetBeaconParameters(USHORT nBeaconAlgorithm,USHORT nBeaconMin,	USHORT nBeaconMax);
BOOL S24_SetDiversity(WORD nDiversity);
BOOL S24_SetSupportedDataRates (WORD nDataRate_1_MBit, WORD nDataRate_2_MBit,WORD nDataRate_5_MBit, WORD nDataRate_11_MBit);
BOOL S24_SetFunctionMode(WORD nMode);
BOOL S24_ResearchAdapters(void);
BOOL S24_SetPowerIndex(WORD nPowerIndex);

DWORD S24_SendWNMP(LPS24PINGINFO lpS24PingInfo);
//DWORD S24_SendWNMP(PBYTE lpPingAddress, PBYTE lpInputBuffer, 
//				   WORD nSizeOfPing, PBYTE lpOutputBuffer,  
//				   PDWORD lpdwRcvLength, WORD nToken, HANDLE hwnd);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CFGS24_H_    */


