
//--------------------------------------------------------------------
// FILENAME:        Comm.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Comm.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef COMM_H_

#define COMM_H_

#ifdef __cplusplus
extern "C"
{
#endif

// --- Nested Include ---
#include "values.h"

#define MAX_COMDEV	20

// --- Functions ---
TCHAR l_szCommSetting[MAX_PATH];
LPTSTR lpszCommSettings[MAX_COMDEV];

DWORD GetCommSettings(LPITEMSELECTOR lpis);
void SelectCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection);
/*
void EnterTCPIP(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitTCPIP(LPITEMSELECTOR lpis,DWORD dwSelection);
*/
static ITEMSELECTOR CommSettingsSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_COMMSETTINGS),
		l_lpszHelpFileName,
		STRING_LIST(lpszCommSettings,GetCommSettings,SelectCommSettings,NULL)
    },
/*
   	{
		NULL,
		RESOURCE_STRING(IDS_TCPIPSETTINGS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterTCPIP,ExitTCPIP)
    },
*/
	END_ITEMSELECTOR
};
#ifdef __cplusplus
}
#endif

#endif  /* #ifndef COMM_H_  */
