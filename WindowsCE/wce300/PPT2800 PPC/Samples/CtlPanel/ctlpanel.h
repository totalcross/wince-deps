
//--------------------------------------------------------------------
// FILENAME:        ctlpanel.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     CtlPanel
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef CTLPANEL_H_

#define CTLPANEL_H_

#ifdef __cplusplus
extern "C"
{
#endif

//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------

#include <StrucInf.h>		// Included for _x86_ definition
#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("ControlPanel")
#define APP_TITLE TEXT("Control Panel Test Program")
#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#define MAX_PATH8			8
#define MAX_PATH32		   32
#define MAX_PATH64		   64
#define MAX_PATH128		  128


//----------------------------------------------------------------------------
// Feature inclusion by platform type
//----------------------------------------------------------------------------

#ifdef _WIN32_WCE_EMULATION

//	#define _PERSISTCTL
	#define _ABOUTCTL

//	#define _SCANCTL
//	#define _DISPLAYCTL
//	#define _AUDIOCTL
//	#define _PRINTERCTL
	#define _UUID
		#define _SIMULATE_UUID TEXT("0123456789ABCDEFFEDCBA9876543210XXXXXX")
//	#define _COMMCTL

//	#define _POWERCTL
//	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
    #define _SYSVERSION

//	#define _TOUCH
	#define _DATEANDTIME

#endif

#ifdef WIN32_PLATFORM_PC

//	#define _PERSISTCTL
	#define _ABOUTCTL

//	#define _SCANCTL
//	#define _DISPLAYCTL
//	#define _AUDIOCTL
//	#define _PRINTERCTL
//	#define _UUID
//	#define _COMMCTL

//	#define _POWERCTL
//	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
//	#define _CRADLECTL
//	#define _SYSVERSION

//	#define _TOUCH
//	#define _DATEANDTIME

#endif

#ifdef WIN32_PLATFORM_2700

	#define _PERSISTCTL
	#define _ABOUTCTL

	#define _SCANCTL
	#define _DISPLAYCTL
//	#define _AUDIOCTL
//	#define _PRINTERCTL
//	#define _UUID
	#define _COMMCTL

//	#define _POWERCTL
	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
//	#define _CRADLECTL
	#define _SYSVERSION

	#define _TOUCH
	#define _DATEANDTIME

#endif

#ifdef WIN32_PLATFORM_2800

	#define _PERSISTCTL
	#define _ABOUTCTL

	#define _SCANCTL
	#define _DISPLAYCTL
	#define _AUDIOCTL
	#define _PRINTERCTL
	#define _UUID
	#define _COMMCTL

//	#define _POWERCTL
	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
    #define _SYSVERSION

	#define _TOUCH
	#define _DATEANDTIME

#endif

#ifdef WIN32_PLATFORM_8100

	#ifdef WIN32_PLATFORM_POCKETPC

		#define _PERSISTCTL
		#define _ABOUTCTL

		#define _SCANCTL
		#define _DISPLAYCTL
		#define _AUDIOCTL
		#define _PRINTERCTL
		#define _UUID
		#define _COMMCTL

	//	#define _POWERCTL
		#define _S24CTL
	//	#define _MEMORY_ADJUSTMENT
		#define _SYSVERSION

		#define _TOUCH
		#define _DATEANDTIME

	#else

		#define _PERSISTCTL
		#define _ABOUTCTL

		#define _SCANCTL
	//	#define _DISPLAYCTL
	//	#define _AUDIOCTL
	//	#define _PRINTERCTL
	//	#define _UUID
		#define _COMMCTL

	//	#define _POWERCTL
		#define _S24CTL
	//	#define _MEMORY_ADJUSTMENT
		#define _SYSVERSION

		#define _TOUCH
		#define _DATEANDTIME

	#endif

#endif

#ifdef WIN32_PLATFORM_7000

	#define _PERSISTCTL
	#define _ABOUTCTL

	#define _SCANCTL
	#define _DISPLAYCTL
	#define _AUDIOCTL
	#define _PRINTERCTL
	#define _UUID
	#define _COMMCTL

	#define _POWERCTL
	#define _S24CTL
	#define _MEMORY_ADJUSTMENT
    #define _CRADLECTL
    #define _SYSVERSION

	#define _TOUCH
	#define _DATEANDTIME

#endif

#ifdef WIN32_PLATFORM_GAZOO

	#define _PERSISTCTL
	#define _ABOUTCTL

//	#define _SCANCTL
//	#define _DISPLAYCTL
	#define _AUDIOCTL
//	#define _PRINTERCTL
//	#define _UUID
	#define _COMMCTL

//	#define _POWERCTL
	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
//  #define _CRADLECTL
    #define _SYSVERSION

	#define _TOUCH
	#define _DATEANDTIME

#endif

#ifdef SYMBOL_OEM_SCAN_KIT

//	#define _PERSISTCTL
	#define _ABOUTCTL

	#define _SCANCTL
//	#define _DISPLAYCTL
//	#define _AUDIOCTL
//	#define _PRINTERCTL
//	#define _UUID
//	#define _COMMCTL

//	#define _POWERCTL
//	#define _S24CTL
//	#define _MEMORY_ADJUSTMENT
//  #define _CRADLECTL
//    #define _SYSVERSION

//	#define _TOUCH
//	#define _DATEANDTIME

#endif


//----------------------------------------------------------------------------
// Library inclusion by feature
//----------------------------------------------------------------------------

#ifdef _WIN32_WCE
	#ifdef _S24CTL
		#pragma comment(lib,"winsock.lib")
		#pragma comment(lib,"icmplib.lib")
	#endif
	#ifdef _AUDIOCTL
		#pragma comment(lib,"audioapi32.lib")
	#endif

	#ifdef _DISPLAYCTL
		#pragma comment(lib,"dispapi32.lib")
	#endif

	#ifdef _MEMORY_ADJUSTMENT
		#pragma comment(lib,"memory.lib")
	#endif

	#ifdef _POWERCTL
		#pragma comment(lib,"pwrapi32.lib")
	#endif

	#ifdef _PRINTERCTL
		#pragma comment(lib,"printapi32.lib")
	#endif

	#ifdef _SCANCTL
		#pragma comment(lib,"scnapi32.lib")
	#endif
#else
	#pragma comment(lib,"comctl32.lib")
	#ifdef _SCANCTL
		#pragma comment(lib,"scancapi.lib")
	#endif
#endif


//----------------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------------
HWND l_hWndList;


//----------------------------------------------------------------------------
// Local functions
//----------------------------------------------------------------------------
BOOL IsValidDword(LPTSTR lpszText);
void ReportInvalidDword(LPTSTR lpszValue);


void CtlPanel(void);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CTLPANEL_H_  */
