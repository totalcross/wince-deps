
//--------------------------------------------------------------------
// FILENAME:       SysVersion.h
//
// Copyright(c) 2000-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for SysVersion.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#ifndef SYSVERSION_H_

#define SYSVERSION_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"


LPTSTR GetOEMVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetLoaderVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetWINCEVersion(LPITEMSELECTOR lpis, DWORD dwIndex);

ITEMSELECTOR SystemVersionSelector[] =
{

	{
		NULL,
		RESOURCE_STRING(IDS_WINCEVERSION),
		NO_HELP,
		GET_STRING(GetWINCEVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_OEMVERSION),
		NULL,
		GET_STRING(GetOEMVersion,NULL)
	},
	{
		NULL,
		RESOURCE_STRING(IDS_LOADERVERSION),
		NO_HELP,
		GET_STRING(GetLoaderVersion,NULL)
	},
	
	END_ITEMSELECTOR
};

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef AUDIO_H_    */
