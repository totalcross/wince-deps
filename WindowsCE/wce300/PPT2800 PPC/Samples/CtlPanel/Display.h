
//--------------------------------------------------------------------
// FILENAME:        Display.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Used for Display.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef DISPLAY_H_

#define DISPLAY_H_


#ifdef __cplusplus
extern "C"
{
#endif

#include "values.h"


static BOOL bBacklightLowHigh = FALSE;
static BOOL bBacklightLowMedHigh = FALSE;
static BOOL bBacklight0to255by1 = FALSE;
static BOOL bBacklight0to15by1 = FALSE;

static BOOL bContrast16 = FALSE;
static BOOL bContrast64 = FALSE;
static BOOL bContrast256 = FALSE;

static LPTSTR lpszRange0to255by1[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_255),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszRange0to63by1[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_63),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszRange0to15by1[] =
{
	RESOURCE_STRING(IDS_0),
	RESOURCE_STRING(IDS_15),
	RESOURCE_STRING(IDS_1),
	NULL
};

static LPTSTR lpszValuesLowHigh[] =
{
	RESOURCE_STRING(IDS_LOW),
	RESOURCE_STRING(IDS_HIGH),
	NULL
};

static LPTSTR lpszValuesLowMedHigh[]=
{
	RESOURCE_STRING(IDS_LOW),
	RESOURCE_STRING(IDS_MED),
	RESOURCE_STRING(IDS_HIGH),
	NULL
};


static LPTSTR lpszValuesOffOn[] =
{
	RESOURCE_STRING(IDS_OFF),
	RESOURCE_STRING(IDS_ON),
	NULL
};

void SelectContrast(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialContrast(LPITEMSELECTOR lpis);

void SelectBacklight(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialBacklight(LPITEMSELECTOR lpis);

void SelectBacklightState(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialBacklightState(LPITEMSELECTOR lpis);

BOOL RefreshDisplayContrast(LPITEMSELECTOR lpis,DWORD dwSelection);
BOOL RefreshBacklight(LPITEMSELECTOR lpis,DWORD dwSelection);

static LPTSTR GetCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex);

ITEMSELECTOR DisplayParametersSelector[] =
{
	{
		&bContrast16,
		RESOURCE_STRING(IDS_DISPLAYCONTRAST),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to15by1,GetInitialContrast,SelectContrast),
		RefreshDisplayContrast,500
	},

	{
		&bContrast64,
		RESOURCE_STRING(IDS_DISPLAYCONTRAST),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to63by1,GetInitialContrast,SelectContrast),
		RefreshDisplayContrast,500
	},

	{
		&bContrast256,
		RESOURCE_STRING(IDS_DISPLAYCONTRAST),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to255by1,GetInitialContrast,SelectContrast),
		RefreshDisplayContrast,500
	},

	{
		&bBacklightLowHigh,
		RESOURCE_STRING(IDS_BACKLIGHT_INTENSITY),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesLowHigh,GetInitialBacklight,SelectBacklight,NULL)
	},

	{
		&bBacklightLowMedHigh,
		RESOURCE_STRING(IDS_BACKLIGHT_INTENSITY),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesLowMedHigh,GetInitialBacklight,SelectBacklight,NULL)
	},

	{
		&bBacklight0to15by1,
		RESOURCE_STRING(IDS_BACKLIGHT_INTENSITY),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to15by1,GetInitialBacklight,SelectBacklight)
	},

	{
		&bBacklight0to255by1,
		RESOURCE_STRING(IDS_BACKLIGHT_INTENSITY),
		l_lpszHelpFileName,
		STRING_RANGE(lpszRange0to255by1,GetInitialBacklight,SelectBacklight)
	},

	{
		NULL,
		RESOURCE_STRING(IDS_BACKLIGHTONOFF),
		NO_HELP,
		STRING_LIST(lpszValuesOffOn,GetInitialBacklightState,SelectBacklightState,NULL),
		RefreshBacklight,500
	},

	{
		NULL,
		RESOURCE_STRING(IDS_CAPIVERSION),
		NO_HELP,
		GET_STRING(GetCAPIVersion,NULL)
	},
	
	END_ITEMSELECTOR
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef DISPLAY_H_   */
