
//--------------------------------------------------------------------
// FILENAME:        paneldlg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     CtlPanel
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef PANELDLG_H_

#define PANELDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif

// --- End ---

// --- Nested include ---
#include "ctlpanel.h"
#include "values.h"

// Macro defining the state bits for "selected" items
#define UI_SELECTED ( LVNI_SELECTED | LVNI_FOCUSED )

#define REG_HKLM_BASE_NAME TEXT("[HKEY_LOCAL_MACHINE\\")
#define REG_HKCU_BASE_NAME TEXT("[HKEY_CURRENT_USER\\")


typedef HKEY FAR * LPHKEY;


#define Button_Show(hwnd,flag) ShowWindow(hwnd,(flag?SW_SHOW:SW_HIDE))


#ifdef _UUID

extern BOOL l_bUUID0;
extern BOOL l_bUUID1;
extern BOOL l_bUUID2;
extern BOOL l_bUUID3;
extern BOOL l_bUUID4;
extern BOOL l_bUUID5;

#endif


// --- Scanner Parameter ---
#ifdef _SCANCTL
void EnterScannerParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitScannerParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif


// --- Touch Calibrate ---
#ifdef _TOUCH
void EnterTouchCalibrate(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitTouchCalibrate(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Date and Time ---
#ifdef _DATEANDTIME
void EnterDateAndTime(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitDateAndTime(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Display Parameters ---
#ifdef _DISPLAYCTL
void EnterDisplayParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitDisplayParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Audio Parameters ---
#ifdef _AUDIOCTL
void EnterAudioParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitAudioParameters(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Printer ---
#ifdef _PRINTERCTL
void EnterPrinterSetup(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPrinterSetup(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Communication ---
#ifdef _COMMCTL
void EnterCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitCommSettings(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Unit ID ---
#ifdef _UUID
void EnterUnitID(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitUnitID(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Power Management ---
#ifdef _POWERCTL
void EnterPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitPwrMgmt(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Configure S24 ---
#ifdef _S24CTL
void EnterConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitConfigS24(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Memory Adjustment ---
#ifdef _MEMORY_ADJUSTMENT
void EnterMemoryAdjustment(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitMemoryAdjustment(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Cradle ---
#ifdef _CRADLECTL
void EnterCradleSetting(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitCradleSetting(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- System Version ---
#ifdef _SYSVERSION
void EnterSystemVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
void ExitSystemVersion(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- About Box ---
#ifdef _ABOUTCTL
void EnterAbout(LPITEMSELECTOR lpis,DWORD dwSelection);
#endif

// --- Persistence ---
#ifdef _PERSISTCTL
void SelectPersistence(LPITEMSELECTOR lpis,DWORD dwSelection);
DWORD GetInitialPersistence(LPITEMSELECTOR lpis);
#endif


#ifdef _PERSISTCTL
static LPTSTR lpszValuesPersistence[] =
{
	RESOURCE_STRING(IDS_TEMPORARY),
	RESOURCE_STRING(IDS_PERMANENT),
	NULL
};
#endif


ITEMSELECTOR CtlPanelSelector[] =
{
#ifdef _ABOUTCTL
	{
		NULL,
		RESOURCE_STRING(IDS_ABOUT),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterAbout,NULL)
	},
#endif

#ifdef _SYSVERSION
	{
		NULL,
		RESOURCE_STRING(IDS_SYSVERSION),
		NULL,
		STRUCTURE_LIST(EnterSystemVersion,ExitSystemVersion)
	},

#endif

#ifdef _UUID
	{
		NULL,
		RESOURCE_STRING(IDS_UNITID),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterUnitID,ExitUnitID)
	},
#endif

#ifdef _PERSISTCTL
	{
		NULL,
		RESOURCE_STRING(IDS_PERSISTENCE),
		l_lpszHelpFileName,
		STRING_LIST(lpszValuesPersistence,GetInitialPersistence,SelectPersistence,NULL)
	},
#endif

#ifdef _DATEANDTIME
	{
		NULL,
		RESOURCE_STRING(IDS_DATEANDTIME),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterDateAndTime,ExitDateAndTime)
	},
#endif

#ifdef _TOUCH
   	{
		NULL,
		RESOURCE_STRING(IDS_TOUCHCALIBRATE),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterTouchCalibrate,ExitTouchCalibrate)
	},
#endif

#ifdef _MEMORY_ADJUSTMENT
	{
		NULL,
		RESOURCE_STRING(IDS_MEMORYADJUSTMENT),
		NULL,
		STRUCTURE_LIST(EnterMemoryAdjustment,ExitMemoryAdjustment)
	},
#endif

#ifdef _PRINTERCTL
	{
		NULL,
		RESOURCE_STRING(IDS_PRINTER),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPrinterSetup,ExitPrinterSetup)
	},
	
#endif

#ifdef _COMMCTL
	{
		NULL,
		RESOURCE_STRING(IDS_COMMSETTINGS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterCommSettings,ExitCommSettings)
	},
#endif

#ifdef _DISPLAYCTL
	{
		NULL,
		RESOURCE_STRING(IDS_DISPLAYPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterDisplayParameters,ExitDisplayParameters)
	},
#endif

#ifdef _AUDIOCTL
	{
		NULL,
		RESOURCE_STRING(IDS_AUDIOPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterAudioParameters,ExitAudioParameters)
	},
#endif

#ifdef _SCANCTL
	{
		NULL,
		RESOURCE_STRING(IDS_SCANNERPARAMETERS),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterScannerParameters,ExitScannerParameters)
	},
#endif

#ifdef _S24CTL
	{
		NULL,
		RESOURCE_STRING(IDS_CONFIGS24),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterConfigS24,ExitConfigS24)
	},
#endif

#ifdef _POWERCTL
	{
		NULL,
		RESOURCE_STRING(IDS_POWERMGMT),
		l_lpszHelpFileName,
		STRUCTURE_LIST(EnterPwrMgmt,ExitPwrMgmt)
	},
#endif

#ifdef _CRADLECTL
	{
		NULL,
		RESOURCE_STRING(IDS_CRADLESETTING),
		NULL,
		STRUCTURE_LIST(EnterCradleSetting,ExitCradleSetting)
	},
#endif

	END_ITEMSELECTOR
};

#ifdef _UUID
LPTSTR GetUnitID(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID0(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID1(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID2(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID3(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID4(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUnitID5(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetRCMCAPIVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetResCoordVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetUUIDVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
LPTSTR GetTemperatureVersion(LPITEMSELECTOR lpis, DWORD dwIndex);
#endif


#ifdef _UUID

ITEMSELECTOR UnitIDSelector[] =
{
	{
		NULL,
		RESOURCE_STRING(IDS_RCMCAPIVERSION),
		l_lpszHelpFileName,
		GET_STRING(GetRCMCAPIVersion,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_RCMRESCOORDVERSION),
		l_lpszHelpFileName,
		GET_STRING(GetResCoordVersion,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_UUIDVERSION),
		l_lpszHelpFileName,
		GET_STRING(GetUUIDVersion,NULL)
    },
	{
		NULL,
		RESOURCE_STRING(IDS_TEMPVERSION),
		l_lpszHelpFileName,
		GET_STRING(GetTemperatureVersion,NULL)
    },
	{
		&l_bUUID0,
		RESOURCE_STRING(IDS_ID0),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID0,NULL)
    },
	{
		&l_bUUID1,
		RESOURCE_STRING(IDS_ID1),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID1,NULL)
    },
	{
		&l_bUUID2,
		RESOURCE_STRING(IDS_ID2),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID2,NULL)
    },
	{
		&l_bUUID3,
		RESOURCE_STRING(IDS_ID3),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID3,NULL)
    },
	{
		&l_bUUID4,
		RESOURCE_STRING(IDS_ID4),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID4,NULL)
    },
	{
		&l_bUUID5,
		RESOURCE_STRING(IDS_ID5),
		l_lpszHelpFileName,
		GET_STRING(GetUnitID5,NULL)
    },
	END_ITEMSELECTOR
};

#endif


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef PANELDLG_H_  */
