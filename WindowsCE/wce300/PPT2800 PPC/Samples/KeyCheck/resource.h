//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDC_FILE_EXIT                   701
#define IDC_HELP_ABOUT                  702
#define IDI_PROGRAMICON                 703
#define IDM_MAIN                        703
#define IDS_PROGRAMNAME                 704
#define IDS_PROGRAMDESCRIPTION          705
#define IDC_FILE                        8003
#define IDS_CAP_FILE                    8005
#define ID_HELP                         8006
#define IDS_CAP_HELP                    8008
#define IDM_MAIN_POCKET                 8931
#define IDM_POPUP                       8932

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        8933
#define _APS_NEXT_COMMAND_VALUE         8010
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
