
//--------------------------------------------------------------------
// FILENAME:        Scansamp1.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     contains variables and prototypes global to the application
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANSAMP1_H_

#define SCANSAMP1_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("ScanSmp1")
#define APP_TITLE TEXT("Scan Sample 1")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

enum tagUSERMSGS
{
	UM_SCAN = UM_USER,
	UM_EVENT,
};


//----------------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------------

TCHAR l_szFileName[MAX_PATH];

BOOL l_bDisplayEvents;
BOOL l_bLogging;
BOOL l_bHaveCommandLine;


void SetReadType(DWORD dwReadType);


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANSAMP1_H_ */
