//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_PROGRAMNAME                 1001
#define IDS_PROGRAMDESCRIPTION          1002
#define IDC_POPUP                       1003
#define IDC_FILE_EXIT                   1004
#define IDC_HELP_ABOUT                  1005
#define IDI_PROGRAMICON                 1006
#define IDM_MAIN                        1007
#define IDM_POPUP                       1008
#define IDR_ACCELERATOR1                1020
#define IDM_MAIN_POCKET                 1021
#define ID_FILE                         40001
#define IDS_CAP_FILE                    40003
#define IDS_CAP_HELP                    40005
#define ID_JELP                         40006
#define IDC_EVENTS                      40008
#define IDC_CLEAR                       40009
#define IDC_FOREGROUND                  40011
#define IDC_BACKGROUND                  40012
#define IDC_MONITOR                     40013
#define IDC_LOGGING                     40014
#define IDC_HIDE                        40015
#define IDC_QUERY                       40016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1022
#define _APS_NEXT_COMMAND_VALUE         40017
#define _APS_NEXT_CONTROL_VALUE         2001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
