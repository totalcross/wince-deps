//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDC_1                           401
#define IDC_2                           402
#define IDC_3                           403
#define IDC_4                           404
#define IDC_5                           405
#define IDC_6                           406
#define IDC_7                           407
#define IDC_8                           408
#define IDC_9                           409
#define IDC_10                          410
#define IDC_FILE_EXIT                   411
#define IDC_HELP_ABOUT                  412
#define IDC_FILE_OPEN                   413
#define IDC_DARK                        414
#define IDC_LIGHT                       415
#define IDC_LAMP                        416
#define IDI_PROGRAMICON                 418
#define IDI_ICONCALC                    419
#define IDI_ICONINKWIZ                  420
#define IDI_ICONSCANPANL                421
#define IDI_ICONPCCONN                  422
#define IDI_ABOUTICON                   423
#define IDM_MAIN                        425
#define IDA_ACCELERATOR1                426
#define IDI_EXITICON                    427
#define IDS_PROGRAMNAME                 428
#define IDS_PROGRAMDESCRIPTION          429
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        430
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
