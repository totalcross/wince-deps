
//--------------------------------------------------------------------
// FILENAME:            Displaytest.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Test the contrast level supported on terminal
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
// Displaytest.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include"resource.h"
#define MAX_LOADSTRING 100

LRESULT DisplayTestMain(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

// Global Variables:
HINSTANCE			hInst;			// The current instance
HWND				hwndCB;			// The command bar handle

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass	(HINSTANCE hInstance, LPTSTR szWindowClass);
BOOL				InitInstance	(HINSTANCE, int);
LRESULT CALLBACK	WndProc			(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About			(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
{
	MSG msg;

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//

ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
	WNDCLASS	wc;

    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WndProc;
    wc.cbClsExtra		= 0;
    wc.cbWndExtra		= 0;
    wc.hInstance		= hInstance;
    wc.hIcon			= 0;//LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wc.hCursor			= 0;
    wc.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName		= 0;
    wc.lpszClassName	= szWindowClass;

	return RegisterClass(&wc);
}

//
//  FUNCTION: InitInstance(HANDLE, int)
//
//  PURPOSE: Saves instance handle and creates main window
//
//  COMMENTS:
//
//    In this function, we save the instance handle in a global variable and
//    create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND	hWnd;
	TCHAR	szTitle[MAX_LOADSTRING];			// The title bar text
	TCHAR	szWindowClass[MAX_LOADSTRING];		// The window class name

	hInst = hInstance;		// Store instance handle in our global variable
	// Initialize global strings
	LoadString(hInstance, IDC_DISPLAYTEST, szWindowClass, MAX_LOADSTRING);
	
	MyRegisterClass(hInstance, szWindowClass);

	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0, 0, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{	
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	
	PAINTSTRUCT ps;

	switch (message) 
	{
			
		case WM_PAINT:
			BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
			DisplayTestMain(hWnd, message, wParam, lParam);
			break;
		
		case WM_DESTROY:

			PostQuitMessage(0);
			break;
	
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}


		

LRESULT DisplayTestMain(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam){

	RECT rt,rect;
	int i;
	TCHAR errmsg[128];
	HDC hdc;
	HBRUSH hBr,hOldBr;

//do top half first	

//BLACK
			
			SystemParametersInfo(SPI_GETWORKAREA,0,(PVOID)&rect,0);

	        hdc=GetDC(hWnd);
			hBr = GetStockObject(4);
			//store so it can be restored
			hOldBr = SelectObject(hdc,hBr);
			
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom/2);
			//tell user color
			rt.top=rect.bottom/2;
			rt.left=rect.left;
			rt.bottom=rect.bottom;
			rt.right=rect.right;
		
			wsprintf(errmsg,TEXT("The color above has changed to %d"),3); 
			DrawText(hdc, errmsg, _tcslen(errmsg), &rt, DT_CENTER);
			Sleep(2000);

			//not change the colors every 2 seconds
			for (i=2;i>=0;i--)
			{
				
			hBr = GetStockObject(i);
			SelectObject(hdc,hBr);
			Rectangle(hdc, rect.left, rect.top, rect.right,rect.bottom/2);
			//tell user color
			rt.top=rect.bottom/2;
			rt.left=rect.left;
			rt.bottom=rect.bottom;
			rt.right=rect.right;
	
			wsprintf(errmsg,TEXT("The color above has changed to %d"),i);
			DrawText(hdc, errmsg, _tcslen(errmsg), &rt, DT_CENTER); 
		
			Sleep (2000);

			}
			
			//now do bottom
			//BLACK
			hBr = GetStockObject(4);
			SelectObject(hdc,hBr);
			Rectangle(hdc,rect.left, rect.bottom/2,rect.right,rect.bottom);
			
			//tell user color
			rt.top=rect.bottom/2 - 20;
			rt.left=0;
			rt.bottom=rect.bottom/2;
			rt.right=rect.right;
		
			wsprintf(errmsg,TEXT("The color below has changed to %d"),3);//set it to 3 because really goes 4-2-1-0 not 3-2-1-0
			DrawText(hdc, errmsg, _tcslen(errmsg), &rt, DT_CENTER);
			Sleep(2000);

			//now change the colors every 2 seconds
			for (i=2;i>=0;i--)
			{
						
			hBr = GetStockObject(i);
			SelectObject(hdc,hBr);
			Rectangle(hdc,rect.left,rect.bottom/2,rect.right,rect.bottom);
			
			rt.top=rect.bottom/2 - 20;
			rt.left=rect.left;
			rt.bottom=rect.bottom/2;
			rt.right=rect.right;
		
			wsprintf(errmsg,TEXT("The color below has changed to %d"),i);
			DrawText(hdc, errmsg, _tcslen(errmsg), &rt, DT_CENTER); 
		
			Sleep (2000);

			}
			
			//reset the drawing object and end paint
			SelectObject(hdc,hOldBr);
//automatically exit test
		DestroyWindow(hWnd);
	return 0;
}
