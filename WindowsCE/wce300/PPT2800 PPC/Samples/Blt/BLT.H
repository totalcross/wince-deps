
//--------------------------------------------------------------------
// FILENAME:            BLT.H
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:         Header file for BLT.C
//
// NOTES:               Public
//
// 
//--------------------------------------------------------------------

#ifndef BLT_H_

#define BLT_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "..\common\regquery.h"

#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("ButtonLaunchTask")
#define APP_TITLE TEXT("Button Launch Task")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

#define BITMAP_WIDTH    16
#define BITMAP_HEIGHT   16
#define LG_BITMAP_WIDTH	32
#define LG_BITMAP_HEIGHT 32


//----------------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------------

int gButtonCount;


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef BLT_H_   */

