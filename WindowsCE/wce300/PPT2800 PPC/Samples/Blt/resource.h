//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_PROGRAMNAME                 1301
#define IDS_PROGRAMDESCRIPTION          1302
#define IDC_1                           1303
#define IDC_2                           1304
#define IDC_3                           1305
#define IDC_4                           1306
#define IDC_5                           1307
#define IDC_6                           1308
#define IDC_7                           1309
#define IDC_8                           1310
#define IDC_9                           1311
#define IDC_10                          1312
#define IDC_HELP_ABOUT                  1314
#define IDC_FILE_OPEN                   1315
#define IDC_DARK                        1316
#define IDC_LIGHT                       1317
#define IDC_LAMP                        1318
#define IDI_PROGRAMICON                 1320
#define IDI_ICONCALC                    1321
#define IDI_ICONINKWIZ                  1322
#define IDI_ICONSCANPANL                1323
#define IDI_ICONPCCONN                  1324
#define IDI_ICONABOUT                   1325
#define IDI_ABOUTICON                   1325
#define IDM_MAIN                        1326
#define IDA_ACCELERATOR1                1327
#define IDI_EXITICON                    1328
#define IDI_ICON2                       1341
#define IDI_ICON1                       1343
#define IDI_SETUP                       1344

#define IDI_CEAPPS                      1346
#define IDI_IOCTL                       1347
#define IDI_DIAG                        1348
#define IDI_DIAG2                       1349
#define IDI_KEYBD                       1350
#define IDI_LED                         1351
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        1352
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
