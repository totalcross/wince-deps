
//--------------------------------------------------------------------
// FILENAME:            paneldlg.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Contains notify panel dialog functions
//
// PLATFORMS:       Windows CE
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "..\common\StdGlobs.h"

#include "..\common\StdWproc.h"
#include "..\common\StdStrng.h"
#include "..\common\StdFuncs.h"
#include "..\common\StdMsg.h"
#include "..\common\StdScrns.h"
#include "..\common\StdAbout.h"

#include "Notify.h"


#define Button_Show(hwnd,flag) ShowWindow(hwnd,(flag?SW_SHOW:SW_HIDE))


static HWND l_hWndList;

static TCHAR l_szLastItem[MAX_PATH];
 
static LPTSTR lpszNotifyObject[] =
{
    TEXT("LED"),
    TEXT("BEEPER"),
    TEXT("VIBRATOR"),
    NULL
};

static LPTSTR lpszState[] =
{
    TEXT("OFF"),
    TEXT("ON"),
    TEXT("CYCLE"),
    NULL
};


static FEEDBACK_INFO FeedbackInfo;

static int nBeeperFrequencyMinimum;
static int nBeeperFrequencyMaximum;
static int nBeeperFrequencyStep;
static int nBeeperDurationMinimum;
static int nBeeperDurationMaximum;
static int nBeeperDurationStep;
static int nBeeperVolumeMinimum;
static int nBeeperVolumeMaximum;
static int nBeeperVolumeStep;
static int nLedDurationMinimum;
static int nLedDurationMaximum;
static int nLedDurationStep;
static int nLedCountMinimum;
static int nLedCountMaximum;
static int nLedCountStep;
static int nVibratorDurationMinimum;
static int nVibratorDurationMaximum;
static int nVibratorDurationStep;


// Get the currently selected item index of a list view

int ListView_GetSelectedItem(HWND hWndList)
{
	int iItem;
	int nState;
	
	iItem = ListView_GetNextItem(hWndList,-1,UI_SELECTED);

	nState = ListView_GetItemState(hWndList,iItem,UI_SELECTED);

	return(iItem);
}


// Get the currently selected item parameter of a list view

LPARAM ListView_GetSelectedParam(HWND hWndList)
{
	int iItem;
	LV_ITEM lvI;

	iItem = ListView_GetSelectedItem(hWndList);

	lvI.mask = LVIF_PARAM;
	lvI.iItem = iItem;
	lvI.iSubItem = 0;
	ListView_GetItem(hWndList,&lvI);

	return(lvI.lParam);
}


// Get the currently selected item text of a list view

void ListView_GetSelectedText(HWND hWndList,
							  LPTSTR lpszText,
							  DWORD dwSize)
{
	int iItem;

	iItem = ListView_GetSelectedItem(hWndList);

	ListView_GetItemText(hWndList,iItem,0,lpszText,dwSize);
}


// Select an item in a list view

void ListView_SelectItem(HWND hWndList,
						 int iItem)
{
	SetFocus(hWndList);

	ListView_EnsureVisible(hWndList,iItem,FALSE);
	
	ListView_SetItemState(hWndList,
						  iItem,
						  LVIS_SELECTED|LVIS_FOCUSED,
						  LVIS_SELECTED|LVIS_FOCUSED);
	
	ListView_Update(hWndList,iItem);
}


LPTSTR FetchStringResource(LPTSTR lpszString)
{
	static TCHAR szString[MAX_PATH];
	DWORD dwId = (DWORD)lpszString;

	if ( HIWORD(dwId) == 0 )
	{
		LoadString(g_hInstance,dwId,szString,TEXTSIZEOF(szString));
		return(szString);
	}
	else
	{
		return(lpszString);
	}
}


#define MAX_NOTIFY_OBJECTS 10

static NOTIFY_FINDINFO NotifyObjects[MAX_NOTIFY_OBJECTS];
static int nNotifyCount = 0;


int GetObjectIndex(VOID)
{
	int iItem;
	
	iItem = ListView_GetNextItem(l_hWndList,-1,UI_SELECTED);

	return(iItem);
}


BOOL IsValidDword(LPTSTR lpszText)
{
	int nLen;
	int i;

	if ( lpszText == NULL )
	{
		return(FALSE);
	}
	
	nLen = TEXTLEN(lpszText);

	if ( nLen == 0 )
	{
		return(FALSE);
	}

	for(i=0;i<nLen;i++)
	{
		TCHAR c = lpszText[i];
		if ( ( c < TEXT('0') ) || ( c > TEXT('9') ) )
		{
			return(FALSE);
		}
	}

	return(TRUE);
}


BOOL Validate(HWND hWndEdit,
			  LPDWORD lpdwValue,
			  DWORD dwMinValue,
			  DWORD dwMaxValue,
              LPTSTR lpszField)
{
	DWORD dwValue;
	TCHAR szText[MAX_PATH];
	BOOL bSuccess = TRUE;

	Edit_GetText(hWndEdit,szText,TEXTSIZEOF(szText));

	bSuccess = IsValidDword(szText);

    // display error message and return right away if 
    //  it's not a valid DWORD string
    if (!bSuccess)
    {
        MyError(lpszField,TEXT("Not a valid DWORD"));
		SetFocus(hWndEdit);
		Edit_SetSel(hWndEdit,0,-1);
        return (bSuccess);
    }

	dwValue = TEXTTOL(szText);
    
	bSuccess = ( ( dwValue >= dwMinValue ) &&
				 ( dwValue <= dwMaxValue ) );

	if ( bSuccess ) // value OK
	{
		if ( lpdwValue != NULL )
		{
			*lpdwValue = dwValue;
		}
	}
	else    // Error
	{
		TCHAR szMsg1[MAX_PATH];
		TCHAR szMsg2[MAX_PATH];
        TCHAR szMsg3[MAX_PATH];
		
        LoadString(g_hInstance,IDS_BADRANGE,szMsg1,TEXTSIZEOF(szMsg1));
        
        TEXTCPY(szMsg3,lpszField);
        TEXTCAT(szMsg3, TEXT(" - "));
        TEXTCAT(szMsg3,szMsg1);

		TEXTSPRINTF(szMsg2,TEXT("%ld to %ld"),dwMinValue,dwMaxValue);
		MyError(szMsg3,szMsg2);

		SetFocus(hWndEdit);
		Edit_SetSel(hWndEdit,0,-1);
	}

	return(bSuccess);
}


static HWND hWndText1,hWndText2,hWndText3;
static HWND hWndEdit1,hWndEdit2,hWndEdit3;
static HWND hWndBar1,hWndBar2,hWndBar3;


int AdjustToRange(int nPos,
				  int nMin,
				  int nMax,
				  int nStep)
{
	int nRemainder;

	if ( nPos < nMin )
	{
		nPos = nMin;
	}

	if ( nStep > 0 )
	{
		nRemainder = ( nPos - nMin ) % nStep;

		if ( nRemainder > 0 )
		{
			nPos -= nRemainder;

			if ( nRemainder > nStep/2 )
			{
				nPos += nStep;
			}
		}
	}

	if ( nPos > nMax )
	{
		nPos = nMax;
	}

	return(nPos);
}


void InitEditScrollBar(HWND hwnd,
					   int nMin,
					   int nMax,
					   int nStep,
					   LPINT lpnPos)
{
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };
	int nPos = *lpnPos;

	si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;

	*lpnPos = nPos = AdjustToRange(nPos,nMin,nMax,nStep);

	si.nMin = nMin;
	si.nPage = nStep;
    si.nMax = nMax + si.nPage - 1;
	si.nPos = nPos;

	SetScrollInfo(hwnd,SB_CTL,&si,TRUE);
}


void HandleEditScrollBars(HWND hwnd,
						  WPARAM wParam,
						  LPARAM lParam,
                          DWORD dwIndex)
{
	HWND hSB = (HWND)lParam;
	HWND hED;
	int nPos = -1;
    int nMax;
	int nMin;
	int nStep;
	SCROLLINFO si = { sizeof(SCROLLINFO),SIF_ALL };

	if ( hSB == hWndBar1 )  
	{
		hED = hWndEdit1;
	}
	else if ( hSB == hWndBar2 ) 
	{
		hED = hWndEdit2;
	}
	else if ( hSB == hWndBar3 ) 
	{
		hED = hWndEdit3;
	}
	else
	{
		return;
	}
      
	GetScrollInfo(hSB,SB_CTL,&si);

    nMin = si.nMin;
	nStep = si.nPage;
	nMax = si.nMax - nStep + 1;

    switch(LOWORD(wParam))
	{
		case SB_LINELEFT:
		case SB_LINERIGHT:
		case SB_PAGELEFT:
		case SB_PAGERIGHT:
			
			switch(LOWORD(wParam))
			{
				case SB_LINELEFT:
				case SB_PAGELEFT:
					
					nPos = si.nPos - nStep;
					
					break;
				
				case SB_LINERIGHT:
				case SB_PAGERIGHT:
					
					nPos = si.nPos + nStep;
                    
					break;
			}

			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:

			nPos = si.nTrackPos + nMin;
			
			break;
					
		case SB_ENDSCROLL:

			nPos = si.nPos;

			break;
	}

	if ( nPos != -1 )
	{
		TCHAR szCurText[MAX_PATH];
		TCHAR szNewText[MAX_PATH];

		nPos = AdjustToRange(nPos,nMin,nMax,nStep);

		si.fMask = SIF_POS;
		si.nPos = nPos;
		SetScrollInfo(hSB,SB_CTL,&si,FALSE);
							
		Edit_GetText(hED,szCurText,TEXTSIZEOF(szCurText));

		TEXTSPRINTF(szNewText,TEXT("%d"),nPos);

		if ( TEXTCMP(szNewText,szCurText) != 0 )   // update editbox
		{
			Edit_SetText(hED,szNewText);

			Edit_SetSel(hED,0,-1);
			
			SetFocus(hED);
		}
	}

}


BOOL SetupFeedbackInformation(void)
{
	nBeeperFrequencyMinimum		= 2500;
	nBeeperFrequencyMaximum		= 3500;
	nBeeperFrequencyStep		= 10;

	nBeeperDurationMinimum		= 0;
	nBeeperDurationMaximum		= 5000;
	nBeeperDurationStep			= 100;

	nBeeperVolumeMinimum		= 0;
	nBeeperVolumeMaximum		= 0;
	nBeeperVolumeStep			= 1;

	nLedDurationMinimum			= 0;
	nLedDurationMaximum			= 5000;
	nLedDurationStep			= 500;

	nLedCountMinimum			= 0;
	nLedCountMaximum			= 20;
	nLedCountStep				= 2;

	nVibratorDurationMinimum	= 0;
	nVibratorDurationMaximum	= 5000;
	nVibratorDurationStep		= 500;

	SI_INIT(&FeedbackInfo);

	if ( !GetFeedbackInfo(&FeedbackInfo) )
	{
		return(FALSE);
	}

	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMinimum,nBeeperFrequencyMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyMaximum,nBeeperFrequencyMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperFrequencyStep,nBeeperFrequencyStep);

	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMinimum,nBeeperDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationMaximum,nBeeperDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperDurationStep,nBeeperDurationStep);

	SI_GET_FIELD(&FeedbackInfo,nBeeperVolumeMinimum,nBeeperVolumeMinimum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperVolumeMaximum,nBeeperVolumeMaximum);
	SI_GET_FIELD(&FeedbackInfo,nBeeperVolumeStep,nBeeperVolumeStep);

	SI_GET_FIELD(&FeedbackInfo,nLedDurationMinimum,nLedDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationMaximum,nLedDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nLedDurationStep,nLedDurationStep);

	SI_GET_FIELD(&FeedbackInfo,nLedCountMinimum,nLedCountMinimum);
	SI_GET_FIELD(&FeedbackInfo,nLedCountMaximum,nLedCountMaximum);
	SI_GET_FIELD(&FeedbackInfo,nLedCountStep,nLedCountStep);

	SI_GET_FIELD(&FeedbackInfo,nVibratorDurationMinimum,nVibratorDurationMinimum);
	SI_GET_FIELD(&FeedbackInfo,nVibratorDurationMaximum,nVibratorDurationMaximum);
	SI_GET_FIELD(&FeedbackInfo,nVibratorDurationStep,nVibratorDurationStep);

	return(TRUE);
}


//----------------------------------------------------------------------------
// CycleEditDlgProc
//----------------------------------------------------------------------------

static LRESULT CALLBACK CycleEditDlgProc(HWND hwnd,
										 UINT uMsg,
										 WPARAM wParam,
										 LPARAM lParam)
{
    static HWND hLastEditCtl = 0;
	static DWORD dwIndex;
	static CYCLE_INFO CycleInfo;

	TCHAR szText[MAX_PATH];
	DWORD dwResult;
	BOOL bSuccess;
	BOOL bSuccess1;
	BOOL bSuccess2;
	BOOL bSuccess3;

	switch(uMsg)
    {
		// When focus is set to dialog
		case WM_SETFOCUS:

			// Return TRUE since message was processed
			return(TRUE);

        case WM_HSCROLL:
			
			HandleEditScrollBars(hwnd,wParam,lParam,dwIndex);
			
			break;

		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			{
				DWORD dwResult;
				DWORD dwObjectType = NOTIFY_TYPE_UNKNOWN;
				
				dwIndex = (DWORD)lParam;

				SetupFeedbackInformation();

				SI_INIT(&CycleInfo);

				dwResult = NOTIFY_GetCycleInfo(dwIndex,&CycleInfo);

				if ( dwResult != E_NTFY_SUCCESS )
				{
					ReportError(TEXT("NOTIFY_GetCycleInfo"),dwResult);
					
					PostMessage(hwnd,WM_COMMAND,IDCANCEL,0);

					break;
				}

				MaxDialog(hwnd);
				
				hWndText1 = GetDlgItem(hwnd,IDC_CYCLE_TEXT1);
				hWndText2 = GetDlgItem(hwnd,IDC_CYCLE_TEXT2);
				hWndText3 = GetDlgItem(hwnd,IDC_CYCLE_TEXT3);

				hWndEdit1 = GetDlgItem(hwnd,IDC_CYCLE_EDIT1);
				hWndEdit2 = GetDlgItem(hwnd,IDC_CYCLE_EDIT2);
				hWndEdit3 = GetDlgItem(hwnd,IDC_CYCLE_EDIT3);
				
				hWndBar1 = GetDlgItem(hwnd,IDC_SCROLLBAR1);
				hWndBar2 = GetDlgItem(hwnd,IDC_SCROLLBAR2);
				hWndBar3 = GetDlgItem(hwnd,IDC_SCROLLBAR3);

				// Get object type from structure
				SI_GET_FIELD(&CycleInfo,dwObjectType,dwObjectType);
				
				// Select interpretation based on object type
				switch(dwObjectType)
				{
					case NOTIFY_TYPE_LED:

						// Window title
						LoadString(g_hInstance,
								   IDS_LED_CYCLE_INFO,
								   szText,
								   TEXTSIZEOF(szText));
						SetWindowTitle(hwnd,szText);

						// On duration
						LoadString(g_hInstance,
								   IDS_LED_CYCLE_ONDURATION,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText1,szText);
						InitEditScrollBar(hWndBar1,
										  nLedDurationMinimum,
										  nLedDurationMaximum,
										  nLedDurationStep,
										  &CycleInfo.ObjectTypeSpecific.LedSpecific.dwOnDuration);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.LedSpecific.dwOnDuration);
						Edit_SetText(hWndEdit1,szText);

						// Off duration
						LoadString(g_hInstance,
								   IDS_LED_CYCLE_OFFDURATION,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText2,szText);
						InitEditScrollBar(hWndBar2,
										  nLedDurationMinimum,
										  nLedDurationMaximum,
										  nLedDurationStep,
										  &CycleInfo.ObjectTypeSpecific.LedSpecific.dwOffDuration);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.LedSpecific.dwOffDuration);
						Edit_SetText(hWndEdit2,szText);

						// Cycle count
						LoadString(g_hInstance,
								   IDS_LED_CYCLE_COUNT,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText3,szText);
						InitEditScrollBar(hWndBar3,
										  nLedCountMinimum,
										  nLedCountMaximum,
										  nLedCountStep,
										  &CycleInfo.ObjectTypeSpecific.LedSpecific.dwCount);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.LedSpecific.dwCount);
						Edit_SetText(hWndEdit3,szText);

						hLastEditCtl = hWndEdit3;
						
                        SetFocus(hWndEdit1);
						Edit_SetSel(hWndEdit1,0,-1);

						break;

					case NOTIFY_TYPE_BEEPER:

						// Window title
						LoadString(g_hInstance,
								   IDS_BEEPER_CYCLE_INFO,
								   szText,
								   TEXTSIZEOF(szText));
						SetWindowTitle(hwnd,szText);

						// Frequency
						LoadString(g_hInstance,
								   IDS_BEEPER_CYCLE_FREQUENCY,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText1,szText);
						InitEditScrollBar(hWndBar1,
										  nBeeperFrequencyMinimum,
										  nBeeperFrequencyMaximum,
										  nBeeperFrequencyStep,
										  &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwFrequency);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwFrequency);
						Edit_SetText(hWndEdit1,szText);

						// Volume
						LoadString(g_hInstance,
								   IDS_BEEPER_CYCLE_VOLUME,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText2,szText);
						InitEditScrollBar(hWndBar2,
										  nBeeperVolumeMinimum,
										  nBeeperVolumeMaximum,
										  nBeeperVolumeStep,
										  &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwVolume);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwVolume);
						Edit_SetText(hWndEdit2,szText);

						// Duration
						LoadString(g_hInstance,
								   IDS_BEEPER_CYCLE_DURATION,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText3,szText);
						InitEditScrollBar(hWndBar3,
										  nBeeperDurationMinimum,
										  nBeeperDurationMaximum,
										  nBeeperDurationStep,
										  &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwDuration);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwDuration);
						Edit_SetText(hWndEdit3,szText);

						hLastEditCtl = hWndEdit3;

						SetFocus(hWndEdit1);
						Edit_SetSel(hWndEdit1,0,-1);

						break;

					case NOTIFY_TYPE_VIBRATOR:

						// Window title
						LoadString(g_hInstance,
								   IDS_VIBRATOR_CYCLE_INFO,
								   szText,
								   TEXTSIZEOF(szText));
						SetWindowTitle(hwnd,szText);
						
						// Duration
						LoadString(g_hInstance,
								   IDS_VIBRATOR_CYCLE_DURATION,
								   szText,
								   TEXTSIZEOF(szText));
						Edit_SetText(hWndText1,szText);
						InitEditScrollBar(hWndBar1,
										  nVibratorDurationMinimum,
										  nVibratorDurationMaximum,
										  nVibratorDurationStep,
										  &CycleInfo.ObjectTypeSpecific.VibratorSpecific.dwDuration);
						TEXTSPRINTF(szText,TEXT("%ld"),CycleInfo.ObjectTypeSpecific.VibratorSpecific.dwDuration);
						Edit_SetText(hWndEdit1,szText);

						ShowWindow(hWndText2,SW_HIDE);
						ShowWindow(hWndEdit2,SW_HIDE);
						ShowWindow(hWndBar2,SW_HIDE);

						ShowWindow(hWndText3,SW_HIDE);
						ShowWindow(hWndEdit3,SW_HIDE);
						ShowWindow(hWndBar3,SW_HIDE);

						hLastEditCtl = hWndEdit1;

						SetFocus(hWndEdit1);
						Edit_SetSel(hWndEdit1,0,-1);

						break;

					default:

						ReportError(TEXT("Unknown Object Type"),dwObjectType);
					
						PostMessage(hwnd,WM_COMMAND,IDCANCEL,0);
						
						break;
				}

			}
	
			// Return FALSE since focus was manually set
			return(FALSE);

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_POPUP:		 
				case IDC_VALIDATE:
                case IDC_ACCEPT:
			
					bSuccess = bSuccess1 = bSuccess2 = bSuccess3 = TRUE;
					
					switch(CycleInfo.dwObjectType)
					{
						case NOTIFY_TYPE_LED:
							
							// On duration
							bSuccess1 = Validate(hWndEdit1,
												 &CycleInfo.ObjectTypeSpecific.LedSpecific.dwOnDuration,
												 nLedDurationMinimum,
												 nLedDurationMaximum,
												 TEXT("On Duration"));
							// Off duration
							bSuccess2 = Validate(hWndEdit2,
												 &CycleInfo.ObjectTypeSpecific.LedSpecific.dwOffDuration,
												 nLedDurationMinimum,
												 nLedDurationMaximum,
												 TEXT("Off Duration"));

							// Cycle count
							bSuccess3 = Validate(hWndEdit3,
											     &CycleInfo.ObjectTypeSpecific.LedSpecific.dwCount,
												 nLedCountMinimum,
												 nLedCountMaximum,
												 TEXT("Count"));
                            
   							break;

						case NOTIFY_TYPE_BEEPER:

							// Frequency
							bSuccess1 = Validate(hWndEdit1,
												 &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwFrequency,
												 nBeeperFrequencyMinimum,
												 nBeeperFrequencyMaximum,
												 TEXT("Frequency"));

							// Volume
							bSuccess2 = Validate(hWndEdit2,
												 &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwVolume,
												 nBeeperVolumeMinimum,
												 nBeeperVolumeMaximum,
												 TEXT("Volume"));

							// Duration
							bSuccess3 = Validate(hWndEdit3,
												 &CycleInfo.ObjectTypeSpecific.BeeperSpecific.dwDuration,
												 nBeeperDurationMinimum,
												 nBeeperDurationMaximum,
												 TEXT("Duration"));

							break;

						case NOTIFY_TYPE_VIBRATOR:

							// Duration
							bSuccess1 = Validate(hWndEdit1,
												 &CycleInfo.ObjectTypeSpecific.VibratorSpecific.dwDuration,
												 nVibratorDurationMinimum,
												 nVibratorDurationMaximum,
												 TEXT("Duration"));

							break;
					}
					
					bSuccess = ( bSuccess1 && bSuccess2 && bSuccess3 );

                    if ( bSuccess )
					{
						switch(LOWORD(wParam))
						{
							case IDC_ACCEPT:
								PostMessage(hwnd,WM_COMMAND,IDOK,0);
								break;
							case IDC_POPUP:
								if ( GetFocus() == hLastEditCtl )   // if we pass the last one
								{
									PostMessage(hwnd,WM_COMMAND,IDOK,0);
								}
								else
								{
									keybd_event(VK_TAB,0,0,0);
									keybd_event(VK_TAB,0,KEYEVENTF_KEYUP,0);
								}
								break;
						}
					}
                        
					break;
				
				case IDOK:
				
					dwResult = NOTIFY_SetCycleInfo(dwIndex,&CycleInfo);
			
					// If it failed
					if ( dwResult != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetCycleInfo"),dwResult);
						SetFocus(hWndEdit1);
						Edit_SetSel(hWndEdit1,0,-1);
						break;
					}
					// Otherwise fall through to exit dialog

				case IDCANCEL:
					//Turns off LED
					
					dwResult = NOTIFY_SetState(GetObjectIndex(),NOTIFY_STATE_OFF);
					if ( dwResult  != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetState"),dwResult);
					}

					// Exit the dialog
					ExitDialog();

					break;

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


DIALOG_CHOICE CycleEditDialogChoices[] =
{
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_CYCLE_EDIT_PPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_CYCLE_EDIT_PPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_CYCLE_EDIT_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_CYCLE_EDIT_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_CYCLE_EDIT_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_CYCLE_EDIT_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_CYCLE_EDIT_7500_TSK,	NULL,	UI_STYLE_DEFAULT }
};


VOID EditCycleInfo(VOID)
{
	g_hdlg = CreateChosenDialog(g_hInstance,
								CycleEditDialogChoices,
								g_hwnd,
								CycleEditDlgProc,
								(LPARAM)GetObjectIndex());

	WaitUntilDialogDoneSkip(FALSE);
}


static VOID InitializeListView(HWND hdlg,
							   HWND hWndList)
{
	LV_COLUMN lvC;
	DWORD dwResult = E_NTFY_SUCCESS;
	HANDLE hFindHandle;
	int i;
	RECT rect;
	int nWidth;
	HFONT hNewFont;

#ifdef WIN32_PLATFORM_POCKETPC
	{
		DWORD dwStyle;
	
		dwStyle = (DWORD)GetWindowLong(hWndList,GWL_STYLE);
		SetWindowLong(hWndList,GWL_STYLE,(LONG)(dwStyle|LVS_NOCOLUMNHEADER));
		UpdateWindow(hWndList);
	}
#endif

	hNewFont = SetNewWindowFont(hWndList,140);

	SetNewWindowFont(GetDlgItem(hdlg,IDC_CYCLE_EDIT),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_STATE_ON),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_STATE_OFF),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_STATE_CYCLE),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_FILE_EXIT),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_GET_STATE),0);
	SetNewWindowFont(GetDlgItem(hdlg,IDC_HELP_ABOUT),0);

	GetClientRect(hWndList,&rect);
	nWidth = rect.right-rect.left;

	for(nNotifyCount=0;nNotifyCount<MAX_NOTIFY_OBJECTS;nNotifyCount++)
	{
		SI_ALLOC_ALL(&NotifyObjects[nNotifyCount]);
		NotifyObjects[nNotifyCount].StructInfo.dwUsed = 0;

		if ( nNotifyCount == 0 )
		{
			dwResult = NOTIFY_FindFirst(&NotifyObjects[nNotifyCount],
										&hFindHandle);
		}
		else
		{
			dwResult = NOTIFY_FindNext(&NotifyObjects[nNotifyCount],
									   hFindHandle);
		}

		if ( dwResult != E_NTFY_SUCCESS )
		{
			break;
		}
	}
	
	NOTIFY_FindClose(hFindHandle);
	
	{
		HDC hdc;
		HFONT hOldFont;
		SIZE size;
		int nHeight;
		int nLines;
		
		hdc = GetDC(hWndList);
		hOldFont = SelectObject(hdc,hNewFont);

		GetTextExtentPoint32(hdc,TEXT("A"),1,&size);

		nHeight = rect.bottom-rect.top;
		nLines = nHeight / size.cy;

		if ( nLines < nNotifyCount )
		{
			nWidth -= GetSystemMetrics(SM_CXVSCROLL)+1;
		}
	}

	lvC.mask = LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM | LVCF_TEXT;
	lvC.fmt = LVCFMT_LEFT;

	lvC.iSubItem = 0;
	lvC.pszText = FetchStringResource(RESOURCE_STRING(IDS_OBJECTNAME));
	lvC.cx = nWidth;
	ListView_InsertColumn(hWndList,0,&lvC);

	for(i=0;i<nNotifyCount;i++)
	{
		LV_ITEM lvI;

		lvI.mask = LVIF_TEXT;
		lvI.iItem = i;
		lvI.iSubItem = 0;
		lvI.pszText = NotifyObjects[i].szObjectName;
		lvI.cchTextMax = TEXTLEN(lvI.pszText);
		ListView_InsertItem(hWndList,&lvI);
	}

	// Set focus to list view and its first item
	ListView_SelectItem(hWndList,0);            
}


//----------------------------------------------------------------------------
// NotifyPanelDlgProc
//----------------------------------------------------------------------------

static LRESULT CALLBACK NotifyPanelDlgProc(HWND hwnd,
										   UINT uMsg,
										   WPARAM wParam,
										   LPARAM lParam)
{
	DWORD dwResult;

    switch(uMsg)
    {
   		HANDLE_MSG(hwnd, WM_WINDOWPOSCHANGED,	Std_NoMove);

		case UM_RESIZE:
			
			// Resize this dialog, no save information required
			ResizeDialog(NULL);
			
			break;

		// When focus is set to dialog
		case WM_SETFOCUS:
			
			// Set focus to the selected list view item
			ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));
			
			// Return TRUE since message was processed
			return(TRUE);

		// WM_INITDIALOG called when dialog box is first initialized
		case WM_INITDIALOG:

			l_hWndList = GetDlgItem(hwnd,IDC_OBJECTS);

			MaxDialog(hwnd);

			InitializeListView(hwnd,l_hWndList);
			
			SetWindowTitle(hwnd,l_szLastItem);
	
			// Return FALSE since focus was manually set
			return(FALSE);

		case WM_NOTIFY:
			{
				LV_DISPINFO *pLvdi = (LV_DISPINFO *)lParam;
				
				switch(pLvdi->hdr.code)
				{
					case NM_CLICK:
					case NM_DBLCLK:
						{
							DWORD dwPos;
							POINT point;
							LV_HITTESTINFO lvhti;
							int htiItemClicked;

							// Find out where the cursor was
							dwPos = GetMessagePos();
							point.x = LOWORD(dwPos);
							point.y = HIWORD(dwPos);

							// Convert to client coordinates
							ScreenToClient(l_hWndList,&point);

							// Determine the item clicked
							lvhti.pt = point;
							htiItemClicked = ListView_HitTest(l_hWndList,&lvhti);

							// If the hit is not on an item
							if ( (lvhti.flags & LVHT_ONITEM) == 0 )
							{
								break;
							}

							switch(GetUIStyle())
							{
								case UI_STYLE_FINGER:
								case UI_STYLE_PEN:

									ListView_SelectItem(l_hWndList,ListView_GetSelectedItem(l_hWndList));

									break;

								case UI_STYLE_KEYPAD:
									
									PostMessage(hwnd,WM_COMMAND,IDC_POPUP,0);
									
									break;								
							}
						}
						break;

						// Fall through

					case LVN_ITEMACTIVATE:
						
						switch(GetUIStyle())
						{
							case UI_STYLE_KEYPAD:

								PostMessage(hwnd,WM_COMMAND,IDC_POPUP,0);
								
								break;
							
							case UI_STYLE_FINGER:
							case UI_STYLE_PEN:
								
								break;
						}

						break;
				}
			}
			break;

       case WM_COMMAND:
            
			// Select acitivty based on command
			switch (LOWORD(wParam))
            {
				case IDC_HELP_ABOUT:

					About(hwnd,
						  IDS_PROGRAMNAME,
						  IDS_PROGRAMDESCRIPTION,
						  IDI_PROGRAMICON);

					break;

				case IDC_POPUP:

					switch(GetUIStyle())
					{
						case UI_STYLE_KEYPAD:

							InvokePopUpMenu(hwnd,IDM_POPUP,0,0,0);
							
							break;

						case UI_STYLE_FINGER:
						case UI_STYLE_PEN:

							// Commented to not take action on Action/Enter key : Suneet 08/25/1999							
							SendMessage(hwnd,WM_COMMAND,IDOK,0L);

							break;
					}

					break;
				
				case IDOK:

					// Fall through

				case IDC_FILE_EXIT:
				case IDCANCEL:
					
					dwResult = NOTIFY_SetState(GetObjectIndex(),NOTIFY_STATE_OFF);
					if ( dwResult  != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetState"),dwResult);
					}

					// Exit the dialog
					ExitDialog();

					break;

				case IDC_STATE_OFF:
					
					dwResult = NOTIFY_SetState(GetObjectIndex(),NOTIFY_STATE_OFF);
					if ( dwResult  != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetState"),dwResult);
					}
					break;

				case IDC_STATE_ON:
					
					dwResult = NOTIFY_SetState(GetObjectIndex(),NOTIFY_STATE_ON);
					if ( dwResult != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetState"),dwResult);
					}
					break;

				case IDC_STATE_CYCLE:

					dwResult = NOTIFY_SetState(GetObjectIndex(),NOTIFY_STATE_CYCLE);

					if ( dwResult != E_NTFY_SUCCESS )
					{
						ReportError(TEXT("NOTIFY_SetState"),dwResult);
					}
					break;

				case IDC_GET_STATE:
					{
						DWORD dwIndex;
						DWORD dwState;
						TCHAR szMsg1[MAX_PATH];
						TCHAR szMsg2[MAX_PATH];

						dwIndex = GetObjectIndex();
						dwResult = NOTIFY_GetState(dwIndex,&dwState);
						if ( dwResult != E_NTFY_SUCCESS )
						{
							ReportError(TEXT("NOTIFY_GetState"),dwResult);
						}
						else
						{
							
							// Changed and added by Suneet 08/23/1999
//							TEXTSPRINTF(szMsg1,TEXT("Object#%d"),dwIndex);
                            TEXTCPY(szMsg1,TEXT("Object "));
                            TEXTCAT(szMsg1,lpszNotifyObject[NotifyObjects[dwIndex].dwObjectType]);

							//TEXTSPRINTF(szMsg1,TEXT("Object : LED"));
                            
                            TEXTCPY(szMsg2,TEXT("State = "));
                            TEXTCAT(szMsg2,lpszState[dwState]);

                            //if (dwState == 0)
							//	TEXTSPRINTF(szMsg2,TEXT("State = OFF"));
							//else
							//	TEXTSPRINTF(szMsg2,TEXT("State = ON"));
							MyMessage(szMsg1,szMsg2);
						}
					}
					break;

				case IDC_CYCLE_EDIT:
					EditCycleInfo();
					break;

				case IDC_LOCK_OFF:
					NOTIFY_SetLock(GetObjectIndex(),FALSE);
					break;

				case IDC_LOCK_ON:
					NOTIFY_SetLock(GetObjectIndex(),TRUE);
					break;

				case IDC_GET_LOCK:
					{
						DWORD dwIndex;
						BOOL bLock;
						TCHAR szMsg1[MAX_PATH];
						TCHAR szMsg2[MAX_PATH];

						dwIndex = GetObjectIndex();
						NOTIFY_GetLock(dwIndex,&bLock);
						TEXTSPRINTF(szMsg1,TEXT("Object#%d"),dwIndex);
						TEXTSPRINTF(szMsg2,TEXT("Lock = %d"),bLock);
						MyMessage(szMsg1,szMsg2);
					}

            }

			// Command was processed
            return (TRUE);
		
    }

	// Message was not processed
    return (FALSE);
}


//----------------------------------------------------------------------------
// NotifyPanel
//----------------------------------------------------------------------------

DIALOG_CHOICE NotifyPanelDialogChoices[] =
{
#ifdef _ST_PPC
	{ ST_PPC_STD,	IDD_NOTIFYPANEL_PPC_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_PPC_SIP,	IDD_NOTIFYPANEL_PPC_SIP,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7200
	{ ST_7200_STD,	IDD_NOTIFYPANEL_7200_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7200_TSK,	IDD_NOTIFYPANEL_7200_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
#ifdef _ST_7500
	{ ST_7500_STD,	IDD_NOTIFYPANEL_7500_STD,	NULL,	UI_STYLE_DEFAULT },
	{ ST_7500_TSK,	IDD_NOTIFYPANEL_7500_TSK,	NULL,	UI_STYLE_DEFAULT },
#endif
	{ ST_UNKNOWN,	IDD_NOTIFYPANEL_7500_TSK,	NULL,	UI_STYLE_DEFAULT }
};


void NotifyPanel(void)
{
	LoadString(g_hInstance,IDS_NOTIFYPANEL,l_szLastItem,TEXTSIZEOF(l_szLastItem));

	g_hdlg = CreateChosenDialog(g_hInstance,
								NotifyPanelDialogChoices,
								g_hwnd,
								NotifyPanelDlgProc,
								(LPARAM)NULL);

	WaitUntilDialogDoneSkip(FALSE);

	SendMessage(g_hwnd,WM_CLOSE,0,0L);
}


