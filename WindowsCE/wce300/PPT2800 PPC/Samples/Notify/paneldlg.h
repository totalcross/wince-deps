
//--------------------------------------------------------------------
// FILENAME:        paneldlg.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Paneldlg.c
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef PANELDLG_H_

#define PANELDLG_H_

#ifdef __cplusplus
extern "C"
{
#endif


// Macro defining the state bits for "selected" items

#define UI_SELECTED ( LVNI_SELECTED | LVNI_FOCUSED )


// Exported functions

void NotifyPanel(void);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef PANELDLG_H_  */
