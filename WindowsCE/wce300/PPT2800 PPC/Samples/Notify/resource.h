//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_OBJECTNAME                  1
#define IDS_NOTIFYPANEL                 2
#define IDS_LED_CYCLE_INFO              3
#define IDS_BEEPER_CYCLE_INFO           4
#define IDS_VIBRATOR_CYCLE_INFO         5
#define IDS_LED_CYCLE_ONDURATION        6
#define IDS_LED_CYCLE_OFFDURATION       7
#define IDS_LED_CYCLE_COUNT             8
#define IDS_VIBRATOR_CYCLE_DURATION     9
#define IDS_BEEPER_CYCLE_FREQUENCY      10
#define IDS_BEEPER_CYCLE_VOLUME         11
#define IDS_BEEPER_CYCLE_DURATION       12
#define IDS_LED_MIN_COUNT               17
#define IDS_LED_MAX_COUNT               18
#define IDS_BEEPER_MIN_VOLUME           21
#define IDS_BEEPER_MAX_VOLUME           22
#define IDS_BADRANGE                    27
#define IDS_LED_PAGE_COUNT              30
#define IDS_BEEPER_PAGE_VOLUME          32
#define IDM_MAIN_POCKET                 587
#define IDC_HELP_ABOUT                  621
#define IDM_MAIN                        644
#define IDS_PROGRAMNAME                 701
#define IDS_PROGRAMDESCRIPTION          702
#define IDI_PROGRAMICON                 703
#define IDC_CYCLE_EDIT1                 801
#define IDC_CYCLE_EDIT2                 802
#define IDC_CYCLE_EDIT3                 803
#define IDC_CYCLE_EDIT4                 804
#define IDC_CYCLE_TEXT1                 805
#define IDC_CYCLE_TEXT2                 806
#define IDC_CYCLE_TEXT3                 807
#define IDC_FILE_EXIT                   808
#define IDC_OBJECTS                     809
#define IDC_POPUP                       810
#define IDC_STATE_OFF                   811
#define IDC_STATE_ON                    812
#define IDC_STATE_CYCLE                 813
#define IDD_NOTIFYPANEL_7200_STD        817
#define IDD_NOTIFYPANEL_7200_TSK        818
#define IDD_NOTIFYPANEL_7500_STD        819
#define IDD_NOTIFYPANEL_7500_TSK        820
#define IDD_NOTIFYPANEL_PPC_SIP         821
#define IDD_NOTIFYPANEL_PPC_STD         822
#define IDM_POPUP                       823
#define IDA_ACCELERATOR1                824
#define IDD_CYCLE_EDIT_7200_TSK         827
#define IDD_CYCLE_EDIT_7500_TSK         828
#define IDD_CYCLE_EDIT_7500_STD         829
#define IDD_CYCLE_EDIT_PPC_STD          830
#define IDD_CYCLE_EDIT_7200_STD         832
#define IDD_CYCLE_EDIT_PPC_SIP          834
#define IDC_LOCK_OFF                    840
#define IDC_LOCK_ON                     841
#define IDC_GET_LOCK                    842
#define IDC_GET_STATE                   843
#define IDC_CYCLE_EDIT                  844
#define IDC_CYCLE_TEXT4                 845
#define IDC_CYCLE_EDIT5                 845
#define IDC_ACCEPT                      1021
#define IDC_SCROLLBAR1                  1029
#define IDC_SCROLLBAR2                  1030
#define IDC_SCROLLBAR3                  1031
#define IDC_VALIDATE                    1032
#define ID_ACCEL40013                   40013
#define IDS_CAP_FILE                    40016
#define ID_FILE                         40017
#define ID_HELP                         40019
#define IDS_CAP_HELP                    40021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        848
#define _APS_NEXT_COMMAND_VALUE         40022
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
