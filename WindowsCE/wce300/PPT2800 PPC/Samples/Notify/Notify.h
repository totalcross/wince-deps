
//--------------------------------------------------------------------
// FILENAME:        Notify.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:     Notify.C
//
// NOTES:           Public
//
// 
//--------------------------------------------------------------------

#ifndef NOTIFY_H_

#define NOTIFY_H_

#ifdef __cplusplus
extern "C"
{
#endif

//----------------------------------------------------------------------------
// Nested includes
//----------------------------------------------------------------------------

#include <ntfycapi.h>

#include "paneldlg.h"

#include "resource.h"


#ifdef _WIN32_WCE
	#pragma comment(lib,"ntfyapi32.lib")
#endif


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("NotifyTest")
#define APP_TITLE TEXT("Notification C API Test Program")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)


//----------------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------------

BOOL g_bSkipAccel;


//----------------------------------------------------------------------------
// Macros
//----------------------------------------------------------------------------

#define RESOURCE_STRING(a) ((LPTSTR)a)

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef NOTIFY_H_    */
