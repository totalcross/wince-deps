
//--------------------------------------------------------------------
// FILENAME:			cmdline.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header for command line parsing
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------
#ifndef CMDLINE_H_

#define CMDLINE_H_

#ifdef __cplusplus
extern "C"
{
#endif


void ParseCmdLine(LPCMDLINE lpCmdLine);


extern BOOL g_bAutoStartTrigger;
extern BOOL g_bShutDown;


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CMDLINE_H_   */

