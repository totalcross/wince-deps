//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_PROGRAM                     601
#define IDS_PROGRAMDESCRIPTION          602
#define IDC_FILE_EXIT                   620
#define IDC_HELP_ABOUT                  621
#define IDM_MAIN                        644
#define IDM_MAIN_POCKET                 645
#define DONE                            1034
#define IDC_POPUP                       1114
#define IDI_APPICON                     1145
#define IDI_PROGRAMICON                 1146
#define IDM_POPUP                       1149
#define IDA_ACCELERATOR1                1150
#define IDC_ADVANCE                     40007
#define IDC_RETARD                      40008
#define IDC_WHOAMI                      40009
#define ID_FILE                         40010
#define IDS_CAP_FILE                    40012
#define ID_HELP                         40013
#define IDS_CAP_HELP                    40015
#define ID_WINDOW                       40016
#define IDS_CAP_WINDOW                  40018
#define IDC_HIDE                        40019
#define IDC_SHOW                        40020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        647
#define _APS_NEXT_COMMAND_VALUE         40021
#define _APS_NEXT_CONTROL_VALUE         1059
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
