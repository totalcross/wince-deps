
//--------------------------------------------------------------------
// FILENAME:			main.c
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Windows startup functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>


#include "..\common\StdGlobs.h"

#include "..\common\StdMsg.h"
#include "..\common\StdStrng.h"
#include "..\common\StdScrns.h"
#include "..\common\StdInit.h"

#include "MyInit.h"
#include "cmdline.h"

#include "ScanWedge.h"


//----------------------------------------------------------------------------
// Exported functions
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
//
//  FUNCTION:   WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
//
//  PURPOSE:    Entry point function, initializes the application, instance,
//              and then launches the message loop.
//
//  PARAMETERS:
//      hInstance     - handle that uniquely identifies this instance of the
//                      application
//      hPrevInstance - always zero in Win32
//      lpszCmdLine   - any command line arguements pass to the program
//      nCmdShow      - the state which the application shows itself on
//                      startup
//
//  RETURN VALUE:
//      (int) Returns the value from PostQuitMessage().
//
//  COMMENTS:
//
//----------------------------------------------------------------------------

int WINAPI WinMain(HINSTANCE hInstance,
				   HINSTANCE hPrevInstance,
				   LPCMDLINE lpCmdLine,
				   int nCmdShow)
{
	int iResult = 0;

	ParseCmdLine(lpCmdLine);

    if ( MyInitApplication(hInstance,
						 APP_CLASS,
						 ((HBRUSH)DEFBKGDCOLOR),
						 MAKEINTRESOURCE(IDM_MAIN),
						 MAKEINTRESOURCE(IDI_APPICON),
						 FALSE) )
	{
		if ( InitInstance(hInstance,
						  SW_SHOW,
						  GetMainWindowStyle(),
						  APP_CLASS,
						  APP_TITLE) )
		{
			iResult = StdDoMain(IDA_ACCELERATOR1);
		}
	}

	return(iResult);
}


