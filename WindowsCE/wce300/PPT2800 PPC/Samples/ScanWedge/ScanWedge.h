
//--------------------------------------------------------------------
// FILENAME:			ScanWedge.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Contains variables and prototypes global to 
//						the application
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef SCANWEDGE_H_

#define SCANWEDGE_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "resource.h"


//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

#define APP_CLASS TEXT("ScanWedge")
#define APP_TITLE TEXT("Scan Keyboard Wedge")

#define DEFBKGDCOLOR (COLOR_WINDOW + 1)

enum tagUSERMSGS
{
	UM_SCAN = UM_USER,
	UM_APPKEY,
	UM_TRIGGER,
	UM_SHUTDOWN,
};


#ifdef __cplusplus
}
#endif

#endif  /* #ifndef SCANWEDGE_H_ */

