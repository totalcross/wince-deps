//--------------------------------------------------------------------
// FILENAME:			ZEBRA.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Zebra Bar Code printer
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _ZEBRA_H_
#define _ZEBRA_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "barcodes.h"

#ifdef _WIPRO_

// Bar Code type definitions that are unique for Zebra Printer
#define TYPE_CODE_11					0x40
#define TYPE_INDUSTRIAL_2_OF_5				0x41
#define TYPE_STANDARD_2_OF_5				0x42
#define TYPE_PLESSEY					0x43
#define TYPE_POST_NET					0x44
#define TYPE_UPC_E	      				0x45
#define TYPE_UPC_EAN_EXT				0x46
#define TYPE_LOGMARS					0x47
#define TYPE_CODE49					0x48

// the below bar codes are common to all the printers and are defined in 
// barcodes.h
/*
TYPE_I2OF5				
TYPE_CODABAR				
TYPE_MSI				
TYPE_EAN8				
TYPE_EAN13				
TYPE_UPCA				
TYPE_CODE39				
TYPE_CODE93				
TYPE_CODE128				
TYPE_PDF417				
*/

#else // _WIPRO_

// Bar Code type definitions for Zebra Printer
#define TYPE_CODE_11					0x40
#define TYPE_INDUSTRIAL_2_OF_5				0x41
#define TYPE_STANDARD_2_OF_5				0x42
#define TYPE_PLESSEY					0x43
#define TYPE_POST_NET					0x44
#define TYPE_UPC_E	      				0x45
#define TYPE_UPC_EAN_EXT				0x46
#define TYPE_LOGMARS					0x47
#define TYPE_CODE49					0x48

#endif	// ifdef _WIPRO_

// BarCode type defintion table.
// Note: This table is Bar Code type defined in prnioctl.h
/*LPCTSTR  pBarCodeTypeTable[] = {
  TEXT("UPCE"),					// 0x0
  TEXT("UPCE"),					// 0x1
  TEXT("UPCA"),					// 0x2
  TEXT("EAN8"),					// 0x3
  TEXT("EAN13"),				// 0x4
  TEXT("MSI"),					// 0x5
  TEXT("MSI1010"),				// 0x6
  TEXT("MSI1110"),				// 0x7
  TEXT("CODABAR"),				// 0x8
  TEXT("CODABAR16"),			// 0x9
  TEXT("39"),					// 0xA
  TEXT("39C"),					// 0xB
  TEXT("F39"),					// 0xC
  TEXT("F39C"),					// 0xD
  TEXT("I2OF5"),				// 0xE
  TEXT("I2OF5"),				// 0xF
  TEXT("93"),					// 0x10
  TEXT("93"),					// 0x11
  TEXT("128"),					// 0x12
  TEXT("PDF-417"),				// 0x13
  TEXT("DELTA_ENCODED"),		// 0x14
  TEXT("WIDTH_ENCODED")			// 0x15
};
*/

typedef struct __BARCODEINFO {
  // For One-dimensional Standard Barcode type.
  int		width;		 // Unit-width of the narrow bar.
  float		ratio;		 // Ratio of the wide bar to the narrow bar.

  // For Two-dimensional Standard Barcode type.
  int		XDval;		 // Unit-width  of the narrowest element.
  int		YDval;		 // Unit-height of the narrowest element.
  int		Cval;		 // Number of columns to use.
  int		Sval;		 // Security level indicate maximum
						 // amount of erros to be detected.
} BARCODEINFO,  *LPBARCODEINFO;

#ifdef __cplusplus
}
#endif
#endif // _ZEBRA_H_


