
//--------------------------------------------------------------------
// FILENAME:			RcmCApi.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Main include file for resource coordinator C API
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef RCMCAPI_H_

#define RCMCAPI_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------


#include <RcmErr.h>
#include <RcmDef.h>

//--------------------------------------------------------------------
// Defines, typedefs, etc
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------


// Get unique unit ID information
DWORD RCMAPI RCM_GetUniqueUnitId(
	LPUNITID lpUnitId);				// Pointer to unit ID information


// Get unique unit ID information (extended)
DWORD RCMAPI RCM_GetUniqueUnitIdEx(
	LPUNITID_EX lpUnitIdEx);		// Pointer to unit ID information


// Get temperature information
DWORD RCMAPI RCM_GetTemperatureInfo(
	LPTEMPERATURE_INFO lpTemperatureInfo);


// Get the real time status of the hardware triggers
DWORD RCMAPI RCM_GetTriggerStatus(
	LPDWORD lpdwTriggerStatus);		// Pointer to returned trigger status


// Get the registered trigger status
DWORD RCMAPI RCM_GetTriggerRegistration(
	LPDWORD lpdwTriggerStatus);		// Pointer to returned exclusive trigger 
									//  registration status. Returns a 1 in each
									//  bit for which the trigger has exclusive 
									//	notification registered.


// Register for trigger message notification
DWORD RCMAPI RCM_RegisterTriggerMessage(
	DWORD dwTriggerMask,			// Mask indicating triggers to notify on
									//  See trigger masks in RcmDef.h
	BOOL bExclusive,				// Flag indicating that exclusive use of
									//	  the specified triggers is required
	HWND hWnd,						// Window handle to which message is posted
	UINT uiMessage,					// Message ID to be posted to notify
									//   of specified trigger state changes
	LPHANDLE lphHandle);			// Pointer to returned handle for deregister


// Register for trigger event notification
DWORD RCMAPI RCM_RegisterTriggerEvent(
	DWORD dwTriggerMask,			// Mask indicating triggers to notify on
									//  See trigger masks in RcmDef.h
	BOOL bExclusive,				// Flag indicating that exclusive use of
									//	  the specified triggers is required
	LPTSTR lpszEventName,			// Name of event to set when trigger state changes
	LPHANDLE lphHandle);			// Pointer to returned handle for deregister


// Deregister trigger notification
DWORD RCMAPI RCM_DeregisterTrigger(
	HANDLE hHandle);				// Handle to be deregistered
	

// Get the trigger status that caused the previous notification on a registered trigger handle
DWORD RCMAPI RCM_GetLastTriggerStatus(
	HANDLE hHandle,					// Handle of registered trigger
	LPDWORD lpdwTriggerStatus);		// Pointer to returned last trigger status
	

// Get configuration data
DWORD RCMAPI RCM_GetConfigurationData(
	DWORD dwConfigType,				// The type of configuration data requested
									//	See tagCONFIG_TYPE
	LPDWORD lpdwConfigData);		// Pointer to returned configuration data. The
									// format of the data is platform and type dependent.

// Get version information
DWORD RCMAPI RCM_GetVersion(
	LPRCM_VERSION_INFO lpRcmVersionInfo);	// Pointer to version structure to fill 
									// with RCM version information.



#ifdef __cplusplus
}
#endif

#endif	// #ifndef RCMCAPI_H_
