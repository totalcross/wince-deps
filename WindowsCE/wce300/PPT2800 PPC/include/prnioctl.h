
//--------------------------------------------------------------------
// FILENAME:			PRNIOCTL.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			IOCTL definitions and related structures 
//						for Symbol printer MDD driver
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _PRNIOCTL_H_
#define _PRNIOCTL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "printapi.h"

//
// Define kernel IOCTL.
//
#define CODE_BASE    2048                    // OEM codes can range from 2048 to 4095

#define IOCTL_CREATEDC      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+1),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DELETEDC      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+2),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_STARTDOC      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+3),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ENDDOC        CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+4),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ABORTDOC      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+5),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_STARTPAGE     CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+6),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ENDPAGE       CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+7),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DRAWTEXT      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+8),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_POLYLINE	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+9),  METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_BITBLT	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+10), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GETDEVICECAPS CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+11), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_BARCODE       CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+12), METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_SETABORTPROC  CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+13), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_PRINTDLG	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+14), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DRAWEDGE      CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+15), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ELLIPSE	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+16), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EXTTEXTOUT    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+17), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FILLRECT	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+18), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_FILLRGN	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+19), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_POLYGON	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+20), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_RECTANGLE	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+21), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ROUNDRECT	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+22), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_SETPIXEL	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+23), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_STRETCHBLT    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+24), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_ENUMFONT	    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+25), METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GETVERSION    CTL_CODE(FILE_DEVICE_HAL, (CODE_BASE+26), METHOD_BUFFERED, FILE_ANY_ACCESS)

// --------------------------------------------------
typedef struct __CREATEDC_IN    {
  DWORD    szDriverOffset;
  DWORD    szDeviceOffset;
  DWORD    szOutputOffset;
  DWORD    InitDataOffset;
  TCHAR	   DataBuf[1];
} CREATEDC_IN, *PCREATEDC_IN;

typedef struct __CREATEDC_OUT    {
  HDC       hDC;
} CREATEDC_OUT, *PCREATEDC_OUT;


// --------------------------------------------------
typedef struct __DELETEDC_IN    {
  HDC       hDC;
} DELETEDC_IN, *PDELETEDC_IN;

typedef struct __DELETEDC_OUT    {
  DWORD     dwRetVal;
} DELETEDC_OUT, *PDELETEDC_OUT;


// --------------------------------------------------
typedef struct __STARTDOC_IN    {
  HDC       hDC;
  DWORD	    diOffset;
  TCHAR	    DataBuf[1];
} STARTDOC_IN, *PSTARTDOC_IN;

typedef struct __STARTDOC_OUT    {
  DWORD     dwRetVal;
} STARTDOC_OUT, *PSTARTDOC_OUT;


// --------------------------------------------------
typedef struct __ENDDOC_IN    {
  HDC       hDC;
} ENDDOC_IN, *PENDDOC_IN;

typedef struct __ENDDOC_OUT    {
  DWORD     dwRetVal;
} ENDDOC_OUT, *PENDDOC_OUT;


// --------------------------------------------------
typedef struct __STARTPAGE_IN    {
  HDC       hDC;
} STARTPAGE_IN, *PSTARTPAGE_IN;

typedef struct __STARTPAGEX_OUT    {
  DWORD     dwRetVal;
} STARTPAGE_OUT, *PSTARTPAGE_OUT;


// --------------------------------------------------
typedef struct __ENDPAGE_IN    {
  HDC       hDC;
} ENDPAGE_IN, *PENDPAGE_IN;

typedef struct __ENDPAGE_OUT    {
  DWORD     dwRetVal;
} ENDPAGE_OUT, *PENDPAGE_OUT;

typedef struct __BARCODE_IN   {
  HDC       hDC;
  int		nAngle;
  UINT		uBarCodeType;
  DWORD		dwBarCodeFlags;
  DWORD		BarCodeInfoOffset;
  int		BarCodeInfoSize;
  DWORD		BarCodeDataOffset;
  int		nCount;
  RECT		BoundingRect;
  PUCHAR	lpBackgndBrush;
  BCCONSTRAINT	BCConstraint;
  DWORD		LabelTextOffset;
  RECT		TextRect;
  UINT		uFormat;
  TCHAR		DataArea[1];
} BARCODE_IN, *PBARCODE_IN;

typedef struct __BARCODE_OUT    {
  DWORD     dwRetVal;
} BARCODE_OUT, *PBARCODE_OUT;


// --------------------------------------------------
typedef struct __DRAWTEXT_IN    {
  HDC       hDC;
  DWORD		StringOffset; 
  int		nCount; 
  RECT		Rect;
  UINT		uFormat; 
  TCHAR		StringData[1];
} DRAWTEXT_IN, *PDRAWTEXT_IN;

typedef struct __DRAWTEXT_OUT    {
  DWORD     dwRetVal;
} DRAWTEXT_OUT, *PDRAWTEXT_OUT;


// --------------------------------------------------
typedef struct __POLYLINE_IN    {
  HDC       hDC;
  int		nPoints;
  POINT		Pt[1];
} POLYLINE_IN, *PPOLYLINE_IN;

typedef struct __POLYLINE_OUT    {
  DWORD     dwRetVal;
} POLYLINE_OUT, *PPOLYLINE_OUT;


// --------------------------------------------------
typedef struct __BITBLT_IN    {
  HDC		hdcDest; 
  int		nXDest; 
  int		nYDest; 
  int		nWidth; 
  int		nHeight; 
  HDC		hdcSrc; 
  int		nXSrc; 
  int		nYSrc; 
  DWORD		dwRop;
} BITBLT_IN, *PBITBLT_IN;

typedef struct __BITBLT_OUT    {
  DWORD     dwRetVal;
} BITBLT_OUT, *PBITBLT_OUT;


// --------------------------------------------------
typedef struct __ABORTDOC_IN    {
  HDC       hDC;
} ABORTDOC_IN, *PABORTDOC_IN;

typedef struct __ABORTDOC_OUT    {
  DWORD     dwRetVal;
} ABORTDOC_OUT, *PABORTDOC_OUT;


// --------------------------------------------------
typedef struct __SETABORTPROC_IN    {
  HDC       hDC;
  ABORTPROC lpAbortProc;
} SETABORTPROC_IN, *PSETABORTPROC_IN;

typedef struct __SETABORTPROC_OUT    {
  DWORD     dwRetVal;
} SETABORTPROC_OUT, *PSETABORTPROC_OUT;


// --------------------------------------------------
typedef struct __GETDEVICECAPS_IN    {
  HDC       hDC;
  int		nIndex;
} GETDEVICECAPS_IN, *PGETDEVICECAPS_IN;

typedef struct __GETDEVICECAPS_OUT    {
  int    	iRetVal;
} GETDEVICECAPS_OUT, *PGETDEVICECAPS_OUT;

#if 0  // Not implement
// --------------------------------------------------
typedef struct __PRINTDLG_IN    {
  PAGESETUPDLG 		PntDlg;
} PRINTDLG_IN, *PPRINTDLG_IN;

typedef struct __PRINTDLG_OUT    {
  DWORD     dwRetVal;
} PRINTDLG_OUT, *PPRINTDLG_OUT;


// --------------------------------------------------
typedef struct __EXTTEXTOUT_IN    {
  HDC		hdc; 
  int		X; 
  int		Y; 
  UINT		fuOptions; 
  RECT		rc; 
  DWORD		StringOffset; 
  UINT		cbCount; 
  DWORD		DxOffset;
  TCHAR		BufData[1];
} EXTTEXTOUT_IN, *PEXTTEXTOUT_IN;

typedef struct __EXTTEXTOUT_OUT    {
  DWORD     dwRetVal;
} EXTTEXTOUT_OUT, *PEXTTEXTOUT_OUT;


// --------------------------------------------------
typedef struct __RECTANGLE_IN    {
  HDC		hDC; 
  int		nLeftRect; 
  int		nTopRect; 
  int		nRightRect; 
  int		nBottomRect;
} RECTANGLE_IN, *PRECTANGLE_IN;

typedef struct __RECTANGLE_OUT    {
  DWORD     dwRetVal;
} RECTANGLE_OUT, *PRECTANGLE_OUT;


// --------------------------------------------------
typedef struct __DRAWEDGE_IN    {
  HDC       hDC;
  RECT		qrc;
  UINT		edge;
  UINT		grfFlags;
} DRAWEDGE_IN, *PDRAWEDGE_IN;

typedef struct __DRAWEDGE_OUT    {
  DWORD     dwRetVal;
} DRAWEDGE_OUT, *PDRAWEDGE_OUT;


// --------------------------------------------------
typedef struct __ELLIPSE_IN    {
  HDC       hDC;
  int		nLeftRect;
  int		nTopRect;
  int		nRightRect;
  int		nBottomRect;
} ELLIPSE_IN, *PELLIPSE_IN;

typedef struct __ELLIPSE_OUT    {
  DWORD     dwRetVal;
} ELLIPSE_OUT, *PELLIPSE_OUT;


// --------------------------------------------------
typedef struct __FILLRECT_IN    {
  HDC       hDC;
  RECT		rc; 
  HBRUSH	hbr;
} FILLRECT_IN, *PFILLRECT_IN;

typedef struct __FILLRECT_OUT    {
  DWORD     dwRetVal;
} FILLRECT_OUT, *PFILLRECT_OUT;


// --------------------------------------------------
typedef struct __FILLRGN_IN    {
  HDC       hDC;
  HRGN		hrgn; 
  HBRUSH	hbr; 
} FILLRGN_IN, *PFILLRGN_IN;

typedef struct __FILLRGN_OUT    {
  DWORD     dwRetVal;
} FILLRGN_OUT, *PFILLRGN_OUT;


// --------------------------------------------------
typedef struct __POLYGON_IN   {
  HDC       hDC;
  POINT		Points;
  int		nCount;
} POLYGON_IN, *PPOLYGON_IN;

typedef struct __POLYGON_OUT    {
  DWORD     dwRetVal;
} POLYGON_OUT, *PPOLYGON_OUT;


// --------------------------------------------------
typedef struct __ROUNDRECT_IN    {
  HDC       hDC;
  int		nLeftRect; 
  int		nTopRect; 
  int		nRightRect; 
  int		nBottomRect; 
  int		nWidth; 
  int		nHeight; 
} ROUNDRECT_IN, *PROUNDRECT_IN;

typedef struct __ROUNDRECT_OUT    {
  DWORD     dwRetVal;
} ROUNDRECT_OUT, *PROUNDRECT_OUT;


// --------------------------------------------------
typedef struct __SETPIXEL_IN    {
  HDC       hDC;
  int		X; 
  int		Y; 
  COLORREF	crColor; 
} SETPIXEL_IN, *PSETPIXEL_IN;

typedef struct __SETPIXEL_OUT    {
  COLORREF    	crRetVal;
} SETPIXEL_OUT, *PSETPIXEL_OUT;


// --------------------------------------------------
typedef struct __SCRETCHBLT_IN    {
  HDC	  	hdcDest; 
  int	  	nXOriginDest; 
  int	  	nYOriginDest; 
  int	  	nWidthDest; 
  int	  	nHeightDest; 
  HDC	  	hdcSrc; 
  int	  	nXOriginSrc; 
  int	  	nYOriginSrc; 
  int	  	nWidthSrc; 
  int	  	nHeightSrc; 
  DWORD	  	dwRop; 
} SCRETCHBLT_IN, *PSCRETCHBLT_IN;

typedef struct __SCRETCHBLT_OUT    {
  DWORD     dwRetVal;
} SCRETCHBLT_OUT, *PSCRETCHBLT_OUT;


// --------------------------------------------------
typedef struct __ENUMFONT_IN    {
  HDC       		hDC;
  DWORD				szFamilyOffset; 
  FONTENUMPROC		lpEnumFontFamProc; 
  DWORD				ParamOffset; 
  TCHAR				BufData[1];
} ENUMFONT_IN, *PENUMFONT_IN;

typedef struct __ENUMFONT_OUT    {
  DWORD     dwRetVal;
} ENUMFONT_OUT, *PENUMFONT_OUT;
#endif	// Not implement

#ifdef __cplusplus
}
#endif

#endif // _PRNIOCTL_H_
