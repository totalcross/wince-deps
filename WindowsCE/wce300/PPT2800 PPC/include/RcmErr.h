
//--------------------------------------------------------------------
// FILENAME:			RcmErr.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by resource coordinator functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef RCMERR_H_
#define RCMERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT				0x80000000	// Use bit 31 to indicate errror
#define	USER_BIT				0x20000000	// Bit 29 means not Win32 error	

#define	RCM_ERROR(code)			(ERROR_BIT | USER_BIT | (WORD) code)

#define	E_RCM_SUCCESS				0
#define	E_RCM_ALREADYINITIALIZED	RCM_ERROR(0x0001)
#define	E_RCM_CREATEEVENTFAILED		RCM_ERROR(0x0002)
#define	E_RCM_CREATETHREADFAILED	RCM_ERROR(0x0003)
#define	E_RCM_NOTENOUGHMEMORY		RCM_ERROR(0x0004)
#define	E_RCM_NOTINITIALIZED		RCM_ERROR(0x0005)
#define	E_RCM_INVALIDDVCCONTEXT		RCM_ERROR(0x0006)
#define	E_RCM_INVALIDOPNCONTEXT		RCM_ERROR(0x0007)
#define	E_RCM_CANTREADREGVALUE		RCM_ERROR(0x0008)
#define	E_RCM_CANTOPENREGKEY		RCM_ERROR(0x0009)
#define	E_RCM_INVALIDIOCTRL			RCM_ERROR(0x000A)
#define	E_RCM_NULLPTR				RCM_ERROR(0x000B)
#define	E_RCM_BADSTRUCTINFO			RCM_ERROR(0x000C)
#define	E_RCM_PARAMMISSING			RCM_ERROR(0x000D)
#define	E_RCM_BUFFERTOOSMALL		RCM_ERROR(0x000E)
#define	E_RCM_MISSINGFIELD			RCM_ERROR(0x000F)
#define	E_RCM_INVALIDHANDLE			RCM_ERROR(0x0010)
#define	E_RCM_INVALIDPARAM			RCM_ERROR(0x0011)
#define	E_RCM_INVALIDDEVICENAME		RCM_ERROR(0x0012)
#define	E_RCM_TRIGGERINUSE			RCM_ERROR(0x0013)
#define	E_RCM_NOTIFYERROR			RCM_ERROR(0x0014)
#define	E_RCM_NOTSUPPORTED			RCM_ERROR(0x0015)
#define	E_RCM_INVALIDCONFIGTYPE		RCM_ERROR(0x0016)
#define	E_RCM_EXCEPTION				RCM_ERROR(0x0017)
#define	E_RCM_WIN32ERROR			RCM_ERROR(0x0018)


#ifdef __cplusplus
}
#endif

#endif	// #ifndef RCMERR_H_

