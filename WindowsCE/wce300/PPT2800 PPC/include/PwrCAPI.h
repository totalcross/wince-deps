//-----------------------------------------------------------------------------
// FILENAME:		PwrCApi.h
// MODULE NAME:		PwrApi32.dll
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:		C Wrapper Header File
//
// NOTES:			None
//
// 
//-----------------------------------------------------------------------------
#ifndef POWERCWRP_H_

#define POWERCWRP_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------

#include "Pwrerr.h"
#include "PwrDef.h"

//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------


// Find first power managed device
DWORD POWERAPI POWER_FindFirst_W(
	LPPOWER_FINDINFO_W lpPwrFindInfo,	// Struct to fill in
	LPHANDLE lphFindHandle);				// Handle for subsequent finds

DWORD POWERAPI POWER_FindFirst_A(
	LPPOWER_FINDINFO_A lpPwrFindInfo,	// Struct to fill in
	LPHANDLE lphFindHandle);				// Handle for subsequent finds

#ifdef UNICODE
#define POWER_FindFirst POWER_FindFirst_W
#else
#define POWER_FindFirst POWER_FindFirst_A
#endif


// Find next power managed device
DWORD POWERAPI POWER_FindNext_W(
	LPPOWER_FINDINFO_W lpPwrFindInfo,	// Struct to fill in
	HANDLE hFindHandle);				// Handle returned by find first

DWORD POWERAPI POWER_FindNext_A(
	LPPOWER_FINDINFO_A lpPwrFindInfo,	// Struct to fill in
	HANDLE hFindHandle);			   	// Handle returned by find first

#ifdef UNICODE
#define POWER_FindNext POWER_FindNext_W
#else
#define POWER_FindNext POWER_FindNext_A
#endif


// Close the handle used by POWER_FindFirst and POWER_FindNext
DWORD POWERAPI POWER_FindClose(HANDLE hFindHandle);


// Open Power Management Driver
DWORD POWERAPI POWER_Open(void);


// Get version information about an PM Driver
DWORD POWERAPI POWER_GetVersion(
   LPPOWER_VERSION_INFO lpPowerVersionInfo);	// Ptr to struct to receive data


// Close connection ton Power Management driver
DWORD POWERAPI POWER_Close(void);


// Used by a device driver to request notification of power events for a device
DWORD POWERAPI POWER_SetDeviceNotify(
   LPNOTIFYREQUEST lpNotifyRequest, // Pointer to DeviceNotify structure
   LPCTSTR lpszDeviceName,          // Name of device doing the notification 
   DWORD  dwNotifyMask);            // Bit mask specifying power event

// Used by a device driver to cancel a notification request				
DWORD POWERAPI POWER_GetDeviceNotify(
   LPNOTIFYREQUEST lpNotifyRequest,  // Pointer to Device Notify structure
   LPCTSTR lpszDeviceName,           // Name of device doing the notification 
   LPDWORD lpdwNotifyMask);          // Pointer to bit mask specifying power event

// Sets the device state
DWORD POWERAPI POWER_SetDeviceState(
   LPCTSTR lpszDeviceName,          // Unicode string containing device name
   DWORD dwDeviceState);            // Bit mask specifying device state

// Gets current state of a device
DWORD POWERAPI POWER_GetDeviceState(
   LPCTSTR lpszDeviceName,         // Unicode string containing device name
   LPDWORD lpdwDeviceState);       // Pointer to DWORD for device state bit mask.

// Request that a device state be allowed. Decrements "Allow" count.
DWORD POWERAPI POWER_AllowDeviceState(
   LPCTSTR lpszDeviceName,        // Unicode string containing device name
   DWORD dwDeviceState);          // Bit mask specifying device state

// Request that a device state be disallowed. Increments "Allow" count.
DWORD POWERAPI POWER_ProhibitDeviceState(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwDeviceState);         // Bit mask specifying device state


// Obtain the value of the "Allow" count for a device state.
DWORD POWERAPI POWER_CheckDeviceState(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwDeviceState,          // Bit mask specifying device state
   LPDWORD  lpdwAllowCnt);       // Pointer to "Allow" count returned by API


// Set inactivity timer value for device and device state
DWORD POWERAPI POWER_SetTimerValue(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwDeviceState,          // Bit mask specifying device state
   DWORD dwTimerVal );           // Inactivity timer value

// Get inactivity timer value for device and device state
DWORD POWERAPI POWER_GetTimerValue(	
   LPTSTR lpszDeviceName,        // Unicode string containing device name
   DWORD dwDeviceState,          // Bit mask specifying device state
   LPDWORD lpdwTimerVal );       // Pointer inactivity timer value returned by API

// Set the state a device will be set to on a given power event
DWORD POWERAPI POWER_SetDeviceMask(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwDeviceState,          // Bit mask of device state device will be
                                 // set to on power event
   DWORD dwDeviceMask );         // Bit mask specifying power event


// Get the power event that sets device to specified device state
DWORD POWERAPI POWER_GetDeviceMask(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwDeviceState,          // Bit mask of device state device will be
                                 // set to on power event
   LPDWORD lpdwDeviceMask );     // Pointer to bit mask of power event

// Set the state a device will be set to on a activity
DWORD POWERAPI POWER_SetDeviceActivity(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   DWORD dwActivityMask );       // Activity bit mask


// Get the last activity that set device to specified device state
DWORD POWERAPI POWER_GetDeviceActivity(
   LPCTSTR lpszDeviceName,       // Unicode string containing device name
   LPDWORD lpdwActivityMask );   // Pointer to activity bit mask

// Get Battery Level, Battery Type and Power Source
DWORD POWERAPI POWER_GetPowerInfo(
   LPPOWER_INFO lpPowerInfo);    // Pointer to the Power Info structure


// Ioctl for Power Management driver, currently unsupported.
DWORD POWERAPI POWER_Ioctl(	DWORD	dwIoctlCode,
							LPVOID	lpvInBuf,
							DWORD	dwInBufSize,
							LPVOID	lpvOutBuf,
							DWORD	dwOutBufSize,
							LPDWORD	lpdwActualOut
							);

#ifdef __cplusplus
}
#endif

#endif	/* #ifndef POWERCWRP_H_	*/
