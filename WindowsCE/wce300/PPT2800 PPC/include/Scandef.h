
//--------------------------------------------------------------------
// FILENAME:			ScanDef.h
//
// Copyright(c) 1998 - 2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Constants and types used by scanner functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef SCANDEF_H_
#define SCANDEF_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested Includes
//--------------------------------------------------------------------

#include <StrucInf.h>


//--------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------

#define SCANAPI WINAPI

#define DECODERS_BUFLEN 128
#define MAX_DECODERS (DECODERS_BUFLEN - 1)

#define MAX_DEVICE_NAME 6


//--------------------------------------------------------------------
// Decoder type definitions
//--------------------------------------------------------------------

#define DECODER_UPCE0			"\x30"
#define DECODER_UPCE1			"\x31"
#define DECODER_UPCA			"\x32"
#define DECODER_MSI				"\x33"
#define DECODER_EAN8			"\x34"
#define DECODER_EAN13			"\x35"
#define DECODER_CODABAR			"\x36"
#define DECODER_CODE39			"\x37"
#define DECODER_D2OF5			"\x38"
#define DECODER_I2OF5			"\x39"
#define DECODER_CODE11			"\x3A"
#define DECODER_CODE93			"\x3B"
#define DECODER_CODE128			"\x3C"
#define DECODER_RESERVED_3D		"\x3D"	// Really unused
#define DECODER_RESERVED_3E		"\x3E"	// Would be IATA2OF5
#define DECODER_RESERVED_3F		"\x3F"	// Would be EAN128
#define DECODER_PDF417			"\x40"
#define DECODER_RESERVED_41		"\x41"	// Would be ISBT128
#define DECODER_TRIOPTIC39		"\x42"
#define DECODER_RESERVED_43		"\x43"	// Would be COUPON
#define DECODER_RESERVED_44		"\x44"	// Would be BOOKLAND
#define DECODER_MICROPDF		"\x45"
#define DECODER_RESERVED_46		"\x46"	// Would be CODE32
#define DECODER_RESERVED_47		"\x47"	// Would be MACROPDF
#define DECODER_MAXICODE		"\x48"
#define DECODER_DATAMATRIX		"\x49"
#define DECODER_QRCODE			"\x4A"

#define DECODER_POINTER			"\x50"	// "P"
#define DECODER_IMAGE			"\x51"
#define DECODER_SIGNATURE		"\x52"

#define DECODER_RESERVED_59		"\x59"	// Would be Supp2
#define DECODER_RESERVED_5A		"\x5A"	// Would be Supp5

#define DECODER_USPOSTNET		"\x61"
#define DECODER_USPLANET		"\x62"
#define DECODER_UKPOSTAL		"\x63"
#define DECODER_JAPPOSTAL		"\x64"
#define DECODER_AUSPOSTAL		"\x65"
#define DECODER_DUTCHPOSTAL		"\x66"

#define DECODER_TYPE(decoder) LABELTYPE_##decoder


typedef CHAR DECODER[2];

typedef CHAR FAR * LPDECODER;


//--------------------------------------------------------------------
// List of all decoder types
//--------------------------------------------------------------------

#define ALL_DECODERS \
		DECODER_UPCE0 \
		DECODER_UPCE1 \
		DECODER_UPCA \
		DECODER_MSI \
		DECODER_EAN8 \
		DECODER_EAN13 \
		DECODER_CODABAR \
		DECODER_CODE39 \
		DECODER_D2OF5 \
		DECODER_I2OF5 \
		DECODER_CODE11 \
		DECODER_CODE93 \
		DECODER_CODE128 \
		DECODER_PDF417 \
		DECODER_TRIOPTIC39 \
		DECODER_MICROPDF \
		DECODER_MAXICODE \
		DECODER_DATAMATRIX \
		DECODER_QRCODE \
		DECODER_IMAGE \
		DECODER_SIGNATURE \
		DECODER_USPOSTNET \
		DECODER_USPLANET \
		DECODER_UKPOSTAL \
		DECODER_JAPPOSTAL \
		DECODER_AUSPOSTAL \
		DECODER_DUTCHPOSTAL \

//--------------------------------------------------------------------
// Number of decoder types actually supported
//--------------------------------------------------------------------

#define NUM_DECODERS ((sizeof(ALL_DECODERS)/sizeof(CHAR))-1)


//--------------------------------------------------------------------
// Label type definitions
//		Note:	Every decoder type listed above must have a
//				corresponding label type with the same hex
//				value.  Additional label types can also be
//				defined if a decoder can produce more than
//				one label type.
//--------------------------------------------------------------------

#define LABELTYPE_UPCE0			0x30
#define LABELTYPE_UPCE1			0x31
#define LABELTYPE_UPCA			0x32
#define LABELTYPE_MSI			0x33
#define LABELTYPE_EAN8			0x34
#define LABELTYPE_EAN13			0x35
#define LABELTYPE_CODABAR		0x36
#define LABELTYPE_CODE39		0x37
#define LABELTYPE_D2OF5			0x38
#define LABELTYPE_I2OF5			0x39
#define LABELTYPE_CODE11		0x3A
#define LABELTYPE_CODE93		0x3B
#define LABELTYPE_CODE128		0x3C
#define LABELTYPE_RESERVED_3D	0x3D	// Really unused
#define LABELTYPE_IATA2OF5		0x3E
#define LABELTYPE_EAN128		0x3F
#define LABELTYPE_PDF417		0x40
#define LABELTYPE_ISBT128		0x41
#define LABELTYPE_TRIOPTIC39	0x42
#define LABELTYPE_COUPON		0x43
#define LABELTYPE_BOOKLAND		0x44
#define LABELTYPE_MICROPDF		0x45
#define LABELTYPE_CODE32		0x46
#define LABELTYPE_MACROPDF		0x47
#define LABELTYPE_MAXICODE		0x48
#define LABELTYPE_DATAMATRIX	0x49
#define LABELTYPE_QRCODE		0x4A

#define LABELTYPE_RESERVED_50	0x50	// Would be Pointer
#define LABELTYPE_IMAGE			0x51
#define LABELTYPE_SIGNATURE		0x52

#define LABELTYPE_RESERVED_59	0x59	// Would be Supp2
#define LABELTYPE_RESERVED_5A	0x5A	// Would be Supp5

#define LABELTYPE_USPOSTNET		0x61
#define LABELTYPE_USPLANET		0x62
#define LABELTYPE_UKPOSTAL		0x63
#define LABELTYPE_JAPPOSTAL		0x64
#define LABELTYPE_AUSPOSTAL		0x65
#define LABELTYPE_DUTCHPOSTAL	0x66

#define LABELTYPE_UNKNOWN		0xFF


typedef DWORD LABELTYPE;

typedef LABELTYPE FAR * LPLABELTYPE;


//--------------------------------------------------------------------
// Symbol Technologies label ID codes
//--------------------------------------------------------------------

#define SYMBOL_ID_UPCEAN		'A'
#define SYMBOL_ID_CODE39		'B'
#define SYMBOL_ID_CODE32		'B'
#define SYMBOL_ID_CODABAR		'C'
#define SYMBOL_ID_CODE128		'D'
#define SYMBOL_ID_CODE93		'E'
#define SYMBOL_ID_I2OF5			'F'
#define SYMBOL_ID_D2OF5			'G'
#define SYMBOL_ID_IATA2OF5		'G'
#define SYMBOL_ID_CODE11		'H'
#define SYMBOL_ID_MSI			'J'
#define SYMBOL_ID_UCCEAN128		'K'
#define SYMBOL_ID_BOOKLANDEAN	'L'
#define SYMBOL_ID_TRIOPTIC39	'M'
#define SYMBOL_ID_COUPON		'N'
#define SYMBOL_ID_PDF417		'X'
#define SYMBOL_ID_MICROPDF		'X'
#define SYMBOL_ID_DATAMATRIX	'P'
	#define SYMBOL_ID_SUPP2_DATAMATRIX		'0'
#define SYMBOL_ID_QRCODE		'P'
	#define SYMBOL_ID_SUPP2_QRCODE			'1'
#define SYMBOL_ID_MAXICODE		'P'
	#define SYMBOL_ID_SUPP2_MAXICODE		'2'
#define SYMBOL_ID_POSTAL		'P'
	#define SYMBOL_ID_SUPP2_USPOSTNET		'3'
	#define SYMBOL_ID_SUPP2_USPLANET		'4'
	#define SYMBOL_ID_SUPP2_JAPPOSTAL		'5'
	#define SYMBOL_ID_SUPP2_UKPOSTAL		'6'
	#define SYMBOL_ID_SUPP2_DUTCHPOSTAL		'8'
	#define SYMBOL_ID_SUPP2_AUSPOSTAL		'9'
#define SYMBOL_ID_IMAGE			'X'


//--------------------------------------------------------------------
// AIM label ID codes
//--------------------------------------------------------------------

#define AIM_ID_CODE39			'A'
#define AIM_ID_CODE128			'C'
#define AIM_ID_UCCEAN128		'C'
#define AIM_ID_DATAMATRIX		'd'
#define AIM_ID_UPCEAN			'E'
#define AIM_ID_CODABAR			'F'
#define AIM_ID_CODE93			'G'
#define AIM_ID_CODE11			'H'
#define AIM_ID_I2OF5			'I'
#define AIM_ID_PDF417			'L'
#define AIM_ID_MICROPDF			'L'
#define AIM_ID_MSI				'M'
#define AIM_ID_QRCODE			'Q'
#define AIM_ID_D2OF5			'S'
#define AIM_ID_IATA2OF5			'S'
#define AIM_ID_MAXICODE			'U'
#define AIM_ID_BOOKLANDEAN		'X'
#define AIM_ID_TRIOPTIC39		'X'
#define AIM_ID_COUPON			'X'
#define AIM_ID_POSTAL			'X'
#define AIM_ID_IMAGE			'Z'

//--------------------------------------------------------------------
// Image file format type definitions
//		Note:	Each image format constant is a bit mask.
//--------------------------------------------------------------------

#define IMAGE_FORMAT_JPEG					0x00000001


//--------------------------------------------------------------------
// Constant for all timers (DWORD) indicating infinite duration
//--------------------------------------------------------------------

#define TIMEOUT_INFINITE	0


//--------------------------------------------------------------------
// Types
//--------------------------------------------------------------------


enum tagDIRECTIONS
{
	DIR_FORWARD=1,
	DIR_REVERSE,
};


// Find information structure
//   Find information structure is filled in by C API Dll by
//     enumerating and reading the registry keys under
//     ..\Drivers\Active\..  and ..\Drivers\BuiltIn\..

typedef struct tagSCAN_FINDINFO_W
{
	STRUCT_INFO	StructInfo;

	WCHAR szDeviceName[MAX_DEVICE_NAME];
	WCHAR szPortName[MAX_DEVICE_NAME];
	WCHAR szFriendlyName[MAX_PATH];
	WCHAR szRegistryBasePath[MAX_PATH];

} SCAN_FINDINFO_W;

typedef SCAN_FINDINFO_W FAR * LPSCAN_FINDINFO_W;

typedef struct tagSCAN_FINDINFO_A
{
	STRUCT_INFO	StructInfo;

	CHAR szDeviceName[MAX_DEVICE_NAME];
	CHAR szPortName[MAX_DEVICE_NAME];
	CHAR szFriendlyName[MAX_PATH];
	CHAR szRegistryBasePath[MAX_PATH];

} SCAN_FINDINFO_A;

typedef SCAN_FINDINFO_A FAR * LPSCAN_FINDINFO_A;

#ifdef UNICODE
#define SCAN_FINDINFO SCAN_FINDINFO_W
#define LPSCAN_FINDINFO LPSCAN_FINDINFO_W
#else
#define SCAN_FINDINFO SCAN_FINDINFO_A
#define LPSCAN_FINDINFO LPSCAN_FINDINFO_A
#endif


// Version information structure
//   Version information for the PDD is cached by the MDD during its
//     initialization.  The MDD and C API add their version
//     information to the structure on demand

typedef struct tagSCAN_VERSION_INFO
{
	STRUCT_INFO	StructInfo;

	DWORD		dwHardwareVersion;	// HIWORD=major, LOWORD=minor
	DWORD		dwDecoderVersion;	// HIWORD=major, LOWORD=minor
	DWORD		dwPddVersion;		// HIWORD=major, LOWORD=minor
	DWORD		dwMddVersion;		// HIWORD=major, LOWORD=minor
	DWORD		dwCAPIVersion;		// HIWORD=major, LOWORD=minor

} SCAN_VERSION_INFO;

typedef SCAN_VERSION_INFO FAR * LPSCAN_VERSION_INFO;


#define MAX_SRC 32

// Scan buffer structure

typedef struct tagSCAN_BUFFER_W
{
	STRUCT_INFO		StructInfo;

	DWORD			dwDataBuffSize;		// Size of data buffer in bytes
	DWORD			dwOffsetDataBuff;	// Offset from start of struct to data buffer
	DWORD			dwDataLength;		// Length of data placed into buffer

	DWORD			dwTimeout;			// Read timeout in milliseconds
										//  Set by application via C API

	DWORD			dwStatus;			// Status of the read request
										//  Set by PDD and by MDD
										//   E_SCN_SUCCESS if complete
										//   E_SCN_DEVICEFAILURE if disconnected
										//   E_SCN_READPENDING while pending
										//   or some other error if appropriate

	BOOL			bText;				// Data is text
										//  Set by application via C API

	LABELTYPE		dwLabelType;		// Symbol label type code
										//  Set by PDD after good read

	DWORD			dwRequestID;		// Request ID assigned to the read
										//  Set by MDD

	SYSTEMTIME		TimeStamp;			// System time when read completed
										//  Set by MDD after good read

	DWORD			dwDirection;		// Direction label was scanned
										//  Set by PDD after good read

	WCHAR			szSource[MAX_SRC];	// SCNx:Symbology
										//  Set by MDD after good read

} SCAN_BUFFER_W;

typedef SCAN_BUFFER_W FAR * LPSCAN_BUFFER_W;


typedef struct tagSCAN_BUFFER_A
{
	STRUCT_INFO		StructInfo;

	DWORD			dwDataBuffSize;		// Size of data buffer in bytes
	DWORD			dwOffsetDataBuff;	// Offset from start of struct to data buffer
	DWORD			dwDataLength;		// Length of data placed into buffer

	DWORD			dwTimeout;			// Read timeout in milliseconds
										//  Set by application via C API

	DWORD			dwStatus;			// Status of the read request
										//  Set by PDD and by MDD
										//   E_SCN_SUCCESS if complete
										//   E_SCN_DEVICEFAILURE if disconnected
										//   E_SCN_READPENDING while pending
										//   or some other error if appropriate

	BOOL			bText;				// Data is text
										//  Set by application via C API

	LABELTYPE		dwLabelType;		// Symbol label type code
										//  Set by PDD after good read

	DWORD			dwRequestID;		// Request ID assigned to the read
										//  Set by MDD

	SYSTEMTIME		TimeStamp;			// System time when read completed
										//  Set by MDD after good read

	DWORD			dwDirection;		// Direction label was scanned
										//  Set by PDD after good read

	CHAR			szSource[MAX_SRC];	// SCNx:Symbology
										//  Set by MDD after good read

} SCAN_BUFFER_A;

typedef SCAN_BUFFER_A FAR * LPSCAN_BUFFER_A;


#ifdef UNICODE
#define SCAN_BUFFER SCAN_BUFFER_W
#define LPSCAN_BUFFER LPSCAN_BUFFER_W
#else
#define SCAN_BUFFER SCAN_BUFFER_A
#define LPSCAN_BUFFER LPSCAN_BUFFER_A
#endif


// UPC dwPreamble values
enum tagPREAMBLE
{
	PREAMBLE_NONE = 0,
	PREAMBLE_SYSTEM_CHAR = 1,
	PREAMBLE_COUNTRY_AND_SYSTEM_CHARS = 2,
};


// MSI dwCheckDigitScheme values
enum tagMSICHECKDIGITSCHEME
{
	MSI_CHKDGT_MOD_11_10 = 0,
	MSI_CHKDGT_MOD_10_10 = 1,
};


// MSI dwCheckDigits values
enum tagMSICHECKDIGITS
{
	MSI_ONE_CHECK_DIGIT = 0,
	MSI_TWO_CHECK_DIGIT = 1,
};


// I2OF5 dwVerifyCheckDigit values
enum tagI2OF5CHECKDIGITS
{
	I2OF5_NO_CHECK_DIGIT = 0,
	I2OF5_USS_CHECK_DIGIT = 1,
	I2OF5_OPCC_CHECK_DIGIT = 2,
};


// CODE 11 dwCheckDigitCount values
enum tagCODE11CHECKDIGITS
{
	CODE11_NO_CHECK_DIGIT = 0,
	CODE11_ONE_CHECK_DIGIT = 1,
	CODE11_TWO_CHECK_DIGIT = 2,
};


// Decoder specific parameters for Get/Set Decoder Parameters subcommands

typedef struct tagUPCEO_PARAMS
{
	BOOL		bReportCheckDigit;	// Check digit report enabled

	DWORD		dwPreamble; 		// Preamble to add
									//   See tagPREAMBLE above

	BOOL		bConvertToUPCA;	 	// Convert to UPC A

} UPCE0_PARAMS;


typedef struct tagUPCE1_PARAMS
{
	BOOL		bReportCheckDigit;	// Check digit report enabled

	DWORD		dwPreamble; 		// Preamble to add
									//   See tagPREAMBLE above

	BOOL		bConvertToUPCA;	 	// Convert to UPC A

} UPCE1_PARAMS;


typedef struct tagUPCA_PARAMS
{
	BOOL		bReportCheckDigit;	// Check digit report enabled

	DWORD		dwPreamble; 		// Preamble to add
									//   See tagPREAMBLE above

} UPCA_PARAMS;


typedef struct tagMSI_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

	DWORD		dwCheckDigits;		// Number of check digits
									//   See tagCHECKDIGITS above

	DWORD		dwCheckDigitScheme;	// Check digit scheme
									//   See tagCHECKDIGITSCHEME above

	BOOL		bReportCheckDigit;	// Check digit report enabled

} MSI_PARAMS;


typedef struct tagEAN8_PARAMS
{
	BOOL		bConvertToEAN13;	// Convert to EAN 13

} EAN8_PARAMS;


typedef struct tagCODABAR_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

	BOOL		bClsiEditing;		// Clsi editing enabled

	BOOL		bNotisEditing;		// Notis editing enabled

} CODABAR_PARAMS;


typedef struct tagCODE39_PARAMS
{
	BOOL		bVerifyCheckDigit;	// Check digit verify enabled

	BOOL		bReportCheckDigit;	// Check digit report enabled

	BOOL		bConcatenation;		// Concatenation enabled

	BOOL		bFullAscii;			// Full ASCII conversion enabled

	BOOL		bRedundancy; 		// Redundancy enabled

	BOOL		bConvertToCode32;	// Convert to Code 32

	BOOL		bCode32Prefix;		// Code 32 Prefix report enable

} CODE39_PARAMS;


typedef struct tagTRIOPTIC39_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

} TRIOPTIC39_PARAMS;


typedef struct tagD2OF5_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

} D2OF5_PARAMS;


typedef struct tagI2OF5_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

	DWORD		dwVerifyCheckDigit;	// Check digit verify

	BOOL		bReportCheckDigit;	// Check digit report enabled

	BOOL		bConvertToEAN13;	// Convert to EAN 13

} I2OF5_PARAMS;


typedef struct tagCODE11_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

	DWORD		dwCheckDigitCount;	// Number of check digits

	BOOL		bReportCheckDigit;	// Check digit report enabled

} CODE11_PARAMS;


typedef struct tagCODE93_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

} CODE93_PARAMS;


typedef struct tagCODE128_PARAMS
{
	BOOL		bRedundancy; 		// Redundancy enabled

	// Sub type enable flags
	//  Note: At least one sub type must be enabled
	//        Defaults are ALL ENABLED

	BOOL		bEAN128;			// EAN128 Sub type enabled

	BOOL		bISBT128;			// ISBT128 Sub type enabled
	
	BOOL		bOther128;			// Other (non EAN or ISBT) 128 Sub types enabled

} CODE128_PARAMS;


typedef struct tagIMAGE_PARAMS
{
	RECT		CroppingRect;			// Pixel addresses to crop to
										//   Limited by DEVICE_INFO.MaxImageRect

	DWORD		dwResolutionDivisor;	// Resolution 1/x (multiple pixels are combined)
										//   (0 = 1 = Full resolution)

	BOOL		bEnableAiming;			// Enable laser aiming during image capture
										//   Enables/Disables use of aiming reader parameters
										//   for image capture.

	BOOL		bEnableIllumination;	// Enable illumination during image capture

	DWORD		dwImageFormat;			// Requested image file format

	//  Note: JPEG image will be optimized for EITHER Quality or Size.
	//        Size restrictions take priority over quality restrictions.

	DWORD		dwJpegImageQuality;		// Quality of compressed image
										//   Range 0 - 100, where 100 is highest quality image
										//   0 means optimize for image size

	DWORD		dwJpegImageSize;		// Maximum compressed image size in kilobytes (KB)
										//   0 means optimize for image quality

} IMAGE_PARAMS;


typedef struct tagSIGNATURE_PARAMS
{
	DWORD		dwImageFormat;			// Requested image file format

	//  Note: JPEG image will be optimized for EITHER Quality or Size.
	//        Size restrictions take priority over quality restrictions.

	DWORD		dwJpegImageQuality;		// Quality of compressed image
										//   Range 0 - 100, where 100 is highest quality image
										//   0 means optimize for image size

	DWORD		dwJpegImageSize;		// Maximum compressed image size in kilobytes (KB)
										//   0 means optimize for image quality

} SIGNATURE_PARAMS;


// Decoder Specific Decoder Parameter Structure

typedef union tagDECODER_SPECIFIC
{
	DWORD				dwUntyped[20];			// For init and anonymous access
	UPCE0_PARAMS		upce0_params;			// UPC E0 specific parameters
	UPCE1_PARAMS		upce1_params;			// UPC E1 specific parameters
	UPCA_PARAMS			upca_params; 			// UPC A specific parameters
	MSI_PARAMS			msi_params;				// MSI specific parameters
	EAN8_PARAMS			ean8_params; 			// EAN 8 specific parameters
	CODABAR_PARAMS		codabar_params;			// Codabar specific parameters
	CODE39_PARAMS		code39_params;			// Code 39 specific parameters
	D2OF5_PARAMS		d2of5_params;			// D 2 of 5 specific parameters
	I2OF5_PARAMS		i2of5_params;			// I 2 of 5 specific parameters
	CODE11_PARAMS		code11_params;			// Code 11 specific parameters
	CODE93_PARAMS		code93_params;			// Code 93 specific parameters
	CODE128_PARAMS		code128_params;			// Code 128 specific parameters
	TRIOPTIC39_PARAMS	trioptic39_params;		// Trioptic 3 of 9 specific parameters
	IMAGE_PARAMS		image_params;			// Image specific parameters
	SIGNATURE_PARAMS	signature_params;		// Signature specific parameters

} DECODER_SPECIFIC;


// Get/Set Decoder Parameters parameter structure
//   Decoder Parameters for Enabled Decoders are cached with each
//     read request and are used by MDD in combining multiple
//     reads to be passed to PDD

typedef struct
{
	STRUCT_INFO	StructInfo;

	DECODER		cDecoder;			// Decoder type

	DWORD		dwMinLength;		// Minimum length
	DWORD		dwMaxLength;		// Maximum length

	DECODER_SPECIFIC dec_specific;	// decoder specific params

} DECODER_PARAMS;

typedef DECODER_PARAMS FAR * LPDECODER_PARAMS;


// List of decoders

typedef struct tagDECODERS
{
	DWORD dwDecoderCount;				// Number of decoders used in list

	BYTE byList[DECODERS_BUFLEN];		// List of decoders

} DECODERS;

typedef DECODERS FAR * LPDECODERS;


// Get/Set Enabled Decoders and Get Supported Decoders parameter structure
//   Enabled Decoders list is cached with each read request and is
//     used by MDD in combining multiple reads to be passed to PDD

typedef struct
{
	STRUCT_INFO	StructInfo;

	DECODERS	Decoders;	// List of supported or enabled decoders

} DECODER_LIST;

typedef DECODER_LIST FAR * LPDECODER_LIST;


// Aim Types
enum tagAIMTYPE
{
	AIM_TYPE_TRIGGER = 0,
	AIM_TYPE_TIMED_HOLD = 1,
	AIM_TYPE_TIMED_RELEASE = 2,
};


// Aim Modes
enum tagAIMMODE
{
	AIM_MODE_NONE = 0,
	AIM_MODE_DOT = 1,
	AIM_MODE_SLAB = 2,
	AIM_MODE_RETICLE = 3,
};


// Laser Specific Raster Modes
enum tagRASTERMODE
{
	RASTER_MODE_NONE = 0,
	RASTER_MODE_OPEN_ALWAYS = 1,
	RASTER_MODE_SMART = 2,
	RASTER_MODE_CYCLONE = 3,
};


// Laser Specific Linear Security Levels
enum tagLINEARSECURITYLEVEL
{
	SECURITY_REDUNDANCY_AND_LENGTH = 0,	// x2 Redundancy based on redundancy flag and length
	SECURITY_SHORT_OR_CODABAR = 1,		// x2 Redundancy if short barcode or codabar.
	SECURITY_ALL_TWICE = 2,				// x2 Redundancy for all code types.
	SECURITY_LONG_AND_SHORT = 3,		// x2 Redunancy for long barcode, x3 for short
	SECURITY_ALL_THRICE = 4,			// x3 Redundancy for all code types
};


// Laser Specific Interface Specific Reader Parameters

typedef struct tagLASER_SPECIFIC
{
	// PDD parameters

	DWORD		dwAimType;		// Aim type to use
								//  See tagAIMTYPE above
								//  (used and validated by MDD)
	
	DWORD		dwAimDuration;	// Duration in ms for timed aim modes
								//  (used and validated by MDD)

	DWORD		dwAimMode;		// Aim mode to use
								//  See tagAIMMODE above
								//  (used and validated by PDD)

	BOOL		bNarrowBeam;	// Horizontal beam width to use
								//  (used and validated by PDD)

	DWORD		dwRasterMode;	// Vertical rastering to use
								//  See tagRASTERMODE above
								//  (used and validated by PDD)

	DWORD		dwBeamTimer;	// Beam timer (max laser on in ms)
								//  (used and validated by PDD)

	BOOL		bControlScanLed;	// Scanning LED control flag
									//  (used and validated by PDD)

	BOOL		bScanLedLogicLevel;	// Scanning LED logic level
									//  (used and validated by PDD)

	BOOL		bKlasseEinsEnable;	// Klasse Eins flag
									//  (used and validated by PDD)

	BOOL		bBidirRedundancy;	// Birdirectional redundancy enabled
									//  (used and validated by PDD)

	DWORD		dwLinearSecurityLevel;	// Linear security level
										//  (used and validated by PDD)

	DWORD		dwPointerTimer;	// Beam timer for pointer mode
								//  (used and validated by PDD)

} LASER_SPECIFIC;


// Contact Specific Interface Specific Reader Parameters

typedef struct tagCONTACT_SPECIFIC
{
	// PDD parameters

	DWORD		dwQuietZoneRatio;	// Quiet zone ration x:1 to 1:x
									//  (used and validated by PDD)

	DWORD		dwInitialScanTime;	// Initial scan time in ms
									//  (used and validated by PDD)
	
	DWORD		dwPulseDelay;		// Pulse delay time in ms
									//  (used and validated by PDD)

} CONTACT_SPECIFIC;


// Imager Specific Interface Specific Reader Parameters

typedef struct tagIMAGER_SPECIFIC
{

	DWORD		dwAimType;		// Aim type to use
								//  See tagAIMTYPE above
								//  (used and validated by MDD)
	
	DWORD		dwAimDuration;	// Duration in ms for timed aim modes
								//  (used and validated by MDD)

	DWORD		dwAimMode;		// Aim mode to use
								//  See tagAIMMODE above
								//  (used and validated by PDD)

	DWORD		dwBeamTimer;	// Beam timer (max imager on in ms)
								//  (used and validated by PDD)

	DWORD		dwPointerTimer;	// Beam timer for pointer mode
								//  (used and validated by PDD)

	DWORD		dwImageCaptureTimeout;
								// Timeout for image capture (ms)
								//  (used and validated by PDD)

	DWORD		dwImageCompressionTimeout;	
								// Timeout for image compression / processing (ms)
								//  (used and validated by PDD)

	DWORD		dwLinearSecurityLevel;
								// Linear security level
								//  (used and validated by PDD)

} IMAGER_SPECIFIC;


// Reader Specific Interface Specific Reader Parameters structure

typedef union tagREADER_SPECIFIC
{
	DWORD dwUntyped[20];					// For init and anonymous access
	
	LASER_SPECIFIC		laser_specific;		// Laser reader specific parameters

	CONTACT_SPECIFIC	contact_specific;	// Contact wand reader specific parameters

	IMAGER_SPECIFIC		imager_specific;	// Imager reader specific parameters

} READER_SPECIFIC;


// Reader Parameters Reader Types
enum tagREADER_TYPE
{
	READER_TYPE_LASER = 0,
	READER_TYPE_CONTACT = 1,
	READER_TYPE_IMAGER = 2,
};


// Get/Set Reader Parameters structure
//   Reader parameters are global to all reads on all open
//     handles on the same PDD

typedef struct tagREADER_PARAMS
{
	STRUCT_INFO	StructInfo;

	DWORD		dwReaderType;			// Type of reader
										// (Read Only)
										//  See tagREADERTYPE above
										//  (used and validated by PDD)

	READER_SPECIFIC	ReaderSpecific;		// Reader specific params

} READER_PARAMS;

typedef READER_PARAMS FAR * LPREADER_PARAMS;


// QSNAC Interface Specific Reader Parameters structure

typedef struct tagQSNAC_SPECIFIC
{
	// Nondecoded engine hardware reader parameters

	// PDD parameters

	DWORD		dwEnableSettlingTime;	// Enable settle time in ms
										//  (used and validated by PDD)

	BOOL		bInverseLabelData;		// Inverse label data
										//  (used and validated by PDD)

	BOOL		bWhiteDataLogicLevel;	// White data logic level
										//  (used and validated by PDD)

	DWORD		dwTransitionResolution;	// Transition resolution value
										//  (sampling clock divisor)
										//  (used and validated by PDD)

	DWORD		dwPowerSettlingTime;	// Power settling time in ms
										//  (used and validated by PDD)

} QSNAC_SPECIFIC;

typedef QSNAC_SPECIFIC FAR * LPQSNAC_SPECIFIC;


// SSI Interface Specific Reader Parameters structure

typedef struct tagSSI_SPECIFIC
{
	DWORD		dwPowerSettlingTime;	// Power settling time in ms
										//  (used and validated by PDD)

} SSI_SPECIFIC;

typedef SSI_SPECIFIC FAR * LPSSI_SPECIFIC;


// LS-48XX Interface Specific Reader Parameters structure

typedef struct tagLS48XX_SPECIFIC
{
	DWORD		dwPowerSettlingTime;	// Power settling time in ms
										//  (used and validated by PDD)

} LS48XX_SPECIFIC;

typedef LS48XX_SPECIFIC FAR * LPLS48XX_SPECIFIC;


// Interface Specific Reader Parameters structure

typedef union tagINTERFACE_SPECIFIC
{
	DWORD dwUntyped[20];			// For init and anonymous access
	
	QSNAC_SPECIFIC QsnacSpecific;	// QSNAC interface specific parameters

	SSI_SPECIFIC SsiSpecific;		// SSI interface specific parameters

	LS48XX_SPECIFIC Ls48xxSpecific;	// LS48XX interface specific parameters

} INTERFACE_SPECIFIC;


// Reader Parameters Interface Types
enum tagINTERFACE_TYPE
{
	INTERFACE_TYPE_QSNAC = 0,
	INTERFACE_TYPE_SSI = 1,
	INTERFACE_TYPE_LS48XX = 2,
};


// Get/Set Interface Parameters structure
//   Interface parameters are global to all reads on all open
//     handles on the same PDD

typedef struct tagINTERFACE_PARAMS
{
	STRUCT_INFO	StructInfo;

	DWORD		dwInterfaceType;			// Interface type used to attach scanner
											//  See tagINTERFACETYPE above
											//  (read only)
											//  (used and filled in by PDD)
											//  (determined during StartDevice)

	INTERFACE_SPECIFIC InterfaceSpecific;	// I/F specific parameters

} INTERFACE_PARAMS;

typedef INTERFACE_PARAMS FAR * LPINTERFACE_PARAMS;


// Special parameter value used when no change is desired
#define PARAMETER_NO_CHANGE 0xFFFFFFFF


// Scan Parameters Code ID Types
enum tagCODEIDTYPE
{
	CODE_ID_TYPE_NONE = 0,
	CODE_ID_TYPE_SYMBOL = 1,
	CODE_ID_TYPE_AIM = 2,
};


// Scan Parameters Scan Types
enum tagSCANTYPE
{
	SCAN_TYPE_FOREGROUND = 0,
	SCAN_TYPE_BACKGROUND = 1,
	SCAN_TYPE_MONITOR = 2,
};


// Get/Set Scan Parameters structure
//   Scan Parameters are cached with each read request but are
//    NOT used by MDD in combining multiple reads to be passed to PDD
//    they are instead used by the MDD for post-read-completion
//    activities (such as feedback production or code type reporting)
//    for the read actually satisfied

typedef struct tagSCAN_PARAMS_W
{
	STRUCT_INFO	StructInfo;

	DWORD		dwCodeIdType;	// Type of code ID to be reported
								//     prepended to data in MDD
								//     ignored by PDD

	DWORD		dwScanType;		// Type of scan request

	// Feedback parameters: Used by MDD directly
	//						Used indirectly by PDD(via DoFeedback)

	BOOL		bLocalFeedback;		// Local feedback flag
									//  (used and validated by MDD)
									//   0 = PDD does feedback
									//  *1 = MDD does feedback

	DWORD		dwDecodeBeepTime;		// Decode beep time
	DWORD		dwDecodeBeepFrequency;	// Decode beep frequency
	DWORD		dwDecodeLedTime;		// Decode LED time

	WCHAR		szWaveFile[MAX_PATH];	// Name of .WAV file to play as feedback
										//  (uses sndPlaySound)

	// Feedback for start event
	DWORD		dwStartBeepTime;			// Start beep time
	DWORD		dwStartBeepFrequency;		// Start beep frequency
	DWORD		dwStartLedTime;				// Start LED time
	WCHAR		szStartWaveFile[MAX_PATH];	// Name of .WAV file to play as start feedback

	// Feedback for intermediate event
	DWORD		dwIntermedBeepTime;			// Intermediate beep time
	DWORD		dwIntermedBeepFrequency;	// Intermediate beep frequency
	DWORD		dwIntermedLedTime;			// Intermediate LED time
	WCHAR		szIntermedWaveFile[MAX_PATH];	// Name of .WAV file to play as intermediate feedback

	// Feedback for fatal error event
	DWORD		dwFatalBeepTime;			// Fatal error beep time
	DWORD		dwFatalBeepFrequency;		// Fatal error beep frequency
	DWORD		dwFatalLedTime;				// Fatal error LED time
	WCHAR		szFatalWaveFile[MAX_PATH];	// Name of .WAV file to play as fatal error feedback

	// Feedback for non-fatal error event
	DWORD		dwNonfatalBeepTime;			// Non-fatal error beep time
	DWORD		dwNonfatalBeepFrequency;	// Non-fatal error beep frequency
	DWORD		dwNonfatalLedTime;			// Non-fatal error LED time
	WCHAR		szNonfatalWaveFile[MAX_PATH];	// Name of .WAV file to play as non-fatal error feedback

	// Feedback for activity event
	DWORD		dwActivityBeepTime;			// Activity beep time
	DWORD		dwActivityBeepFrequency;	// Activity beep frequency
	DWORD		dwActivityLedTime;			// Activity LED time
	WCHAR		szActivityWaveFile[MAX_PATH];	// Name of .WAV file to play as activity feedback

} SCAN_PARAMS_W;

typedef SCAN_PARAMS_W FAR * LPSCAN_PARAMS_W;

typedef struct tagSCAN_PARAMS_A
{
	STRUCT_INFO	StructInfo;

	DWORD		dwCodeIdType;	// Type of code ID to be reported
								//     prepended to data in MDD
								//     ignored by PDD

	DWORD		dwScanType;		// Type of scan request

	// Feedback parameters: Used by MDD directly
	//						Used indirectly by PDD(via DoFeedback)

	BOOL		bLocalFeedback;		// Local feedback flag
									//  (used and validated by MDD)
									//   0 = PDD does feedback
									//  *1 = MDD does feedback

	DWORD		dwDecodeBeepTime;		// Decode beep time
	DWORD		dwDecodeBeepFrequency;	// Decode beep frequency
	DWORD		dwDecodeLedTime;		// Decode LED time

	CHAR		szWaveFile[MAX_PATH];	// Name of .WAV file to play as feedback
										//  (uses sndPlaySound)

	// Feedback for start event
	DWORD		dwStartBeepTime;			// Start beep time
	DWORD		dwStartBeepFrequency;		// Start beep frequency
	DWORD		dwStartLedTime;				// Start LED time
	CHAR		szStartWaveFile[MAX_PATH];	// Name of .WAV file to play as start feedback

	// Feedback for intermediate event
	DWORD		dwIntermedBeepTime;			// Intermediate beep time
	DWORD		dwIntermedBeepFrequency;	// Intermediate beep frequency
	DWORD		dwIntermedLedTime;			// Intermediate LED time
	CHAR		szIntermedWaveFile[MAX_PATH];	// Name of .WAV file to play as intermediate feedback

	// Feedback for fatal error event
	DWORD		dwFatalBeepTime;			// Fatal error beep time
	DWORD		dwFatalBeepFrequency;		// Fatal error beep frequency
	DWORD		dwFatalLedTime;				// Fatal error LED time
	CHAR		szFatalWaveFile[MAX_PATH];	// Name of .WAV file to play as fatal error feedback

	// Feedback for non-fatal error event
	DWORD		dwNonfatalBeepTime;			// Non-fatal error beep time
	DWORD		dwNonfatalBeepFrequency;	// Non-fatal error beep frequency
	DWORD		dwNonfatalLedTime;			// Non-fatal error LED time
	CHAR		szNonfatalWaveFile[MAX_PATH];	// Name of .WAV file to play as non-fatal error feedback

	// Feedback for activity event
	DWORD		dwActivityBeepTime;			// Activity beep time
	DWORD		dwActivityBeepFrequency;	// Activity beep frequency
	DWORD		dwActivityLedTime;			// Activity LED time
	CHAR		szActivityWaveFile[MAX_PATH];	// Name of .WAV file to play as activity feedback

} SCAN_PARAMS_A;

typedef SCAN_PARAMS_A FAR * LPSCAN_PARAMS_A;

#ifdef UNICODE
#define SCAN_PARAMS SCAN_PARAMS_W
#define LPSCAN_PARAMS LPSCAN_PARAMS_W
#else
#define SCAN_PARAMS SCAN_PARAMS_A
#define LPSCAN_PARAMS LPSCAN_PARAMS_A
#endif


enum tagSCANSTATUS
{
	SCAN_STATUS_STOPPED = 1,		// Scanner is not enabled
	SCAN_STATUS_IDLE = 2,			// Scanner is enabled but no reads are pending
	SCAN_STATUS_WAITING = 3,		// One or more reads are pending, waiting for trigger event
	SCAN_STATUS_SCANNING = 4,		// Beam is on an acquiring data
	SCAN_STATUS_AIMING = 5,			// Beam is on for aiming
	SCAN_STATUS_EMPTY = 6,			// Beam is off waiting for Klasse Eins Gas Tank to recover
};


// Get Scan Status parameter structure

typedef struct tagSCAN_STATUS
{
	STRUCT_INFO StructInfo;

	BOOL			bEnabled;				// Scan enabled flag

	DWORD			dwStatus;				// Current scan status
											//  See tagSCANSTATUS above

	DWORD			dwKlasseEinsTimeUsed;	// Klasse Eins time used in ms

	DWORD			dwKlasseEinsTimeLeft;	// Klasse Eins time left in ms

} SCAN_STATUS;

typedef SCAN_STATUS FAR * LPSCAN_STATUS;


enum tagUPCEANSECURITYLEVEL
{
	UPCEAN_SECURITY_NONE = 0,
	UPCEAN_SECURITY_AMBIGUOUS = 1,
	UPCEAN_SECURITY_ALL = 2
};

enum tagSUPPLEMENTALMODE
{
	SUPPLEMENTALS_NONE = 0,
	SUPPLEMENTALS_ALWAYS = 1,
	SUPPLEMENTALS_AUTO = 2
};


// Get/Set UPC/EAN General Parameters parameter structure
//   UPC/EAN General Parameters are cached with each read request and
//     are used by MDD in combining multiple reads to be passed to PDD

typedef struct tagUPC_EAN_PARAMS
{
	STRUCT_INFO	StructInfo;

	DWORD		dwSecurityLevel;			// Security Level value
											//  See tagSECURITYLEVEL

	BOOL		bSupplemental2;				// Length 2 supplemental enabled

	BOOL		bSupplemental5;				// Length 5 supplemental enabled

	DWORD		dwSupplementalMode;			// Supplemental mode
											//  See tagSUPPLEMENTALMODE

	DWORD		dwRetryCount;				// Retry count for auto-
											//  discriminating
											//  for supplementals

	BOOL		bRandomWeightCheckDigit;	// Random weight check digit enabled

	BOOL		bLinearDecode;				// Linear decode enabled

	BOOL		bBookland;					// Bookland code enabled

	BOOL		bCoupon;					// Coupon code enabled

} UPC_EAN_PARAMS;

typedef UPC_EAN_PARAMS FAR * LPUPC_EAN_PARAMS;


// Device capabilities information structure

typedef struct tagDEVICE_INFO
{
	STRUCT_INFO	StructInfo;

	DECODERS	Decoders;	// List of supported or enabled decoders
							//   for this PDD, can include
							//   'P' for pointer mode
	
	BOOL	bBeamWidth;		// Device supports beam width control
	BOOL	bAimMode;		// Device supports aim mode(s)
	BOOL	bDirection;		// Device supports scan direction reporting
	BOOL	bFeedback;		// Device has its own feedback mechanism

	DWORD	dwSupportedImageFormats;	// Bit mask of supported image formats
	RECT	MaxImageRect;	// Maximum image cropping rectangle (pixels)

} DEVICE_INFO;

typedef DEVICE_INFO FAR * LPDEVICE_INFO;


// Scan Event Codes
//   For message notification, event code passed as wParam
enum tagSCAN_EVENT_CODE
{
	SCAN_EVENT_ERROR = 0,				// An error occurred while trying to wait for an event

	SCAN_EVENT_STATE_CHANGE,			// The PDD state has changed
										//   Call SCAN_GetScanStatus for new status
										//   PDD passes SCAN_STATUS structure as param
										//   For message notification, dwStatus passed as lParam

	// Following events only sent by MDD to open scanner handles causing event

	SCAN_EVENT_ACTIVITY,				// The scanner driver is busy / active
										//   MDD should do activity feedback
										//   PDD passes READ_SPECIFIER structure as param
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_IMAGE_CAPTURE,			// The PDD has captured an image and begun download
										//   MDD should do start feedback
										//   PDD passes READ_SPECIFIER structure as param
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_IMAGE_ERROR,				// The PDD has encountered a fatal error downloading image
										//   MDD should do fatal error feedback
										//   PDD passes READ_SPECIFIER structure as param
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_START_SEQUENCE,			// The MDD has captured the first barcode in a concatenation sequence
										//   MDD should do start feedback
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_SEQUENCE_CONTINUE,		// The MDD has captured another barcode in a concatenation sequence
										//   MDD should do intermediate feedback
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_SEQUENCE_FAIL,			// The MDD has encountered a fatal concatenation error
										//   MDD should do fatal error feedback
										//   For message notification, dwRequestID passed as lParam

	SCAN_EVENT_SEQUENCE_ERROR,			// The MDD has encountered a non-fatal concatenation error
										// i.e. non-sequence barcode scanned
										//   MDD should do non-fatal error feedback
										//   For message notification, dwRequestID passed as lParam
};


// Beep Patterns
enum tagBEEPPATTERN
{
    BEEP_PATTERN_ONE_SHORT_HIGH = 0,
    BEEP_PATTERN_TWO_SHORT_HIGH = 1,
    BEEP_PATTERN_THREE_SHORT_HIGH = 2,
    BEEP_PATTERN_FOUR_SHORT_HIGH = 3,
    BEEP_PATTERN_FIVE_SHORT_HIGH = 4,

    BEEP_PATTERN_ONE_SHORT_LOW = 5,
    BEEP_PATTERN_TWO_SHORT_LOW = 6,
    BEEP_PATTERN_THREE_SHORT_LOW = 7,
    BEEP_PATTERN_FOUR_SHORT_LOW = 8,
    BEEP_PATTERN_FIVE_SHORT_LOW = 9,

    BEEP_PATTERN_ONE_LONG_HIGH = 10,
    BEEP_PATTERN_TWO_LONG_HIGH = 11,
    BEEP_PATTERN_THREE_LONG_HIGH = 12,
    BEEP_PATTERN_FOUR_LONG_HIGH = 13,
    BEEP_PATTERN_FIVE_LONG_HIGH = 14,

    BEEP_PATTERN_ONE_LONG_LOW = 15,
    BEEP_PATTERN_TWO_LONG_LOW = 16,
    BEEP_PATTERN_THREE_LONG_LOW = 17,
    BEEP_PATTERN_FOUR_LONG_LOW = 18,
    BEEP_PATTERN_FIVE_LONG_LOW = 19,

    BEEP_PATTERN_FAST_WARBLE = 20,
    BEEP_PATTERN_SLOW_WARBLE = 21,
    BEEP_PATTERN_MIX1 = 22,
    BEEP_PATTERN_MIX2 = 23,
    BEEP_PATTERN_MIX3 = 24,
    BEEP_PATTERN_MIX4 = 25,
};


// Led Patterns
//   The values 1 to 127 are interpreted as bit masks for 7 Leds
//   The values 128 to 255 are interpreted as blink patterns
enum tagLEDPATTERN
{
	LED_PATTERN_DECODE = 1,
	LED_PATTERN_LED1 = 2,
	LED_PATTERN_LED2 = 4,
	LED_PATTERN_LED3 = 8,
	LED_PATTERN_LED4 = 16,
	LED_PATTERN_LED5 = 32,
	LED_PATTERN_LED6 = 64,

	// Blinking patterns
	LED_PATTERN_BLACK = 128,
	LED_PATTERN_PULSE_GREEN = 129,
	LED_PATTERN_PULSE_YELLOW = 130,
	LED_PATTERN_PULSE_RED = 131,
	LED_PATTERN_GREEN = 132,
	LED_PATTERN_YELLOW = 133,
	LED_PATTERN_RED = 134,
	LED_PATTERN_ALT_GB = 135,	// Alternate Green / Black
	LED_PATTERN_ALT_YB = 136,
	LED_PATTERN_ALT_RB = 137,
	LED_PATTERN_ALT_GY = 138,
	LED_PATTERN_ALT_YR = 139,
	LED_PATTERN_ALT_RG = 140,
	LED_PATTERN_ALT_RYG = 141,
	LED_PATTERN_ALT_GYB = 142,
	LED_PATTERN_ALT_YRG = 143,
	LED_PATTERN_ALT_RGB = 144,
	LED_PATTERN_ALT_RYGB = 145,
};


// Feedback Params structure

typedef struct tagFEEDBACK_PARAMS
{
	STRUCT_INFO	StructInfo;

	DWORD		dwBeepTime;				// Beep time
	DWORD		dwBeepFrequency;		// Beep frequency
	DWORD		dwLedTime;				// LED on-time

	WCHAR		szWaveFile[MAX_PATH];	// Name of .WAV file to play as feedback
										// Also used to name special feedback sequences

	DWORD		dwBeepPattern;			// Beep pattern (see tagBEEPPATTERN)
	DWORD		dwLedPattern;			// LED pattern (see tagLEDPATTERN)

} FEEDBACK_PARAMS;

typedef FEEDBACK_PARAMS FAR * LPFEEDBACK_PARAMS;


#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANDEF_H_
