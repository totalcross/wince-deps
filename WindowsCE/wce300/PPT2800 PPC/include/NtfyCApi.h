
//--------------------------------------------------------------------
// FILENAME:			NtfyCApi.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Main include file for Notify C API
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef NTFYCAPI_H_

#define NTFYCAPI_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------


#include <NtfyDef.h>
#include <NtfyErr.h>


#define NOTIFYAPI WINAPI


//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------


// Find the first notification object
DWORD NOTIFYAPI NOTIFY_FindFirst(LPNOTIFY_FINDINFO lpNotifyFindInfo,
								 LPHANDLE lphFindHandle);


// Find the next notification object in the ongoing find
DWORD NOTIFYAPI NOTIFY_FindNext(LPNOTIFY_FINDINFO lpNotifyFindInfo,
								HANDLE hFindHandle);


// Close the ongoing find
DWORD NOTIFYAPI NOTIFY_FindClose(HANDLE hFindHandle);


// Get the cycle information for a specified notification object
DWORD NOTIFYAPI NOTIFY_GetCycleInfo(DWORD dwIndex,
									LPCYCLE_INFO lpCycleInfo);


// Set the cycle information for a specified notification object
DWORD NOTIFYAPI NOTIFY_SetCycleInfo(DWORD dwIndex,
									LPCYCLE_INFO lpCycleInfo);


// Get the lock status for a specified notification object
DWORD NOTIFYAPI NOTIFY_GetLock(DWORD dwIndex,
							   LPBOOL lpbLock);


// Set the lock status for a specified notification object
DWORD NOTIFYAPI NOTIFY_SetLock(DWORD dwIndex,
							   BOOL bLock);


// Get the current state for a specified notification object
DWORD NOTIFYAPI NOTIFY_GetState(DWORD dwIndex,
								LPDWORD lpdwState);


// Set the current state for a specified notification object
DWORD NOTIFYAPI NOTIFY_SetState(DWORD dwIndex,
								DWORD dwState);

// Get the Version Information
DWORD NOTIFYAPI NOTIFY_GetVersion(LPNOTIFY_VERSION_INFO lpNotifyVersionInfo);

#ifdef __cplusplus
}
#endif

#endif	// #ifndef NTFYCAPI_H_
