// Copyright (C) 1996-1999  Symbol Technologies.  All Rights Reserved.
//
//  MODULE:   S24SP32.h
//
//  DESCRIPTION:
// 		Privileged Application interface to the Spectrum24 32-Bit 
//		Driver Extensions
//
//  PURPOSE:
//    	Supports the SPECIAL Functions in the Support DLL document release.
//

//--------------------------------------------------------------
// INTERFACE DECLARATION TO THE DLL.  
//
// NOTE: Include "windows.h" before you include this header file.
//
// NOTE: Include "S24SDK32.h" before you include this header file.
//--------------------------------------------------------------

BOOL __declspec (dllimport) WINAPI S24DriverNTInit ();

//--------------------------------------------------------------
// GENERAL DEFINES
//--------------------------------------------------------------


//--------------------------------------------------------------
// FUNCTION CODES
//--------------------------------------------------------------

#define		S24_DISABLE_WNMP_BROADCAST			109			// Special
#define		S24_ENABLE_WNMP_BROADCAST			110			// Special
#define		S24_GET_ENCRYPTION_KEY_ID			120			// Special - TRILOGY only
#define		S24_GET_ENCRYPTION_TYPE				119			// Special - TRILOGY only
#define		S24_GET_TRANSMITTER_STATE			90			// Special
#define		S24_GET_XMIT_DISABLE_REQ_STATUS		39			// Special
#define		S24_READ_FIRMWARE					106			// Special
#define		S24_RESEARCH_ADAPTERS				112			// Special			
#define		S24_SEND_WNMP_PACKET				108			// Special
#define		S24_SET_COUNTRY						121			// Internal ONLY - Top Secret Company Confidental - DO NOT PUBLISH!
#define		S24_SET_ENCRYPTION_KEY_ID			117			// Special - TRILOGY only
#define		S24_SET_ENCRYPTION_KEY_VALUE		118			// Special - TRILOGY only
#define		S24_SET_ENCRYPTION_TYPE				50			// Special - TRILOGY only
#define		S24_SET_INTERNAL_TIMEOUTS			82			// Special
#define		S24_SET_TRANSMITTER_STATE			73			// Special
#define		S24_UPDATE_FIRMWARE					60			// Special - GALAXY ONLY
#define		S24_UPDATE_TRILOGY_FIRMWARE			114			// Special - TRILOGY only

//--------------------------------------------------------------
// RETURN CODES
//--------------------------------------------------------------


//--------------------------------------------------------------
// SPECTRUM24 OID'S FOR EVENT NOTIFICATION
//--------------------------------------------------------------

// Define Unique Symbol OIDs for WNMP packets

#define	OID_WL_S24_WNMP_RETURN				(OID_WL_S24_OBJ_OPER | 0x000000F1)	// WNMP
#define	OID_WL_S24_WNMP_BROADCAST			(OID_WL_S24_OBJ_OPER | 0x000000F2)	// BROADCAST

//--------------------------------------------------------------
// GLOBAL CONSTANT DEFINES
//--------------------------------------------------------------

// Maximum Country text length.

#define	S24_COUNTRY_TEXT_LENGTH	40

// Hop Sequence Table Length

#define S24_HOP_SEQ_TABLE_LEN 100


//--------------------------------------------------------------
// TYPE DEFINITIONS AND STRUCTURES 
//--------------------------------------------------------------

#pragma pack(1)
 
// Internal Timers

typedef struct S24INTERNALTIMERS {

	int			iCompletionTime;		// Seconds to wait for caller to pick up event (-1 = forever)
	int			iPingReturnTime;		// Seconds to wait for ping return (-1 = forever)

} S24INTERNALTIMERS, *PS24INTERNALTIMERS;

// Transmitter State

typedef	USHORT	S24_DISABLE_TRANSMITTER;		// Transmitter state

#define	S24_DISABLE_TX_FALSE		0  			// Transmitter not inhibited
#define	S24_DISABLE_TX_TRUE			1  			// Transmitter inhibited

// Tramsmitter Request Status

typedef	USHORT	S24_TRANSMITTER_REQ_STATUS;		// Tx disable req. status

#define	S24_TRANSMITTER_REQ_IN_PROGRESS		0	// Request is still in progress
#define	S24_TRANSMITTER_REQ_COMPLETE		1	// Request is complete

#pragma	pack()

// OID_WL_S24_ENCRYPTION_TYPES parameter

typedef	ULONG	S24_ENCRYPTION_TYPE;			// IN OUT - Encryption Types
#define	S24_ENCRYPTION_TYPE_OPEN_SYSTEM		1	
#define	S24_ENCRYPTION_TYPE_SHARED_KEY		2

// OID_WL_S24_ENCRYPTION_KEY_IN_USE parameter
typedef	USHORT	S24_ENCRYPTION_KEY_ID;			// IN OUT - Encryption Key In use

// OID_WL_S24_ENCRYPTION_VALUE parameter

#define	S24_ENCRYPTION_KEY_LENGTH	5
typedef	struct	S24EncryptionKeyValue {			// OUT - Encryption Key Value
	USHORT		usKeyID;
	USHORT		usKeyLength;							// Set usKeyLength = S24_ENCRYPTION_KEY_LENGTH
	UCHAR		ucKeyValue[S24_ENCRYPTION_KEY_LENGTH];
} S24_ENCRYPTION_KEY_VALUE,	*PS24_ENCRYPTION_KEY_VALUE;

// OID_WL_SYM_COUNTRY_CONFIGURATION parameter.

typedef struct _S24CountryConfiguration {			// Country Configuration information
	UCHAR	ucCountry_Id;							// 00-00 (00-00) Country number
	UCHAR	ucCallSign[16];							// 01-16 (01-10) Japan callsign
	UCHAR	ucRSSI;									// 17-17 (11-11) Japan/Korea RSSI
	UCHAR	ucCountryID[2];							// 18-19 (12-13) Two letter Country ID
	UCHAR	ucCountryName[S24_COUNTRY_TEXT_LENGTH];	// 20-55 (14-37) Country Name
	UCHAR	ucHopMethod;							// 56-56 (38-38) Hop Method
	UCHAR	ucFirstChannel;							// 57-57 (39-39) First Channel
	UCHAR	ucNumberChannels;						// 58-58 (3A-3A) Total number of channels
	UCHAR	ucSequenceTable[S24_HOP_SEQ_TABLE_LEN];	// 59-158 (3B-9E) Hop Table
	UCHAR	ucChecksum[16];							// 159-174 (9F-AE) Checksum
} SYM_COUNTRY_CONFIGURATION, *PSYM_COUNTRY_CONFIGURATION;


