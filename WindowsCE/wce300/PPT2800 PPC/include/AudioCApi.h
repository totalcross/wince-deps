
//--------------------------------------------------------------------
// FILENAME:			AudioCApi.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Windows CE Audio Wrapper DLL main header module
//
// NOTES:				Public	
//
// 
//--------------------------------------------------------------------

#ifndef AUDIOCAPI_H_
#define AUDIOCAPI_H_

#ifdef __cplusplus
extern "C"
{
#endif

//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------
#include <AudioErr.h>
#include <strucinf.h>


#define AUDIO_API WINAPI

//Used by AUDIO_PlayBeep()
typedef struct tagAUDIO_INFO 
{
    TCHAR	szSound[MAX_PATH];  // wav file name
    DWORD   dwDuration;          // Beep duration (in milliseconds)
    DWORD	dwFrequency;         // Frequency in Hz
} AUDIO_INFO;

typedef AUDIO_INFO FAR * LPAUDIO_INFO;


// Version information structure
typedef struct tagAUDIO_VERSION_INFO
{
	STRUCT_INFO 	StructInfo;

	DWORD			dwNotifyAPIVersion;
	DWORD			dwCAPIVersion;	// HIWORD = major ver LOWORD = minor ver.

} AUDIO_VERSION_INFO;

typedef AUDIO_VERSION_INFO FAR * LPAUDIO_VERSION_INFO;


//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------

DWORD AUDIO_API AUDIO_SetEarSaveDelay(DWORD dwEarSaveDelay);

DWORD AUDIO_API AUDIO_GetEarSaveDelay(LPDWORD lpdwEarSaveDelay);

DWORD AUDIO_API AUDIO_SetBeeperVolume(DWORD dwBeeperVolume);

DWORD AUDIO_API AUDIO_GetBeeperVolume(LPDWORD lpdwBeeperVolume);

DWORD AUDIO_API AUDIO_GetBeeperVolumeLevels(LPDWORD lpdwBeeperVolumeLevels);

DWORD AUDIO_API AUDIO_PlayBeeper(LPAUDIO_INFO lpAudioInfo);

DWORD AUDIO_API AUDIO_GetVersion(LPAUDIO_VERSION_INFO lpAudioVersionInfo);

#ifdef __cplusplus
}
#endif

#endif	// #ifndef AUDIOCAPI_H_
