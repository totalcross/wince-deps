// Copyright (C) 1996-1999  Symbol Technologies.  All Rights Reserved.
//
//  MODULE:   S24SDK32.h
//
//  DESCRIPTION:
// 		Application interface to the Spectrum24 32-Bit Driver Extensions
//
//  PURPOSE:
//    	Supports the "S24SDK32.doc" SDK document release.
//    	Supports the "S24SDK32.exe" SDK program example release.
//

//--------------------------------------------------------------
// GENERAL DEFINES
//--------------------------------------------------------------

#define WORD  USHORT 
#define DWORD ULONG

#define byte  UCHAR
#define word  USHORT 
#define dword ULONG

//--------------------------------------------------------------
// INTERFACE DECLARATION TO THE DLL.  
//
// NOTE: Include "windows.h" before you include this header file.
//--------------------------------------------------------------

#ifdef __cplusplus
	extern "C" {
#endif

BOOL __declspec (dllimport) WINAPI S24DriverExtension (
								PBYTE		bMacAddress,			// MAC Address
								DWORD		dwFunction,				// Function
								HWND		hWindow,				// Client Window Handle
								DWORD		dwToken,				// Client Token
								DWORD		dwMaxWaitForComplete,	// Synchronous timeout
								LPVOID		lpInputBuffer,			// Pointer to Input Buffer
								DWORD		dwInputBufferLength,	// Length of Input Buffer
								LPVOID		lpOutputBuffer,			// Pointer to Output Buffer
								DWORD		dwOutputBufferLength,	// Length of Output Buffer
								LPDWORD		dwReturnedLength,		// Returned Length
								LPDWORD		dwReturnCode);			// Return Code

BOOL __declspec (dllimport) WINAPI S24DriverInit ();

#ifdef __cplusplus
}
#endif

// PARAMETER DEFINITIONS:
//
//     	MAC Address - The form of the MAC address is a 6 byte string, msb first
//
//     	Function - The function is one of the functions defined in s24mudef.h
//
//     	Client Window Handle - this is the handle of the window to which messages are to
//			be sent by this subsystem.  This parameter is not required except for functions
//			to enable/disable event notification and for functions addressed to adapters
//			other than those resident on this computer.
//
//     	Client Token - this is a value defined by the application and is used to identify
//			which function call is being responded to when return data is available from
//			functions which referenced adapters other than the resident ones.  This value
//			is never tested by the subsystem.
//
//     	Pointer to Input Buffer - the address of a buffer which contains parameters to be sent
//			to the adapter, not used on functions which only return data.
//
//     	Length of Input Buffer - in bytes.
//
//     	Pointer to Output Buffer - the address of a buffer into which data is to be placed by
//			the subsystem.  It must be present if data is to be returned to the user.
//
//     	Length of Output Buffer - in bytes.
//
//     	Pointer to Returned Length - the address of a DWORD into which the length of any returned
//			data will be placed.
//
//     	Pointer to Return Code - the address of a DWORD into which a return code is placed. A
//			return code of ERROR_NO_ERROR (0) indicates success.
//
//   	RETURN VALUE:
//    		Returns TRUE if function is completely processed.
//    		Returns FALSE if function cannot be sent to driver or has not been completed.
//    		If the request is to be processed but completion will be delayed,
//    		the function returns FALSE and ReturnCode is set to S24_RC_PENDING.


//--------------------------------------------------------------
// FUNCTION CODES
//--------------------------------------------------------------
#define	S24_NULL_FUNCTION					0
#define	S24_ADD_ACCESS_CONTROL_ITEMS		1
#define	S24_CLEAR_ACCESS_CONTROL_LIST		62
#define S24_CLEAR_ADAPTER_DEBUG_INFO		103			// NOT Used
#define	S24_CLEAR_ADAPTER_STATISTICS		79
#define	S24_CLEAR_DEBUG_INFO				89			// Internal
#define	S24_COMMIT_PARAMETER_CHANGES		83
#define	S24_DELETE_ACCESS_CONTROL_ITEMS		2
#define	S24_DISABLE_EVENT_NOTIFICATION		3
#define	S24_ENABLE_EVENT_NOTIFICATION		4
#define	S24_EXECUTE_SELF_TEST				5
#define	S24_GET_ACCESS_CONTROL_LIST			6
#define	S24_GET_ADAPTER_BSS_ID				7
#define	S24_GET_ADAPTER_ESS_ID				8
#define	S24_GET_ADAPTER_FIRMWARE_VERSION	74
#define	S24_GET_ADAPTER_INTERRUPT_STATE		9
#define	S24_GET_ADAPTER_LIST				75	
#define	S24_GET_ADAPTER_MAC_ADDRESS			10
#define	S24_GET_ADAPTER_NET_ID				12
#define	S24_GET_ADAPTER_OPTION_FLAGS		13
#define	S24_GET_ADAPTER_POWER_DOWN_STATE	14
#define	S24_GET_ADAPTER_POWER_MODE			64
#define	S24_GET_ADAPTER_STATISTICS			15
#define	S24_GET_ADAPTER_TIME				16
#define	S24_GET_ANTENNA_DIVERSITY			84
#define	S24_GET_AP_TABLE					65
#define	S24_GET_ASSOCIATED_AP_BSS_ID		85
#define	S24_GET_ASSOCIATED_AP_ID			18
#define	S24_GET_ASSOCIATED_AP_MAC_ADDR		19
#define	S24_GET_ASSOCIATION_STATUS			20
#define	S24_GET_BEACON_PARAMETERS			21
#define	S24_GET_CARD_TYPE					99
#define	S24_GET_CHANNEL_ID					66
#define	S24_GET_CHANNEL_LIST				122
#define	S24_GET_CHANNEL_QUALITY				67
#define	S24_GET_CONFIGURATION_MEMORY		22
#define	S24_GET_DEVICE_MANUFACTURING_INFO	25
#define	S24_GET_FUNCTIONAL_MODE				27
#define	S24_GET_HARDWARE_TYPE				111			// TRILOGY
#define	S24_GET_HARDWARE_VERSION			123
#define	S24_GET_INDICATION_ENABLE_STATE		28
#define	S24_GET_LEAST_PREFERRED_AP_ID		29
#define	S24_GET_LEAST_PREFERRED_AP_MAC_ADDR	30
#define	S24_GET_MANDATORY_BSS_ID			31
#define S24_GET_MANDATORY_AP_ID				80
#define	S24_GET_MAP_TABLE					68
#define	S24_GET_MEDIA_CONFIGURATION			32
#define	S24_GET_MICROAP_CONFIGURATION		69
#define	S24_GET_MKK_CALL_SIGN				33
#define	S24_GET_PREFERRED_BSS_ID			34
#define	S24_GET_PREFERRED_AP_ID				35
#define S24_GET_PROTOCOL_TYPE				92
#define	S24_GET_RADIO_LINK_SPEED			76
#define	S24_GET_RESTART_COUNT				36
#define	S24_GET_ROAMING_CONFIGURATION		70
#define S24_GET_SELF_TEST_RESULTS			91
#define	S24_GET_SERVICE_VERSION				86
#define	S24_GET_SUPPORTED_DATA_RATES		101
#define	S24_RESET_ADAPTER_STATISTICS		40
#define S24_RESET_DEVICE					95
#define	S24_RESTART_ADAPTER					63
#define	S24_RETRIEVE_RETURNED_DATA			61
#define	S24_RETRIEVE_RETURNED_OID			81
#define	S24_SEND_WNMP_PING					41
#define	S24_SET_ADAPTER_ESS_ID				43
#define	S24_SET_ADAPTER_INTERRUPT_STATE		42
#define	S24_SET_ADAPTER_NET_ID				45
#define	S24_SET_ADAPTER_OPTION_FLAGS		46
#define	S24_SET_ADAPTER_POWER_DOWN_STATE	47
#define	S24_SET_ADAPTER_POWER_MODE			72
#define	S24_SET_ANTENNA_DIVERSITY			87
#define	S24_SET_BEACON_PARAMETERS			48
#define	S24_SET_CARD_TYPE					100
#define	S24_SET_CHANNEL_ID					127
#define	S24_SET_FUNCTIONAL_MODE				51
#define	S24_SET_LEAST_PREFERRED_AP_ID		52
#define	S24_SET_LEAST_PREFERRED_MAC_ADDRESS	53
#define	S24_SET_MANDATORY_BSS_ID			55
#define	S24_SET_MICROAP_CONFIGURATION		54
#define	S24_SET_PREFERRED_AP_ID				57
#define	S24_SET_PREFERRED_BSS_ID			56
#define	S24_SET_ROAMING_CONFIGURATION		58
#define	S24_SET_SUPPORTED_DATA_RATES		102


#define S24_RESEARCH_ADAPTERS			112
#define	S24_GET_ENCRYPTION_TYPE				  119

//--------------------------------------------------------------
// RETURN CODES
//--------------------------------------------------------------

#define RC_SUCCESS								0
#define	RC_NO_ERROR								0
#define RC_NOT_SUPPORTED						ERROR_NOT_SUPPORTED
#define RC_PENDING								ERROR_IO_PENDING
#define RC_INVALID_FUNCTION						ERROR_INVALID_FUNCTION
#define	RC_INVALID_PARAMETER					ERROR_INVALID_PARAMETER

#define	FIRST_USER_ERROR						5000
#define	RC_INVALID_MAC_ADDRESS					FIRST_USER_ERROR + 0
#define	RC_PROCESS_NOT_REGISTERED				FIRST_USER_ERROR + 1
#define	RC_INVALID_INPUT_BUFFER_ADDRESS			FIRST_USER_ERROR + 2
#define	RC_INVALID_INPUT_BUFFER_LENGTH			FIRST_USER_ERROR + 3
#define	RC_INVALID_OUTPUT_BUFFER_ADDRESS		FIRST_USER_ERROR + 4
#define	RC_INVALID_OUTPUT_BUFFER_LENGTH			FIRST_USER_ERROR + 5
#define	RC_INVALID_RETURNED_LENGTH_POINTER		FIRST_USER_ERROR + 6
#define	RC_NO_DATA_AVAILABLE					FIRST_USER_ERROR + 7
#define RC_PACKET_NOT_ALLOCATED					FIRST_USER_ERROR + 8
#define RC_PACKET_NOT_SENT						FIRST_USER_ERROR + 9
#define RC_SERVICE_NOT_RUNNING					FIRST_USER_ERROR + 10
#define	RC_CANT_ALLOCATE_MEMORY					FIRST_USER_ERROR + 11
#define	RC_CANT_SEND_TO_SERVICE					FIRST_USER_ERROR + 12
#define	RC_CANT_RECEIVE_FROM_SERVICE			FIRST_USER_ERROR + 13
#define	RC_OUT_OF_RESOURCES						FIRST_USER_ERROR + 14
#define	RC_FUNCTION_TIMED_OUT					FIRST_USER_ERROR + 15
#define RC_SERVICE_UNRESPONSIVE					FIRST_USER_ERROR + 16
#define RC_INVALID_FIRMWARE_FILE				FIRST_USER_ERROR + 17
#define	RC_DEVICE_UNAVAILABLE					FIRST_USER_ERROR + 18
#define	RC_INVALID_HARDWARE_TYPE				FIRST_USER_ERROR + 19


// The following errors are part of the initialization of the system.  The errors are divided into
// several groups.  The first group are errors that will not allow the Event Monitor to continue;
// the event monitor will display a message box and quit if any of the following group of errors
// happen during initialization.  The calling application will never see these errors.

#define ERROR_INIT_APP_CREATE_WINDOW_FAILED							FIRST_USER_ERROR + 1000 + 0
#define ERROR_INIT_APP_CREATE_EVENT_FAILED							FIRST_USER_ERROR + 1000 + 1
#define ERROR_INIT_APP_REGISTER_WINDOW_MESSAGE_FAILED				FIRST_USER_ERROR + 1000 + 2
#define ERROR_INIT_APP_POST_THREAD_MESSAGE_FAILED					FIRST_USER_ERROR + 1000 + 3
#define ERROR_INIT_APP_SERVICE_INIT_FAILED							FIRST_USER_ERROR + 1000 + 4
#define ERROR_INIT_APP_START_THREAD_FAILED							FIRST_USER_ERROR + 1000 + 5
#define ERROR_INIT_APP_UNRECOGNIZED_OPERATING_SYSTEM				FIRST_USER_ERROR + 1000 + 6
#define ERROR_INIT_APP_GLOBAL_ALLOC_PTR_FAILED						FIRST_USER_ERROR + 1000 + 7

#define ERROR_INIT_SHARED_MEMORY_CREATE_MUTEX_FAILED				FIRST_USER_ERROR + 1100 + 0
#define ERROR_INIT_SHARED_MEMORY_CREATE_FILE_MAPPING_FAILED			FIRST_USER_ERROR + 1100 + 1
#define ERROR_INIT_SHARED_MEMORY_MAP_VIEW_OF_FILE_FAILED			FIRST_USER_ERROR + 1100 + 2
#define ERROR_INIT_SHARED_MEMORY_CREATE_EVENT_FAILED				FIRST_USER_ERROR + 1100 + 3

// The next set of errors are application errors that prevent the Event Monitor from doing it's
// intended function, but doesn't prevent it from communicating with applications.  The calling
// application will see these errors if they happen; having these errors happen prevent the Event
// Monitor from working with any adapters.

#define ERROR_INIT_NDIS_CREATE_EVENT_FAILED							FIRST_USER_ERROR + 1200 + 0
#define ERROR_INIT_NDIS_UNABLE_TO_OPEN_LINK_TO_VXD					FIRST_USER_ERROR + 1200 + 1
#define ERROR_INIT_NDIS_GETTING_ADAPTER_LIST_FAILED					FIRST_USER_ERROR + 1200 + 2
#define ERROR_INIT_NDIS_UNABLE_TO_OPEN_SERVICE_CONTROL_MANAGER		FIRST_USER_ERROR + 1200 + 3
#define ERROR_INIT_NDIS_UNABLE_TO_OPEN_PACKET_DRIVER				FIRST_USER_ERROR + 1200 + 4
#define ERROR_INIT_NDIS_UNABLE_TO_START_PACKET_DRIVER				FIRST_USER_ERROR + 1200 + 5
#define ERROR_INIT_NDIS_UNRECOGNIZED_OPERATING_SYSTEM				FIRST_USER_ERROR + 1200 + 6

// The next set of errors are errors that occurred while attempting to open an adapter.  These
// errors are specific to a given adapter.  When the application attempts to use a specific adapter,
// it may get back one of these errors.  Getting one of these errors on a specific adapter does not
// preclude another adapter from working correctly.

#define ERROR_INIT_ADAPTER_ISSUE_A_RECEIVE_FAILED					FIRST_USER_ERROR + 1300 + 0
#define ERROR_INIT_ADAPTER_UNABLE_TO_CREATE_SYMBOLIC_LINK			FIRST_USER_ERROR + 1300 + 1
#define ERROR_INIT_ADAPTER_UNABLE_TO_OPEN_ADAPTER					FIRST_USER_ERROR + 1300 + 2
#define ERROR_INIT_ADAPTER_UNABLE_TO_GET_MEDIA_TYPE					FIRST_USER_ERROR + 1300 + 3
#define ERROR_INIT_ADAPTER_UNRECOGNIZED_OPERATING_SYSTEM			FIRST_USER_ERROR + 1300 + 4

// The following errors are specific to the Mobile IP subsystem of the Event Monitor.  They are
// specific to a given adapter.  getting one of these errors on a specific adapter does not
// necessarily preclude another adapter from running Mobile IP correctly.  Currently, there is no
// way for an application to get these errors.

#define ERROR_INIT_MOBILE_IP_NDIS_FAILED							FIRST_USER_ERROR + 1400 + 0
#define ERROR_INIT_MOBILE_IP_PROTOCOL_NOT_SUPPORTED					FIRST_USER_ERROR + 1400 + 1
#define ERROR_INIT_MOBILE_IP_SET_TIMER_FAILED						FIRST_USER_ERROR + 1400 + 2
#define ERROR_INIT_MOBILE_IP_TCPIP_NOT_ENABLED						FIRST_USER_ERROR + 1400 + 3
#define ERROR_INIT_MOBILE_IP_DHCP_ENABLED							FIRST_USER_ERROR + 1400 + 4
#define ERROR_INIT_MOBILE_IP_CANNOT_GET_IP_ADDRESS					FIRST_USER_ERROR + 1400 + 5
#define ERROR_INIT_MOBILE_IP_CANNOT_GET_SUBNET_MASK					FIRST_USER_ERROR + 1400 + 6
#define ERROR_INIT_MOBILE_IP_CANNOT_OPEN_ADAPTER_KEY				FIRST_USER_ERROR + 1400 + 7
#define ERROR_INIT_MOBILE_IP_CANNOT_OPEN_NETTRANS_KEY				FIRST_USER_ERROR + 1400 + 8
#define ERROR_INIT_MOBILE_IP_CANNOT_FIND_MOBILE_IP_KEY				FIRST_USER_ERROR + 1400 + 9
#define ERROR_INIT_MOBILE_IP_KEY_DISABLE							FIRST_USER_ERROR + 1400 + 10
#define ERROR_INIT_MOBILE_IP_NOT_A_SYMBOL_ADAPTER					FIRST_USER_ERROR + 1400 + 11
#define ERROR_INIT_MOBILE_IP_UNRECOGNIZED_OPERATING_SYSTEM			FIRST_USER_ERROR + 1400 + 12

//--------------------------------------------------------------
// SPECTRUM24 OID'S FOR EVENT NOTIFICATION
//--------------------------------------------------------------

// Spectrum24 OID Type.

#define	OID_WL_S24_OBJ_OPER					0xff010100
#define	OID_WL_S24_OBJ_STAT					0xff020100

// Define a unique OID for ping returns

#define	OID_WL_S24_PING_RETURN				(OID_WL_S24_OBJ_OPER | 0x000000F0)	// PING

// Supported OID's

#define	OID_WL_S24_OPERATION_MODE			(OID_WL_S24_OBJ_OPER | 0x00000007)	// Operational power mode
#define OID_WL_S24_NETWORK_ID				(OID_WL_S24_OBJ_OPER | 0x0000000a)	// Associated network id.
#define	OID_WL_S24_HARDWARE_VERSION			(OID_WL_S24_OBJ_OPER | 0x0000000b)	// List of available channels
#define	OID_WL_S24_CHANNEL_LIST				(OID_WL_S24_OBJ_OPER | 0x0000000c)	// List of available channels
#define	OID_WL_S24_CHANNEL_ID				(OID_WL_S24_OBJ_OPER | 0x0000000f)	// Current channel (freq.)
#define	OID_WL_S24_REGISTRATION_STATUS		(OID_WL_S24_OBJ_OPER | 0x00000014)	// Association status
#define	OID_WL_S24_FUNCTIONAL_MODE			(OID_WL_S24_OBJ_OPER | 0x00000034)	// MU/MAP mode
#define	OID_WL_S24_BEACON_INTERVAL			(OID_WL_S24_OBJ_OPER | 0x00000035)	// Beacon algortihm, min, max
#define	OID_WL_S24_ROAMING_CONFIGURATION	(OID_WL_S24_OBJ_OPER | 0x00000036)	// Roaming configuration (SPRING)
#define	OID_WL_S24_LEAST_PREF_AP_ID			(OID_WL_S24_OBJ_OPER | 0x00000037)	// Least preferred AP Id. (SPRING)
#define	OID_WL_S24_LEAST_PREF_MAC_ADDRESS	(OID_WL_S24_OBJ_OPER | 0x00000038)	// Least preferred AP MAC address (SPRING)
#define	OID_WL_S24_MAP_CONFIGURATION		(OID_WL_S24_OBJ_OPER | 0x0000003a)	// MicroAP configuration
#define	OID_WL_S24_ANTENNA_DIVERSITY		(OID_WL_S24_OBJ_OPER | 0x00000041)	// Antenna diversity
#define	OID_WL_S24_ADAPTER_PROTOCOL			(OID_WL_S24_OBJ_OPER | 0x00000042)	// Adapter Protocol (SPRING, IEEE802.11, etc.)
#define	OID_WL_S24_ESS_ID					(OID_WL_S24_OBJ_OPER | 0x00000043)	// Associated AP ESS ID (IEEE802.11)
#define	OID_WL_S24_AP_BSS_ID				(OID_WL_S24_OBJ_OPER | 0x00000044)	// Associated AP BSS ID (IEEE802.11)
#define	OID_WL_S24_MANDATORY_AP_BSS_ID		(OID_WL_S24_OBJ_OPER | 0x00000045)	// Mandatory AP BSS ID (IEEE802.11)
#define	OID_WL_S24_PREFERRED_AP_BSS_ID		(OID_WL_S24_OBJ_OPER | 0x00000046)	// Preferred AP BSS ID (IEEE802.11)
#define	OID_WL_S24_ADAPTER_CARD_TYPE		(OID_WL_S24_OBJ_OPER | 0x00000047)	// Type of adapter card (leds only)
#define	OID_WL_S24_ADAPTER_DATA_RATES		(OID_WL_S24_OBJ_OPER | 0x00000048)	// Data rates supported by firmware
#define	OID_WL_S24_ACCESS_CONTROL_LIST		(OID_WL_S24_OBJ_OPER | 0x00000050)	// MAP access control list
#define	OID_WL_S24_ADAPTER_DISABLE			(OID_WL_S24_OBJ_OPER | 0x00000051)	// Adapter disable state
#define	OID_WL_S24_POWER_DOWN_STATE			(OID_WL_S24_OBJ_OPER | 0x00000052)	// Adapter power down state
#define	OID_WL_S24_RESTART					(OID_WL_S24_OBJ_OPER | 0x00000053)	// Adapter restart state
#define	OID_WL_S24_AP_MAC_ADDRESS			(OID_WL_S24_OBJ_OPER | 0x00000070)	// Associated AP MAC address
#define	OID_WL_S24_ADAPTER_FLAGS			(OID_WL_S24_OBJ_OPER | 0x00000073)	// Adapter option flags	
#define	OID_WL_S24_AP_TABLE					(OID_WL_S24_OBJ_STAT | 0x00000093)	// AP Table
#define	OID_WL_S24_MAP_TABLE				(OID_WL_S24_OBJ_STAT | 0x00000096)	// MAP association table

//--------------------------------------------------------------
// GLOBAL CONSTANT DEFINES
//--------------------------------------------------------------

// MAC/IEEE address length.

#define	S24_MAC_ADDR_LENGTH			6

// BSS ID's are always 6 bytes.

#define	S24_BSS_ID_LENGTH			6

// Access Point name length.

#define	S24_AP_NAME_LENGTH			13

// Maximum ESS ID length.

#define	S24_ESS_ID_MAX_LENGTH		32

// Maximum Country text length.

#define	S24_COUNTRY_TEXT_MAX_LENGTH	36

// Min. and max. AP ID's.

#define	S24_MIN_AP_ID				0x01
#define	S24_MAX_AP_ID				0x7F

// Min. and max. Hop Sequence values.

#define	S24_MIN_HOP_SEQUENCE		1
#define	S24_MAX_HOP_SEQUENCE		22

// Min. and max. Net ID values.

#define	S24_MIN_NET_ID				0x100
#define	S24_MAX_NET_ID				0x1FE

// Min. and max. Channel ID or Frequency values.

#define	S24_MIN_CHANNEL				1
#define	S24_MAX_CHANNEL				79

// Min. and max. RSSI values (7 bits).

#define	S24_MIN_RSSI				0
#define	S24_MAX_RSSI				127									

// Other 
#define	MAX_DATA_LENGTH				1600
#define	MAX_BLOCK_SIZE				1800


#pragma pack(1)

//--------------------------------------------------------------
// EVENTS
//--------------------------------------------------------------

typedef struct S24EventInfo {

	ULONG		oOid;							// The OID of the generating object
	BYTE		bMacAddr[S24_MAC_ADDR_LENGTH];	// The MAC address of the generating adapter
	int			iLength;						// The length in bytes of the associated data
	BYTE		bData[0];						// the beginning of the data section

} S24EVENTINFO, *PS24EVENTINFO;


//--------------------------------------------------------------
// TYPE DEFINITIONS AND STRUCTURES 
//--------------------------------------------------------------

// ACCESS CONTROL LIST

typedef UCHAR	S24_ACCESS_CONTROL_LIST[S24_MAC_ADDR_LENGTH];	// MAP access control list
#define	S24_MAX_ACL_ENTRIES			20					// Maximum # of ACL entries

// SELF TEST

typedef	struct	S24SelfTest {						// Adapter self test
	USHORT	usStatus;									// Self test status
#define	S24_TEST_STATUS_FAILED					0x0001	// Test failed (see result bitmap)
#define	S24_TEST_STATUS_IN_PROGRESS				0x0010	// Test is in progress
#define	S24_TEST_STATUS_COMPLETE				0x0080	// Test is complete
	USHORT	usCurrentResults;							// Current results
	USHORT	usCumulativeResults;						// Cumulative results
#define	S24_TEST_RESULT_ROM_XSUM				0x0001	// ROM checksum failure
#define	S24_TEST_RESULT_RX_FIFO					0x0002	// Rx FIFO failure
#define	S24_TEST_RESULT_TX_FIFO					0x0004	// Tx FIFO failure
#define	S24_TEST_RESULT_RX_DMA					0x0008	// Rx DMA failure
#define	S24_TEST_RESULT_TX_DMA					0x0010	// Tx DMA failure
#define	S24_TEST_RESULT_RADIO_ASIC_REG			0x0020	// Radio ASIC register failure
#define	S24_TEST_RESULT_RADIO_ASIC_TEST_LOOP	0x0040	// Radio ASIC test loop failure
#define	S24_TEST_RESULT_RTC						0x0080	// Real time clock failure
#define	S24_TEST_RESULT_TIMER2_AND_INT			0x0100	// Timer2 and Interrupt failure
#define	S24_TEST_RESULT_HOST_ASIC_INT			0x0200	// Host ASIC interrupt failure
#define	S24_TEST_RESULT_RADIO_ASIC_INT			0x0400	// Radio ASIC interrupt failure
#define	S24_TEST_RESULT_RAM			  			0x0800	// RAM test failure

} S24_SELF_TEST, *PS24_SELF_TEST;

// BSS ID 

typedef	UCHAR	S24_BSS_ID[S24_BSS_ID_LENGTH];			// BSS Identifier

// ESS ID 
typedef USHORT	S24_ESS_ID_LENGTH;						// Variable length value
typedef	UCHAR	S24_ESS_ID[];							// ESS Identifier

// ADAPTER FIRMWARE VERSION 
typedef USHORT	S24_FW_VERSION_LENGTH;					// Variable length value
typedef	UCHAR	S24_FW_VERSION[];						// Firmware version
#define	S24_FW_VERSION_MAX_LENGTH			12			// Maximum length
#define	S24_MAX_FW_INFO					12				// Max. FW (version + date) length
#define	S24_MAX_FW_INFO_GALAXY				12					//     Galaxy
#define	S24_MAX_FW_INFO_TRILOGY				32					//     Trilogy

// ADAPTER INTERRUPT STATE

typedef	USHORT	S24_ADAPTER_INTERRUPT_STATE;			// Adapter interrupt (disable) state
#define	S24_ADAPTER_INTERRUPTS_ENABLED		0			// Adapter enabled (not disabled)
#define	S24_ADAPTER_INTERRUPTS_DISABLED		1			// Adapter disabled

// ADAPTER (NETWORK) LIST

#define S24_ADAPTER_NAME_SIZE				64
#define S24_ADAPTER_DESCRIPTION_SIZE		64

typedef 	struct 	S24AdapterListEntry {

	BYTE	btMACAddress[S24_MAC_ADDR_LENGTH];
	UCHAR	ucAdapterName[S24_ADAPTER_NAME_SIZE];
	UCHAR	ucAdapterDescription[S24_ADAPTER_DESCRIPTION_SIZE];
	BOOL	bSymbolAdapter;

} S24_ADAPTER_LIST_ENTRY, *PS24_ADAPTER_LIST_ENTRY;

// ADAPTER MAC ADDRESS

typedef	UCHAR	S24_ADAPTER_MAC_ADDRESS[S24_MAC_ADDR_LENGTH];	// Adapter MAC address

// NETWORK ID 

typedef	USHORT	S24_NETWORK_ID;							// Network id.

// ADAPTER OPTION FLAGS

typedef	USHORT	S24_ADAPTER_FLAGS;						// Adapter option flags
#define S24_AFLAGS_ENABLE_WMNP			0x0001			// Enable WMNP packet processing
#define S24_AFLAGS_SELF_BCAST_FILTER	0x0002			// Override self-broadcast filter
#define S24_AFLAGS_NO_ACK				0x0004			// Send broad/multicast w/o ack
#define	S24_AFLAGS_MAP_FILTER			0x0008			// Enable MAP destiation filter
#define	S24_AFLAGS_PWDN_BCAST			0x0020			// Enable reception of broad/multicast when host is powered down.
#define	S24_AFLAGS_ENABLE_INST_WNMP		0x0080			// Enable Tx of WNMP instrumentation msgs.

// ADAPTER POWER DOWN STATE

typedef	USHORT	S24_POWER_DOWN_STATE;					// Adpater power down state
#define	S24_POWER_DOWN_STATE_FALSE		0				// Adapter not powered down
#define	S24_POWER_DOWN_STATE_TRUE		1				// Adapter powered down
#define	S24_ADAPTER_RESUME				0
#define	S24_ADAPTER_SLEEP				1

// ADAPTER POWER MODE

typedef	USHORT	S24_OP_MODE;							// Operational power mode
#define	S24_OP_MODE_CAM				0  					// Continuous Active Mode (full power)
#define S24_OP_MODE_PSP				1  					// Power Save Polling (low power)

// ADAPTER TIME

typedef	ULONG	S24_ADAPTER_TIME;						// Adapter time

// ANTENNA DIVERSITY

typedef	USHORT	S24_ANTENNA_DIVERSITY;					// IN OUT - Antenna diversity
#define	S24_DIVERSITY_DISABLED		0					// Galaxy - Enabled (dual antenna's)
#define	S24_DIVERSITY_ENABLED		1					// Galaxy - Disabled (single antenna)
#define	SYM_ANTENNA_A				1					// Trilogy - Antenna A
#define	SYM_ANTENNA_B				2					// Trilogy - Antenna B
#define	SYM_ANTENNA_AUTO			0					// Trilogy - Firmware (AUTO) selected antenna

// ACCESS POINT TABLE

#define	S24_MAX_AP_TABLE		20						// Max. number of AP Table entries

typedef struct _S24NetAddr {
	unsigned char addr[S24_MAC_ADDR_LENGTH];			// IEEE address
} S24_NET_ADDR;

typedef struct  S24AP_Name {
	unsigned char name[S24_AP_NAME_LENGTH];				// AP name string
} S24_AP_NAME;

// NOTE: The following AP structure applies only to SPRING Firmware 
//       Versions 3.x and earlier.

typedef	struct	S24APTable3 { 						// AP Table	3

	USHORT			AP_Link;							// links entries in used or unused lists
	UCHAR			AP_Status;							// entry status
#define	AP_STATUS_ASSOC_AP	   		0x80				// set if MU associated with the AP
#define	AP_STATUS_IN_USE	   		0x40				// set if the AP_Table entry is in use
#define	AP_STATUS_RATE         		0x20				// if entry ineligible due to rate mismatch
#define	AP_STATUS_QUALITY	   		0x10				// if prev. assoc. produced poor quality
#define AP_STATUS_AGEOUT_COUNT 		0x0F				// age-out counter for the AP entry
	UCHAR			AP_AP_ID;			 				// the AP's AP_ID
	UCHAR			AP_Hop_Sequence;	 				// the hop sequence number
	UCHAR			AP_Hoptick;							// hoptick value from Probe Response
	UCHAR			AP_Frequency;		 				// frequency from Probe Response
	USHORT			AP_Timestamp_LSW;					// time when Probe Response Rx-ed (LSW)
	USHORT			AP_Timestamp_MSW;					// time when Probe Response Rx-ed (MSW)
	S24_NET_ADDR 	AP_Address;	   						// IEEE/MAC address of AP adapter
	UCHAR			AP_Beacon_Interval;					// Beacon interval (1 MS LSB)
	USHORT			AP_Time_To_Next_Beacon;				// time offset to next Beacon (1 us LSB)
	S24_AP_NAME		AP_AP_Name;							// AP name (ASCII string)
	UCHAR			AP_MU_RSSI;							// RSSI of Probe received by AP
	USHORT			AP_AP_RSSI;							// RSSI of Probe Response Rx-ed by MU
	USHORT			AP_NETID;							// NETID of the responding AP
	UCHAR			AP_MU_Count;		 				// Count of associated MUs
	UCHAR			AP_Traffic;							// Effective traffic load
	USHORT			AP_Qual_RSSI;						// RSSI at time of poor Tx/Rx quality
	UCHAR			AP_RSSI_Array_Index; 				// index into RSSI_Array
	UCHAR			AP_RSSI_Array[5];	 				// last N RSSI values
	USHORT			AP_RSSI_Total;						// current sum of RSSI_Array

} S24_AP_TABLE3, *PS24_AP_TABLE3;

// NOTE: The following AP structure applies only to IEEE802.11 Firmware 
//       Versions 4.x and later.

typedef	struct	S24APTable4 { 					// AP Table 4

	USHORT			AP_Link;							// links entries in used or unused lists
	UCHAR			AP_Status;							// entry status
#define	AP_STATUS_ASSOC_AP4	   		0x80				// set if MU associated with the AP
#define	AP_STATUS_IN_USE4	   		0x40				// set if the AP_Table entry is in use
#define	AP_STATUS_RATE4        		0x20				// if entry ineligible due to rate mismatch
#define	AP_STATUS_QUALITY4	   		0x10				// if prev. assoc. produced poor quality
#define AP_STATUS_INELIGIBLE4  		0x08 				// if AP is ineligible for association
#define AP_STATUS_1MB_RATE4    		0x04				// if 1MB data rate in AP's basic rate set
	UCHAR			AP_Ageout_Count;					// ageout down counter
	UCHAR			AP_BSS_ID[S24_BSS_ID_LENGTH]; 		// the AP's BSS_ID
	USHORT			AP_Dwell_Time;						// hop dwell time
	UCHAR			AP_Hop_Set;			 				// hop set number
	UCHAR			AP_Hop_Pattern;						// hop pattern number
	UCHAR			AP_Hop_Index;						// current hop index
	UCHAR			AP_MU_Count;		 				// Count of associated MUs
	UCHAR			AP_Traffic;							// Effective traffic load
	USHORT			AP_Beacon_Interval;					// Beacon interval (1 MS LSB)
	USHORT			AP_Hop_Start_Time_LSW;				// hop start time for the AP
	USHORT			AP_Hop_Start_Time_MSW;				// 
	USHORT			AP_AP_RSSI;							// RSSI of Probe Response Rx-ed by MU
	USHORT			AP_RSSI_Total;						// current sum of RSSI_Array
	UCHAR			AP_RSSI_Array_Index; 				// index into RSSI_Array
	UCHAR			AP_Temp_RSSI;						// value using during partial scan series
	UCHAR			AP_RSSI_Array[5];	 				// last N RSSI values
	USHORT			AP_Qual_RSSI;						// RSSI at time of poor Tx/Rx quality
	UCHAR			AP_Qual_MU_Cnt;						// number of MUs at time of poor qual

} S24_AP_TABLE4, *PS24_AP_TABLE4;

// ASSOCIATED BSS ID 

typedef	UCHAR	S24_ASSOC_BSS_ID[S24_BSS_ID_LENGTH];	// Associated AP BSS Identifier

// ASSOCIATED AP ID 

typedef	USHORT	S24_ASSOC_AP_ID;						// Associated network id.

// ASSOCIATED MAC ADDRESS

typedef	UCHAR	S24_ASSOC_AP_MAC_ADDRESS[S24_MAC_ADDR_LENGTH];	// Associated AP MAC address

// ASSOCIATION (REGISTRATION) STATUS

typedef	USHORT	S24_REGISTRATION_STATUS;				// Association status
#define	S24_REG_STATUS_ASSOCIATED		0x0080			// Associated with an AP
#define	S24_REG_STATUS_NOT_ASSOCIATED	0x0040			// Not associated with an AP
#define	S24_REG_STATUS_ROAMED			0x0001			// Roamed to a new AP

// BEACON PARAMETERS

typedef	struct	S24BeaconInterval {				// Beacon algorithm, min, max
	USHORT	usBeaconAlgorithm;							// Beacon interval algorithm
#define	S24_BEACON_ALGORITHM_1			1				// Beacon interval  1  (100 ms)
#define	S24_BEACON_ALGORITHM_2			2				// Beacon interval  2  (200 ms)
#define	S24_BEACON_ALGORITHM_3			3				// Beacon interval  3  (300 ms)
#define	S24_BEACON_ALGORITHM_4			4				// Beacon interval  4  (400 ms)
#define	S24_BEACON_ALGORITHM_5			5				// Beacon interval  5  (500 ms)
#define	S24_BEACON_ALGORITHM_6			6				// Beacon interval  6  (600 ms)
#define	S24_BEACON_ALGORITHM_7			7				// Beacon interval  7  (700 ms)
#define	S24_BEACON_ALGORITHM_8			8				// Beacon interval  8  (800 ms)
#define	S24_BEACON_ALGORITHM_9			9				// Beacon interval  9  (900 ms)
#define	S24_BEACON_ALGORITHM_10			10				// Beacon interval  10 (1000 ms)
#define	S24_BEACON_ALGORITHM_11			11				// Beacon algorithm 11 (dynamic - MIN to MAX)
#define	S24_BEACON_ALGORITHM_12			12				// Beacon algorithm 12 (dynamic - CAM to MAX)
	USHORT	usBeaconMinimum;							// Min. beacon interval for adaptive algorithm
	USHORT	usBeaconMaximum;							// Max. beacon interval for adaptive algorithm
#define	S24_BEACON_MINIMUM				1				// Minimum beacon interval
#define	S24_BEACON_MAXIMUM				10				// Maximum beacon interfal
} S24_BEACON_INTERVAL, *PS24_BEACON_INTERVAL;

// CARD TYPE

typedef ULONG	S24_CARD_TYPE;
	#define CARD_TYPE_PCMCIA			0
	#define CARD_TYPE_PCI				3

// CHANNEL ID

typedef	USHORT	S24_CHANNEL_ID;							// Current channel (freq).

// CHANNEL QUALITY

typedef	struct  S24ChannelQuality {				// Current channel quality
	USHORT	usPercentMissedBeacons;						// percent missed beacons in last 5 sec
	USHORT	usPercentTxRetries;							// percent Tx retries in last 2.5 sec
	USHORT	usPercentRxCRCErrors;						// percent CRC errors in last 2.5 sec
	USHORT	usRxRSSI;									// last Rx RSSI
} S24_CHANNEL_QUALITY, *PS24_CHANNEL_QUALITY;

// DEVICE MANUFACTURING INFORMATION

#define	S24_VD_MAX_MFG_ID			32 				// Max. Mfg id length
#define	S24_VD_MAX_MODEL			32				// Max. Adapter model # length
#define	S24_VD_MAX_FW_INFO			12				// Max. FW (version + date) length
#define	S24_VD_MAX_FW_INFO_GALAXY		12					//     Galaxy
#define	S24_VD_MAX_FW_INFO_TRILOGY		32					//     Trilogy
#define	S24_VD_MAX_SERIAL			8				// Max. Adapter serial # length
/*
if   (GALAXY)
	#define	S24_VD_MAX_FW_INFO	S24_VD_MAX_FW_INFO_GALAXY
else
	#define	S24_VD_MAX_FW_INFO	S24_VD_MAX_FW_ TRILOGY_TRILOGY
*/
typedef		struct	S24DeviceInformationParameter {	
	UCHAR	vdManufacture[S24_VD_MAX_MFG_ID];		// Mfg id string
	UCHAR	vdModelNum[S24_VD_MAX_MODEL];			// Adapter model #
	UCHAR	vdFWVersionNum[S24_VD_MAX_FW_INFO];		// FW version + date 
	UCHAR	vdSerialNum[S24_VD_MAX_SERIAL];			// Adapter serial #
} S24_DEV_INFO, *PS24_DEV_INFO;

// FUNCTIONAL MODE

typedef	USHORT	S24_FUNCTIONAL_MODE;		// IN OUT - MU/MAP mode
#define	S24_FUNCTIONAL_MODE_MU			0		// Standard Mobile Unit (MU) mode
#define	S24_FUNCTIONAL_MODE_MAP			1		// MicroAP (MAP) mode

// HARDWARE TYPE

typedef	ULONG	SYM_HARDWARE_TYPE;			
#define	SYM_HARDWARE_TYPE_GALAXY		0				// Galaxy (default if error return from driver)
#define	SYM_HARDWARE_TYPE_TRILOGY		1				// Trilogy

// INDICATION ENABLE STATE

typedef DWORD	S24_INDICATION_STATE;
#define	S24_INDICATION_DISABLED			0
#define	S24_INDICATION_ENABLED			1

// LEAST PREFERRED AP ID 

typedef	USHORT	S24_LEAST_PREF_AP_ID;					// Least Preferred network id.

// LEAST PREFERRED AP MAC ADDRESS

typedef	UCHAR	S24_LEAST_PREF_MAC[S24_MAC_ADDR_LENGTH];// Least preferred Access Point MAC address

// MANDATORY BSS ID 

typedef	UCHAR	S24_MAND_BSS_ID[S24_BSS_ID_LENGTH];		// Mandatory BSS Identifier

// MANDATORY AP ID 

typedef	USHORT	S24_MAND_AP_ID;							// Mandatory Network id.

// MAP ASSOCIATION TABLE

#define	S24_MAX_MAP_TABLE				16				// Max. number of MAP association table entries

// NOTE: The following structure applies only to SPRING
// Firmware Versions 3.x and earlier.

typedef struct 	S24MAPTableDef3 {		   			// MicroAP Table
	UCHAR	AS_Status;									// entry status
#define AS_STATUS_IN_USE				0x80			// set if the entry is in use
#define AS_STATUS_DISASSOC				0x40			// if disassociation is pending
#define AS_STATUS_MODE_CHANGE			0x04			// if a power mode change is needed
#define AS_STATUS_OK_TO_TX				0x02			// if OK to Tx next PSP queue buffer
#define AS_STATS_MODE					0x01			// power mode (0 = CAM, 1 = PSP)
	UCHAR	AS_Address[S24_MAC_ADDR_LENGTH];			// the MU's MAC/IEEE address
	USHORT	AS_Fifo_Head_Ptr;							// ptr to first buffer in PSP Tx queue
	USHORT	AS_Fifo_Tail_Ptr;							// ptr to last buffer in PSP Tx queue
	USHORT	AS_Fifo_Count;								// number of buffer in PSP Tx queue
	USHORT	AS_Station_ID;								// PSP station number for this entry
	USHORT	AS_Stat_Tx_Dir;								// no. of successful directed uniframe
	USHORT	AS_Stat_Tx_Retries;							// no. of Tx retries
	USHORT	AS_Stat_Tx_Failures;						// no. of failed Tx-s, even after retry
	USHORT	AS_Stat_Rx_MAC_Frames;						// no. of MAC packets received
	USHORT	AS_Stat_Duplicate_Frames;					// no. of duplicate Rx frames
	USHORT	AS_Stat_CRC_Errors;							// no. Rx CRC errors
	UCHAR	AS_Tx_Consecutive_Fails;					// number of consecutive Tx failures
	USHORT	AS_Association_Countdown;					// MU activity countdown
	USHORT	AS_PSP_Queue_Countdown;						// countdown for Poll not being Rx-ed
} S24_MAP_TABLE3, *PS24_MAP_TABLE3;

// NOTE: The following structure applies only to IEEE802.11
// Firmware Versions 4.x and later.

typedef struct S24MAPTableDef4 {		   	// MicroAP Table

	UCHAR	AS_Status;					// entry status
#define AS_STATUS_IN_USE4		0x80	// set if the entry is in use
#define AS_STATUS_DISASSOC4		0x40	// if disassociation is pending
#define AS_STATUS_MODE_CHANGE4	0x20	// if a power mode change is needed
#define AS_STATUS_MU_DISASSOC4	0x10	// if disassn initiated by MU is pending
#define AS_STATUS_OK_TO_TX4		0x02	// if OK to Tx next PSP queue buffer
#define AS_STATS_MODE4			0x01	// power mode (0 = CAM, 1 = PSP)

	UCHAR	AS_Address[S24_MAC_ADDR_LENGTH]; // the MU's MAC/IEEE address
	USHORT	AS_Fifo_Head_Ptr;			// ptr to first buffer in PSP Tx queue
	USHORT	AS_Fifo_Tail_Ptr;			// ptr to last buffer in PSP Tx queue
	USHORT	AS_Fifo_Count;				// number of buffer in PSP Tx queue
	USHORT	AS_Station_ID;				// PSP station number for this entry

	USHORT	AS_Stat_Tx_Dir;				// no. of successful directed uniframe
	USHORT	AS_Stat_Tx_Retries;			// no. of Tx retries
	USHORT	AS_Stat_Tx_Failures;		// no. of failed Tx-s, even after retry

	USHORT	AS_Stat_Rx_MAC_Frames;		// no. of MAC packets received
	USHORT	AS_Stat_Duplicate_Frames;	// no. of duplicate Rx frames
	USHORT	AS_Stat_CRC_Errors;			// no. Rx CRC errors

	USHORT	AS_Last_Sequence_Num;		// seq/frag of last directed MPDU

	UCHAR	AS_Tx_Rate;					// current tx rate
	UCHAR	AS_Max_Tx_Rate;				// max tx rate for this station
	USHORT	AS_Rate_Bitmap;				// allowed rates

	USHORT	AS_2MB_Tx_Attempts;			// # 2mb tx attempts in last 10 seconds
	USHORT	AS_2MB_Tx_Unsuccessful;		// # unsuccessful 2mb tx in last 10 sec
	UCHAR	AS_2MB_Tx_Consec_Unsuc;		// # consecutive unsuccessful 2mb tx
	UCHAR	AS_MSE_History;				// 8-bit shift register for MSE history

	UCHAR	AS_Tx_Consecutive_Fails;	// number of consecutive Tx failures
	USHORT	AS_Association_Countdown;	// MU activity countdown
	USHORT	AS_PSP_Queue_Countdown;		// countdown for Poll not being Rx-ed

} S24_MAP_TABLE4, *PS24_MAP_TABLE4;

// MEDIA CONFIGURATION

typedef	struct	S24MediaConfiguration {			// Media configuration
	USHORT	usMajorVersion;								// Driver extension major version #
#define	S24_MC_VER_PRE_802_11			3				// Versions at or before this number are not designed for 802.11
#define	S24_MC_VER_802_11				4				// Versions at or after this number are designed for 802.11
	USHORT	usMinorVersion;								// Driver extension minor version #
	ULONG	ulInterruptNumber; 							// Adapter interrupt number
	ULONG	ulIOAddress;								// Adapter I/O port base address
	ULONG	ulMemAddress;								// Adapter Memory base address
	ULONG	ulMemSize;									// Adapter Memory size
	ULONG	ulAttrMemAddress;							// Adapter Attribute Memory base address
	ULONG	ulAttrMemSize;								// Adapter Attribute Memory size
	ULONG	ulControllerIOAddress;						// PCMCIA controller base address
	ULONG	ulPacketFilterMask;							// Allowable packet filters
	USHORT	usAddressMode;	 							// Adapter addressing mode (memory/IO)
#define	S24_MC_ADDR_MODE_MEMORY	  		0				// Memory mode
#define	S24_MC_ADDR_MODE_IO		  		1				// I/O mode
	USHORT	usCardType;									// Adapter card type
#define	S24_MC_CARD_TYPE_PCMCIA			0				// PC Card
#define	S24_MC_CARD_TYPE_WPOS_ISA		1				// WPOS/ISA card
	USHORT	usSocketNumber;								// PCMCIA socket in use
	USHORT	usScrambleKey;								// Scramble key - obsolete
	USHORT	usDiversity;								// Antenna diversity setting
	USHORT	usNetId;									// Associated AP Net Id
#ifdef	OLD_DRIVER
	USHORT	usRxTimestampLSW;							// Rx timestamp least significant word
	USHORT	usRxTimestampMSW;							// Rx timestamp most significant word
	USHORT	usRxRSSI;									// Rx receive signal strength indicator (RSSI)
	USHORT	usRxChannel;								// Rx current channel (frequency)
	USHORT	usTxStatus;									// Tx status
#else
	USHORT	usReserved1;								// Reserved
	USHORT	usReserved2;								// Reserved
	USHORT	usReserved3;								// Reserved
	USHORT	usReserved4;								// Reserved
	USHORT	usReserved5;								// Reserved
#endif
} S24_MEDIA_CONFIG, *PS24_MEDIA_CONFIG;

// MAP_CONFIGURATION

typedef	struct	S24MicroAPConfiguration {		// MicroAP configuration
	union {
		USHORT	usAPID;									// SPRING MicroAP's AP Id.
		USHORT	usHopSet;								// IEEE802.11 MicroAP's Hop Set.
	} u;
#define	S24_MAP_AP_ID_AUTO	   			255				// Automatic selection
	USHORT	usHopSequence;								// MicroAP's channel (freq) hop sequence
#define	S24_MAP_HOP_SEQ_AUTO    		255				// Automatic selection
	USHORT	usBeaconDelay;								// MicroAP's broadcast beacon delay
#define	S24_MAP_BDELAY_MIN				1				// Minimum beacon delay
#define	S24_MAP_BDELAY_MAX				10				// Maximum beacon delay
#define	S24_MAP_BDELAY_DFLT	  			10				// Default
} S24_MAP_CONFIG, *PS24_MAP_CONFIG;

// MKK CALL SIGN

#define	S24_MKK_CALL_SIGN_LENGTH		16
typedef	UCHAR	S24_MKK_CALL_SIGN[S24_MKK_CALL_SIGN_LENGTH];// MKK call sign

// PREFERRED AP BSS ID 

typedef	UCHAR	S24_PREF_BSS_ID[S24_BSS_ID_LENGTH];		// Preferred BSS Identifier

// PROTOCOL TYPE

typedef ULONG	S24_PROTOCOL_TYPE;
#define	S24_PROTOCOL_SPRING				0
#define	S24_PROTOCOL_80211				1

// RADIO LINK SPEED

typedef	ULONG	S24_RADIO_LINK_SPEED;					// Radio link speed (bps)
typedef	USHORT	S24_LEAST_AP_ID; 						// Least preferred Access Point id.

// RESTART COUNT

typedef	ULONG	S24_RESTART_COUNTER;					// Driver/Adapter restart counter

// ROAMING CONFIGURATION

typedef	struct	S24RoamingConfiguration {		// Roaming configuration
	USHORT	usAPID;										// Preferred/Mandatory Access Point id.
#define	S24_RC_APID_MANDATORY			0x0080			// Indicates this is the only AP ID to associate with (or not at all)
	USHORT	usMinTime;									// Min. time between re-associations (sec.)
	USHORT	usMinRSSI;									// RESERVED
	USHORT	usTxQuality;								// RESERVED
} S24_ROAMING_CONFIG, *PS24_ROAMING_CONFIG;

// SERVICE VERSION

#define	EVENT_MONITOR_VERSION_LENGTH	128
#define	SUPPORT_DLL_VERSION_LENGTH		128

typedef struct S24_SERVICE_VERSION {
	char	cEventMonitorVersion[EVENT_MONITOR_VERSION_LENGTH];
	char	cSupportDLLVersion[SUPPORT_DLL_VERSION_LENGTH];
} S24_SERVICE_VERSION,  *PS24_SERVICE_VERSION;

// SUPPORTED DATA RATES

typedef	struct S24_Supported_Data_Rates {
	USHORT	DATA_RATE_1_MBit;
	USHORT	DATA_RATE_2_MBit;
	USHORT	DATA_RATE_5_5_MBit;
	USHORT	DATA_RATE_11_MBit;
	USHORT	Reserved[4];
} S24_SUPPORTED_DATA_RATES, *PS24_SUPPORTED_DATA_RATES;

#define	S24_DATA_RATE_NOT_SUPPORTED		0
#define	S24_DATA_RATE_SUPPORTED			1
#define	S24_DATA_RATE_MANDATORY			0x81

//--------------------------------------------------------------
// ADAPTER STATISTICS STRUCTURES AND ORDINALS 
//--------------------------------------------------------------

// Ordinal data descriptor.
// NOTE: usDataLength MUST be an EVEN multiple of 4.

typedef	struct	_S24OrdinalDataDesc {					// Ordinal data descriptor
	USHORT	usOrdinal;									// Ordinal value
	USHORT	usDataLength;								// Number of bytes of data/buffer (updated)
} S24_ORDINAL_DESC, *PS24_ORDINAL_DESC;

// Ordinal list descriptor.

typedef struct _S24OrdinalListDesc {					// Ordinal list descriptor
	USHORT	usOrdinalCount;								// # of ordinals
	S24_ORDINAL_DESC	odOrdinalDesc[];				// Array of ordinal descriptors
} S24_ORDINAL_LIST, *PS24_ORDINAL_LIST;

// ORDINAL LIST 

// Fixed size data:

#define	S24_ORD_STAT_TX_HOST_REQUESTS			1		// # of requested Host Tx's (MSDU)
#define	S24_ORD_STAT_TX_HOST_COMPLETE			2		// # of successful Host Tx's (MSDU)
#define	S24_ORD_STAT_TX_DIR_DATA				3		// # of successful Directed Tx's (MSDU)
#define	S24_ORD_STAT_TX_DIR_DATA1				4		// # of successful Directed Tx's (MSDU) @ 1MB
#define	S24_ORD_STAT_TX_DIR_DATA2				5		// # of successful Directed Tx's (MSDU) @ 2MB
#define	S24_ORD_STAT_TX_NONDIR_DATA				6		// # of successful Non-Directed Tx's (MSDU)
#define	S24_ORD_STAT_TX_NONDIR_DATA1			7		// # of successful Non-Directed Tx's (MSDU) @ 1MB
#define	S24_ORD_STAT_TX_NONDIR_DATA2			8		// # of successful Non-Directed Tx's (MSDU) @ 2MB
#define	S24_ORD_STAT_TX_BEACON					9		// # of successful Beacon Tx's
#define	S24_ORD_STAT_TX_POLL					10		// # of successful Poll Tx's
#define	S24_ORD_STAT_TX_WNMP					11		// # of successful WNMP Tx's
#define	S24_ORD_STAT_TX_TOTAL_BYTES				12		// Total # of successful Data Tx Bytes
#define	S24_ORD_STAT_TX_RETRIES					13		// # of Tx retries
#define	S24_ORD_STAT_TX_RETRIES1				14		// # of Tx retries @ 1MB
#define	S24_ORD_STAT_TX_RETRIES2				15		// # of Tx retries @ 2MB
#define	S24_ORD_STAT_TX_FAILURES				16		// # of failed Tx's, even after retries
#define	S24_ORD_STAT_TX_ERR_ACK					17		// # of Tx errors due to Ack
#define	S24_ORD_STAT_TX_ERR_ACK_TIMEOUT			18		// # of Tx errors due timeout waiting for Ack
#define	S24_ORD_STAT_TX_ERR_ACK_TYPE			19		// # of Tx errors due to missing Ack
#define	S24_ORD_STAT_TX_ERR_ACK_ADDRESS			20		// # of Tx errors due to dest. address in Ack
#define	S24_ORD_STAT_TX_ERR_ACK_CRC				21		// # of Tx errors due to CRC error
#define	S24_ORD_STAT_POLL_ERR					22		// # of Poll errors due to Data frame
#define	S24_ORD_STAT_POLL_ERR_PLCP				23		// # of Poll errors due to Data frame PLCP timeouts
#define	S24_ORD_STAT_POLL_ERR_TYPE				24		// # of Poll errors due to bad type for Data frame
#define	S24_ORD_STAT_POLL_ERR_ADDRESS			25		// # of Poll errors due to dest. address mismatch
#define	S24_ORD_STAT_TX_ABORT_AT_HOP			26		// # of Tx's aborted at hop time
#define	S24_ORD_STAT_TX_MAX_TRIES_IN_HOP		27		// # of times all allowed tries in a given hop failed
#define	S24_ORD_STAT_TX_ABORT_LATE_DMA			28		// # of Tx aborts due to late DMA switch
#define	S24_ORD_STAT_LATE_RX_TO_TX				29		// # of times RxToTx was set too late
#define	S24_ORD_STAT_TX_RETRY_THRESHOLD			30		// # of  Tx's where Tx retry threshold was exceeded
#define	S24_ORD_STAT_TX_ABORT_STX				31		// # of times backoff was aborted
#define	S24_ORD_STAT_TX_ABORT_STX_MAP			32		// # of times backoff for BCN was aborted
#define	S24_ORD_STAT_RX_HOST					33		// # of Rx packets passed to host
#define	S24_ORD_STAT_RX_DIR_DATA				34		// # of Directed Data Rx's
#define	S24_ORD_STAT_RX_DIR_DATA1				35		// # of Directed Data Rx's @ 1MB
#define	S24_ORD_STAT_RX_DIR_DATA2				36		// # of Directed Data Rx's @ 2MB
#define	S24_ORD_STAT_RX_NONDIR_DATA				37		// # of Non-Directed Data Rx's
#define	S24_ORD_STAT_RX_NONDIR_DATA1			38		// # of Non-Directed Data Rx's @ 1MB
#define	S24_ORD_STAT_RX_NONDIR_DATA2			39		// # of Non-Directed Data Rx's @ 2MB
#define	S24_ORD_STAT_RX_BEACON					40		// # of received Beacons
#define	S24_ORD_STAT_RX_POLL					41		// # of received Polls
#define	S24_ORD_STAT_RX_WNMP					42		// # of received WNMP
#define	S24_ORD_STAT_RX_TOTAL_BYTES				43		// Total # of successful MPDU Rx bytes
#define	S24_ORD_STAT_RX_ERR_CRC					44		// # of Rx CRC errors
#define	S24_ORD_STAT_RX_ERR_CRC1				45		// # of Rx CRC errors @ 1MB
#define	S24_ORD_STAT_RX_ERR_CRC2				46		// # of Rx CRC errors @ 2MB
#define	S24_ORD_STAT_RX_DUPLICATE				47		// # of duplicate Rx MPDU's
#define	S24_ORD_STAT_RX_TOO_LATE				48		// # of frames w/PLCP read too late
#define	S24_ORD_STAT_RX_ABORTED					49		// # of frames aborted due to CRUX anomaly
#define	S24_ORD_STAT_RX_INVALID_PLCP			50		// # of frames Rx'ed w/CRC error in PLCP
#define	S24_ORD_STAT_RX_PLCP_TOO_BIG			51		// # of frames with PLCP length too big
#define	S24_ORD_STAT_RX_PLCP_TOO_SMALL			52		// # of frames with PLCP length too small
#define	S24_ORD_STAT_RX_INVALID_PROTOCOL		53		// # of frames Rx'ed w/invalid protocol version
#define	S24_ORD_STAT_RX_INVALID_TYPE			54		// # of invalid types after SFD
#define	S24_ORD_STAT_RX_NO_BUFFER				55		// # of frames dropped due to no buffers
#define	S24_ORD_STAT_RX_ABORT_LATE_DMA			56		// # of times Rx aborted due to returning from
														// suspension too late to switch DMA buffers
#define	S24_ORD_STAT_RX_ABORT_AT_HOP			57		// # of Rx's aborted at hop time
#define	S24_ORD_STAT_RX_CRUX_ERROR2				58		// Rx error: TC reached, CRC32G=0,RXCNT=0
#define	S24_ORD_STAT_RX_CRUX_ERROR3				59		// Rx error: TC reached, CRC32G=1, but more bytes
														// DMA'ed than expected
#define	S24_ORD_STAT_MISSING_FRAGMENTS			60		// # of dropped Rx fragments
#define	S24_ORD_STAT_ORPHAN_FRAGMENTS			61		// # of non-sequential fragments
#define	S24_ORD_STAT_ORPHAN_FRAME				62		// # of unmatched non-first frames
#define	S24_ORD_STAT_FRAGMENT_AGEOUT			63		// # of unmatched branches aged out
#define	S24_ORD_STAT_RX_BAD_SRC_ADR				64		// Src_Adr not in Association Table
#define	S24_ORD_STAT_RX_BAD_SSID				65		// ESSID does not match MAP's ESSID
#define	S24_ORD_STAT_RX_BAD_STATION_ID			66		// Station ID mismatch in Poll
#define	S24_ORD_STAT_RX_MULTIPLE_POLLS			67		// Multiple Polls Rx'ed for same packet
#define	S24_ORD_STAT_RX_POLL_NO_DATA			68		// Poll but no data queued
#define	S24_ORD_STAT_RX_POLL_TIMEOUT			69		// MAP timeout waiting for Poll
#define	S24_ORD_STAT_PSP_SUSPENSIONS			70		// # of PSP adapter suspensions
#define	S24_ORD_STAT_PSP_RX_TIMS				71		// # of PSP TIM's received
#define	S24_ORD_STAT_PSP_RX_DTIMS				72		// # of PSP DTIM's received
#define	S24_ORD_STAT_PSP_BEACON_TIMEOUTS		73		// # of timeouts waiting for Beacons
#define	S24_ORD_STAT_PSP_POLL_TIMEOUTS			74		// # of timeouts waiting for Poll response
#define	S24_ORD_STAT_PSP_BCAST_TIMEOUT			75		// # of timeouts waiting for the last broadcast/
														// multicast msg. when in PSP mode
#define	S24_ORD_STAT_FIRST_ASSOC_TIME			76		// Time of first AP association
#define	S24_ORD_STAT_LAST_ASSOC_TIME			77		// Time of last AP association
#define	S24_ORD_STAT_PERCENT_MISSED_BEACONS		78		// % missed Beacons in the last 5 seconds
#define	S24_ORD_STAT_PERCENT_RETRIES			79		// % Tx retries in the last 2.5 seconds
#define	S24_ORD_STAT_PERCENT_CRC_ERRORS			80		// % CRC errors in the last 2.5 seconds
#define	S24_ORD_STAT_ASSOCIATED_AP				81		// ptr. to the AP Table entry for the currently associated AP
#define	S24_ORD_STAT_AVAIL_AP_CNT				82		// # of AP's described in the AP Table
#define	S24_ORD_STAT_AP_LIST					83		// ptr. to the linked list of available AP's
#define	S24_ORD_STAT_AP_ASSOCIATIONS			84		// # of AP associations
#define	S24_ORD_STAT_ASSOCIATE_FAIL				85		// # of failed AP associations
#define	S24_ORD_STAT_ASSOC_RESPONSE_FAIL		86		// # of failed association responses
#define	S24_ORD_STAT_FULL_SCANS					87		// # of full scans performed
#define	S24_ORD_STAT_PARTIAL_SCANS				88		// # of partial scans performed
#define	S24_ORD_STAT_ROAM_INHIBIT				89		// Roaming is / is not inhibited
#define	S24_ORD_STAT_RSSI_PEAK					90		// Peak RSSI for the  associated AP (LSB=1/4 counts)
#define	S24_ORD_STAT_RSSI_TRIGGER				91		// RSSI value at which a PSS is triggered (LSB=1/4 counts)
#define	S24_ORD_STAT_RSSI_TRIGGER_RESETS		92		// # of RSSI_Trigger resets
#define	S24_ORD_STAT_ASSOC_CAUSE1				93		// Assoc. cause: No Tx seen from the AP in the last N hops
														// or no Probe Response seen in 3 minutes
#define	S24_ORD_STAT_ASSOC_CAUSE2				94		// Assoc. cause: AP RSSI fell below the eligble threshold
#define	S24_ORD_STAT_ASSOC_CAUSE3				95		// Assoc. cause: Association dropped by the AP
														// (reported in a Probe Response)
#define	S24_ORD_STAT_ASSOC_CAUSE4				96		// Assoc. cause: Poor Rx/Tx quality
#define	S24_ORD_STAT_ASSOC_CAUSE5				97		// Assoc. cause: AP load leveling
#define	S24_ORD_STAT_ASSOC_CAUSE6				98		// Assoc. cause: Power mode change
#define	S24_ORD_STAT_PSS_CAUSE1					99		// Partial scan series cause: Unassociated
#define	S24_ORD_STAT_PSS_CAUSE2					100		// Partial scan series cause: RSSI dropped 8 counts from the peak
#define	S24_ORD_STAT_PSS_CAUSE3					101		// Partial scan series cause: RSSI dropped an additional 3 counts
#define	S24_ORD_STAT_PSS_CAUSE4					102		// Partial scan series cause: Poor Rx/Tx quality
#define	S24_ORD_STAT_PSS_CAUSE5					103		// Partial scan series cause: AP load leveling
#define	S24_ORD_STAT_PSS_CUM_CAUSE1				104		// Cum. Partial scan series cause: Unassociated
#define	S24_ORD_STAT_PSS_CUM_CAUSE2				105		// Cum. Partial scan series cause: RSSI dropped 8
														// counts from the peak
#define	S24_ORD_STAT_PSS_CUM_CAUSE3				106		// Cum. Partial scan series cause: RSSI dropped an
														// additional 3 counts
#define	S24_ORD_STAT_PSS_CUM_CAUSE4				107		// Cum. Partial scan series cause: Poor Rx/Tx quality
#define	S24_ORD_STAT_PSS_CUM_CAUSE5				108		// Cum. Partial scan series cause: AP load leveling
#define	S24_ORD_STAT_STEST_RESULTS_CURR			109		// Results of the last self-test
#define	S24_ORD_STAT_STEST_RESULTS_CUM			110		// Cumulative results of the self-test
#define	S24_ORD_STAT_SELFTEST_STATUS			111		// Current self-test status
#define	S24_ORD_STAT_POWER_MODE					112		// Current power mgt. Mode 0=CAM, 10h=PSP
#define	S24_ORD_STAT_TIM_ALGORITHM				113		// TIM interval algorithm number
#define	S24_ORD_STAT_TIM_MINIMUM				114		// Minimum TIM listen interval
#define	S24_ORD_STAT_TIM_MAXIMUM				115		// Maximum TIM listen interval
#define	S24_ORD_STAT_TX_DISABLED				116		// Transmitter disable state: 0=enabled, 1=disabled
#define	S24_ORD_STAT_COUNTRY_CODE				117		// Country code: 1=US, 2=Japan, 3=Fra, etc.
#define	S24_ORD_STAT_SCRAMBLE_KEY				118		// Data scramble key (default=0)
#define	S24_ORD_STAT_STATION_ID					119		// Station ID for PSP mode
#define	S24_ORD_STAT_RESET_COUNT				120		// Resets since power up
#define	S24_ORD_STAT_BEACON_INTERVAL			121		// Beacon interval (LSB = 1K us)
#define	S24_ORD_STAT_CRUX_VERSION				122		// CRUX config. Register version
#define	S24_ORD_STAT_MOON_VERSION				123		// Moon config. Register version
#define	S24_ORD_STAT_ANTENNA_DIVERSITY			124		// Antenna diversity state: 0=enabled, 1=disabled
#define	S24_ORD_STAT_MKK_RSSI					125		// Manufacturing RSSI threshold
#define	S24_ORD_STAT_FLASH_UPDATE				126		// # of times FLASH was updated
#define	S24_ORD_STAT_CW_TX						127		// # of times Tx started on top of CW
#define	S24_ORD_STAT_HOP_SET					128		// Hop set number
#define	S24_ORD_STAT_HOP_PATTERN				129		// Hop pattern number
#define	S24_ORD_STAT_HOP_INDEX					130		// Hop index
#define	S24_ORD_STAT_DWELL_TIME					131		// Hop interval (LSB = 1K us)
#define	S24_ORD_STAT_DTIM_PERIOD				132		// # of Beacon intervals between DTIM's
#define	S24_ORD_STAT_OUR_HOP_SEQUENCE			133		// Hop set/hop set index
#define	S24_ORD_STAT_OUR_HOP_DELTA				134		// Hop delta from hop  table
#define	S24_ORD_STAT_OUR_FREQUENCY				135		// Current frequency
#define S24_ORD_STAT_TX_NULL_DATA				136		// Successful null data Tx's
#define S24_ORD_STAT_TX_AUTHENTICATION			137		// Successful authentication Tx's
#define S24_ORD_STAT_TX_ASSOCIATION				138		// Successful association Tx's
#define S24_ORD_STAT_TX_ASSOC_RESP				139		// Successful association response Tx's
#define S24_ORD_STAT_TX_DEAUTHENTICATION		140		// Successful deauthentic Tx's
#define S24_ORD_STAT_TX_DISASSOCIATION			141		// Successful disassoc Tx's
#define S24_ORD_STAT_TX_PROBE					142		// Successful probe Tx
#define S24_ORD_STAT_TX_PROBE_RESP				143		// Successful probe response Tx
#define S24_ORD_STAT_TX_RTS						144		// Successful RTS
#define S24_ORD_STAT_TX_CTS						145		// Successful CTS
#define S24_ORD_STAT_TX_CFEND					146		// Successful CF end
#define S24_ORD_STAT_RX_NULL_DATA				147		// Receive null data msg
#define S24_ORD_STAT_RX_AUTHENTICATION			148		// Receive authentication msg
#define S24_ORD_STAT_RX_ASSOCIATION				149		// Receive association msg
#define S24_ORD_STAT_RX_ASSOC_RESP				150		// Receive assoc resp msg
#define S24_ORD_STAT_RX_DEAUTHENTICATION		151		// Receive deauthentic msg
#define S24_ORD_STAT_RX_DISASSOCIATION			152		// Receive disassoc msg
#define S24_ORD_STAT_RX_PROBE					153		// Receive probe msg
#define S24_ORD_STAT_RX_PROBE_RESP				154		// Receive probe resp msg
#define S24_ORD_STAT_RX_RTS						155		// Receive RTS frame
#define S24_ORD_STAT_RX_CTS						156		// Receive CTS frame
#define S24_ORD_STAT_RX_CFEND					157		// Receive CF end
#define S24_ORD_STAT_RX_DUPLICATE1				158		// Duplicate msgs at 1 mbps
#define S24_ORD_STAT_RX_DUPLICATE2				159		// Duplicate msgs at 2 mbps
#define S24_ORD_STAT_AUTHENTICATE_FAIL			160		// Authentication phase fail
#define S24_ORD_STAT_AUTHENTICATE_RESP_FAIL		161		// Authentication resp phase fail
#define S24_ORD_STAT_ASSOC_CAUSE7				162		// Assoc. cause: ESSID change
#define S24_ORD_STAT_ASSOC_CAUSE8				163		// Assoc. cause: Preferred/Mandatory change
#define S24_ORD_RTC_TIME						164		// 30.5 usec RTC time
#define S24_ORD_OP_MODE							165		// Operating mode
#define S24_ORD_ASSOCIATION_EVENTS				166		// Association events/status
#define S24_ORD_ASSOC_TABLE_COUNT				167		// Number of associated MUs
#define S24_ORD_DEBUG_PROGRAM_COUNTER			168		// Diagnostic program counter
#define S24_ORD_ADAPTER_POWER_TYPE				169		// Adapter power (100/500)
#define S24_ORD_TX_RATE							170		// Current Tx rate
#define S24_ORD_MAX_TX_RATE						171		// Max Tx rate allowed by MU
#define S24_ORD_MAX_ASSOC_TX_RATE				172		// Max Tx rate allowed by currently assoc AP
#define S24_ORD_CAPABILITY_INFO					173		// IEEE802.11 required - no practical use
#define S24_ORD_STAT_TX_HW_FAIL					174		// # of time Tx completed before Xmit dma completed
#define S24_ORD_BASIC_RATES_BITMAP				175		// Bitmap of allowed data rates
#define S24_ORD_STAT_DISASSOCIATION_FAIL		176		// # of times disassociation frame sent, not ack'ed
#define	S24_ORD_ACL_ENTRIES						177		// # of entries in Micro AP ACL
#define	S24_ORD_AUTHENTICATION_TYPE				178		// Open system / shared key
#define S24_ORD_ICV_ERRORS						179		// # of ICV errors
#define S24_ORD_RADIO_TYPE						180		// Type of radio installed
#define S24_ORD_VARIABLE_CPU_SPEED				181		// Capable or not
#define S24_ORD_CTS								182		// # of CTS errors - LSW
#define S24_ORD_CTS_MSW							183		// # of CTS errors - MSW
#define S24_ORD_RTS_THRESHOLD					184		// RTS threshold
#define S24_ORD_DISCOVER_MODE_COUNT				189		// Discover Mode Count

// New Trilogy Stuff
#define SYM_ORD_STAT_ASSOC_MU_COUNT				211		// count of associated mus
#define SYM_ORD_STAT_LOWEST_MU_COUNT			212		// lowest count of associated mus
#define	SYM_ORD_STAT_LOAD_LEVELING_SCANS		213		// # of load leveling scans
#define	SYM_ORD_STAT_PERIODIC_SCANS				214		// # of periodic scans
#define	SYM_ORD_STAT_SRTSEQ_PTR					215		// Pointer to ordered ap list
#define	SYM_ORD_STAT_RATE_LOG_INDEX				216		// 'Next' entry in rate log
#define	SYM_ORD_STAT_RATE_LOG_WRAP				217		// Rate log has wrapped or not
#define SYM_ORD_STAT_ENABLE_COUNT				218		// # of adapter enables
#define	SYM_ORD_STAT_DISABLE_COUNT				219		// # of adapter disables
#define S24_ORD_STAT_TX_DIR_DATA5_5				220		// # of successful Directed Tx @ 5.5MB
#define S24_ORD_STAT_TX_DIR_DATA11				221		// # of successful Directed Tx @ 11MB
#define S24_ORD_STAT_TX_NONDIR_DATA5_5			222		// # of successful Non-Direct Tx @5.5MB 
#define S24_ORD_STAT_TX_NONDIR_DATA11			223		// # of successful Non-Direct Tx @ 11MB
#define S24_ORD_STAT_TX_RETRIES5_5				224		// # of Tx retries @ 5.5MB
#define S24_ORD_STAT_TX_RETRIES11				225		// # of Tx retries @ 11MB
#define S24_ORD_STAT_RX_DIR_DATA5_5				226		// # of Directed Data Rx @ 5.5MB
#define S24_ORD_STAT_RX_DIR_DATA11				227		// # of Directed Data Rx @ 11MB
#define S24_ORD_STAT_RX_NONDIR_DATA5_5			228		// # of successful Non-Direct Rx @5.5MB 
#define S24_ORD_STAT_RX_NONDIR_DATA11			229		// # of successful Non-Direct Rx @ 11MB
#define S24_ORD_STAT_RX_ERR_CRC5_5				230		// # of Rx CRC errors @ 5.5MB
#define S24_ORD_STAT_RX_ERR_CRC11				231		// # of Rx CRC errors @ 11MB
#define S24_ORD_STAT_RX_DUPLICATE5_5			232		// # of Rx Duplicate msgs @ 5.5 MB
#define S24_ORD_STAT_RX_DUPLICATE11				233		// # of Rx Duplicate msgs at 11MB
#define SYM_ORD_STAT_TIME						234		// Location of firmware time
#define	SYM_ORD_STAT_MAX_BASIC_RATE				235
#define	SYM_ORD_STAT_CURRENT_RATES				236
#define	SYM_ORD_STAT_LOG_INDEX					237		// 'Next' entry in roam log
#define	SYM_ORD_STAT_LOG_WRAP					238		// Log has wrapped or not
#define	SYM_ORD_STAT_FREE_BUFFERS				239
#define SYM_ORD_STAT_TX_NOR_Q_COUNT				240
#define SYM_ORD_STAT_TX_EXP_Q_COUNT				241
#define	SYM_ORD_STAT_TX_PRND_Q_COUNT			242
#define	SYM_ORD_STAT_TX_ALLOC_Q_COUNT			243
#define	SYM_ORD_STAT_TX_COMP_Q_COUNT			244
#define	SYM_ORD_STAT_RX_ACTIV_Q_COUNT			245
#define	SYM_ORD_STAT_RX_Q_COUNT					246
#define	SYM_ORD_STAT_AP_TABLE_START				247
#define	SYM_ORD_STAT_CHANNEL_QUALITY			248
#define	SYM_ORD_STAT_AVERAGE_SIGNAL_LEVEL		249
#define	SYM_ORD_STAT_AVERAGE_NOISE_LEVEL		250

#define	LAST_FIXED_LENGTH_ORDINAL				250

// Variable length data:

#define FIRST_VARIABLE_LENGTH_ORDINAL			1001

#define	S24_ORD_STAT_BSSID						1001	// Adapter BSSID (MAC address)
#define	S24_ORD_STAT_PREFERRED_BSSID			1002	// BSSID of the preferred AP
#define	S24_ORD_STAT_MANDATORY_BSSID			1003	// BSSID of the mandatory AP
#define	S24_ORD_STAT_CALL_SIGN					1004	// MKK call sign
#define	S24_ORD_STAT_COUNTRY_TEXT				1005	// Country code (text)
#define	S24_ORD_STAT_FW_REV						1006	// Firmware version string
#define	S24_ORD_STAT_SSID						1007	// ESSID string
#define	S24_ORD_STAT_AP_TABLE					1008	// AP Table
#define	S24_ORD_STAT_AC_TABLE					1009	// Access control list (MAP only)
#define	S24_ORD_STAT_ASSOC_TABLE				1010	// MU Association Table (MAP only)
#define	S24_ORD_STAT_ROAM_LOG					1011	// Roaming event log
#define	S24_ORD_STAT_FIFO						1012	// Address of F/W FIFO structures
#define	S24_ORD_STAT_STATISTICS					1013	// Address of F/W Statistics structures
#define S24_ORD_FIRMWARE_REVISION				1014	// Firmware version string
#define S24_ORD_FIRMWARE_DATE					1015	// Firmware date string
#define S24_ORD_OUR_AP_ADDRESS					1016	// BSS_ID of associated AP
#define S24_ORD_WLAN_PROFILE_NAME				1017	// Current WLAN Profile Use
#define S24_ORD_WLAN_OPERATION_MODE				1018	// Current WLAN Operating mode

#define LAST_VARIABLE_LENGTH_ORDINAL			1018

#define MAX_STR_LEN								80
#define	PEERS_LIST_SIZE							800



