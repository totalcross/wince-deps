
//--------------------------------------------------------------------
// FILENAME:			RcmDef.h
//
// Copyright(c) 1999-2001 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Constants and types used by resource coordinator functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef RCMDEF_H_
#define RCMDEF_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested Includes
//--------------------------------------------------------------------

#include <StrucInf.h>


//--------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------


#define RCMAPI WINAPI

// Hard Trigger State
#define TRIGGER_STAGE1_MASK		0x000000ff
#define TRIG1_STAGE1			0x00000001
#define TRIG2_STAGE1			0x00000002
#define TRIG3_STAGE1			0x00000004
#define TRIG4_STAGE1			0x00000008
#define TRIG5_STAGE1			0x00000010
#define TRIG6_STAGE1			0x00000020
#define TRIG7_STAGE1			0x00000040
#define EXT_TRIG_STAGE1			0x00000080

#define TRIGGER_STAGE2_MASK		0x0000ff00
#define TRIG1_STAGE2			0x00000100
#define TRIG2_STAGE2			0x00000200
#define TRIG3_STAGE2			0x00000400
#define TRIG4_STAGE2			0x00000800
#define TRIG5_STAGE2			0x00001000
#define TRIG6_STAGE2			0x00002000
#define TRIG7_STAGE2			0x00004000
#define EXT_TRIG_STAGE2			0x00008000

#define TRIGGER_ALL_MASK		( TRIGGER_STAGE1_MASK | TRIGGER_STAGE2_MASK )
#define TRIGGER_NONE_MASK		0x00000000


#define MAX_DEVICE_NAME 6


//--------------------------------------------------------------------
// Pre-defined Semaphore Names
//--------------------------------------------------------------------

#define SCANNER_RADIO_LOCKOUT TEXT("SCANNER_RADIO_LOCKOUT")


//--------------------------------------------------------------------
// Types
//--------------------------------------------------------------------


typedef BYTE UNITID[8];
typedef UNITID FAR * LPUNITID;


typedef struct tagUNITID_EX
{
	STRUCT_INFO StructInfo;			// Information about structure extents

	BYTE byUUID[24];				// Up to 24 bytes of unit ID

} UNITID_EX;

typedef UNITID_EX FAR * LPUNITID_EX;


typedef struct tagTEMPERATURE_INFO
{
	STRUCT_INFO StructInfo;			// Information about structure extents

	LONG lTemperature;

} TEMPERATURE_INFO;

typedef TEMPERATURE_INFO FAR * LPTEMPERATURE_INFO;


typedef enum tagPROFILE_FLAGS
{
	PROFILE_EARLIESTSTARTTIME	= 0x00000001,	// EarliestStartTime field is present/used
	PROFILE_LATESTSTARTTIME		= 0x00000002,	// LatestStartTime field is present/used
	PROFILE_MINDURATION			= 0x00000004,	// dwMinDuration field is present/used
	PROFILE_MAXDURATION			= 0x00000008,	// dwMaxDuration field is present/used
	PROFILE_MAXPOWER			= 0x00000010,	// dwMaxPower field is present/used
	PROFILE_SEMAPHORE			= 0x00000020,	// szSemaphore field is present/used
};

enum tagPROFILE_PRIORITY
{
	PROFILE_PRIORITY_LOWEST			= 0,
	PROFILE_PRIORITY_LOW			= 1,
	PROFILE_PRIORITY_BELOW_NORMAL	= 2,
	PROFILE_PRIORITY_NORMAL			= 3,
	PROFILE_PRIORITY_ABOVE_NORMAL	= 4,
	PROFILE_PRIORITY_HIGH			= 5,
	PROFILE_PRIORITY_VERY_HIGH		= 6,
	PROFILE_PRIORITY_HIGHEST		= 7
};

typedef DWORD HPROFILE;

typedef HPROFILE FAR * LPHPROFILE;

typedef struct tagPROFILE_A
{
	STRUCT_INFO StructInfo;			// Information about structure extents

	HPROFILE	hProfile;			// Profile handle assigned by the Resource Coordinator

	BOOL bRequested;				// Profile enable requested flag
									//
									//   FALSE: Enabling of profile is not requested
									//			(Creator does not need the resources)
									//
									//   TRUE:  Enabling of profile is requested
									//			(Creator needs the resources)
									//
									//	Note:	If this flag is FALSE, the profile
									//			might still be enabled by the
									//			resource coordinator if no other
									//			profile is requesting it.  This
									//			flag merely indicates if the creator
									//			has a pressing need to use the
									//			the resources controlled by this
									//			profile.

	CHAR szDeviceName[MAX_DEVICE_NAME];		// Name of device to use with IOCTL codes

	DWORD dwEnableIoctl;					// IOCTL code for enable notify

	DWORD dwDisableIoctl;					// IOCTL code for disable notify
	
	
	int nPriority;					// Priority of profile
									//	(Compared to other active profiles)
									//
									//	See tagPROFILE_PRIORITY above
									//
									//  Note:	Required field

	DWORD dwFlags;					// Flags indicating which fields of the
									//   profile structure are present/used
									//
									//	See tagPROFILE_FLAGS above

	SYSTEMTIME EarliestStartTime;	// Earliest time the profile can be enabled
									//	(Compared against current system time)
									//
									//	Note:	Used only if the
									//			PROFILE_EARLIESTSTARTTIME
									//			flag is set in dwFlags.  If not 
									//			used, the profile is allowed to 
									//			start at any time.  This is a hint
									//			to the resource coordinator that
									//			the creator does not expect to
									//			need the resources until the
									//			specified time.  This would
									//			be used to improve "look ahead".

	SYSTEMTIME LatestStartTime;		// Latest time by which activity must begin
									//	(Compared against current system time)
									//
									//	Note:	Used only if the
									//			PROFILE_LATESTSTARTTIME flag
									//			is set in dwFlags.  If not used,
									//			the profile is assumed to have
									//			no "deadline".  This is a hint
									//			to the resource coordinator to
									//			help arbitrate the assignment of
									//			resources when the priorities
									//			are indecisive.

	DWORD dwMinDuration;			// Shortest duration activity could last
									//	(Delta in ms from actual time profile is enabled)
									//
									//	Note:	Used only if the
									//			PROFILE_MINDURATION flag is set
									//			in dwFlags.  If not used, the
									//			profile is assumed to have no
									//			minimum duration.  This is a hint
									//			to the resource coordinator that
									//			the resources, once allocated,
									//			are expected need be continuously
									//			needed for at least the specified
									//			time.

	DWORD dwMaxDuration;			// Longest duration activity could last
									//	(Delta in ms from actual time profile is enabled)
									//
									//	Note:	Used only if the
									//			PROFILE_MAXDURATION flag is set
									//			in dwFlags.  If not used, the
									//			profile is assumed to have no
									//			maximum duration.  This is a hint
									//			to the resource coordinator that
									//			the resources, once allocated, might
									//			be needed for as long as the specified
									//			time.

	DWORD dwMaxPower;				// Maximum additional power used by profile
									//	(In system power units = mA)
									//
									//	Note:	Used only if the PROFILE_MAXPOWER 
									//			flag is set in dwFlags.  If not
									//			used, the profile is assumed to
									//			have no power requirements.

	CHAR szSemaphore[65];			// Name of semaphore required for activity
									//	(No two profiles with the same
									//	 semaphore name can be enabled at
									//	 the same time)
									//
									//	Note:	Used only if the PROFILE_SEMAPHORE
									//			flag is set in dwFlags.  If not
									//			used, the profile is assumed to
									//			 have no semaphore requirements.

} PROFILE_A;

typedef PROFILE_A FAR * LPPROFILE_A;

typedef struct tagPROFILE_W
{
	STRUCT_INFO StructInfo;			// Information about structure extents

	HPROFILE	hProfile;			// Profile handle assigned by the Resource Coordinator

	BOOL bRequested;				// Profile enable requested flag
									//
									//   FALSE: Enabling of profile is not requested
									//			(Creator does not need the resources)
									//
									//   TRUE:  Enabling of profile is requested
									//			(Creator needs the resources)
									//
									//	Note:	If this flag is FALSE, the profile
									//			might still be enabled by the
									//			resource coordinator if no other
									//			profile is requesting it.  This
									//			flag merely indicates if the creator
									//			has a pressing need to use the
									//			the resources controlled by this
									//			profile.

	WCHAR szDeviceName[MAX_DEVICE_NAME];	// Name of device to use with IOCTL codes

	DWORD dwEnableIoctl;					// IOCTL code for enable notify

	DWORD dwDisableIoctl;					// IOCTL code for disable notify
	
	
	int nPriority;					// Priority of profile
									//	(Compared to other active profiles)
									//
									//	See tagPROFILE_PRIORITY above
									//
									//  Note:	Required field

	DWORD dwFlags;					// Flags indicating which fields of the
									//   profile structure are present/used
									//
									//	See tagPROFILE_FLAGS above

	SYSTEMTIME EarliestStartTime;	// Earliest time the profile can be enabled
									//	(Compared against current system time)
									//
									//	Note:	Used only if the
									//			PROFILE_EARLIESTSTARTTIME
									//			flag is set in dwFlags.  If not 
									//			used, the profile is allowed to 
									//			start at any time.  This is a hint
									//			to the resource coordinator that
									//			the creator does not expect to
									//			need the resources until the
									//			specified time.  This would
									//			be used to improve "look ahead".

	SYSTEMTIME LatestStartTime;		// Latest time by which activity must begin
									//	(Compared against current system time)
									//
									//	Note:	Used only if the
									//			PROFILE_LATESTSTARTTIME flag
									//			is set in dwFlags.  If not used,
									//			the profile is assumed to have
									//			no "deadline".  This is a hint
									//			to the resource coordinator to
									//			help arbitrate the assignment of
									//			resources when the priorities
									//			are indecisive.

	DWORD dwMinDuration;			// Shortest duration activity could last
									//	(Delta in ms from actual time profile is enabled)
									//
									//	Note:	Used only if the
									//			PROFILE_MINDURATION flag is set
									//			in dwFlags.  If not used, the
									//			profile is assumed to have no
									//			minimum duration.  This is a hint
									//			to the resource coordinator that
									//			the resources, once allocated,
									//			are expected need be continuously
									//			needed for at least the specified
									//			time.

	DWORD dwMaxDuration;			// Longest duration activity could last
									//	(Delta in ms from actual time profile is enabled)
									//
									//	Note:	Used only if the
									//			PROFILE_MAXDURATION flag is set
									//			in dwFlags.  If not used, the
									//			profile is assumed to have no
									//			maximum duration.  This is a hint
									//			to the resource coordinator that
									//			the resources, once allocated, might
									//			be needed for as long as the specified
									//			time.

	DWORD dwMaxPower;				// Maximum additional power used by profile
									//	(In system power units = mA)
									//
									//	Note:	Used only if the PROFILE_MAXPOWER 
									//			flag is set in dwFlags.  If not
									//			used, the profile is assumed to
									//			have no power requirements.

	WCHAR szSemaphore[65];			// Name of semaphore required for activity
									//	(No two profiles with the same
									//	 semaphore name can be enabled at
									//	 the same time)
									//
									//	Note:	Used only if the PROFILE_SEMAPHORE
									//			flag is set in dwFlags.  If not
									//			used, the profile is assumed to
									//			 have no semaphore requirements.

} PROFILE_W;

typedef PROFILE_W FAR * LPPROFILE_W;

#ifdef UNICODE
#define	PROFILE	PROFILE_W
#define	LPPROFILE LPPROFILE_W
#else
#define	PROFILE	PROFILE_A
#define	LPPROFILE LPPROFILE_A
#endif


// The following structure is used to register a trigger event handler.
// If hWnd is non-NULL, notification is via the specified Windows message.
// If hWnd is NULL, notification is via an IOCTL call to the specified device.

typedef DWORD HRCMEVENT;

typedef HRCMEVENT FAR * LPHRCMEVENT;

typedef struct tagRCM_EVENT_A
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	HRCMEVENT	hRCMEvent;			// Profile handle assigned by the Resource Coordinator
	DWORD	dwTriggerMask;			// Trigger event(s) to monitor
	BOOL	bExclusive;				// Flag indicating exclusive trigger use required
	HWND	hWnd;					// Window handle to receive message
	UINT	uiMessage;				// Message number to send
	DWORD	dwIoctl;						// IOCTL code to use for event notifications
	CHAR	szDeviceName[MAX_DEVICE_NAME];	// Name of device to use with IOCTLs
	CHAR	szEventName[MAX_PATH];	// Name of event to set for event notification

} RCM_EVENT_A;

typedef RCM_EVENT_A FAR * LPRCM_EVENT_A;

typedef struct tagRCM_EVENT_W
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	HRCMEVENT	hRCMEvent;			// Profile handle assigned by the Resource Coordinator
	DWORD	dwTriggerMask;			// Trigger event(s) to monitor
	BOOL	bExclusive;				// Flag indicating exclusive trigger use required
	HWND	hWnd;					// Window handle to receive message
	UINT	uiMessage;				// Message number to send
	DWORD	dwIoctl;						// IOCTL code to use for event notifications
	WCHAR	szDeviceName[MAX_DEVICE_NAME];	// Name of device to use with IOCTLs
	WCHAR	szEventName[MAX_PATH];	// Name of event to set for event notification

} RCM_EVENT_W;

typedef RCM_EVENT_W FAR * LPRCM_EVENT_W;

#ifdef UNICODE
#define	RCM_EVENT	RCM_EVENT_W
#define	LPRCM_EVENT	LPRCM_EVENT_W
#else
#define	RCM_EVENT	RCM_EVENT_A
#define	LPRCM_EVENT	LPRCM_EVENT_A
#endif


typedef struct tagENABLE_DISABLE_PROFILE
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	
	HPROFILE	hProfile;			// Handle of profile to enable or disable

	BOOL		bEnabled;			// Enable/Disable flag
									//	FALSE = Disabled
									//	TRUE = Enabled

} ENABLE_DISABLE_PROFILE;

typedef ENABLE_DISABLE_PROFILE FAR * LPENABLE_DISABLE_PROFILE;


typedef struct tagTRIGGERSTATUS
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	
	DWORD		dwOldTriggerStatus;	// Old trigger status (before change)

	DWORD		dwNewTriggerStatus;	// New trigger status (after change)

} TRIGGER_STATUS;

typedef TRIGGER_STATUS FAR * LPTRIGGER_STATUS;


// Types of configuration data available.  Not all types available on all platforms.
enum tagCONFIG_TYPE
{
	CONFIG_TYPE_SCANNER = 0,
	CONFIG_TYPE_DISPLAY,
	CONFIG_TYPE_KEYBOARD,
	CONFIG_TYPE_UART,
	CONFIG_TYPE_TERMINAL,
};


// The following structure is used to request configuration data
typedef struct tagCONFIGURATION_DATA
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	
	DWORD		dwConfigType;		// The type of configuration data requested
									//	See tagCONFIG_TYPE above
	DWORD		dwConfigData;		// The configuration data

} CONFIGURATION_DATA;

typedef CONFIGURATION_DATA FAR * LPCONFIGURATION_DATA;


// Version information structure
typedef struct tagRCM_VERSION_INFO
{
	STRUCT_INFO StructInfo;			// Information about structure extents
	
	// HIWORD = major, LOWORD = minor
	DWORD		dwCAPIVersion;	
	DWORD		dwResCoordVersion;	
	DWORD		dwUUIDVersion;	
	DWORD		dwTemperatureVersion;	

} RCM_VERSION_INFO;

typedef RCM_VERSION_INFO FAR * LPRCM_VERSION_INFO;


#ifdef __cplusplus
}
#endif

#endif	// #ifndef RCMDEF_H_

