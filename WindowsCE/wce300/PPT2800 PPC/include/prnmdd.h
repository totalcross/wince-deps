
//--------------------------------------------------------------------
// FILENAME:			PRNMDD.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Symbol printer MDD driver
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _PRNMDD_H_
#define _PRNMDD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <winioctl.h>
#include <commdlg.h>
#include <tchar.h>
#include "prnioctl.h"

#define  SYMPNT_DEVCONTEXT_SIGNATURE    0x1357
#define  SYMPNT_OPENCONTEXT_SIGNATURE   0x2468

typedef struct tagSTART_DEVICE    {
//	STRUCT_INFO	StructInfo;
	LPCWSTR		lpszDriverKey;
	LPCWSTR		lpszDeviceName;
	DWORD		dwBaseIoctl;
} START_DEVICE, *LPSTART_DEVICE;


// Device context structure 
//
typedef struct _PRN_DEV_CONTEXT {
    int					Signature;
	CRITICAL_SECTION	ioctlCritSect;
    HINSTANCE			hPDD;		  	// Handle to PDD DLL
    HINSTANCE			hDLG;	  		// Handle to DLG DLL
    HINSTANCE			hTLD;		  	// Handle to TLD DLL
    struct __HWPDDOBJ	*pPddHWObj;	  	// Pointer to PDD HWOBJ
    struct __HWTLDOBJ	*pTldHWObj; 	// Pointer to TLD HWOBJ
    struct __HWDLGOBJ	*pDlgHWObj;	  	// Pointer to DLG HWOBJ
    TCHAR				PrinterDevName[100];
    TCHAR				devkeypath[50];
	HANDLE 				hPort;
} PRN_DEV_CONTEXT, *PPRN_DEV_CONTEXT;


// UI HWOBJ and VTBL
//
typedef struct __PNT_DLG_VTBL    {
    DWORD	(*DLGPageSetupDlg)(PPRN_DEV_CONTEXT, LPPAGESETUPDLG);
} PNT_DLG_VTBL, *PPNT_DLG_VTBL;

typedef struct __HWDLGOBJ {
    PPNT_DLG_VTBL	pFuncTbl;
} HWDLGOBJ, *PHWDLGOBJ;


// TLD HWOBJ and VTBL
//
typedef struct __PNT_TLD_VTBL    {
	DWORD (*TLDConnect)(PPRN_DEV_CONTEXT);
	DWORD (*TLDDisconnect)(PPRN_DEV_CONTEXT);
	DWORD (*TLDWrite)(PPRN_DEV_CONTEXT,LPVOID,DWORD,LPDWORD);
	DWORD (*TLDRead)(PPRN_DEV_CONTEXT,LPVOID,DWORD,TCHAR,LPDWORD);
	DWORD (*TLDSER_GetStatus)(PPRN_DEV_CONTEXT,LPDWORD);
	DWORD (*TLDSER_GetVersion)(PPRN_DEV_CONTEXT,LPDWORD);
} PNT_TLD_VTBL, *PPNT_TLD_VTBL;

typedef struct __HWTLDOBJ {
    PPNT_TLD_VTBL	pFuncTbl;
} HWTLDOBJ, *PHWTLDOBJ;


// PDD HWOBJ and VTBL
//
typedef struct __PNT_PDD_VTBL    {
    DWORD	(*PNTInit)(PPRN_DEV_CONTEXT);
    DWORD	(*PNTDeinit)(PPRN_DEV_CONTEXT);
    DWORD	(*PNTOpen)(PPRN_DEV_CONTEXT);
    DWORD	(*PNTClose)(PPRN_DEV_CONTEXT);

    HDC     (*PNTCreateDC)(PPRN_DEV_CONTEXT,LPCTSTR,LPCTSTR,LPCTSTR,PDEVMODE);
    DWORD   (*PNTDeleteDC)(PPRN_DEV_CONTEXT,HDC);
    DWORD   (*PNTStartDoc)(PPRN_DEV_CONTEXT,HDC,DOCINFO *);
    DWORD   (*PNTEndDoc)(PPRN_DEV_CONTEXT,HDC);
    DWORD   (*PNTStartPage)(PPRN_DEV_CONTEXT,HDC);
    DWORD   (*PNTEndPage)(PPRN_DEV_CONTEXT,HDC);
    DWORD   (*PNTAbortDoc)(PPRN_DEV_CONTEXT,HDC);
    DWORD   (*PNTSetAbortProc)(PPRN_DEV_CONTEXT,HDC,ABORTPROC);
	int		(*PNTGetDeviceCaps)(PPRN_DEV_CONTEXT,HDC,int); 

    DWORD   (*PNTBarCode)(PPRN_DEV_CONTEXT,
						  HDC, int, UINT, DWORD, LPVOID, int, LPCTSTR, int,
						  LPRECT, PUCHAR, LPBCCONSTRAINT, LPCTSTR, LPRECT, UINT);
    DWORD   (*PNTDrawText)(PPRN_DEV_CONTEXT,HDC,LPCTSTR,int,LPRECT,UINT);
	DWORD	(*PNTPolyLine)(PPRN_DEV_CONTEXT,HDC,LPPOINT,int); 
	DWORD	(*PNTBitBlt)(PPRN_DEV_CONTEXT,HDC,int,int,int,int,HDC,int,int,DWORD);

#if 0 // Not implement
    DWORD   (*PNTExtTextOut)(PPRN_DEV_CONTEXT,HDC,int,int,UINT,LPRECT,LPCTSTR,UINT,LPINT); 
	DWORD	(*PNTRectangle)(PPRN_DEV_CONTEXT,HDC,int,int,int,int);
	DWORD	(*PNTDrawEdge)(PPRN_DEV_CONTEXT,HDC,LPRECT,UINT,UINT);
	DWORD	(*PNTEllipse)(PPRN_DEV_CONTEXT,HDC,int,int,int,int); 
	DWORD	(*PNTFillRect)(PPRN_DEV_CONTEXT,HDC,LPRECT,HBRUSH); 
	DWORD	(*PNTFillRgn)(PPRN_DEV_CONTEXT,HDC,HRGN,HBRUSH); 
	DWORD	(*PNTPolygon)(PPRN_DEV_CONTEXT,HDC,LPPOINT,int); 
	DWORD	(*PNTRoundRect)(PPRN_DEV_CONTEXT,HDC,int,int,int,int,int,int); 
	COLORREF (*PNTSetPixel)(PPRN_DEV_CONTEXT,HDC,int,int,COLORREF); 
	DWORD	(*PNTStretchBlt)(PPRN_DEV_CONTEXT,HDC,int,int,int,int,HDC,int,int,int,int,DWORD); 
	DWORD	(*PNTEnumFontFamilies)(PPRN_DEV_CONTEXT,HDC,LPCTSTR,FONTENUMPROC,LPARAM); 
#endif // Not implement

} PNT_PDD_VTBL, *PPNT_PDD_VTBL;

typedef struct __HWPDDOBJ {
    PPNT_PDD_VTBL	pFuncTbl;
} HWPDDOBJ, *PHWPDDOBJ;


//
// Open context structure for this device
//
typedef struct _PNT_OPEN_CONTEXT {
	PPRN_DEV_CONTEXT    pDeviceContext;
	PRN_DEV_CONTEXT     OpenDevContext;
} PNT_OPEN_CONTEXT, *PPNT_OPEN_CONTEXT;


//
// IOCTL definitions
//

// Internal Functions

#ifdef __cplusplus
}
#endif

#endif // _PRNMDD_H_


