
//--------------------------------------------------------------------
// FILENAME:			BARCODES.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Bar Code Type definitions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _BARCODES_H_
#define _BARCODES_H_

// Bar Code type definitions
// *** Note:***
// If you can the defintions below, make sure to update the correponding
// tables in COMTEC.H and MONARCH.H.
//
#define TYPE_UPCA				0x0
#define TYPE_UPCE				0x1
#define TYPE_UPCE0				0x1
#define TYPE_UPCE1				0x2
#define TYPE_CODABAR			0x3
#define TYPE_CODE39				0x4
#define TYPE_CODE93				0x5
#define TYPE_CODE128			0x6
#define TYPE_EAN8				0x7
#define TYPE_EAN13				0x8
#define TYPE_MSI				0x9
#define TYPE_I2OF5				0xA
#define TYPE_PDF417				0xB

#endif // _BARCODES_H_


