
//--------------------------------------------------------------------
// FILENAME:			ScanMacr.h
//
// Copyright(c) 1998 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Macros to assist use of scanner structures
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef SCANMACR_H_
#define SCANMACR_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Macros
//--------------------------------------------------------------------

// Scan Buffer Access Macros

#define SCNBUF_ALLOC(size) \
	((LPSCAN_BUFFER)LocalAlloc(LPTR,(sizeof(SCAN_BUFFER)+size)))

#define SCNBUF_GETDATA(ptr) \
	(((LPBYTE)ptr)+SCNBUF_GETOFFSET(ptr))

#define SCNBUF_GETSTAT(ptr) \
	( SI_FIELD_VALID(ptr,dwStatus) ? (ptr)->dwStatus : 0 )

#define SCNBUF_GETREQID(ptr) \
	( SI_FIELD_VALID(ptr,dwRequestID) ? (ptr)->dwRequestID : 0 )

#define SCNBUF_GETSRC(ptr) \
	( SI_FIELD_VALID(ptr,szSource) ? (ptr)->szSource : NULL )

#define SCNBUF_GETLBLTYP(ptr) \
	( SI_FIELD_VALID(ptr,dwLabelType) ? (ptr)->dwLabelType : LABELTYPE_UNKNOWN )

#define SCNBUF_GETTIMEOUT(ptr) \
	( SI_FIELD_VALID(ptr,dwTimeout) ? (ptr)->dwTimeout : 0 )

#define SCNBUF_GETSIZE(ptr) \
	( SI_FIELD_VALID(ptr,dwDataBuffSize) ? (ptr)->dwDataBuffSize : 0 )

#define SCNBUF_GETOFFSET(ptr) \
	( SI_FIELD_VALID(ptr,dwOffsetDataBuff) ? (ptr)->dwOffsetDataBuff : sizeof(SCAN_BUFFER) )

#define SCNBUF_ISTEXT(ptr) \
	( SI_FIELD_VALID(ptr,bText) ? (ptr)->bText : TRUE )

#define SCNBUF_GETLEN(ptr) \
	( SI_FIELD_VALID(ptr,dwDataLength) ? (ptr)->dwDataLength : 0 )


// Other Macros

#define SCAN_TRANS_RES(x) (x)


#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANMACR_H_
