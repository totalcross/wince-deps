
//--------------------------------------------------------------------
// FILENAME:			MONARCH.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Monarch Bar Code printer
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _MONARCH_H_
#define _MONARCH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "barcodes.h"

typedef struct __MONARCH_BARCODEINFO {
  // Refer to the Monarch Packet Reference Manual for details
  int		Density;	 // Density
  int		TextType;	 // Text appearance type
} MONARCH_BARCODEINFO,  *LPMONARCH_BARCODEINFO;

// Text appearance type definitions
#define TEXTTYPE_DEFAULT				0	// Default
#define TEXTTYPE_NOCHKDIG_NONUMSYS		1	// No check digit or Number system
#define TEXTTYPE_NOCHKDIG_NUMSYS		5	// Number system at buttom, No check digit
#define TEXTTYPE_CHECKDIG_NONUMSYS		6	// Check digit at buttom, No number system
#define TEXTTYPE_CHKDIG_NUMSYS			7	// Check digit and Number system at buttom
#define TEXTTYPE_NOTEXT					8	// No text, Barcode only

#ifdef __cplusplus
}
#endif

#endif // _MONARCH_H_			
								

