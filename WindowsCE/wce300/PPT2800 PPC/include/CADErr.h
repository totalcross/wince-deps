//--------------------------------------------------------------------
// FILENAME:			CADErr.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by Catch All DLL (CAD)
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef CADERR_H_
#define CADERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT			0x80000000	/* Use bit 31 to indicate errror	*/
#define	USER_BIT		   	0x20000000	/* Bit 29 means not Win32 error	*/

#define	CAD_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_CAD_SUCCESS					0

#define E_CAD_ERROR					CAD_ERROR(0x0001)

// A passed argument is out of range

#define	E_CAD_INVALIDARG			CAD_ERROR(0x0002)

#ifdef __cplusplus
}
#endif

#endif	// #ifndef CADERR_H_
