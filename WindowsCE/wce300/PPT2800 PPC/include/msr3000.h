
//--------------------------------------------------------------------
// FILENAME:			msr3000.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Definitions for MSR 3000
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef _MSR3000_H_
#define _MSR3000_H_

#include "cetypes.h"

#define MSR3000_OSEXTN _declspec(dllimport)

/******************************************************************************
*
*                                Return Codes
*                                 ------------
*
*Description: Funtions in MSR3000.dll return codes reflecting the status
*of the execution. 
*
*The return codes are typedefed to RET_STATUS. The format of the enumeration
*of the error code will of the format MSR_ERR_xxxx. Where xxxx corresponds to
*the execution status.
*
* The success will be returned as MSR3000_SUCCESS.
******************************************************************************/
#define SUCCESS 0x0

typedef enum ret_status
{
    MSR3000_SUCCESS				= SUCCESS		,
    MSR3000_ERR_GLOBAL			= 0x20000001	,
    MSR3000_ERR_PARAMETER		= 0x20000002	,
    MSR3000_ERR_NOTOPEN			= 0x20000003	,
    MSR3000_ERR_STILLOPEN		= 0x20000004	,
    MSR3000_ERR_MEMORY			= 0x20000005	,
    MSR3000_ERR_SIZE			= 0x20000006	,
    MSR3000_ERR_NAK        		= 0x20000007	,
    MSR3000_ERR_BAD_ANS			= 0x20000008	,
    MSR3000_ERR_TIMEOUT    		= 0x20000009	,
    MSR3000_ERR_ROM        		= 0x2000000a	,
    MSR3000_ERR_RAM				= 0x2000000b	,
    MSR3000_ERR_EEPROM			= 0x2000000c	,
    MSR3000_ERR_RES				= 0x2000000d	,
    MSR3000_ERR_CHECKSUM		= 0x2000000e	,
    MSR3000_ERR_BADREAD			= 0x2000000f	,
    MSR3000_ERR_NODATA     		= 0x20000010	,
    MSR3000_ERR_NULLPOINTER		= 0x20000011	,
	MSR3000_ERR_BUSY			= 0x20000012	,
	MSR3000_ERR_WAKEUP			= 0x20000013	,
    MSR3000_ERR_BUFFERED_MODE	= 0x20000014	,
    MSR3000_ERR_UNBUFFERED_MODE = 0x20000015	,
	MSR3000_ERR_PORTEVENT		= 0x20000016	,
    MSR3000_ERR_OPENFAILURE     = 0x20000017	,
    MSR3000_ERR_BATTERYLOW		= 0x20000018	,
    MSR3000_ERR_POWERDOWN       = 0x20000019	,
    MSR3000_ERR_OSERROR         = 0x2000001a	,
} MSR3000_RET_STATUS;

typedef UWORD RET_STATUS;

/******************************************************************************
                                  DLL GLOBALS
                                  -----------
******************************************************************************/


#define MAX_CARD_DATA   	400         /* max data in a card 				 */
#define MAX_COMMAND_LENGTH  223         /* The maximium legth of command     */
#define MAX_PRE_POST_SIZE 	10          /* max preamle or postamble size 	 */
#define MAX_AFLD_NUM        6           /* max added field number 			 */
#define MAX_AFLD_LEN        6           /* max added field length 			 */
#define MAX_SCMD_NUM        4           /* max edit send command number 	 */
#define MAX_SCMD_LEN        40          /* max length in data edit send 	 */
#define MAX_SCMD_CHAR       110         /* max chars in whole data edit send */
#define MAX_FFLD_NUM        16          /* max flexible field number 		 */
#define MAX_FFLD_LEN        20          /* max flexible field setting command*/
#define MAX_FFLD_CHAR       60          /* max chars in whole flex field set */
#define MAX_RES_CHAR_NUM    6           /* max reserved chars to define      */
#define MAX_TRACK_NUM       3           /* max number of tracks              */
#define TRACK_FORMAT_LEN    5           /* chars for track format            */



#define UNBUFFERED_MODE     			((UBYTE)0x30)
#define BUFFERED_MODE       			((UBYTE)0x31)

#define NO_LRC              			((UBYTE)0x30)
#define SEND_LRC            			((UBYTE)0x31)

#define CR_LF_MODE          			((UBYTE)0x30)
#define CR_MODE             			((UBYTE)0x31)
#define LF_MODE             			((UBYTE)0x32)
#define NO_TERM_MODE        			((UBYTE)0x33)

#define ANY_TRACK           			((UBYTE)0x30)
#define TRACK_1             			((UBYTE)0x31)
#define TRACK_2             			((UBYTE)0x32)
#define TRACK_1_2           			((UBYTE)0x33)
#define TRACK_3             			((UBYTE)0x34)
#define TRACK_1_3           			((UBYTE)0x35)
#define TRACK_2_3           			((UBYTE)0x36)
#define ALL_TRACKS          			((UBYTE)0x37)


#define GET_ALL_TRACKS      			((UBYTE)0x30)
#define GET_TRACK_1         			((UBYTE)0x31)
#define GET_TRACK_2         			((UBYTE)0x32)
#define GET_TRACK_3         			((UBYTE)0x33)


#define STANDARD_DECODE     			((UBYTE)0x30)
#define GENERIC_DECODE      			((UBYTE)0x31)
#define RAW_DECODE          			((UBYTE)0x32)

#define BITS_5              			((UBYTE)0x30)
#define BITS_7              			((UBYTE)0x31)

#define DISABLE_DATA_EDIT       		((UBYTE)0x30)
#define ENABLE_DATA_MATCH       		((UBYTE)0x31)
#define ENABLE_DATA_NOMATCH     		((UBYTE)0x33)

#define TRACK_ALL_FORMAT        		((UBYTE)0x30)
#define TRACK_1_FORMAT          		((UBYTE)0x31)
#define TRACK_2_FORMAT          		((UBYTE)0x32)
#define TRACK_3_FORMAT          		((UBYTE)0x33)



/******************************************************************************
                                 RESERVED CHAR
                                 -------------
******************************************************************************/
typedef struct ReservedChar
{
    UBYTE   Format;
    UBYTE   Sr_Bits;
    UBYTE   Sr_Ascii;
} MSR3000_RESERVED_CHAR;

/******************************************************************************
                                 TRACK FORMAT
                                 -------------
******************************************************************************/
typedef struct TrackFormat
{
	UBYTE	Track_Id;
	UBYTE	Format;
	UBYTE	Ss_Bits;
	UBYTE	Ss_Ascii;
	UBYTE	Es_Bits;
	UBYTE	Es_Ascii;
}MSR3000_TRACKFORMAT;

/******************************************************************************
                                  MSR_SETTING
                                  -----------

******************************************************************************/
typedef	struct	MSR_Setting 
{
	UBYTE		Buffer_mode;
	UBYTE		Terminator;
	UBYTE		Preamble[MAX_PRE_POST_SIZE+1];
	UBYTE		Postamble[MAX_PRE_POST_SIZE+1];
	UBYTE		Track_selection;
	UBYTE		Track_separator;
	UBYTE		LRC_setting;
	UBYTE		Data_edit_setting;
	UBYTE		Decoder_mode;
	UBYTE		Track_format[MAX_TRACK_NUM][TRACK_FORMAT_LEN];
	MSR3000_RESERVED_CHAR	Reserved_chars[MAX_RES_CHAR_NUM];
	UBYTE		Added_field[MAX_AFLD_NUM][MAX_AFLD_LEN+1];
	UBYTE		Send_cmd[MAX_SCMD_NUM][ MAX_SCMD_LEN];
	UBYTE		Flexible_field[MAX_FFLD_NUM][MAX_FFLD_LEN];
}MSR_Setting;



typedef MSR_Setting* 				MSR_Setting_PTR;
typedef UBYTE*       				MSR3000_CARDINFO_PTR;
typedef MSR3000_RESERVED_CHAR*  	MSR3000_RESERVED_CHAR_PTR;

/******************************************************************************
*                             Function Prototypes
*                             -------------------
*
******************************************************************************/
MSR3000_OSEXTN RET_STATUS 	Msr3000_Open(UBYTE_PTR,UBYTE_PTR,UBYTE_PTR,void *);
MSR3000_OSEXTN RET_STATUS 	Msr3000_Close(void *);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetDefault(void);
MSR3000_OSEXTN RET_STATUS	Msr3000_GetSetting(MSR_Setting_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SendSetting(MSR_Setting_PTR);
MSR3000_OSEXTN RET_STATUS	Msr3000_GetVersion(UBYTE_PTR,UBYTE_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_GetStatus(UBYTE_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SelfDiagnose(UBYTE_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetBufferMode(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_ArmToRead(void);
MSR3000_OSEXTN RET_STATUS 	Msr3000_ClearBuffer(void);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetTerminator(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetPreamble(UBYTE_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetPostamble(UBYTE_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetTrackSelection(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetTrackSeperator(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetLRC(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetDataEdit(UBYTE);

MSR3000_OSEXTN RET_STATUS 	Msr3000_SetAddedField(UBYTE [MAX_AFLD_NUM][MAX_AFLD_LEN+1]);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetDataEditSend(UBYTE [MAX_SCMD_NUM][MAX_SCMD_LEN]);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetFlexibleField(UBYTE [MAX_FFLD_NUM][MAX_FFLD_LEN]);
MSR3000_OSEXTN RET_STATUS 	Msr3000_GetDataBuffer(MSR3000_CARDINFO_PTR, UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_ReadMSRBuffer(MSR3000_CARDINFO_PTR, UHWORD);
MSR3000_OSEXTN RET_STATUS 	Msr3000_ReadMSRUnbuffer(MSR3000_CARDINFO_PTR,void *);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetDecoderMode(UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetTrackFormat(UBYTE,UBYTE,UBYTE,UBYTE,UBYTE,UBYTE);
MSR3000_OSEXTN RET_STATUS 	Msr3000_SetReservedChar(MSR3000_RESERVED_CHAR_PTR);
MSR3000_OSEXTN RET_STATUS 	Msr3000_GetOsError(void);

#endif /* of _MSR3000_H_ */

/*************************** End of MSR3000.h ************************/
