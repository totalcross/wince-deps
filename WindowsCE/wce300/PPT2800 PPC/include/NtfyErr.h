
//--------------------------------------------------------------------
// FILENAME:			NtfyErr.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by notify functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef NTFYERR_H_
#define NTFYERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT				0x80000000	// Use bit 31 to indicate errror
#define	USER_BIT				0x20000000	// Bit 29 means not Win32 error	

#define	NOTIFY_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_NTFY_SUCCESS					0

// No more items are available to be returned from NOTIFY_FindFirst/NOTIFY_FindNext

#define E_NTFY_NOMOREITEMS			NOTIFY_ERROR(0x0001)

// A required mutex could not be created

#define E_NTFY_CANTMAKEMUTEX		NOTIFY_ERROR(0x0002)

// The notify API is busy for that object

#define E_NTFY_BUSY					NOTIFY_ERROR(0x0003)

// A null pointer was passed as a required parameter

#define	E_NTFY_NULLPTR				NOTIFY_ERROR(0x0004)

// An attempt to allocate memory failed

#define	E_NTFY_NOTENOUGHMEMORY		NOTIFY_ERROR(0x0005)

// An attempt to access a non-existent index

#define	E_NTFY_BADINDEX				NOTIFY_ERROR(0x0006)

// StructInfo error

#define	E_NTFY_STRUCTINFO			NOTIFY_ERROR(0x0007)

#define	E_NTFY_MISSINGFIELD			NOTIFY_ERROR(0x0008)

#ifdef __cplusplus
}
#endif

#endif	// #ifndef NTFYERR_H_
