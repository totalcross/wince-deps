
//--------------------------------------------------------------------
// FILENAME:			cetypes.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Windows CE types header file
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _CETYPES_H_
#define _CETYPES_H_

#ifdef ANSI                 						/* ANSI COMPILER */

typedef	_int8							UBYTE	;	/* unsigned byte 		  */
typedef _int8							VUBYTE  ;   /* volatile unsigned byte */
typedef _int8							BYTE    ;   /* signed byte 			  */
typedef _int8							VBYTE   ;   /* volatile signed byte   */

typedef _int16							UHWORD	;   /* unsigned short int     */
typedef _int16							VUHWORD ;   /* volatile unsigned shortint */
typedef _int16							HWORD   ;   /* signed short int       */
typedef _int16							VHWORD  ;   /* volatile signed short int */

typedef _int32							UWORD   ;   /* unsigned word          */
typedef _int32							VUWORD  ;   /* volatile unsigned word */
typedef _int32							WORD    ;   /* signed word           */
typedef _int32							VWORD   ;   /* volatile signed word */

#elif (( _WIN32 || _WIN32_WCE))

/* include windows.h only for WIN32 format */

#include <windows.h>
typedef unsigned char					UBYTE	;
typedef volatile unsigned char			VUBYTE  ;

typedef signed char						SBYTE   ;
typedef volatile signed char			VSBYTE  ;

typedef unsigned short int				UHWORD  ;
typedef volatile unsigned short int		VUHWORD ;
typedef signed short int				HSWORD  ;
typedef volatile signed short int		VSHWORD ;
typedef unsigned long					UWORD   ;
typedef volatile unsigned long			VUWORD  ;
typedef signed long						SWORD   ;
typedef volatile signed long			VSWORD  ;


#else

typedef unsigned char					UBYTE	;
typedef volatile unsigned char			VUBYTE  ;

typedef signed char						SBYTE   ;
typedef volatile signed char			VSBYTE  ;

typedef unsigned short int				UHWORD  ;
typedef volatile unsigned short int		VUHWORD ;
typedef short signed int				HSWORD  ;
typedef volatile signed short int		VSHWORD ;

typedef unsigned int					UWORD   ;
typedef volatile unsigned int			VUWORD  ;
typedef short int						SWORD   ;
typedef volatile short int				VSWORD  ;


#endif /* of ifdef ANSI */

typedef	UBYTE*							UBYTE_PTR	;
typedef	VUBYTE*							VUBYTE_PTR	;
typedef	SBYTE*							SBYTE_PTR  	;
typedef	VSBYTE*							VSBYTE_PTR	;
typedef	UHWORD*							UHWORD_PTR	;
typedef	VUHWORD*						VUHWORD_PTR	;
typedef	HSWORD*							HSWORD_PTR 	;
typedef	VSHWORD*						VSHWORD_PTR	;
typedef	UWORD*							UWORD_PTR  	;
typedef	VUWORD*							VUWORD_PTR 	;
typedef	SWORD*							SWORD_PTR  	;
typedef	VSWORD*							VSWORD_PTR 	;


#endif /* of _CETYPES_H_ */

/*************************** End of cetypes.h ************************/