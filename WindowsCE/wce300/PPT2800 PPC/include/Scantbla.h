
//--------------------------------------------------------------------
// FILENAME:			ScanTblA.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for scanner tables
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef _WIN32_WCE

#ifdef UNICODE
#undef UNICODE
#endif

#endif

#include <ScanTabl.h>
