//--------------------------------------------------------------------
// FILENAME:			PwrDef.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:	   Constants and types used by Power Management 
//							functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef PWRDEF_H_
#define PWRDEF_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested Includes
//--------------------------------------------------------------------

#include <StrucInf.h>


//--------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------

#define POWERAPI WINAPI

#define POWER_MAX_DEVICE_NAME   16
#define MAX_DLL_NAME        	16
#define MAX_FRIENDLY_NAME   	MAX_PATH
#define MAX_PDD		        	8
#define MAX_DEVICE_STATES   	5
#define MAX_NOTIFY_ORDER    	10

//
// Well-known power-managed devices.
//
#define POWER_SYSTEM_DEVICE     TEXT("SYSTEM")
#define POWER_BACKLIGHT_DEVICE	TEXT("BACKLIGHT")
#define POWER_COM1_DEVICE  	    TEXT("COM1")
#define POWER_COM4_DEVICE    	TEXT("COM4")
#define POWER_COM5_DEVICE    	TEXT("COM5")

//
// Broadcast notification message id.
//
#define UM_DEVICE_STATE_CHANGE  WM_USER+0x2000
#define UM_DEVICE_SUSPEND_PROHIBIT  WM_USER+0x2001


//--------------------------------------------------------------------
// Predefined Actitivies
//--------------------------------------------------------------------
#define ACTIVITY_COM1RING         0x00000001
#define ACTIVITY_COM4RING         0x00000002
#define ACTIVITY_PCMCIA0          0x00000004
#define ACTIVITY_PCMCIA1          0x00000008
#define ACTIVITY_COM5RING         0x00000010
#define ACTIVITY_ALARM            0x00000020
#define ACTIVITY_TOUCH            0x00000040
#define ACTIVITY_KEYBOARD         0x00000080
#define ACTIVITY_TRIGGER1         0x00000100
#define ACTIVITY_TRIGGER2         0x00000200
#define ACTIVITY_TRIGGER3	      0x00000400
#define ACTIVITY_BATTERYINSERT    0x00000800
#define ACTIVITY_CRADLEREMOVAL    0x00001000
#define ACTIVITY_CRADLEINSERT     0x00002000
#define ACTIVITY_ACCONNECT        0x00004000
#define ACTIVITY_POWERSWITCH      0x00008000
#define ACTIVITY_COM1DATA         0x00010000
#define ACTIVITY_COM4DATA         0x00020000
#define ACTIVITY_COM5DATA         0x00040000
#define ACTIVITY_BATTERYEJECT     0x00080000
#define ACTIVITY_SOFTWARE		  0x00100000
#define ACTIVITY_NETWORK		  0x00200000

//
// Device state change reasons.  
// Used in Device State Change Notify reports.
//

#define POWER_CHANGE_QUERY_ONLY  0x80000000
#define POWER_CHANGE_FORCING     0x40000000

#define POWER_CHANGE_MASK	 ~(POWER_CHANGE_QUERY_ONLY | POWER_CHANGE_FORCING)



//--------------------------------------------------------------------
// Enums
//--------------------------------------------------------------------
typedef enum tagPOWER_CHANGE_REASON
{
   POWER_CHANGE_UNKNOWN     = 0x00,
   POWER_CHANGE_INACTIVITY,
   POWER_CHANGE_ACTIVITY,
   POWER_CHANGE_REQUEST,
   POWER_CHANGE_POWER,
   POWER_CHANGE_CRITICAL_RESUME,
} POWER_CHANGE_REASON;

typedef enum tagPOWER_STATUS
{
    POWER_STATUS_INTERNAL   = 0x00, 
    POWER_STATUS_EXTERNAL   = 0x01, 
    POWER_STATUS_GOOD       = 0x02, 
    POWER_STATUS_LOW        = 0x04, 
    POWER_STATUS_VERY_LOW   = 0x08,
    POWER_STATUS_NO_BATTERY = 0x10
} POWER_STATUS;

typedef enum tagDEVICE_STATE
{
    DEVICE_STATE_HYPER   = 0x01,
    DEVICE_STATE_ACTIVE  = 0x02,
    DEVICE_STATE_DOZE    = 0x04,  
    DEVICE_STATE_SLEEP   = 0x08, 
    DEVICE_STATE_SUSPEND = 0x10
} DEVICE_STATE;


typedef enum tagBATTERY_TYPE
{
    PWR_BATTERY_NONE     = 0,
    PWR_BATTERY_ALKALINE = 1,
    PWR_BATTERY_NICAD    = 2,
    PWR_BATTERY_NIMH     = 3,
    PWR_BATTERY_LIION    = 4,
} BATTERY_TYPE;


//--------------------------------------------------------------------
// Typedefs
//--------------------------------------------------------------------

// Find information structure
//   Find information structure is filled in by C Wrapper by
//     enumerating and reading the registry keys under
//     ..\Drivers\Active\..  and ..\Drivers\BuiltIn\..

typedef struct tagPOWER_FINDINFO_W
{
	STRUCT_INFO	StructInfo;
	WCHAR szDeviceName[POWER_MAX_DEVICE_NAME];
	WCHAR szFriendlyName[MAX_PATH];
	WCHAR szPDDName[MAX_PATH];
    DWORD dwPDDVersion;
	DWORD dwSupportedStates;

} POWER_FINDINFO_W;

typedef POWER_FINDINFO_W FAR * LPPOWER_FINDINFO_W;

typedef struct tagPOWER_FINDINFO_A
{
	STRUCT_INFO	StructInfo;
	CHAR szDeviceName[POWER_MAX_DEVICE_NAME];
	CHAR szFriendlyName[MAX_PATH];
	CHAR szPDDName[MAX_PATH];
	DWORD dwPDDVersion;
	DWORD dwSupportedStates;

} POWER_FINDINFO_A;

typedef POWER_FINDINFO_A FAR * LPPOWER_FINDINFO_A;

#ifdef UNICODE
#define POWER_FINDINFO POWER_FINDINFO_W
#define LPPOWER_FINDINFO LPPOWER_FINDINFO_W
#else
#define POWER_FINDINFO POWER_FINDINFO_A
#define LPPOWER_FINDINFO LPPOWER_FINDINFO_A
#endif

//
// Version information structure
//   Version information for the PDD is cached by the MDD during its
//     initialization.  The MDD and C Wrapper add their version
//     information to the structure on demand
//
typedef struct tagPOWER_VERSION_INFO
	{
	 STRUCT_INFO 	StructInfo;
	 DWORD			dwMddVersion;	   	// HIWORD = major ver LOWORD = minor ver.
	 DWORD			dwCAPIVersion;		// HIWORD = major ver LOWORD = minor ver.
	}  POWER_VERSION_INFO;

typedef POWER_VERSION_INFO FAR * LPPOWER_VERSION_INFO;

//
// Due to how the STRUCT_INFO macros work, these fields cannot be nested
// within another structure.  Use a macro to keep the fields defined in
// one place.
//
#define _NOTIFYREQUEST \
    TCHAR szNotifyDeviceName[POWER_MAX_DEVICE_NAME];  \
    DWORD IOControlCode;                        \

//
// Used to supply Power Management driver with device name, IO Control Code ,
// and power events for which the caller will be notified.
//
//	 TCHAR szDeviceName[POWER_MAX_DEVICE_NAME];	 Recipient device driver name ie: COM1:
//											 PM Driver does Createfile on this name
//	 DWORD IOControlCode;					 PM Driver calls above driver's DeviceIOControl
//											 with this code 
typedef struct tagNotifyRequest 
{
    STRUCT_INFO StructInfo;                     
    _NOTIFYREQUEST
}
  NOTIFYREQUEST;

typedef  NOTIFYREQUEST FAR * LPNOTIFYREQUEST;

typedef struct tagPOWER_INFO
	{
	 STRUCT_INFO StructInfo;
	 DWORD		 dwBatteryType;			    // See Battery enum 
	 DWORD		 dwBatteryLevel;	   	    // See Power_State
	 DWORD		 dwPowerSource;			    // See Power_State
	} POWER_INFO;

typedef POWER_INFO FAR *  LPPOWER_INFO;


typedef struct tagDEVICE_STATE_CHANGE
	{
	 STRUCT_INFO StructInfo;
	 TCHAR szDeviceName[POWER_MAX_DEVICE_NAME];	// Name of device whose state is changing
	 DWORD dwOldState;						// Old state of the device
	 DWORD dwNewState;						// New (current) state of the device
     DWORD dwReason;
     DWORD dwActivity;		                // If reason = activity, this is the 
                                            // activity mask.
	} DEVICE_STATE_CHANGE;

typedef  DEVICE_STATE_CHANGE FAR * LPDEVICE_STATE_CHANGE;

#ifdef UNICODE
#define PWRFNDFRST		TEXT("POWER_FindFirst_W")
#else
#define PWRFNDFRST		TEXT("POWER_FindFirst_A")
#endif

#ifdef UNICODE
#define PWRFNDNXT		TEXT("POWER_FindNext_W")
#else
#define PWRFNDNXT		TEXT("POWER_FindNext_A")
#endif
#define PWR_FNDCLS			TEXT("POWER_FindClose")
#define PWR_OPEN			TEXT("POWER_Open")
#define PWR_CLOSE			TEXT("POWER_Close")
#define PWR_SETDEVNOTIFY	TEXT("POWER_SetDeviceNotify")
#define PWR_GETDEVNOTIFY	TEXT("POWER_GetDeviceNotify")
#define PWR_SETDEVSTATE		TEXT("POWER_SetDeviceState")
#define PWR_GETDEVSTATE		TEXT("POWER_GetDeviceState")
#define PWR_ALLWDEVSTATE	TEXT("POWER_AllowDeviceState")
#define PWR_PROHBTDEVSTATE	TEXT("POWER_ProhibitDeviceState")
#define PWR_CHKDEVSTATE		TEXT("POWER_CheckDeviceState")
#define PWR_SETTMRVAL		TEXT("POWER_SetTimerValue")
#define PWR_GETTMRVAL		TEXT("POWER_GetTimerValue")
#define PWR_SETDEVMSK		TEXT("POWER_SetDeviceMask")
#define PWR_GETDEVMSK		TEXT("POWER_GetDeviceMask")
#define PWR_SETDEVACT		TEXT("POWER_SetDeviceActivity")
#define PWR_GETDEVACT		TEXT("POWER_GetDeviceActivity")
#define PWR_GETVER			TEXT("POWER_GetVersion")
#define PWR_GETINF			TEXT("POWER_GetPowerInfo")

#ifdef UNICODE
typedef DWORD (POWERAPI* LPFNPWRFNDFRST)(LPPOWER_FINDINFO_W lpPwrFindInfo, LPHANDLE lphFindHandle);				
#else
typedef DWORD (POWERAPI* LPFNPWRFNDFRST)(LPPOWER_FINDINFO_A lpPwrFindInfo, LPHANDLE lphFindHandle);				
#endif

#ifdef UNICODE
typedef DWORD (POWERAPI* LPFNPWRFNDNXT)(LPPOWER_FINDINFO_W lpPwrFindInfo, HANDLE hFindHandle);				
#else
typedef DWORD (POWERAPI* LPFNPWRFNDNXT)(LPPOWER_FINDINFO_A lpPwrFindInfo, HANDLE hFindHandle);			   	
#endif

typedef DWORD (POWERAPI* LPFNPWR_FNDCLS)(HANDLE hFindHandle);

typedef DWORD (POWERAPI* LPFNPWR_OPEN)(void);

typedef DWORD (POWERAPI* LPFNPWR_GETVER)(LPPOWER_VERSION_INFO lpPowerVersionInfo);	

typedef DWORD (POWERAPI* LPFNPWR_CLOSE)(void);

typedef DWORD (POWERAPI* LPFNPWR_SETDEVNOTIFY)(LPNOTIFYREQUEST lpNotifyRequest, LPCTSTR lpszDeviceName, DWORD  dwNotifyMask);            

typedef DWORD (POWERAPI* LPFNPWR_GETDEVNOTIFY)(LPNOTIFYREQUEST lpNotifyRequest, LPCTSTR lpszDeviceName, LPDWORD lpdwNotifyMask);          

typedef DWORD (POWERAPI* LPFNPWR_SETDEVSTATE)(LPCTSTR lpszDeviceName, DWORD dwDeviceState);            

typedef DWORD (POWERAPI* LPFNPWR_GETDEVSTATE)(LPCTSTR lpszDeviceName, LPDWORD lpdwDeviceState);       

typedef DWORD (POWERAPI* LPFNPWR_ALLWDEVSTATE)(LPCTSTR lpszDeviceName, DWORD dwDeviceState);          

typedef DWORD (POWERAPI* LPFNPWR_PROHBTDEVSTATE)(LPCTSTR lpszDeviceName, DWORD dwDeviceState);         

typedef DWORD (POWERAPI* LPFNPWR_CHKDEVSTATE)(LPCTSTR lpszDeviceName, DWORD dwDeviceState, LPDWORD  lpdwAllowCnt);       

typedef DWORD (POWERAPI* LPFNPWR_SETTMRVAL)(LPCTSTR lpszDeviceName, DWORD dwDeviceState, DWORD dwTimerVal );           

typedef DWORD (POWERAPI* LPFNPWR_GETTMRVAL)(LPTSTR lpszDeviceName, DWORD dwDeviceState, LPDWORD lpdwTimerVal );       

typedef DWORD (POWERAPI* LPFNPWR_SETDEVMSK)(LPCTSTR lpszDeviceName, DWORD dwDeviceState, DWORD dwDeviceMask );

typedef DWORD (POWERAPI* LPFNPWR_GETDEVMSK)(LPCTSTR lpszDeviceName, DWORD dwDeviceState, LPDWORD lpdwDeviceMask );

typedef DWORD (POWERAPI* LPFNPWR_SETDEVACT)(LPCTSTR lpszDeviceName, DWORD dwActivityMask );       

typedef DWORD (POWERAPI* LPFNPWR_GETDEVACT)(LPCTSTR lpszDeviceName, LPDWORD lpdwActivityMask );   

typedef DWORD (POWERAPI* LPFNPWR_GETPWRINF)(LPPOWER_INFO lpPowerInfo);    


#ifdef __cplusplus
}
#endif

#endif	// #ifndef PWRDEF_H_
