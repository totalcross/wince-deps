
//--------------------------------------------------------------------
// FILENAME:			ScanErr.h
//
// Copyright(c) 1998 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by scanner functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef SCANERR_H_
#define SCANERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT				0x80000000	// Use bit 31 to indicate errror
#define	USER_BIT				0x20000000	// Bit 29 means not Win32 error	

#define	SCAN_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_SCN_SUCCESS				0


// The enumerated key assigned to the driver under

#define	E_SCN_OPENINGACTIVEKEY		SCAN_ERROR(0x0001)


// The enumerated key assigned to the driver under

#define	E_SCN_READINGACTIVEKEY		SCAN_ERROR(0x0002)


// The registry key containing the driver's

#define	E_SCN_OPENINGPARAMKEY		SCAN_ERROR(0x0003)


// The registry key containing the driver's

#define	E_SCN_READINGPARAMKEY		SCAN_ERROR(0x0004)


// An attempt to allocate memory failed

#define	E_SCN_NOTENOUGHMEMORY		SCAN_ERROR(0x0005)


// The device context ID passed to SCN_Open or
//  SCN_Deinit is invalid

#define	E_SCN_INVALIDDVCCONTEXT		SCAN_ERROR(0x0006)


// The open context ID passed to SCN_Close,
// 	SCN_IOControl, SCN_Read, SCN_Seek or
//	SCN_Write is invalid

#define	E_SCN_INVALIDOPENCONTEXT	SCAN_ERROR(0x0007)


// SCN_Open or SCN_Deint was called before a
//  successful init

#define	E_SCN_NOTINITIALIZED		SCAN_ERROR(0x0008)


// The PDD DLL could not be loaded

#define	E_SCN_CANTLOADDEVICE		SCAN_ERROR(0x0009)


// The PDD DLL did not contain the required entry points

#define	E_SCN_INVALIDDEVICE			SCAN_ERROR(0x000A)


// Required device is not present, already in use
//  or not functioning properly

#define E_SCN_DEVICEFAILURE			SCAN_ERROR(0x000B)


// The device could not be started

#define	E_SCN_CANTSTARTDEVICE		SCAN_ERROR(0x000C)


// The default parameters could not be
//	obtained from the PDD

#define	E_SCN_CANTGETDEFAULTS		SCAN_ERROR(0x000D)


// An attempt was made to use or stop the
//  scanner device and it was not started

#define	E_SCN_NOTSTARTED			SCAN_ERROR(0x000E)


// An attempt was made to start the device
//  when the device was already started

#define	E_SCN_ALREADYSTARTED		SCAN_ERROR(0x000F)


// An attempt was made to access the
//  scanner device and it was not enabled

#define	E_SCN_NOTENABLED			SCAN_ERROR(0x0010)


// An attempt was made to enable scanning
//  when scanning was already enabled

#define	E_SCN_ALREADYENABLED		SCAN_ERROR(0x0011)


// The control code passed to SCN_IOControl is invalid

#define	E_SCN_INVALIDIOCTRL			SCAN_ERROR(0x0012)


// A NULL pointer was passed for a required argument

#define	E_SCN_NULLPTR				SCAN_ERROR(0x0013)


// A passed argument is out of range

#define	E_SCN_INVALIDARG			SCAN_ERROR(0x0014)


// The size of the buffer passed as an input to
//  IOControl is less than sizeof(STRUCT_INFO) or
//  is less than the size specified in
//  StructInfo.dwUsed

#define	E_SCN_BUFFERSIZEIN			SCAN_ERROR(0x0015)


// The size of the buffer passed as an output to
//  IOControl is less than sizeof(STRUCT_INFO) or
//  is less than the size specified in
//  StructInfo.dwAllocated

#define	E_SCN_BUFFERSIZEOUT			SCAN_ERROR(0x0016)


// A STRUCT_INFO structure field is invalid. Either
//  dwAllocated or dwUsed is less than the size of
//  STRUCT_INFO or dwUsed is greater than dwAllocated

#define	E_SCN_STRUCTSIZE			SCAN_ERROR(0x0017)


// The size of a structure specified in a StructInfo
//  is too small to contain a required field

#define	E_SCN_MISSINGFIELD			SCAN_ERROR(0x0018)


// An invalid handle was passed to a function

#define	E_SCN_INVALIDHANDLE			SCAN_ERROR(0x0019)


// The value of a parameter either passed as an
//  argument to a function or as a member of a
//  structure is out of range or conflicts with
//  other parameters

#define	E_SCN_INVALIDPARAM			SCAN_ERROR(0x001A)


// Unable to create a required event

#define	E_SCN_CREATEEVENT			SCAN_ERROR(0x001B)


// Unable to create a required thread

#define	E_SCN_CREATETHREAD			SCAN_ERROR(0x001C)


// A read request was cancelled

#define	E_SCN_READCANCELLED			SCAN_ERROR(0x001D)


// A read request timed out

#define	E_SCN_READTIMEOUT			SCAN_ERROR(0x001E)


// Attempt to cancel a read that is not pending

#define	E_SCN_READNOTPENDING		SCAN_ERROR(0x001F)


// Attempt to start a read when one is pending

#define	E_SCN_READPENDING			SCAN_ERROR(0x0020)


// Buffer is too small for incoming data

#define	E_SCN_BUFFERTOOSMALL		SCAN_ERROR(0x0021)


// Attempt to access fields of an invalid scan buffer

#define E_SCN_INVALIDSCANBUFFER		SCAN_ERROR(0x0022)


// Attempt to submit a read that is incompatible with reads already queued

#define E_SCN_READINCOMPATIBLE		SCAN_ERROR(0x0023)


// Attempt to perform PDD feedback with no feedback capabilities

#define E_SCN_NOFEEDBACK			SCAN_ERROR(0x0024)


// MDD needs to restart read due to a parameter change
//   Note: this should never be returned to the application

#define E_SCN_RESTART				SCAN_ERROR(0x0025)


// Version of function not supported (e.g. ANSI vs. UNICODE)

#define E_SCN_NOTSUPPORTED			SCAN_ERROR(0x0026)


// The requested operation is inconsitent with the current state of the device

#define E_SCN_WRONGSTATE			SCAN_ERROR(0x0027)


// No more items are available to be returned from SCAN_FindFirst/SCAN_FindNext

#define E_SCN_NOMOREITEMS			SCAN_ERROR(0x0028)


// A required registry key could not be opened

#define E_SCN_CANTOPENREGKEY		SCAN_ERROR(0x0029)


// A required registry value could not be read

#define E_SCN_CANTREADREGVAL		SCAN_ERROR(0x002A)


// A exception occurred while trying to call the scanner driver

#define E_SCN_EXCEPTION				SCAN_ERROR(0x002B)


// A scanner API function failed with a non-scanner API error
// code, call GetLastError to get the actual Win32 error code

#define E_SCN_WIN32ERROR			SCAN_ERROR(0x002C)

// Requested scanner resource is already in use

#define E_SCN_ALREADYINUSE			SCAN_ERROR(0x002D)

// Specified scanner resource was not allocated

#define E_SCN_NOTINUSE				SCAN_ERROR(0x002E)

#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANERR_H_
