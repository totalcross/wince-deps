
//--------------------------------------------------------------------
// FILENAME:			DispCApi.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Main include file for Display C API
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef DISPCAPI_H_

#define DISPCAPI_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------


#include "DispErr.h"
#include "strucinf.h"


#define DISPLAYAPI WINAPI

// Version information structure
typedef struct tagDISPLAY_VERSION_INFO
{
	STRUCT_INFO 	StructInfo;

	DWORD			dwCAPIVersion;	// HIWORD = major ver LOWORD = minor ver.

} DISPLAY_VERSION_INFO;

typedef DISPLAY_VERSION_INFO FAR * LPDISPLAY_VERSION_INFO;

#define BACKLIGHT_ON	1
#define BACKLIGHT_OFF	0

//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------


// Get number of contrast levels
DWORD DISPLAYAPI DISPLAY_GetContrastLevels(LPDWORD lpdwContrastLevels);


// Get current contrast
DWORD DISPLAYAPI DISPLAY_GetContrast(LPDWORD lpdwContrast);


// Set current contrast
DWORD DISPLAYAPI DISPLAY_SetContrast(DWORD dwContrast);


// Get number of backlight intensity levels
DWORD DISPLAYAPI DISPLAY_GetBacklightIntensityLevels(LPDWORD lpdwBacklightIntensityLevels);


// Get current backlight intensity
DWORD DISPLAYAPI DISPLAY_GetBacklightIntensity(LPDWORD lpdwBacklightIntensity);


// Set current backlight intensity
DWORD DISPLAYAPI DISPLAY_SetBacklightIntensity(DWORD dwBacklightIntensity);

// Get current backlight state
DWORD DISPLAYAPI DISPLAY_GetBacklightState(LPDWORD lpdwBacklightState);


// Set current backlight state -- Turn on/off
DWORD DISPLAYAPI DISPLAY_SetBacklightState(DWORD dwBacklightState);

// Get the display driver Version info
DWORD DISPLAYAPI DISPLAY_GetVersion(LPDISPLAY_VERSION_INFO lpDisplayVersionInfo);


#ifdef __cplusplus
}
#endif

#endif	// #ifndef DISPCAPI_H_
