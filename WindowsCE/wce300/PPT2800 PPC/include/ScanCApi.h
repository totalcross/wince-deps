
//--------------------------------------------------------------------
// FILENAME:			ScanCApi.h
//
// Copyright(c) 1998 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Main include file for scanner C API
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef SCANCAPI_H_

#define SCANCAPI_H_

#ifdef __cplusplus
extern "C"
{
#endif


//--------------------------------------------------------------------
// Nested includes
//--------------------------------------------------------------------


#include <ScanErr.h>
#include <ScanDef.h>
#include <ScanMacr.h>


//--------------------------------------------------------------------
// Exported Function Prototypes
//--------------------------------------------------------------------


// Allocate a scan buffer
LPSCAN_BUFFER_W SCANAPI SCAN_AllocateBuffer_W(
	BOOL bText,
	DWORD dwSize);

LPSCAN_BUFFER_A SCANAPI SCAN_AllocateBuffer_A(
	BOOL bText,
	DWORD dwSize);


#ifdef UNICODE
#define SCAN_AllocateBuffer SCAN_AllocateBuffer_W
#else
#define SCAN_AllocateBuffer SCAN_AllocateBuffer_A
#endif


// Deallocate a scan buffer
DWORD SCANAPI SCAN_DeallocateBuffer_W(
	LPSCAN_BUFFER_W lpScanBuffer);

DWORD SCANAPI SCAN_DeallocateBuffer_A(
	LPSCAN_BUFFER_A lpScanBuffer);


#ifdef UNICODE
#define SCAN_DeallocateBuffer SCAN_DeallocateBuffer_W
#else
#define SCAN_DeallocateBuffer SCAN_DeallocateBuffer_A
#endif


// Find first available scanner device
DWORD SCANAPI SCAN_FindFirst_W(
	LPSCAN_FINDINFO_W lpScanFindInfo,	// Struct to fill in
	LPHANDLE lphFindHandle);			// Handle for subsequent finds

DWORD SCANAPI SCAN_FindFirst_A(
	LPSCAN_FINDINFO_A lpScanFindInfo,	// Struct to fill in
	LPHANDLE lphFindHandle);			// Handle for subsequent finds

#ifdef UNICODE
#define SCAN_FindFirst SCAN_FindFirst_W
#else
#define SCAN_FindFirst SCAN_FindFirst_A
#endif


// Find next available scanner device
DWORD SCANAPI SCAN_FindNext_W(
	LPSCAN_FINDINFO_W lpScanFindInfo,	// Struct to fill in
	HANDLE hFindHandle);				// Handle returned by find first

DWORD SCANAPI SCAN_FindNext_A(
	LPSCAN_FINDINFO_A lpScanFindInfo,	// Struct to fill in
	HANDLE hFindHandle);				// Handle returned by find first

#ifdef UNICODE
#define SCAN_FindNext SCAN_FindNext_W
#else
#define SCAN_FindNext SCAN_FindNext_A
#endif


// Close the handle used by SCAN_FindFirst and SCAN_FindNext
DWORD SCANAPI SCAN_FindClose(HANDLE hFindHandle);


// Open the specified scanner device
DWORD SCANAPI SCAN_Open(
	LPCTSTR lpszDeviceName,					// Name of scanner to open
	LPHANDLE lphScanner);					// Handle for subsequent operations


// Get version information about an open scanner
DWORD SCANAPI SCAN_GetVersionInfo(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_VERSION_INFO lpScanVersionInfo);	// Ptr to struct to receive data


// Close an open scanner
DWORD SCANAPI SCAN_Close(
	HANDLE hScanner);						// Handle of open scanner


// Enable scanning operations on an open scanner
DWORD SCANAPI SCAN_Enable(
	HANDLE hScanner);						// Handle of open scanner


// Disable scanning operations on an open scanner
DWORD SCANAPI SCAN_Disable(
	HANDLE hScanner);						// Handle of open scanner


// Submit a read on an open scanner handle which uses event notification when done
DWORD SCANAPI SCAN_ReadLabelEvent_W(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_W lpScanBuffer,			// Ptr to scan buffer
	HANDLE hEvent,							// Event to signal when done
	DWORD dwTimeout,						// Timeout, written into scan buffer
	LPDWORD lpdwRequestID);					// Unique id

DWORD SCANAPI SCAN_ReadLabelEvent_A(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_A lpScanBuffer,			// Ptr to scan buffer
	HANDLE hEvent,							// Event to signal when done
	DWORD dwTimeout,						// Timeout, written into scan buffer
	LPDWORD lpdwRequestID);					// Unique id

#ifdef UNICODE
#define SCAN_ReadLabelEvent SCAN_ReadLabelEvent_W
#else
#define SCAN_ReadLabelEvent SCAN_ReadLabelEvent_A
#endif


// Submit a read on an open scanner handle which uses message notification when done
DWORD SCANAPI SCAN_ReadLabelMsg_W(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_W lpScanBuffer,			// Ptr to scan buffer, becomes lParam
	HWND hWnd,								// Window to send msg to when done
	UINT uiMsgNo,							// Msg to be sent when done
	DWORD dwTimeout,						// Timeout, written into scan buffer
	LPDWORD lpdwRequestID);					// Unique id, becomes wParam

DWORD SCANAPI SCAN_ReadLabelMsg_A(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_A lpScanBuffer,			// Ptr to scan buffer, becomes lParam
	HWND hWnd,								// Window to send msg to when done
	UINT uiMsgNo,							// Msg to be sent when done
	DWORD dwTimeout,						// Timeout, written into scan buffer
	LPDWORD lpdwRequestID);					// Unique id, becomes wParam

#ifdef UNICODE
#define SCAN_ReadLabelMsg SCAN_ReadLabelMsg_W
#else
#define SCAN_ReadLabelMsg SCAN_ReadLabelMsg_A
#endif


// Submit a read on an open scanner handle which waits until done
DWORD SCANAPI SCAN_ReadLabelWait_W(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_W lpScanBuffer,			// Ptr to scan buffer, becomes lParam
	DWORD dwTimeout);						// Timeout, written into scan buffer

DWORD SCANAPI SCAN_ReadLabelWait_A(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_BUFFER_A lpScanBuffer,			// Ptr to scan buffer, becomes lParam
	DWORD dwTimeout);						// Timeout, written into scan buffer

#ifdef UNICODE
#define SCAN_ReadLabelWait SCAN_ReadLabelWait_W
#else
#define SCAN_ReadLabelWait SCAN_ReadLabelWait_A
#endif


// Cancel a previously submitted, but not completed, read on an open scanner handle
DWORD SCANAPI SCAN_CancelRead(
	HANDLE hScanner,						// Handle of open scanner
	DWORD dwRequestID);						// Unique id returned by read


// Cancel all reads submitted on an open scanner handle
DWORD SCANAPI SCAN_Flush(
	HANDLE hScanner);						// Handle of open scanner


// Get soft trigger flag for this open scanner handle
DWORD SCANAPI SCAN_GetSoftTrigger(
	HANDLE hScanner,							// Handle of open scanner
	LPBOOL lpSoftTrigger);					// Ptr to flag to receive data


// Set soft trigger flag for this open scanner handle
DWORD SCANAPI SCAN_SetSoftTrigger(
	HANDLE hScanner,							// Handle of open scanner
	LPBOOL lpSoftTrigger);					// Ptr to flag containing new data


// Get device capabilities information for an open scanner handle
DWORD SCANAPI SCAN_GetDeviceInfo(
	HANDLE hScanner,						// Handle of open scanner	
	LPDEVICE_INFO lpDeviceInfo);			// Ptr to struct to receive data


// Get list of supported decoders for an open scanner handler
DWORD SCANAPI SCAN_GetSupportedDecoders(
	HANDLE hScanner,						// Handle of open scanner	
	LPDECODER_LIST lpDecoderList);			// Ptr to struct to receive data


// Get list of enabled decoders for an open scanner handler
DWORD SCANAPI SCAN_GetEnabledDecoders(
	HANDLE hScanner,						// Handle of open scanner
	LPDECODER_LIST lpDecoderList);			// Ptr to struct to receive data


// Set list of enabled decoders for an open scanner handler
DWORD SCANAPI SCAN_SetEnabledDecoders(
	HANDLE hScanner,						// Handle of open scanner
	LPDECODER_LIST lpDecoderList);			// Ptr to struct containing new data


// Get decoder parameters structure for an open scanner handler
DWORD SCANAPI SCAN_GetDecoderParams(
	HANDLE hScanner,						// Handle of open scanner
	LPDECODER lpDecoder,					// Decoder to get
	LPDECODER_PARAMS lpDecoderParams);		// Ptr to struct to receive data


// Set decoder parameters structure for an open scanner handler
DWORD SCANAPI SCAN_SetDecoderParams(
	HANDLE hScanner,						// Handle of open scanner
	LPDECODER lpDecoder,					// Decoder to set
	LPDECODER_PARAMS lpDecoderParams);		// Ptr to struct containing new data


// Get UPCEAN parameters for an open scanner handler
DWORD SCANAPI SCAN_GetUPCEANParams(
	HANDLE hScanner,						// Handle of open scanner
	LPUPC_EAN_PARAMS lpUPCEANParams);		// Ptr to struct to receive data


// Set UPCEAN parameters for an open scanner handler
DWORD SCANAPI SCAN_SetUPCEANParams(
	HANDLE hScanner,						// Handle of open scanner
	LPUPC_EAN_PARAMS lpUPCEANParams);		// Ptr to struct containing new data


// Get reader parameters for an open scanner handler
DWORD SCANAPI SCAN_GetReaderParams(
	HANDLE hScanner,						// Handle of open scanner
	LPREADER_PARAMS lpReaderParams);		// Ptr to struct to receive data


// Set reader parameters for an open scanner handler
DWORD SCANAPI SCAN_SetReaderParams(
	HANDLE hScanner,						// Handle of open scanner
	LPREADER_PARAMS lpReaderParams);		// Ptr to struct containing new data


// Get interface parameters for an open scanner handler
DWORD SCANAPI SCAN_GetInterfaceParams(
	HANDLE hScanner,						// Handle of open scanner
	LPINTERFACE_PARAMS lpInterfaceParams);	// Ptr to struct to receive data


// Set interface parameters for an open scanner handler
DWORD SCANAPI SCAN_SetInterfaceParams(
	HANDLE hScanner,						// Handle of open scanner
	LPINTERFACE_PARAMS lpInterfaceParams);	// Ptr to struct containing new data


// Get scan parameters for an open scanner handler
DWORD SCANAPI SCAN_GetScanParameters_W(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_PARAMS_W lpScanParams);			// Ptr to struct to receive data

DWORD SCANAPI SCAN_GetScanParameters_A(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_PARAMS_A lpScanParams);			// Ptr to struct to receive data

#ifdef UNICODE
#define SCAN_GetScanParameters SCAN_GetScanParameters_W
#else
#define SCAN_GetScanParameters SCAN_GetScanParameters_A
#endif


// Set scan parameters for an open scanner handler
DWORD SCANAPI SCAN_SetScanParameters_W(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_PARAMS_W lpScanParams);			// Ptr to struct containing new data

DWORD SCANAPI SCAN_SetScanParameters_A(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_PARAMS_A lpScanParams);			// Ptr to struct containing new data

#ifdef UNICODE
#define SCAN_SetScanParameters SCAN_SetScanParameters_W
#else
#define SCAN_SetScanParameters SCAN_SetScanParameters_A
#endif


// Get scanner status for an open scanner handler
DWORD SCANAPI SCAN_GetScanStatus(
	HANDLE hScanner,						// Handle of open scanner
	LPSCAN_STATUS lpScanStatus);			// Ptr to struct to receive data


// Register for scanner message notification
//   wParam - SCAN_EVENT_CODE
//   lParam - code specific
DWORD SCANAPI SCAN_RegisterScanMessage(
	HANDLE hScanner,				// Handle of open scanner
	HWND hWnd,						// Window handle to which message is posted
	UINT uiMessage);				// Message ID to be posted to notify of scan events


// Deregister scanner message notification
DWORD SCANAPI SCAN_DeregisterScanMessage(
	HANDLE hScanner);				// Handle of open scanner
	

// Do remote feedback
DWORD SCANAPI SCAN_DoRemoteFeedback(
	HANDLE hScanner,				// Handle of open scanner
	LPFEEDBACK_PARAMS lpFeedbackParams);	// Ptr to struct containing params
	

// Generic IOCTL
DWORD SCANAPI SCAN_Ioctl(HANDLE hScanner,
						 DWORD dwIoctlCode,
						 LPVOID lpvInBuf,
						 DWORD dwInBufSize,
						 LPVOID lpvOutBuf,
						 DWORD dwOutBufSize,
						 LPDWORD lpdwActualOut);


#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANCAPI_H_
