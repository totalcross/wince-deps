
//--------------------------------------------------------------------
// FILENAME:			AudioErr.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by audio functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef AUDIOERR_H_
#define AUDIOERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT				0x80000000	// Use bit 31 to indicate errror
#define	USER_BIT				0x20000000	// Bit 29 means not Win32 error	

#define	AUDIO_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_AUD_SUCCESS           0
#define E_AUD_NULLPOINTER       AUDIO_ERROR(0x0001)
#define E_AUD_CREATEMUTEX       AUDIO_ERROR(0x0002)
#define E_AUD_BUSYMUTEX         AUDIO_ERROR(0x0003)
#define E_AUD_OUTOFRANGE        AUDIO_ERROR(0x0004)
#define E_AUD_IOCTRLFAILED      AUDIO_ERROR(0x0005)
#define E_AUD_NTFYFAILED        AUDIO_ERROR(0x0006)
#define E_AUD_NOTSUPPORTED      AUDIO_ERROR(0x0007)
#define E_AUD_STRUCTINFO        AUDIO_ERROR(0x0008)
#define E_AUD_MISSINGFIELD      AUDIO_ERROR(0x0009)


#ifdef __cplusplus
}
#endif

#endif	// #ifndef AUDIOERR_H_	
