
//--------------------------------------------------------------------
// FILENAME:			PRINTAPI.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for Symbol printer driver C 
//						wrapper functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _PRINTAPI_H_
#define _PRINTAPI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "strucinf.h"

// Bar Code flags definitions
#define BARCODE_FLIP_HORIZONTAL		0x1
#define BARCODE_FLIP_VERTICAL		0x2
#define BARCODE_SUPPRESS_LABEL_TEXT	0x4


// Version information structure
typedef struct tagPRINT_VERSION_INFO
{
	STRUCT_INFO 	StructInfo;

	DWORD			dwCAPIVersion;	// HIWORD = major ver LOWORD = minor ver.
	DWORD			dwPddVersion;	// HIWORD = major ver LOWORD = minor ver.
	DWORD			dwMddVersion;	// HIWORD = major ver LOWORD = minor ver.
	DWORD			dwTldVersion;	// HIWORD = major ver LOWORD = minor ver.

} PRINT_VERSION_INFO;

typedef PRINT_VERSION_INFO FAR * LPPRINT_VERSION_INFO;


#if defined(_WIN32_WCE) && (defined(_MIPS_) || defined(_ARM_)) && !defined(_WIN32_WCE_EMULATION)
// we knew that X86 and HPC platform support DOCINFO in WinGdi.h
// but not in MIPS, this #if defined block is copy from wingdi.h for x86
#define SP_ERROR                     (-1)

#ifdef STRICT
typedef BOOL (CALLBACK* ABORTPROC)(HDC, int);
#else
typedef FARPROC ABORTPROC;
#endif

typedef struct _DOCINFOA {
    int     cbSize;
    LPCSTR   lpszDocName;
    LPCSTR   lpszOutput;
    LPCSTR   lpszDatatype;
    DWORD    fwType;
} DOCINFOA, *LPDOCINFOA;

typedef struct _DOCINFOW {
    int     cbSize;
    LPCWSTR  lpszDocName;
    LPCWSTR  lpszOutput;
    LPCWSTR  lpszDatatype;
    DWORD    fwType;
} DOCINFOW, *LPDOCINFOW;

#ifdef UNICODE
typedef DOCINFOW DOCINFO;
typedef LPDOCINFOW LPDOCINFO;
#else
typedef DOCINFOA DOCINFO;
typedef LPDOCINFOA LPDOCINFO;
#endif // UNICODE

#endif

typedef struct __HDL_DC_TABLE    {
  HANDLE PntHdl;
  HDC    hDC;
} HDL_DC_TABLE, *PHDL_DC_TABLE;

typedef struct __BCCONSTRAINT {
  UINT		uMinX;
  UINT		uMinY;
  UINT		uQuietZoneX;
  UINT		uQuietZoneY;
  UINT		uMinGap;
  UINT		uMinRatio;
} BCCONSTRAINT,  *LPBCCONSTRAINT;


HDC  Print_CreateDC(LPCTSTR,LPCTSTR,LPCTSTR,PDEVMODE);
BOOL Print_DeleteDC(HDC);
int  Print_StartDoc(HDC,DOCINFO *);
int  Print_EndDoc(HDC);
int  Print_StartPage(HDC);
int  Print_EndPage(HDC);
int  Print_AbortDoc(HDC);
int  Print_GetDeviceCaps(HDC,int);
int  Print_BarCode(HDC,int,UINT,DWORD,LPVOID,int,LPCTSTR,
				   int,LPRECT,PUCHAR,LPBCCONSTRAINT,LPCTSTR,LPRECT,UINT);
int  Print_DrawText(HDC,LPCTSTR,int,LPRECT,UINT);
BOOL Print_PolyLine(HDC,LPPOINT,int); 
BOOL Print_BitBlt(HDC,int,int,int,int,HDC,int,int,DWORD); 
HANDLE Print_FindFirst(LPWSTR,LPDWORD);
BOOL Print_FindNext(HANDLE,LPWSTR,LPDWORD);
BOOL Print_FindClose(HANDLE);
int  Print_GetVersion(HANDLE, LPPRINT_VERSION_INFO); 

// marco define for the upper case 
#define PRINT_CreateDC		Print_CreateDC
#define PRINT_DeleteDC		Print_DeleteDC
#define	PRINT_StartDoc		Print_StartDoc
#define	PRINT_EndDoc		Print_EndDoc
#define	PRINT_StartPage		Print_StartPage
#define	PRINT_EndPage		Print_EndPage
#define	PRINT_AbortDoc		Print_AbortDoc
#define	PRINT_GetDeviceCaps	Print_GetDeviceCaps
#define	PRINT_BarCode		Print_BarCode
#define	PRINT_DrawText		Print_DrawText
#define	PRINT_PolyLine		Print_PolyLine
#define	PRINT_BitBlt		Print_BitBlt
#define	PRINT_FindFirst		Print_FindFirst
#define	PRINT_FindNext		Print_FindNext
#define	PRINT_FindClose		Print_FindClose
#define	PRINT_GetVersion	Print_GetVersion
// end marco definition

#if defined(_WIN32_WCE) && (defined(_MIPS_) || defined(_ARM_)) && !defined(_WIN32_WCE_EMULATION)

typedef UINT (APIENTRY* LPPAGEPAINTHOOK)( HWND, UINT, WPARAM, LPARAM );
typedef UINT (APIENTRY* LPPAGESETUPHOOK)( HWND, UINT, WPARAM, LPARAM );


// only wide types supported in CE - include for synchronization with nt .h
typedef struct tagPSDA
{
    DWORD           lStructSize;
    HWND            hwndOwner;
    HGLOBAL         hDevMode;
    HGLOBAL         hDevNames;
    DWORD           Flags;
    POINT           ptPaperSize;
    RECT            rtMinMargin;
    RECT            rtMargin;
    HINSTANCE       hInstance;
    LPARAM          lCustData;
    LPPAGESETUPHOOK lpfnPageSetupHook;
    LPPAGEPAINTHOOK lpfnPagePaintHook;
    LPCSTR          lpPageSetupTemplateName;
    HGLOBAL         hPageSetupTemplate;
} PAGESETUPDLGA, * LPPAGESETUPDLGA;
typedef struct tagPSDW
{
    DWORD           lStructSize;
    HWND            hwndOwner;
    HGLOBAL         hDevMode;
    HGLOBAL         hDevNames;
    DWORD           Flags;
    POINT           ptPaperSize; // ignored in CE
    RECT            rtMinMargin;
    RECT            rtMargin;
    HINSTANCE       hInstance;
    LPARAM          lCustData;
    LPPAGESETUPHOOK lpfnPageSetupHook;
    LPPAGEPAINTHOOK lpfnPagePaintHook; // ignored in CE
    LPCWSTR         lpPageSetupTemplateName;
    HGLOBAL         hPageSetupTemplate;
} PAGESETUPDLGW, * LPPAGESETUPDLGW;
#ifdef UNICODE
typedef PAGESETUPDLGW PAGESETUPDLG;
typedef LPPAGESETUPDLGW LPPAGESETUPDLG;
#else
typedef PAGESETUPDLGA PAGESETUPDLG;
typedef LPPAGESETUPDLGA LPPAGESETUPDLG;
#endif // UNICODE

BOOL APIENTRY PageSetupDlgA( LPPAGESETUPDLGA );
BOOL APIENTRY PageSetupDlgW( LPPAGESETUPDLGW );
#ifdef UNICODE
#define PageSetupDlg  PageSetupDlgW
#else
#define PageSetupDlg  PageSetupDlgA
#endif // !UNICODE

#define PSD_DEFAULTMINMARGINS             0x00000000 // default (printer's)
#define PSD_INWININIINTLMEASURE           0x00000000 // 1st of 4 possible

#define PSD_MINMARGINS                    0x00000001 // use caller's
#define PSD_MARGINS                       0x00000002 // use caller's
#define PSD_INTHOUSANDTHSOFINCHES         0x00000004 // 2nd of 4 possible
#define PSD_INHUNDREDTHSOFMILLIMETERS     0x00000008 // 3rd of 4 possible
#define PSD_DISABLEMARGINS                0x00000010
#define PSD_DISABLEPRINTER                0x00000020
// #define PSD_NOWARNING                  0x00000080 // not used in CE
#define PSD_DISABLEORIENTATION            0x00000100
#define PSD_DISABLEPAPER                  0x00000200
#define PSD_RETURNDEFAULT                 0x00000400
// #define PSD_SHOWHELP                   0x00000800 // not used in CE
#define PSD_ENABLEPAGESETUPHOOK           0x00002000
#define PSD_ENABLEPAGESETUPTEMPLATE       0x00008000
#define PSD_ENABLEPAGESETUPTEMPLATEHANDLE 0x00020000
// #define PSD_ENABLEPAGEPAINTHOOK        0x00040000 // not used in CE
// #define PSD_DISABLEPAGEPAINTING        0x00080000 // not used in CE
// #define PSD_NONETWORKBUTTON            0x00200000 // not used in CE

// new in Win CE - print range flags
#define PSD_DISABLEPRINTRANGE		      0x10000000
#define PSD_RANGESELECTION				  0x20000000
#endif

#ifdef __cplusplus
}
#endif

#endif // _PRINTAPI_H_


