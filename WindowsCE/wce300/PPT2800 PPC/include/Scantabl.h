
//--------------------------------------------------------------------
// FILENAME:			ScanTabl.h
//
// Copyright(c) 1998 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Header file for scanner tables
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef SCANTABL_H_
#define SCANTABL_H_

#ifdef __cplusplus
extern "C"
{
#endif

//--------------------------------------------------------------------
// Defines
//--------------------------------------------------------------------

//--------------------------------------------------------------------
// Types
//--------------------------------------------------------------------

typedef struct tagLABELTYPEINFO_A
{
	LABELTYPE	dwLabelType;
	LPDECODER	lpDecoder;
	LPSTR		lpszSymbology;
	CHAR		cSymbolId[3];
	CHAR		cAIMId;
} LABELTYPEINFO_A;

typedef LABELTYPEINFO_A FAR * LPLABELTYPEINFO_A;

typedef struct tagLABELTYPEINFO_W
{
	LABELTYPE	dwLabelType;
	LPDECODER	lpDecoder;
	LPWSTR		lpszSymbology;
	CHAR		cSymbolId[3];
	CHAR		cAIMId;
} LABELTYPEINFO_W;

typedef LABELTYPEINFO_W FAR * LPLABELTYPEINFO_W;

#ifdef UNICODE
#define LABELTYPEINFO LABELTYPEINFO_W
#define LPLABELTYPEINFO LPLABELTYPEINFO_W
#define LabelTypeInfoTable LabelTypeInfoTable_W
#define dwLabelTypeInfoCount dwLabelTypeInfoCount_W
#else
#define LABELTYPEINFO LABELTYPEINFO_A
#define LPLABELTYPEINFO LPLABELTYPEINFO_A
#define LabelTypeInfoTable LabelTypeInfoTable_A
#define dwLabelTypeInfoCount dwLabelTypeInfoCount_A
#endif

//--------------------------------------------------------------------
// Externs
//--------------------------------------------------------------------

extern LABELTYPEINFO LabelTypeInfoTable[];

extern DWORD dwLabelTypeInfoCount;


#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANTABL_H_

