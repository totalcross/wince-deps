
//-----------------------------------------------------------------------------
// FILENAME:		ScanParm.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:	Windows CE scanner driver parameter verification definitions
//
// NOTES:			Uses tab stops of 3
//
// 
//-----------------------------------------------------------------------------

#ifndef SCANPARM_H_
#define SCANPARM_H_

#ifdef __cplusplus
extern "C"
{
#endif

// Macro to get number of elements in an array
#define countof(x)	(sizeof(x) / sizeof(x[0]))

// Macro to get offset of a field within a structure
#define OFFSET_IN(type,fld) (UINT) &((type*)0)->fld

// Parameter verification descriptor
//
//	The following structure is used in parameter verification tables.
//	Each parameter structure has a parameter verification table.
//	For each field in a structure there is a corresponding entry in
//	the parameter verification table.
//
//		dwOffset			The offset of the field w/in the corresponding structure.
//		dwType			Enumeration that indicates the type of data in the field.
//							See the enumeration for a description of the types.
//		dwLoRangeOff	For fields with a contiguous range of valid values, this
//							is the lowest valid value for the field. For fields that
//							don't have a contiguous range of valid values, this is
//							the address of an array containing a list of valid values.
//							For boolean fields, this is set to zero.
//		dwHiRangeLen	For fields with a contiguous range of valid values, this
//							is the highest valid value for the field. For fields that
//							don't have a contiguous range of valid values, this is
//							the number of elements in the array of valid values.
//							For boolean fields, this is set to one.
//

typedef struct tagPARAM_DESCRIPTOR
{
	DWORD		dwOffset;		// Offset of field w/in corresponding structure
	DWORD		dwType;			// Data type (see enumeration)
	DWORD		dwLoRangeOff;	// Low end of valid range or array offset
	DWORD		dwHiRangeLen;	// High end of valid range or array length

} PARAM_DESCRIPTOR;

typedef PARAM_DESCRIPTOR FAR * LPPARAM_DESCRIPTOR;

enum
{
	PD_BYTE	= 0,			// Field is a BYTE with a contiguous range
	PD_WORD,					// Field is a WORD with a contiguous range
	PD_DWORD,				// Field is a DWORD with a contiguous range
	PD_BOOL,					// Field is a BOOL, 0=FALSE, 1=TRUE
	PD_ARRAY,				// Field is a DWORD with list of valid values
	PD_ARRAY_RO,			// Field is a read only DWORD with list of valid values
	PD_TERMINATOR = 255	// Indicates end of descriptor list
};

// Function prototypes
BOOL ValidateParams(LPPARAM_DESCRIPTOR lpPD, LPVOID lpParamSource, LPVOID lpParamDest);
BOOL ValidateDecoderParams(LPDECODER_PARAMS lpDPSource, LPDECODER_PARAMS lpDPDest);
BOOL ValidateReaderParams(LPREADER_PARAMS lpRPSource, LPREADER_PARAMS lpRPDest);
BOOL ValidateInterfaceParams(LPINTERFACE_PARAMS lpIPSource, LPINTERFACE_PARAMS lpIPDest);

// Parameter verification descriptor tables
extern PARAM_DESCRIPTOR	g_aReaderParamDesc[2];
extern PARAM_DESCRIPTOR	g_aInterfaceParamDesc[2];
extern PARAM_DESCRIPTOR	g_aUPCEANParamDesc[10];

#ifdef __cplusplus
}
#endif

#endif	// #ifndef SCANPARM_H_

