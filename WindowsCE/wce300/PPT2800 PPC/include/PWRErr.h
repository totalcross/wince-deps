
//--------------------------------------------------------------------
// FILENAME:			PwrErr.h
//
// Copyright(c) 1999 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by Power Management functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef POWRERR_H_
#define POWRERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT			0x80000000	/* Use bit 31 to indicate errror	*/
#define	USER_BIT		   	0x20000000	/* Bit 29 means not Win32 error	*/

#define	POWER_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_PWR_SUCCESS					0


// The enumerated key assigned to the driver under

#define	E_PWR_OPENINGACTIVEKEY		POWER_ERROR(0x0001)


// The enumerated key assigned to the driver under

#define	E_PWR_READINGACTIVEKEY		POWER_ERROR(0x0002)


// The registry key containing the driver's

#define	E_PWR_OPENINGPARAMKEY		POWER_ERROR(0x0003)


// The registry key containing the driver's

#define	E_PWR_READINGPARAMKEY		POWER_ERROR(0x0004)


// An attempt to allocate memory failed

#define	E_PWR_NOTENOUGHMEMORY		POWER_ERROR(0x0005)


// The device context ID passed to PWR_Open or
//  PWR_Deinit is invalid

#define	E_PWR_INVALIDDVCCONTEXT		POWER_ERROR(0x0006)


// The open context ID passed to PWR_Close,
// 	PWR_IOControl, PWR_Read, PWR_Seek or
//	PWR_Write is invalid

#define	E_PWR_INVALIDOPENCONTEXT	POWER_ERROR(0x0007)


// PWR_Open or PWR_Deint was called before a
//  successful init

#define	E_PWR_NOTINITIALIZED		POWER_ERROR(0x0008)


// The PDD DLL could not be loaded

#define	E_PWR_CANTLOADDEVICE		POWER_ERROR(0x0009)


// The PDD DLL did not contain the required entry points

#define	E_PWR_INVALIDDEVICE			POWER_ERROR(0x000A)


// Required device is not present, already in use
//  or not functioning properly

#define E_PWR_DEVICEFAILURE			POWER_ERROR(0x000B)


// The device could not be started

#define	E_PWR_CANTSTARTDEVICE		POWER_ERROR(0x000C)


// The default parameters could not be
//	obtained from the PDD

#define	E_PWR_CANTGETDEFAULTS		POWER_ERROR(0x000D)

// The control code passed to PWR_IOControl is invalid

#define	E_PWR_INVALIDIOCTRL			POWER_ERROR(0x000E)


// A NULL pointer was passed for a required argument

#define	E_PWR_NULLPTR				POWER_ERROR(0x000F)


// A passed argument is out of range

#define	E_PWR_INVALIDARG			POWER_ERROR(0x0010)


// The size of the buffer passed as an input to
//  IOControl is less than sizeof(STRUCT_INFO) or
//  is less than the size specified in
//  StructInfo.dwUsed

#define	E_PWR_BUFFERSIZEIN			POWER_ERROR(0x0011)


// The size of the buffer passed as an output to
//  IOControl is less than sizeof(STRUCT_INFO) or
//  is less than the size specified in
//  StructInfo.dwAllocated

#define	E_PWR_BUFFERSIZEOUT			POWER_ERROR(0x0012)


// A STRUCT_INFO structure field is invalid. Either
//  dwAllocated or dwUsed is less than the size of
//  STRUCT_INFO or dwUsed is greater than dwAllocated

#define	E_PWR_STRUCTSIZE			POWER_ERROR(0x0013)


// The size of a structure specified in a StructInfo
//  is too small to contain a required field

#define	E_PWR_MISSINGFIELD			POWER_ERROR(0x0014)


// An invalid handle was passed to a function

#define	E_PWR_INVALIDHANDLE			POWER_ERROR(0x0015)


// The value of a parameter either passed as an
//  argument to a function or as a member of a
//  structure is out of range or conflicts with
//  other parameters

#define	E_PWR_INVALIDPARAM			POWER_ERROR(0x0016)


// Unable to create a required event

#define	E_PWR_CREATEEVENT			POWER_ERROR(0x0017)


// Unable to create a required thread

#define	E_PWR_CREATETHREAD			POWER_ERROR(0x0018)


// Buffer is too small for incoming data

#define	E_PWR_BUFFERTOOSMALL		POWER_ERROR(0x0019)

// Version of function not supported (e.g. ANSI vs. UNICODE)

#define E_PWR_NOTSUPPORTED			POWER_ERROR(0x001A)


// The requested operation is inconsistent with the current state of the device

#define E_PWR_WRONGSTATE			POWER_ERROR(0x001B)


// No more items are available to be returned from POWER_FindFirst/POWER_FindNext

#define E_PWR_NOMOREITEMS			POWER_ERROR(0x001C)


// A required registry key could not be opened

#define E_PWR_CANTOPENREGKEY		POWER_ERROR(0x001D)


// A required registry value could not be read

#define E_PWR_CANTREADREGVAL		POWER_ERROR(0x001E)

// Device state change not permitted.

#define E_PWR_PERMISSIONDENIED		POWER_ERROR(0x001F)

// No such notify request found.

#define E_PWR_NOTFOUND			POWER_ERROR(0x0020)

// Invalid or unsupported state.

#define E_PWR_INVALIDSTATE		POWER_ERROR(0x0021)

// System shutdown in progress.  No further state changes permitted.

#define E_PWR_SHUTDOWNINPROGRESS	POWER_ERROR(0x0022)


#ifdef __cplusplus
}
#endif

#endif	// #ifndef PWRERR_H_
