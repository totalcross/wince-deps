
//--------------------------------------------------------------------
// FILENAME:			BARCODES.H
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Include file for ComTec Bar Code printer
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------

#ifndef _COMTEC_H_
#define _COMTEC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "barcodes.h"

typedef struct __COMTEC_BARCODEINFO {
  // For One-dimensional Standard Barcode type.
  int		width;		 // Unit-width of the narrow bar.
  int		ratio;		 // Ratio of the wide bar to the narrow bar.

  // For Two-dimensional Standard Barcode type.
  int		XDval;		 // Unit-width  of the narrowest element.
  int		YDval;		 // Unit-height of the narrowest element.
  int		Cval;		 // Number of columns to use.
  int		Sval;		 // Security level indicate maximum
						 // amount of erros to be detected.
} COMTEC_BARCODEINFO,  *LPCOMTEC_BARCODEINFO;

// *** Note:***
// The order of the elmements in the following table is corresponsing to
// Bar Code type definitions in BARCODES.H.
//
LPCTSTR  pCOMTEC_BarCodeTypeTable[] = {
  TEXT("UPCA"),				 // TYPE_UPCA	
  TEXT("UPCE"),				 // TYPE_UPCE	
  TEXT("UPCE"),				 // TYPE_UPCE
  TEXT("CODABAR"),			 // TYPE_CODABAR
  TEXT("39"),				 // TYPE_CODE39	
  TEXT("93"),				 // TYPE_CODE93	
  TEXT("128"),				 // TYPE_CODE128
  TEXT("EAN8"),				 // TYPE_EAN8	
  TEXT("EAN13"),			 // TYPE_EAN13	
  TEXT("MSI10"),  			 // TYPE_MSI	
  TEXT("I2OF5"),			 // TYPE_I2OF5	
  TEXT("PDF-417"),			 // TYPE_PDF417
};

#ifdef __cplusplus
}
#endif

#endif // _COMTEC_H_


