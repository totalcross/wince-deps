
//--------------------------------------------------------------------
// FILENAME:			DispErr.h
//
// Copyright(c) 1999,2000 Symbol Technologies Inc. All rights reserved.
//
// DESCRIPTION:			Error codes used by display functions
//
// NOTES:				Public
//
// 
//--------------------------------------------------------------------


#ifndef DISPERR_H_
#define DISPERR_H_

#ifdef __cplusplus
extern "C"
{
#endif


#define	ERROR_BIT				0x80000000	// Use bit 31 to indicate errror
#define	USER_BIT				0x20000000	// Bit 29 means not Win32 error	

#define	DISPLAY_ERROR(code)		(ERROR_BIT | USER_BIT | (WORD) code)

// The function completed successfully.

#define	E_DSP_SUCCESS           0

#define E_DSP_ERROR             DISPLAY_ERROR(0x0001)
#define	E_DSP_NULLPTR			DISPLAY_ERROR(0x0002)

// A STRUCT_INFO structure field is invalid. Either
//  dwAllocated or dwUsed is less than the size of
//  STRUCT_INFO or dwUsed is greater than dwAllocated

#define	E_DSP_STRUCTINFO		DISPLAY_ERROR(0x0003)

// The size of a structure specified in a StructInfo
//  is too small to contain a required field

#define	E_DSP_MISSINGFIELD		DISPLAY_ERROR(0x0004)

#define	E_DSP_PWRFAILED			DISPLAY_ERROR(0x0005)




#ifdef __cplusplus
}
#endif

#endif	// #ifndef DISPERR_H_
