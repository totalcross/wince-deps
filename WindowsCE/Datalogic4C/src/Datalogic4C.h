
#include <windows.h>

#if defined(WIN32) || defined(WINCE)
 #ifdef DATALOGIC_EXPORTS
 #define DATALOGIC_API __declspec(dllexport)
 #else
 #define DATALOGIC_API __declspec(dllimport)
 #endif
#else
 #define DATALOGIC_API extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

   DATALOGIC_API void DLScanner_Init();
   DATALOGIC_API void DLScanner_ScanEnable();
   DATALOGIC_API void DLScanner_RegisterLabelMessage(HWND hLabelWindow, UINT uiLabelMessage);

#ifdef __cplusplus
}
#endif