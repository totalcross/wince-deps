
#include "datalogic4c.h"
#include "dl_sedapi.h"

DLScanner scanner;

DATALOGIC_API void DLScanner_Init()
{
   scanner.init();
}

DATALOGIC_API void DLScanner_ScanEnable()
{
   scanner.scanEnable();
}

DATALOGIC_API void DLScanner_RegisterLabelMessage(HWND hLabelWindow, UINT uiLabelMessage)
{
   scanner.registerLabelMessage(hLabelWindow, uiLabelMessage);
}

