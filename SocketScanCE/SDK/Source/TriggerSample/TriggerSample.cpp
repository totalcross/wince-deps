#include "stdafx.h"

#define SCAN_TRIGGER_MSG_STR TEXT("ScanTrigger")

int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
{
	DWORD triggerMsg;
	TCHAR errorStr[256];
	int scannerHandle = 1;

	// Override the default if specified as a compiler define
#ifdef SCANNER_HANDLE_DEFAULT
	scannerHandle = SCANNER_HANDLE_DEFAULT;
#endif

	// If a parameter is supplied, use that as the handle of the scanner to trigger
	if ((lpCmdLine) && (*lpCmdLine))
	{
		scannerHandle = _wtoi(lpCmdLine);
	}

 	// Create the broadcast msg
	triggerMsg = RegisterWindowMessage(SCAN_TRIGGER_MSG_STR);
	if(triggerMsg == 0)
	{
		wsprintf(errorStr, TEXT("RegisterWindowMessage (%s) failed, error = %d."),  SCAN_TRIGGER_MSG_STR, GetLastError());
		MessageBox(NULL, errorStr, TEXT("ScanTrigger"), MB_TOPMOST | MB_ICONERROR);
	}
	else
	{
		SendNotifyMessage(HWND_BROADCAST, triggerMsg, (WPARAM) scannerHandle, 0);
	}

	return 0;
}

