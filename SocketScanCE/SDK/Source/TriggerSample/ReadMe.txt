 Trigger Sample   
  
This application demostrates how to send a trigger message directly to the SCANAPI library.
When used with the handle number as a parameter it will trigger the appropriate scanner. 
The user can program a button to trigger on the device to use this executable for triggering.
This trigger is compatible with .NET as well as native C applications.   
   
