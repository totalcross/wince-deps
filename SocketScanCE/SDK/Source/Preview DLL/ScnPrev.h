// Some Wizard-generated stuff...

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SCNPREV_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SCNPREV_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SCNPREV_EXPORTS
#define SCNPREV_API __declspec(dllexport)
#else
#define SCNPREV_API __declspec(dllimport)
#endif

// Our DataPreview declaration. Note the important use of the
// extern "C" directive to prevent name mangling! If you are
// developing your DLL in some other language, make sure your
// compiler creates an equivalent calling convention (WINAPI).

extern "C" SCNPREV_API int DataPreview(TCHAR * lpData, int nSize);

