/**************************************************************************************

 Copyright (C) 2004 by Socket Communications, All Rights Reserved.

   THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
   ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
   PARTICULAR PURPOSE.

 MODULE:		RFIDDemo.cpp (RFID Demo)

 PURPOSE:		Main CPP source file for the Socket RFID demo app.
				
 DESCRIPTION:	This program demonstrates the use of the RFID portion
				of Socket ScanAPI.
				
 COMMENTS:

****************************************************************************************/

#include "stdafx.h"
#include "RFIDDemo.h"
#include "ScanApi.h"
#include <commctrl.h>
#include <commdlg.h>
#include <windowsx.h>

// A few extra includes for Pocket PC...

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

#include <windows.h>
#include "newres.h"
#include <sipapi.h>
#include <commctrl.h>
#include <aygshell.h>
#pragma comment(lib, "aygshell.lib")
#include "resource.h" 

#endif

#if defined(WIN32_PLATFORM_PSPC)
#define SHGetMenu(hWndMB)  (HMENU)SendMessage((hWndMB), SHCMBM_GETMENU, (WPARAM)0, (LPARAM)0);
#define SHGetSubMenu(hWndMB,ID_MENU) (HMENU)SendMessage((hWndMB), SHCMBM_GETSUBMENU, (WPARAM)0, (LPARAM)ID_MENU);
#define SHSetSubMenu(hWndMB,ID_MENU) (HMENU)SendMessage((hWndMB), SHCMBM_SETSUBMENU, (WPARAM)0, (LPARAM)ID_MENU);
#endif

#define MAX_LOADSTRING		100
#define MAX_BUF				256
#define SCAN_BUF_SIZE		4096
#define SCREEN_WIDTH_CUTOFF	320

// Messages needed for our scanner application...

#define WM_INSERTION	WM_USER + 1 // the message we want on device insertions
#define WM_REMOVAL		WM_USER + 2 // the message we want on device removals
#define WM_SCANNERDATA	WM_USER + 3 // the message we want when data is available

const LPCTSTR gTagTypes[NUM_TAG_TYPES] = 
{
	TEXT("AUTO DETECT (00)"),
	TEXT("ISO 15693 (01)"),
	TEXT("ICODE1 (02)"),
	TEXT("TAG IT HF (03)"),
	TEXT("ISO 14443A (04)"),
	TEXT("ISO 14443B (05)"),
	TEXT("PICO TAG (06)"),
	TEXT("RFU (07)"),
	TEXT("GEMWAVE C210 (08)")
};


// Global Variables:
HINSTANCE	hInst;						// The current instance
HWND		hwndCB;						// The command bar handle
BOOL		launchSocketScan;			// Whether to restart SocketScan when we exit
HWND		parentWindow;				// The main window of this app
HWND		mainDialog = NULL;			// The main scan info dialog handle
HWND		advancedDialog = NULL;		// The advanced scan info dialog handle
BYTE		scanBuffer[SCAN_BUF_SIZE];	// Buffer used to store the last scan data
ULONG		scanDataSize = 0;			// Number of characters scanned last
RECT		gScreenRect;				// Screen size used to select dialog template
BOOL		loopMode = FALSE;			// Whether we loop when selecting tags
BOOL		looping = FALSE;			// Whether we are actually looping at this time
BOOL		advancedRead = FALSE;		// Whether this is a read from the advanced page

// The Scanner Handle for our application, & etc...

HANDLE		MyScanner = 0;					// Our scanner handle
BOOL		APIInitialized = FALSE;			// Tracks if ScanInit succeeded
SCANDEVINFO	ScanDevInfo;					// To get info about the scanner
DWORD		StartBlock = 0;					// Starting block to read from
DWORD		NumBlocks = 4;					// Number of blocks to read
UCHAR		gTagType = 0;					// Tag type to use when selecting a tag

// Foward declarations of functions included in this code module:

ATOM				MyRegisterClass		(HINSTANCE hInstance, LPTSTR szWindowClass);
BOOL				InitInstance		(HINSTANCE hInstance, int nCmdShow);
LRESULT CALLBACK	WndProc				(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	About				(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	ChooseTag			(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	MainDlgProc			(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	AdvancedDlgProc		(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void				UpdateScanInfo		(HWND dlg, PBYTE scanData, ULONG dataSize);
void				SendSelectRequest	(HANDLE scanner, BOOL invMode);
void				HandleScannerData	(HANDLE scanner, HWND hDlg);
void				AddTagInfo			(PRFID_RESPONSE response, HWND hDlg);
void				EnableControls		(BOOL enable);
void				SendReadRequest		(HANDLE scanner, PRFID_RESPONSE selectResponse);
void				SendWriteRequest	(HANDLE scanner, PRFID_RESPONSE selectResponse);
void				ClearDisplay		(HWND parentWindow);
void				SetStatusText		(HWND parentWindow, LPTSTR statusText, ...);
BOOL				ShowSip				(BOOL show);
void				DoCheckMenuItem		(DWORD item, BOOL check);
void				ToggleLoopMode		(void);
void				EnableLooping		(BOOL enable);
void				HandleAdvResponse	(HANDLE scanner, PRFID_RESPONSE selectResponse);
void				UpdateBlockInfo		(HWND hDlg);
ULONG				BytesPerBlock		(PRFID_RESPONSE response);
void				EnableRWControls	(BOOL enable);
void				GetTagTypeStr		(UCHAR tagType, LPTSTR tagTypeStr);
void				MakeTagString		(PRFID_RESPONSE response, LPTSTR tagStr);

void
DetermineScreenSize(void)
{
   BOOL  bResult;

   // Get the limits of the 'workarea'
   bResult = SystemParametersInfo(SPI_GETWORKAREA, sizeof(RECT), &gScreenRect, 0);
   if (!bResult) 
   {
      gScreenRect.left = gScreenRect.top = 0;
      gScreenRect.right = GetSystemMetrics(SM_CXSCREEN);
      gScreenRect.bottom = GetSystemMetrics(SM_CYSCREEN);
   }
}


int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	HWND hWnd;

	// If SocketScan is running, shut it down and remember we did so we can
	// restart it
	launchSocketScan = ((hWnd = FindWindow(SOCKET_SCAN_WIN_NAME, NULL)) != NULL);
	if (hWnd)
	{
		HANDLE socketScanExitEvent;
		
		socketScanExitEvent = CreateEvent(NULL, FALSE, FALSE, SOCKET_SCAN_EXIT_EVENT);

		// Wait up to 5s for the SocketScan application to close
		SendMessage(hWnd, WM_CLOSE, 0, 0);
		WaitForSingleObject(socketScanExitEvent, SOCKET_EXIT_WAIT_TIME);
		CloseHandle(socketScanExitEvent);
	}

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_SCANDEMO);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	// Notify any interested apps (SocketScan) that we are leaving
	HANDLE RFIDDemoExitEvent;
	RFIDDemoExitEvent = CreateEvent(NULL, FALSE, FALSE, RFID_DEMO_EXIT_EVENT);
	SetEvent(RFIDDemoExitEvent);
	CloseHandle(RFIDDemoExitEvent);

	// If we closed SocketScan when we started, restart it now that we
	// are leaving
	if (launchSocketScan)
	{
		PROCESS_INFORMATION procInfo;
		TCHAR appFilename[MAX_PATH];
		TCHAR appPathname[MAX_PATH];

		appFilename[0] = 0;
		appPathname[0] = 0;
		LoadString(hInstance, IDS_SCKTSCAN_LAUNCH_NAME, appFilename, sizeof(appFilename) / sizeof(TCHAR));
		LoadString(hInstance, IDS_SCKTSCAN_LAUNCH_PATH, appPathname, sizeof(appPathname) / sizeof(TCHAR));

		// First try and launch the filename only, if that doesn't work, try the full path
		if (!CreateProcess(appFilename, NULL, NULL, NULL, FALSE, 0, NULL, NULL, NULL, &procInfo))
		{
			if ((appPathname[0] != 0) && (appPathname[wcslen(appPathname) - 1] != TEXT('\\')))
			{
				wcscat(appPathname, TEXT("\\"));
				wcscat(appPathname, appFilename);

				CreateProcess(appPathname, NULL, NULL, NULL, FALSE, 0, NULL, NULL, NULL, &procInfo);
			}
		}
	}

	return msg.wParam;
}

// MyRegisterClass is just "wizard-generated code"...

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
	WNDCLASS	wc;

    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WndProc;
    wc.cbClsExtra		= 0;
    wc.cbWndExtra		= 0;
    wc.hInstance		= hInstance;
    wc.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_RFID_DEMO));
    wc.hCursor			= 0;
    wc.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName		= 0;
    wc.lpszClassName	= szWindowClass;

	return RegisterClass(&wc);
}

//
//  FUNCTION: CheckApiCall()
//
//  PURPOSE: Reports ScanAPI errors to the user.
//
//  COMMENTS:
//
//    Generates a dialog box explaining the SCAN_ERROR received when calling an
//    API function. Owner is the owner of the dialog box. lpFuncName is the name
//    of the API function. MyApiResult is the error code to be examined. If the
//    MyApiResult parameter is SR_SUCCESS, this function reports no error. The
//    result is passed back to the caller.
//
SCAN_RESULT CheckApiCall(HWND Owner, LPCTSTR lpFuncName, SCAN_RESULT MyApiResult)
{
	TCHAR ErrText[MAX_BUF];
	TCHAR ErrTranslation[MAX_BUF];
	int	ErrTranslationSize = sizeof(ErrTranslation);
	SCAN_RESULT Result;
	SCAN_RESULT TranslateResult;
	HWND targetDlg = (advancedDialog) ? advancedDialog : mainDialog;

	SetStatusText(targetDlg, TEXT("Calling %s"), lpFuncName);

	// save function return code...
	Result = MyApiResult;

	// report error if it is not SR_SUCCESS...
	if (Result != SR_SUCCESS)
	{
		// if ScanErrToText fails, report both failures...
		if ((TranslateResult = ScanErrToText(Result, ErrTranslation, &ErrTranslationSize)) != SR_SUCCESS)
		{
			wsprintf(ErrText, TEXT("A call to %s failed with error code %d."), lpFuncName, Result);
			SetStatusText(targetDlg, ErrText);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST | MB_APPLMODAL);
			
			wsprintf(ErrText, TEXT("Also, an error occured attempting to translate error code %d using ScanErrToText."), TranslateResult);
			SetStatusText(targetDlg, ErrText);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST | MB_APPLMODAL);
		}
		else
		{
			wsprintf(ErrText, TEXT("A call to %s failed with error code %d: %s"), lpFuncName, Result, ErrTranslation);
			SetStatusText(targetDlg, ErrText);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST | MB_APPLMODAL);
		}
	}
	else
	{
		SetStatusText(targetDlg, TEXT("%s succeeded"), lpFuncName);
	}
	// give the result back to the caller...
	return (Result);
}


LRESULT CALLBACK 
MainDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
		case WM_INITDIALOG:
		{
#ifdef WIN32_PLATFORM_PSPC
			SHINITDLGINFO shidi;

			shidi.dwMask = SHIDIM_FLAGS;
			shidi.dwFlags = SHIDIF_DONEBUTTON | SHIDIF_SIPDOWN | SHIDIF_SIZEDLGFULLSCREEN;
			shidi.hDlg = hwnd;
			SHInitDialog(&shidi);

#else
			SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) & ~WS_EX_CAPTIONOKBTN);
#endif

#ifndef WIN32_PLATFORM_PSPC

            if(gScreenRect.right <= SCREEN_WIDTH_CUTOFF)
			   // Move the window down below the menu bar
			   SetWindowPos(hwnd, NULL, 0, 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#endif

			// Should return nonzero to set focus to the first control in the
			// dialog, or zero if this routine sets the focus manually.
			return FALSE;
		}

		case WM_DESTROY:
			if (parentWindow)
			{
				DestroyWindow(parentWindow);
			}
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_TAG_LIST:
					if (ListBox_GetCurSel(GetDlgItem(hwnd, IDC_TAG_LIST)) != LB_ERR)
					{
						Button_Enable(GetDlgItem(hwnd, IDC_READ_TAG), TRUE);
					}
					break;

				case IDC_CLEAR_TAGS:
					ClearDisplay(hwnd);		// Clear the tag list box
					Button_Enable(GetDlgItem(hwnd, IDC_READ_TAG), FALSE);
					Button_Enable(GetDlgItem(hwnd, IDC_CLEAR_TAGS), FALSE);
					break;

				case IDC_SELECT_TAGS:
					if (looping)
					{
						EnableLooping(FALSE);
					}
					else
					{
						if (loopMode)
						{
							EnableLooping(TRUE);
						}
						else
						{
							Button_Enable(GetDlgItem(hwnd, IDC_SELECT_TAGS), FALSE);
						}

						// Disable buttons that can't be used until this finishes
						Button_Enable(GetDlgItem(hwnd, IDC_READ_TAG), FALSE);

						// First clear the list box
						ClearDisplay(hwnd);

						SendSelectRequest(MyScanner, TRUE);	// Send Select tag cmd
					}
					break;

				case IDC_READ_TAG:
					{
						ULONG index;
						PRFID_RESPONSE selectResponse;
						HWND listBox = GetDlgItem(hwnd, IDC_TAG_LIST);

						// Disable buttons that can't be used until this finishes
						Button_Enable(GetDlgItem(hwnd, IDC_READ_TAG), FALSE);

						// First clear the read data field
						Edit_SetText(GetDlgItem(hwnd, IDC_READ_DATA), TEXT(""));
						scanDataSize = 0;	// Clear count of bytes in data field

						// Get the Selected tag request struct
						index = ListBox_GetCurSel(listBox);
						if (index != LB_ERR)
						{
							selectResponse = (PRFID_RESPONSE) ListBox_GetItemData(listBox, index);
							SendReadRequest(MyScanner, selectResponse);		// Send Read data cmd
						}
					}
					break;

				case IDC_DISPLAY_AS_HEX:
					// Update the scan data control
					UpdateScanInfo(hwnd, NULL, 0);
					break;

				case IDOK:
				case IDCANCEL:
					DestroyWindow(hwnd);
					break;
			}
			return TRUE;

    }
    return FALSE;
}


void
EnableRWControls(BOOL enable)
{
	if (advancedDialog)
	{
		Button_Enable(GetDlgItem(advancedDialog, IDC_READ_TAG), enable);
		Button_Enable(GetDlgItem(advancedDialog, IDC_WRITE_TAG), enable &&
			Edit_GetTextLength(GetDlgItem(advancedDialog, IDC_WRITE_DATA)));
	}
	else
	{
		BOOL tagsToRead;

		tagsToRead = (ListBox_GetCurSel(GetDlgItem(mainDialog, IDC_TAG_LIST)) != LB_ERR);
		Button_Enable(GetDlgItem(mainDialog, IDC_READ_TAG), enable && tagsToRead);
	}
}


LRESULT CALLBACK 
AdvancedDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR workStr[256];

    switch(uMsg)
    {
		case WM_INITDIALOG:
		{
#ifdef WIN32_PLATFORM_PSPC
			SHINITDLGINFO shidi;

			shidi.dwMask = SHIDIM_FLAGS;
			shidi.dwFlags = SHIDIF_DONEBUTTON | SHIDIF_SIPDOWN | SHIDIF_SIZEDLGFULLSCREEN;
			shidi.hDlg = hwnd;
			SHInitDialog(&shidi);

#else
			SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) & ~WS_EX_CAPTIONOKBTN);
#endif

#ifndef WIN32_PLATFORM_PSPC

            if(gScreenRect.right <= SCREEN_WIDTH_CUTOFF)
			   // Move the window down below the menu bar
			   SetWindowPos(hwnd, NULL, 0, 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#endif

			// Set default values in edit fields
			wsprintf(workStr, TEXT("%d"), StartBlock);
			Edit_SetText(GetDlgItem(hwnd, IDC_START_BLOCK), workStr);
			wsprintf(workStr, TEXT("%d"), NumBlocks);
			Edit_SetText(GetDlgItem(hwnd, IDC_NUM_BLOCKS), workStr);

			// Should return nonzero to set focus to the first control in the
			// dialog, or zero if this routine sets the focus manually.
			return FALSE;
		}

		case WM_DESTROY:
			// Save block info
			UpdateBlockInfo(hwnd);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_READ_TAG:
					EnableRWControls(FALSE);
					UpdateBlockInfo(hwnd);
					advancedRead = TRUE;
					Edit_SetText(GetDlgItem(hwnd, IDC_READ_DATA), TEXT(""));
					SendSelectRequest(MyScanner, FALSE);
					break;
				case IDC_WRITE_TAG:
					EnableRWControls(FALSE);
					UpdateBlockInfo(hwnd);
					advancedRead = FALSE;
					SendSelectRequest(MyScanner, FALSE);
					break;
				case IDC_WRITE_DATA:
					if (MyScanner)
					{
						Button_Enable(GetDlgItem(hwnd, IDC_WRITE_TAG),
							Edit_GetTextLength(GetDlgItem(hwnd, IDC_WRITE_DATA)));
					}
					break;
				case IDOK:
				case IDCANCEL:
#ifdef WIN32_PLATFORM_PSPC
					DoCheckMenuItem(ID_RFID_ADVANCED, FALSE);
#else
					DoCheckMenuItem(IDM_RFID_ADVANCED, FALSE);
#endif
					ShowWindow(mainDialog, SW_SHOW);
					DestroyWindow(hwnd);
					advancedDialog = NULL;
					break;
			}
			return TRUE;

    }
    return FALSE;
}


// InitInstance has been modified to make the call to the ScanInit() function. It is
// done here because we want the main window to be created first - as soon as ScanInit()
// is called, our main window will begin to receive WM_INSERTION messages if scanning
// devices are present, so we want the main window to be fully created and ready to
// rock and roll when those messages come in...

//
//  FUNCTION: InitInstance(HANDLE, int)
//
//  PURPOSE: Saves instance handle and creates main window
//
//  COMMENTS:
//
//    In this function, we save the instance handle in a global variable and
//    create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	TCHAR	szTitle[MAX_LOADSTRING];			// The title bar text
	TCHAR	szWindowClass[MAX_LOADSTRING];		// The window class name

	hInst = hInstance;		// Store instance handle in our global variable
	// Initialize global strings
	LoadString(hInstance, IDC_RFIDDEMO, szWindowClass, MAX_LOADSTRING);
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);

	// If we are already running, just bring our window to the front and exit
	if (FindWindow(szWindowClass, szTitle))
	{
		SetForegroundWindow(FindWindow(szWindowClass, szTitle));
		return FALSE;
	}

	MyRegisterClass(hInstance, szWindowClass);

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)
	parentWindow = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0, 26, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
#else

	parentWindow = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0, 0, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

#endif

	if (!parentWindow)
	{	
		return FALSE;
	}

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

	ShowWindow(parentWindow, nCmdShow);
	UpdateWindow(parentWindow);

#else

	ShowWindow(parentWindow, nCmdShow);
	UpdateWindow(parentWindow);
	if (hwndCB)
		CommandBar_Show(hwndCB, TRUE);

#endif

	// call ScanInit...

	if (CheckApiCall(parentWindow, TEXT("ScanInit"), ScanInit(parentWindow, WM_INSERTION, WM_REMOVAL)) != SR_SUCCESS)
	{
		// get rid of the main window and return FALSE...
		DestroyWindow(parentWindow);
		return FALSE;
	}
	else
		// set this so we won't call ScanDeinit() needlessly...
		APIInitialized = TRUE;

	// Determine our screen size so we know which dialog template to use
	DetermineScreenSize();

	// Create the scan info dialog
#ifdef WIN32_PLATFORM_PSPC
    mainDialog = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_SCAN_INFO_PSPC), parentWindow, (DLGPROC) MainDlgProc); 
#else
    mainDialog = CreateDialog(hInstance, (gScreenRect.right > SCREEN_WIDTH_CUTOFF) ?
		MAKEINTRESOURCE(IDD_SCAN_INFO) : MAKEINTRESOURCE(IDD_SCAN_INFO_PSPC), parentWindow, 
		(DLGPROC) MainDlgProc); 
#endif

	ShowWindow(mainDialog, TRUE);

	return TRUE;
}


void
UpdateScanInfo(HWND dlg, PBYTE scanData, ULONG dataSize)
{
	TCHAR buffer[32];
	TCHAR displayBuffer[SCAN_BUF_SIZE * 5];	// Enough space to display in hex format
	BOOL displayAsHex = FALSE;
	ULONG index;

	// If no new data is provided, use the stored data from the last scan
	if (scanData == NULL)
	{
		scanData = scanBuffer;
		dataSize = scanDataSize;
	}
	else
	{
		// Save the new data in our global buffer
		memcpy(scanBuffer, scanData, dataSize);
		scanDataSize = dataSize;
	}

	// Display the scanned data
	if (dlg != advancedDialog)
	{
		displayAsHex = (Button_GetCheck(GetDlgItem(dlg, IDC_DISPLAY_AS_HEX)) == BST_CHECKED);
	}

	*displayBuffer = 0;
	for (index = 0; index < dataSize; index++)
	{
		if (displayAsHex)
		{
			wsprintf(buffer, TEXT("0x%02X "), *scanData++);
			wcscat(displayBuffer, buffer);
		}
		else
		{
			if (*scanData >= ' ')
			{
				wsprintf(buffer, TEXT("%c"), *scanData++);
				wcscat(displayBuffer, buffer);
			}
			else if (*scanData == '\r')
			{
				wcscat(displayBuffer, TEXT("\r\n"));
				scanData++;
			}
			else
			{
				if (dlg != advancedDialog)
				{
					wsprintf(buffer, TEXT("<0x%02X>"), *scanData++);
					wcscat(displayBuffer, buffer);
				}
				else
				{
					scanData++;
				}
			}
		}
	}

	Edit_SetText(GetDlgItem(dlg, IDC_READ_DATA), displayBuffer);
}


//  Most of the interesting API calls happen in this function.

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId;
	int wmEvent;

	switch (message) 
	{
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;

		case WM_COMMAND:

			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				// I'm using two sets of identifiers here to support the
				// different control IDs used with Pocket PC and HPC menu items...
				case ID_RFID_ABOUT:
				case IDM_RFID_ABOUT:

				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;

				case ID_RFID_CHOOSE_TAG:
				case IDM_RFID_CHOOSE_TAG:

				   DialogBox(hInst, (LPCTSTR)IDD_CHOOSE_TAG_TYPE, hWnd, (DLGPROC)ChooseTag);
				   break;

				case ID_RFID_EXIT:
				case IDM_RFID_EXIT:
				   DestroyWindow(hWnd);
				   break;

				case ID_RFID_LOOP_MODE:
				case IDM_RFID_LOOP_MODE:
					ToggleLoopMode();
					break;

				case IDM_RFID_ADVANCED:
				case ID_RFID_ADVANCED:
					if (!advancedDialog)
					{
						if (looping)
						{
							EnableLooping(FALSE);
						}

#ifdef WIN32_PLATFORM_PSPC
						advancedDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_ADVANCED_PSPC), parentWindow, 
							(DLGPROC) AdvancedDlgProc); 
						DoCheckMenuItem(ID_RFID_ADVANCED, TRUE);
#else
						advancedDialog = CreateDialog(hInst, (gScreenRect.right > SCREEN_WIDTH_CUTOFF) ?
							MAKEINTRESOURCE(IDD_ADVANCED) : MAKEINTRESOURCE(IDD_ADVANCED_PSPC), parentWindow, 
							(DLGPROC) AdvancedDlgProc); 
						DoCheckMenuItem(IDM_RFID_ADVANCED, TRUE);
#endif
						EnableControls(MyScanner != 0);
						ShowWindow(mainDialog, SW_HIDE);
						ShowWindow(advancedDialog, SW_SHOW);
					}
					else
					{
						ShowWindow(mainDialog, SW_SHOW);
						DestroyWindow(advancedDialog);
						advancedDialog = NULL;
#ifdef WIN32_PLATFORM_PSPC
						DoCheckMenuItem(ID_RFID_ADVANCED, FALSE);
#else
						DoCheckMenuItem(IDM_RFID_ADVANCED, FALSE);
#endif
					}
				   break;

				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}

			break;

		// Keep the focus on our child dialog so its OK button will stay in the
		// menu bar
		case WM_SETFOCUS:
			if (mainDialog)
			{
				SetFocus(mainDialog);
			}
			break;

		case WM_CREATE:

			// If we are compiling for a Pocket PC, we do the menu much
			// differently than for the HPC-class devices...

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

			//Create the menubar.
			SHMENUBARINFO mbi;

			memset (&mbi, 0, sizeof (SHMENUBARINFO));
			mbi.cbSize     = sizeof (SHMENUBARINFO);
			mbi.hwndParent = hWnd;
			mbi.nToolBarId = IDR_MENUBAR3;
			mbi.hInstRes   = hInst;
			mbi.nBmpId     = 0;
			mbi.cBmpImages = 0;  

			if (!SHCreateMenuBar(&mbi))
			{
				MessageBox(hWnd, L"SHCreateMenuBar Failed", L"Error", MB_OK | MB_TOPMOST | MB_APPLMODAL);

				return -1;
			}
      
			hwndCB = mbi.hwndMB;

#else

			// Create a command bar...
			hwndCB = CommandBar_Create(hInst, hWnd, 1);	
			CommandBar_InsertMenubar(hwndCB, hInst, IDM_MENU, 0);
			CommandBar_AddAdornments(hwndCB, 0, 0);

#endif
			break;

		case WM_INSERTION:

			// do an hourglass...
			SetCursor(LoadCursor(NULL, IDC_WAIT));

			// save the scanner handle...
			MyScanner = (HANDLE) lParam;
			
			// call API and check return code...
			if (CheckApiCall(hWnd, TEXT("ScanOpenDevice"), ScanOpenDevice(MyScanner)) == SR_SUCCESS)
			{
				// request device capabilities of the scanner...
				memset(&ScanDevInfo, 0, sizeof(SCANDEVINFO));

				// don't forget to set the StructSize member or 
				// you'll get SR_INVALID_STRUCTURE
				ScanDevInfo.StructSize = sizeof(SCANDEVINFO);

				// call API and check return code...
				CheckApiCall(hWnd, TEXT("ScanGetDevInfo"), ScanGetDevInfo(MyScanner, &ScanDevInfo));
		
				if (ScanDevInfo.ScannerType != SCANNER_RFID)
				{
					// make the API call...
					CheckApiCall(hWnd, TEXT("ScanCloseDevice"), ScanCloseDevice(MyScanner));
					MyScanner = 0;

					MessageBox(parentWindow, TEXT("The inserted scanner is not a compatible RFID scanner."), 
						TEXT("Wrong Scanner"), MB_ICONERROR | MB_TOPMOST | MB_APPLMODAL);
				}
				else
				{
					// request data events for the new scanner...
					// call API and check return code...
					CheckApiCall(hWnd, TEXT("ScanRequestDataEvents"), ScanRequestDataEvents(MyScanner, hWnd, WM_SCANNERDATA));

					// confirmation we have an open scanning device...
					EnableControls(TRUE);
					MessageBeep(0);
				}
			}

			SetCursor(NULL);
			
			break;

		case WM_REMOVAL:
			if (MyScanner)
			{
				// make the API call...
				CheckApiCall(hWnd, TEXT("ScanCloseDevice"), ScanCloseDevice(MyScanner));

				EnableControls(FALSE);
				MessageBeep(0);
				
				// zero out our scanner handle...
				MyScanner = 0;
			}
			break;

		case WM_SCANNERDATA:
			HandleScannerData(MyScanner, advancedDialog ? advancedDialog : mainDialog);
			break;

		// SocketScan will send us this msg if we are running when it is launched, we
		// just perform a select tag command
		case WM_DEMO_SCAN:
			if (advancedDialog)	// Go back to the main page if necessary
			{
				ShowWindow(mainDialog, SW_SHOW);
				DestroyWindow(advancedDialog);
				advancedDialog = NULL;
			}
			SetForegroundWindow(parentWindow);
			SendMessage(mainDialog, WM_COMMAND, IDC_SELECT_TAGS, 0);
			break;

		case WM_DESTROY:
			if (APIInitialized)
			{
				// call the API and check the return code...
				CheckApiCall(hWnd, TEXT("ScanDeinit"), ScanDeinit());
			}

			CommandBar_Destroy(hwndCB);
			PostQuitMessage(0);
			break;

		default:

			return DefWindowProc(hWnd, message, wParam, lParam);
   }

   return 0;
}


// Mesage handler for the About box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;

	switch (message)
	{
		case WM_INITDIALOG:
			// trying to center the About dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the About box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}
			return TRUE;

		case WM_COMMAND:
			if ((LOWORD(wParam) == IDOK) || (LOWORD(wParam) == IDCANCEL))
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}


// Mesage handler for the Choose tag type dialog box
LRESULT CALLBACK ChooseTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;

	switch (message)
	{
		case WM_INITDIALOG:
			HWND comboCtrl;
			ULONG index;
			ULONG selection;

			// trying to center the Choose Tag dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the About box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}

			// Populate the combo box
			comboCtrl = GetDlgItem(hDlg, IDC_TAG_TYPE);
			for (index = 0; index < NUM_TAG_TYPES; index++)
			{
				ComboBox_AddString(comboCtrl, gTagTypes[index]);
			}

			// Select the appropriate item
			selection = gTagType;
			ComboBox_SetCurSel(comboCtrl, selection);

			return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK)
			{
				// Save the tag type
				gTagType = ComboBox_GetCurSel(GetDlgItem(hDlg, IDC_TAG_TYPE));
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}


// Construct a Select Tag request and send it using the RFID specific
// send request function of Scan API
void SendSelectRequest(HANDLE scanner, BOOL invMode)
{
	CheckApiCall(parentWindow, TEXT("ScanRFIDSelectTag"), ScanRFIDSelectTag(scanner, 
		gTagType, NULL, loopMode && (!advancedDialog), invMode, 0));
}


// Construct a Read Tag request and send it using the RFID specific
// send request function of Scan API
void 
SendReadRequest(HANDLE scanner, PRFID_RESPONSE selectResponse)
{
	CheckApiCall(parentWindow, TEXT("ScanRFIDReadTag"), ScanRFIDReadTag(scanner, 
		selectResponse->tagType, selectResponse->tagID, StartBlock, NumBlocks));
}


void 
SendWriteRequest(HANDLE scanner, PRFID_RESPONSE selectResponse)
{
	PBYTE writeBuffer;
	DWORD writeSize;
	HWND writeField = GetDlgItem(advancedDialog, IDC_WRITE_DATA);
	ULONG blockSize;

	// Get the size of the buffer needed
	blockSize = BytesPerBlock(selectResponse);

	// Get the length of the text
	writeSize = Edit_GetTextLength(writeField);	

	// Make sure the text is not too long
	if (writeSize > blockSize * NumBlocks)
	{
		SetStatusText(advancedDialog, TEXT("Incorrect data length for write (%d).  Should be %d."), writeSize,
			blockSize * NumBlocks);
		EnableRWControls(TRUE);
		return;
	}

	// Allocate space for the write buffer
	writeBuffer = (PBYTE) LocalAlloc(LPTR, (blockSize * NumBlocks + 1) * sizeof(TCHAR));	// Incl. terminating zero

	if (writeBuffer)
	{
		LPTSTR srcPtr = (LPTSTR) writeBuffer;
		PBYTE dstPtr = writeBuffer;

		// Get the data (unicode)
		Edit_GetText(writeField, (LPTSTR) writeBuffer, writeSize + 1);

		// Now convert to ascii (fits in same buffer)
		while (*srcPtr)
		{
			*dstPtr++ = (BYTE) (*srcPtr++);
		}

		// If the data is too small (writeSize < blockSize * NumBlocks) then pad with zeros
		while (writeSize < blockSize * NumBlocks)
		{
			*dstPtr++ = 0;
			writeSize++;
		}

		CheckApiCall(parentWindow, TEXT("ScanRFIDWriteTag"), ScanRFIDWriteTag(scanner, 
			selectResponse->tagType, selectResponse->tagID, StartBlock, NumBlocks, writeBuffer, writeSize));

		LocalFree(writeBuffer);
	}
}


void HandleScannerData(HANDLE scanner, HWND hDlg)
{
	RFID_RESPONSE response;
	PBYTE scannerData = 0;
	UINT scannerDataSize = 0;
	TCHAR workStr[256];
	static TCHAR tagData[(MAX_TAG_ID_SIZE + 1) * 2 + 2];

	// First call Scan API to get just the response struct
	if (CheckApiCall(parentWindow, TEXT("ScanRFIDGetData"), ScanRFIDGetData(scanner, &response, NULL, 
		&scannerDataSize)) != SR_SUCCESS)
	{
		EnableRWControls(TRUE);
		return;
	}

	// If our read/write tag returned certain status values, display appropriate message and leave
	switch (response.lastError)
	{
		case RESP_READ_TAG_FAIL:
			wcscpy(workStr, TEXT("Failed to read data from tag"));
			break;
		case RESP_INVALID_STARTING_BLOCK:
			wcscpy(workStr, TEXT("Invalid starting block specified for read"));
			break;
		case RESP_INVALID_NUMBER_OF_BLOCKS:
			wcscpy(workStr, TEXT("Invalid number of blocks specified for read"));
			break;
		case RESP_WRITE_TAG_PASS:
			wcscpy(workStr, TEXT("Successfully wrote data to tag"));
			break;
		case RESP_WRITE_TAG_FAIL:
			wcscpy(workStr, TEXT("Failed to write data to tag"));
			break;
		case RESP_INVALID_MESSAGE_LENGTH:
			wcscpy(workStr, TEXT("The size of the data does not match the size determined by the number of blocks"));
			break;
		default:
			workStr[0] = 0;
			break;
	}

	if (workStr[0])
	{
		SetStatusText(hDlg, workStr);
		EnableRWControls(TRUE);
		return;
	}

	// If this is a Select tag response, there is no data
	if (response.dataLength == 0)
	{
		if (advancedDialog)
		{
			// Update the count on the advanced dialog
			if (advancedRead)
			{
				TCHAR workStr[256];
				TCHAR tagType[64];

				if (response.lastError == RESP_SELECT_TAG_PASS)
				{
					MakeTagString(&response, tagData); //Save the tag ID

					GetTagTypeStr(response.tagType, tagType);
					wsprintf(workStr, TEXT("%s - %d bytes per block"), tagType, BytesPerBlock(&response));
				}
				else
				{
					workStr[0] = 0;
				}
				Static_SetText(GetDlgItem(hDlg, IDC_NUM_BYTES_READ), workStr);
			}

			HandleAdvResponse(scanner, &response);
		}
		else
		{
			// Add the tag type/ID to the list box
			AddTagInfo(&response, hDlg);
		}
		return;
	}

	// Now we have a response struct and there is data as well, allocate space for the data
	// and retrieve it
	scannerDataSize = response.dataLength;
	scannerData = (PBYTE) LocalAlloc(LPTR, scannerDataSize);
	if (scannerData)
	{
		// Now call Scan API to get the data
		if (CheckApiCall(parentWindow, TEXT("ScanRFIDGetData"), ScanRFIDGetData(scanner, &response, 
			scannerData, &scannerDataSize)) == SR_SUCCESS)
		{
			UpdateScanInfo(hDlg, scannerData, MIN(response.dataLength, SCAN_BUF_SIZE));

			// On the advanced dialog, also display the tag ID
			if (advancedDialog)
			{
				Edit_GetText(GetDlgItem(advancedDialog, IDC_STATUS), workStr, 
					sizeof(workStr) / sizeof(TCHAR));
				wcscat(workStr, TEXT(". Tag ID is "));
				wcscat(workStr, tagData);
				Edit_SetText(GetDlgItem(advancedDialog, IDC_STATUS), workStr);
			}
		}

		LocalFree(scannerData);
	}

	// Enable controls so we can do another command
	EnableRWControls(TRUE);
}


void
MakeTagString(PRFID_RESPONSE response, LPTSTR tagStr)
{
	ULONG index;
	TCHAR workStr[4];

	// Make the formatted tag data
	wsprintf(tagStr, TEXT("%02X:"), response->tagType);

	// For now, just return all ten possible tag bytes, might want to change this
	// to return the correct number based on the tag type
	for (index = 0; index < MAX_TAG_ID_SIZE; index++)
	{
		wsprintf(workStr, TEXT("%02X"), response->tagID[index]);
		wcscat(tagStr, workStr);
	}
}


void AddTagInfo(PRFID_RESPONSE response, HWND hDlg)
{
	PRFID_RESPONSE allocResponse;
	TCHAR tagData[(MAX_TAG_ID_SIZE + 1) * 2 + 2];	// format: <Tag Type>:<Tag ID>
	ULONG index;
	ULONG numItems;
	HWND listBox = GetDlgItem(hDlg, IDC_TAG_LIST);

	// If the Select tag succeeded, then save the tag type and id
	if (response->lastError != RESP_SELECT_TAG_PASS)
	{
		if (response->lastError == RESP_SELECT_TAG_FAIL)
		{
			SetStatusText(hDlg, TEXT("Found %d tag(s) in range"), ListBox_GetCount(listBox));
			Button_Enable(GetDlgItem(hDlg, IDC_SELECT_TAGS), TRUE);	// We can do another select now
			Button_Enable(GetDlgItem(hDlg, IDC_CLEAR_TAGS), ListBox_GetCurSel(GetDlgItem(hDlg, 
				IDC_TAG_LIST)) != LB_ERR);
		}
		return;
	}

	// Make a persistent copy of the response
	allocResponse = (PRFID_RESPONSE) LocalAlloc(LPTR, sizeof(RFID_RESPONSE));
	if (!allocResponse)
	{
		return;
	}

	memcpy(allocResponse, response, sizeof(RFID_RESPONSE));

	// Construct a string from the tag type and tag id and add it to the list box
	MakeTagString(allocResponse, tagData);

	// If the string already exists, don't add it again, just select it
	if ((index = ListBox_FindStringExact(listBox, 0, tagData)) != LB_ERR)
	{
		ListBox_SetCurSel(listBox, index);
		LocalFree(allocResponse);
	}
	else
	{
		ListBox_AddString(listBox, tagData);

		// This is kludgy but we don't know where our new string has been placed since
		// the list is sorted.  So we walk through looking for an item that has no ItemData.
		// This must be our new item so we store our response struct there
		numItems = ListBox_GetCount(listBox);
		for (index = 0; index < numItems; index++)
		{
			if (ListBox_GetItemData(listBox, index) == 0)
			{
				ListBox_SetItemData(listBox, index, allocResponse);

				// And select the new item
				ListBox_SetCurSel(listBox, index);
				Button_Enable(GetDlgItem(hDlg, IDC_CLEAR_TAGS), TRUE);
				if (!looping)
				{
					Button_Enable(GetDlgItem(hDlg, IDC_READ_TAG), TRUE);
				}
				break;
			}
		}
	}
}


void
EnableControls(BOOL enable)
{
	BOOL isSelection;

	if (!enable)
	{
		ClearDisplay(mainDialog);
	}

	isSelection = (ListBox_GetCurSel(GetDlgItem(mainDialog, IDC_TAG_LIST)) != LB_ERR);

	Button_Enable(GetDlgItem(mainDialog, IDC_SELECT_TAGS), enable);
	Button_Enable(GetDlgItem(mainDialog, IDC_CLEAR_TAGS), isSelection);
	Button_Enable(GetDlgItem(mainDialog, IDC_READ_TAG), enable && isSelection);	// Only if an item is selected

	if (advancedDialog)
	{
		Button_Enable(GetDlgItem(advancedDialog, IDC_READ_TAG), enable);
		Button_Enable(GetDlgItem(advancedDialog, IDC_WRITE_TAG), enable &&
			Edit_GetTextLength(GetDlgItem(advancedDialog, IDC_WRITE_DATA)));
	}
}


void
SetStatusText(HWND parentWindow, LPTSTR statusText, ...)
{
	TCHAR statusBuf[1024];
	HWND statusField = GetDlgItem(parentWindow, IDC_STATUS);

	if (parentWindow)
	{
		if (statusText)
		{
			void *pArguments = (void *) ((BYTE*)&statusText+sizeof(statusText));
			wvsprintf(statusBuf, statusText, (va_list)pArguments);
			Edit_SetText(statusField, statusBuf);
		}
		else
		{
			Edit_SetText(statusField, TEXT(""));
		}
	}
}


// Free the item data associated with each list element then clear the 
// list box
void
ClearDisplay(HWND parentWindow)
{
	DWORD numItems;
	DWORD listItemData;
	DWORD index;
	HWND tagList = GetDlgItem(parentWindow, IDC_TAG_LIST);
	HWND dataField = GetDlgItem(parentWindow, IDC_READ_DATA);
	HWND statusField = GetDlgItem(parentWindow, IDC_STATUS);

	numItems = ListBox_GetCount(tagList);
	for (index = 0; index < numItems; index++)
	{
		listItemData = ListBox_GetItemData(tagList, index);
		if (listItemData)
		{
			LocalFree((PVOID) listItemData);
		}
	}

	// Clear count of bytes in data field
	scanDataSize = 0;

	ListBox_ResetContent(tagList);
	Edit_SetText(dataField, TEXT(""));
	Edit_SetText(statusField, TEXT(""));
}


// Display/hide the input panel
BOOL ShowSip(BOOL show)
{
#ifdef WIN32_PLATFORM_PSPC
   BOOL returnValue = FALSE;
   SIPINFO si;

   memset(&si, 0, sizeof(si));
   si.cbSize = sizeof(si);
   if (SHSipInfo(SPI_GETSIPINFO, 0, &si, 0)) 
   {
	  returnValue = (si.fdwFlags & SIPF_ON);
      if (show)
	  {
		  si.fdwFlags |= SIPF_ON;
	  }
	  else
	  {
		  si.fdwFlags &= ~SIPF_ON;
	  }
      SHSipInfo(SPI_SETSIPINFO, 0, &si, 0);
   }

   return returnValue;
#else
   return FALSE;
#endif
}


void
DoCheckMenuItem(DWORD item, BOOL check)
{
	HMENU menu;

#if defined(WIN32_PLATFORM_PSPC)
	menu = SHGetSubMenu(hwndCB, ID_MAIN_RFID)
	CheckMenuItem(menu, item, check ? MF_CHECKED : MF_UNCHECKED);
#else
	menu = CommandBar_GetMenu(hwndCB, 0);
	CheckMenuItem(menu, item, check ? MF_CHECKED : MF_UNCHECKED);
#endif
}


void
ToggleLoopMode(void)
{
	if ((loopMode) && (looping))
	{
		EnableLooping(FALSE);
	}

	loopMode = !loopMode;

#ifdef WIN32_PLATFORM_PSPC
	DoCheckMenuItem(ID_RFID_LOOP_MODE, loopMode);
#else
	DoCheckMenuItem(IDM_RFID_LOOP_MODE, loopMode);
#endif
}


void
EnableLooping(BOOL enable)
{
	if (enable)
	{
		Button_SetText(GetDlgItem(mainDialog, IDC_SELECT_TAGS), TEXT("Stop Looping"));
		looping = TRUE;
	}
	else
	{
		Button_SetText(GetDlgItem(mainDialog, IDC_SELECT_TAGS), TEXT("Select Tags"));
		looping = FALSE;
		CheckApiCall(parentWindow, TEXT("ScanAbort"), ScanAbort(MyScanner));

		// Enable the read button if there are tags avail
		if (ListBox_GetCurSel(GetDlgItem(mainDialog, IDC_TAG_LIST)) != LB_ERR)
		{
			Button_Enable(GetDlgItem(mainDialog, IDC_READ_TAG), TRUE);
		}
	}
}


void
HandleAdvResponse(HANDLE scanner, PRFID_RESPONSE response)
{
	TCHAR statusStr[256];
	TCHAR tagTypeStr[16];
	TCHAR actionStr[16];

	// If this is a select response, send the read or write command
	if (response->lastError == RESP_SELECT_TAG_PASS)
	{
		if (advancedRead)
		{
			SendReadRequest(MyScanner, response);		// Send Read data cmd
		}
		else
		{
			SendWriteRequest(MyScanner, response);		// Send Write data cmd
		}
	}
	else
	{
		if (advancedRead)
		{
			wcscpy(actionStr, TEXT("read"));
		}
		else
		{
			wcscpy(actionStr, TEXT("write"));
		}

		if (gTagType != TAG_AUTO_DETECT)
		{
			GetTagTypeStr(gTagType, tagTypeStr);		
			wsprintf(statusStr, TEXT("No tag of type %s (%d) found to %s"), tagTypeStr, gTagType, actionStr);
		}
		else
		{
			wsprintf(statusStr, TEXT("No tag found to %s"), actionStr);
		}

		SetStatusText(advancedDialog, statusStr);
		EnableRWControls(TRUE);
	}
}


void
UpdateBlockInfo(HWND hDlg)
{
	TCHAR workStr[256];

	// Save block info
	Edit_GetText(GetDlgItem(hDlg, IDC_START_BLOCK), workStr, sizeof(workStr) / sizeof(TCHAR));
	StartBlock = _wtoi(workStr);
	Edit_GetText(GetDlgItem(hDlg, IDC_NUM_BLOCKS), workStr, sizeof(workStr) / sizeof(TCHAR));
	NumBlocks = _wtoi(workStr);
}


ULONG
BytesPerBlock(PRFID_RESPONSE response)
{
	ULONG blockSize;
	PUCHAR id = response->tagID;

	switch (response->tagType)
	{
		case TAG_ISO_15693:
			// Special case for my-d SRF55VxxP
			if ((id[0] == 0x60) && (id[1] == 0x05))
			{
				blockSize = 8;
			}
			else
			{
				blockSize = 4;
			}
			break;

		case TAG_ISO14443A:	
			blockSize = 16;
			break;

		case TAG_ISO14443B:		
			blockSize = 2;
			break;

		case TAG_PICOTAG:		
			blockSize = 8;
			break;

		// case TAG_I_CODE1:		
		// case TAG_TAG_IT_HF:	
		// case TAG_RFU:			
		// case TAG_GEMWAVE_C210:	
		default:
			blockSize = 4;
			break;
	}

	return blockSize;
}


void
GetTagTypeStr(UCHAR tagType, LPTSTR tagTypeStr)
{
	switch (tagType)
	{
		case TAG_ISO_15693:
			wcscpy(tagTypeStr, TEXT("ISO15693"));
			break;

		case TAG_ISO14443A:	
			wcscpy(tagTypeStr, TEXT("ISO14443-A"));
			break;

		case TAG_ISO14443B:		
			wcscpy(tagTypeStr, TEXT("ISO14443-B"));
			break;

		case TAG_PICOTAG:		
			wcscpy(tagTypeStr, TEXT("PicoTag"));
			break;

		case TAG_TAG_IT_HF:		
			wcscpy(tagTypeStr, TEXT("Tag-it HF"));
			break;

		case TAG_I_CODE1:		
			wcscpy(tagTypeStr, TEXT("I Code1"));
			break;

		case TAG_RFU:		
			wcscpy(tagTypeStr, TEXT("RFU"));
			break;

		case TAG_GEMWAVE_C210:		
			wcscpy(tagTypeStr, TEXT("Gemwave C210"));
			break;

		default:
			wcscpy(tagTypeStr, TEXT("Unknown"));
			break;
	}
}