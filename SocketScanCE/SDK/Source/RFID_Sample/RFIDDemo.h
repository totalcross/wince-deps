
#if !defined(AFX_RFID_DEMO_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_)
#define AFX_RFID_DEMO_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#define WM_DEMO_SCAN			WM_USER + 100 // Used to start a select tag by SocketScan
#define NUM_TAG_TYPES			9
#define DEFAULT_TAG_TYPE		0

// The name of the SocketScan application and its window
#define SOCKET_SCAN_WIN_NAME	TEXT("SocketScan")
#define SOCKET_EXIT_WAIT_TIME	5000
#define SOCKET_SCAN_EXIT_EVENT	TEXT("SOCKET_SCAN_EXIT")
#define RFID_DEMO_EXIT_EVENT	TEXT("RFID_DEMO_EXIT")

#define MIN(x,y)	x<y ? x : y

#endif // !defined(AFX_RFID_DEMO_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_)
