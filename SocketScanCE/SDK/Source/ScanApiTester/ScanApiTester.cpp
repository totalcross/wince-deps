/**************************************************************************************

 Copyright (C) 1999, 2000 by Socket Communications, All Rights Reserved.

   THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
   ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
   PARTICULAR PURPOSE.

 MODULE:		ScanApiTester.cpp

 PURPOSE:		Main CPP source file for the Socket Scanner API Tester.
				
 DESCRIPTION:	This program demonstrates the use of the Socket ScanAPI.
				
 COMMENTS:		Initial coding on 10/8/99 by Gary Cuevas - most code is
				wizard-generated.

				Added some conditional UI code for compiling on Pocket PC
				on 12/3/00 by Gary Cuevas


****************************************************************************************/

#include "stdafx.h"
#include "ScanApiTester.h"
#include "ScanApi.h"
#include <commctrl.h>
#include <commdlg.h>

// A few extra includes for Pocket PC...

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

#include <windows.h>
#include "newres.h"
#include <sipapi.h>
#include <windowsx.h>
#include <commctrl.h>
#include <aygshell.h>
#pragma comment(lib, "aygshell.lib")
#include "resource.h" 

#endif

#define MAX_LOADSTRING 100
#define MAX_BUF 256

// Messages needed for our scanner application...

#define WM_INSERTION	WM_USER + 1 // the message we want on device insertions
#define WM_REMOVAL		WM_USER + 2 // the message we want on device removals
#define WM_SCANNERDATA	WM_USER + 3 // the message we want when data is available
#define WM_CHS_STATUS	WM_USER + 4 // the message we want when CHS status changes

#define MAX_SCANNERS	10			// The max number of scanners that can be connected at once

// define to enable the setting of parameters on an SDIO scanner or ISC scanner 
//#define ISC_SCANNER


// Global Variables:
HINSTANCE			hInst;			// The current instance
HWND				hwndCB;			// The command bar handle

// The Scanner Handle for our application, & etc...

HANDLE		ScannerList[MAX_SCANNERS];
HANDLE		MyScanner = 0;			// Our scanner handle
BOOL		APIInitialized = FALSE;	// Tracks if ScanInit succeeded
SCANDEVINFO	ScanDevInfo;			// To get info about the scanner

// For tracking scanner sounds

GRS_TYPE CurrentSound = GRS_MESSAGEBEEP; // This is the default
TCHAR	 CurrentWav[128] = TEXT("");	 // Again the default
TCHAR	 TempFilename[128];				 // for GetOpenFileName


// The number of symbol types reported by ISC type cards
#define NUM_SYMBOL_TYPES	28

// Table for matching barcode IDs to names
const SYMBOL_TYPES symbolTypes[NUM_SYMBOL_TYPES] =
{
	0x01, TEXT("Code 39"), 
	0x02, TEXT("Codabar"), 
	0x03, TEXT("Code 128"), 
	0x04, TEXT("Discrete 2 of 5"), 
	0x05, TEXT("IATA 2 of 5"), 
	0x06, TEXT("Interleaved 2 of 5"), 
	0x07, TEXT("Code 93"), 
	0x08, TEXT("UPC A"), 
	0x09, TEXT("UPC E0"), 
	0x0A, TEXT("EAN 8"),
	0x0B, TEXT("EAN 13"), 

	0x0E, TEXT("MSI Plessey"), 
	0x0F, TEXT("EAN 128"), 
	0x10, TEXT("UPC E1"), 
	0x11, TEXT("PDF417"), 

	0x15, TEXT("Trioptic Code 39"), 
	0x16, TEXT("Bookland EAN"), 
	0x17, TEXT("Coupon Code"), 

	0x48, TEXT("UPC A w/ 2 supps."), 
	0x49, TEXT("UPC E0 w/ 2 supps."), 
	0x4A, TEXT("EAN 8 w/ 2 supps."), 
	0x4B, TEXT("EAN 13 w/ 2 supps."), 

	0x50, TEXT("UPC E1 w/ 2 supps."), 

	0x88, TEXT("UPC A w/ 5 supps."), 
	0x89, TEXT("UPC E0 w/ 5 supps."), 
	0x8A, TEXT("EAN 8 w/ 5 supps."), 
	0x8B, TEXT("EAN 13 w/ 5 supps."), 
	0x90, TEXT("UPC E1 w/ 5 supps."), 
 
};

void    MapSymbolTypeToText(TCHAR symbolType, TCHAR *symbolTypeText);

void    MapSymbolTypeToTextISCI(TCHAR symbolType, TCHAR *symbolTypeText);

// The number of symbol types reported by ISC type cards
#define NUM_SYMBOL_TYPES_ISCI	32

    //-----------------------------------------------------------------------------
    //  HHP Symbology ID characters
    //-----------------------------------------------------------------------------
const SYMBOL_TYPES symbolTypesISCI[NUM_SYMBOL_TYPES_ISCI] =
{
    'z', TEXT("AZTEC"),         
    'z', TEXT("MESA"),          
    'a', TEXT("CODABAR"),       
    'h', TEXT("CODE11"),
    'j', TEXT("CODE128"),
    'b', TEXT("CODE39"),
    'l', TEXT("CODE49"),
    'i', TEXT("CODE93"),
    'y', TEXT("COMPOSITE"),
    'u', TEXT("DATAMATRIX"),
    'd', TEXT("EAN"),
    'e', TEXT("INT25"),
    'x', TEXT("MAXICODE"),
    'R', TEXT("MICROPDF"),
    'r', TEXT("PDF417"),
    'P', TEXT("POSTNET"),
    'o', TEXT("OCR"),
    's', TEXT("QR"),
    'y', TEXT("RSS"),
    'c', TEXT("UPC"),
    'j', TEXT("ISBT"),
    'B', TEXT("BPO"),
    'C', TEXT("CANPOST"),
    'A', TEXT("AUSPOST"),
    'f', TEXT("IATA25"),
    'q', TEXT("CODABLOCK"),
    'J', TEXT("JAPOST"),
    'L', TEXT("PLANET"),
    'K', TEXT("DUTCHPOST"),
    'g', TEXT("MSI"),
    'T', TEXT("TLC39"),
    'm', TEXT("MATRIX25"),

};


// For keeping track of CHS state

BOOL	CHSDriverLoaded = FALSE;

// Foward declarations of functions included in this code module:

ATOM				MyRegisterClass	(HINSTANCE hInstance, LPTSTR szWindowClass);
BOOL				InitInstance	(HINSTANCE, int);
LRESULT CALLBACK	WndProc			(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About			(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	Choose			(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	Sound			(HWND, UINT, WPARAM, LPARAM);
BOOL				AddScanner		(HANDLE scanner);
BOOL				RemoveScanner	(HANDLE scanner);
void				ChooseScanner	(HWND hWnd);
void				NameScanner		(SCANNER_TYPE scannerType, LPTSTR scannerName);

// WinMain is just standard "wizard-generated code"...

int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_SCANAPITESTER);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}

// MyRegisterClass is just "wizard-generated code"...

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
	WNDCLASS	wc;

    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WndProc;
    wc.cbClsExtra		= 0;
    wc.cbWndExtra		= 0;
    wc.hInstance		= hInstance;
    wc.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SCANAPITESTER));
    wc.hCursor			= 0;
    wc.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName		= 0;
    wc.lpszClassName	= szWindowClass;

	return RegisterClass(&wc);
}

//
//  FUNCTION: CheckApiCall()
//
//  PURPOSE: Reports ScanAPI errors to the user.
//
//  COMMENTS:
//
//    Generates a dialog box explaining the SCAN_ERROR received when calling an
//    API function. Owner is the owner of the dialog box. lpFuncName is the name
//    of the API function. MyApiResult is the error code to be examined. If the
//    MyApiResult parameter is SR_SUCCESS, this function reports no error. The
//    result is passed back to the caller.
//
SCAN_RESULT CheckApiCall(HWND Owner, LPCTSTR lpFuncName, SCAN_RESULT MyApiResult)
{
	TCHAR ErrText[MAX_BUF];
	TCHAR ErrTranslation[MAX_BUF];
	int	ErrTranslationSize = sizeof(ErrTranslation);
	SCAN_RESULT Result;
	SCAN_RESULT TranslateResult;

	// save function return code...
	Result = MyApiResult;

	// report error if it is not SR_SUCCESS...
	if (Result != SR_SUCCESS)
	{
		// if ScanErrToText fails, report both failures...
		if ((TranslateResult = ScanErrToText(Result, ErrTranslation, &ErrTranslationSize)) != SR_SUCCESS)
		{
			wsprintf(ErrText, TEXT("A call to %s failed with error code %d."), lpFuncName, Result);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST);
			
			wsprintf(ErrText, TEXT("Also, an error occured attempting to translate error code %d using ScanErrToText."), TranslateResult);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST);
		}
		else
		{
			wsprintf(ErrText, TEXT("A call to %s failed with error code %d: %s"), lpFuncName, Result, ErrTranslation);
			MessageBox(Owner, ErrText, TEXT("ScanAPI Error"), MB_ICONEXCLAMATION | MB_TOPMOST);
		}
	}
	// give the result back to the caller...
	return (Result);
}

// InitInstance has been modified to make the call to the ScanInit() function. It is
// done here because we want the main window to be created first - as soon as ScanInit()
// is called, our main window will begin to receive WM_INSERTION messages if scanning
// devices are present, so we want the main window to be fully created and ready to
// rock and roll when those messages come in...

//
//  FUNCTION: InitInstance(HANDLE, int)
//
//  PURPOSE: Saves instance handle and creates main window
//
//  COMMENTS:
//
//    In this function, we save the instance handle in a global variable and
//    create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND	hWnd;
	TCHAR	szTitle[MAX_LOADSTRING];			// The title bar text
	TCHAR	szWindowClass[MAX_LOADSTRING];		// The window class name

	hInst = hInstance;		// Store instance handle in our global variable
	// Initialize global strings
	LoadString(hInstance, IDC_SCANAPITESTER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance, szWindowClass);

	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);

	// Clear our list of scanners
	memset(ScannerList, 0, sizeof(ScannerList));

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

	hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0, 26, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
#else

	hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		0, 0, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

#endif

	if (!hWnd)
	{	
		return FALSE;
	}

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

#else

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	if (hwndCB)
		CommandBar_Show(hwndCB, TRUE);

#endif

	// Enable support for multiple scanners connected simultaneously

	if (CheckApiCall(hWnd, TEXT("ScanEnableMultiScanner"), ScanEnableMultiScanner()) != SR_SUCCESS)
	{
		// get rid of the main window and return FALSE...
		DestroyWindow(hWnd);
		return FALSE;
	}
	// call ScanInit...

	if (CheckApiCall(hWnd, TEXT("ScanInit"), ScanInit(hWnd, WM_INSERTION, WM_REMOVAL)) != SR_SUCCESS)
	{
		// get rid of the main window and return FALSE...
		DestroyWindow(hWnd);
		return FALSE;
	}
	else
		// set this so we won't call ScanDeinit() needlessly...
		APIInitialized = TRUE;

	return TRUE;
}

//  Most of the interesting API calls happen in this function.

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId;
	int wmEvent;
	int ScannerDataSize;
	TCHAR ScannerData[MAX_BUF];
	TCHAR ErrTxt[MAX_BUF];
	HANDLE targetScanner;
    SCANDEVINFO	tmpScanDevInfo;
	TCHAR buffer[32];
	HWND chooseWindow;
	
	switch (message) 
	{
		case WM_COMMAND:

			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				// I'm using two sets of identifiers here to support the
				// different control IDs used with Pocket PC and HPC menu items...
				case ID_HELP_ABOUT:
				case IDM_HELP_ABOUT:

				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;

				case ID_FILE_CHOOSE:
				case IDM_FILE_CHOOSE:

				   ChooseScanner(hWnd);
				   break;

				case ID_FILE_EXIT:
				case IDM_FILE_EXIT:

				   DestroyWindow(hWnd);
				   break;

				case ID_FILE_SCAN:
				case IDM_FILE_SCAN:

					if (MyScanner != NULL)
					{
						// don't call if the scanner doesn't support soft trigger ...
						if (!ScanDevInfo.fSoftwareTrigger)
						{
							MessageBeep(0);
							break;
						}

						// call the API and check return code...
						CheckApiCall(hWnd, TEXT("ScanTrigger"), ScanTrigger(MyScanner));
					}
					break;

				case ID_FILE_ABORTSCAN:
				case IDM_FILE_ABORTSCAN:

					if (MyScanner != NULL)
					{
						// don't call if the scanner doesn't support soft abort...
						if (!ScanDevInfo.fSoftwareAbortTrigger)
						{
							MessageBeep(0);
							break;
						}

						// call the API and check return code...
						CheckApiCall(hWnd, TEXT("ScanAbort"), ScanAbort(MyScanner));
					}
					break;

				case ID_FILE_SOUND:
				case IDM_FILE_SOUND:

					DialogBox(hInst, (LPCTSTR)IDD_SOUND, hWnd, (DLGPROC)Sound);
					break;

				case ID_FILE_TOGGLE_CHS:
				case IDM_FILE_TOGGLE_CHS:
					if (!CHSDriverLoaded)
					{
						// Start the CHS driver
						if (CheckApiCall(hWnd, TEXT("ScanEnableCHS"), ScanEnableCHS(hWnd, WM_CHS_STATUS)) == SR_SUCCESS)
						{
							CHSDriverLoaded = TRUE;
							// We will report success when the CHS driver sends WM_INSERTION
						}
					}
					else
					{
						// Stop the CHS driver
						CheckApiCall(hWnd, TEXT("ScanDisableCHS"), ScanDisableCHS(MyScanner));
						CHSDriverLoaded = FALSE;
					}

					break;

				default:

				   return DefWindowProc(hWnd, message, wParam, lParam);
			}

			break;

		case WM_CHS_STATUS:
			switch (wParam)
			{
				case CHS_BTLINK_DROPPED:
				wsprintf(ErrTxt, TEXT("CHS_BTLINK_DROPPED"));
				MessageBox(hWnd, ErrTxt, TEXT("CHS connection state"), MB_ICONINFORMATION | MB_TOPMOST);
				break;	

				case CHS_BTLINK_ABORTED:
				wsprintf(ErrTxt, TEXT("CHS_BTLINK_ABORTED"));
				MessageBox(hWnd, ErrTxt, TEXT("CHS connection state"), MB_ICONINFORMATION | MB_TOPMOST);

				break;	

				case CHS_BTLINK_RE_ESTABLISHED:
				wsprintf(ErrTxt, TEXT("CHS_BTLINK_RE_ESTABLISHED"));
				MessageBox(hWnd, ErrTxt, TEXT("CHS connection state"), MB_ICONINFORMATION | MB_TOPMOST);
				break;	
				
			}
			break;
		case WM_CREATE:

			// If we are compiling for a Pocket PC, we do the menu much
			// differently than for the HPC-class devices...

#if defined(WIN32_PLATFORM_PSPC) && (_WIN32_WCE >= 300)

			//Create the menubar.
			SHMENUBARINFO mbi;

			memset (&mbi, 0, sizeof (SHMENUBARINFO));
			mbi.cbSize     = sizeof (SHMENUBARINFO);
			mbi.hwndParent = hWnd;
			mbi.nToolBarId = IDR_MENUBAR3;
			mbi.hInstRes   = hInst;
			mbi.nBmpId     = 0;
			mbi.cBmpImages = 0;  

			if (!SHCreateMenuBar(&mbi))
			{
				MessageBox(hWnd, L"SHCreateMenuBar Failed", L"Error", MB_OK | MB_TOPMOST);

				return -1;
			}
      
			hwndCB = mbi.hwndMB;

#else

			// Create a command bar...
			hwndCB = CommandBar_Create(hInst, hWnd, 1);			
			CommandBar_InsertMenubar(hwndCB, hInst, IDM_MENU, 0);
			CommandBar_AddAdornments(hwndCB, 0, 0);

#endif

			break;

		case WM_INSERTION:

			// do an hourglass...
			SetCursor(LoadCursor(NULL, IDC_WAIT));

			targetScanner = (HANDLE) lParam;	// The scanner handle is passed in

			// Add this scanner to our list
			if (AddScanner(targetScanner))
			{
				// call API and check return code...
				if (CheckApiCall(hWnd, TEXT("ScanOpenDevice"), ScanOpenDevice(targetScanner)) == SR_SUCCESS)
				{
					// If we do not have a current scanner, use this one
					if (MyScanner == NULL)
					{
						MyScanner = targetScanner;

						// request device capabilities of the scanner. ScanDevInfo always has
						// the capabilities of the current scanner
						memset(&ScanDevInfo, 0, sizeof(SCANDEVINFO));

						// don't forget to set the StructSize member or 
						// you'll get SR_INVALID_STRUCTURE
						ScanDevInfo.StructSize = sizeof(SCANDEVINFO);

						// call API and check return code...
						CheckApiCall(hWnd, TEXT("ScanGetDevInfo"), ScanGetDevInfo(MyScanner, &ScanDevInfo));
					}

					// request data events for the new scanner...
					// call API and check return code...
					CheckApiCall(hWnd, TEXT("ScanRequestDataEvents"), ScanRequestDataEvents(targetScanner, hWnd, WM_SCANNERDATA));

					// Here, we must either set the good-read sound to something we want, or
					// set our globals to the default of a newly opened scanner...
					lstrcpy(CurrentWav, TEXT(""));
					CurrentSound = GRS_MESSAGEBEEP;	
#ifdef ISC_SCANNER
					{
						BYTE p=0x23;       // UPC-E preamble enable/disable (refer to manual for other codes)
						UINT16 val=0x0000; // disable preamble; use val=0x8000 for permanent (high order bit set)
						int result=0;
						
						CheckApiCall(hWnd, TEXT("ScanParamSend"), ScanParamSend(targetScanner, &p, sizeof(BYTE), (PBYTE)&val, sizeof(UINT16)));
						// check result for error
					}
#endif		

					// confirmation we have an open scanning device...
					MessageBeep(0);
				}
			}
			else
			{
				MessageBox(hWnd, TEXT("An error occurred adding the new scanner to the scanner list."), 
					TEXT("ScanAPITester"), MB_ICONERROR | MB_TOPMOST);
			}

			SetCursor(NULL);
			
			break;

		case WM_REMOVAL:

			targetScanner = (HANDLE) lParam;	// The scanner handle is passed in

			// If the choose scanner dlg is on-screen, close it
			chooseWindow = FindWindow(TEXT("DIALOG"), TEXT("Choose Scanner"));
			if (chooseWindow)
			{
				SendMessage(chooseWindow, WM_COMMAND, IDCANCEL, 0);
			}

			// make the API call...
			CheckApiCall(hWnd, TEXT("ScanCloseDevice"), ScanCloseDevice(targetScanner));

			MessageBeep(0);
			
			// Remove the scanner handle from our list
			if (!RemoveScanner(targetScanner))
			{
				MessageBox(hWnd, TEXT("An error occurred removing the scanner from the scanner list."), 
					TEXT("ScanAPITester"), MB_ICONERROR | MB_TOPMOST);
			}
			break;

		case WM_SCANNERDATA:

			// get the size of the scanned data...
			ScannerDataSize = wParam;

			targetScanner = (HANDLE) lParam;	// The scanner handle is passed in
			
			// zero-fill the data buffer...
			memset(ScannerData, 0, sizeof(ScannerData));

			// call the API and check the return code...
			// call the API and check the return code...
			if (CheckApiCall(hWnd, TEXT("ScanGetData"), ScanGetData(targetScanner, (TCHAR *) &ScannerData, &ScannerDataSize)) == SR_SUCCESS)
			{
				// The symbol type of the last scan is stored in the scanner info so retrieve it
				// This works primarily for ISC and SDIO scanners
				memset(&tmpScanDevInfo, 0, sizeof(SCANDEVINFO));
				tmpScanDevInfo.StructSize = sizeof(SCANDEVINFO);
				if ( CheckApiCall(hWnd, TEXT("ScanGetDevInfo"), ScanGetDevInfo(targetScanner, &tmpScanDevInfo)) == SR_SUCCESS)
				{
				    memset(&buffer, 0, sizeof(buffer));

                    if (tmpScanDevInfo.ScannerType == SCANNER_ISCI)
					    MapSymbolTypeToTextISCI (tmpScanDevInfo.SymbolType, buffer);
				    else
						MapSymbolTypeToText (tmpScanDevInfo.SymbolType, buffer);
				}
				else
					strcpy((char *)buffer, "unable to retrieve info");

				// informational messagebox...
				wsprintf(ErrTxt, TEXT("Got a WM_SCANNERDATA message for scanner handle %d with a size of %d"), targetScanner, wParam); 
				MessageBox(hWnd, ErrTxt, TEXT("Status"), MB_TOPMOST);

				wsprintf(ErrTxt, TEXT("Barcode type %s. Scanned Data: %s"),buffer, ScannerData ); 
				
				MessageBox(hWnd, ErrTxt, TEXT("Received Data:"), MB_ICONINFORMATION | MB_TOPMOST);
				
				// Beware of treating the data as a simple string (as I did here) - some 
				// symbologies allow for embedded NULLs in the data stream - it would be best 
				// to perform some validation checks on the data before doing anything with it!
				//
				// Note also that the data you get back is UNICODE and in most cases it
				// will NOT be NULL terminated!
			}

			break;

		case WM_DESTROY:

			// call the API and check the return code...
			if (APIInitialized)
			{
				if (CHSDriverLoaded)
				{
					// Stop the CHS driver
					CheckApiCall(hWnd, TEXT("ScanDisableCHS"), ScanDisableCHS(MyScanner));
					CHSDriverLoaded = FALSE;
				}

				CheckApiCall(hWnd, TEXT("ScanDeinit"), ScanDeinit());
			}
			
			CommandBar_Destroy(hwndCB);
			PostQuitMessage(0);
			break;

		default:

			return DefWindowProc(hWnd, message, wParam, lParam);
   }

   return 0;
}

//
//  FUNCTION: GetWavFileName()
//
//  PURPOSE:  Gets the name of a .wav file using an Open File Dialog
//
//  COMMENTS:
//
//  On success, the name of the .wav file is copied to the TempFilename global.
//
//  As you may know, on a Palm PC the Open File Dialog only looks at "My Documents"
//  and it's subdirectories and, by default, there are no .wav files there. You can
//  drag some .wav files there with your Mobile Devices Explorer if you want.
//
BOOL GetWavFileName(HWND Owner)
{
	OPENFILENAME ofn;
	TCHAR Filter[] = TEXT("Wave Sound (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0");
	TCHAR Title[] = TEXT("Select a WAVE File");
	TCHAR InitDir[] = TEXT("\\Windows"); // gets ignored on a Palm PC

	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize =sizeof(ofn);
	ofn.hwndOwner = Owner;
	ofn.hInstance = hInst;
	ofn.lpstrFilter = (LPCTSTR) &Filter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = (LPTSTR) &TempFilename;
	ofn.nMaxFile = 128;
	ofn.lpstrTitle = (LPCTSTR) &Title;
	ofn.lpstrInitialDir = (LPTSTR) &InitDir;
	ofn.Flags = OFN_EXPLORER;
	return (GetOpenFileName(&ofn));
}

// Dialog proc for the Sound dialog.
LRESULT CALLBACK Sound(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;
	int wmId    = LOWORD(wParam); 
	int wmEvent = HIWORD(wParam); 

	switch (message)
	{
		case WM_INITDIALOG:

			// trying to center the Sound dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the Sound box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}

			// set up controls with current values...
			switch (CurrentSound)
			{
				case GRS_NONE:
					
					SendDlgItemMessage(hDlg, IDC_NO_SOUND, BM_SETCHECK, BST_CHECKED, 0);
					break;

				case GRS_MESSAGEBEEP:
				
					SendDlgItemMessage(hDlg, IDC_BEEP, BM_SETCHECK, BST_CHECKED, 0);
					break;

				case GRS_WAVFILE:

					SendDlgItemMessage(hDlg, IDC_PLAY_WAVFILE, BM_SETCHECK, BST_CHECKED, 0);
					SendDlgItemMessage(hDlg, IDC_WAVFILE_NAME, WM_SETTEXT, 0, (LONG) &CurrentWav);
					break;
			}

			lstrcpy((LPTSTR) &TempFilename, (LPCTSTR) &CurrentWav);
			return TRUE;

		case WM_COMMAND:

			switch (wmId)
			{
				case IDC_BROWSE:

					if (GetWavFileName(hDlg))
						// set the filename in the edit control...
						SendDlgItemMessage(hDlg, IDC_WAVFILE_NAME, WM_SETTEXT, 0, (LONG) &TempFilename);
					break;

				case IDOK:
					
					// return sound values...
					if (SendDlgItemMessage(hDlg, IDC_NO_SOUND, BM_GETCHECK, 0, 0) == BST_CHECKED)
						CurrentSound = GRS_NONE;
				    else if (SendDlgItemMessage(hDlg, IDC_BEEP, BM_GETCHECK, 0, 0) == BST_CHECKED)
						CurrentSound = GRS_MESSAGEBEEP;
					else
					{
						CurrentSound = GRS_WAVFILE;
						// copy TempFilename to our current sound file...
						lstrcpy((LPTSTR) &CurrentWav, (LPCTSTR) &TempFilename);
						SendDlgItemMessage(hDlg, IDC_WAVFILE_NAME, WM_GETTEXT, (UINT) &CurrentWav, sizeof(CurrentWav) / sizeof(TCHAR));
					}
					CheckApiCall(hDlg, TEXT("ScanSetGoodReadSound"), ScanSetGoodReadSound(MyScanner, CurrentSound, CurrentWav));
					
					// fall thru to end the dialog...

				case IDCANCEL:

					EndDialog(hDlg, LOWORD(wParam));
					return TRUE;
			}
			break;
	}
    return FALSE;
}


void
MapSymbolTypeToText(TCHAR symbolType, TCHAR *symbolTypeText)
{
	ULONG index;

	for (index = 0; index < NUM_SYMBOL_TYPES; index++)
	{
		if (symbolType == symbolTypes[index].symbolType)
		{
			wcscpy(symbolTypeText, symbolTypes[index].symbolTypeName);
			return;
		}
	}

	wcscpy(symbolTypeText, TEXT("n/a"));
}

void
MapSymbolTypeToTextISCI(TCHAR symbolType, TCHAR *symbolTypeText)
{
	ULONG index;

	for (index = 0; index < NUM_SYMBOL_TYPES; index++)
	{
		if (symbolType == symbolTypesISCI[index].symbolType)
		{
			wcscpy(symbolTypeText, symbolTypesISCI[index].symbolTypeName);
			return;
		}
	}

	wcscpy(symbolTypeText, TEXT("n/a"));
}

void 
HandleCHSStatusEvent(DWORD statusEvent)
{
	// CHS events could be handled here

	switch (statusEvent)
	{
		case CHS_BTLINK_DROPPED:
			break;

		case CHS_BTLINK_RE_ESTABLISHED:
			break;

		case CHS_BTLINK_ABORTED:
			break;
	}
}

//
//
// nothing but "wizard-generated code" appears below...
//
//

// Mesage handler for the About box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;

	switch (message)
	{
		case WM_INITDIALOG:
			// trying to center the About dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the About box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}
			return TRUE;

		case WM_COMMAND:
			if ((LOWORD(wParam) == IDOK) || (LOWORD(wParam) == IDCANCEL))
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}


// Mesage handler for the Choose Scanner dialog box.
LRESULT CALLBACK Choose(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt1;
	int DlgWidth, DlgHeight;	// dialog width and height in pixel units
	int NewPosX, NewPosY;
	LONG exStyle;
	ULONG index;
	HWND listControl;

	switch (message)
	{
		case WM_INITDIALOG:
			// trying to center the dialog
			if (GetWindowRect(hDlg, &rt1)) {
				GetClientRect(GetParent(hDlg), &rt);
				DlgWidth	= rt1.right - rt1.left;
				DlgHeight	= rt1.bottom - rt1.top ;
				NewPosX		= (rt.right - rt.left - DlgWidth)/2;
				NewPosY		= (rt.bottom - rt.top - DlgHeight)/2;
				
				// if the About box is larger than the physical screen 
				if (NewPosX < 0) NewPosX = 0;
				if (NewPosY < 0) NewPosY = 0;
				SetWindowPos(hDlg, 0, NewPosX, NewPosY,
					0, 0, SWP_NOZORDER | SWP_NOSIZE);
			}

			// Display the OK button
			exStyle = GetWindowLong(hDlg, GWL_EXSTYLE);
			SetWindowLong(hDlg, GWL_EXSTYLE, exStyle | WS_EX_CAPTIONOKBTN);

			// Populate the list of scanners
			listControl = GetDlgItem(hDlg, IDC_SCANNER_LIST);
			for (index = 0; index < MAX_SCANNERS; index++)
			{
				TCHAR scannerName[64];
				SCANDEVINFO scanDevInfo;
				int count;

				if (ScannerList[index] != NULL)
				{
					// request device capabilities of the scanner. 
					memset(&scanDevInfo, 0, sizeof(SCANDEVINFO));
					scanDevInfo.StructSize = sizeof(SCANDEVINFO);
					if (CheckApiCall(hDlg, TEXT("ScanGetDevInfo"), ScanGetDevInfo(ScannerList[index], 
						&scanDevInfo)) == SR_SUCCESS)
					{
						// Get a name for the scanner
						NameScanner(scanDevInfo.ScannerType, scannerName);

						// Add the name and scanner handle to the list
						SendMessage(listControl, LB_ADDSTRING, 0, (LPARAM) scannerName);
						count = SendMessage(listControl, LB_GETCOUNT, 0, 0);
						SendMessage(listControl, LB_SETITEMDATA, (WPARAM) count - 1, (LPARAM) ScannerList[index]);

						// If this is the current scanner, select it
						if (MyScanner == ScannerList[index])
						{
							SendMessage(listControl, LB_SETCURSEL, (WPARAM) count - 1, (LPARAM) count);
						}
					}
				}
			}

			return TRUE;

		case WM_COMMAND:
			if ((LOWORD(wParam) == IDOK) || (LOWORD(wParam) == IDCANCEL))
			{
				if (LOWORD(wParam) == IDOK)
				{
					int sel;

					listControl = GetDlgItem(hDlg, IDC_SCANNER_LIST);
					if ((sel = SendMessage(listControl, LB_GETCURSEL, 0, 0)) != LB_ERR)
					{
						// Save the selected scanner
						MyScanner = (HANDLE) SendMessage(listControl, LB_GETITEMDATA, sel, 0);
					}
				}
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}


BOOL				
AddScanner(HANDLE scanner)
{
	ULONG index;

	// Find an empty slot and record the handle
	for (index = 0; index < MAX_SCANNERS; index++)
	{
		if (ScannerList[index] == NULL)
		{
			ScannerList[index] = scanner;
			return TRUE;
		}
	}

	// Fell through, no slots avail so fail
	return FALSE;
}


BOOL
RemoveScanner(HANDLE scanner)
{
	ULONG index;

	// Find the scanner and remove it
	for (index = 0; index < MAX_SCANNERS; index++)
	{
		if (ScannerList[index] == scanner)
		{
			// Zero the entry
			ScannerList[index] = NULL;

			// If this is the current scanner we are removing , try and find another to use
			if (MyScanner == scanner)
			{
				for (index = 0; index < MAX_SCANNERS; index++)
				{
					if (ScannerList[index] != NULL)
					{
						MyScanner = ScannerList[index];
						return TRUE;
					}
				}

				// Couldn't find another scanner so set current scanner to NULL
				MyScanner = NULL;
				return TRUE;
			}
			else
			{
				// We removed the scanner and this isn't the current scanner
				return TRUE;
			}
		}
	}

	// Fell through, no slots avail so fail
	return FALSE;
}



void
ChooseScanner(HWND hWnd)
{
	DialogBox(hInst, (LPCTSTR)IDD_CHOOSE_SCANNER, hWnd, (DLGPROC) Choose);
}




void
NameScanner(SCANNER_TYPE scannerType, LPTSTR scannerName)
{
	switch (scannerType)
	{
		case SCANNER_CFCARD:
			wcscpy(scannerName, TEXT("In-Hand Scanner"));
			break;
		case SCANNER_WAND:
			wcscpy(scannerName, TEXT("Wand Scanner"));
			break;
		case SCANNER_GUN:
			wcscpy(scannerName, TEXT("Gun Scanner"));
			break;
		case SCANNER_ISCI:
			wcscpy(scannerName, TEXT("2D Scanner"));
			break;
		case SCANNER_CHS:
			wcscpy(scannerName, TEXT("Cordless Hand Scanner"));
			break;
		case SCANNER_SDIO:
			wcscpy(scannerName, TEXT("SDIO Scanner"));
			break;
		case SCANNER_RFID:
			wcscpy(scannerName, TEXT("RFID Scanner"));
			break;
		default:
			wcscpy(scannerName, TEXT("Unknown Scanner"));
			break;
	}
}
