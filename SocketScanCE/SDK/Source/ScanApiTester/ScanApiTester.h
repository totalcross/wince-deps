
#if !defined(AFX_SCANAPITESTER_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_)
#define AFX_SCANAPITESTER_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"


// a structure for keeping track of sound settings

struct tagSounds{
	int CurrentSound;
	TCHAR CurrentWavFile[256];
}SOUNDS;

typedef struct symbolTypes{
	TCHAR symbolType;
	TCHAR *symbolTypeName;
}SYMBOL_TYPES;


#endif // !defined(AFX_SCANAPITESTER_H__94725707_75C4_11D3_AB75_0008C78F1D83__INCLUDED_)
