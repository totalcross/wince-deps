//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by ScanApiTester.rc
//
#define IDS_APP_TITLE                   1
#define IDS_HELLO                       2
#define IDC_SCANAPITESTER               3
#define IDI_SCANAPITESTER               101
#define IDM_MENU                        102
#define IDD_ABOUTBOX                    103
#define IDD_SOUND                       104
#define IDD_CHOOSE_SCANNER              105
#define IDR_MENUBAR1                    106
#define IDM_MENUBAR2                    111
#define IDR_MENUBAR2                    112
#define IDR_MENUBAR3                    114
#define IDC_NO_SOUND                    1001
#define IDC_BEEP                        1002
#define IDC_PLAY_WAVFILE                1003
#define IDC_BROWSE                      1004
#define IDC_WAVFILE_NAME                1005
#define IDC_SCANNER_LIST                1006
#define IDM_FILE_EXIT                   40002
#define IDM_HELP_ABOUT                  40003
#define IDM_FILE_SCAN_1                 40006
#define IDM_FILE_SCAN                   40006
#define IDM_FILE_SCAN_2                 40007
#define IDM_FILE_SOUND_1                40008
#define IDM_FILE_SOUND_2                40009
#define IDM_FILE_SOUND                  40009
#define IDM_FILE_ERRTEST                40010
#define IDM_FILE_ABORTSCAN              40011
#define ID_FILE                         40012
#define IDS_CAP_FILE                    40014
#define ID_FILE_SCAN                    40015
#define ID_FILE_ABORTSCAN               40016
#define ID_FILE_SOUND                   40017
#define ID_FILE_EXIT                    40018
#define IDS_CAP_HELP                    40021
#define ID_HELP_ABOUT                   40022
#define ID_SKJHDSK                      40026
#define IDS_CAP_SKJHDSK                 40028
#define ID_SKJHDSK_SDLKJFSKD            40029
#define ID_SCAN                         40029
#define ID_SDFGJ                        40030
#define IDS_CAP_SDFGJ                   40032
#define IS_FILE                         40033
#define ID_ABORT_SCAN                   40034
#define ID_EXIT                         40035
#define ID_SOUND                        40036
#define ID_MAIN_HELP                    40037
#define IDS_HELP                        40038
#define ID_ABOUT                        40039
#define IDS_MAIN_FILE                   40041
#define ID_MAIN_FILE                    40042
#define ID_HELP                         40048
#define ID_TEST                         40051
#define IDS_CAP_TEST                    40053
#define ID_TEST_TEST                    40054
#define IDM_FILE_TOGGLE_CHS             40055
#define ID_FILE_TOGGLE_CHS              40056
#define IDM_FILE_CHOOSE                 40057
#define ID_FILE_CHOOSE                  40058

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        115
#define _APS_NEXT_COMMAND_VALUE         40059
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
