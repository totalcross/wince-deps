/**************************************************************************************

 Copyright (C) 1999 by Socket Communications, All Rights Reserved.

 MODULE:		ScanAPI.h

 PURPOSE:		Include file for the Socket Scanner API.
				
 DESCRIPTION:	Currently supports only one scanner.		
				
 COMMENTS:		Initial coding on 9/2/99 by Gary Cuevas
				Added  SymbolType 8/5/01 tjn
			
****************************************************************************************/

#ifdef SCANAPI_EXPORTS
#define SCANAPI_API __declspec(dllexport)
#else
#define SCANAPI_API __declspec(dllimport)
#endif

#ifdef __cplusplus
  extern "C" {
#endif

// Types of Socket Scanner products

enum SCANNER_TYPE {SCANNER_NONE = 0,		// no scanner
				   SCANNER_CFCARD,			// Socket In-Hand Scan Card
				   SCANNER_WAND,			// a wand
				   SCANNER_GUN,				// a gun
				   SCANNER_ISCI,			// an ISCI
				   SCANNER_CHS,				// a CHS (cordless hand scanner)
				   SCANNER_SDIO,			// an SDIO scanner
				   SCANNER_RFID,			// an RFID scanner
				   SCANNER_MAG_STRIPE,		// a Magnetic Stripe Reader
				   SCANNER_CRS				// a CRS (cordless ring scanner)
				};

// Types of good-read sounds

enum GRS_TYPE {GRS_NONE = 0,	// no good read sound
			   GRS_MESSAGEBEEP,	// play MessageBeep(0) on good read
			   GRS_WAVFILE};	// play user-supplied .WAV file on good read

// Types of CHS status events

enum CHS_EVENTS {CHS_BTLINK_DROPPED = 0,	// connection to CHS was lost
			     CHS_BTLINK_RE_ESTABLISHED,	// connection to CHS reestablished
			     CHS_BTLINK_ABORTED};		// reconnect attempts aborted by timeout

// Scanner device information structure

typedef struct tagSCANDEVINFO{
	DWORD			StructSize;					// size of the structure
	enum SCANNER_TYPE	ScannerType;				// gun, wand, or integrated card
	unsigned int	fHardwareTrigger :1;		// most likely a gun
	unsigned int	fSoftwareTrigger :1;		// most likely an integrated card
	unsigned int	fSoftwareAbortTrigger :1;	// most likely an integrated card
	unsigned int	fGoodReadLight :1;			// most likely a gun
	unsigned int	fGoodReadSound :1;			// most likely a gun
	TCHAR			SymbolType;					// Symbol type code from ISC scanner

} SCANDEVINFO, *LPSCANDEVINFO;

// RFID Scanner defines structs

#define MAX_TAG_ID_SIZE					10

// Tag types
#define TAG_AUTO_DETECT					0x00
#define TAG_ISO_15693					0x01
#define TAG_I_CODE1						0x02
#define TAG_TAG_IT_HF					0x03
#define TAG_ISO14443A					0x04
#define TAG_ISO14443B					0x05
#define TAG_PICOTAG						0x06
#define TAG_RFU							0x07
#define TAG_GEMWAVE_C210				0x08

// Response codes
#define	RESP_SELECT_TAG_PASS			0x14
#define RESP_SELECT_TAG_LOOP_ACTIVATE	0x1c
#define RESP_SELECT_TAG_FAIL			0x94
#define RESP_SELECT_TAG_LOOP_CANCEL		0x9c
#define RESP_READ_TAG_PASS				0x24
#define RESP_READ_TAG_FAIL				0xa4
#define RESP_WRITE_TAG_PASS				0x44
#define RESP_WRITE_TAG_FAIL				0xc4
#define RESP_BAD_CRC					0x81
#define RESP_FLAGS_DONT_MATCH_COMMAND	0x82
#define RESP_FLAGS_DONT_MATCH_TAG_TYPE	0x83
#define RESP_UNKNOWN_COMMAND			0x84
#define RESP_UNKNOWN_TAG_TYPE			0x85
#define RESP_INVALID_STARTING_BLOCK		0x86
#define RESP_INVALID_NUMBER_OF_BLOCKS	0x87
#define RESP_INVALID_MESSAGE_LENGTH		0x88

typedef struct tagRFID_RESPONSE{
		DWORD	lastError;						// Error code of last operation
		BYTE	tagType;						// Tag type from request or from response if auto
		BYTE	tagID[MAX_TAG_ID_SIZE];         // Tag ID from request or from response data if select tag
		DWORD	dataLength;						// Size in bytes of data (read), 0 = no data
} RFID_RESPONSE, *PRFID_RESPONSE;

// API return codes

enum SCAN_RESULT {SR_SUCCESS = 0,
				  SR_INVALID_WMINSERTION,
				  SR_INVALID_WMREMOVAL,
				  SR_PLUG_THREAD_FAILURE,
				  SR_DEVICE_THREAD_FAILURE,
				  SR_INVALID_SCANNER_HANDLE,
				  SR_OPEN_FAILURE,
				  SR_INVALID_WMSCANNERDATA,
				  SR_NO_DATA,
				  SR_BUFFER_TOO_SMALL,
				  SR_SCANNER_NOT_OPEN,
				  SR_INVALID_SOUND_TYPE,
				  SR_WAVFILE_NOT_FOUND,
				  SR_MEMORY_FAILURE,
				  SR_INVALID_ERR,
				  SR_TOO_MANY_USERS,
				  SR_NOT_INITIALIZED,
				  SR_DEVICE_FAILURE,
				  SR_INTERNAL_FAILURE,
				  SR_INVALID_STRUCTURE,
				  SR_SCANNER_REMOVED,
				  SR_UNSUPPORTED_FEATURE,
				  SR_INVALID_WMCHSSTATUS,
				  SR_NOT_CHS_DEVICE,
                  SR_WAIT_TIMEOUT_ERROR,
                  SR_SYMBOLOGY_NOT_SUPPORTED,
                  SR_SCANNER_BUSY};

// API Scanner Option Cmd Opcodes

enum SCAN_CMDOPCODE {ENABLE_SCANDATA_RECV_PROTOCOL = 0,
};

// Symbology Handling
enum SCANNER_SYMBOLOGY {
	// 1-D Barcodes
	SYMB_CODE11 = 0,         // 0
	SYMB_CODE128,
	SYMB_CODE39,
	SYMB_CODE49,
	SYMB_CODE93,
	SYMB_CODE39_FULL_ASCII,  // 5
	SYMB_TRIOPTIC_CODE39,
	SYMB_UPC_A,
	SYMB_UPC_E,
	SYMB_UPC_E0,
	SYMB_UPC_E1,             // 10
	SYMB_EAN_8,
	SYMB_EAN_13,
	SYMB_BOOKLAND_EAN,
	SYMB_UCC_EAN_128,
	SYMB_CODABAR,            // 15
	SYMB_CODABLOCK,
	SYMB_DISCRETE_2_5,
	SYMB_IATA_2_5,
	SYMB_INTERLEAVED_2_5,
	SYMB_ISBT,               // 20
	SYMB_MATRIX_2_5,
	SYMB_MSI_PLESSEY,
	SYMB_RSS14,
	SYMB_TLCODE39,

	// 2-D Barcodes
	SYMB_AUSTRALIAN_POSTAL,  // 25
	SYMB_AZTEC,
	SYMB_AZTEC_MESA,
	SYMB_BRITISH_POSTAL,
	SYMB_CANADIAN_POSTAL,
	SYMB_DATAMATRIX_CODE,    // 30
	SYMB_DUTCH_POSTAL,
	SYMB_JAPANESE_POSTAL,
	SYMB_MAXICODE,
	SYMB_MICROPDF,
	SYMB_OCR,                // 35
	SYMB_PDF,
	SYMB_QR_CODE,
	SYMB_US_PLANET_POSTAL,
	SYMB_US_POSTNET_POSTAL,
	SYMB_ALL_SYMBOLOGIES     // 40
};	

// Structure for symbologies with no specified min or max length.
typedef struct _tagScannerSymFlagsOnly
{
    DWORD   dwStructSize;                           // Set to sizeof( ScannerSymFlagsOnly_t );
    DWORD   dwMask;
    DWORD   dwFlags;

} ScannerSymFlagsOnly_t, *PScannerSymFlagsOnly_t;

// Structure for symbologies with min & max length.
typedef struct _tagScannerSymFlagsRange
{
    DWORD   dwStructSize;                           // Set to sizeof( ScannerSymFlagsRange_t );
    DWORD   dwMask;
    DWORD   dwFlags;
    DWORD   dwMinLen;
    DWORD   dwMaxLen;

} ScannerSymFlagsRange_t, *PScannerSymFlagsRange_t;

// API function prototypes

SCANAPI_API SCAN_RESULT ScanEnableMultiScanner(void);
SCANAPI_API SCAN_RESULT ScanInit(HWND hWnd, UINT wmInsertion, UINT wmRemoval);
SCANAPI_API SCAN_RESULT ScanOpenDevice(HANDLE hScanner);
SCANAPI_API SCAN_RESULT ScanCloseDevice(HANDLE hScanner);
SCANAPI_API SCAN_RESULT ScanGetDevInfo(HANDLE hScanner, LPSCANDEVINFO lpScanDevInfo);
SCANAPI_API SCAN_RESULT ScanRequestDataEvents(HANDLE hScanner, HWND hWnd, UINT wmScannerData);
SCANAPI_API SCAN_RESULT ScanTrigger(HANDLE hScanner);
SCANAPI_API SCAN_RESULT ScanAbort(HANDLE hScanner);
SCANAPI_API SCAN_RESULT ScanGetData(HANDLE hScanner, TCHAR * lpBuff, LPINT BufSize);
SCANAPI_API SCAN_RESULT ScanGetStatus(HANDLE hScanner);
SCANAPI_API SCAN_RESULT	ScanSetGoodReadSound(HANDLE hScanner, enum GRS_TYPE Sound, LPCTSTR lpWavFile);
SCANAPI_API SCAN_RESULT ScanDeinit(void);
SCANAPI_API SCAN_RESULT ScanErrToText(SCAN_RESULT Err, LPTSTR lpBuff, LPINT BufSize);
SCANAPI_API SCAN_RESULT ScanParamSend(HANDLE hScanner, PBYTE paramIn, UINT paramInLen,
									  PBYTE paramVal, UINT paramValLen);
SCANAPI_API SCAN_RESULT ScanParamRequest(HANDLE hScanner, PBYTE paramIn, UINT paramInLen,
										 PBYTE paramOut, PUINT paramOutLen);
SCANAPI_API SCAN_RESULT ScanParamSendEx(HANDLE hScanner, PUINT16 paramIn, UINT paramInLen,
									  PBYTE paramVal, UINT paramValLen);
SCANAPI_API SCAN_RESULT ScanParamRequestEx(HANDLE hScanner, PUINT16 paramIn, UINT paramInLen,
										 PBYTE paramOut, PUINT paramOutLen);
SCANAPI_API SCAN_RESULT WINAPI ScanEnableDisableSymbology (HANDLE hScanner, INT nSymID, BOOL flag);
SCANAPI_API SCAN_RESULT WINAPI ScanReadSymbologyConfig (HANDLE hScanner, INT cfgType, INT nSymbol, PVOID pvSym);
SCANAPI_API SCAN_RESULT WINAPI ScanWriteSymbologyConfig (HANDLE hScanner, INT nSymbol, PVOID pvSym);
SCANAPI_API SCAN_RESULT ScanSendCommand(HANDLE hScanner, 
										IN OUT LPTSTR pCmd_CmdResponse, IN OUT int* piLength);
SCANAPI_API SCAN_RESULT ScanEnableCHS(HWND hWnd, UINT wmCHSStatus);
SCANAPI_API SCAN_RESULT ScanDisableCHS(HANDLE hScanner);
SCANAPI_API SCAN_RESULT ScanSetSaveWindowHndFlag();
SCANAPI_API SCAN_RESULT SetScanOptions(int CmdOpcode, DWORD Param);
SCANAPI_API BOOL		IsScannerConnected (void);
SCANAPI_API BOOL		IsScannerConnectedEx (enum SCANNER_TYPE scannerType);
SCANAPI_API SCAN_RESULT ScanSendCmdtoCHS(HANDLE hScanner, PBYTE paramIn, UINT paramInLen, DWORD Timeout);
SCANAPI_API SCAN_RESULT ScanGetScannerRegKey(HANDLE hScanner, BOOL activeKey, LPTSTR lpBuff, LPINT BufSize);

// RFID exported functions
SCANAPI_API SCAN_RESULT ScanRFIDSelectTag(HANDLE hScanner, BYTE tagType, IN PBYTE tagID, BOOL loopMode,
										  BOOL invMode, BYTE afi);
SCANAPI_API SCAN_RESULT ScanRFIDReadTag(HANDLE hScanner, BYTE tagType, IN PBYTE tagID, DWORD startBlock, 
										DWORD numBlocks);
SCANAPI_API SCAN_RESULT ScanRFIDWriteTag(HANDLE hScanner, BYTE tagType, IN PBYTE tagID, DWORD startBlock, 
										 DWORD numBlocks, IN PBYTE data, DWORD dataSize);
SCANAPI_API SCAN_RESULT ScanRFIDGetData(HANDLE hScanner, OUT PRFID_RESPONSE response, 
										OUT PBYTE responseData, IN OUT PUINT responseDataSize);
SCANAPI_API SCAN_RESULT ScanRFIDLockTag(HANDLE hScanner, BYTE tagType, IN PBYTE tagID, DWORD startBlock, 
										DWORD numBlocks);

#ifdef __cplusplus
  }
#endif

